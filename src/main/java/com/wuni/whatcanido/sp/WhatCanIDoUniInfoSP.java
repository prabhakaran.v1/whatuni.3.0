package com.wuni.whatcanido.sp;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
  * This SP is used to get the Uni employment info of What course i do(Page3).
  * @author
  * @version 1.0
  * @since 3-JUNE-2014 Release.
  * Change Log
  * *******************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
  * *******************************************************************************************************************************
  * 08-Mar-2016    Prabhakaran V.            1.1      Page redesign                                                        wu_550
  *
  */
public class WhatCanIDoUniInfoSP extends StoredProcedure {
  public WhatCanIDoUniInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.SORT_WHATCANIDO_INFO_PRC);
    declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_jacs_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual_type_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_tariff_point", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_sortby_which_col_str", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_sortby_how_str", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_employ_info_fetch_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_JACS_subject_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniEmpRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_emp_info", OracleTypes.CURSOR, new EmploymentInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_industry_info", OracleTypes.CURSOR, new IndustryRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_job_info", OracleTypes.CURSOR, new JobRowMapperImpl()));
  }
  public Map execute(WhatCanIDoBean inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_ukprn", inputBean.getUkprn());
      inMap.put("p_college_id", inputBean.getCollegeId());
      inMap.put("p_jacs_code", inputBean.getJacsCode());
      inMap.put("p_qual_type_id", inputBean.getQualificationType());
      inMap.put("p_tariff_point", inputBean.getTariffPoint());
      inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());
      inMap.put("p_sortby_which_col_str", inputBean.getSortingType());
      inMap.put("p_sortby_how_str", inputBean.getSortingOrder());
      inMap.put("p_employ_info_fetch_flag", inputBean.getEmployeeInfoFlag());
      inMap.put("p_page_no", inputBean.getPageNo());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class UniEmpRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean universitiesEmpVO = new WhatCanIDoBean();
      universitiesEmpVO.setCollegeId(rs.getString("college_id"));
      universitiesEmpVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      universitiesEmpVO.setEmpRate(rs.getString("stickman_figure"));
      universitiesEmpVO.setEmpRateColor(rs.getString("stickman_figure_colour"));
      universitiesEmpVO.setSalaryRange(rs.getString("salary_range"));
      universitiesEmpVO.setSalaryRangeColor(rs.getString("salary_range_colour"));
      universitiesEmpVO.setSalaryBarWidth(rs.getString("salary_display_percentages"));
      universitiesEmpVO.setUkprn(rs.getString("ukprn"));
      universitiesEmpVO.setImagePath(rs.getString("college_logo"));
      universitiesEmpVO.setProviderViewDegreeFlag(rs.getString("view_degree_flag"));
      universitiesEmpVO.setSubjectGuideURL(rs.getString("college_url"));
      universitiesEmpVO.setTotalCount(rs.getString("total_cnt"));
      universitiesEmpVO.setCollegeNameForUrl(rs.getString("college_name_for_url"));
      universitiesEmpVO.setJobIndustryFlag(rs.getString("view_industry_job_flag"));
      return universitiesEmpVO;
    }
  }
  private class IndustryRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean industryInfoVO = new WhatCanIDoBean();
      industryInfoVO.setIndustryCode(rs.getString("industry_code"));
      industryInfoVO.setIndustryDescription(rs.getString("industry_desc"));
      industryInfoVO.setIndustryWisePerncentage(rs.getString("industry_wise_percentage"));
      String donutColor = rs.getString("colour");
      if (!GenericValidator.isBlankOrNull(donutColor)) { //Hex color for donut
        industryInfoVO.setIndustryColour("grn".equalsIgnoreCase(donutColor)? "#00b260": "red".equalsIgnoreCase(donutColor)? "#FC7268": "org".equalsIgnoreCase(donutColor)? "#FF8B00": "");
      }
      return industryInfoVO;
    }
  }
  private class JobRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean jobIndfoVO = new WhatCanIDoBean();
      jobIndfoVO.setJobCode(rs.getString("job_code"));
      jobIndfoVO.setJobDescription(rs.getString("job_desc"));
      jobIndfoVO.setJobWisePercentage(rs.getString("job_wise_percentage"));
      String donutColor = rs.getString("colour");
      if (!GenericValidator.isBlankOrNull(donutColor)) { //Hex color for donut
        jobIndfoVO.setJobColour("grn".equalsIgnoreCase(donutColor)? "#00b260": "red".equalsIgnoreCase(donutColor)? "#FC7268": "org".equalsIgnoreCase(donutColor)? "#FF8B00": "");
      }
      return jobIndfoVO;
    }
  }
  private class EmploymentInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean employmentInfoVO = new WhatCanIDoBean();
      employmentInfoVO.setEmpRate(rs.getString("stickman_figure"));
      employmentInfoVO.setEmpRateColor(rs.getString("stickman_figure_colour"));
      employmentInfoVO.setSalaryRange(rs.getString("salary_range"));
      employmentInfoVO.setSalaryRangeColor(rs.getString("salary_range_colour"));
      employmentInfoVO.setSalaryBarWidth(rs.getString("salary_display_percentages"));
      employmentInfoVO.setJacsSubject(rs.getString("jacs_subject"));
      return employmentInfoVO;
    }
  }
}
