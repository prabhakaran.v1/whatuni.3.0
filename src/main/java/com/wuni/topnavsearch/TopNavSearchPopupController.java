package com.wuni.topnavsearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.valueobject.interstitialsearch.GardeFilterVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.util.valueobject.interstitialsearch.QualificationVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;

/**
 * @see This class is used to show the top nav search popup pod
 * @author Sangeeth.S
 * @since  26.09.2019- intital draft
 * @version 1.0
 *
 * Modification history:
 * *****************************************************************************************************************
 * Author          Relase tag       Modification Details                                                  Rel Ver.
 * *****************************************************************************************************************  
 * Sangeeth.S       1.0             initial draft                                                           wu_595
 *
*/
@Controller
public class TopNavSearchPopupController {
  @RequestMapping(value="/ajax/common/top-navigation", method = {RequestMethod.GET,RequestMethod.POST}) 
  public ModelAndView topNavSearchPopupController(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    // ---(A)--- DECLARE local objects & varialbes
    TopNavSearchVO topNavSearchVO = new TopNavSearchVO();
    ISearchBusiness searchBusiness = null;
    Map outMap = null;
    CommonUtil commonUtil = new CommonUtil();
    CommonFunction common = new CommonFunction();
   // model.addAttribute("getJsDomainValue", CommonUtil.getJsPath());
    String clearingOnOffSysvar = common.getWUSysVarValue("CLEARING_ON_OFF");
    if(StringUtils.isNotBlank(clearingOnOffSysvar)) {
      //model.addAttribute("clearingHomeJsName", bundle.getString("wuni.common.clearing.home.js"));
      model.addAttribute("clearingOnOffSysvar", clearingOnOffSysvar);
      //System.out.println("clearingOnOffSysvartop nav-->"+clearingOnOffSysvar);
    }
    
    ArrayList recentSearchList = null;    
    ArrayList locationList = null;
    ArrayList adviceKeyTopicList = null;
    ArrayList studyLevelQualList = null;
    ArrayList clearingRecentSearchList = null;
    //
    // ---(B) Business logic follow
    //
    try {
      //
      // --- (B.1) Setting inputs for DB call ---
      //
      String queryString = request.getQueryString();
      
      //topNavSearchVO.setPageName("INTERSTITIAL_PAGE");
      //topNavSearchVO.setDefaultQualSelected(defaultQualSelected);
      topNavSearchVO.setUserId(new SessionData().getData(request, "y"));
      topNavSearchVO.setTrackSessionId(new SessionData().getData(request, "userTrackId"));//p_track_session
      /*if(StringUtils.isNotBlank(clearingOnOffSysvar) && StringUtils.equalsIgnoreCase(clearingOnOffSysvar, "ON")) {
    	topNavSearchVO.setClearingUserType(GlobalConstants.CLEARING_USER_TYPE_VAL);	
        }*/
      //
      // --- (B.1) Calling DB --
      //      
      searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
      outMap = searchBusiness.getTopNavSearchFilters(topNavSearchVO);
      //
      // --- (B.2) Get Cursors details from out map ---
      //
      recentSearchList = (ArrayList)outMap.get("PC_RECENT_SUBJECT_SEARCH");
      locationList = (ArrayList)outMap.get("PC_LOCATION_LIST");
      adviceKeyTopicList = (ArrayList)outMap.get("PC_KEY_TOPICS_LIST");
      studyLevelQualList = (ArrayList)outMap.get("PC_QUALIFICATION_LIST");
      clearingRecentSearchList = (ArrayList)outMap.get("PC_CL_RECENT_SUBJECT_SEARCH");
      //
      // --- (B.2.1)
      //
      /*
      if (!CommonUtil.isBlankOrNull(gardeFilterList)) {
        GardeFilterVO gardeFilterVO = null;
        for (int i = 0; i < gardeFilterList.size(); i++) {
          gardeFilterVO = (GardeFilterVO)gardeFilterList.get(i);
          if (!GenericValidator.isBlankOrNull(gardeFilterVO.getGradeStr())) {
            gardeFilterVO.setGradesList(Arrays.asList(gardeFilterVO.getGradeStr().split(",")));
          }
        }
      }
      //
      if (!CommonUtil.isBlankOrNull(qualificationList)) {
        QualificationVO qualificationVO = null;
        for (int i = 0; i < qualificationList.size(); i++) {
          qualificationVO = (QualificationVO)qualificationList.get(i);
          if (!GenericValidator.isBlankOrNull(qualificationVO.getUrlText())) {
            if ("clearing".equals(qualificationVO.getUrlText()) && "OFF".equals(clearingOnOffFlag)) {
              qualificationList.remove(i);
            }
          }
        }
      }
      */
      //
      // --- (B.3) Set in request scope for using view (JSP) ---
      //
      
      request.setAttribute("recentSearchList", recentSearchList);
      request.setAttribute("locationList", locationList);
      request.setAttribute("adviceKeyTopicList", adviceKeyTopicList);    
      request.setAttribute("studyLevelQualList", studyLevelQualList);
      model.addAttribute("clearingRecentSearchList", clearingRecentSearchList);
      //
    } catch (Exception ex) {
        ex.printStackTrace();
    } finally {
      //
      // -- (D) Nullify object -- 
      //
      searchBusiness = null;
      if (outMap != null) {
        outMap.clear();
        outMap = null;
      }
    }    
    //
     return new  ModelAndView("topnav-search-popup");
   }

}
