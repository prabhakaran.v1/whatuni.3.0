package com.wuni.contenthub.controller;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import spring.valueobject.seo.SEOMetaDetailsVO;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.token.TokenFormController;
import com.wuni.util.seo.ContentHubUrls;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.valueobject.contenthub.CampusVO;
import com.wuni.util.valueobject.contenthub.CollegeDetailsVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.CostOfLivingVO;
import com.wuni.util.valueobject.contenthub.EnquiryVO;
import com.wuni.util.valueobject.contenthub.GalleryVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.contenthub.PopularSubjectsVO;
import com.wuni.util.valueobject.contenthub.ReviewVO;
import com.wuni.util.valueobject.contenthub.SectionsVO;
import com.wuni.util.valueobject.contenthub.SocialVO;
import com.wuni.util.valueobject.contenthub.UserDetailsVO;
import com.wuni.util.valueobject.contenthub.StudentStatsVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.ModelAndView;

/**
  * @ContentHubAction
  * @author Sabapathi
  * @version 1.0
  * @since 23.JAN.2018
  * @purpose This program is used to display the content hub profile page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 31_07_2018    Prabhakaran V.             1.1      Added for hiding/showing opendays button                       wu_579
  * 01_03_2019    Sangeeth.S                 1.2      Added for CMMT institution profile page  
  * 24_04_2020 	  Sangeeth.S				 1.3      Handled open day event type changes for dimension 18                    
  * 14_06_2020    Sangeeth.S				 1.4      Added clearing show switch flag
  * 20_10_2020    Sri Sankari R              1.5      Added dynamic meta details from BO                              wu_20201020
*/

public class ContentHubController{

  public ModelAndView getContentHubDetails(ContentHubVO contentHubVO,  HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws Exception {
    // ---(A)--- DECLARE local objects & varialbes
	CommonUtil commonUtil = new CommonUtil();  
    Map contentHubDetailsMap = null;
    ArrayList latestUniReviewList = null;
    ArrayList studentUniReviewList = null;
    ArrayList enquiryInfoList = null;
    ArrayList socialNetworkUrlList = null;
    ArrayList campusesVenueList = null;
    ArrayList uniStatsList = null;
    ArrayList sectionsList = null;
    ArrayList livingCostList = null;
    ArrayList collegeDetailsList = null;
    ArrayList openDayList = null;
    ArrayList galleryList = null;
    ArrayList popularSubjectsList = null;
    ArrayList reviewRatingList = null;
    ArrayList userInfoList = null;
    String orderItemId = null;
    String subOrderItemId = null;
    String myhcProfileId = null;
    String profileType = null;
    String networkId = null;
    String collegeName = null;
    String collegeId = null;
    String collegeNameDisplay = null;
    String totalReviewCount = null;
    String overAllRating = null;
    String accomodationLower = null;
    String accomodationUpper = null;
    String livingCost = null;
    String costPint = null;
    String openDayFlag = null;
    String emailFlag = null;
    String emailWebFormFlag = null;
    String keyStatsEmptyFlag = null;
    String studentsStatsEmptyFlag = null;
    String allStatEmptyFlag = null;
    String collegeLogo = null;
    String absoluteRating = null;
    String galleryFlag = "N"; 
    ArrayList emailFlagList = null;
    String clearingCourseExistFlag = null;
    //
    // ---(B) Business logic follow
    //
    try {
      String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
      if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
        cpeQualificationNetworkId = GlobalConstants.UG_NETWORK_ID;
      }
      if (GlobalConstants.UG_NETWORK_ID.equals(cpeQualificationNetworkId)) {
        request.setAttribute("highlightTab", "UG");
      } else {
        request.setAttribute("highlightTab", "PG");
      }
      //
      //----DB call
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      //
      // Getting details from DB
      contentHubDetailsMap = commonBusiness.getContentHubDetails(contentHubVO);
      //
      // Setting Request atributes for using in JSP
      if (!CommonUtil.isBlankOrNull(contentHubDetailsMap)) {
        latestUniReviewList = (ArrayList)contentHubDetailsMap.get("OC_LATEST_UNI_REVIEWS");
        if (!CommonUtil.isBlankOrNull(latestUniReviewList)) {
          request.setAttribute("LATEST_UNI_REVIEW_LIST", latestUniReviewList);
          ReviewVO reviewVO = (ReviewVO)latestUniReviewList.get(0);
          request.setAttribute("collegeId",reviewVO.getCollegeId());
          request.setAttribute("collegeName",reviewVO.getCollegeName());
          request.setAttribute("rating",reviewVO.getOverAllRating());
        }
        //
        studentUniReviewList = (ArrayList)contentHubDetailsMap.get("OC_STUDENT_UNI_REVIEWS");
        if (!CommonUtil.isBlankOrNull(studentUniReviewList)) {
          ReviewVO reviewVO = null;
          for (int i = 0; i < studentUniReviewList.size(); i++) {
            reviewVO = (ReviewVO)studentUniReviewList.get(i);
            if (!GenericValidator.isBlankOrNull(reviewVO.getMediaPath()) && !"30".equals(reviewVO.getMediaTypeId())) {
              reviewVO.setMediaPath(CommonUtil.getImgPath(reviewVO.getMediaPath(), 0));
            } 
            if (!GenericValidator.isBlankOrNull(reviewVO.getThumbNailPath())) {
              reviewVO.setThumbNailPath(CommonUtil.getImgPath(reviewVO.getThumbNailPath(), 0));
            }
          }
          request.setAttribute("STUDENT_UNI_REVIEW_LIST", studentUniReviewList);
        }
        //
        enquiryInfoList = (ArrayList)contentHubDetailsMap.get("OC_ENQUIRY_INFO");
        if (!CommonUtil.isBlankOrNull(enquiryInfoList)) {
          request.setAttribute("ENQUIRY_INFO_LIST", enquiryInfoList);
          EnquiryVO enquiryInfoVO = new EnquiryVO();
          for (int i = 0; i < enquiryInfoList.size(); i++) {
            enquiryInfoVO = (EnquiryVO)enquiryInfoList.get(i);
            orderItemId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getOrderItemId())) ? enquiryInfoVO.getOrderItemId() : "";
            subOrderItemId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getSubOrderItemId())) ? enquiryInfoVO.getSubOrderItemId() : "";
            myhcProfileId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getMyhcProfileId())) ? enquiryInfoVO.getMyhcProfileId() : "";
            profileType = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getProfileType())) ? enquiryInfoVO.getProfileType() : "";
            networkId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getNetworkId())) ? enquiryInfoVO.getNetworkId() : "";
            emailFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailFlag())) ? enquiryInfoVO.getEmailFlag() : "";
            emailWebFormFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebFormFlag())) ? enquiryInfoVO.getEmailWebFormFlag() : "";
          }
        }
        //
        socialNetworkUrlList = (ArrayList)contentHubDetailsMap.get("OC_SOCIAL_NETWORK_URLS");
        if (!CommonUtil.isBlankOrNull(socialNetworkUrlList)) {
          SocialVO socialVO = null;
          for (int i = 0; i < socialNetworkUrlList.size(); i++) {
            socialVO = (SocialVO)socialNetworkUrlList.get(i);           
            if ("TWITTER".equalsIgnoreCase(socialVO.getSocialNetworkName())) {
              if (socialVO.getSocialNetworkUrl() != null && !"".equals(socialVO.getSocialNetworkUrl())) {
                String provTwitterName = "";
                if (socialVO.getSocialNetworkUrl().indexOf("https") != -1) {
                  if (socialVO.getSocialNetworkUrl().indexOf("www.") != -1) {
                    provTwitterName = socialVO.getSocialNetworkUrl().replace("https://www.twitter.com/", "");
                  } else {
                    provTwitterName = socialVO.getSocialNetworkUrl().replace("https://twitter.com/", "");
                  }
                } else if (socialVO.getSocialNetworkUrl().indexOf("http") != -1) {
                  if (socialVO.getSocialNetworkUrl().indexOf("www.") != -1) {
                    provTwitterName = socialVO.getSocialNetworkUrl().replace("http://www.twitter.com/", "");
                  } else {
                    provTwitterName = socialVO.getSocialNetworkUrl().replace("http://twitter.com/", "");
                  }
                } else if(socialVO.getSocialNetworkUrl().indexOf("@") != -1){
                    provTwitterName = socialVO.getSocialNetworkUrl().replace("@", "");
                }
                if (!"".equals(provTwitterName)) {
                  request.setAttribute("providerTwitterName", provTwitterName);
                }
                request.setAttribute("providerTwitterUrl", socialVO.getSocialNetworkUrl());
              }
            } else if ("FACEBOOK".equalsIgnoreCase(socialVO.getSocialNetworkName())) {
              if (socialVO.getSocialNetworkUrl() != null && !"".equals(socialVO.getSocialNetworkUrl())) {
                request.setAttribute("providerFaceBookUrl", socialVO.getSocialNetworkUrl().replace("https://www.facebook.com/", ""));
              }
            } else if ("INSTAGRAM".equalsIgnoreCase(socialVO.getSocialNetworkName())) { 
              if (!GenericValidator.isBlankOrNull(socialVO.getSocialNetworkUrl())) {
                String instagramUrl = socialVO.getSocialNetworkUrl();
                if(instagramUrl.indexOf("https://www.instagram.com/") >= 0){
                  instagramUrl = instagramUrl.replace("https://www.instagram.com/", "");
                  request.setAttribute("instagramURL", instagramUrl.replace("/", ""));
                }
              }
            }           
          }
          request.setAttribute("SOCIAL_NETWORK_URL_LIST", socialNetworkUrlList);
        }
        //
        galleryList = (ArrayList)contentHubDetailsMap.get("OC_HERO_GALLERY");
        if (!CommonUtil.isBlankOrNull(galleryList)) {
          GalleryVO galleryVO = null;
          for (int i = 0; i < galleryList.size(); i++) {
            galleryVO = (GalleryVO)galleryList.get(i);
            request.setAttribute("PromotionalText",galleryVO.getPromotionalText());
            if (!GenericValidator.isBlankOrNull(galleryVO.getMediaPath()) && !"30".equals(galleryVO.getMediaTypeId())) {
              galleryVO.setMediaPath(CommonUtil.getImgPath(galleryVO.getMediaPath(), 0));
            } 
            if (!GenericValidator.isBlankOrNull(galleryVO.getThumbNailPath())) {
              galleryVO.setThumbNailPath(CommonUtil.getImgPath(galleryVO.getThumbNailPath(), 0));
            }
          }
          request.setAttribute("GALLERY_LIST", galleryList);   
          if( galleryList.size() > 1){
            galleryFlag = "Y";            
          }
        }
        //
        campusesVenueList = (ArrayList)contentHubDetailsMap.get("OC_CAMPUSES_VENUE");
        if (!CommonUtil.isBlankOrNull(campusesVenueList)) {
          CampusVO campusVO = null;
          for (int i = 0; i < campusesVenueList.size(); i++) {
            campusVO = (CampusVO)campusesVenueList.get(i);
            if (!GenericValidator.isBlankOrNull(campusVO.getMediaPath())) {
              campusVO.setMediaPath(CommonUtil.getImgPath(campusVO.getMediaPath(), 0));
            }
          }
          request.setAttribute("CAMPUSES_VENUE_LIST", campusesVenueList);
        }
        //
        openDayList = (ArrayList)contentHubDetailsMap.get("OC_OPEN_DAY_INFO");
        if (!CommonUtil.isBlankOrNull(openDayList)) {   	  
          OpendayVO opendayVO = null;
          String openDayEventType = "";
    		opendayVO  = (OpendayVO)openDayList.get(0);
    		openDayEventType = commonUtil.setEventTypeValueGA(openDayEventType, opendayVO.getEventCategoryName());			
    	  if(!GenericValidator.isBlankOrNull(openDayEventType)) {
    	    openDayEventType = openDayEventType.substring(0, openDayEventType.length() - 1);
    	    request.setAttribute("eventCategoryNameGa", commonUtil.setOpenDayEventGALabel(openDayEventType));
    	    request.setAttribute("openDayEventType", openDayEventType);//For logging open day event type in dimension 18 by sangeeth.S in APR-24-20
    	  }	
          request.setAttribute("listOfOpendayInfo", openDayList);
          request.setAttribute("OPEN_DAY_LIST", openDayList);
          request.setAttribute("opendayListSize", openDayList.size());          
        }
        String hideOpendaysFlag = (String)contentHubDetailsMap.get("p_hide_od_button_flag");
        if(!GenericValidator.isBlankOrNull(hideOpendaysFlag)){
          request.setAttribute("hideOpendaysFlag", hideOpendaysFlag); //Version 1.1
        }
        uniStatsList = (ArrayList)contentHubDetailsMap.get("OC_UNI_STATS_INFO");
        if (!CommonUtil.isBlankOrNull(uniStatsList)) {
          StudentStatsVO studentStatsVO = null;
          keyStatsEmptyFlag = "Y";
          studentsStatsEmptyFlag = "Y";
          allStatEmptyFlag = "N";
          for(int i = 0; i < uniStatsList.size(); i++){
            studentStatsVO = (StudentStatsVO)uniStatsList.get(i);
            if(i < 4){
              if(!GenericValidator.isBlankOrNull(studentStatsVO.getCustomizedFlag())){
                keyStatsEmptyFlag = "N";
              }
            }
            if(i >= 4){
              if(!GenericValidator.isBlankOrNull(studentStatsVO.getCustomizedFlag())){
                studentsStatsEmptyFlag = "N";
              }
            }
          }
          if("Y".equalsIgnoreCase(keyStatsEmptyFlag) && "Y".equalsIgnoreCase(studentsStatsEmptyFlag)){
            allStatEmptyFlag = "Y";                        
          }
          request.setAttribute("UNI_STATS_LIST", uniStatsList);
        }
        List locationInfoList = (List)contentHubDetailsMap.get("PC_GET_ADMIN_VENUE");
        if (!CollectionUtils.isEmpty(locationInfoList)) {
          request.setAttribute("locationInfoList", locationInfoList);
        }
        sectionsList = (ArrayList)contentHubDetailsMap.get("OC_PROFILE_SECTIONS");
        if (!CommonUtil.isBlankOrNull(sectionsList)) {
          SectionsVO sectionsVO = null;
          String displaySectionNames = "";
          for (int i = 0; i < sectionsList.size(); i++) {
            sectionsVO = (SectionsVO)sectionsList.get(i);
            if("Overview".equalsIgnoreCase(sectionsVO.getSectionName())){              
              myhcProfileId = (!GenericValidator.isBlankOrNull(sectionsVO.getMyhcProfileId())) ? sectionsVO.getMyhcProfileId() : "";
              subOrderItemId = (!GenericValidator.isBlankOrNull(sectionsVO.getSubOrderItemId())) ? sectionsVO.getSubOrderItemId() : "";
            }
            if (!GenericValidator.isBlankOrNull(sectionsVO.getImagePath()) && !"VIDEO".equals(sectionsVO.getMediaType())) {
              sectionsVO.setImagePath(CommonUtil.getImgPath(sectionsVO.getImagePath(), 0));
            }
            if (!GenericValidator.isBlankOrNull(sectionsVO.getThumbnailPath())) {
              sectionsVO.setThumbnailPath(CommonUtil.getImgPath(sectionsVO.getThumbnailPath(), 0));
            }
            displaySectionNames = new CommonUtil().titleCaseWithSlash(sectionsVO.getSectionNameDisplay());
            sectionsVO.setDisplaySectionName(displaySectionNames);
          }
          request.setAttribute("SECTIONS_LIST", sectionsList);
        }
        //
        livingCostList = (ArrayList)contentHubDetailsMap.get("PC_COST_OF_LIVING");
        if (!CommonUtil.isBlankOrNull(livingCostList)) {
          CostOfLivingVO costOfLivingVO = null;
          for (int i = 0; i < livingCostList.size(); i++) {
            costOfLivingVO = (CostOfLivingVO)livingCostList.get(i);
            accomodationLower = costOfLivingVO.getAccomodationLower();
            accomodationUpper = costOfLivingVO.getAccomodationUpper();
            livingCost = costOfLivingVO.getLivingCost();
            costPint = costOfLivingVO.getCostPint();
              if(!GenericValidator.isBlankOrNull(costPint)){
                if(costPint.contains("-")){
                  String[] costPintOne = costPint.split("-");
                  if (costPintOne.length > 1) {
                    request.setAttribute("CostPintOne",costPintOne[0].trim());
                    request.setAttribute("CostPintTwo",costPintOne[1].trim());
                  }
                } else {
                  request.setAttribute("CostPint", costOfLivingVO.getCostPint());
                }
              }
          }
        }
        //
        String donutChartPercentage = (String)contentHubDetailsMap.get("O_JOB_PERCENT");
        String donutChartPercentageRemaining = GenericValidator.isBlankOrNull(donutChartPercentage) ? "" : String.valueOf(100 - Integer.parseInt(donutChartPercentage));
        request.setAttribute("DONUT_CHART_PERCENTAGE", donutChartPercentage);
        request.setAttribute("DONUT_CHART_PERCENTAGE_REMAIN", donutChartPercentageRemaining);
        //
        collegeDetailsList = (ArrayList)contentHubDetailsMap.get("OC_COLLEGE_DETAILS");
        if (!CommonUtil.isBlankOrNull(collegeDetailsList)) {
          CollegeDetailsVO collegeDetailsVO = null;
          for (int i = 0; i < collegeDetailsList.size(); i++) {
            collegeDetailsVO = (CollegeDetailsVO)collegeDetailsList.get(i);
            collegeName = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeName())) ? collegeDetailsVO.getCollegeName() : "";
            collegeId = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeId())) ? collegeDetailsVO.getCollegeId() : "";
            collegeNameDisplay = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeNameDisplay())) ? collegeDetailsVO.getCollegeNameDisplay() : "";
            totalReviewCount = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getReviewCount())) ? collegeDetailsVO.getReviewCount() : "";
            overAllRating = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getOverAllRating())) ? collegeDetailsVO.getOverAllRating() : "";
            collegeLogo = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeLogo())) ? CommonUtil.getImgPath(collegeDetailsVO.getCollegeLogo(), 0) : "";
            absoluteRating = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getAbsoluteRating())) ? collegeDetailsVO.getAbsoluteRating() : "";
          }
        }
        //         
        popularSubjectsList = (ArrayList)contentHubDetailsMap.get("OC_POPULAR_SUBJECTS");
        if (!CommonUtil.isBlankOrNull(popularSubjectsList)) {
          PopularSubjectsVO popularSubjectsVO = null; //need to do
          for (int i = 0; i < popularSubjectsList.size(); i++) {
            String url = "";
            String qual = "degree";
            popularSubjectsVO = (PopularSubjectsVO)popularSubjectsList.get(i);
            if (!GenericValidator.isBlankOrNull(popularSubjectsVO.getSubjectMyhcProfileId())) {
              url += new ContentHubUrls().construnctSpURL(popularSubjectsVO.getCollegeId(), popularSubjectsVO.getCollegeName(), popularSubjectsVO.getSubjectMyhcProfileId(), "0", networkId, "overview");             
            }
            if ("".equals(url)) {
              if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
                qual = "postgraduate";
              }
              url += new ContentHubUrls().constructCourseUrlCH(qual, profileType, collegeName, popularSubjectsVO.getBrowseTextKey());
            }
            popularSubjectsVO.setPopularSubjectURL(url);
          }
          request.setAttribute("POPULAR_SUBJECTS_LIST", popularSubjectsList);
        }
        //
        userInfoList = (ArrayList)contentHubDetailsMap.get("OC_USER_DETAILS");
        if (!CollectionUtils.isEmpty(userInfoList)) {
          UserDetailsVO userDetailsVO = new UserDetailsVO();
          userDetailsVO = (UserDetailsVO)userInfoList.get(0);
          String day = "";
          String month = "";
          String year = "";
          if (!GenericValidator.isBlankOrNull(userDetailsVO.getDateOfBirth())) {
            String[] dob = userDetailsVO.getDateOfBirth().split("/");
            day = dob[0];
            month = dob[1];
            year = dob[2];
          }
          userDetailsVO.setDay(day);
          userDetailsVO.setMonth(month);
          userDetailsVO.setYear(year);
          emailFlagList = (ArrayList)contentHubDetailsMap.get("PC_USER_SUBSCRIPTION_FLAGS");
          if (!CollectionUtils.isEmpty(emailFlagList)) {
            BasicFormVO basicFormVO = new BasicFormVO();
            basicFormVO = (BasicFormVO)emailFlagList.get(0);
            userDetailsVO.setMarketingEmail(basicFormVO.getMarketingEmail());
            userDetailsVO.setSolusEmail(basicFormVO.getSolusEmail());
            userDetailsVO.setSurveyEmail(basicFormVO.getSurveyEmail());
          
        } 
        }else {
          UserDetailsVO userDetailsVO = new UserDetailsVO();
          String[] yoeArr = new CommonFunction().getYearOfEntryArr();
          userDetailsVO.setYeartoJoinCourse(yoeArr[0]);
          userInfoList = new ArrayList();
          userInfoList.add(userDetailsVO);
        }
        request.setAttribute("USER_DETAILS_LIST", userInfoList);
        
        //
        reviewRatingList = (ArrayList)contentHubDetailsMap.get("OC_WUSCA_CATEGORIES_RATINGS");
        if (!CommonUtil.isBlankOrNull(reviewRatingList)) {
          request.setAttribute("REVIEW_RATING_LIST", reviewRatingList);
        }
        //
        String courseExistsFlag = (String)contentHubDetailsMap.get("O_COURSE_EXIST_FLAG");
        //        
        String collegeBasketFlag = (String)contentHubDetailsMap.get("O_COLLEGE_IN_BASKET");
        if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
          request.setAttribute("COLLEGE_IN_BASKET", collegeBasketFlag);
        }
        String pixelTrackingCode = (String)contentHubDetailsMap.get("O_PIXEL_TRACKING_CODE");
          if (!GenericValidator.isBlankOrNull(pixelTrackingCode)) {
            request.setAttribute("pixelTrackingCode", pixelTrackingCode);
          }
        String enquiryExistFlag = (String)contentHubDetailsMap.get("P_ENQUIRY_EXIST_FLAG");
        if(!GenericValidator.isBlankOrNull(enquiryExistFlag)){
          request.setAttribute("enquiryExistFlag", enquiryExistFlag);
        }
       //
        //Added review subject cursor for review redesign by Hema.S on 12_FEB_2019_REL   
         List reviewSubjectList = (List)contentHubDetailsMap.get("PC_REVIEW_SUBJECT");
         if (!CollectionUtils.isEmpty(reviewSubjectList)) {
           request.setAttribute("reviewSubjectList", reviewSubjectList);
         }
        //Added review breakdown cursor for review redesign by Hema.S on 12_FEB_2019_REL   
         List reviewBreakdownlist = (List)contentHubDetailsMap.get("PC_REVIEW_BREAKDOWN");
         if (!CollectionUtils.isEmpty(reviewBreakdownlist)) {
           request.setAttribute("reviewBreakdownlist", reviewBreakdownlist);
           ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakdownlist.get(0);
           request.setAttribute("questionTitle",reviewBreakDownVO.getQuestionTitle());
          request.setAttribute("rating",reviewBreakDownVO.getRating());
          request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
          request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
           
         }
          ArrayList commonPhrasesList = (ArrayList)contentHubDetailsMap.get("PC_COMMON_PHRASES");
          if (!CollectionUtils.isEmpty(commonPhrasesList)) { 
            CommonPhrasesVO  commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(0);
              if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
                request.setAttribute("commonPhrasesList", commonPhrasesList);
              }      
          }
          //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
          ArrayList clientLeadConsentList = (ArrayList) contentHubDetailsMap.get("O_CLIENT_CONSENT_DETAILS");
          if (!CollectionUtils.isEmpty(clientLeadConsentList)) {
        	ClientLeadConsentVO clientLeadConsentVO = (ClientLeadConsentVO)clientLeadConsentList.get(0);
        	request.setAttribute("consentFlag", clientLeadConsentVO.getClientMarketingConsent());
 
            request.setAttribute("clientLeadConsentList", clientLeadConsentList);
          }
          
          // Added for dynamic meta data details from Back office, 20201020 rel by Sri Sankari
    	  ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) contentHubDetailsMap.get("PC_META_DETAILS");
    	  new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);
          //Added for CMMT institution profile page by Sangeeth.S
          clearingCourseExistFlag = (String)contentHubDetailsMap.get("P_CL_COURSE_EXIST_FLAG");           
          //
        //Added for the top clearing switch pod For June_23_rel by sangeeth.s
          String showSwitchFlag = (String)contentHubDetailsMap.get("P_CLEARING_SWITCH_FLAG");
          request.setAttribute("SHOW_SWITCH_FLAG", showSwitchFlag);
          //
        //Creating the view all course url
        String viewAllCourseUrl = "";
        String qualification = "degree";
        if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
          qualification="postgraduate";
        }        
        if("N".equalsIgnoreCase(courseExistsFlag)){
          qualification = "all";
        }
        viewAllCourseUrl = new SeoUrls().constructCourseUrl(qualification, profileType, collegeName);  
         new TokenFormController().saveToken(request);
         String  breadCrumb = new SeoUtilities().getBreadCrumb("UNI_PROFILE_PAGE", "", "", "", "", collegeId);
         if (breadCrumb != null && breadCrumb.trim().length() > 0) {
          request.setAttribute("breadCrumb", breadCrumb);
         }
         //
        request.setAttribute("CLEARING_COURSE_EXIST_FLAG", clearingCourseExistFlag); 
        request.setAttribute("ORDER_ITEM_ID", orderItemId);
        request.setAttribute("SUBORDER_ITEM_ID", subOrderItemId);
        request.setAttribute("MYHC_PROFILE_ID", myhcProfileId);
        request.setAttribute("PROFILE_TYPE", profileType);
        request.setAttribute("NETWORK_ID", networkId);
        request.setAttribute("EMAIL_FLAG", emailFlag);
        request.setAttribute("EMAIL_WEBFORM_FLAG", emailWebFormFlag);
        request.setAttribute("COLLEGE_NAME", collegeName);
        request.setAttribute("hitbox_college_name", collegeName);
        request.setAttribute("COLLEGE_ID", collegeId);
        request.setAttribute("COLLEGEID_IN_INTERACTION", collegeId);
        request.setAttribute("COLLEGE_NAME_DISPLAY", collegeNameDisplay);
        //
        if(!GenericValidator.isBlankOrNull(collegeName)){
          request.setAttribute("cDimCollegeDisName", ("\""+collegeName+"\""));
        }
        //
        request.setAttribute("COLLEGE_NAME_GA", new CommonFunction().replaceSpecialCharacter(collegeNameDisplay));
        request.setAttribute("TOTAL_REVIEW_COUNT", totalReviewCount);
        request.setAttribute("OVERALL_RATING", overAllRating);
        
        request.setAttribute("collegeNameDisplay", collegeNameDisplay);
        request.setAttribute("reviewCount", totalReviewCount);
        request.setAttribute("overallRating", absoluteRating);
        
        request.setAttribute("ACCOMMODATION_LOWER", accomodationLower);
        request.setAttribute("ACCOMMODATION_UPPER", accomodationUpper);
        request.setAttribute("LIVING_COST", livingCost);
        request.setAttribute("COST_PINT", costPint);
        request.setAttribute("OPEN_DAY_FLAG", openDayFlag);
        request.setAttribute("REQUEST_URL", CommonUtil.getRequestedURL(request));
        request.setAttribute("REFERER_URL", request.getHeader("referer") == null ? "" : request.getHeader("referer"));
        request.setAttribute("KEY_STATS_EMPTY_FLAG", keyStatsEmptyFlag);
        request.setAttribute("STUDENTS_STATS_EMPTY_FLAG", studentsStatsEmptyFlag);
        request.setAttribute("ALL_STATS_EMPTY_FLAG", allStatEmptyFlag);
        request.setAttribute("VIEW_ALL_COURSE_URL", viewAllCourseUrl);
        request.setAttribute("COLLEGE_LOGO", collegeLogo);
        request.setAttribute("showBanner", "no");
        request.setAttribute("ABSOLUTE_RATING", absoluteRating);
        request.setAttribute("GALLERY_FLAG", galleryFlag);
        request.setAttribute("chAwardYearReq", new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS"));
        request.setAttribute("cDimCollegeId", collegeId);
        if(!GenericValidator.isBlankOrNull(collegeId)){
          request.setAttribute("cDimCollegeIds", ("'" + collegeId + "'"));
        }
      }
      //
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (contentHubDetailsMap != null) {
        contentHubDetailsMap.clear();
      }
      contentHubDetailsMap = null;
    }
    //
    return null;
  }

}
