package com.wuni.contenthub.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonUtil;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.contenthub.OpendayVO;

@Controller
public class LoadOpendayPopupController {
 
  @RequestMapping(value = "/openday-popup", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView getOpendaysPopup(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	String collegeId = !GenericValidator.isBlankOrNull(request.getParameter("collegeId")) ? request.getParameter("collegeId") : null;
	String eventId = !GenericValidator.isBlankOrNull(request.getParameter("eventId")) ? request.getParameter("eventId") : null;
    String qualCode = !GenericValidator.isBlankOrNull(request.getParameter("studyLevelId")) ? request.getParameter("studyLevelId") : "M";
	OpendayVO opendayVO = new OpendayVO();
	opendayVO.setCollegeId(collegeId);
	opendayVO.setEventId(eventId);
	opendayVO.setQualCode(qualCode);
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    Map opendaysAjaxInfoMap = commonBusiness.getOpendayPopupAjax(opendayVO);
    if(!CommonUtil.isBlankOrNull(opendaysAjaxInfoMap)){
	  ArrayList opendaysInfoList = (ArrayList)opendaysAjaxInfoMap.get("PC_OPENDAYS_INFO");
	  if(opendaysInfoList != null && opendaysInfoList.size() > 0){
	    request.setAttribute("opendaysInfoList", opendaysInfoList);
	  }
	  ArrayList opendayDetailList = (ArrayList)opendaysAjaxInfoMap.get("PC_OD_DET_LIST");
	  if(opendayDetailList != null && opendayDetailList.size() > 1){
	    request.setAttribute("opendayDetailList", opendayDetailList);
	  }
	}
    return new ModelAndView("opendayPopupAjax");
  }
}
