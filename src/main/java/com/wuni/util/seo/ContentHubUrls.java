package com.wuni.util.seo;

import java.util.ResourceBundle;

import org.apache.commons.validator.GenericValidator;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.wuni.util.CommonValidator;

public class ContentHubUrls {
  ResourceBundle bundle = CommonUtil.getProperties();
  public String constructReviewPageSeoUrl(String uniName, String uniId) {
    String reviewsSeoUrl = "";
    if ((uniName != null && uniName.trim().length() > 0) && (uniId != null && uniId.trim().length() > 0) ) {
      StringBuffer tmpSeoUrl = new StringBuffer();
      //tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
      //tmpSeoUrl.append("/degrees/university-course-reviews/");
      tmpSeoUrl.append("/university-course-reviews/");
      tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(uniName)));
      tmpSeoUrl.append("/");
      tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(uniId));        
      //tmpSeoUrl.append(".html");
      tmpSeoUrl.append("/");
      
      reviewsSeoUrl = tmpSeoUrl.toString().toLowerCase();
      //nullify unused objects
      tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
      tmpSeoUrl = null;
    }    
    return reviewsSeoUrl;
  }
  
  /**
   *
   * URL_PATTERN:  www.whatuni.com/degrees/courses/[COURSE_TITLE]/[COLLEGE_TITLE]/cd/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   * URL_EXAMPLE:  www.whatuni.com/degrees/courses/Accounting-and-Finance-BA-Hons/University-Of-Greenwich/cd/54930094/1039/cdetail.html  
   * CD page URL restructure 
   * @sincewu549_20160216
   * @author Prabhakaran V.
   * 
   *
   */
  public String construnctCourseDetailsPageURL(String studyLevelDesc, String courseTitle, String courseId, String collegeId, String pagename) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(studyLevelDesc) || validate.isBlankOrNull(courseTitle) || validate.isBlankOrNull(courseId) || validate.isBlankOrNull(collegeId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    StringBuffer courseDetailsPageURL = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    if(courseTitle.indexOf("/") != -1){
        courseTitle = courseTitle.replaceAll("/", " ");
    }
    String collegeName = null;
    collegeName = comFn.getCollegeTitleFunction(collegeId);
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    courseTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(courseTitle)));
    //
    courseDetailsPageURL.append("/degrees/");
    courseDetailsPageURL.append(courseTitle);
    courseDetailsPageURL.append("/");
    courseDetailsPageURL.append(collegeName);
    courseDetailsPageURL.append("/cd/");
    courseDetailsPageURL.append(courseId);
    courseDetailsPageURL.append("/");
    courseDetailsPageURL.append(collegeId);
    if("COURSE_SPECIFIC".equalsIgnoreCase(pagename) || "NORMAL_COURSE".equalsIgnoreCase(pagename)){
        courseDetailsPageURL.append("/");    
    }else if("CLEARING".equalsIgnoreCase(pagename) || "CLEARING_COURSE".equalsIgnoreCase(pagename)){
        courseDetailsPageURL.append("/?clearing");    
    }
    validate = null;
    comFn = null;
    return courseDetailsPageURL.toString().toLowerCase();
  }
  /**
   * will give link to opendays provider page
   * URL_PATTERN:  http://www.whatuni.com/degrees/open-days/a/od.html
   * URL_EXAMPLE:  http://www.whatuni.com/open-days/
   * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
   * @author Thiyagu G
   * @since wu338_20150317
   * @param providerName
   * @return providerId
   */
  public String constructOpendaysSeoUrl(String providerName, String providerId) {
    String opendaysSeoUrl = "";
    if ((providerName != null && providerName.trim().length() > 0) && (providerId != null && providerId.trim().length() > 0) ) {
      StringBuffer tmpSeoUrl = new StringBuffer();
      //tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
      //tmpSeoUrl.append("/degrees/open-days/");
      tmpSeoUrl.append("/open-days/");
      tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(providerName).trim()));
      tmpSeoUrl.append("/");       
      tmpSeoUrl.append(providerId);
      tmpSeoUrl.append("/");
      //tmpSeoUrl.append(".html");
      opendaysSeoUrl = tmpSeoUrl.toString().toLowerCase();
      //nullify unused objects
      tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
      tmpSeoUrl = null;
    }
    return opendaysSeoUrl;
  } //constructOpendaysSeoUrl
   /**
    *
    * URL_PATTERN: www.whatuni.com/degrees/home.html
    * EXAMPLE:     www.whatuni.com/degrees/home.html
    *
    * @since wu318_20111020
    * @author Mohamed Syed
    *
    */
   public String getWhatuniHomeURL() {
     return "/degrees/home.html";
   }
  /**
     *
     *  URL_PATTERN:  http://www.whatuni.com/degrees/subject-profile/[PROVIDER_NAME]-[SECTION_DESC]/[PROVIDER_ID]/[SECTION_DESC]/[MYHC_PROFILE_ID]/[PROFILE_ID]/[CPE_QUAL_NETWORK_ID]/subjectprofile.html
     *  URL_EXAMPLE:  http://www.whatuni.com/degrees/subject-profile/kensington-college-of-business-overview/904/overview/11446/0/2/subjectprofile.html
     *
     * @since wu318_20111020
     * @author Mohamed Syed
     *
     */
  public String construnctSpURL(String collegeId, String collegeName, String myhcProfileId, String profileId, String cpeQualificationNetworkId, String sectionDesc) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(collegeName) || validate.isBlankOrNull(collegeId) || validate.isBlankOrNull(myhcProfileId) || validate.isBlankOrNull(cpeQualificationNetworkId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    if (validate.isBlankOrNull(profileId)) {
      profileId = "0";
    }
    if (validate.isBlankOrNull(sectionDesc)) {
      sectionDesc = "overview";
    }
    //
    StringBuffer spURL = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    CommonUtil commonUtil = new CommonUtil();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    sectionDesc = commonUtil.removeSpecialCharAndConstructSeoText(sectionDesc);
    //
    spURL.append("/degrees/subject-profile/");
    spURL.append(collegeName);
    spURL.append("-");
    spURL.append(sectionDesc);
    spURL.append("/");
    spURL.append(collegeId);
    spURL.append("/");
    spURL.append(sectionDesc);
    spURL.append("/");
    spURL.append(myhcProfileId);
    spURL.append("/");
    spURL.append(profileId);
    spURL.append("/");
    spURL.append(cpeQualificationNetworkId);
    spURL.append("/subjectprofile.html");
    //nullify unused objects
    validate = null;
    comFn = null;
    commonUtil = null;
    //
    return spURL.toString().toLowerCase();
  }
  /**
   * URL formation for UG and PG redirection
   * @author Indumathi.S
   * @since wu548_20160127
   */
  public String constructCourseUrlCH(String qualification, String profileType, String collegeName, String subjectName) {
    String courseUrl = "";
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/");
    tmpSeoUrl.append(qualification);
    tmpSeoUrl.append("-courses");
    tmpSeoUrl.append("/");
    tmpSeoUrl.append("csearch?"); //
    if (!GenericValidator.isBlankOrNull(profileType) && "CLEARING".equals(profileType)) {
      tmpSeoUrl.append("clearing&");
    }
    tmpSeoUrl.append("subject=");
    tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subjectName)));
    tmpSeoUrl.append("&university=");
    tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(collegeName)));
    courseUrl = tmpSeoUrl.toString().toLowerCase();
    return courseUrl;
  }
  
  /**
   * URL formation for UG and PG redirection
   * @author Indumathi.S
   * @since wu548_20160127
   */
  public String constructCourseUrl(String qualification, String profileType, String collegeName) {
    String courseUrl = "";
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/");
    tmpSeoUrl.append(qualification);
    tmpSeoUrl.append("-courses");
    tmpSeoUrl.append("/");
    tmpSeoUrl.append("csearch?"); //
    if (!GenericValidator.isBlankOrNull(profileType) && "CLEARING".equals(profileType)) {
      tmpSeoUrl.append("clearing&");
    }
    tmpSeoUrl.append("university=");
    tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(collegeName)));
    courseUrl = tmpSeoUrl.toString().toLowerCase();
    return courseUrl;
  }
  //
  public String getHomeUrl(){
    String homeUrl = GlobalConstants.WHATUNI_SCHEME_NAME + bundle.getString("wuni.whatuni.device.specific.css.path");
    return homeUrl;
  }
  //
  public String getFindUniUrl(){
    String findUniUrl =  GlobalConstants.WHATUNI_SCHEME_NAME + bundle.getString("wuni.whatuni.device.specific.css.path") + GlobalConstants.WU_CONTEXT_PATH +"/find-university/";
    return findUniUrl;
  }
}
