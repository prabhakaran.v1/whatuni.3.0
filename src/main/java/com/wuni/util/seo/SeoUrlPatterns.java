package com.wuni.util.seo;

/**
  * will hold all SEO page's URL_PATTERN
  *
  * @author   Mohamed Syed
  * @since    wu314_20110712
  * @Purpose  : This program used to maintain all the global constans used in the Application.
  *
  */
public interface SeoUrlPatterns {

  public final String BROWSE_MONEY_PAGE_URL_PATTERN = "/degrees/courses/STUDY_LEVEL_DESC-courses/CATEGORY_DESC-STUDY_LEVEL_DESC-courses-LOCATION_WITH_HYPHEN/STUDY_LEVEL_CODE/LOCATION_WITH_PLUS/r/CATEGORY_ID/page.html";
  public final String BROWSE_MONEY_PAGE_URL_PATTERN_UNI = "/degrees/courses/STUDY_LEVEL_DESC-courses/CATEGORY_DESC-STUDY_LEVEL_DESC-courses-LOCATION_WITH_HYPHEN/STUDY_LEVEL_CODE/LOCATION_WITH_PLUS/r/CATEGORY_ID/uniview.html";
  public final String BROWSE_MONEY_PAGE_URL_PATTERN_CLCOURSE = "/degrees/courses/STUDY_LEVEL_DESC-courses/CATEGORY_DESC-STUDY_LEVEL_DESC-courses-LOCATION_WITH_HYPHEN/STUDY_LEVEL_CODE/LOCATION_WITH_PLUS/r/CATEGORY_ID/page.html?nd&clearing";
  public final String BROWSE_MONEY_PAGE_URL_PATTERN_CLUNI = "/degrees/courses/STUDY_LEVEL_DESC-courses/CATEGORY_DESC-STUDY_LEVEL_DESC-courses-LOCATION_WITH_HYPHEN/STUDY_LEVEL_CODE/LOCATION_WITH_PLUS/r/CATEGORY_ID/cluniview.html";
  
  public final String NEW_BROWSE_MONEY_PAGE_URL_PATTERN = "/STUDY_LEVEL_DESC-courses/search?subject=";
  public final String NEW_BROWSE_MONEY_PAGE_URL_PATTERN_CLCOURSE = "/STUDY_LEVEL_DESC-courses/search?clearing&subject=";
}
