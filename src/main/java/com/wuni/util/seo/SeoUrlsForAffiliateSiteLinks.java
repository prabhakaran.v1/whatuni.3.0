package com.wuni.util.seo;

import WUI.utilities.CommonFunction;

import com.wuni.util.CommonValidator;

public class SeoUrlsForAffiliateSiteLinks {

  /**
   *
   * SEO URL for provider specific clearing page:
   * URL_PATTERN: www.hotcourses.com/clearing-2011/[COLLEGE_NAME]-clearing-house/[AFFILAITE_ID]/[COLEEGE_ID]/clearing.html
   * Example:     www.hotcourses.com/clearing-2011/university-of-wales-lampeter-business-clearing-house/16180339/3770/clearing.html
   *
   * @since wu314_20110712
   * @author Mohamed Syed
   *
   */
  public String hcPoviderClearingURL(String collegeId, String collegeName) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(collegeId) || validate.isBlankOrNull(collegeName)) {
      return "javascript:void(0);";
    }
    String hcPoviderClearingURL_pattern = "http://www.hotcourses.com/clearing-2011/COLLEGE_NAME-clearing-house/16180339/COLEEGE_ID/clearing.html";
    CommonFunction comFn = new CommonFunction();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName))).toLowerCase(); //removeSpecialChar(collegeName);
    //
    String hcPoviderClearingURL = hcPoviderClearingURL_pattern.replaceAll("COLLEGE_NAME", collegeName);
    hcPoviderClearingURL = hcPoviderClearingURL.replaceAll("COLEEGE_ID", collegeId);
    //
    return hcPoviderClearingURL;
  }

  /**
   *
   * SEO URL for Hotcourses Clearing micro site:
   * URL_PATTERN: http://www.hotcourses.com/clearing-2011/[AFFILAITE_ID]/index.html
   * Example:     http://www.hotcourses.com/clearing-2011/16180339/index.html
   *
   * @since wu314_20110712
   * @author Mohamed Syed
   *
   */
  public String hcClearingHomeURL() {
    return "http://www.hotcourses.com/clearing-2011/16180339/index.html";
  }

  private String removeSpecialChar(String collegeName) {
    if (collegeName == null) {
      collegeName = "";
    }
    collegeName = collegeName.replaceAll(";", "-");
    collegeName = collegeName.replaceAll(" ", "-");
    collegeName = collegeName.replaceAll("(", "-");
    collegeName = collegeName.replaceAll(")", "-");
    collegeName = collegeName.replaceAll("[;", "-");
    collegeName = collegeName.replaceAll("]", "-");
    collegeName = collegeName.replaceAll(",", "-");
    collegeName = collegeName.replaceAll(".", "-");
    collegeName = collegeName.replaceAll("#", "-");
    collegeName = collegeName.replaceAll("+", "-");
    //
    collegeName = collegeName.replaceAll("----", "-");
    collegeName = collegeName.replaceAll("---", "-");
    collegeName = collegeName.replaceAll("--", "-");
    return collegeName;
  }

}
