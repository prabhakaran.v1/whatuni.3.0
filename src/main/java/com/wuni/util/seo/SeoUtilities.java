package com.wuni.util.seo;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;

import spring.valueobject.seo.BreadcrumbVO;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.CommonValidator;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.SpellingSuggestionVO;

/**
 * contains seo related utilities
 *
 * @author  Mohamed Syed
 * @since   wu313_20110628
 *
 */
public class SeoUtilities {

  /**
   * @getBreadCrumb method will return the complete SEO-microformated breadcrumb URLs with html tags. you just need to print..
   * @param callingFrom
   * @param qualificationCode
   * @param categoryId
   * @param categoryCode
   * @param location
   * @param collegeId
   * @return complete SEO-microformated breadcrumb URLs
   * @author Mohamed Syed
   * @since wu313_20110628
   * 
   * Modification history:
   * ************************************************************************************************************************
   * Author          Relase Date              Modification Details
   * ************************************************************************************************************************
   * Prabhakaran V.  21.03.2017               method changed to data model into spring and modified function into procedure 
   * 
   */
  public String getBreadCrumb(String callingFrom, String qualificationCode, String categoryId, String categoryCode, String location, String collegeId) {
    String breadCrumb = "";
    BreadcrumbVO breadcrumbVO = new BreadcrumbVO();
    breadcrumbVO.setPageName(callingFrom);
    breadcrumbVO.setQualCode(qualificationCode);
    breadcrumbVO.setCatId(!GenericValidator.isBlankOrNull(categoryId) ? categoryId : null);
    breadcrumbVO.setCatCode(categoryCode);
    breadcrumbVO.setLocation(location);
    breadcrumbVO.setCollegeId(!GenericValidator.isBlankOrNull(collegeId) ? collegeId : null);
    breadCrumb = getBreadcrumbContent(null, breadcrumbVO);
    return breadCrumb;
  }
  /**
   * @getKeywordBreadCrumb method return the SR page complete SEO-microformated breadcrumb URLs with html tags. you just need to print..
   * @param request
   * @param callingFrom
   * @param qualificationCode
   * @param categoryId
   * @param categoryCode
   * @param location
   * @param collegeId
   * @param keyword
   * @return complete SEO-microformated breadcrumb URLs
   * @author 
   * @since
   * 
   * Modification history:
   * ************************************************************************************************************************
   * Author          Relase Date              Modification Details
   * ************************************************************************************************************************
   * Prabhakaran V.  21.03.2017               method changed to data model into spring and modified function into procedure 
   * 
   */
  public String getKeywordBreadCrumb(HttpServletRequest request, String callingFrom, String qualificationCode, String categoryId, String categoryCode, String location, String collegeId, String keyword) {
    String breadCrumb = "";
    BreadcrumbVO breadcrumbVO = new BreadcrumbVO();
    breadcrumbVO.setPageName(callingFrom);
    breadcrumbVO.setQualCode(qualificationCode);
    breadcrumbVO.setCatId(!GenericValidator.isBlankOrNull(categoryId) ? categoryId : null);
    breadcrumbVO.setCatCode(categoryCode);
    breadcrumbVO.setLocation(location);
    breadcrumbVO.setCollegeId(!GenericValidator.isBlankOrNull(collegeId) ? collegeId : null);
    breadcrumbVO.setSearchKeyword(keyword);
    breadCrumb = getBreadcrumbContent(request, breadcrumbVO);
    return breadCrumb;
  }
  
  /**
   * @getBreadcrumbContent method to get the breadcrumb content
   * @param request
   * @param breadcrumbVO
   * @return breadCrumb as breadcrumb content
   * @author Prabhakaran V.
   * @since wu563_20170321
   */
    public String getBreadcrumbContent(HttpServletRequest request, BreadcrumbVO breadcrumbVO){
      ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
      String breadCrumb = "";
      Map getBreadcrumbData = searchBusiness.getBreadcrumbData(breadcrumbVO);
      if(getBreadcrumbData != null && !getBreadcrumbData.isEmpty()){
        breadCrumb = (String)getBreadcrumbData.get("p_breadcrumbs");
        ArrayList breadcrumbSchemaList = (ArrayList)getBreadcrumbData.get("pc_bc_schema_tagging"); //Schema tagging details, it comes only for SR page
        if(breadcrumbSchemaList != null && !breadcrumbSchemaList.isEmpty()){
          request.setAttribute("breadcrumbSchemaList", breadcrumbSchemaList);
          request.setAttribute("schemaListSize", String.valueOf(breadcrumbSchemaList.size()));
          
        }
      }
      return breadCrumb;
    }

  /**
   * will tell you the given categoryId is valid or not interms of forming URL.
   *
   * @author  Mohamed Syed
   * @since   wu316_20110823
   *
   * @param categoryId
   * @return true/false.
   *
   */
  public boolean isValidCategoryId(String categoryId) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNullOrZero(categoryId)) {
      validate = null;
      return false;
    }
    String isValidCategoryDB = "";
    DataModel dataModel = new DataModel();
    boolean isValidCategory = false;
    //
    Vector parameters = new Vector();
    parameters.add(categoryId);
    //
    try {
      isValidCategoryDB = dataModel.getString("wu_seo_pkg.check_cat_id_fn", parameters);
      isValidCategory = validate.getBoolean(isValidCategoryDB);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      //nullify unused objects
      dataModel = null;
      parameters.clear();
      parameters = null;
      isValidCategoryDB = null;
      validate = null;
    }
    return isValidCategory;
  }

  /**
   * will return advertiser's featured courses
   * @param keyword
   * @param categoryCode
   * @param qualCode
   * @return
   */
  public ArrayList getSimilarStudyLevelCourses(HttpServletRequest request, String keyword, String categoryCode, String qualCode, String pageName, String searchType) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList listSimilarStudyLevelCourses = new ArrayList();
    SpellingSuggestionVO spellingSuggestionVO = null;
    SeoUrls seoUrls = new SeoUrls();
    Vector parameters = new Vector();
    parameters.add(keyword);
    parameters.add(categoryCode);
    parameters.add(qualCode);
    parameters.add(searchType);
    try {
      resultset = dataModel.getResultSet("WU_UTIL_PKG.GET_SUBJECT_LIST_FN", parameters);
      while (resultset.next()) {
        spellingSuggestionVO = new SpellingSuggestionVO();
        spellingSuggestionVO.setCategoryId(resultset.getString("BROWSE_CAT_ID"));
        spellingSuggestionVO.setCategoryDesc(resultset.getString("SUBJECT"));
        spellingSuggestionVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
        spellingSuggestionVO.setLinkToBrowseMoneyPage(seoUrls.construnctNewBrowseMoneyPageURL(qualCode, spellingSuggestionVO.getCategoryId(), spellingSuggestionVO.getCategoryDesc(), pageName));
        listSimilarStudyLevelCourses.add(spellingSuggestionVO);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    request.setAttribute("listSimilarStudyLevelCourses", listSimilarStudyLevelCourses);
    return listSimilarStudyLevelCourses;
  } //getFeaturedCourseSponsership()

}//end of class
