package com.wuni.util.seo;

/**
 * will contain all SEO page names & flags which will be used to fetch the SEO META details from DB.
 *
 * @since wu306_20110405
 * @author Mohamed Syed.
 *
 */
public interface SeoPageNamesAndFlags {

  /**
   * PAGE:         Archieved Uni home(Uni landing) page. Since: wu311_20110531. Author: Mohamed Syed
   * URL_PATTERN:  www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   * URL_EXAMPLE:  ww.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/3769/cdetail.html 
   */
  public final String UNI_LANDING_PAGE_NAME = "COLLEGE LANDING PAGE";
  public final String UNI_LANDING_PAGE_FLAG = "UNI_LANDING";
  //
  //
  /**
   * PAGE:         Archieved Articles home page. Since: wu311_20110531. Author: Mohamed Syed
   * URL_PATTERN:  www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   * URL_EXAMPLE:  ww.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/3769/cdetail.html 
   */
  public final String COURSE_DETAILS_PAGE_NAME = "COURSE DETAILS";
  public final String COURSE_DETAILS_PAGE_FLAG = "COURSE_DETAIL";
  //
  //
  // 
  /**
   * PAGE:         Archieved Articles home page. Since: wu311_20110531. Author: Mohamed Syed
   * URL_PATTERN:  http://www.whatuni.com/degrees/student-centre/study-help-centre-archive/student-tips.html
   * URL_EXAMPLE:  http://www.whatuni.com/student-centre/study-help-centre-archive/student-tips.html
   */
  public final String ARCHIVED_ARTICLES_HOME_PAGE_NAME = "WU ARCHIVED ARTICLES HOME PAGE";
  public final String ARCHIVED_ARTICLES_HOME_PAGE_FLAG = "WU_ARCHIVED_ARTICLES_HOME_PAGE";
  //
  //
  // 
  
  /**
   * PAGE:        Article groups home page. Since: wu311_20110531. Author: Mohamed Syed
   * URL_PATTERN:  http://www.whatuni.com/degrees/student-centre/study-help-centre/student-tips.html
   * URL_EXAMPLE:  http://www.whatuni.com/student-centre/study-help-centre/student-tips.html
   */
  public final String ARTICLE_GROUPS_HOME_PAGE_NAME = "WU ARTICLE GROUPS HOME PAGE";
  public final String ARTICLE_GROUPS_HOME_PAGE_FLAG = "WU_ARTICLE_GROUPS_HOME_PAGE";
  //
  //
  //  
   
    /**
     * PAGE:        Article category page.
     * URL_PATTERN:  www.whatuni.com/student-centre/study-help-centre/[category-name].html
     * URL_EXAMPLE:  http://www.whatuni.com/student-centre/study-help-centre/student-finance.html
     */
    public final String ARCHIVED_ARTICLES_CATEGORY_PAGE_NAME = "WU STUDENT CENTER CATEGORY";
    public final String ARCHIVED_ARTICLES_CATEGORY_PAGE_FLAG = "WU STUDENT CENTER CATEGORY";
    //
    //
    //

  /**
    * PAGE:        new SEO page "browse > browse by location > browse by category > money page" 's second page. Since: wu306_20110405. Author: Mohamed Syed
    * URL_PATTERN: www.whatuni.com/degrees/courses/[LOCATION_NAME]-degree/[LOCATION_NAME]/[QUALIFICATION_CODE]/[PAGE_NO]/degrees.html
    * EXAMPLE:     www.whatuni.com/degrees/courses/london-degree/london/m/1/degrees.html
    */
  public final String BROWSE_COURSES_BY_SUBJECT_PAGE_NAME = "WU BROWSE COURSES BY SUBJECTS";
  public final String BROWSE_COURSES_BY_SUBJECT_PAGE_FLAG = "WU_BROWSE_COURSES_BY_SUBJECTS";
  //
  //
  // 

  /**
   * PAGE:        new SEO page "browse > browse by location > browse by category > money page" 's first page. Since: wu306_20110405. Author: Mohamed Syed
   * URL_PATTERN: www.whatuni.com/degrees/courses/uk-degree/degree-courses-by-location/degrees.html
   * EXAMPLE:     www.whatuni.com/degrees/courses/uk-degree/degree-courses-by-location/degrees.html
   */
  public final String BROWSE_COURSES_BY_LOCATION_PAGE_NAME = "WU BROWSE COURSES BY LOCATION";
  public final String BROWSE_COURSES_BY_LOCATION_PAGE_FLAG = "WU_BROWSE_COURSES_BY_LOCATION";
  //
  //
  //
  
  /**
   * PAGE:        new SEO page Clearing landing page. Since: wu588_20190423. Author: Sujitha V
   * URL_PATTERN: www.whatuni.com/a-level-btec-exam-survival-guide
   * EXAMPLE:     www.whatuni.com/a-level-btec-exam-survival-guide
   */
  public final String LEAD_CAPTURE_NAME = "LEAD CAPTURE"; 
  public final String LEAD_CAPTURE_FLAG = "LEAD_CAPTURE"; 
  
  /**
   * PAGE:        new SEO page Clearing profile page. Since: 11052020. Author: Sangeeth.S
   * URL_PATTERN: www.whatuni.com/a-level-btec-exam-survival-guide
   * EXAMPLE:     www.whatuni.com/a-level-btec-exam-survival-guide
   */
  public final String CLEARING_PROFILE_PAGE_FLAG = "CLEARING_PROFILE"; 
  public final String CLEARING_PROFILE_PAGE_NAME = "CLEARING PROFILE"; 
}
