package com.wuni.util.seo;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;

import spring.valueobject.search.RefineByOptionsVO;
import WUI.search.form.SearchBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.wuni.util.CommonValidator;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

/**
 * will contains all static SEO/non-SEO URLs. so this will be the centralized place where you can refer URLs
 *
 * @since wu306_20110405
 * @author Mohamed Syed
 * Modification history:
 * **************************************************************************************************************
 * Author           Relase Date           Modification Details
 * **************************************************************************************************************
 * Thiyagu G        27_Jan_2016           URL restructure -Added new method for generating StudyMode, StudyLevel, 
 *                                        Subject, Location and RefineList for search filter  
 * Thiaygu G        08_Mar_2016           URL restructure - Changed Uni Home URL.
 */
public class SeoUrls {

  /**
   * PASS_THROUGH_CONTEXT_PATH will be used to form BLOG & ARTICLE related URLs alone.
   *
   * DEVELOPMENT: we are making PASS_THROUGH_CONTEXT_PATH = "/degrees".
   * LIVE:        we are making PASS_THROUGH_CONTEXT_PATH = "".
   * REASON:      during development we dont want context-path removed URLs.
   * P.S:         This is purly to the URLs which dont have context path (like BLOG, and ARTICLE related URLs)
   *
   * @since wu311_20110531
   * @author Mohamed Syed
   *
   */
  private final String PASS_THROUGH_CONTEXT_PATH = ""; //TODO remove context-path from BLOG & ARTICLE related page URLs
  // private final String PASS_THROUGH_CONTEXT_PATH = "/degrees";

  /**
      *
      * URL_PATTERN:  http://www.whatuni.com/degrees/subprofilelist.html?z=[COLLEGE_ID]
      * URL_EXAMPLE:  http://www.whatuni.com/degrees/subprofilelist.html?z=904
      *
      * @since wu318_20111020
      * @author Mohamed Syed
      *
      */
  public String construnctSpListHomeURL(String collegeId) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(collegeId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    return ("/degrees/subprofilelist.html?z=" + collegeId);
  }

  public String construnctULURL(String ultype) {
    String url = "";
    if("WCID".equalsIgnoreCase(ultype)){
      //url = "/degrees/courses/a-level-choices.html";  //9_DEC_14 Modified by Amir based on New URL pattern provided
      url = "/degrees/courses/a-level-choices/";  //TODO Please uncomment this line and comment above while deploying in app server.
    }else{
      //url = "/degrees/courses/career-choices.html";   //9_DEC_14 Modified by Amir based on New URL pattern provided
      url = "/degrees/courses/career-choices/"; //TODO Please uncomment this line and comment above while deploying in app server.
    }
    return url;
  }

  /**
     *
     *  URL_PATTERN:  http://www.whatuni.com/degrees/subject-profile/[PROVIDER_NAME]-[SECTION_DESC]/[PROVIDER_ID]/[SECTION_DESC]/[MYHC_PROFILE_ID]/[PROFILE_ID]/[CPE_QUAL_NETWORK_ID]/subjectprofile.html
     *  URL_EXAMPLE:  http://www.whatuni.com/degrees/subject-profile/kensington-college-of-business-overview/904/overview/11446/0/2/subjectprofile.html
     *
     * @since wu318_20111020
     * @author Mohamed Syed
     *
     */
  public String construnctSpURL(String collegeId, String collegeName, String myhcProfileId, String profileId, String cpeQualificationNetworkId, String sectionDesc) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(collegeName) || validate.isBlankOrNull(collegeId) || validate.isBlankOrNull(myhcProfileId) || validate.isBlankOrNull(cpeQualificationNetworkId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    if (validate.isBlankOrNull(profileId)) {
      profileId = "0";
    }
    if (validate.isBlankOrNull(sectionDesc)) {
      sectionDesc = "overview";
    }
    //
    StringBuffer spURL = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    CommonUtil commonUtil = new CommonUtil();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    sectionDesc = commonUtil.removeSpecialCharAndConstructSeoText(sectionDesc);
    //
    spURL.append("/degrees/subject-profile/");
    spURL.append(collegeName);
    spURL.append("-");
    spURL.append(sectionDesc);
    spURL.append("/");
    spURL.append(collegeId);
    spURL.append("/");
    spURL.append(sectionDesc);
    spURL.append("/");
    spURL.append(myhcProfileId);
    spURL.append("/");
    spURL.append(profileId);
    spURL.append("/");
    spURL.append(cpeQualificationNetworkId);
    spURL.append("/subjectprofile.html");
    //nullify unused objects
    validate = null;
    comFn = null;
    commonUtil = null;
    //
    return spURL.toString().toLowerCase();
  }

  /**
    *
    * URL_PATTERN: www.whatuni.com/degrees/university-uk/[COLLGE_NAME]-ranking/[COLLEGE_ID]/page.html
    * EXAMPLE:     www.whatuni.com/degrees/university-uk/University-Of-Worcester-ranking/5635/page.html
    *
    * @since wu318_20111020
    * @author Mohamed Syed
    *
    */
  public String construnctUniHomeURL(String collegeId, String collegeName) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(collegeName) || validate.isBlankOrNull(collegeId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    //URL restructure - Added Uni Home URL for 08_Mar_2016 By Thiyagu G.
    StringBuffer uniHomeURL = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    uniHomeURL.append("/university-profile/"); 
    uniHomeURL.append(collegeName);
    uniHomeURL.append("/");
    uniHomeURL.append(collegeId);
    uniHomeURL.append("/");    
    validate = null;
    comFn = null;
    return uniHomeURL.toString().toLowerCase();//25_MAR_2014_REL
  }

  /**
   *
   * URL_PATTERN:  www.whatuni.com/degrees/courses/[COURSE_TITLE]/[COLLEGE_TITLE]/cd/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   * URL_EXAMPLE:  www.whatuni.com/degrees/courses/Accounting-and-Finance-BA-Hons/University-Of-Greenwich/cd/54930094/1039/cdetail.html  
   * CD page URL restructure 
   * @sincewu549_20160216
   * @author Prabhakaran V.
   * 
   *
   */
  public String construnctCourseDetailsPageURL(String studyLevelDesc, String courseTitle, String courseId, String collegeId, String pagename) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(studyLevelDesc) || validate.isBlankOrNull(courseTitle) || validate.isBlankOrNull(courseId) || validate.isBlankOrNull(collegeId)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    StringBuffer courseDetailsPageURL = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    if(courseTitle.indexOf("/") != -1){
        courseTitle = courseTitle.replaceAll("/", " ");
    }
    String collegeName = null;
    collegeName = comFn.getCollegeTitleFunction(collegeId);
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    courseTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(courseTitle)));
    //
    courseDetailsPageURL.append("/degrees/");
    courseDetailsPageURL.append(courseTitle);
    courseDetailsPageURL.append("/");
    courseDetailsPageURL.append(collegeName);
    courseDetailsPageURL.append("/cd/");
    courseDetailsPageURL.append(courseId);
    courseDetailsPageURL.append("/");
    courseDetailsPageURL.append(collegeId);
    if("COURSE_SPECIFIC".equalsIgnoreCase(pagename) || "NORMAL_COURSE".equalsIgnoreCase(pagename)){
        courseDetailsPageURL.append("/");    
    }else if("CLEARING".equalsIgnoreCase(pagename) || "CLEARING_COURSE".equalsIgnoreCase(pagename)){
        courseDetailsPageURL.append("/?clearing");    
    }
    validate = null;
    comFn = null;
    return courseDetailsPageURL.toString().toLowerCase();
  }
  public String construnctCourseDetailsPageURL(String studyLevelDesc, String courseTitle, String courseId, String collegeId, String pagename, String opportunityId) {
     CommonValidator validate = new CommonValidator();
     if (validate.isBlankOrNull(studyLevelDesc) || validate.isBlankOrNull(courseTitle) || validate.isBlankOrNull(courseId) || validate.isBlankOrNull(collegeId)) {
       validate = null;
       return getWhatuniHomeURL();
     }
     StringBuffer courseDetailsPageURL = new StringBuffer();
     CommonFunction comFn = new CommonFunction();
     if(courseTitle.indexOf("/") != -1){
         courseTitle = courseTitle.replaceAll("/", " ");
     }
     String collegeName = null;
     collegeName = comFn.getCollegeTitleFunction(collegeId);
     collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
     courseTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(courseTitle)));
     //
     courseDetailsPageURL.append("/degrees/");
     courseDetailsPageURL.append(courseTitle);
     courseDetailsPageURL.append("/");
     courseDetailsPageURL.append(collegeName);
     courseDetailsPageURL.append("/cd/");
     courseDetailsPageURL.append(courseId);
     courseDetailsPageURL.append("/");
     courseDetailsPageURL.append(collegeId);
     if("COURSE_SPECIFIC".equalsIgnoreCase(pagename) || "NORMAL_COURSE".equalsIgnoreCase(pagename)){
       courseDetailsPageURL.append("/");
     }else if("CLEARING".equalsIgnoreCase(pagename) || "CLEARING_COURSE".equalsIgnoreCase(pagename)){
       courseDetailsPageURL.append("/?clearing");
     }
     //Passing opportunityId for forming CD page url from apply now journey by Prabha
     if(!GenericValidator.isBlankOrNull(opportunityId)){
       if(courseDetailsPageURL.indexOf("?") > -1){
         courseDetailsPageURL.append("&opportunityId=");
         courseDetailsPageURL.append(opportunityId);
       }else{
         courseDetailsPageURL.append("?opportunityId=");
         courseDetailsPageURL.append(opportunityId);
       }
     }
     //
     validate = null;
     comFn = null;
     return courseDetailsPageURL.toString().toLowerCase();
   }
  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/university-application-ucas/applying-university/university-admissions.html
   * EXAMPLE:     www.whatuni.com/degrees/university-application-ucas/applying-university/university-admissions.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getApplyingToUniURL() {
    return "/degrees/university-application-ucas/applying-university/university-admissions.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/home.html
   * EXAMPLE:     www.whatuni.com/degrees/home.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getWhatuniHomeURL() {
    return "/degrees/home.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/addreview.html?x=[X]&y=[Y]
   * EXAMPLE:     www.whatuni.com/degrees/addreview.html?x=442085207506&y=2691033
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getWriteAReviewURL(String x, String y) {
    return "/degrees/addreview.html?x=" + x + "&y=" + y;
  }

/**
 * URL_PATTERN: www.whatuni.com/degrees/clearing-home.html
 * @since wu407_10042012
 * @author Sekhar Kasala
 * 
 */
 public String getClearingHomePageURL(){
     return "/university-clearing-"+GlobalConstants.CLEARING_YEAR+".html";
 }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/song-contest-song-showcase/song-contest.html
   * EXAMPLE:     www.whatuni.com/degrees/song-contest-song-showcase/song-contest.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getSongShowcaseHomeURL() {
    return "/degrees/song-contest-song-showcase/song-contest.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/uk-best-university-ranking/rankings-2009/universityrankings.html
   * EXAMPLE:     www.whatuni.com/degrees/uk-best-university-ranking/rankings-2009/universityrankings.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getStudentChoiceAwardWinnersHomeURL() {
    return "/degrees/uk-best-university-ranking/rankings-2009/universityrankings.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/courses/ucas-course-codes/200/1/ucascode.html
   * EXAMPLE:     www.whatuni.com/degrees/courses/ucas-course-codes/200/1/ucascode.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getUcasBrowseHomeURL() {
    return "/degrees/courses/ucas-course-codes/200/1/ucascode.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/university-colleges-uk/uni-browse.html
   * EXAMPLE:     www.whatuni.com/degrees/university-colleges-uk/uni-browse.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getUniByLocationHomeURL() {
    return "/degrees/university-colleges-uk/uni-browse.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/prospectus/university-prospectus-college-prospectus/order-prospectus.html
   * EXAMPLE:     www.whatuni.com/degrees/prospectus/university-prospectus-college-prospectus/order-prospectus.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getBulkProspectusHomeURL() {
    return "/degrees/prospectus/university-prospectus-college-prospectus/order-prospectus.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/open-days/a/university-open-days.html
   * EXAMPLE:     www.whatuni.com/degrees/open-days/a/university-open-days.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getOpenDaysBrowseHomeURL() {
    return "/degrees/open-days.html";
  }

    public String getUniProspectusHomeURL(){
        return "/degrees/prospectus/university-prospectus-college-prospectus/order-prospectus.html";
    }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/courses/uk-degree/degree-courses-by-location/degrees.html
   * EXAMPLE:     www.whatuni.com/degrees/courses/uk-degree/degree-courses-by-location/degrees.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getDegreeCoursesByCitiesURL() {
    return "/degrees/courses/uk-degree/degree-courses-by-location/degrees.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/courses/browse.html
   * EXAMPLE:     www.whatuni.com/degrees/courses/browse.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getStudyLevelBrowseHomeURL() {
    return "/degrees/courses/browse.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/university-uk/find-university/browse.html
   * EXAMPLE:     www.whatuni.com/degrees/university-uk/find-university/browse.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getInstitutionFinderURL() {
    return "/degrees/find-university/";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/viewprofile.html?x=[X]&y=[Y]&e=reg
   * EXAMPLE:     www.whatuni.com/degrees/viewprofile.html?x=442085207506&y=2691033&e=reg
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getUserProfileHomeURL(String x, String y) {
    return ("/degrees/viewprofile.html?x=" + x + "&y=" + y + "&e=reg");
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/college-videos/student-videos/highest-rated/1/vids.html
   * EXAMPLE:     www.whatuni.com/degrees/college-videos/student-videos/highest-rated/1/vids.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getVideoReviewHomeURL() {
    return "/degrees/college-videos/student-videos/highest-rated/1/vids.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/reviewHome.html
   * EXAMPLE:     www.whatuni.com/degrees/reviewHome.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getReviewHomeURL() {
    return "/degrees/reviewHome.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/searchHome.html
   * EXAMPLE:     www.whatuni.com/degrees/searchHome.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getSearchHomeURL() {
    return "/degrees/searchHome.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/advertise.html
   * EXAMPLE:     www.whatuni.com/degrees/advertise.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getAdvertiseWithUsURL() {
    return "/degrees/advertise-with-what-uni.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/aboutus.html
   * EXAMPLE:     www.whatuni.com/degrees/aboutus.html
   *
   * @since wu318_20111020
   * @author Mohamed Syed
   *
   */
  public String getAboutUsURL() {
    return "/degrees/about-what-uni.html";
  }

  /**
   *
   * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-courses/[CATEGORY_DESC]-[STUDY_LEVEL_DESC]-courses-[LOCATION_WITH_HYPHEN]/[STUDY_LEVEL_CODE]/[LOCATION_WITH_PLUS]/r/[CATEGORY_ID]/page.html";
   * EXAMPLE:     www.whatuni.com/degrees/courses/degree-courses/business-degree-courses-england/m/england/r/8219/page.html
   *
   * @param qualificationCode
   * @param categoryId
   * @param categoryDesc
   * @param location
   * @return urlToBrowseMoneyPage
   *
   * @since wu306_20110405
   * @author Mohamed Syed
   *
   */
  public String construnctBrowseMoneyPageURL(String qualificationCode, String categoryId, String categoryDesc, String location, String pageName) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(qualificationCode) || validate.isBlankOrNull(categoryId) || validate.isBlankOrNull(categoryDesc) || validate.isBlankOrNull(location)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    //
    CommonFunction common = new CommonFunction();
    String urlTemplate = "";
    if (validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("PAGE")) { // -- /courses/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/page.html
        urlTemplate = SeoUrlPatterns.BROWSE_MONEY_PAGE_URL_PATTERN;
    } else if (validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("UNIVIEW")) { // -- /courses/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/uniview.html        
        urlTemplate = SeoUrlPatterns.BROWSE_MONEY_PAGE_URL_PATTERN_UNI;
    }else if(validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("CLCOURSE")){
        urlTemplate = SeoUrlPatterns.BROWSE_MONEY_PAGE_URL_PATTERN_CLCOURSE;
    }else if(validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("CLUNIVIEW")){
        urlTemplate = SeoUrlPatterns.BROWSE_MONEY_PAGE_URL_PATTERN_CLUNI;
    }
    String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); //refered from regionHome.jsp       
    String locationWithPlus = location.replaceAll(" ", "+"); //refered from regionHome.jsp  
    categoryDesc = common.replaceHypen(common.replaceURL(categoryDesc));
    String categoryDesc1 = categoryDesc.replaceAll("All-", " ");
    String qualificationDesc = "";
    qualificationCode = qualificationCode.toUpperCase();
    if (qualificationCode.equals("M")) {
      qualificationDesc = "degree";
    } else if (qualificationCode.equals("N")) {
      qualificationDesc = "hnd-hnc";
    } else if (qualificationCode.equals("A")) {
      qualificationDesc = "foundation-degree";
    } else if (qualificationCode.equals("T")) {
      qualificationDesc = "access-foundation";
    } else if (qualificationCode.equals("L")) {
      qualificationDesc = "postgraduate";
    }
    String urlToBrowseMoneyPage = "";
    //-- "/degrees/courses/[STUDY_LEVEL_DESC]-courses/[CATEGORY_DESC]-[STUDY_LEVEL_DESC]-courses-[LOCATION_WITH_HYPHEN]/[STUDY_LEVEL_CODE]/[LOCATION_WITH_PLUS]/r/[CATEGORY_ID]/page.html";
    urlToBrowseMoneyPage = urlTemplate.replaceAll("STUDY_LEVEL_DESC", qualificationDesc);
    urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("STUDY_LEVEL_CODE", qualificationCode);
    urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("LOCATION_WITH_HYPHEN", locationWithHyphen);
    urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("LOCATION_WITH_PLUS", locationWithPlus);
    urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("CATEGORY_DESC", categoryDesc.replaceAll("All-", ""));
    urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("CATEGORY_ID", categoryId);
    //
    //nulify unused objects
    common = null;
    //
    return urlToBrowseMoneyPage.toLowerCase();
  }

  public String construnctNewBrowseMoneyPageURL(String qualificationCode, String categoryId, String categoryDesc, String pageName) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(qualificationCode) || validate.isBlankOrNull(categoryId) || validate.isBlankOrNull(categoryDesc)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    //    
    CommonFunction common = new CommonFunction();
    String urlTemplate = "";
    if (validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("PAGE")) {
        urlTemplate = SeoUrlPatterns.NEW_BROWSE_MONEY_PAGE_URL_PATTERN;
    } else if(validate.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("CLCOURSE")){
        urlTemplate = SeoUrlPatterns.NEW_BROWSE_MONEY_PAGE_URL_PATTERN_CLCOURSE;
    }
    categoryDesc = common.replaceHypen(common.replaceURL(categoryDesc));    
    String qualificationDesc = "";
    qualificationCode = qualificationCode.toUpperCase();
    if (qualificationCode.equals("M")) {
      qualificationDesc = "degree";
    } else if (qualificationCode.equals("N")) {
      qualificationDesc = "hnd-hnc";
    } else if (qualificationCode.equals("A")) {
      qualificationDesc = "foundation-degree";
    } else if (qualificationCode.equals("T")) {
      qualificationDesc = "access-foundation";
    } else if (qualificationCode.equals("L")) {
      qualificationDesc = "postgraduate";
    }
    String urlToBrowseMoneyPage = "";
    //--/degrees/STUDY_LEVEL_DESC-courses/search/
    urlToBrowseMoneyPage = urlTemplate.replaceAll("STUDY_LEVEL_DESC", qualificationDesc);
    urlToBrowseMoneyPage = urlToBrowseMoneyPage + categoryDesc;
    //nulify unused objects
    common = null;
    //
    return urlToBrowseMoneyPage.toLowerCase();
  }
    
  /**
     *
     * URL_PATTERN: www.whatuni.com/degrees/courses/[QUALIFICATION_DESC]-courses/[SUBJECT_DESC]-[QUALIFICATION_DESC]-courses-[LOCATION_1]/m/[LOCATION_2]/r/[SUBJECT_ID]/page.html
     * EXAMPLE:     www.whatuni.com/degrees/courses/degree-courses/business-degree-courses-england/m/england/r/8219/page.html
     *
     * @param urlTemplate = "/degrees/courses/degree-courses/SUBJECT_NAME-degree-courses-LOCATION_1/m/LOCATION_2/r/SUBJECT_ID/page.html"
     * @param location
     * @param subjectId
     * @param subjectDesc
     * @return
     *
     * @since wu306_20110405
     * @author Mohamed Syed
     *
     */
  public String construnctUrlToBrowseMoneyPage(String urlTemplate, String location, String subjectId, String subjectDesc) {
    String urlToBrowseMoneyPage = "";
    if (urlTemplate != null && location != null && subjectId != null && subjectDesc != null) {
      //
      CommonFunction common = new CommonFunction();
      String location1 = common.replaceURL(common.replaceHypen(String.valueOf(location))); //refered from regionHome.jsp       
      String location2 = location.replaceAll(" & ", " & ").replaceAll(" ", "+"); //refered from regionHome.jsp   
      //
      //-- degrees/courses/degree-courses/SUBJECT_NAME-degree-courses-LOCATION_1/m/LOCATION_2/r/SUBJECT_ID/page.html
      urlToBrowseMoneyPage = urlTemplate.replaceAll("LOCATION_1", location1);
      urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("LOCATION_2", location2);
      urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("SUBJECT_NAME", common.replaceHypen(common.replaceURL(subjectDesc)));
      urlToBrowseMoneyPage = urlToBrowseMoneyPage.replaceAll("SUBJECT_ID", subjectId);
      //
      //nulify unused objects
      common = null;
      location1 = null;
      location2 = null;
    }
    return urlToBrowseMoneyPage.toLowerCase();
  }

  /**
   *
   * new SEO page "browse > browse by location > browse by category > money page" 's second page. Since:2011-APR-05. Author: Mohamed Syed
   * URL_PATTERN: www.whatuni.com/degrees/courses/[LOCATION_NAME]-degree/[LOCATION_NAME]/[QUALIFICATION_CODE]/[PAGE_NO]/degrees.html -->
   * Example:     www.whatuni.com/degrees/courses/london-degree/london/m/1/degrees.html -->
   *
   * @param location
   * @param qualificationCode
   * @return
   *
   * @since wu306_20110405
   * @author Mohamed Syed
   *
   */
  public String construnctUrlToSubjectListingPage(String location, String qualificationCode) {
    StringBuffer urlToSubjectListingPage = new StringBuffer();
    if (location != null && location.trim().length() > 0) {
      //
      location = location.replaceAll("ALL ", "");
      //
      if (qualificationCode == null || qualificationCode.trim().length() == 0) {
        qualificationCode = "m";
      }
      //
      CommonFunction common = new CommonFunction();
      String locationName1 = common.replaceURL(common.replaceHypen(String.valueOf(location))); //refered from regionHome.jsp
      String locationName2 = location.replaceAll(" & ", " & ").replaceAll(" ", "+"); //refered from regionHome.jsp
      //
      urlToSubjectListingPage.append("/degrees/courses/");
      urlToSubjectListingPage.append(locationName1);
      urlToSubjectListingPage.append("-degree/");
      urlToSubjectListingPage.append(locationName2);
      urlToSubjectListingPage.append("/");
      urlToSubjectListingPage.append(qualificationCode);
      urlToSubjectListingPage.append("/1/degrees.html");
      //nulify unused objects
      common = null;
      locationName1 = null;
      locationName2 = null;
    }
    return urlToSubjectListingPage.toString().toLowerCase();
  }

  /**
   * will give link Category home ("Footer > Degree") URLs based on QUALIFICATION_CODE
   *
   * URL_PATTERN:   http://www.whatuni.com/degrees/courses/[QUALIFICATION_DESC]-UK/qualification/[QUALIFICATION_CODE]/list.html
   * URL_EXAMPLE:   http://www.whatuni.com/degrees/courses/Degree-UK/qualification/M/list.html
   *                http://www.whatuni.com/degrees/courses/HND-HNC-UK/qualification/N/list.html
   *                http://www.whatuni.com/degrees/courses/Foundation-Degree-UK/qualification/A/list.html
   *                http://www.whatuni.com/degrees/courses/Access-foundation-UK/qualification/T/list.html
   *                http://www.whatuni.com/degrees/courses/Postgraduate-UK/qualification/L/list.html
   *
   * @author Mohamed Syed
   * @since wu314_20110712
   *
   * @return browseCategoryHomeUrl
   *
   */
  public String getBrowseCategoryHomeUrl(String qualificationCode) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(qualificationCode)) {
      validate = null;
      return getWhatuniHomeURL();
    }
    qualificationCode = qualificationCode.toUpperCase();
    String browseCategoryHomeUrl = "/degrees/courses/Degree-UK/qualification/M/list.html";
    if (qualificationCode.equals("M")) {
      browseCategoryHomeUrl = "/degrees/courses/Degree-UK/qualification/M/list.html";
    } else if (qualificationCode.equals("N")) {
      browseCategoryHomeUrl = "/degrees/courses/HND-HNC-UK/qualification/N/list.html";
    } else if (qualificationCode.equals("A")) {
      browseCategoryHomeUrl = "/degrees/courses/Foundation-Degree-UK/qualification/A/list.html";
    } else if (qualificationCode.equals("T")) {
      browseCategoryHomeUrl = "/degrees/courses/Access-foundation-UK/qualification/T/list.html";
    } else if (qualificationCode.equals("L")) {
      browseCategoryHomeUrl = "/degrees/courses/Postgraduate-UK/qualification/L/list.html";
    }
    return browseCategoryHomeUrl;
  } //getBrowseCategoryHomeUrl

  /**
   * will give link to archived-article-details page
   *
   * URL_PATTERN:  http://www.whatuni.com/degrees/student-centre/study-help-centre-archive/student-tips.html
   * URL_EXAMPLE:  http://www.whatuni.com/student-centre/study-help-centre-archive/student-tips.html
   * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
   *
   * @author Mohamed Syed
   * @since wu311_20110531
   *
   * @return archivedArticlesPageUrl
   *
   */
  public String getArchivedArticlesPageUrl() {
    return (PASS_THROUGH_CONTEXT_PATH + "/student-centre/study-help-centre-archive/student-tips.html");
  } //getArchivedArticlesPageUrl

  /**
   * will give link to article-details page
   *
   * URL_PATTERN:  http://www.whatuni.com/degrees/student-centre/[TITLE_USED_IN_URL]/student-tips.html
   * URL_EXAMPLE:  http://www.whatuni.com/student-centre/undergratuate-maths-subject/student-tips.html
   * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
   * NOTE 2:       [TITLE_USED_IN_URL] - is the replacement parameter from CMI, this is mandatory and defined by SEOs during article creation.
   *
   * @author Mohamed Syed
   * @since wu311_20110531
   *
   * @param articleSeoUrlSeoTitle
   * @return articleSeoUrlSeoTitle
   *
   */
  public String constructArticleSeoUrl(String articleSeoUrlSeoTitle, String articleGroupUrlSeoName ) {
    String articleSeoUrl = "";
    if ((articleSeoUrlSeoTitle != null && articleSeoUrlSeoTitle.trim().length() > 0) && (articleGroupUrlSeoName != null && articleGroupUrlSeoName.trim().length() > 0) ) {
      StringBuffer tmpSeoUrl = new StringBuffer();
      tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
      tmpSeoUrl.append("/student-centre/");
      tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(articleGroupUrlSeoName)));
      tmpSeoUrl.append("/");
      tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(articleSeoUrlSeoTitle));
      tmpSeoUrl.append(".html");
      articleSeoUrl = tmpSeoUrl.toString().toLowerCase();
      //nullify unused objects
      tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
      tmpSeoUrl = null;
    }
    return articleSeoUrl;
  } //constructArticleSeoUrl

   /**
    * will give link to advice-details page
    *
    * URL_PATTERN:  http://www.whatuni.com/degrees/advice/[name-of-article]/[id].html
    * URL_EXAMPLE:  http://www.whatuni.com/advice/undergratuate-maths-subject/2145.html
    * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
    * NOTE 2:       [TITLE_USED_IN_URL] - is the replacement parameter from CMI, this is mandatory and defined by SEOs during article creation.
    *
    * @author Thiyagu G
    * @since wu333_20141209
    *
    * @param adviceSeoUrlSeoTitle
    * @return adviceSeoUrlSeoTitle
    *
    */
   public String constructAdviceSeoUrl(String advicePrimaryCategoryName, String postUrlName, String postId) {
     String articleSeoUrl = "";
     if ((advicePrimaryCategoryName != null && advicePrimaryCategoryName.trim().length() > 0) && (postUrlName != null && postUrlName.trim().length() > 0) ) {
       StringBuffer tmpSeoUrl = new StringBuffer();
       tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
       //tmpSeoUrl.append("/degrees/advice/");
       tmpSeoUrl.append("/advice/");
       tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(advicePrimaryCategoryName)));
       tmpSeoUrl.append("/");
       tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(postUrlName));
       tmpSeoUrl.append("/");
       tmpSeoUrl.append(postId);
       tmpSeoUrl.append("/");
       //tmpSeoUrl.append(".html");
       articleSeoUrl = tmpSeoUrl.toString().toLowerCase();
       //nullify unused objects
       tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
       tmpSeoUrl = null;
     }
     return articleSeoUrl;
   } //constructArticleSeoUrl
    
  /**
   * will give link to article-groups-home page
   *
   * URL_PATTERN:  http://www.whatuni.com/degrees/student-centre/study-help-centre/student-tips.html
   * URL_EXAMPLE:  http://www.whatuni.com/student-centre/study-help-centre/student-tips.html
   * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
   *
   * @author Mohamed Syed
   * @since wu311_20110531
   *
   * @return archivedArticlesPageUrl
   *
   */
  public String getArticleGroupsHomePageUrl() {
    return (PASS_THROUGH_CONTEXT_PATH + "/student-centre/student-tips.html");
  } //getArticleGroupsHomePageUrl  

  /**
   * will give link to review-details page
   *
   * URL_PATTERN: http://www.whatuni.com/degrees/showReviewDetail.html?rid=[REVIEW_ID]&z=[COLLEGE_ID]
   * Example:     http://www.whatuni.com/degrees/showReviewDetail.html?rid=27031&z=3745
   *
   * @author Mohamed Syed
   * @since wu310_20110517
   *
   * @param collegeId
   * @param reviewId
   * @return reviewDetailsUrl
   *
   */
  public String constructReviewDetailsUrl(String collegeId, String reviewId) {
    if (collegeId == null || collegeId.trim().length() == 0 || reviewId == null || reviewId.trim().length() == 0) {
      return "";
    }
    //
    StringBuffer reviewDetailsUrl = new StringBuffer();
    reviewDetailsUrl.append("/degrees/showReviewDetail.html?rid=");
    reviewDetailsUrl.append(reviewId);
    reviewDetailsUrl.append("&z=");
    reviewDetailsUrl.append(collegeId);
    //return
    return reviewDetailsUrl.toString();
  } //constructReviewDetailsUrl

  /**
   * will give link to uni-specific-review page
   *
   * URL_PATTERN: http://www.whatuni.com/degrees/reviews/uni/[COLLEGE_NAME]-reviews/[ORDER_BY]/[FILTER_BY]/[SUBJECT_CODE]/[COLLEGE_ID]/[PAGE_NO]/studentreviews.html
   * EXAMPLE:     http://www.whatuni.com/degrees/reviews/uni/birkbeck-university-of-london-reviews/highest-rated/0/0/3449/1/studentreviews.html
   *
   * @author Mohamed Syed
   * @since wu310_20110517
   *
   * @param collegeId
   * @param collegeName
   * @return uniSpecificReviewUrl
   *
   */
  public String constructUniSpecificReviewUrl(String collegeId, String collegeName) {
    if (GenericValidator.isBlankOrNull(collegeId) || GenericValidator.isBlankOrNull(collegeName)) {
      return "";
    }
    //Modified the review URL by Indumathi.S Mar_29_2016
    StringBuffer uniSpecificReviewUrl = new StringBuffer();
    CommonFunction comFn = new CommonFunction();
    collegeName = collegeName.trim();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    //
    uniSpecificReviewUrl.append(GlobalConstants.SEO_PATH_LIST_REVIEW);
    uniSpecificReviewUrl.append(collegeName);
    uniSpecificReviewUrl.append("/");
    uniSpecificReviewUrl.append(collegeId);
    uniSpecificReviewUrl.append("/");
    //nullify unused objects
    comFn = null;
    //return
    return uniSpecificReviewUrl.toString().toLowerCase();
  } //constructUniSpecificReviewUrl

  /**
   * will give link to uni-specific-schoalrship page
   *
   * URL_PATTERN: http://www.whatuni.com/degrees/scholarship-uk/[COLEEGE_NAME]-scholarships-bursaries/[STUDY_LEVEL]/[COLLEGE_ID]/[PAGE_NO]/bursary-uni.html
   * EXAMPLE:     http://www.whatuni.com/degrees/scholarship-uk/university-of-east-anglia-scholarships-bursaries/m,n,a,t,l/5637/2/bursary-uni.html
   *
   * @author Mohamed Syed
   * @since wu310_20110517
   *
   * @param collegeId
   * @param collegeName
   * @return uniSpecificScholarshipUrl
   *
   */
  public String constructUniSpecificScholarshipUrl(String collegeId, String collegeName) {
    if (collegeId == null || collegeId.trim().length() == 0 || collegeName == null || collegeName.trim().length() == 0) {
      return "";
    }
    //
    CommonFunction comFn = new CommonFunction();
    StringBuffer uniSpecificScholarshipUrl = new StringBuffer();
    collegeName = collegeName.trim();
    collegeName = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(collegeName)));
    //
    uniSpecificScholarshipUrl.append("/degrees/scholarship-uk/");
    uniSpecificScholarshipUrl.append(collegeName);
    uniSpecificScholarshipUrl.append("-scholarships-bursaries/m,n,a,t,l/");
    uniSpecificScholarshipUrl.append(collegeId);
    uniSpecificScholarshipUrl.append("/1/bursary-uni.html");
    //nullify unused objects
    comFn = null;
    //return
    return uniSpecificScholarshipUrl.toString().toLowerCase();
  } //constructUniSpecificScholarshipUrl

  /**
   * will give link to uni-individual-scholarship page
   * 
   * URL_PATTERN: http://www.whatuni.com/degrees/scholarship-uk/[SCHOLARSHIP-TITLE]/[SCHOLARSHIP-ID]/bursary-info.html
   * EXAMPLE : http://www.whatuni.com/degrees/scholarship-uk/llm-master-of-laws-scholarships/16018/bursary-info.html
   * 
   * @author Sekhar K
   * @since wu310_20110517
   *
   * @param scholarshipId
   * @param scholarshipTitle
   * @return UniIndividualScholarshipUrl
   */
   public String UniIndividualScholarshipUrl(String scholarshipId, String scholarshipTitle) {
       if (scholarshipId == null || scholarshipId.trim().length() == 0 || scholarshipTitle == null || scholarshipTitle.trim().length() == 0) {
         return "";
       }
       CommonFunction comFn = new CommonFunction();
       StringBuffer uniIndividualScholarshipUrl = new StringBuffer();
       scholarshipTitle = scholarshipTitle.trim();
       scholarshipTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(scholarshipTitle)));
       //If last character of the scholarshipTile having specialCharater, in this case hypen is forming at last position,
       //The following few lines will remove that hypen.
       char lastChar = scholarshipTitle.charAt(scholarshipTitle.length() - 1);
       if(lastChar == '-'){
            scholarshipTitle = scholarshipTitle.substring(0, scholarshipTitle.length() - 1);
       }
       //
       uniIndividualScholarshipUrl.append("/degrees/scholarship-uk/");
       uniIndividualScholarshipUrl.append(scholarshipTitle);
       uniIndividualScholarshipUrl.append("/");
       uniIndividualScholarshipUrl.append(scholarshipId);
       uniIndividualScholarshipUrl.append("/bursary-info.html");
       //nullify unused objects
       comFn = null;
       //return
       return uniIndividualScholarshipUrl.toString().toLowerCase();
   }

  /**
   * will give link to uni-specific-opendays page
   *
   * URL_PATTERN:  http://www.whatuni.com/degrees/open-days/[COLLEGE_LOCATION]-open-days-[PROVIDER_NAME]/[COLLEGE_ID]/page.html
   * URL:          http://www.whatuni.com/degrees/open-days/aberdeen-open-days-aberdeen-college/43961/page.html
   *
   * @author Mohamed Syed
   * @since wu310_20110517
   *
   * @param collegeId
   * @param collegeName
   * @param collegeLocation
   * @return uniLandingReviewUrl
   *
   */
  public String constructUniSpecificOpenDaysUrl(String collegeId, String collegeName, String collegeLocation) {
    if (collegeId == null || collegeId.trim().length() == 0 || collegeName == null || collegeName.trim().length() == 0 || collegeLocation == null || collegeLocation.trim().length() == 0) {
      return "";
    }
    //
    CommonFunction comFn = new CommonFunction();
    StringBuffer uniSpecificOpenDaysUrl = new StringBuffer();
    //
    collegeName = collegeName.trim();
    collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
    collegeLocation = collegeLocation.trim();
    collegeLocation = comFn.replaceHypen(comFn.replaceURL(collegeLocation));
    //
    uniSpecificOpenDaysUrl.append("/degrees/open-days/");
    uniSpecificOpenDaysUrl.append(collegeLocation);
    uniSpecificOpenDaysUrl.append("-open-days-");
    uniSpecificOpenDaysUrl.append(collegeName);
    uniSpecificOpenDaysUrl.append("/");
    uniSpecificOpenDaysUrl.append(collegeId);
    uniSpecificOpenDaysUrl.append("/page.html");
    //nullify unused objects
    comFn = null;
    //return
    return uniSpecificOpenDaysUrl.toString().toLowerCase();
  } //constructUniSpecificOpenDaysUrl
   
   /**
    * will give link to opendays provider page
    * URL_PATTERN:  http://www.whatuni.com/degrees/open-days/a/od.html
    * URL_EXAMPLE:  http://www.whatuni.com/open-days/
    * NOTE 1:       The original URL won't have the context-path. PT-command will update the context-path to the URL from App-server-level.
    * @author Thiyagu G
    * @since wu338_20150317
    * @param providerName
    * @return providerId
    */
   public String constructOpendaysSeoUrl(String providerName, String providerId) {
     String opendaysSeoUrl = "";
     if ((providerName != null && providerName.trim().length() > 0) && (providerId != null && providerId.trim().length() > 0) ) {
       StringBuffer tmpSeoUrl = new StringBuffer();
       tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
       //tmpSeoUrl.append("/degrees/open-days/");
       tmpSeoUrl.append("/open-days/");
       tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(providerName).trim()));
       tmpSeoUrl.append("/");       
       tmpSeoUrl.append(providerId);
       tmpSeoUrl.append("/");
       //tmpSeoUrl.append(".html");
       opendaysSeoUrl = tmpSeoUrl.toString().toLowerCase();
       //nullify unused objects
       tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
       tmpSeoUrl = null;
     }
     return opendaysSeoUrl;
   } //constructOpendaysSeoUrl
   
   /**
    * will give link to city guide page
    *
    * URL_PATTERN: http://www.whatuni.com/degrees/[loccation]/[guidetitle].html
    * EXAMPLE:     http://www.whatuni.com/degrees/manchester/manchester-guide.html
    * 
    *
    * @param locationName
    * @param articleSeoUrlSeoTitle
    * @return CityGuideSeoUrl
    *
    */  
   public String constructCityGuideSeoUrl(String articleSeoUrlSeoTitle, String locationName ) {
     String articleSeoUrl = "";
     if ((articleSeoUrlSeoTitle != null && articleSeoUrlSeoTitle.trim().length() > 0) && (locationName != null && locationName.trim().length() > 0) ) {
       StringBuffer tmpSeoUrl = new StringBuffer();
       tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
       tmpSeoUrl.append("/student-city-guides/");
       tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(locationName.toLowerCase())));
       tmpSeoUrl.append("/");
       tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(articleSeoUrlSeoTitle.toLowerCase()));
       tmpSeoUrl.append(".html");
       articleSeoUrl = tmpSeoUrl.toString().toLowerCase();
       //nullify unused objects
       tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
       tmpSeoUrl = null;
     }
     return articleSeoUrl;
   } //constructArticleSeoUrl
   
    /**
     * will give link to city guide page
     *
     * URL_PATTERN: http://www.whatuni.com/degrees/[loccation]/[guidetitle].html
     * EXAMPLE:     http://www.whatuni.com/degrees/manchester/manchester-guide.html
     * 
     *
     * @param locationName
     * @param articleSeoUrlSeoTitle
     * @return CityGuideSeoUrl
     *
     */  
    public String constructUltimateGuideSeoUrl(String articleSeoUrlSeoTitle) {
      String articleSeoUrl = "";
      if ((articleSeoUrlSeoTitle != null && articleSeoUrlSeoTitle.trim().length() > 0) ) {
        StringBuffer tmpSeoUrl = new StringBuffer();
        tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
        tmpSeoUrl.append("/ultimate-guides/");
        tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(articleSeoUrlSeoTitle.toLowerCase()));
        tmpSeoUrl.append(".html");
        articleSeoUrl = tmpSeoUrl.toString().toLowerCase();
        //nullify unused objects
        tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
        tmpSeoUrl = null;
      }
      return articleSeoUrl;
    } //constructArticleSeoUrl
   /**
    *
    * URL_PATTERN: www.whatuni.com/degrees/cookies.html
    * EXAMPLE:     www.whatuni.com/degrees/cookies.html
    *
    * @since wu410_20120522
    * @author Sekhar Kasala
    *
    */
   public String getCookiesURL() {
     return "/degrees/cookies.html";
   }
  /**   
   * URL_PATTERN: www.whatuni.com/degrees/mywhatuni.html
   * EXAMPLE:     www.whatuni.com/degrees/mywhatuni.html
   *
   * @since wu420_08012012
   * @author Anbarasan.r   
   */
   public String getMyWhatuniURL() {
     return "/degrees/mywhatuni.html";
   }
   
   public String constructBrowseLocationURL(String pageURL, String location, String filterParam, String searchType){     // Added for search redesign
     String urlArray[] = pageURL.split("/");
     String separator = "/";
     CommonFunction common = new CommonFunction();
     String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); 
     String locationWithPlus = location.replaceAll(" ", "+"); 
     String subjectLocation =   urlArray[3].substring(0, urlArray[3].lastIndexOf("-courses-"))+"-courses-"+locationWithHyphen;
     String newURL = "/degrees/" + urlArray[1]+ separator + urlArray[2]+ separator + subjectLocation + separator +urlArray[4] + separator + locationWithPlus + separator + urlArray[6] + separator + urlArray[7] + searchType;   
     if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
     return newURL;
   }

    public String constructKwdLocationURL(String pageURL, String location, String filterParam, String searchType){     // Added for search redesign
      String urlArray[] = pageURL.split("/");
      String separator = "/";
      CommonFunction common = new CommonFunction();
      String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); 
      String locationWithPlus = location.replaceAll(" ", "+"); 
      String subjectLocation =   urlArray[3].substring(0, urlArray[3].lastIndexOf("-courses-"))+"-courses-"+locationWithHyphen;
      String newURL = "/degrees/" + urlArray[1]+ separator + urlArray[2]+ separator + subjectLocation + separator + urlArray[4] + separator + urlArray[5] + separator +locationWithPlus + separator + locationWithPlus + separator +  urlArray[8] + separator + urlArray[9] + separator ;  
      newURL = newURL + urlArray[10] + separator +  urlArray[11] + separator + urlArray[12] + separator +  urlArray[13] + separator +  urlArray[14] + separator + urlArray[15] + separator + urlArray[16] + separator + urlArray[17] + separator +urlArray[18] + searchType;
      if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
      return newURL;
    }
    
    public String constructStudyModeURLFromBrowse(String pageURL, String studyLevelId, String catCode, String filterParam, String searchType){     // Added for search redesign
      String urlArray[] = pageURL.split("/");
      String separator = "/";
      CommonFunction common = new CommonFunction();      
      String newURL = "/degrees/" + urlArray[1]+ separator + urlArray[2] + separator + urlArray[3] + separator + "0" + separator + urlArray[4] + separator +  urlArray[5]  +"/0/25/0/" ;  
      newURL = newURL + studyLevelId + separator + catCode.toLowerCase() + separator + urlArray[6] + "/0/1/0/uc/0/0" + searchType;
      if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
      return newURL;
    }
    
    public String constructStudyModeURLFromKwd(String pageURL, String studyLevelId, String catCode, String filterParam, String searchType){     // Added for search redesign
      String urlArray[] = pageURL.split("/");
      String separator = "/";
      CommonFunction common = new CommonFunction();      
      String newURL = "/degrees/" + urlArray[1]+ separator + urlArray[2] + separator + urlArray[3] + separator + urlArray[4] + separator + urlArray[5] + separator +  urlArray[6]  + separator +  urlArray[7]  + separator +  urlArray[8] + separator + urlArray[9] + separator ;  
      newURL = newURL + studyLevelId + separator + urlArray[11]  + separator + urlArray[12] + separator +  urlArray[13] + separator +  urlArray[14] + separator + urlArray[15] + separator + urlArray[16] + separator + urlArray[17] + separator +urlArray[18] + searchType;
      if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
      return newURL;
   }
  //Added new method for generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. Start
   public String constructStudyModeURLForSR(HttpServletRequest request, String pageURL, String studyLevelDesc, String qryString, String searchCount, String resultExists, String rf, String watSearch){
     String urlArray[] = pageURL.split("/");
     String separator = "/";
     String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");
     String qryReformatUrl = constructUrlParameters(request, studyLevelDesc, "study-mode", qryString, searchCount, resultExists, rf);
     if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformatUrl; }
     return newURL;
   }
   
  public String constructAssessmentURLForSR(HttpServletRequest request, String pageURL, String assessmentTypeName, String qryString, String searchCount, String resultExists, String rf, String watSearch){
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");
    String qryReformatUrl = constructUrlParameters(request, assessmentTypeName, "assessment-type", qryString, searchCount, resultExists, rf);
    if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformatUrl; }
    return newURL;
  }
  
  public String constructReviewCatURLForSR(HttpServletRequest request, String pageURL, String reviewCatTextKey, String qryString, String searchCount, String resultExists, String rf, String watSearch){
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");
    String qryReformatUrl = constructUrlParameters(request, reviewCatTextKey, "your-pref", qryString, searchCount, resultExists, rf);
    if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformatUrl; }
    return newURL;
  }
  
  public String constructStudyLevelURLForSR(HttpServletRequest request, String pageURL, String studyLevel, String browseCatSubject, String qryString, String searchCount, String resultExists, String rf, String watSearch){
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    String newStudyLevel="";
    CommonFunction common = new CommonFunction();
    studyLevel=common.replaceSlashWithHypen(studyLevel);
    studyLevel=common.replaceSpaceWithHypen(studyLevel);    
    if(!GenericValidator.isBlankOrNull(urlArray[1])){
      newStudyLevel=studyLevel.toLowerCase()+"-courses";
    }
    String newURL = "/" + newStudyLevel + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");
    String qryReformat="";
    if(!GenericValidator.isBlankOrNull(browseCatSubject)){
      qryReformat = constructUrlParameters(request, browseCatSubject, "subject", qryString, searchCount, resultExists, rf);
    }else{
      qryReformat = constructUrlParameters(request, "", "", qryString, searchCount, resultExists, rf);
    }
    if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformat; }
    return newURL;
  }
  
  public String constructSubjectURLForSR(HttpServletRequest request, String pageURL, SimilarStudyLevelCoursesVO subjectVO, String qryString, String searchCount, String resultExists, String rf, String watSearch){
    String urlArray[] = pageURL.split("/");
    String separator = "/";    
    String categoryDesc = subjectVO.getSubjectTextKey();
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");    
    String qryReformatUrl = constructUrlParameters(request, categoryDesc, "subject", qryString, searchCount, resultExists, rf);    
    if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformatUrl; }
    return newURL;
  }
  
  public String constructLocationURLForSR(HttpServletRequest request, String pageURL, String location, String qryString, String searchCount, String resultExists, String rf, String watSearch){
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    CommonFunction common = new CommonFunction();
    String locationWithHyphen = common.replaceURL(common.replaceHypen(location));
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : "");
    String qryReformatUrl = constructUrlParameters(request, locationWithHyphen, "location", qryString, searchCount, resultExists, rf);
    if(!GenericValidator.isBlankOrNull(qryString)){ newURL = newURL + qryReformatUrl; }
    return newURL;
  }
  
  public String constructUrlParameters(HttpServletRequest request, String desc, String genParam, String qryString, String searchCount, String resultExists, String rf){
    String finalQryString="";    
    if(qryString.indexOf("clearing") > -1){finalQryString="?clearing";}
    String q_val = !GenericValidator.isBlankOrNull(request.getParameter("q"))? request.getParameter("q") : "";
    String subject_val=!GenericValidator.isBlankOrNull(request.getParameter("subject"))? request.getParameter("subject") : "";
    String ucas_code_val=!GenericValidator.isBlankOrNull(request.getParameter("ucas-code"))? request.getParameter("ucas-code") : "";
    String jacs_val=!GenericValidator.isBlankOrNull(request.getParameter("jacs"))? request.getParameter("jacs") : "";     
    String university_val = !GenericValidator.isBlankOrNull(request.getParameter("university"))? request.getParameter("university") : "";
    String module_val = !GenericValidator.isBlankOrNull(request.getParameter("module"))? request.getParameter("module") : "";    
    String location_val=!GenericValidator.isBlankOrNull(request.getParameter("location"))? request.getParameter("location") : "";
    String postcode_val=!GenericValidator.isBlankOrNull(request.getParameter("postcode"))? request.getParameter("postcode") : "";
    String distance_val=!GenericValidator.isBlankOrNull(request.getParameter("distance"))? request.getParameter("distance") : "";
    String location_type_val=!GenericValidator.isBlankOrNull(request.getParameter("location-type"))? request.getParameter("location-type") : "";
    String campus_type_val=!GenericValidator.isBlankOrNull(request.getParameter("campus-type"))? request.getParameter("campus-type") : "";
    String study_mode_val=!GenericValidator.isBlankOrNull(request.getParameter("study-mode"))? request.getParameter("study-mode") : "";
    String employment_rate_min_val=!GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min"))? request.getParameter("employment-rate-min") : "";
    String employment_rate_max_val=!GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max"))? request.getParameter("employment-rate-max") : "";
    String ucas_points_min_val=!GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min"))? request.getParameter("ucas-points-min") : "";
    String ucas_points_max_val=!GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max"))? request.getParameter("ucas-points-max") : "";
    String russell_group_val=!GenericValidator.isBlankOrNull(request.getParameter("russell-group"))? request.getParameter("russell-group") : "";
    String entry_level_val=!GenericValidator.isBlankOrNull(request.getParameter("entry-level"))? request.getParameter("entry-level") : "";
    String entry_points_val=!GenericValidator.isBlankOrNull(request.getParameter("entry-points"))? request.getParameter("entry-points") : "";
    String rf_val=!GenericValidator.isBlankOrNull(request.getParameter("rf"))? request.getParameter("rf") : "";
    String sortorder_val=!GenericValidator.isBlankOrNull(request.getParameter("sort"))? request.getParameter("sort") : "";    
    String assessment_val=!GenericValidator.isBlankOrNull(request.getParameter("assessment-type"))? request.getParameter("assessment-type") : "";
    String yourPref_val =!GenericValidator.isBlankOrNull(request.getParameter("your-pref"))? request.getParameter("your-pref") : "";
    String goApplyFlag =  !GenericValidator.isBlankOrNull(request.getParameter("applyflag"))? request.getParameter("applyflag") : "";
    String score =  !GenericValidator.isBlankOrNull(request.getParameter("score"))? request.getParameter("score") : "";
    String acceptance =  !GenericValidator.isBlankOrNull(request.getParameter("acceptance"))? request.getParameter("acceptance") : "";
    String scholarship =  !GenericValidator.isBlankOrNull(request.getParameter("scholarship"))? request.getParameter("scholarship") : "";
    String accommodation =  !GenericValidator.isBlankOrNull(request.getParameter("accommodation"))? request.getParameter("accommodation") : "";

    
    if("q".equals(genParam)){q_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("subject".equals(genParam)){subject_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("study-mode".equals(genParam)){study_mode_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("location-type".equals(genParam)){location_type_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("campus-type".equals(genParam)){campus_type_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("russell-group".equals(genParam)){russell_group_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("location".equals(genParam)){location_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("university".equals(genParam)){university_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("assessment-type".equals(genParam)){assessment_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    if("your-pref".equals(genParam)){yourPref_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
    
    if(!GenericValidator.isBlankOrNull(subject_val)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&subject="+ subject_val : "?subject=" + subject_val;}
    if(!GenericValidator.isBlankOrNull(q_val) && !"subject".equals(genParam)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&q="+q_val : "?q="+ q_val;}
    if(GenericValidator.isBlankOrNull(subject_val) && GenericValidator.isBlankOrNull(q_val) && !GenericValidator.isBlankOrNull(ucas_code_val)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&ucas-code="+ ucas_code_val : "?ucas-code=" + ucas_code_val;}
    if(GenericValidator.isBlankOrNull(subject_val) && GenericValidator.isBlankOrNull(q_val) && GenericValidator.isBlankOrNull(ucas_code_val) && !GenericValidator.isBlankOrNull(jacs_val)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&jacs="+ jacs_val : "?jacs=" + jacs_val;}
    if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") == -1){finalQryString += (finalQryString.indexOf("?") > -1) ? "&university="+ university_val : "?university=" + university_val;}
    if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") > -1){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&university="+ university_val : "?university=" + university_val;}
    if(!GenericValidator.isBlankOrNull(location_val)){finalQryString += "&location=" + location_val;}
    if(!GenericValidator.isBlankOrNull(searchCount) && "N".equals(resultExists)){
      if(!"m".equals(rf)){
        if(!GenericValidator.isBlankOrNull(module_val)){finalQryString += "&module=" + module_val;}  
      }
      if(!"p".equals(rf)){
        if(!GenericValidator.isBlankOrNull(postcode_val)){finalQryString += "&postcode=" + postcode_val;}
        if(!GenericValidator.isBlankOrNull(distance_val)){finalQryString += "&distance=" + distance_val;}
      }
      if(!"g".equals(rf)){
        if(!GenericValidator.isBlankOrNull(entry_level_val)){finalQryString += "&entry-level=" + entry_level_val;}
        if(!GenericValidator.isBlankOrNull(entry_points_val)){finalQryString += "&entry-points=" + entry_points_val;}
      }
      //rf_val="";
    }else if(!GenericValidator.isBlankOrNull(searchCount) && "Y".equals(resultExists)){      
      if(!GenericValidator.isBlankOrNull(module_val)){finalQryString += "&module=" + module_val;}
      if(!GenericValidator.isBlankOrNull(postcode_val)){finalQryString += "&postcode=" + postcode_val;}
      if(!GenericValidator.isBlankOrNull(distance_val)){finalQryString += "&distance=" + distance_val;}      
      if(!GenericValidator.isBlankOrNull(entry_level_val)){finalQryString += "&entry-level=" + entry_level_val;}
      if(!GenericValidator.isBlankOrNull(entry_points_val)){finalQryString += "&entry-points=" + entry_points_val;}      
    }    
    if(!GenericValidator.isBlankOrNull(location_type_val)){finalQryString += "&location-type=" + location_type_val;}
    if(!GenericValidator.isBlankOrNull(campus_type_val)){finalQryString += "&campus-type=" + campus_type_val;}
    if(!GenericValidator.isBlankOrNull(study_mode_val)){finalQryString += "&study-mode=" + study_mode_val;}
    if(!GenericValidator.isBlankOrNull(employment_rate_min_val)){finalQryString += "&employment-rate-min=" + employment_rate_min_val;}
    if(!GenericValidator.isBlankOrNull(employment_rate_max_val)){finalQryString += "&employment-rate-max=" + employment_rate_max_val;}
    if(!GenericValidator.isBlankOrNull(ucas_points_min_val)){finalQryString += "&ucas-points-min=" + ucas_points_min_val;}
    if(!GenericValidator.isBlankOrNull(ucas_points_max_val)){finalQryString += "&ucas-points-max=" + ucas_points_max_val;}
    if(!GenericValidator.isBlankOrNull(russell_group_val)){finalQryString += "&russell-group=" + russell_group_val;}
    
    if(!GenericValidator.isBlankOrNull(rf_val) && (!"N".equalsIgnoreCase(rf_val))){finalQryString += "&rf=" + rf_val;}    
    if(!GenericValidator.isBlankOrNull(sortorder_val)){finalQryString += "&sort=" + sortorder_val;}
    if(!GenericValidator.isBlankOrNull(assessment_val)){finalQryString += "&assessment-type=" + assessment_val;}
    if(!GenericValidator.isBlankOrNull(yourPref_val)){finalQryString += "&your-pref=" + yourPref_val;}
    if(!GenericValidator.isBlankOrNull(goApplyFlag)){ finalQryString += "&applyflag=" + goApplyFlag;} 
    if(!GenericValidator.isBlankOrNull(score)){ finalQryString += "&score=" + score;}  
    if(!GenericValidator.isBlankOrNull(acceptance)){ finalQryString += "&acceptance=" + acceptance;}
    if(!GenericValidator.isBlankOrNull(scholarship)){ finalQryString += "&scholarship=" + scholarship;}
    if(!GenericValidator.isBlankOrNull(accommodation)){ finalQryString += "&accommodation=" + accommodation;}

    return finalQryString;
   }
  //Added new method for generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. End
  
   /**
    * @description Construct keyword URL for roll up search.
    * @since wu557_29092016
    * @author Thiyagu G.
    */
   public String constructRollUpKeywordUrl(HttpServletRequest request, String qryString){
     String finalQryString="";    
     if(qryString.indexOf("clearing") > -1){finalQryString="?clearing";}
     String q_val = !GenericValidator.isBlankOrNull(request.getParameter("subject"))? request.getParameter("subject") : "";     
     String university_val = !GenericValidator.isBlankOrNull(request.getParameter("university"))? request.getParameter("university") : "";     
     if(!GenericValidator.isBlankOrNull(q_val)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&q="+q_val : "?q="+ q_val;}     
     if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") == -1){finalQryString += (finalQryString.indexOf("?") > -1) ? "&university="+ university_val : "?university=" + university_val;}
     if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") > -1){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&university="+ university_val : "?university=" + university_val;}     
     return finalQryString;
    }
    
    /**
     * @description Construct browse URL for roll up search.
     * @since wu557_29092016
     * @author Thiyagu G.
     */
    public String constructRollUpBrowseUrl(HttpServletRequest request, String desc, String genParam, String qryString){
      String finalQryString="";    
      if(qryString.indexOf("clearing") > -1){finalQryString="?clearing";}
      String subject_val=!GenericValidator.isBlankOrNull(request.getParameter("subject"))? request.getParameter("subject") : "";      
      String university_val = !GenericValidator.isBlankOrNull(request.getParameter("university"))? request.getParameter("university") : ""; 
      if("subject".equals(genParam)){subject_val = new CommonFunction().replaceSpaceWithHypen(desc.toLowerCase());}
      if(!GenericValidator.isBlankOrNull(subject_val)){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&subject="+ subject_val : "?subject=" + subject_val;}
      if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") == -1){finalQryString += (finalQryString.indexOf("?") > -1) ? "&university="+ university_val : "?university=" + university_val;}
      if(!GenericValidator.isBlankOrNull(university_val) && finalQryString.indexOf("?clearing") > -1){finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&university="+ university_val : "?university=" + university_val;}      
      return finalQryString;
     }

    public String constructStudyLevelURLFromBrowse(String pageURL, String studyLevel, String studyLevelId, SearchBean searchBean, String catId ,String catname, String filterParam, String searchType){     // Added for search redesign
      String urlArray[] = pageURL.split("/");
      String separator = "/";
      String newURL = "";
      CommonFunction common = new CommonFunction();  
      studyLevel = common.replaceHypen(studyLevel.toLowerCase().replace("/", " "));
      String studyLevelText = (!GenericValidator.isBlankOrNull(studyLevel)? studyLevel.toLowerCase():"") + "-courses";
      String subjectName = catname; //DB_CALL
      if(subjectName!=null && subjectName!=""){
      String location = common.replaceURL(new CommonFunction().replaceHypen(searchBean.getPostCode()));
      String subjectstudyLevelText =  common.replaceHypen(common.replaceURL(subjectName.toLowerCase())) + "-" + studyLevelText +  "-" + location; 
      newURL = "/degrees/courses/" + studyLevelText + separator + subjectstudyLevelText + separator + studyLevelId.toLowerCase() + separator + urlArray[5] + separator + urlArray[6] + separator + catId + searchType;
      if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
      }
      return newURL;
    }    
    
    public String constructStudyLevelURLFromKwd(String pageURL, RefineByOptionsVO studyLevelVO, SearchBean searchBean, String filterParam, String searchType){     // Added for search redesign
      String urlArray[] = pageURL.split("/");
      String separator = "/";
      CommonFunction common = new CommonFunction(); 
      String newURL = "";
      String location = "";
      String catCode = studyLevelVO.getCategoryCode();
      String studyLevel = studyLevelVO.getRefineDesc();
      String studyLevelId = studyLevelVO.getRefineCode();
      String catName = studyLevelVO.getCategoryName();
      String subjectName = searchBean.getCourseSearchText(); 
      if(GenericValidator.isBlankOrNull(subjectName)){
        subjectName= catName;   
        if(!GenericValidator.isBlankOrNull(subjectName)){
          subjectName = common.replaceHypen(common.replaceURL(subjectName.toLowerCase()));
        }
      }
      if(subjectName!=null && subjectName!=""){
      studyLevel = common.replaceHypen(studyLevel.toLowerCase().replace("/", " "));
      String studyLevelText = (!GenericValidator.isBlankOrNull(studyLevel)? studyLevel.toLowerCase():"") + "-courses";      
      location = !GenericValidator.isBlankOrNull(searchBean.getRegionId()) ? searchBean.getRegionId().toLowerCase(): "" ;
      String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); 
      String locationWithPlus = location.replaceAll(" ", "+"); 
      String subjectstudyLevelText =  subjectName + "-" + studyLevelText +  "-" + locationWithHyphen; 
      if(!GenericValidator.isBlankOrNull(catCode)){
        newURL = "/degrees/courses/" + studyLevelText + separator + subjectstudyLevelText + separator + "0" + separator + studyLevelId.toLowerCase() + separator + locationWithPlus + separator + locationWithPlus + separator + urlArray[8] + separator + urlArray[9] + separator ;  
        newURL = newURL + urlArray[10] + separator + catCode.toLowerCase()  + separator + urlArray[12] + separator +  urlArray[13] + separator +  urlArray[14] + separator + urlArray[15] + separator + urlArray[16] + separator + urlArray[17] + separator +urlArray[18] + searchType;
      }else{
        newURL = "/degrees/courses/" + studyLevelText + separator + subjectstudyLevelText + separator + subjectName + separator + studyLevelId.toLowerCase() + separator + locationWithPlus + separator + locationWithPlus + separator + urlArray[8] + separator + urlArray[9] + separator ;  
        newURL = newURL + urlArray[10] + separator + "0" + separator + urlArray[12] + separator +  urlArray[13] + separator +  urlArray[14] + separator + urlArray[15] + separator + urlArray[16] + separator + urlArray[17] + separator +urlArray[18] + searchType;
      }
      if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
      }
      return newURL;
    }

  public String constructBrowseSubjectURL(String pageURL, String location, String filterParam, String searchType){     // Added for search redesign
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    CommonFunction common = new CommonFunction();
    location =GenericValidator.isBlankOrNull(location)? "united kingdom"  : location.toLowerCase();
    String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); 
    String locationWithPlus = location.replaceAll(" ", "+"); 
    String subjectLocation =   urlArray[4].substring(0, urlArray[4].lastIndexOf("-courses-"))+"-courses-"+locationWithHyphen;
    String newURL =  separator + urlArray[1]+ separator + urlArray[2]+ separator + urlArray[3]+ separator + subjectLocation + separator +urlArray[5] + separator + locationWithPlus + separator + urlArray[7] + separator + urlArray[8] + searchType;   
    if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
    return newURL;
  }
  //
   public String constructKeywordSubjectURL(String pageURL, SimilarStudyLevelCoursesVO subjectVO, String location, String filterParam, String searchType){     // Added for search redesign
     String urlArray[] = pageURL.split("/");
     String separator = "/";
     CommonFunction common = new CommonFunction();
     location = GenericValidator.isBlankOrNull(location)? "united kingdom"  : location.toLowerCase();
     String locationWithHyphen = common.replaceURL(common.replaceHypen(location)); 
     String categoryDesc = common.replaceHypen(common.replaceURL(subjectVO.getCategoryDesc()));
     categoryDesc = categoryDesc.replaceAll("All-", "");     
     String subjectLocation =  categoryDesc + "-" + urlArray[2] + "-" + locationWithHyphen;
     String newURL =  "/degrees" + separator + urlArray[1] + separator + urlArray[2]+ separator + subjectLocation + separator + urlArray[4] + separator + urlArray[5] + separator + urlArray[6] + separator + urlArray[7] + separator + urlArray[8] + separator + urlArray[9] ;
     newURL = newURL + separator + urlArray[10] + separator + subjectVO.getCategoryCode()  + separator + urlArray[12] + separator +  urlArray[13] + separator +  urlArray[14] + separator + urlArray[15] + separator + urlArray[16] + separator + urlArray[17] + separator +urlArray[18] +  searchType;  
     newURL = newURL.toLowerCase();
     if(!GenericValidator.isBlankOrNull(filterParam)){ newURL = newURL + "?nd" + filterParam; }
     return newURL;
   } 
   
   public String SearchURLfromProviderResults(String subjectName, String subjectCode, String location, String qualification, String qualCode, String searchType){ //24_JUN_2014
    String separator = "/";
    CommonFunction common = new CommonFunction();
    qualification = common.replaceHypen(qualification.toLowerCase().replace("/", " "));
    String qualText = (!GenericValidator.isBlankOrNull(qualification)? qualification.toLowerCase():"") + "-courses";     
    String locText =  common.replaceHypen(common.replaceURL(location.toLowerCase()));
    String locPlusText = location.toLowerCase();
    locPlusText = locPlusText.replaceAll(" ", "+");
    String newURL = "";
    String sname = common.replaceURL(subjectName.toLowerCase());
    String subjectText = common.replaceHypen(sname) +  "-" + qualText + "-" + locText ;
    String forkwd = sname.replaceAll(" ","+");
    if(!GenericValidator.isBlankOrNull(subjectCode)){   
      newURL =  "/degrees" + separator + "courses" + separator + qualText + separator + subjectText + separator + "0"+ separator + qualCode.toLowerCase() + separator + locPlusText +  "/0/25/0/0";
      newURL = newURL + separator + subjectCode +  "/r/0/1/0/uc/0/0" + searchType;  //24_JUN_2014
    } else{
      newURL =  "/degrees" + separator + "courses" + separator + qualText + separator + subjectText + separator + forkwd + separator + qualCode.toLowerCase() + separator + locPlusText + separator + locPlusText + separator + "25" + separator + "0/0";
      newURL = newURL +   "/0/r/0/1/0/uc/0/0" + searchType;  //24_JUN_2014
    }
    return newURL;
  }
  
    /**
     * will give link to reviews page          
     * @author Thiyagu G
     * @since wu333_20141209     
     */
    public String constructReviewPageSeoUrl(String uniName, String uniId) {
      String reviewsSeoUrl = "";
      if ((uniName != null && uniName.trim().length() > 0) && (uniId != null && uniId.trim().length() > 0) ) {
        StringBuffer tmpSeoUrl = new StringBuffer();
        tmpSeoUrl.append(PASS_THROUGH_CONTEXT_PATH);
        //tmpSeoUrl.append("/degrees/university-course-reviews/");
        tmpSeoUrl.append("/university-course-reviews/");
        tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(uniName)));
        tmpSeoUrl.append("/");
        tmpSeoUrl.append(new CommonUtil().replaceSpaceByHypen(uniId));        
        //tmpSeoUrl.append(".html");
        tmpSeoUrl.append("/");
        
        reviewsSeoUrl = tmpSeoUrl.toString().toLowerCase();
        //nullify unused objects
        tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
        tmpSeoUrl = null;
      }
      return reviewsSeoUrl;
    } 
  
  /**
   * URL formation for UG and PG redirection
   * @author Indumathi.S
   * @since wu548_20160127
   */
  public String constructCourseUrl(String qualification, String profileType, String collegeName) {
    String courseUrl = "";
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/");
    tmpSeoUrl.append(qualification);
    tmpSeoUrl.append("-courses");
    tmpSeoUrl.append("/");
    tmpSeoUrl.append("csearch?"); //
    if (!GenericValidator.isBlankOrNull(profileType) && "CLEARING".equals(profileType)) {
      tmpSeoUrl.append("clearing&");
    }
    tmpSeoUrl.append("university=");
    tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(collegeName)));
    courseUrl = tmpSeoUrl.toString().toLowerCase();
    return courseUrl;
  }
  
  /**
   * @constructCourseUrl method to forming the PR page url with subject and location fillter
   * @param qualification
   * @param profileType
   * @param collegeName
   * @param subject
   * @param location
   * @return PR page URL
   * 
   */
  public String constructCourseUrl(String qualificationCode, String profileType, String collegeName, String subject, String location) {
    String courseUrl = "";
	StringBuffer tmpSeoUrl = new StringBuffer();
	tmpSeoUrl.append("/");
	//System.out.println("getQualificationDesc(qualificationCode)-->" + getQualificationDesc(qualificationCode));
	tmpSeoUrl.append(getQualificationDesc(qualificationCode));
	tmpSeoUrl.append("-courses");
	tmpSeoUrl.append("/");
	tmpSeoUrl.append("csearch?"); //
	if (!GenericValidator.isBlankOrNull(profileType) && "CLEARING".equalsIgnoreCase(profileType)) {
	  tmpSeoUrl.append("clearing&");
	}
	if(!GenericValidator.isBlankOrNull(subject)) {
	  tmpSeoUrl.append("subject=");
	  tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subject)));}
	  tmpSeoUrl.append((!GenericValidator.isBlankOrNull(subject) ? "&" : "") + "university=");
	  tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(collegeName)));
	if(!GenericValidator.isBlankOrNull(location)) {
	  tmpSeoUrl.append("&location=");
	  tmpSeoUrl.append(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(location)));
	}
	courseUrl = tmpSeoUrl.toString().toLowerCase();
	return courseUrl;
  }
  /**
   * URL formation for iwanttobe 
   * @author Indumathi.S
   * @since wu549_20160216
   */
  public String constructIWantToBeSearchUrl(String jacsCode) {
    String searchUrl = "";
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/degree-courses/search?"); 
    tmpSeoUrl.append("jacs=" + jacsCode);
    searchUrl = tmpSeoUrl.toString().toLowerCase();
    return searchUrl;
  }
  /**
   * I Want to be Widget view degrees link (Page 4)
   * @param jacsCode
   * @param uniName
   * @return searchUrl as URL
   */
  public String constructIWantToBeUniSearchUrl(String jacsCode, String uniName) {
    String searchUrl = "";
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/degree-courses/csearch?"); 
    tmpSeoUrl.append("jacs=" + jacsCode);
    tmpSeoUrl.append("&");
    tmpSeoUrl.append("university=");
    tmpSeoUrl.append(uniName);
    searchUrl = tmpSeoUrl.toString().toLowerCase();
    return searchUrl;
  }
  /**
   * Method to build jacsProvider url in what course i do widget
   * @param jacsCode
   * @param uniName
   * @param entryLevel
   * @param entryPoints
   * @return searchUrl as String
   */
  public String getJacsProviderURL(String jacsCode, String uniName, String entryLevel, String entryPoints){
    String searchUrl = "";
    String entryLevelStr = "ucas_points";
    if("AL".equalsIgnoreCase(entryLevel)){
      entryLevelStr = "a_level";
    }else if("H".equalsIgnoreCase(entryLevel)){
      entryLevelStr = "sqa_higher";
    }else if("AH".equalsIgnoreCase(entryLevel)){
      entryLevelStr = "sqa_adv";
    }else if("BTEC".equalsIgnoreCase(entryLevel)){
      entryLevelStr = "btec";
    }
    StringBuffer tmpSeoUrl = new StringBuffer();
    tmpSeoUrl.append("/degree-courses/csearch?"); 
    tmpSeoUrl.append("jacs=" + jacsCode);
    tmpSeoUrl.append("&university=");
    tmpSeoUrl.append(uniName);
    tmpSeoUrl.append("&entry-level=");
    tmpSeoUrl.append(entryLevelStr);
    tmpSeoUrl.append("&entry-points=");
    tmpSeoUrl.append(entryPoints);
    searchUrl = tmpSeoUrl.toString().toLowerCase();
    return searchUrl;
  }
  //
   /**
      *  method to build login page URL
      *
      * @author   Sabapathi
      * @since    04_Oct_2016 - intial draft
      * @version  1.0
      *
      */
  public String getWhatuniLoginURL() {
    return GlobalConstants.WU_CONTEXT_PATH+"/login.html";
  }
  //
  
   /**
      *  method to build login page URL
      *
      * @author  Jeyalakshmi 
      * @since    18_Dec_2018 - intial draft
      * @version  1.0
      *
      */
   public String courseDetailsPageURL(String courseTitle, String courseId, String collegeId) {
        CommonValidator validate = new CommonValidator();
        if (validate.isBlankOrNull(courseTitle) || validate.isBlankOrNull(courseId) || validate.isBlankOrNull(collegeId)) {
          validate = null;
          return getWhatuniHomeURL();
        }
        StringBuffer courseDetailsURL = new StringBuffer();
        CommonFunction comFn = new CommonFunction();
        if(courseTitle.indexOf("/") != -1){
            courseTitle = courseTitle.replaceAll("/", " ");
        }
        String collegeName = null;
        collegeName = comFn.getCollegeTitleFunction(collegeId);
        collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
        courseTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(courseTitle)));
        //
        courseDetailsURL.append("/degrees/");
        courseDetailsURL.append(courseTitle);
        courseDetailsURL.append("/");
        courseDetailsURL.append(collegeName);
        courseDetailsURL.append("/cd/");
        courseDetailsURL.append(courseId);
        courseDetailsURL.append("/");
        courseDetailsURL.append(collegeId);
courseDetailsURL.append("/");
        validate = null;
        comFn = null;
        return courseDetailsURL.toString().toLowerCase();
      }
   /**
    *  method to build Apply now url for whatunigo from cd page
    *
    * @author  Jeyalakshmi 
    * @since    22_04_2019 - intial draft
    * @version  1.0
    *
    */
    
  public String applyNowWugoURL(String courseId, String opportunityId) {
       if (GenericValidator.isBlankOrNull(courseId) || GenericValidator.isBlankOrNull(opportunityId)) {
         return getWhatuniHomeURL();
       }
       StringBuffer applyNowURL = new StringBuffer();
       //
       applyNowURL.append(GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH + "/wugo/apply-now.html?");
       //applyNowURL.append(GlobalConstants.WU_CONTEXT_PATH+"/wugo/apply-now.html?");
       applyNowURL.append("courseId=" + courseId);
       applyNowURL.append("&opportunityId=" + opportunityId);
      // applyNowURL.append("/");
       return applyNowURL.toString();
 }
 public String provisionalJourneyURL(String courseId, String opportunityId, String actionParam){
   String url = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH;
   //String url = GlobalConstants.WU_CONTEXT_PATH;
   
   url += "/clearing-match-maker-tool.html?";
   url += "action-param=" + actionParam;
   
   if (GenericValidator.isBlankOrNull(courseId) || GenericValidator.isBlankOrNull(opportunityId)) {
     return getWhatuniHomeURL();
   }
   StringBuffer provJouneyURL = new StringBuffer();
   provJouneyURL.append(url + "&action=prov_qual");
   
   provJouneyURL.append("&courseId=" + courseId);
   provJouneyURL.append("&opportunityId=" + opportunityId);
   return provJouneyURL.toString();
 }
 
 public String offerJourneyURL(String courseId, String opportunityId, String actionParam){
   String url = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH;
   //String url = GlobalConstants.WU_CONTEXT_PATH;
   url += "/clearing-match-maker-tool.html?";
   url += "action-param=" + actionParam;
   
   if (GenericValidator.isBlankOrNull(courseId) || GenericValidator.isBlankOrNull(opportunityId)) {
     return getWhatuniHomeURL();
   }
   StringBuffer provJouneyURL = new StringBuffer();    
   provJouneyURL.append(url);
   provJouneyURL.append("&courseId=" + courseId);
   provJouneyURL.append("&opportunityId=" + opportunityId);
   return provJouneyURL.toString();
 }
 
   /**
      *  method to build profile page url for whatunigo from application page
      *
      * @author  Hema 
      * @since    13_05_2019 - intial draft
      * @version  1.0
      *
      */
      
   public String construnctProfileURL(String collegeId, String collegeName,String pagename) {
     if (GenericValidator.isBlankOrNull(collegeName) || GenericValidator.isBlankOrNull(collegeId)) {
       return getWhatuniHomeURL();
     }
     StringBuffer uniHomeURL = new StringBuffer();
     CommonFunction comFn = new CommonFunction();
     collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
     uniHomeURL.append("/university-profile/"); 
     uniHomeURL.append(collegeName);
     uniHomeURL.append("/");
     uniHomeURL.append(collegeId);
     uniHomeURL.append("/");   
     if("CLEARING".equalsIgnoreCase(pagename) || "CLEARING_COURSE".equalsIgnoreCase(pagename)){
       uniHomeURL.append("clearing/");    
     }
     return uniHomeURL.toString().toLowerCase();
   }
   
   /**
    * This method get the qualification code and returns the qualification description 
    * 
    * @author   Sujitha V 
    * @since    04_02_2020 - intial draft
    * @version  1.0
    */
   
   public String getQualificationDesc(String qualificationCode) {
	 String qualificationDesc = "all";
	 //qualificationCode = qualificationCode.toUpperCase();
	 //System.out.println("qualificationCode-->" + qualificationCode);
	 if("M".equalsIgnoreCase(qualificationCode)){
	   qualificationDesc = "degree";
	 }else if("N".equalsIgnoreCase(qualificationCode)){
	   qualificationDesc = "hnd-hnc";
	 }else if("A".equalsIgnoreCase(qualificationCode)){
	   qualificationDesc = "foundation-degree";
     }else if("T".equalsIgnoreCase(qualificationCode)) {
	   qualificationDesc = "access-foundation";
	 }else if("L".equalsIgnoreCase(qualificationCode)) {
	   qualificationDesc = "postgraduate";
     }
	 return qualificationDesc;
   }
}