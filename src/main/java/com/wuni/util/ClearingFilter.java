package com.wuni.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
* Class         : ClearingFilter.java
*
* Description   :
*
* @version      : 1.0
*
* Change Log :
* **************************************************************************************************************
* author	  Ver 	   Created On      Modified On 	    Modification Details 	Change
* **************************************************************************************************************
* Priyaa Parthasarathy   1.0       28-07-2014        First draft           10_DEC_2013
*
*/
public class ClearingFilter implements Filter {
  private FilterConfig filterConfig = null;
  public ClearingFilter() {
  }
  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, 
                                                                                                         ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;    
    filterChain.doFilter(request, response);      
  }
  public void destroy() {
    this.filterConfig = null;
  }
  public static boolean checkUnicode(String str) {
    char data[] = str.toCharArray();
    for (int i = 0; i < data.length; i++) {
      int x = data[i];
      if (32 < x && x > 127) {
        return true;
      }
    }
    return false;
  }
}
