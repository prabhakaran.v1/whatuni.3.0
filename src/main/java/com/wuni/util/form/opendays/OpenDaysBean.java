package com.wuni.util.form.opendays;

import org.apache.struts.action.ActionForm;

public class OpenDaysBean {
  private String collegeId = null;
  private String userId = null;
  private String basketId = null;
  private String networkId = null;
  private String openDayMnthYear = null;
  private String pageNo = null;
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setOpenDayMnthYear(String openDayMnthYear) {
    this.openDayMnthYear = openDayMnthYear;
  }
  public String getOpenDayMnthYear() {
    return openDayMnthYear;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
}
