package com.wuni.util.form.studentawards;

import org.apache.struts.action.ActionForm;

public class StudentAwardsBean {
  String collegeId = null;
  String collegeName = null;
  String uniHomeURL = null;
  String collegeDisplayName = null;
  String questionId = null;
  String questionTitle = null;
  String selCategoryId = null;
  String rating = null;
  String sponsorBy = null;
  String sponsorURL = null;
  String displaySeq = null;
  String displayTitle = null;
  String pageNo = null;
  String curYear = null;
  String lastYear = null;
  String rankStatus = null;
  String collegeLogo = null;
  String orderItemId = null;
  String subOrderEmail = null;
  String subOrderEmailWebform = null;
  String subOrderProspectus = null;
  String subOrderProspectusWebform = null;
  String subOrderItemId = null;
  String myhcProfileId = null;
  String profileType = null;
  String networkId = null;
  String hotline = null;
  String mediaId = null;
  String mediaPath = null;
  String mediaThumbPath = null;
  String mediaType = null;
  String advertName = null;
  String qlFlag = null;
  String subOrderWebsite = null;
  String curYearSubScript = null;
  String lastYearSubScript = null;
  String ratingPercent = null;
  String currentYear = null;
  //
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }
  public String getQuestionId() {
    return questionId;
  }
  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }
  public String getQuestionTitle() {
    return questionTitle;
  }
  public void setRating(String rating) {
    this.rating = rating;
  }
  public String getRating() {
    return rating;
  }
  public void setSponsorBy(String sponsorBy) {
    this.sponsorBy = sponsorBy;
  }
  public String getSponsorBy() {
    return sponsorBy;
  }
  public void setSponsorURL(String sponsorURL) {
    this.sponsorURL = sponsorURL;
  }
  public String getSponsorURL() {
    return sponsorURL;
  }
  public void setDisplaySeq(String displaySeq) {
    this.displaySeq = displaySeq;
  }
  public String getDisplaySeq() {
    return displaySeq;
  }
  public void setDisplayTitle(String displayTitle) {
    this.displayTitle = displayTitle;
  }
  public String getDisplayTitle() {
    return displayTitle;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setCurYear(String curYear) {
    this.curYear = curYear;
  }
  public String getCurYear() {
    return curYear;
  }
  public void setLastYear(String lastYear) {
    this.lastYear = lastYear;
  }
  public String getLastYear() {
    return lastYear;
  }
  public void setRankStatus(String rankStatus) {
    this.rankStatus = rankStatus;
  }
  public String getRankStatus() {
    return rankStatus;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setHotline(String hotline) {
    this.hotline = hotline;
  }
  public String getHotline() {
    return hotline;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }
  public String getMediaThumbPath() {
    return mediaThumbPath;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }
  public String getAdvertName() {
    return advertName;
  }
  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }
  public String getQlFlag() {
    return qlFlag;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSelCategoryId(String selCategoryId) {
    this.selCategoryId = selCategoryId;
  }
  public String getSelCategoryId() {
    return selCategoryId;
  }
  public void setCurYearSubScript(String curYearSubScript) {
    this.curYearSubScript = curYearSubScript;
  }
  public String getCurYearSubScript() {
    return curYearSubScript;
  }
  public void setLastYearSubScript(String lastYearSubScript) {
    this.lastYearSubScript = lastYearSubScript;
  }
  public String getLastYearSubScript() {
    return lastYearSubScript;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setRatingPercent(String ratingPercent) {
    this.ratingPercent = ratingPercent;
  }
  public String getRatingPercent() {
    return ratingPercent;
  }

  public void setCurrentYear(String currentYear) {
    this.currentYear = currentYear;
  }

  public String getCurrentYear() {
    return currentYear;
  }

}
