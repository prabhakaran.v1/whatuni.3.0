package com.wuni.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SearchUtilities;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

/**
 * Class         : SearchURLRewriteFilter.java
 * Description   :
 * @version      : 1.0
 * Change Log :
 * **************************************************************************************************************
 * author	       Ver 	     Created On       Modified On 	       Modification Details 	Change
 * **************************************************************************************************************
 * Indumathi.S   1.0       27-01-2016       First draft
 * Thiyagu G     1.1       27_Jan_2016      Get the PG university new subject to form the new URL pettern.
 *
 */
public class SearchURLRewriteFilter implements Filter {

  private FilterConfig filterConfig = null;

  public SearchURLRewriteFilter() {
  }

  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    String urlString = req.getRequestURI();
    String str1 = URLDecoder.decode(urlString, "UTF-8");
    String redirectURL = null;
    GlobalFunction globalFn = new GlobalFunction();
    CommonFunction common = new CommonFunction();
    String subject_val = "";
    String university = "";
    String studyLevel = "";
    String studyLevelId = "";
    String studyModeId = "";
    String location = "";
    String entryLevel = "";
    String entryLevelDesc = "";
    String entryPoints = "";    
    String ucasCode = "";
    String studyMode = "";
    String finalQryString = "";
    String[] urlArray = null;    
    if (urlString != null) {
      urlString = urlString.substring(8);
      urlArray = urlString.split("/");
    }
    boolean notFoundPage = false;    
    String p_jacsCode = GenericValidator.isBlankOrNull(request.getParameter("jacs")) ? null : (request.getParameter("jacs"));
    if (urlString != null && (!urlString.contains("/herb/csearch.html")) && urlString.contains("/csearch.html") && urlArray.length != 3) {
      //To get the PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G. Start
      if (urlString != null && urlString.contains("/postgraduate-university/")) {
        ///courses/postgraduate-university/business-courses-(.*)/(.*)/(.*)/(.*)/(.*)/(.*)/(.*)/(.*)/csearch.html
        String postgraduateUrl = req.getRequestURI();
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);       
        Map inputMap = commonBusiness.getPGUniversityNewUrl(postgraduateUrl);
        if (inputMap != null) {
          String pgNewUrlDetails = (String)inputMap.get("pg_new_url_details");
          if (!GenericValidator.isBlankOrNull(pgNewUrlDetails)) {
            redirectURL = pgNewUrlDetails.toLowerCase();
          }
        }
        //To get the PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G. Start
      }else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Comes from Search / Money pages and Browse Money Page
         * http://192.168.1.167:8989/degrees/courses/Degree-university/Accountancy-courses-at-University-Of-Kent/AK.6/M/0/england/3793/a-level/3a*-1a-0b-0c-0d-2e/csearch.html
         *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        if (urlString != null && urlArray != null && urlArray.length == 12) {
          //
          if (!GenericValidator.isBlankOrNull(urlArray[3]) && urlArray[3].contains("-courses-at-")) {
            subject_val = urlArray[3].substring(0, urlArray[3].indexOf("-courses-at-"));
            university = urlArray[3].substring(urlArray[3].indexOf("-courses-at-"), urlArray[3].length());
            university = university.replace("-courses-at-", "");
          }
          studyLevel = urlArray[2].substring(0, urlArray[2].indexOf("-university"));
          String ucasflag = globalFn.checkValidUCASCode(common.replacePlus(urlArray[4]));
          String commonCagtegoryName = globalFn.getCategoryDescFromBrowseCategories(urlArray[4], urlArray[5]);
          if (commonCagtegoryName != null && !commonCagtegoryName.equalsIgnoreCase("null") && commonCagtegoryName.trim().length() > 0) {
            if (GenericValidator.isBlankOrNull(p_jacsCode)) {
              String nodeExistsFlag = new SearchUtilities().checkDuplicateNode("", urlArray[5], urlArray[4]);
              if (GenericValidator.isBlankOrNull(nodeExistsFlag)) {
                notFoundPage = true;
              }
            }
          } else if (ucasflag != null && ucasflag.equalsIgnoreCase("TRUE")) {
            ucasCode = common.replacePlus(urlArray[4]);
          }
          studyLevelId = checkAndGetVal(urlArray[5]);
          studyModeId = checkAndGetVal(urlArray[6]);
          location = checkAndGetVal(urlArray[7]);
          entryLevel = checkAndGetVal(urlArray[8]);
          entryPoints = checkAndGetVal(urlArray[9]);
          studyMode = getStudyMode(studyModeId);
          if (!GenericValidator.isBlankOrNull(entryLevel)) {          
            entryLevelDesc = getEntryLevel(entryLevel);
          }
        }
        //
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Comes from View all courses link
         * http://www.whatuni.com/degrees/courses/all/all-courses-at-university-of-plymouth/3758/csearch.html
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        if (urlString != null && urlArray != null && urlArray.length == 10) {
          studyLevel = "all";
          if (!GenericValidator.isBlankOrNull(urlArray[2]) && urlArray[2].contains("-courses-at-")) {
            subject_val = urlArray[2].substring(0, urlArray[2].indexOf("-courses-at-"));
            university = urlArray[2].substring(urlArray[2].indexOf("-courses-at-"), urlArray[2].length());
            university = university.replace("-courses-at-", "");
          }
          String ucasflag = globalFn.checkValidUCASCode(common.replacePlus(urlArray[3]));
          if (ucasflag != null && ucasflag.equalsIgnoreCase("TRUE")) {
            ucasCode = common.replacePlus(urlArray[3]);
          }
          studyModeId = checkAndGetVal(urlArray[4]);
          location = checkAndGetVal(urlArray[5]);
          entryLevel = checkAndGetVal(urlArray[6]);
          entryPoints = checkAndGetVal(urlArray[7]);
          studyMode = getStudyMode(studyModeId);
          if (!GenericValidator.isBlankOrNull(entryLevel)) {          
            entryLevelDesc = getEntryLevel(entryLevel);
          }
        }
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Comes from View all courses link
         * http://www.whatuni.com/degrees/courses/all/all-courses-at-university-of-plymouth/3758/csearch.html
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        if (urlString != null && urlArray != null && urlArray.length == 6) {
          studyLevel = "all";
          if (!GenericValidator.isBlankOrNull(urlArray[3]) && urlArray[3].contains("-courses-at-")) {
            subject_val = urlArray[3].substring(0, urlArray[3].indexOf("-courses-at-"));
            university = urlArray[3].substring(urlArray[3].indexOf("-courses-at-"), urlArray[3].length());
            university = university.replace("-courses-at-", "");
          }
        }
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Comes from Vew all Postgraduate/Undergraduate courses Link
         * http://www.whatuni.com/degrees/courses/postgraduate-university/all-postgraduate-courses-at-university-of-plymouth/l/3758/csearch.html
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        if (urlString != null && urlArray != null && urlArray.length == 7) {
          studyLevel = urlArray[2].substring(0, urlArray[2].indexOf("-university"));
          if (!GenericValidator.isBlankOrNull(urlArray[3]) && urlArray[3].contains("-courses-at-")) {
            subject_val = urlArray[3].substring(0, urlArray[3].indexOf("-" + studyLevel + "-courses-at-"));
            university = urlArray[3].substring(urlArray[3].indexOf("-courses-at-"), urlArray[3].length());
            university = university.replace("-courses-at-", "");
          }
          studyLevelId = checkAndGetVal(urlArray[4]);
        }
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * When Click the page nation and URL will contains header-id
         * http://www.whatuni.com/degrees/courses/postgraduate-university/all-postgraduate-courses-at-university-of-plymouth-pg2/246406914/2/3758/csearch.html
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        
        if (urlString != null && urlArray != null && urlArray.length == 8) {
          notFoundPage = true;
        }
        if (!GenericValidator.isBlankOrNull(subject_val) && !"all".equals(subject_val)) {
          finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&subject=" + subject_val : "?subject=" + subject_val;
        }
        if (GenericValidator.isBlankOrNull(subject_val) && !GenericValidator.isBlankOrNull(ucasCode)) {
          finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&ucas-code=" + ucasCode : "?ucas-code=" + ucasCode;
        }
        if (!GenericValidator.isBlankOrNull(university)) {
          finalQryString += (finalQryString.indexOf("?clearing") > -1 || finalQryString.indexOf("subject=") > -1 || finalQryString.indexOf("ucas-code=") > -1) ? "&university=" + university : "?university=" + university;        
        }
        if (!GenericValidator.isBlankOrNull(location) && !"united+kingdom".equalsIgnoreCase(location)) {
          finalQryString += "&location=" + location;
        }
        if (!GenericValidator.isBlankOrNull(studyMode)) {
          finalQryString += "&study-mode=" + studyMode;
        }
        if (!GenericValidator.isBlankOrNull(entryLevelDesc)) {
          finalQryString += "&entry-level=" + entryLevelDesc;
        }
        if (!GenericValidator.isBlankOrNull(entryPoints)) {
          finalQryString += "&entry-points=" + entryPoints;
        } 
        if (!GenericValidator.isBlankOrNull(finalQryString) && !notFoundPage) {
          finalQryString = "/" + studyLevel + "-courses/csearch" + finalQryString.toLowerCase();
        } else if (notFoundPage) {
          finalQryString = "/degrees/notfound.html";
        }
        redirectURL = finalQryString.toLowerCase();
      }             
      res.setStatus(res.SC_MOVED_PERMANENTLY);
      res.setHeader("Location", redirectURL);
      res.setHeader("Connection", "close");
      return;
    }else {
      filterChain.doFilter(request, response);
    }
  }

  public void destroy() {
    this.filterConfig = null;
  }

  private String getEntryLevel(String inValue) {
    String entryLevel = "";
    if ("A_LEVEL".equalsIgnoreCase(inValue)) { //a_level
      entryLevel = "a-level";
    } else if ("SQA_HIGHER".equalsIgnoreCase(inValue)) { //sqa_higher
      entryLevel = "sqa-higher";
    } else if ("SQA_ADV".equalsIgnoreCase(inValue)) { //sqa_adv
      entryLevel = "sqa-adv";
    } else if ("BTEC".equalsIgnoreCase(inValue)) { //btec
      entryLevel = "btec";
    } else if ("ucas_points".equalsIgnoreCase(inValue)) { //ucas_points
      entryLevel = "ucas-points";
    }
    return entryLevel;
  }

  private String getStudyMode(String inValue) {
    String studyMode = "";
    if ("b,b1,b2,b3,b4,b5,b6,c,c1,c2,c3,c4,c5,c6".equalsIgnoreCase(inValue)) {
      studyMode = "distance-or-online";
    } else if ("a1".equalsIgnoreCase(inValue)) {
      studyMode = "full-time";
    } else if ("a6".equalsIgnoreCase(inValue)) {
      studyMode = "sandwich";
    } else if ("a2,a21,a22,a3,a23,a4,a5".equalsIgnoreCase(inValue)) {
      studyMode = "part-time";
    }
    return studyMode;
  }

  private String checkAndGetVal(String inValue) {
    if (inValue != null && !inValue.trim().equals("0")) {
      return inValue.trim();
    }
    return null;
  }

}
