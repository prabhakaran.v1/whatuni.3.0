package com.wuni.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.CommonVO;
import WUI.utilities.GlobalConstants;
import spring.util.GlobalMethods;

/**
* Class         : UrlRewriteFilter.java
*
* Description   : 
*
* @version      : 1.0
*
* Change Log :
* *********************************************************************************************************************************
* author	              Ver 	    Created On        Modified On 	  Modification Details 	               Change
* *********************************************************************************************************************************
* Priyaa Parthasarathy    1.0       28-11-2013        First draft         10_DEC_2013
* Sujitha V               2.0                         Second draft        02_JUNE_2020         Added 301 Redirect changes for advice page urls.
*
*/
@WebFilter(urlPatterns = {"*.html", "/advice/*"})
public class URLRewriteFilter implements Filter {
  private FilterConfig filterConfig = null;
  public URLRewriteFilter() {
  }
  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, 
                                                                                                         ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
  //  boolean flag = false;
    String url =  req.getRequestURI();
    String str1 = URLDecoder.decode(url, "UTF-8");
   // flag = checkUnicode(str1);
    String redirectURL = null;
    //if (flag) {
      if (url.contains("/study.html")) {
        String rank_split[]=url.split("/");
        String categoryCode = rank_split[4];
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);       
        Map inputMap = commonBusiness.getCategorydetails(categoryCode); 
        ArrayList list = (ArrayList)inputMap.get("lc_browse_details");
        Iterator itr = list.iterator();
        String catName = "";
        String catId = "";
        while(itr.hasNext()){
         CommonVO commonVO = (CommonVO)itr.next();
          catId = commonVO.getCategoryCode();
          catName = commonVO.getCategoryName();
        }if(!GenericValidator.isBlankOrNull(catId) && !GenericValidator.isBlankOrNull(catName)){
           redirectURL = "/degrees/courses/Degree-list/" + catName + "-Degree-courses-UK/qualification/M/search_category/" + catId +"/loc.html";  
        }else{
           redirectURL = "/degrees/home.html";
        }
        res.setStatus(res.SC_MOVED_PERMANENTLY);
        res.setHeader("Location", redirectURL);
        res.setHeader("Connection", "close");
        return;
      }else if(url.contains("/advice/")) {
    	  Map redirectedURLMap = new HashMap();
    	  redirectedURLMap = GlobalMethods.getRedirectURLSFromExcel();
    	  String newUrl = StringUtils.replaceEach(url, new String[]{GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.SLASH, SpringConstants.DOT_HTML}, new String[]{GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.SLASH,"/"});
    	  redirectURL = (String) redirectedURLMap.get(newUrl);
    	  //System.out.println("redirectURL-->"+redirectURL);
    	
        if(StringUtils.isNotBlank(redirectURL)) {
          //System.out.println("131-->"+redirectURL);
          res.setStatus(res.SC_MOVED_PERMANENTLY);
          res.setHeader("Location", redirectURL);
          res.setHeader("Connection", "close");
          return;
        }else {
          filterChain.doFilter(request, response);
        }
      } else {
        filterChain.doFilter(request, response);
      }
  /*  } else {
      filterChain.doFilter(request, response);
    }*/
  }
  public void destroy() {
    this.filterConfig = null;
  }
  public static boolean checkUnicode(String str) {
    char data[] = str.toCharArray();
    for (int i = 0; i < data.length; i++) {
      int x = data[i];
      if (32 < x && x > 127) {
        return true;
      }
    }
    return false;
  }
}
