package com.wuni.util.interaction;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;

import com.wuni.util.CommonValidator;
import com.wuni.util.sql.DataModel;

/**
 * contains all interaction specific utilites
 *
 * @authour Mohamed Syed
 * @since   wu315_20110726
 *
 */
public class InteractionUtilities {

  public HashMap getUserBlockedStatus(String ip, String emailId, String userId) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(ip) && validate.isBlankOrNull(emailId) && validate.isBlankOrNull(userId)) {
      validate = null;
      return null;
    }
    if (userId.equals("0")) { //reassign y = -999, to avoid blocking the default un-logged-in user id 
      userId = "-999";
    }
    HashMap userBlockedStatus = new HashMap();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    parameters.add(ip);
    parameters.add(emailId);
    parameters.add(userId);
    try {
      resultset = datamodel.getResultSet("WU_EMAIL_PKG.IS_BLOCKED_USER_FN", parameters);
      while (resultset.next()) {
        userBlockedStatus.put("IP_BLOCKED", resultset.getString("IP_BLOCKED"));
        userBlockedStatus.put("EMAIL_ID_BLOCKED", resultset.getString("EMAIL_ID_BLOCKED"));
        userBlockedStatus.put("USER_ID_BLOCKED", resultset.getString("USER_ID_BLOCKED"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
      datamodel = null;
      if (resultset != null) {
        try {
          resultset.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      resultset = null;
      parameters.clear();
      parameters = null;
      validate = null;
    }
    return userBlockedStatus;
  } //end of getUserBlockedStatus()

  public boolean isBlockedUserId(String userId) {
    boolean userIdBlocked = false;
    //
    if (userId != null && userId.trim().length() > 0) {
      userId = userId.trim();
      if (userId.equals("0")) {
        userId = "-999";
      }
      DataModel datamodel = new DataModel();
      String tmp = "";
      Vector parameters = new Vector();
      parameters.add(userId);
      try {
        tmp = datamodel.getString("WU_EMAIL_PKG.IS_USER_ID_BLOCKED_FN", parameters);
        if (tmp != null && tmp.equalsIgnoreCase("TRUE")) {
          userIdBlocked = true;
        } else {
          userIdBlocked = false;
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        datamodel = null;
        parameters.clear();
        parameters = null;
        tmp = null;
      }
    }
    //
    return userIdBlocked;
  }

  public boolean isBlockedEmailId(String emailId) {
    boolean emailIdBlocked = false;
    //
    if (emailId != null && emailId.trim().length() > 0) {
      emailId = emailId.trim();
      DataModel datamodel = new DataModel();
      String tmp = "";
      Vector parameters = new Vector();
      parameters.add(emailId);
      try {
        tmp = datamodel.getString("WU_EMAIL_PKG.IS_EMAIL_ID_BLOCKED_FN", parameters);
        if (tmp != null && tmp.equalsIgnoreCase("TRUE")) {
          emailIdBlocked = true;
        } else {
          emailIdBlocked = false;
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        datamodel = null;
        parameters.clear();
        parameters = null;
        tmp = null;
      }
    }
    //
    return emailIdBlocked;
  }

  public boolean isBlockedIp(String clientIp) {
    boolean ipBlocked = false;
    //
    if (clientIp != null && clientIp.trim().length() > 0) {
      clientIp = clientIp.trim();
      DataModel datamodel = new DataModel();
      String tmp = "";
      Vector parameters = new Vector();
      parameters.add(clientIp);
      try {
        tmp = datamodel.getString("WU_EMAIL_PKG.IS_IP_BLOCKED_FN", parameters);
        if (tmp != null && tmp.equalsIgnoreCase("TRUE")) {
          ipBlocked = true;
        } else {
          ipBlocked = false;
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        datamodel = null;
        parameters.clear();
        parameters = null;
        tmp = null;
      }
    }
    //
    return ipBlocked;
  }

}
