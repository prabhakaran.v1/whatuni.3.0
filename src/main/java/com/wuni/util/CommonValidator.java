package com.wuni.util;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * will contain simple validation function
 *
 * @author Mohamed Syed
 * @since wu314_20110712
 *
 */
@Component
public class CommonValidator {

  public boolean isBlankOrNullOrZero(String inString) {
    if (inString == null || inString.trim().length() == 0 || inString.trim().equals("0")) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isNotBlankAndNotNullAndNotZero(String inString) {
    if (inString == null || inString.trim().length() == 0 || inString.trim().equals("0")) {
      return false;
    } else {
      return true;
    }
  }

  public boolean getBoolean(String inString) {
    if ((inString != null && inString.trim().length() > 0) && (inString.equalsIgnoreCase("TRUE") || inString.equalsIgnoreCase("YES"))) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isNotBlankAndNotNull(String inString) {
    if (inString == null || inString.trim().length() == 0) {
      return false;
    } else {
      return true;
    }
  }

  public boolean isBlankOrNull(String inString) {
    if (inString == null || inString.trim().length() == 0) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isEmptyList(List inList) {
    if (inList == null || inList.size() == 0) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isNonEmptyList(List inList) {
    if (inList == null || inList.size() == 0) {
      return false;
    } else {
      return true;
    }
  }

}
