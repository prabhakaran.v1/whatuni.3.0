package com.wuni.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

/**
* Class         : UrlRewriteFilter.java
*
* Description   : 
*
* @version      : 1.0
*
* Change Log :
* **************************************************************************************************************
* author	  Ver 	   Created On      Modified On 	    Modification Details 	Change
* **************************************************************************************************************
* Priyaa Parthasarathy   1.0       28-11-2013        First draft           10_DEC_2013
*
*/
public class L2Filter implements Filter {
  private FilterConfig filterConfig = null;
  public L2Filter() {
  }
  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, 
                                                                                                         ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    String url =  req.getRequestURI();
    String str1 = URLDecoder.decode(url, "UTF-8");
    String redirectURL = null;
    if(url.contains("/loc.html")) {
        String rank_split[]=url.split("/");
        String studyLevelId = rank_split[6];
        String categoryId = rank_split[8];
        String subjectName = rank_split[4];
        String studyleveltext = "";
        
        if("M".equals(studyLevelId)){
          studyleveltext = "degree";
        }else if("L".equals(studyLevelId)){      
            studyleveltext = "postgraduate";
        }else if("N".equals(studyLevelId)){         
           studyleveltext = "hnd-hnc";
        }else if("A".equals(studyLevelId)){        
          studyleveltext = "foundation-degree";
        }else if("T".equals(studyLevelId)){
          studyleveltext = "access-foundation";
        }
        studyleveltext = studyleveltext + "-courses";
        if(!GenericValidator.isBlankOrNull(subjectName)){
          subjectName = subjectName.substring(0,subjectName.indexOf("UK"));
          subjectName = subjectName + "united-kingdom";
          subjectName = subjectName.toLowerCase();
        }
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);       
        Map resultMap = (Map)commonBusiness.checkIfL2Node(categoryId, studyLevelId);        
        String str = (String)resultMap.get("p_status");
        if("Y".equals(str)){
        redirectURL = "/degrees/courses/" + studyleveltext + "/" + subjectName + "/" + studyLevelId.toLowerCase() + "/united+kingdom/r/" + categoryId +"/page.html" ;
        res.setStatus(res.SC_MOVED_PERMANENTLY);
        res.setHeader("Location", redirectURL);
        res.setHeader("Connection", "close");
        return;       
        }else{
          filterChain.doFilter(request, response);
        }
      } else {
        filterChain.doFilter(request, response);
      }
  }
  public void destroy() {
    this.filterConfig = null;
  }
  public static boolean checkUnicode(String str) {
    char data[] = str.toCharArray();
    for (int i = 0; i < data.length; i++) {
      int x = data[i];
      if (32 < x && x > 127) {
        return true;
      }
    }
    return false;
  }
}
