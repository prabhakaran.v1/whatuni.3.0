package com.wuni.util.affiliate;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;

import com.wuni.util.seo.SeoUrlsForAffiliateSiteLinks;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.ClearingInfoVO;

public class AffiliateUtilities {

  /**
   * will return getClearingInfoList 
   * @return
   * @throws Exception
   * @author Mohamed Syed
   */
  public ArrayList getClearingInfoList(String collegeId) {
    ArrayList clearingInfoList = null;
    if (collegeId == null || collegeId.trim().length() == 0) {
      return clearingInfoList;
    }
    clearingInfoList = new ArrayList();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    ClearingInfoVO clearingInfoVO = null;
    SeoUrlsForAffiliateSiteLinks affiliateSiteLinks = new SeoUrlsForAffiliateSiteLinks();
    try {
      parameters.add(collegeId);
      resultset = datamodel.getResultSet("WU_UTIL_PKG.GET_CLEARING_INFO_FN", parameters);
      while (resultset.next()) {
        clearingInfoVO = new ClearingInfoVO();
        clearingInfoVO.setClearingCollegeId(resultset.getString("college_id"));
        clearingInfoVO.setClearingCollegeName(resultset.getString("college_name"));
        clearingInfoVO.setClearingProfileCount(resultset.getString("clearing_profile_count"));
        if (clearingInfoVO.getClearingProfileCount() != null && (Integer.parseInt(String.valueOf(clearingInfoVO.getClearingProfileCount())) == 1)) {
          clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcPoviderClearingURL(clearingInfoVO.getClearingCollegeId(), clearingInfoVO.getClearingCollegeName()));
        } else {
          clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcClearingHomeURL());
        }
        clearingInfoList.add(clearingInfoVO);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
      datamodel = null;
      resultset = null;
      parameters.clear();
      parameters = null;
      affiliateSiteLinks = null;
    }
    return clearingInfoList;
  } //end of getClearingInfo()

}
