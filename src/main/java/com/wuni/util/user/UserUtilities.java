package com.wuni.util.user;

import java.util.Vector;

import com.wuni.util.sql.DataModel;

/**
 * contains all user specific utilites
 *
 * @authour Mohamed Syed
 * @since   wu315_20110726
 *
 */
public class UserUtilities {

  public String getEmailIdByUserId(String userId) {
    if (userId == null || userId.trim().length() == 0 || userId.equals("0")) {
      return "";
    }
    userId = userId.trim();
    DataModel datamodel = new DataModel();
    String userEmailId = "";
    Vector parameters = new Vector();
    parameters.add(userId);
    try {
      userEmailId = datamodel.getString("WU_EMAIL_PKG.GET_EMAIL_ID_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel = null;
      parameters.clear();
      parameters = null;
    }
    return userEmailId;
  }

  public String getUserIdByEmailId(String emailId) {
    if (emailId == null || emailId.trim().length() == 0) {
      return "";
    }
    emailId = emailId.trim();
    DataModel datamodel = new DataModel();
    String userEmailId = "";
    Vector parameters = new Vector();
    parameters.add(emailId);
    try {
      userEmailId = datamodel.getString("WU_EMAIL_PKG.GET_USER_ID_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel = null;
      parameters.clear();
      parameters = null;
    }
    return userEmailId;
  }

}
