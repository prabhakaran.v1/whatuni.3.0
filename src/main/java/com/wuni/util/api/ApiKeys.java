package com.wuni.util.api;

import WUI.utilities.GlobalConstants;

/**
 * will contain all third party API/account releated details.
 *
 * @author Mohamed Syed
 * @since wu303_20110222
 *
 */
public interface ApiKeys {
  //
  //----------------------------------------------------------------------------
  //
  /*
   * Yahoo mail account details
   */
  public final String yahooMailForYahooApis_UserName = "api.keys@yahoo.in";
  public final String yahooMailForYahooApis_Password = "apikeys@yahoo";
  //
  //----------------------------------------------------------------------------
  //
  /*
   * Yahoo PlaceFinder API key-details for http://www.whatuni.com
   * Verification file path:     http://www.whatuni.com/THX3CmVqT3klYtlTmKT61Q--.html
   * Example call to ge lat/lng value
   *  http://where.yahooapis.com/geocode?
   *     appid=dj0yJmk9ck03TGdUU2NNRmVZJmQ9WVdrOVNHcFRiV2x3Tm5VbWNHbzlOakF4TlRBME9UWXkmcz1jb25zdW1lcnNlY3JldCZ4PTZm
   *    &country=UK
   *    &postal=LL57 2DG
   */
  public final String yahooPlacefinderWebserviceBaseUrl = "http://where.yahooapis.com/geocode";
  public final String yahooPlacefinderConsumerKey = "dj0yJmk9ck03TGdUU2NNRmVZJmQ9WVdrOVNHcFRiV2x3Tm5VbWNHbzlOakF4TlRBME9UWXkmcz1jb25zdW1lcnNlY3JldCZ4PTZm";
  public final String yahooPlacefinderConsumerSecret = "8af14a7f877f3d27cf5d138ebd641f940f899729";
  public final String yahooPlacefinderApplicationUrl = GlobalConstants.WHATUNI_SCHEME_NAME + "www.whatuni.com";//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
  public final String yahooPlacefinderVerificationFile = GlobalConstants.WHATUNI_SCHEME_NAME + "www.whatuni.com/ublCzqtGdB8iiRiZTG_iHQ--.html";//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
  public final String yahooPlacefinderSiteOwnerName = "David Wadham";
  public final String yahooPlacefinderSiteOwnerEmailId = "whatuni@hotcourses.co.in";
  //
  //----------------------------------------------------------------------------
  //
  /*
   * Yahoo PlaceFinder API key-details for http://www.schoolsnet.com
   * Verification file path:     http://www.schoolsnet.com/KszipvxrQhRSlgfvLY26Gw--.html
   * Example call to ge lat/lng value
   *  http://where.yahooapis.com/geocode?
   *     appid=dj0yJmk9amtqZ0hqdERJdklFJmQ9WVdrOU5IaDBPSE5TTTJNbWNHbzlPREExTWpjNU56WXkmcz1jb25zdW1lcnNlY3JldCZ4PTI1
   *    &country=UK
   *    &postal=LL57 2DG
   *
   * i am storing these schoolsnet details here as backup
   */
  public final String sn_yahooPlacefinderWebserviceBaseUrl = "http://where.yahooapis.com/geocode";
  public final String sn_yahooPlacefinderConsumerKey = "dj0yJmk9amtqZ0hqdERJdklFJmQ9WVdrOU5IaDBPSE5TTTJNbWNHbzlPREExTWpjNU56WXkmcz1jb25zdW1lcnNlY3JldCZ4PTI1";
  public final String sn_yahooPlacefinderConsumerSecret = "2b26ce4b9befcd159a5164b8b84804175ed5d020";
  public final String sn_yahooPlacefinderApplicationUrl = "http://www.schoolsnet.com";
  public final String sn_yahooPlacefinderVerificationFile = "http://www.schoolsnet.com/ublCzqtGdB8iiRiZTG_iHQ--.html";
  public final String sn_yahooPlacefinderSiteOwnerName = "David Wadham";
  public final String sn_yahooPlacefinderSiteOwnerEmailId = "schoolsnet@hotcourses.co.in";
  //
  //----------------------------------------------------------------------------
  //
  /*
   * Sharethis account details for whatuni:
   * URL:         http://sharethis.com
   * Full Name:   Whatuni
   * Email:       whatuni@hotcourses.co.in
   * Password:    share_whatuni
   */
  public final String SHARETHIS_URL = "http://sharethis.com";
  public final String SHARETHIS_FULLNAME = "Whatuni";
  public final String SHARETHIS_EMAIL = "whatuni@hotcourses.co.in";
  public final String SHARETHIS_PASSWORD = "share_whatuni";
  public final String SHARETHIS_PUBLISHER_KEY = "764f7829-e68f-4d7b-953f-3ff3b14b61b5";
  //

}//ApiKeys
