package com.wuni.util.api.yahoo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import com.wuni.util.api.ApiKeys;

/**
 * will contain all Yahoo related webservice API functionality (like Yahoo map, PlaceFinder and etc).
 *
 * @author Mohamed Syed
 * @since wu303_20110222
 *
 */
public class YahooPlaceFinder {

  /**
   * will return latitudeLongitudeMap by generating yahooApiWebserviceURL accouding to callParam and using core getLatitudeLongitudeFromYahoo() function call.
   *
   * @param callParam
   * @param callType
   * @return latitudeLongitudeMap
   *
   * @author Mohamed Syed
   * @since wu303_20110222
   *
   */
  public HashMap getLatitudeLongitude(String callParam, String callType) {
    HashMap latitudeLongitudeMap = new HashMap();
    String yahooApiWebserviceURL = null;
    if ((callParam == null || callParam.trim().length() == 0) || (callType == null || callType.trim().length() == 0)) {
      latitudeLongitudeMap.put("latitude", "51.492306");
      latitudeLongitudeMap.put("longitude", "-0.229320");
      latitudeLongitudeMap.put("api.yahoo.url", "NULL");
    } else {
      yahooApiWebserviceURL = ApiKeys.yahooPlacefinderWebserviceBaseUrl + "?appid=" + ApiKeys.yahooPlacefinderConsumerKey + "&country=UK";
      if (callType.equals("POSTCODE")) {
        yahooApiWebserviceURL = yahooApiWebserviceURL + "&postal=" + callParam;
      } else if (callType.equals("CITY")) {
        yahooApiWebserviceURL = yahooApiWebserviceURL + "&city=" + callParam;
      }
      latitudeLongitudeMap = getLatitudeLongitudeFromYahoo(yahooApiWebserviceURL);
    }
    //nullify unused objects
    yahooApiWebserviceURL = null;
    //
    return latitudeLongitudeMap;
  }

  /**
   * core functionality to fetch latitude & longitude from Yahoo PlaceFinder, according to postcode/city/address.
   *
   * @param yahooApiWebserviceURL
   * @return latitudeLongitudeMap
   *
   * @author Mohamed Syed
   * @since wu303_20110222
   *
   */
  public HashMap getLatitudeLongitudeFromYahoo(String yahooApiWebserviceURL) {
    HashMap latitudeLongitudeMap = new HashMap();
    if (yahooApiWebserviceURL == null && yahooApiWebserviceURL.trim().length() == 0) {
      latitudeLongitudeMap.put("latitude", "51.492306");
      latitudeLongitudeMap.put("longitude", "-0.229320");
      latitudeLongitudeMap.put("api.yahoo.url", "NULL");
    } else {
      int responseCode = 0;
      String inputLine;
      String resposeFromYahooAsXml = "";
      //String yahooApiWebserviceURL = ApiKeys.yahooPlacefinderWebserviceUrl + "?appid=" + ApiKeys.yahooPlacefinderConsumerKey + "&country=UK&postal=" + postcode;
      //
      URL url = null;
      BufferedReader bufferReader = null;
      URLConnection urlConnection = null;
      HttpURLConnection httpUrlConnection = null;
      InputStreamReader inputStreamReader = null;
      //
      try {
        url = new URL(yahooApiWebserviceURL);
        //
        urlConnection = url.openConnection();
        httpUrlConnection = (HttpURLConnection)urlConnection;
        httpUrlConnection.connect();
        responseCode = httpUrlConnection.getResponseCode();
        //        
        if (responseCode == 200) {
          inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
          bufferReader = new BufferedReader(inputStreamReader);
          while ((inputLine = bufferReader.readLine()) != null) {
            resposeFromYahooAsXml += inputLine;
          }
        } else {
          //nullify unused objects
          try {
            url = null;
            if (bufferReader != null) {
              bufferReader.close();
            }
            bufferReader = null;
            urlConnection = null;
            if (httpUrlConnection != null) {
              httpUrlConnection.disconnect();
            }
            httpUrlConnection = null;
            if (inputStreamReader != null) {
              inputStreamReader.close();
            }
            inputStreamReader = null;
          } catch (Exception e) {
            e.printStackTrace();
          }
          inputLine = null;
          resposeFromYahooAsXml = null;
          // return default values
          latitudeLongitudeMap.put("latitude", "51.492306");
          latitudeLongitudeMap.put("longitude", "-0.229320");
          latitudeLongitudeMap.put("api.yahoo.url", "NULL");
          return latitudeLongitudeMap;
        }
        //        
        resposeFromYahooAsXml = resposeFromYahooAsXml.toUpperCase();
        int startLat = resposeFromYahooAsXml.indexOf("<LATITUDE>");
        int endLat = resposeFromYahooAsXml.indexOf("</LATITUDE>");
        int startLng = resposeFromYahooAsXml.indexOf("<LONGITUDE>");
        int endLng = resposeFromYahooAsXml.indexOf("</LONGITUDE>");
        //
        if (startLat > 0 && endLat > 0 && startLng > 0 && endLng > 0) {
          latitudeLongitudeMap.put("latitude", resposeFromYahooAsXml.substring(startLat + 10, endLat));
          latitudeLongitudeMap.put("longitude", resposeFromYahooAsXml.substring(startLng + 11, endLng));
        } else {
          latitudeLongitudeMap.put("latitude", "51.492306");
          latitudeLongitudeMap.put("longitude", "-0.229320");
        }
        latitudeLongitudeMap.put("api.yahoo.url", url);
        //
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        try {
          //nullify unused objects
          url = null;
          if (bufferReader != null) {
            bufferReader.close();
          }
          bufferReader = null;
          urlConnection = null;
          if (httpUrlConnection != null) {
            httpUrlConnection.disconnect();
          }
          httpUrlConnection = null;
          if (inputStreamReader != null) {
            inputStreamReader.close();
          }
          inputStreamReader = null;
        } catch (Exception e) {
          e.printStackTrace();
        }
        inputLine = null;
        resposeFromYahooAsXml = null;
      }
    }
    return latitudeLongitudeMap;
  } //end of getLatitudeLongitudeByPostcode()

}//end of class
