package com.wuni.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtilities {

  public void updateSessionData(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    CommonValidator validator = new CommonValidator();
    // update x
    String x = (String)session.getAttribute("x");
    if (validator.isBlankOrNull(x)) {
      session.setAttribute("x", "16180339");
    }
    // update y
    String y = (String)session.getAttribute("y");
    if (validator.isBlankOrNull(y)) {
      session.setAttribute("y", "0");
    }
    // update GEO_TARGET_FLAG //TODO
    String geoTargetFlag = (String)session.getAttribute("GEO_TARGET_FLAG");
    if (validator.isBlankOrNull(geoTargetFlag)) {
      session.setAttribute("GEO_TARGET_FLAG", "");
    }
    // update SERVER_NAME //TODO
    String serverName = (String)session.getAttribute("SERVER_NAME");
    if (validator.isBlankOrNull(serverName)) {
      session.setAttribute("SERVER_NAME", "GOOGLE");
    }
  }

  public String getX(HttpSession session) {
    String x = (String)session.getAttribute("x");
    CommonValidator validator = new CommonValidator();
    if (validator.isBlankOrNull(x)) {
      x = "16180339";
    }
    return x;
  }

  public String getY(HttpSession session) {
    CommonValidator validator = new CommonValidator();
    String y = (String)session.getAttribute("y");
    if (validator.isBlankOrNull(y)) {
      y = "0";
    }
    return y;
  }

  public void setX(HttpSession session, String x) {
    //session.removeAttribute("x");
    session.setAttribute("x", x);
  }

  public void setY(HttpSession session, String y) {
    //session.removeAttribute("y");
    session.setAttribute("y", y);
  }

  public void releaseAllSessionData(HttpServletRequest request) { //TODO
    HttpSession session = request.getSession(false);
    session.getAttributeNames();
    //will release all data which are stored in session scope.
  }

}
