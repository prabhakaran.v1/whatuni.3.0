package com.wuni.util;

import javax.servlet.http.HttpServletRequest;

/**
 * will contain Request & Response related utilities
 *
 * @author Mohamed Syed
 * @since wu314_20110712
 *
 */
public class RequestResponseUtils {

  public String getRemoteIp(HttpServletRequest request) {
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    return clientIp;
  }

}
