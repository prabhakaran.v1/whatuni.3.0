package com.wuni.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;

/**
* Class         : RequestEncodeFilter.java
*
* Description   : Java Filter to set widget id in session.
*
* @version      : 1.0
*
* Change Log :
* **************************************************************************************************************
* author                 Ver          Created On      Modified On          Modification Details    Change
* **************************************************************************************************************
* Sekhar Kasala   		1.0      29/06/2012                           First draft             wu413_20120710
* Sangeeth.S	  		1.1	     10_NOV_2020             Added cookie consent flag check   
*
*/
public class RequestEncodeFilter implements Filter {

  private FilterConfig filterConfig = null;

  public RequestEncodeFilter() {
  }

  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    // Below code added 12th Apr 2013 release 
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    Cookie cook[] = req.getCookies();
    String dname = request.getServerName();
    String dname_prefix = dname != null ? dname.substring(dname.indexOf("."), dname.length()) : "";
    if (cook != null) {
      for (int i = 0; i < cook.length; i++) {
        if (cook[i].getName().equals("JSESSIONID")) {
          Cookie scookieDummy = new Cookie("JSESSIONID", cook[i].getValue());
          scookieDummy.setMaxAge(-1);
          scookieDummy.setDomain(dname_prefix); //Specifies the domain within which this cookie should be presented.        
          scookieDummy.setPath("/");
          scookieDummy.setSecure(true);
          res.addCookie(scookieDummy);
        }
      }
    }
    request.setCharacterEncoding("UTF-8");
    HttpSession session = req.getSession();
    if (request.getParameter("campaign") != null) {
      String widgetId = request.getParameter("campaign");
      try {
        session.setAttribute("widgetId", widgetId);
      } catch (Exception e) {
        session.setAttribute("widgetId", null);
      }
    }
    //Added by Sangeeth.S for setting user cookie content preference CookiePOC
    if(StringUtils.isBlank(new CookieManager().getCookieValue(req,GlobalConstants.COOKIE_CONSENT_COOKIE))){
	    String cookieConsent = GlobalConstants.CONSENT_COOKIE_DEFAULT_VAL; //0-Strictly necessary, 1 - Functional, 1 - performance , 1- target
	    Cookie cookie = CookieManager.createCookie(req,GlobalConstants.COOKIE_CONSENT_COOKIE, cookieConsent);	    
	    res.addCookie(cookie);	    
	}
    CommonUtil.setThirdPartyCookieFlagInsession(req,res);  
    new CommonFunction().getUserTrackSessionID(req, res);
    //new CommonFunction().setUserHomePage((HttpServletRequest)request,(HttpServletResponse)response); //16-Sep-14 Created by Amir
    new CommonFunction().getUserTimeLineTrackPod(req); //08_OCT_14 Created by Amir
    //
    /* ::START:: Yogeswari :: 09.06.2015 :: setting request URL for clearing splash screen */
    String queryString = req.getQueryString();
    request.setAttribute("REQUEST_URI", req.getRequestURI() + ((queryString != null && queryString.trim().length() > 0) ? "?" + queryString : ""));
    /* ::END:: Yogeswari :: 09.06.2015 :: setting request URL for clearing splash screen */
    //
    filterChain.doFilter(request, response);
    if (cook != null) {
      for (int i = 0; i < cook.length; i++) {
        if (cook[i].getName().equals("JSESSIONID")) {
          Cookie scookieDummy = new Cookie("JSESSIONID", cook[i].getValue());
          scookieDummy.setMaxAge(-1);
          scookieDummy.setDomain(dname_prefix); //Specifies the domain within which this cookie should be presented.        
          scookieDummy.setPath("/");
          scookieDummy.setSecure(true);
          res.addCookie(scookieDummy);
        }
      }
    }
    response.setContentType("text/html; charset=UTF-8"); //Setting the character set for the response 
  }

  public void destroy() {
    this.filterConfig = null;
  }

}
