package com.wuni.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;

import com.wuni.util.seo.SeoUrls;

/**
* Class         : AuthenticationFilter.java
* Description   : Java Filter to redirect all ajax urls if the session is expired.
* @version      : 1.0
* Change Log :
* **************************************************************************************************************
* author                 Ver          Created On      Modified On        Modification Details    Change
* **************************************************************************************************************
* Thiyagu G              1.0          16/04/2017                         First draft             wu564_20170416
*
*/
public class AuthenticationFilter implements Filter {

  private FilterConfig filterConfig = null;

  public AuthenticationFilter() {
  }

  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {    
    HttpServletRequest httpRequest = (HttpServletRequest)request;
    HttpServletResponse httpResponse = (HttpServletResponse)response;
    String contextRoot = httpRequest.getContextPath();
    CommonUtil commonUtil = new CommonUtil();
    if("/degrees".equals(contextRoot)) {      
      SessionData sessionData = new SessionData();
      SeoUrls seoUrls=new SeoUrls();
      String userId = sessionData != null ? new SessionData().getData(httpRequest, "y") : null;            
      //boolean doesThisPageNeedsAuthentication = commonUtil.doesThisPageNeedAuthentication(httpRequest);
      //if(doesThisPageNeedsAuthentication) {
        if(commonUtil.isThisAjaxCall(httpRequest)) {          
          if((userId == null || "0".equals(userId) || "".equals(userId))){
            CommonUtil.setResponseText(httpResponse, new StringBuffer("SESSION_EXPIRED##" + seoUrls.getWhatuniLoginURL()));            
          } else{
            filterChain.doFilter(request, response);
          }
        } else{
          if(commonUtil.isThisAjaxAndNormalCall(httpRequest)) {
            String fromFlag = request.getParameter("fromFlag") != null ? request.getParameter("fromFlag") : "";
            if(!"".equals(fromFlag) && (userId == null || "0".equals(userId) || "".equals(userId))){
              CommonUtil.setResponseText(httpResponse, new StringBuffer("SESSION_EXPIRED##" + seoUrls.getWhatuniLoginURL()));            
            }else{
              filterChain.doFilter(request, response);
            }
          }else {
            filterChain.doFilter(request, response);
          }
        }
      //} else {
        //filterChain.doFilter(request, response);
      //}
    }
  }

  public void destroy() {
    this.filterConfig = null;
  }

}
