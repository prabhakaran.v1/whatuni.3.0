package com.wuni.util.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

/**
  * @DataModel.java
  * @Version : 2.0
  * @July 28 2009
  * @www.whatuni.com
  * @Created By : Velayutharaj.M
  * @Modified Mohamed Syed
  * @Purpose  : This program used to establish the connection between Application server & Database server.
  * *************************************************************************************************************************
  * Date          Name                      Ver.        Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 2010-03-30    Mohamed Syed                          closing cursors which are not in map
  * 2010-09-15    Mohamed Syed              wu_2.0.9    time & resource tracking added.
  *
  */
public class DataModel {

  private static Hashtable openCursors = null;
  private static Hashtable openConnections = null;
  private static DataSource datasource = null;
  static {
    try {
      InitialContext ic = new InitialContext();
      datasource = (DataSource)ic.lookup("whatuni");
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("1) DataModel --> STATIC BLOCK --> Exception --> while datasource creation");
    } finally {
      if (datasource == null) {
        try {
          InitialContext ic = new InitialContext();
          datasource = (DataSource)ic.lookup("whatuni");
        } catch (Exception e) {
          e.printStackTrace();
          System.out.println("2) DataModel --> STATIC BLOCK  --> Exception --> while datasource creation");
        }
      }
    }
    if (datasource != null) {
      System.out.println("DataModel --> STATIC BLOCK --> SUCCESS --> while datasource creation");
    } else {
      System.out.println("DataModel --> STATIC BLOCK --> FAILURE --> while datasource creation");
    }
  }

  /**
   *   This function is used to create a datasource for the Database connection.
   * @return connection as Connection.
   */
  public Connection getDataSourceConnection() {
    Connection connection = null;
    try {
      connection = datasource.getConnection();
      /* 27 April 2009 To handle if any chance the datasource lost from memory */
      if (connection == null) {
        try {
          InitialContext ic = new InitialContext();
          datasource = (DataSource)ic.lookup("whatuni");
          connection = datasource.getConnection();
        } catch (Exception e) {
          e.printStackTrace();
          System.out.println("2) DataModel --> getData() --> Exception --> while conn object creation");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("1) DataModel --> getData() --> Exception --> while conn object creation");
    } finally { /* 2010-06-27-->WU_2.0.3 To handle if any chance the datasource lost from memory */
      if (connection == null) {
        try {
          InitialContext ic = new InitialContext();
          datasource = (DataSource)ic.lookup("whatuni");
          connection = datasource.getConnection();
        } catch (Exception innerE) {
          innerE.printStackTrace();
          System.out.println("3) DataModel --> getData() --> Exception --> while conn object creation");
        } finally {
          if (connection == null) {
            try {
              InitialContext ic = new InitialContext();
              datasource = (DataSource)ic.lookup("whatuni");
              connection = datasource.getConnection();
            } catch (Exception innerE) {
              innerE.printStackTrace();
              System.out.println("4) DataModel --> getData() --> Exception --> while conn object creation");
            }
          }
        }
      }
    }
    /*
    if(connection != null){
      System.out.println("DataModel --> getData() --> SUCCESS --> while conn object creation");
    } else {
      System.out.println("DataModel --> getData() --> FAILURE --> while conn object creation");
    }
    */
    return connection;
  }

  public DataModel() {
    if (openCursors == null) {
      openCursors = new Hashtable();
    }
    if (openConnections == null) {
      openConnections = new Hashtable();
    }
    /* 27 April 2009 To handle if any chance the datasource lost from memory */
    if (datasource == null) {
      try {
        InitialContext ic = new InitialContext();
        datasource = (DataSource)ic.lookup("whatuni");
      } catch (Exception e) {
        e.printStackTrace();
        System.out.println("1) DataModel --> CONSTRUCTOR BLOCK --> Exception --> while datasource creation");
      } finally {
        if (datasource == null) {
          try {
            InitialContext ic = new InitialContext();
            datasource = (DataSource)ic.lookup("whatuni");
          } catch (Exception e) {
            e.printStackTrace();
            System.out.println("2) DataModel --> CONSTRUCTOR BLOCK --> Exception --> while datasource creation");
          }
        }
      }
    }
  }

  /**
    *   This function is used to get the data from database after execute a SQL statement.
    * @param needsCursor
    * @param procedureName_in
    * @param values_in
    * @return ResultSet as Object.
    * @throws SQLException
    */
  private Object getData(boolean needsCursor, String procedureName_in, Vector values_in) throws java.sql.SQLException {
    SQLException exLog = null;
    Object retObj = null;
    CallableStatement cstmt = null;
    Connection conn = null;
    try {
      conn = datasource.getConnection();
      /* 27 April 2009 To handle if any chance the datasource lost from memory */
      if (conn == null) {
        try {
          InitialContext ic = new InitialContext();
          datasource = (DataSource)ic.lookup("whatuni");
          conn = datasource.getConnection();
        } catch (Exception e) {
          e.printStackTrace();
          System.out.println("2) DataModel --> getData() --> Exception --> while conn object creation");
        } finally {
          if (conn == null) {
            try {
              InitialContext ic = new InitialContext();
              datasource = (DataSource)ic.lookup("whatuni");
              conn = datasource.getConnection();
            } catch (Exception innerE) {
              innerE.printStackTrace();
              System.out.println("3) DataModel --> getData() --> Exception --> while conn object creation");
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("1) DataModel --> getData() --> Exception --> while conn object creation");
    } finally { /* 2010-06-27-->WU_2.0.3 To handle if any chance the datasource lost from memory */
      if (conn == null) {
        try {
          InitialContext ic = new InitialContext();
          datasource = (DataSource)ic.lookup("whatuni");
          conn = datasource.getConnection();
        } catch (Exception innerE) {
          innerE.printStackTrace();
          System.out.println("4) DataModel --> getData() --> Exception --> while conn object creation");
        } finally {
          if (conn == null) {
            try {
              InitialContext ic = new InitialContext();
              datasource = (DataSource)ic.lookup("whatuni");
              conn = datasource.getConnection();
            } catch (Exception innerE) {
              innerE.printStackTrace();
              System.out.println("5) DataModel --> getData() --> Exception --> while conn object creation");
            }
          }
        }
      }
    }
    /*
    System.out.println("OPEN Connection name : " + conn.toString());
    System.out.println("Procedure  name : " + procedureName_in + "  Values " + values_in);
    */
    try {
      StringBuffer buff = new StringBuffer();
      buff.append("{ ? = call ");
      buff.append(procedureName_in);
      if (values_in != null && values_in.size() > 0) {
        /* if there are parameters to the procedure, then modify the call to include the placeholders */
        buff.append("(");
        for (int i = 0; i < values_in.size(); i++) {
          buff.append("?,");
        }
        buff.setLength(buff.length() - 1);
        buff.append(")}");
        cstmt = conn.prepareCall(buff.toString());
        if (needsCursor) {
          cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        } else {
          cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
        }
        for (int i = 0; i < values_in.size(); i++) {
          /* since the Oracle Cursor is the 1st parameter, the rest must follow */
          cstmt.setString(i + 2, (String)values_in.elementAt(i));
        }
      } else {
        /* there are no parameters to the procedure */
        buff.append("}");
        cstmt = conn.prepareCall(buff.toString());
        if (needsCursor) {
          cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        } else {
          cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
        }
      }
      /* execute the query */
      cstmt.execute();
      retObj = cstmt.getObject(1);
      if (needsCursor) {
        if (retObj != null) {
          openCursors.put((ResultSet)retObj, cstmt);
          openConnections.put((ResultSet)retObj, conn); /*10 Mar 2009 */
        }
      }
    } catch (SQLException ex) {
      if (retObj != null && needsCursor) {
        closeCursor((ResultSet)retObj);
      } else {
        if (cstmt != null)
          try {
            cstmt.close();
          } catch (SQLException closeEx) {
            closeEx.printStackTrace();
          }
      }
      if (conn != null) {
        /* System.out.println("1) CLOSE Connection name : " + conn.toString());      //10.02.2009 release */
        conn.close();
      }
      exLog = new SQLException(ex.getMessage(), ex.getSQLState(), ex.getErrorCode());
    } finally {
      if (cstmt != null && !needsCursor) {
        try {
          cstmt.close();
          cstmt = null;
        } catch (SQLException closeEx) {
          closeEx.printStackTrace();
        }
        if (conn != null) {
          /* System.out.println("2) CLOSE Connection name : " + conn.toString());      //10.02.2009 release */
          conn.close();
        }
      }
      if (exLog != null)
        throw exLog;
    }
    return retObj;
  }

  /**
    * This function is used to close the currently open cursors.
    * @param rs
    * @return none.
    */
  public static void closeCursor(ResultSet rs) {
    if (rs == null) {
      return;
    }
    if (openCursors.containsKey(rs)) {
      CallableStatement cstmt = (CallableStatement)openCursors.get(rs);
      Connection connection = (Connection)openConnections.get(rs); /* 10 Mar 2009 */
      try {
        /*
        System.out.println("CLOSED CallableStatement name" + cstmt.getConnection().toString()); //10.02.2009 release
        System.out.println("CLOSED Connection name" + connection.toString()); //10.02.2009 release
        */
        cstmt.getConnection().close();
        connection.close(); /* 10 Mar 2009 */
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (cstmt != null) {
        openCursors.remove(rs);
        openConnections.remove(rs); /* 10 Mar 2009 */
        try {
          rs.close();
          cstmt.close();
          rs = null;
          cstmt = null;
        } catch (SQLException ex) {
          System.out.println("1) Exception closing cursor");
        }
      }
    } else {
      try {
        rs.close();
      } catch (SQLException ex) {
        System.out.println("2) Exception closing cursor");
      }
    }
  }

  /**
   *
   * Returns a resultset, used to get multiple records from the Database
   *
   * @param procedureName_in
   * @param values_in
   * @return
   * @throws SQLException
   */
  public ResultSet getResultSet(String procedureName_in, Vector values_in) throws SQLException {
    Long startDbtime = new Long(System.currentTimeMillis());
    ResultSet resultSet = (ResultSet)getData(true, procedureName_in, values_in); //DB_CALL
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallFullDetails(procedureName_in, values_in, startDbtime, endDbtime);
    }
    return resultSet;
  }

  /**
   *
   * Returns a string,  used to call Insert, Update and Delete
   *
   * @param procedureName_in
   * @param values_in
   * @return
   * @throws SQLException
   */
  public String executeUpdate(String procedureName_in, Vector values_in) throws SQLException {
    Long startDbtime = new Long(System.currentTimeMillis());
    String responseStringFromDB = (String)getData(false, procedureName_in, values_in);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallFullDetails(procedureName_in, values_in, startDbtime, endDbtime);
    }
    return responseStringFromDB;
  }

  /**
   *
   * Returns a string,  used to call ordinary functions
   *
   * @param procedureName_in
   * @param values_in
   * @return
   * @throws SQLException
   */
  public String getString(String procedureName_in, Vector values_in) throws SQLException {
    Long startDbtime = new Long(System.currentTimeMillis());
    String responseStringFromDB = (String)getData(false, procedureName_in, values_in);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallFullDetails(procedureName_in, values_in, startDbtime, endDbtime);
    }
    return responseStringFromDB;
  }

  public void setOpenCursors(Hashtable openCursors) {
    this.openCursors = openCursors;
  }

  public Hashtable getOpenCursors() {
    return openCursors;
  }

  public void setOpenConnections(Hashtable openConnections) {
    this.openConnections = openConnections;
  }

  public Hashtable getOpenConnections() {
    return openConnections;
  }

}
