package com.wuni.util.sql;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
  * @GetPLSQL.java
  * @Version : 1.0
  * @Date 13 Feburary 2006
  * @www.hotcourses.com
  * @Created By : Dave, Neil
  * @This class will calls the Given URL and gets called from hotcoursesHeader.jsp,
  *       hotcoursesFooter.jsp.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class GetPLSQL {

  /**
    *   This function is used to call the PL/SQL procedure by using URLConnection.
    * @param inputUrl
    * @return Output of procedure as String.
    * @throws Exception
    */
  public String runProc(String inputUrl) throws Exception {
    try {
      URL url = new URL(inputUrl);
      URLConnection uconnection = url.openConnection();
      String inputLine = "";
      String inputLine2 = "";
      BufferedReader inp = new BufferedReader(new InputStreamReader(uconnection.getInputStream()));
      while ((inputLine = inp.readLine()) != null) {
        inputLine2 += inputLine;
      }
      inp.close();
      inp = null;
      url = null;
      uconnection = null;
      inputLine = null;
      return inputLine2;
    } catch (Exception urlException) {
      return "Error from call " + urlException.getLocalizedMessage();
    }
  }

  /**
    *   This function is used to display the PL/SQL procedure content.
    * @param url
    * @return Output of PL/SQL procedure as String.
    * @throws Exception
    */
  public String showText(String url) throws Exception {
    GetPLSQL getPlsql = new GetPLSQL();
    String l_return = getPlsql.runProc(url);
    getPlsql = null;
    return l_return;
  }

}
 //////////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////