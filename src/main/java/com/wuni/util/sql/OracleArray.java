package com.wuni.util.sql;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class OracleArray {

  public static ARRAY getOracleArray(Connection connection, String typeName, Object[][] values) throws SQLException {
    //Building the array for the type of schools
    ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(typeName, connection);
    ARRAY array = new ARRAY(descriptor, connection, values);
    return array;
  }

  public static ARRAY getOracleArray(Connection connection, String typeName, Object[] values) throws SQLException {
    //Building the array for the type of schools
    ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(typeName, connection);
    ARRAY array = new ARRAY(descriptor, connection, values);
    return array;
  }

}
