package com.wuni.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import WUI.utilities.CommonFunction;

/**
 * Class         : CDURLRewriteFilter.java
 * Description   :
 * @version      : 1.0
 * Change Log :
 * **************************************************************************************************************
 * author	       Ver 	     Created On       Modified On 	       Modification Details 	Change
 * **************************************************************************************************************
 * 
 *
 */
public class CDURLRewriteFilter implements Filter {

  private FilterConfig filterConfig = null;

  public CDURLRewriteFilter() {
  }

  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    String urlString = req.getRequestURI();
    String redirectURL = "";
    if(urlString.contains("/cdetail.html")) {
      String rank_split[]= urlString.split("/");
     //URL length      http://www.whatuni.com/degrees/courses/Degree-details/French-with-Business-Studies-BA-Hons-course-details/54951302/3769/cdetail.html
      CommonFunction comFn = new CommonFunction();
      if(rank_split.length==8){
      String collegeName =   comFn.getCollegeTitleFunction(rank_split[6]);
      collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
      collegeName = collegeName.toLowerCase();
      String courseName = rank_split[4];
      courseName = courseName.substring(0,courseName.indexOf("-course-details"));
      redirectURL = "/degrees/"+ courseName + "/" + collegeName + "/cd/" + rank_split[5]+"/"+rank_split[6]+"/";
      redirectURL = redirectURL.toLowerCase();
      res.setStatus(res.SC_MOVED_PERMANENTLY);
      res.setHeader("Location", redirectURL);
      res.setHeader("Connection", "close");
      return;
      }else{
        filterChain.doFilter(request, response);
      }
    }else {
      filterChain.doFilter(request, response);
    }
  }

  public void destroy() {
    this.filterConfig = null;
  }

 
}
