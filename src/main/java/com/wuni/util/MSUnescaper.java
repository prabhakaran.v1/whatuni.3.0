package com.wuni.util;

import java.nio.CharBuffer;

/**
  * @UniLanding
  * @author Amirtharaj
  * @version 1.0
  * @since 08_APR_2015
  * @purpose This program is return string without escape character. (Especially used when we need to print String using Jsp scriptlet tags)
  */
public class MSUnescaper {
  private static final char PERCENT = '%';
  private static final char NONSTANDARD_PCT_ESCAPE = 'u';
  public static String unescape(String input) {
    final StringBuilder sb = new StringBuilder(input.length());
    final CharBuffer buf = CharBuffer.wrap(input);
    char c;
    while (buf.hasRemaining()) {
      c = buf.get();
      if (c != PERCENT) {
        sb.append(c);
        continue;
      }
      if (!buf.hasRemaining())
        throw new IllegalArgumentException();
      c = buf.get();
      sb.append(c == NONSTANDARD_PCT_ESCAPE? msEscape(buf): standardEscape(buf, c));
    }
    return sb.toString();
  }
  private static char standardEscape(final CharBuffer buf, final char c) {
    if (!buf.hasRemaining())
      throw new IllegalArgumentException();
    final char[] array = { c, buf.get() };
    return (char)Integer.parseInt(new String(array), 16);
  }
  private static char msEscape(final CharBuffer buf) {
    if (buf.remaining() < 4)
      throw new IllegalArgumentException();
    final char[] array = new char[4];
    buf.get(array);
    return (char)Integer.parseInt(new String(array), 16);
  }
}
