package com.wuni.util.autocomplete;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.form.MobileUniBrowseBean;

import org.apache.commons.validator.GenericValidator;

import WUI.review.bean.UniBrowseBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.CommonValidator;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectAjaxVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectListVO;

/**
 * used for Autocomplete college name and course title.
 * Previouly these possible college/course names were populated by STATIC block
 * now these possible college/course names will be populated by dynamic DB call
 *
 * @author  Mohamed Syed
 * @since   wu316_20110823
 *
 */
public class AutoCompleteUtilities {

  /**
   * used for Autocomplete college name.
   * Previouly these possible college names were populated by STATIC block
   * now these possible college names will be populated by dynamic DB call
   *
   * @author  Mohamed Syed
   * @since   wu316_20110823
   *
   * @param freeText
   * @param response
   */
  public void populateCollegeNameAutoComplete(String freeText, String searchType, HttpServletResponse response) { //3_JUN_2014
    CommonValidator validate = new CommonValidator();
    CommonFunction comFn = new CommonFunction();
    if (validate.isBlankOrNull(freeText)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(freeText);
    parameters.add(searchType); //3_JUN_2014
    PrintWriter out = null;
    try {
      resultset = datamodel.getResultSet("wu_util_pkg.autocomplete_college_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCollegeId(resultset.getString("college_id"));
        autoCompleteVO.setCollegeName(resultset.getString("college_name"));
        autoCompleteVO.setCollegeNameAlias(resultset.getString("college_name_alias"));
        autoCompleteVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
        autoCompleteVO.setCollegeLocation(resultset.getString("college_location"));
        autoCompleteVO.setOpenDayMessage(resultset.getString("od_msg"));
        autoCompleteVO.setReviewMessage(resultset.getString("review_msg"));
        autoCompleteList.add(autoCompleteVO);
      }
      //
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          //Added condition for the university ajax for the read review page for Dec_18 by Sangeeth.S
          if("REVIEW_PAGE".equalsIgnoreCase(searchType)){
            if(autoCompleteVO.getReviewMessage() != null){
              out.println((autoCompleteVO.getCollegeId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span class=\"sub_titl opd_nav\">"+autoCompleteVO.getCollegeNameDisplay()+ "</span>" +"<span class=\"noopn\">"+autoCompleteVO.getReviewMessage()+"</span>" + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCollegeLocation() +"|")); 
            }
            else{
              out.println((autoCompleteVO.getCollegeId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span class=\"sub_titl opd_av\">"+autoCompleteVO.getCollegeNameDisplay()+ "</span>" + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCollegeLocation() +"|"));  
            }           
          }else if("topNavUni".equalsIgnoreCase(searchType)){
            out.println((autoCompleteVO.getCollegeId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span class=\"ajx_sub\" id=\"UNI_"+autoCompleteVO.getCollegeId()+"\"><span class=\"ajx_rt\"><span class=\"ajx_txt\">"+autoCompleteVO.getCollegeNameDisplay() +"</span></span></span>"+ GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCollegeLocation() + "|"));
          }else { //else condition for university ajax for the other page 
            out.println((autoCompleteVO.getCollegeId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCollegeNameDisplay() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCollegeLocation() + "|")); //03_FEB_15 modified by Amir for Review provider URL Forming
          }
          //
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      autoCompleteList.clear();
      autoCompleteList = null;
      if (autoCompleteVO != null) {
        autoCompleteVO.flush();
      }
      autoCompleteVO = null;
      parameters.clear();
      parameters = null;
      if (out != null) {
        out.close();
      }
      out = null;
      if (validate != null) {
        validate = null;
      }
    }
  }
  
    public void populateOpendaysCollegeNameAutoComplete(String freeText, String searchType, HttpServletResponse response) { //3_JUN_2014
      CommonValidator validate = new CommonValidator();
      CommonFunction comFn = new CommonFunction();
      if (validate.isBlankOrNull(freeText)) {
        validate = null;
        return;
      }
      //
      freeText = freeText.trim();
      DataModel datamodel = new DataModel();
      ResultSet resultset = null;
      List autoCompleteList = new ArrayList();
      AutoCompleteVO autoCompleteVO = null;
      Vector parameters = new Vector();
      parameters.add(freeText);
      parameters.add(searchType); //3_JUN_2014
      PrintWriter out = null;
      try {
        resultset = datamodel.getResultSet("wu_util_pkg.autocomplete_college_fn", parameters);
        while (resultset.next()) {
          autoCompleteVO = new AutoCompleteVO();
          autoCompleteVO.setCollegeId(resultset.getString("college_id"));
          autoCompleteVO.setOpenDayMessage(resultset.getString("od_msg"));
          autoCompleteVO.setCollegeName(resultset.getString("college_name"));
          autoCompleteVO.setCollegeNameAlias(resultset.getString("college_name_alias"));
          autoCompleteVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
          autoCompleteVO.setCollegeLocation(resultset.getString("college_location"));
          autoCompleteList.add(autoCompleteVO);
        }
        //
        response.setContentType("text/html");
        if (autoCompleteList.size() > 0) {
          out = response.getWriter();
          for (int i = 0; i < autoCompleteList.size(); i++) {
            autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);     
              if(autoCompleteVO.getOpenDayMessage() != null){
                  out.println((autoCompleteVO.getCollegeId() + "###" + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + "###" + "<span class=\"sub_titl opd_nav\">"+autoCompleteVO.getCollegeNameDisplay()+ "</span>" +"<span class=\"noopn\">"+autoCompleteVO.getOpenDayMessage()+"</span>" +"###" + autoCompleteVO.getCollegeLocation() +"|")); //03_FEB_15 modified by Amir for Review provider URL Forming
              }
              else{
                  out.println((autoCompleteVO.getCollegeId() + "###" + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getCollegeName()))) + "###" + "<span class=\"sub_titl opd_av\">"+autoCompleteVO.getCollegeNameDisplay()+ "</span>" +"###" + autoCompleteVO.getCollegeLocation() +"|")); //03_FEB_15 modified by Amir for Review provider URL Forming  
              }
            autoCompleteVO.flush();
            autoCompleteVO = null;
          }
        }
        //
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        //close opened IO-objcects, resultsets, and nullify unused objects.
        try {
          datamodel.closeCursor(resultset);
          if (resultset != null) {
            resultset.close();
          }
          resultset = null;
        } catch (Exception e) {
          e.printStackTrace();
        }
        //nullify unused objects
        datamodel = null;
        autoCompleteList.clear();
        autoCompleteList = null;
        if (autoCompleteVO != null) {
          autoCompleteVO.flush();
        }
        autoCompleteVO = null;
        parameters.clear();
        parameters = null;
        if (out != null) {
          out.close();
        }
        out = null;
        if (validate != null) {
          validate = null;
        }
      }
    }

  /**
   * used for Autocomplete college name and course title.
   * Previouly these possible course names were populated by STATIC block
   * now these possible course names will be populated by dynamic DB call
   *
   * @param freeText
   * @param collegeId
   * @param response
   */
  public void populateCourseTitleAutoComplete(String freeText, String collegeId, HttpServletResponse response) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(freeText) || validate.isBlankOrNullOrZero(collegeId)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(freeText);
    PrintWriter out = null;
    try {
      resultset = datamodel.getResultSet("wu_util_pkg.autocomplete_course_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCourseId(resultset.getString("course_id"));
        autoCompleteVO.setCourseTitle(resultset.getString("course_title"));
        autoCompleteVO.setCourseTitleDisplay(autoCompleteVO.getCourseTitle());
        autoCompleteVO.setCourseTitleAlias(autoCompleteVO.getCourseTitle());
        autoCompleteList.add(autoCompleteVO);
      }
      //
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCourseId() + "###" + autoCompleteVO.getCourseTitle() + "###" + autoCompleteVO.getCourseTitleDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      autoCompleteList.clear();
      autoCompleteList = null;
      if (autoCompleteVO != null) {
        autoCompleteVO.flush();
      }
      autoCompleteVO = null;
      parameters.clear();
      parameters = null;
      if (out != null) {
        out.close();
      }
      out = null;
      if (validate != null) {
        validate = null;
      }
    }
  }
  //

  /**
    * used for Autocomplete college name and course title.
    * Previouly these possible course names were populated by STATIC block
    * now these possible course names will be populated by dynamic DB call
    *
    * @param freeText
    * @param collegeId
    * @param response
    */
  public void populateCourseTitleAutoCompleteInMobile(String freeText, String collegeId, HttpServletResponse response) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(freeText) || validate.isBlankOrNullOrZero(collegeId)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(freeText);
    PrintWriter out = null;
    try {
      resultset = datamodel.getResultSet("WU_UTIL_PKG.ALEVEL_RESULTS_COURSE_FN", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCourseId(resultset.getString("course_id"));
        autoCompleteVO.setCourseTitle(resultset.getString("course_title"));
        autoCompleteVO.setCourseTitleDisplay(autoCompleteVO.getCourseTitle());
        autoCompleteVO.setCourseTitleAlias(autoCompleteVO.getCourseTitle());
        autoCompleteList.add(autoCompleteVO);
      }
      //
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCourseId() + "###" + autoCompleteVO.getCourseTitle() + "###" + autoCompleteVO.getCourseTitleDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      autoCompleteList.clear();
      autoCompleteList = null;
      if (autoCompleteVO != null) {
        autoCompleteVO.flush();
      }
      autoCompleteVO = null;
      parameters.clear();
      parameters = null;
      if (out != null) {
        out.close();
      }
      out = null;
      if (validate != null) {
        validate = null;
      }
    }
  }
  //

  /**
    *
    * Creadted for 21-Jan-2013
    * This is used to get the first university courses for mywhatuni and pg qlform , here we are displaying only UG courses.
    *
    * @param freeText
    * @param collegeId
    * @param response
    */
  public void firstUniversityCourses(String freeText, String collegeId, HttpServletResponse response) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(freeText) || validate.isBlankOrNullOrZero(collegeId)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(freeText);
    PrintWriter out = null;
    try {
      resultset = datamodel.getResultSet("wu_user_profile_pkg.get_ajax_list_ug_courses_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCourseId(resultset.getString("course_id"));
        autoCompleteVO.setCourseTitle(resultset.getString("course_title"));
        autoCompleteVO.setCourseTitleDisplay(autoCompleteVO.getCourseTitle());
        autoCompleteVO.setCourseTitleAlias(autoCompleteVO.getCourseTitle());
        autoCompleteList.add(autoCompleteVO);
      }
      //
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCourseId() + "###" + autoCompleteVO.getCourseTitle() + "###" + autoCompleteVO.getCourseTitleDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      autoCompleteList.clear();
      autoCompleteList = null;
      if (autoCompleteVO != null) {
        autoCompleteVO.flush();
      }
      autoCompleteVO = null;
      parameters.clear();
      parameters = null;
      if (out != null) {
        out.close();
      }
      out = null;
      if (validate != null) {
        validate = null;
      }
    }
  }
  //

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see courseOfInterestAutoComplete this method to get the suborder item related course list
   * @param mapping, form, request,response
   * @throws Exception
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void courseOfInterestAutoComplete(String freeText, String subOrderItemId, String collegeId, HttpServletResponse response) {
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(freeText);
    parameters.add(subOrderItemId);
    parameters.add(collegeId);
    try {
      resultset = datamodel.getResultSet("wu_util_pkg.get_related_course_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCourseId(resultset.getString("course_id"));
        autoCompleteVO.setCourseTitle(resultset.getString("course_title"));
        autoCompleteVO.setCourseTitleDisplay(autoCompleteVO.getCourseTitle());
        autoCompleteVO.setCourseTitleAlias(autoCompleteVO.getCourseTitle());
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCourseId() + "###" + autoCompleteVO.getCourseTitle() + "###" + autoCompleteVO.getCourseTitleDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the ajax list of ukschools
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void ukSchoolListAutoComplete(String freeText, HttpServletResponse response) {
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("wu_util_pkg.get_school_autocomplete_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setSchoolId(resultset.getString("school_id"));
        autoCompleteVO.setSchoolName(resultset.getString("school_name"));
        autoCompleteVO.setSchoolDisplayName(resultset.getString("school_display_name"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getSchoolId() + "###" + autoCompleteVO.getSchoolName() + "###" + autoCompleteVO.getSchoolDisplayName() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   *
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void keywordBrowseNodesMobile(String freeText, HttpServletResponse response, String qual, String siteFlag) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    List inputList = new ArrayList();
    inputList.add(freeText);
    inputList.add(qual);
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = null;
    String dbObject = "WU_UTIL_PKG.GET_SUBJECT_LIST_PRC"; //Added by Indumathi.S Nov-03-2015 Rel For clearing mobile objects.
    Map keywordBrowseNodes = commonBusiness.getKeywordBrowseNodes(inputList, dbObject);
    ArrayList autoCompleteList = (ArrayList)keywordBrowseNodes.get("pc_subject_list");
    String keywordmatchBrowseId = (String)keywordBrowseNodes.get("p_browse_cat_id");
    String keywordmatchBrowseNode = (String)keywordBrowseNodes.get("p_browse_category_code");
    String matchBrowseURl = "NOMATCH";
    String pagename = "PAGE"; //15_JUL_2014_REL
    if (qual.equals("CLEARING")) {
      qual = "M";
      pagename = "CLCOURSE";
    }
    if (!GenericValidator.isBlankOrNull(keywordmatchBrowseId)) {
      matchBrowseURl = new SeoUrls().construnctNewBrowseMoneyPageURL(qual, keywordmatchBrowseId, freeText, pagename);
    }
    try {
      out = response.getWriter();
      out.println(matchBrowseURl + "@@@");
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          String categoryURL = new SeoUrls().construnctNewBrowseMoneyPageURL(qual, autoCompleteVO.getBrowseCatId(), autoCompleteVO.getSubject(), pagename); //15_JUL_2014_REL
          out.println((autoCompleteVO.getBrowseCatId() + "###" + categoryURL + "###" + autoCompleteVO.getSubject() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void keywordBrowseNodesDesktop(String freeText, HttpServletResponse response, String qual, String siteFlag) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    List inputList = new ArrayList();
    inputList.add(freeText);
    inputList.add(qual);
    String excludeUni = "";
    if ("desktopsurvey".equals(siteFlag)) {
      excludeUni = "Y";
    }
    inputList.add(excludeUni);
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = null;
    String dbObject = "WHATUNI_SEARCH_PKG.get_subject_list_prc";
    Map keywordBrowseNodes = commonBusiness.getKeywordBrowseNodesDesktop(inputList);
    ArrayList autoCompleteList = (ArrayList)keywordBrowseNodes.get("pc_subject_list");
    String keywordmatchBrowseId = (String)keywordBrowseNodes.get("p_browse_cat_id");
    String keywordmatchBrowseNode = (String)keywordBrowseNodes.get("p_browse_category_code");
    String matchBrowseURl = "NOMATCH";
    String ucasCode = null;
    String qualCode = null;
    String ucasURl = "";
    if (!GenericValidator.isBlankOrNull(keywordmatchBrowseId)) {
      if (("CLEARING").equals(qual)) {
        matchBrowseURl = new SeoUrls().construnctNewBrowseMoneyPageURL("M", keywordmatchBrowseId, freeText, "CLCOURSE");
      } else {
        matchBrowseURl = new SeoUrls().construnctNewBrowseMoneyPageURL(qual, keywordmatchBrowseId, freeText, "PAGE");
      }
    }
    if (("UCAS_CODE").equals(qual)) {
      if ((!GenericValidator.isBlankOrNull(keywordmatchBrowseNode))) { //13_MAY_2014_RL
        String splitArr[] = keywordmatchBrowseNode.split(",");
        ucasCode = splitArr[0];
        qualCode = splitArr[1];
        ucasURl = splitArr[2];
        matchBrowseURl = ucasURl;
      } else {
        String ucasfreeText = new CommonFunction().replaceHypen(freeText);
        String ucasplusText = new CommonFunction().replacePlus(freeText);
        matchBrowseURl = GlobalConstants.WHATUNI_SCHEME_NAME + "www.whatuni.com/degree-courses/search?ucas-code=" + ucasfreeText;//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
      }
    }
    try {
      out = response.getWriter();
      out.println(matchBrowseURl + "@@@");
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          if ("desktopsurvey".equals(siteFlag)) { //Added by Priyaa for 21_JULY_2015_REL For Student Survey 
            out.println((autoCompleteVO.getBrowseCatId() + "###" + autoCompleteVO.getUrl() + "###" + autoCompleteVO.getSubject() + "###" + autoCompleteVO.getCatCode() + "|"));
          } else {
            out.println((autoCompleteVO.getBrowseCatId() + "###" + autoCompleteVO.getUrl() + "###" + autoCompleteVO.getSubject() + "###" + "" + "|"));
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the qualification subject of Student Ultimate Search.
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void ultimateQualSubject(String freeText, HttpServletResponse response, String qualification) {
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(qualification);
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("WU_ULTIMATE_SRCH_ENTRY_DET_PKG.GET_QUAL_SUBJECTS_FN", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setUlQualSubjectCode(resultset.getString("qual_subject_id"));
        autoCompleteVO.setUlQualSubjectName(resultset.getString("qual_subject_desc"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getUlQualSubjectCode() + "###" + autoCompleteVO.getUlQualSubjectName() + "###" + autoCompleteVO.getUlQualSubjectName() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * @see this method will return the qualification subject of Student Ultimate Search.
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     */
  public void universitySearchList(String freeText, HttpServletResponse response, String jacsCode) {
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(jacsCode);
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("wu_ultimate_search_util_pkg.get_college_list_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCollegeId(resultset.getString("college_id"));
        autoCompleteVO.setCollegeNameDisplay(resultset.getString("college_display_name"));
        autoCompleteVO.setCollegeName(resultset.getString("college_name"));
        autoCompleteVO.setUniUkprn(resultset.getString("ukprn"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCollegeId() + "###" + autoCompleteVO.getUniUkprn() + "###" + autoCompleteVO.getCollegeNameDisplay() + "###" + autoCompleteVO.getCollegeNameDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * @see this method will return the qualification subject of Student Ultimate Search.
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     */
  public void ultimateUniOfInterestQualSubject(String freeText, HttpServletResponse response) {
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("WU_ALEVEL_RESULTS_PKG.GET_AJAX_BRWOSE_NODE_FN", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setUniOfInterestDescription(resultset.getString("description"));
        autoCompleteVO.setUniOfInterestCategoryCode(resultset.getString("category_code"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getUniOfInterestCategoryCode() + "###" + autoCompleteVO.getUniOfInterestDescription() + "###" + autoCompleteVO.getUniOfInterestDescription() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the Jobs of Student Ultimate Search.
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void ultimateChooseJobOrIndustry(String freeText, HttpServletResponse response) {
    //Modifed by Indumathi.S Feb-16-2016 IWantToBe redesign
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("wu_ultimate_srch_entry_det_pkg.get_job_industry_list_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setUlJobFlag(resultset.getString("job_industry_flag"));
        autoCompleteVO.setUlJobCode(resultset.getString("job_industry_id"));
        autoCompleteVO.setUlJobDesc(resultset.getString("job_industry_name"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getUlJobCode() + "###" + autoCompleteVO.getUlJobFlag() + "###" + autoCompleteVO.getUlJobDesc() + "###" + autoCompleteVO.getUlJobDesc() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the ajax list of uni finder mobile
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void popualteCollegeNamesMobile(String freeText, HttpServletResponse response) {
    //
    PrintWriter out = null;
    MobileUniBrowseBean mblUniBrowseBean = new MobileUniBrowseBean();
    mblUniBrowseBean.setSearchText(freeText);
    mblUniBrowseBean.setOrderBy("AZ");
    mblUniBrowseBean.setPageNo("1");
    mblUniBrowseBean.setNetworkId("2");
    try {
      ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
      Map resultMap = searchBusiness.getMobileUniversityBrowseList(mblUniBrowseBean);
      if (resultMap != null) {
        ArrayList uniBrowseList = (ArrayList)resultMap.get("pc_getdata");
        if (uniBrowseList != null && uniBrowseList.size() > 0) {
          out = response.getWriter();
          Iterator autoCompleteItr = uniBrowseList.iterator();
          while (autoCompleteItr.hasNext()) {
            UniBrowseBean uniBean = (UniBrowseBean)autoCompleteItr.next();
            out.println((uniBean.getCollegeId() + "###" + uniBean.getCollegeNameUrl() + "###" + uniBean.getCollegeNameDisplay() + "###" + "" + "|"));
          }
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the ajax list of browse subjects and uni name
   * Added for 28_OCT_2014 Release Prospectus changes
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void prospectusBrowseNodesDesktop(String freeText, HttpServletResponse response, String qual) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    List inputList = new ArrayList();
    inputList.add(freeText);
    inputList.add(qual);
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = null;
    Map keywordBrowseNodes = commonBusiness.getAutoCompleteSubUniResults(inputList);
    ArrayList autoCompleteList = (ArrayList)keywordBrowseNodes.get("pc_subject_list");
    try {
      out = response.getWriter();
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getBrowseCatId() + "###" + autoCompleteVO.getUrl() + "###" + autoCompleteVO.getSubject() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * @see this method will return the ajax list of browse uni name and keyword flag
     * Added for 17_Mar_2015 Release Opendays browse
     * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     */
  public void opendaysBrowseNodesDesktop(String freeText, HttpServletResponse response, String qual) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    List inputList = new ArrayList();
    inputList.add(freeText);
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = null;
    Map opendaysKeywordBrowseNodes = commonBusiness.getOpendaysBrowseNodesDesktop(inputList);
    ArrayList autoCompleteList = (ArrayList)opendaysKeywordBrowseNodes.get("oc_opendays_list");
    String opendaysUrl = (String)opendaysKeywordBrowseNodes.get("o_opendays_url");
    String collegeName = (String)opendaysKeywordBrowseNodes.get("o_college_name");
    String collegeId = (String)opendaysKeywordBrowseNodes.get("o_college_id");
    String matchBrowseURl = "NOMATCH";
    if (!GenericValidator.isBlankOrNull(opendaysUrl)) {
      matchBrowseURl = opendaysUrl;
    } else {
      if (!GenericValidator.isBlankOrNull(freeText)) {
        String keywordFreeText = new CommonFunction().replaceWhiteSpaceToPlus(freeText);
        matchBrowseURl = "/open-days/search/?keyword=" + keywordFreeText;
      }
    }
    try {
      out = response.getWriter();
      out.println(matchBrowseURl + "@@@");
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          if(autoCompleteVO.getOpenDayMessage() != null){
            out.println((autoCompleteVO.getCollegeId() + "###" + autoCompleteVO.getOpendaysUrl() + "###" + autoCompleteVO.getCollegeNameDisplay()+"<span class=\"noopn\">"+autoCompleteVO.getOpenDayMessage()+"</sapn>"+ "###" + "" + "|"));
          }
          else {
            out.println((autoCompleteVO.getCollegeId() + "###" + autoCompleteVO.getOpendaysUrl() + "###" + autoCompleteVO.getCollegeNameDisplay()+ "###" + "" + "|"));
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * used for Autocomplete college name and course title.
   * Previouly these possible course names were populated by STATIC block
   * now these possible course names will be populated by dynamic DB call
   *
   * @param freeText
   * @param collegeId
   * @param response
   */
  public void populateSurveyCourseTitleAutoComplete(String freeText, String collegeId, HttpServletResponse response) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNull(freeText) || validate.isBlankOrNullOrZero(collegeId)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(freeText);
    PrintWriter out = null;
    try {
      resultset = datamodel.getResultSet("WU_ALEVEL_RESULTS_PKG.autocomplete_course_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setCourseId(resultset.getString("course_id"));
        autoCompleteVO.setCourseTitle(resultset.getString("course_title"));
        autoCompleteVO.setCourseTitleDisplay(autoCompleteVO.getCourseTitle());
        autoCompleteVO.setCourseTitleAlias(autoCompleteVO.getCourseTitle());
        autoCompleteList.add(autoCompleteVO);
      }
      //
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getCourseId() + "###" + autoCompleteVO.getCourseTitle() + "###" + autoCompleteVO.getCourseTitleDisplay() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      autoCompleteList.clear();
      autoCompleteList = null;
      if (autoCompleteVO != null) {
        autoCompleteVO.flush();
      }
      autoCompleteVO = null;
      parameters.clear();
      parameters = null;
      if (out != null) {
        out.close();
      }
      out = null;
      if (validate != null) {
        validate = null;
      }
    }
  }
  
  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see this method will return the qualification subject of Student Ultimate Search.
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void ucasQualSubject(String freeText, HttpServletResponse response) {    
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    PrintWriter out = null;
    List autoCompleteList = new ArrayList();
    AutoCompleteVO autoCompleteVO = null;
    Vector parameters = new Vector();    
    parameters.add(freeText);
    try {
      resultset = datamodel.getResultSet("wu_ucas_calculator_pkg.get_qual_subjects_fn", parameters);
      while (resultset.next()) {
        autoCompleteVO = new AutoCompleteVO();
        autoCompleteVO.setUlQualSubjectCode(resultset.getString("qual_subject_id"));
        autoCompleteVO.setUlQualSubjectName(resultset.getString("qual_subject_desc"));
        autoCompleteList.add(autoCompleteVO);
      }
      response.setContentType("text/html");
      if (autoCompleteList.size() > 0) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          out.println((autoCompleteVO.getUlQualSubjectCode() + "###" + autoCompleteVO.getUlQualSubjectName() + "###" + autoCompleteVO.getUlQualSubjectName() + "###" + "" + "|"));
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
  
  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   *
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void ucaskeywordBrowseNodes(String freeText, HttpServletResponse response, String qual, String siteFlag) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    List inputs = new ArrayList();
    inputs.add(freeText);
    inputs.add(qual);
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = null;    
    Map ucasKeywordBrowseNodes = commonBusiness.getUcasKeywordBrowseNodes(inputs);
    ArrayList autoCompleteList = (ArrayList)ucasKeywordBrowseNodes.get("PC_SUBJECT_LIST");
    String keywordmatchBrowseId = (String)ucasKeywordBrowseNodes.get("P_BROWSE_CAT_ID");    
    String matchBrowseURl = "NOMATCH";    
    if (!GenericValidator.isBlankOrNull(keywordmatchBrowseId)) {
      if (("CLEARING").equals(qual)) {
        matchBrowseURl = new SeoUrls().construnctNewBrowseMoneyPageURL("M", keywordmatchBrowseId, freeText, "CLCOURSE");
      } else {
        matchBrowseURl = new SeoUrls().construnctNewBrowseMoneyPageURL(qual, keywordmatchBrowseId, freeText, "PAGE");
      }
    }    
    try {
      out = response.getWriter();
      out.println(matchBrowseURl + "@@@");
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          if ("desktopsurvey".equals(siteFlag)) { //Added by Priyaa for 21_JULY_2015_REL For Student Survey 
            out.println((autoCompleteVO.getBrowseCatId() + "###" + autoCompleteVO.getUrl() + "###" + autoCompleteVO.getSubject() + "###" + autoCompleteVO.getCatCode() + "|"));
          } else {
            out.println((autoCompleteVO.getBrowseCatId() + "###" + autoCompleteVO.getUrl() + "###" + autoCompleteVO.getSubject() + "###" + "" + "|"));
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  
  /**
   * used for Autocomplete subject list.
   *
   * @author  Sabapathi S
   * @since   wu580_20180828
   *
   * @param freeText
   * @param response
   */
  public ArrayList populateInterstitialSubjectListAutoComplete(String freeText, String qualCode, String l1Flag, String parentCategoryId, String pageName, HttpServletResponse response) { //3_JUN_2014
    if ((GenericValidator.isBlankOrNull(freeText) || freeText.trim().length() < 3) && !("Y".equalsIgnoreCase(l1Flag))) {
      return null;
    }
    //
    freeText = freeText.trim();
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    ArrayList subjectList = new ArrayList();
    SubjectAjaxVO subjectAjaxVO = new SubjectAjaxVO();
    Map outMap = null;
    qualCode = GenericValidator.isBlankOrNull(qualCode) ? "M" : qualCode;
    subjectAjaxVO.setPageName(pageName);
    subjectAjaxVO.setL1Flag(l1Flag);
    subjectAjaxVO.setCategoryId(parentCategoryId);
    subjectAjaxVO.setKeywordText(freeText);
    subjectAjaxVO.setQualification(qualCode);
    String displaySubjectDesc = "";
    SubjectListVO subjectListVO = null;
    try {
      //
      outMap = commonBusiness.getInterstitialSubjectList(subjectAjaxVO);
      //
      StringBuffer returnString = new StringBuffer();
      subjectList = (ArrayList)outMap.get("RetVal");
      if("Y".equalsIgnoreCase(l1Flag)) {
        return subjectList;
      }
      // Added below to show the word searched by the user, 25_Sep_2018 By Sabapathi
      displaySubjectDesc = "<span class=\"ajx_sub"+""+"\"><span class=\"ajx_rt\"><span class=\"ajx_txt\"><a href=\"javascript:void(0);\" >KEYWORD SEARCH FOR <span class='bld' id='SUB_'>\""+freeText+"\"</span></a></span></span></span>";
      returnString.append("" + GlobalConstants.AUTOCOMPLETE_SPLITTER + "" + GlobalConstants.AUTOCOMPLETE_SPLITTER + "" + GlobalConstants.AUTOCOMPLETE_SPLITTER + displaySubjectDesc + GlobalConstants.AUTOCOMPLETE_SPLITTER + "" + "|");
      if (!CommonUtil.isBlankOrNull(subjectList)) {
        for (int i = 0; i < subjectList.size(); i++) {
          subjectListVO = (SubjectListVO)subjectList.get(i);
          displaySubjectDesc = "<span class=\"ajx_sub\" id=\"SUB_"+subjectListVO.getBrowseCategoryId()+"\"><span class=\"ajx_rt\"><span class=\"ajx_txt\">"+subjectListVO.getBrowseDescription()+"</span></span></span>";
          subjectListVO.setBrowseDescription(displaySubjectDesc);
          returnString.append(subjectListVO.getBrowseCategoryId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + subjectListVO.getUrlText() + GlobalConstants.AUTOCOMPLETE_SPLITTER + subjectListVO.getCategoryCode() + GlobalConstants.AUTOCOMPLETE_SPLITTER + subjectListVO.getBrowseDescription()+ GlobalConstants.AUTOCOMPLETE_SPLITTER + subjectListVO.getL2Flag() + "|");
        }
      }
      //
      response.setContentType("text/html");
      response.setHeader("Cache-Control", "no-cache");
      response.getWriter().println(returnString.toString());
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //nullify unused objects
      subjectAjaxVO = null;
      if(outMap != null) {
        outMap.clear();
        outMap = null;
      }
    }
    return null;
  }
  
  /**
   * @author Sangeeth.S
   * @description This method is used to get the recent search result list
   * @param userID
   * @since 7.5.18
   */
  public void getRecentSearchResultList(String navQual, String userID, HttpServletRequest request, HttpServletResponse response) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    
    PrintWriter out = null;
    AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
    autoCompleteVO.setUserId(userID);
    autoCompleteVO.setNavQual(navQual);
    autoCompleteVO.setTrackSessionId(new SessionData().getData(request, "userTrackId"));//p_track_session
    Map recentSearchResult = commonBusiness.getRecentSearchResultList(autoCompleteVO);
    ArrayList autoCompleteList = (ArrayList)recentSearchResult.get("PC_RECENT_SEARCH");  
    String optionDivID = "";       
    try {
      out = response.getWriter();
      out.println("NOMATCH@@@");
      if (autoCompleteList.size() > 0) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);          
          optionDivID = ("0".equals(autoCompleteVO.getOrd())) ? "SG" : ("6".equals(autoCompleteVO.getOrd())) ? "ADV" : autoCompleteVO.getBrowseCatId();
          //if("6".equals(autoCompleteVO.getOrd())){request.setAttribute("advSearchQual",autoCompleteVO.getUrl());}
          if(!(autoCompleteList.size() == 2 && ("0".equals(autoCompleteVO.getOrd())))){
            out.println(((!GenericValidator.isBlankOrNull(optionDivID) ? URLDecoder.decode(optionDivID, "UTF-8") : optionDivID) + "###" + (!GenericValidator.isBlankOrNull(autoCompleteVO.getUrl()) ? URLDecoder.decode(autoCompleteVO.getUrl(), "UTF-8") : autoCompleteVO.getUrl()) + "###" + (!GenericValidator.isBlankOrNull(autoCompleteVO.getDescription()) ? URLDecoder.decode(autoCompleteVO.getDescription(), "UTF-8") : autoCompleteVO.getDescription()) + "###" + "|"));          
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * @author Sangeeth.S
   * @description This method is used to get the review page subject search result list
   * @param freetext,searchtype
   * @since 28.11.18
   */
  public void populateReviewSubjectNameAutoComplete(String freeText, String searchType, HttpServletResponse response) { //3_JUN_2014
    CommonValidator validate = new CommonValidator();
    CommonFunction comFn = new CommonFunction();
    AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
    if (validate.isBlankOrNull(freeText)) {
      validate = null;
      return;
    }
    //
    freeText = freeText.trim();
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    PrintWriter out = null;    
    autoCompleteVO.setKeywordText(freeText);        
    Map recentSearchResult = commonBusiness.getReviewSubjectList(autoCompleteVO);
    ArrayList autoCompleteList = (ArrayList)recentSearchResult.get("PC_REVIEW_SUBJECT");          
    try {
      response.setContentType("text/html");
      out = response.getWriter();      
      if (!CommonUtil.isBlankOrNull(autoCompleteList)) {
        out = response.getWriter();
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);     
          if(autoCompleteVO.getReviewMessage() != null){
            out.println((autoCompleteVO.getCategoryCode() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getDescription()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span class=\"sub_titl opd_nav\">"+autoCompleteVO.getDescription()+ "</span>" + "<span class=\"noopn\">"+autoCompleteVO.getReviewMessage()+"</span>" +GlobalConstants.AUTOCOMPLETE_SPLITTER + "|")); 
          }
          else{
            out.println((autoCompleteVO.getCategoryCode() + GlobalConstants.AUTOCOMPLETE_SPLITTER + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(autoCompleteVO.getDescription()))) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span class=\"sub_titl opd_av\">"+autoCompleteVO.getDescription()+ "</span>" + GlobalConstants.AUTOCOMPLETE_SPLITTER + "|"));  
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }   
  }  

  /**
   * @author Hema.S
   * @description This method is used to get the course list for subject landing page
   * @param freetext,searchtype
   * @since 
   */
  public void populateCourseNameAutoComplete(String freeText, HttpServletResponse response) { 
      CommonValidator validate = new CommonValidator();
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      if (validate.isBlankOrNull(freeText)) {
        validate = null;
        return;
      }
      //
      freeText = freeText.trim();
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
      PrintWriter out = null;    
      autoCompleteVO.setKeywordText(freeText);        
      Map recentSearchResult = commonBusiness.getCourseList(autoCompleteVO);
      ArrayList autoCompleteList = (ArrayList)recentSearchResult.get("GET_AJAX_COURSE_LIST");
      try {
        response.setContentType("text/html");
        out = response.getWriter();
        if (!CommonUtil.isBlankOrNull(autoCompleteList)) {
          for (int i = 0; i < autoCompleteList.size(); i++) {
            autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i); 
          if(autoCompleteVO.getCourseMessage() != null){
                out.println((autoCompleteVO.getBrowseCatId() +GlobalConstants.AUTOCOMPLETE_SPLITTER +autoCompleteVO.getCoursePageURL() + GlobalConstants.AUTOCOMPLETE_SPLITTER + (autoCompleteVO.getDescription()) + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span id=\"cor_nav\" class=\"sub_titl cor_nav\">"+autoCompleteVO.getDescription() + "</span>" + "<span class=\"nocrse\">"+autoCompleteVO.getCourseMessage()+"</span>"+GlobalConstants.AUTOCOMPLETE_SPLITTER + "|"));  
              }
              else {
                out.println((autoCompleteVO.getBrowseCatId()+GlobalConstants.AUTOCOMPLETE_SPLITTER +autoCompleteVO.getCoursePageURL() + GlobalConstants.AUTOCOMPLETE_SPLITTER + (autoCompleteVO.getDescription() + GlobalConstants.AUTOCOMPLETE_SPLITTER + "<span id=\"cor_nav\" class=\"sub_titl cor_nav\">"+autoCompleteVO.getDescription()+ "</span>"+ GlobalConstants.AUTOCOMPLETE_SPLITTER + "|")));   
              }
              autoCompleteVO.flush();
              autoCompleteVO = null;
            
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }   
  }
  
  /**
   * @author Sangeeth.S
   * @description This method is used to get the top nav subject search popup 
   * @param freetext,qual
   * @since 30.09.19
   */
  public void subjectSearchTopNavAutoComplete(String freeText, HttpServletResponse response, String qual, String siteFlag) { //3_JUN_2014
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
    SeoUrls seoUrls = new SeoUrls();
    CommonFunction comFn = new CommonFunction();
    autoCompleteVO.setKeywordText(freeText);
    autoCompleteVO.setQualificationCode(qual);  
    PrintWriter out = null;
    //String ucasCode = null;
    //String qualCode = null;
    String ucasURl = "";
    Map topNavSubjectResult = commonBusiness.getTopNavSubjectList(autoCompleteVO);
    ArrayList autoCompleteList = (ArrayList)topNavSubjectResult.get("PC_GET_SUBJECT_LIST");
    String keywordmatchBrowseId = String.valueOf(topNavSubjectResult.get("P_BROWSE_CAT_ID"));
    String keywordmatchBrowseNode = (String)topNavSubjectResult.get("P_BROWSE_CATEGORY_CODE");
    String matchBrowseURl = "NOMATCH";  
    if (!GenericValidator.isBlankOrNull(keywordmatchBrowseId) && !"null".equalsIgnoreCase(keywordmatchBrowseId)) {
      if (("CLEARING").equals(qual)) {
        matchBrowseURl = seoUrls.construnctNewBrowseMoneyPageURL("M", keywordmatchBrowseId, freeText, "CLCOURSE");
      } else {
        matchBrowseURl = seoUrls.construnctNewBrowseMoneyPageURL(qual, keywordmatchBrowseId, freeText, "PAGE");
      }
    }
    
    try {
      out = response.getWriter();
      out.println(matchBrowseURl + "@@@");
      if (!CommonUtil.isBlankOrNull(autoCompleteList)) {
        for (int i = 0; i < autoCompleteList.size(); i++) {
          autoCompleteVO = (AutoCompleteVO)autoCompleteList.get(i);
          if ("desktopsurvey".equals(siteFlag)) {//need to check and remove
            out.println((autoCompleteVO.getBrowseCatId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getUrl() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getDescription() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getCategoryCode() + "|"));
          } else {
            out.println((autoCompleteVO.getBrowseCatId() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getUrl() + GlobalConstants.AUTOCOMPLETE_SPLITTER + autoCompleteVO.getDescription() + GlobalConstants.AUTOCOMPLETE_SPLITTER + "|"));
          }
          autoCompleteVO.flush();
          autoCompleteVO = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
