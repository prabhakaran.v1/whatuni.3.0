package com.wuni.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;

import WUI.utilities.CommonFunction;

import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;

/**
* Class         : ProviderURLRewriteFilter.java*
* Description   :*
* @version      : 1.0*
* Change Log :
* **************************************************************************************************************
* Author	       Ver 	   Created On       Modified On 	       Modification On    Details of change
* **************************************************************************************************************
* Thiyagu G     1.0    13_Dec_2016       First draft
*
*/
public class ProviderURLRewriteFilter implements Filter {
  private FilterConfig filterConfig = null;
  public ProviderURLRewriteFilter() {
  }
  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, 
                                                                                                         ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;  
    String url =  req.getRequestURI();    
    String redirectURL = null;    
    if (url.contains("/university-profile")) {
      String urlSplit[]=url.split("/");      
      String urlProviderName = urlSplit[3];
      String urlProviderId = urlSplit[4].replaceAll(".html", "");      
      String providerId = "";
      String providerName = "";
      CollegeNamesVO uniNames = null;
      CommonFunction common = new CommonFunction();
      uniNames = new CollegeUtilities().getCollegeNames(urlProviderId, req);
      if (uniNames != null) {
        providerId = uniNames.getCollegeId();
        providerName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(uniNames.getCollegeName().trim().toLowerCase())));        
      }      
      if(!GenericValidator.isBlankOrNull(urlProviderName) && !GenericValidator.isBlankOrNull(providerName)){
        if(!urlProviderName.equals(providerName)){
          redirectURL = "/university-profile/" + providerName + "/" + providerId +"/"; 
          res.setStatus(res.SC_MOVED_PERMANENTLY);
          res.setHeader("Location", redirectURL);
          res.setHeader("Connection", "close");
          return;
        }else {
          filterChain.doFilter(request, response);
        }           
      }else {
        filterChain.doFilter(request, response);
      }  
    } else {
      filterChain.doFilter(request, response);
    }  
  }
  public void destroy() {
    this.filterConfig = null;
  }  
}
