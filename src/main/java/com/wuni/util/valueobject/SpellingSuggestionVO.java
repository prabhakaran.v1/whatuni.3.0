package com.wuni.util.valueobject;

public class SpellingSuggestionVO {

  private String categoryId = "";
  private String categoryDesc = "";
  private String categoryCode = "";
  private String linkToBrowseMoneyPage = "";

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }

  public String getCategoryDesc() {
    return categoryDesc;
  }

  public void setLinkToBrowseMoneyPage(String linkToBrowseMoneyPage) {
    this.linkToBrowseMoneyPage = linkToBrowseMoneyPage;
  }

  public String getLinkToBrowseMoneyPage() {
    return linkToBrowseMoneyPage;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
}
