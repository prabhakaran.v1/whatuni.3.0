package com.wuni.util.valueobject.mywhatuni;

import lombok.Data;

@Data
public class MyWhatuniInputVO {
   //Added by Indumathi.S for passing my settings input values Jan-27-16 Rel
   private String userId = null;
   private String receiveNoEmailFlag = null;
   private String permitEmail = null;
   private String solusEmail = null;
   private String markettingEmail = null;
   private String privacy = null;
   private String userAttributeType = null;
   private String surveyMail = null;  //Added survey mail flag VO in mailing preferences for 31_May_2016, By Thiyagu G.
   private String caTrackFlag = null;  //Added cat privacy flag for 04_Oct_2016, By Thiyagu G.
   private String reviewSurveyFlag = null;
}
