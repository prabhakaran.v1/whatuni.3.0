package com.wuni.util.valueobject.mywhatuni;

public class MyprospectusPEVO {

  
  private String dateEnquired = null;
  private String enquiryType = null;
  private String interactionId = null;
  private String interactionType = null;
  
   private String collegeName = null;
   private String collegeDisplayName = null;
   private String collegeId = null;
   private String collegeLogo = null;
   private String orderItemId = null;
   private String subOrderItemId = null;
   private String myhcProfileId = null;
   private String profileId = null;
   private String profileType = null;
   private String advertName = null;
   private String cpeQualificationNetworkId = null;
   private String subOrderEmail = null;
   private String subOrderProspectus = null;
   private String subOrderWebsite = null;
   private String subOrderEmailWebform = null;
   private String subOrderProspectusWebform = null;
   private String totalCount = null;
   private String uniHomeUrl = null;
   private String dpFlag = null;
   private String dpURL = null;
   private String emailPrice = null;
   
   
  public void setDateEnquired(String dateEnquired) {
    this.dateEnquired = dateEnquired;
  }
  public String getDateEnquired() {
    return dateEnquired;
  }
  public void setEnquiryType(String enquiryType) {
    this.enquiryType = enquiryType;
  }
  public String getEnquiryType() {
    return enquiryType;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getProfileId() {
    return profileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }
  public String getAdvertName() {
    return advertName;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }
  public String getTotalCount() {
    return totalCount;
  }
  public void setUniHomeUrl(String uniHomeUrl) {
    this.uniHomeUrl = uniHomeUrl;
  }
  public String getUniHomeUrl() {
    return uniHomeUrl;
  }
  public void setInteractionId(String interactionId) {
    this.interactionId = interactionId;
  }
  public String getInteractionId() {
    return interactionId;
  }
  public void setInteractionType(String interactionType) {
    this.interactionType = interactionType;
  }
  public String getInteractionType() {
    return interactionType;
  }
  public void setDpFlag(String dpFlag) {
    this.dpFlag = dpFlag;
  }
  public String getDpFlag() {
    return dpFlag;
  }
  public void setDpURL(String dpURL) {
    this.dpURL = dpURL;
  }
  public String getDpURL() {
    return dpURL;
  }
  public void setEmailPrice(String emailPrice) {
    this.emailPrice = emailPrice;
  }
  public String getEmailPrice() {
    return emailPrice;
  }
}
