package com.wuni.util.valueobject.mywhatuni;

import java.util.List;
import lombok.Data;

@Data
public class MyWhatuniGetdataVO {
  //User Basic Data
  private String firstName = null;
  private String lastName = null;
  private String userName = null;
  private String addressLine1 = null;
  private String addressLine2 = null;
  private String town = null;
  private String postCode = null;
  private String phoneNumber = null;
  private String nationality = null;
  private String countryOfResidence = null;
  private String dateOfBirth = null;
  private String email = null;
  private String password = null;
  private String userPhoto = null;
  //User Attribute data
  private String attributeId = null;
  private String attributeName = null;
  private String defaultValue = null;
  private String attributeText = null;
  private String optionFlag = null;
  private List options = null;
  private String optionId = null;
  private String optionDesc = null;
  private String optionValue = null;
  private String attributeValueId = null;
  private String attributeValue = null;
  private String attrEnquiryType = null;
  private String actualAttrValue = null;
    
  
  //My Prospectus Data
  private String dateEnquired = null;
  private String enquiryType = null;
  //My Enquiries Data
  private String courseTitle = null;
  private String userContent = null;
  private String enquiredOn = null;
  private String courseId = null;
  private String studyLevelDesc = null;
  private String dpURL = null;
  
  //common properties.
   private String collegeName = null;
   private String collegeDisplayName = null;
   private String collegeId = null;
   private String collegeLogo = null;
   private String orderItemId = null;
   private String subOrderItemId = null;
   private String myhcProfileId = null;
   private String profileId = null;
   private String profileType = null;
   private String advertName = null;
   private String cpeQualificationNetworkId = null;
   private String subOrderEmail = null;
   private String subOrderProspectus = null;
   private String subOrderWebsite = null;
   private String subOrderEmailWebform = null;
   private String subOrderProspectusWebform = null;
   private String uniHomeUrl = null;
   private String totalCount = null;
   private String courseDetailsPageURL = null;
   private String interactionType = null;
   private String yeartoJoinCourse = null;
   private String personalNotes = null;
   private List enquiryGroupList = null;
   private List prospectusGroupList = null;
   private String inneerInteractionType = null;
   private String courseDeletedFlag = null;
   
   //Profile complete fields
   private String openDayCount = null;
   private String prospectusCount = null;
   private String enquiryCount = null;
   private String basketContentCount = null;
   private String profileComplete = null;
   //Added by Indumathi.S for Mailing preference Pod Jan-27-16 Rel
   private String userId = null;
   private String receiveNoEmailFlag = null;
   private String permitEmail = null;
   private String solusEmail = null;
   private String markettingEmail = null;
   private String surveyMail = null; //Added survey mail flag VO in mailing preferences for 31_May_2016, By Thiyagu G.
   private String reviewSurveyFlag = null; 

   private String defaultYoe = null;
  
}
