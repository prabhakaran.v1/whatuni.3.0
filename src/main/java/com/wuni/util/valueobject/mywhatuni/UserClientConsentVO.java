package com.wuni.util.valueobject.mywhatuni;

public class UserClientConsentVO {
  private String collegeName = null;
  private String collegeURL = null;
  private String userOptDate = null;
  public String getUserOptDate() {
	return userOptDate;
}
public void setUserOptDate(String userOptDate) {
	this.userOptDate = userOptDate;
}
public String getCollegeName() {
	return collegeName;
  }
  public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
  }
  public String getCollegeURL() {
	return collegeURL;
  }
  public void setCollegeURL(String collegeURL) {
	this.collegeURL = collegeURL;
  }
}
