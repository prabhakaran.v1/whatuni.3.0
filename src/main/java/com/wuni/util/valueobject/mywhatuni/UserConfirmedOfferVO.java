package com.wuni.util.valueobject.mywhatuni;

public class UserConfirmedOfferVO {
	
    private String userSubmittedDate;
    private String collegeId;
    private String collgeName;
    private String collegeDisplayName;
    private String collegeLogo;
    private String courseId;
    private String courseTitle;
    
	public String getUserSubmittedDate() {
		return userSubmittedDate;
	}
	public void setUserSubmittedDate(String userSubmittedDate) {
		this.userSubmittedDate = userSubmittedDate;
	}
	public String getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(String collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollgeName() {
		return collgeName;
	}
	public void setCollgeName(String collgeName) {
		this.collgeName = collgeName;
	}
	public String getCollegeDisplayName() {
		return collegeDisplayName;
	}
	public void setCollegeDisplayName(String collegeDisplayName) {
		this.collegeDisplayName = collegeDisplayName;
	}
	public String getCollegeLogo() {
		return collegeLogo;
	}
	public void setCollegeLogo(String collegeLogo) {
		this.collegeLogo = collegeLogo;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
    
    

}
