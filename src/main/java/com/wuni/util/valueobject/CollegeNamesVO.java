package com.wuni.util.valueobject;

public class CollegeNamesVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameAlias = null;
  private String collegeNameDisplay = null;
  private String uniHomeURL = null;

  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameAlias = null;
    this.collegeNameDisplay = null;
    this.uniHomeURL = null;
  }

  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameAlias = "";
    this.collegeNameDisplay = "";
    this.uniHomeURL = "";
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameAlias(String collegeNameAlias) {
    this.collegeNameAlias = collegeNameAlias;
  }

  public String getCollegeNameAlias() {
    return collegeNameAlias;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }

  public String getUniHomeURL() {
    return uniHomeURL;
  }

}
