package com.wuni.util.valueobject;
//wu414_2012073 Added By Sekhar K
public class ClearingFeaturedProvidersVO {
  private String catSponsorId = "";
  private String collegeId = "";
  private String collegeDisplayName = "";
  private String url = "";
  private String keyword = "";
  private String mediaPath = "";
  private String text = "";
  private String courseUrl = "";
  private String domainMediaPath = "";
  private String hotline = "";
  private String hotlineOriginal = "";
  private String collegeName = "";
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String CpeQualificationNetworkId = "";
  private String interactivityId = "";

  
  public void setCatSponsorId(String catSponsorId) {
    this.catSponsorId = catSponsorId;
  }

  public String getCatSponsorId() {
    return catSponsorId;
  }

  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }

  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCourseUrl(String courseUrl) {
    this.courseUrl = courseUrl;
  }

  public String getCourseUrl() {
    return courseUrl;
  }
  public void setDomainMediaPath(String domainMediaPath) {
    this.domainMediaPath = domainMediaPath;
  }
  public String getDomainMediaPath() {
    return domainMediaPath;
  }
  public void setHotline(String hotline) {
    this.hotline = hotline;
  }
  public String getHotline() {
    return hotline;
  }
  public void setHotlineOriginal(String hotlineOriginal) {
    this.hotlineOriginal = hotlineOriginal;
  }
  public String getHotlineOriginal() {
    return hotlineOriginal;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  
  public void setInteractivityId(String interactivityId) {
    this.interactivityId = interactivityId;
  }
  public String getInteractivityId() {
    return interactivityId;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.CpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return CpeQualificationNetworkId;
  }
}
