package com.wuni.util.valueobject;

public class LocationVO {
  private String latitude = null;
  private String longitude = null;
  private String college_name_display = null;
  private String address = null;
  private String headline = null; //19_Nov_2013_REL
  private String cityGuideLocation  = null;  
  private String cityGuideDispalyFlag = null; 
  private String addStr = null;//24_JUN_2014_REL
  private String articleCateName = null;
  private String articleID = null;
  private String articleTitleURL = null;
  
  public void init() {
    this.latitude = "";
    this.longitude = "";
    this.college_name_display = "";
    this.address = "";
  }
  public void flush() {
    this.latitude = null;
    this.longitude = null;
    this.college_name_display = null;
    this.address = null;
  }
  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }
  public String getLatitude() {
    return latitude;
  }
  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
  public String getLongitude() {
    return longitude;
  }
  public void setCollege_name_display(String college_name_display) {
    this.college_name_display = college_name_display;
  }
  public String getCollege_name_display() {
    return college_name_display;
  }
  public void setAddress(String address) {
    this.address = address;
  }
  public String getAddress() {
    return address;
  }
  public void setCityGuideDispalyFlag(String cityGuideDispalyFlag) {
    this.cityGuideDispalyFlag = cityGuideDispalyFlag;
  }
  public String getCityGuideDispalyFlag() {
    return cityGuideDispalyFlag;
  }
  public void setHeadline(String headline) {
    this.headline = headline;
  }
  public String getHeadline() {
    return headline;
  }
  public void setCityGuideLocation(String cityGuideLocation) {
    this.cityGuideLocation = cityGuideLocation;
  }
  public String getCityGuideLocation() {
    return cityGuideLocation;
  }
  public void setAddStr(String addStr) {
    this.addStr = addStr;
  }
  public String getAddStr() {
    return addStr;
  }
  public void setArticleCateName(String articleCateName) {
    this.articleCateName = articleCateName;
  }
  public String getArticleCateName() {
    return articleCateName;
  }
  public void setArticleID(String articleID) {
    this.articleID = articleID;
  }
  public String getArticleID() {
    return articleID;
  }
  public void setArticleTitleURL(String articleTitleURL) {
    this.articleTitleURL = articleTitleURL;
  }
  public String getArticleTitleURL() {
    return articleTitleURL;
  }
}
