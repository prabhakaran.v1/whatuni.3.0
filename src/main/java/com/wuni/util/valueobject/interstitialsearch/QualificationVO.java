package com.wuni.util.valueobject.interstitialsearch;

/**
  * @QualificationVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class QualificationVO {

  private String qualDesc;
  private String qualCode;
  private String urlText;

  public void setQualDesc(String qualDesc) {
    this.qualDesc = qualDesc;
  }

  public String getQualDesc() {
    return qualDesc;
  }

  public void setQualCode(String qualCode) {
    this.qualCode = qualCode;
  }

  public String getQualCode() {
    return qualCode;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

}
