package com.wuni.util.valueobject.interstitialsearch;

/**
  * @LocationTypeVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class LocationTypeVO {

  private String optionId;
  private String optionDesc;
  private String urlText;

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }

  public String getOptionDesc() {
    return optionDesc;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

}
