package com.wuni.util.valueobject.interstitialsearch;

/**
  * @AssessmentTypeVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class AssessmentTypeVO {

  private String assessmentTypeId;
  private String assessmentDisplayName;
  private String urlText;
  private String browseMoneyAssessedRefineUrl;

  public void setAssessmentTypeId(String assessmentTypeId) {
    this.assessmentTypeId = assessmentTypeId;
  }

  public String getAssessmentTypeId() {
    return assessmentTypeId;
  }

  public void setAssessmentDisplayName(String assessmentDisplayName) {
    this.assessmentDisplayName = assessmentDisplayName;
  }

  public String getAssessmentDisplayName() {
    return assessmentDisplayName;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

  public void setBrowseMoneyAssessedRefineUrl(String browseMoneyAssessedRefineUrl) {
    this.browseMoneyAssessedRefineUrl = browseMoneyAssessedRefineUrl;
  }

  public String getBrowseMoneyAssessedRefineUrl() {
    return browseMoneyAssessedRefineUrl;
  }

}
