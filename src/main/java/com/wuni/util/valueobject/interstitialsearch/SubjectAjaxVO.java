package com.wuni.util.valueobject.interstitialsearch;

/**
  * @SubjectAjaxVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class SubjectAjaxVO {

  private String pageName;
  private String keywordText;
  private String qualification;
  private String categoryId;
  private String l1Flag;

  public void setKeywordText(String keywordText) {
    this.keywordText = keywordText;
  }

  public String getKeywordText() {
    return keywordText;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setL1Flag(String l1Flag) {
    this.l1Flag = l1Flag;
  }

  public String getL1Flag() {
    return l1Flag;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public String getPageName() {
    return pageName;
  }

}
