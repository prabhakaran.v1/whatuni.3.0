package com.wuni.util.valueobject.interstitialsearch;

import java.util.List;

/**
  * @GardeFilterVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class GardeFilterVO {

  private String qualification;
  private String gradeStr;
  private List gradesList;
  private String gradeLevel;
  private String urlText;
  private String entry_qual_id;
  private String entry_qualification;
  private String entry_subject;
  private String entry_grade;
  private String qual_level;
  private String entry_old_grade;
  private String parent_qualification;

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setGradeStr(String gradeStr) {
    this.gradeStr = gradeStr;
  }

  public String getGradeStr() {
    return gradeStr;
  }

  public void setGradeLevel(String gradeLevel) {
    this.gradeLevel = gradeLevel;
  }

  public String getGradeLevel() {
    return gradeLevel;
  }

  public void setGradesList(List gradesList) {
    this.gradesList = gradesList;
  }

  public List getGradesList() {
    return gradesList;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

  public String getEntry_qual_id() {
	return entry_qual_id;
  }

  public void setEntry_qual_id(String entry_qual_id) {
	this.entry_qual_id = entry_qual_id;
  }

  public String getEntry_qualification() {
	return entry_qualification;
  }

  public void setEntry_qualification(String entry_qualification) {
	this.entry_qualification = entry_qualification;
  }

  public String getEntry_subject() {
	return entry_subject;
  }

  public void setEntry_subject(String entry_subject) {
	this.entry_subject = entry_subject;
  }

  public String getEntry_grade() {
	return entry_grade;
  }

  public void setEntry_grade(String entry_grade) {
	this.entry_grade = entry_grade;
  }

  public String getQual_level() {
	return qual_level;
  }

  public void setQual_level(String qual_level) {
	this.qual_level = qual_level;
  }

  public String getEntry_old_grade() {
	return entry_old_grade;
  }

  public void setEntry_old_grade(String entry_old_grade) {
	this.entry_old_grade = entry_old_grade;
  }

  public String getParent_qualification() {
	return parent_qualification;
  }

  public void setParent_qualification(String parent_qualification) {
	this.parent_qualification = parent_qualification;
  }
  
}
