package com.wuni.util.valueobject.interstitialsearch;

/**
  * @StudymodeVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class StudymodeVO {

  private String studymodeDesc;
  private String urlText;

  public void setStudymodeDesc(String studymodeDesc) {
    this.studymodeDesc = studymodeDesc;
  }

  public String getStudymodeDesc() {
    return studymodeDesc;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

}
