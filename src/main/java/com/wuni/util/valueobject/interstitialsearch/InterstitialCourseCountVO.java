package com.wuni.util.valueobject.interstitialsearch;

import java.util.ArrayList;

import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;

/**
  * @InterstitialCourseCountVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class InterstitialCourseCountVO {

  private String pageName;
  private String qualification;
  private String searchKeyword;
  private String searchCategory;
  private String townCity;
  private String locationTypeString;
  private String studymode;
  private String entryLevel;
  private String entryPoints;
  private String assessmentType;
  private String reviewCategory;
  private String jacsCode;
  //Added for Advance search grade filter changes - Jeya 19th Nov 2019
  private Object[][] qualDetailsArr = null;
  private ArrayList<GradeFilterValidationVO> qualSubList;

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public String getPageName() {
    return pageName;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setSearchCategory(String searchCategory) {
    this.searchCategory = searchCategory;
  }

  public String getSearchCategory() {
    return searchCategory;
  }

  public void setTownCity(String townCity) {
    this.townCity = townCity;
  }

  public String getTownCity() {
    return townCity;
  }

  public void setLocationTypeString(String locationTypeString) {
    this.locationTypeString = locationTypeString;
  }

  public String getLocationTypeString() {
    return locationTypeString;
  }

  public void setStudymode(String studymode) {
    this.studymode = studymode;
  }

  public String getStudymode() {
    return studymode;
  }

  public void setEntryLevel(String entryLevel) {
    this.entryLevel = entryLevel;
  }

  public String getEntryLevel() {
    return entryLevel;
  }

  public void setEntryPoints(String entryPoints) {
    this.entryPoints = entryPoints;
  }

  public String getEntryPoints() {
    return entryPoints;
  }

  public void setAssessmentType(String assessmentType) {
    this.assessmentType = assessmentType;
  }

  public String getAssessmentType() {
    return assessmentType;
  }
  
  public void setReviewCategory(String reviewCategory) {
    this.reviewCategory = reviewCategory;
  }

  public String getReviewCategory() {
    return reviewCategory;
  }

  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }

  public String getSearchKeyword() {
    return searchKeyword;
  }

  public void setJacsCode(String jacsCode) {
    this.jacsCode = jacsCode;
  }

  public String getJacsCode() {
    return jacsCode;
  }
  
  public void setQualDetailsArr(Object[][] qualDetailsArr) {
    this.qualDetailsArr = qualDetailsArr;
  }

  public Object[][] getQualDetailsArr() {
	return qualDetailsArr;
  }

  public void setQualSubList(ArrayList<GradeFilterValidationVO> qualSubList) {
	this.qualSubList = qualSubList;
  }

  public ArrayList<GradeFilterValidationVO> getQualSubList() {
	return qualSubList;
  }
}
