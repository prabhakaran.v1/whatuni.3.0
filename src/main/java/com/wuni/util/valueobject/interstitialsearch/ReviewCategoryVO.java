package com.wuni.util.valueobject.interstitialsearch;

/**
  * @ReviewCategoryVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class ReviewCategoryVO {

  private String reviewCategoryId;
  private String reviewCategoryDisplayName;
  private String urlText;
  private String browseMoneyReviewCatRefineUrl;

  public void setReviewCategoryId(String reviewCategoryId) {
    this.reviewCategoryId = reviewCategoryId;
  }

  public String getReviewCategoryId() {
    return reviewCategoryId;
  }

  public void setReviewCategoryDisplayName(String reviewCategoryDisplayName) {
    this.reviewCategoryDisplayName = reviewCategoryDisplayName;
  }

  public String getReviewCategoryDisplayName() {
    return reviewCategoryDisplayName;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

  public void setBrowseMoneyReviewCatRefineUrl(String browseMoneyReviewCatRefineUrl) {
    this.browseMoneyReviewCatRefineUrl = browseMoneyReviewCatRefineUrl;
  }

  public String getBrowseMoneyReviewCatRefineUrl() {
    return browseMoneyReviewCatRefineUrl;
  }

}
