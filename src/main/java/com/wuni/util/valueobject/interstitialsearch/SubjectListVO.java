package com.wuni.util.valueobject.interstitialsearch;

/**
  * @SubjectListVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class SubjectListVO {

  private String browseDescription;
  private String browseCategoryId;
  private String categoryCode;
  private String orderSequence;
  private String urlText;
  private String l2Flag;

  public void setBrowseDescription(String browseDescription) {
    this.browseDescription = browseDescription;
  }

  public String getBrowseDescription() {
    return browseDescription;
  }

  public void setBrowseCategoryId(String browseCategoryId) {
    this.browseCategoryId = browseCategoryId;
  }

  public String getBrowseCategoryId() {
    return browseCategoryId;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setOrderSequence(String orderSequence) {
    this.orderSequence = orderSequence;
  }

  public String getOrderSequence() {
    return orderSequence;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

  public void setL2Flag(String l2Flag) {
    this.l2Flag = l2Flag;
  }

  public String getL2Flag() {
    return l2Flag;
  }

}
