package com.wuni.util.valueobject.interstitialsearch;

/**
  * @RegionVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class RegionVO {

  private String regionName;
  private String urlText;

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setUrlText(String urlText) {
    this.urlText = urlText;
  }

  public String getUrlText() {
    return urlText;
  }

}
