package com.wuni.util.valueobject.interstitialsearch;

/**
  * @InterstitialSearchVO.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : ValueObject to hold Advance search related data
  */
public class InterstitialSearchVO {

  private String pageName;
  private String defaultQualSelected;
  private String userId;
  private String subjectSessionId;

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public String getPageName() {
    return pageName;
  }

  public void setDefaultQualSelected(String defaultQualSelected) {
    this.defaultQualSelected = defaultQualSelected;
  }

  public String getDefaultQualSelected() {
    return defaultQualSelected;
  }

  public String getUserId() {
	return userId;
  }

  public void setUserId(String userId) {
	this.userId = userId;
  }

  public String getSubjectSessionId() {
	return subjectSessionId;
  }

  public void setSubjectSessionId(String subjectSessionId) {
	this.subjectSessionId = subjectSessionId;
  }
  
}
