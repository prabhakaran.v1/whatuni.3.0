package com.wuni.util.valueobject;
//wu411_20120612
public class UniversityDPPodVo {
    private String subOrderItemId="";
    private String mediaPath="";
    private String dpFlag = "";
    private String dpteasertext = null;
    private String prospectusType = "";

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setDpFlag(String dpFlag) {
        this.dpFlag = dpFlag;
    }

    public String getDpFlag() {
        return dpFlag;
    }
    
  public void setDpteasertext(String dpteasertext) {
      this.dpteasertext = dpteasertext;
  }

  public String getDpteasertext() {
      return dpteasertext;
  }

    public void setProspectusType(String prospectusType) {
        this.prospectusType = prospectusType;
    }

    public String getProspectusType() {
        return prospectusType;
    }
}
