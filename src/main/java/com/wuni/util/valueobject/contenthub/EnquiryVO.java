package com.wuni.util.valueobject.contenthub;

public class EnquiryVO {
  private String orderItemId = null;
  private String email = null;
  private String emailWebform = null;
  private String prospectus = null;
  private String prospectusWebform = null;
  private String website = null;
  private String subOrderItemId = null;
  private String myhcProfileId = null;
  private String profileType = null;
  private String networkId = null;
  private String hotline = null;
  private String advertName = null;
  private String websitePrice = null;
  private String webformPrice = null;
  private String emailFlag = null;
  private String prospectusFlag = null;
  private String websiteFlag = null;
  private String emailWebFormFlag = null;
  
  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setEmailWebform(String emailWebform) {
    this.emailWebform = emailWebform;
  }

  public String getEmailWebform() {
    return emailWebform;
  }

  public void setProspectus(String prospectus) {
    this.prospectus = prospectus;
  }

  public String getProspectus() {
    return prospectus;
  }

  public void setProspectusWebform(String prospectusWebform) {
    this.prospectusWebform = prospectusWebform;
  }

  public String getProspectusWebform() {
    return prospectusWebform;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getWebsite() {
    return website;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public void setHotline(String hotline) {
    this.hotline = hotline;
  }

  public String getHotline() {
    return hotline;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setEmailFlag(String emailFlag) {
    this.emailFlag = emailFlag;
  }

  public String getEmailFlag() {
    return emailFlag;
  }

  public void setProspectusFlag(String prospectusFlag) {
    this.prospectusFlag = prospectusFlag;
  }

  public String getProspectusFlag() {
    return prospectusFlag;
  }

  public void setWebsiteFlag(String websiteFlag) {
    this.websiteFlag = websiteFlag;
  }

  public String getWebsiteFlag() {
    return websiteFlag;
  }

  public void setEmailWebFormFlag(String emailWebFormFlag) {
    this.emailWebFormFlag = emailWebFormFlag;
  }

  public String getEmailWebFormFlag() {
    return emailWebFormFlag;
  }

}
