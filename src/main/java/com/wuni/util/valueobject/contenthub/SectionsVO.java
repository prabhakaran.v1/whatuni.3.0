package com.wuni.util.valueobject.contenthub;

public class SectionsVO {

  private String collegeId = null;
  private String description = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String mediaId = null;
  private String mediaTypeId = null;
  private String mediaType = null;
  private String imagePath = null;
  private String sectionNameDisplay = null;
  private String sectionName = null;
  private String profileId = null;
  private String myhcProfileId = null;
  private String wuscaRank = null;
  private String wuscaOverall = null;
  private String mediaName = null;
  private String thumbnailPath = null;
  private String awardImage = null;
  private String subOrderItemId = null;
  private String displaySectionName = null;

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }  

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setWuscaRank(String wuscaRank) {
    this.wuscaRank = wuscaRank;
  }

  public String getWuscaRank() {
    return wuscaRank;
  }

  public void setWuscaOverall(String wuscaOverall) {
    this.wuscaOverall = wuscaOverall;
  }

  public String getWuscaOverall() {
    return wuscaOverall;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setSectionNameDisplay(String sectionNameDisplay) {
    this.sectionNameDisplay = sectionNameDisplay;
  }

  public String getSectionNameDisplay() {
    return sectionNameDisplay;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public String getSectionName() {
    return sectionName;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setMediaName(String mediaName) {
    this.mediaName = mediaName;
  }

  public String getMediaName() {
    return mediaName;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

    public void setAwardImage(String awardImage) {
        this.awardImage = awardImage;
    }

    public String getAwardImage() {
        return awardImage;
    }

  public void setThumbnailPath(String thumbnailPath) {
    this.thumbnailPath = thumbnailPath;
  }

  public String getThumbnailPath() {
    return thumbnailPath;
  }


  public String getSubOrderItemId() {
   return subOrderItemId;
  }


  public void setSubOrderItemId(String subOrderItemId) {
   this.subOrderItemId = subOrderItemId;
  }

  public String getDisplaySectionName() {
	return displaySectionName;
  }
  public void setDisplaySectionName(String displaySectionName) {
	this.displaySectionName = displaySectionName;
  }

}
