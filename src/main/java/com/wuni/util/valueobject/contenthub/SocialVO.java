package com.wuni.util.valueobject.contenthub;

public class SocialVO {

  private String socialNetworkName = null;
  private String socialNetworkUrl = null;

  public void setSocialNetworkName(String socialNetworkName) {
    this.socialNetworkName = socialNetworkName;
  }

  public String getSocialNetworkName() {
    return socialNetworkName;
  }

  public void setSocialNetworkUrl(String socialNetworkUrl) {
    this.socialNetworkUrl = socialNetworkUrl;
  }

  public String getSocialNetworkUrl() {
    return socialNetworkUrl;
  }
}
