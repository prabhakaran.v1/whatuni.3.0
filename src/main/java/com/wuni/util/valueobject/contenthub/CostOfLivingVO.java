package com.wuni.util.valueobject.contenthub;

public class CostOfLivingVO {

  private String accomodationLower;
  private String accomodationUpper;
  private String livingCost;
  private String accomLowerBarLimit;
  private String accomUpperBarLimit;
  private String livingBarLimit;
  private String costPint;
  private String accomToolTipFlag;

  public void setAccomodationLower(String accomodationLower) {
    this.accomodationLower = accomodationLower;
  }

  public String getAccomodationLower() {
    return accomodationLower;
  }

  public void setAccomodationUpper(String accomodationUpper) {
    this.accomodationUpper = accomodationUpper;
  }

  public String getAccomodationUpper() {
    return accomodationUpper;
  }

  public void setLivingCost(String livingCost) {
    this.livingCost = livingCost;
  }

  public String getLivingCost() {
    return livingCost;
  }

  public void setAccomLowerBarLimit(String accomLowerBarLimit) {
    this.accomLowerBarLimit = accomLowerBarLimit;
  }

  public String getAccomLowerBarLimit() {
    return accomLowerBarLimit;
  }

  public void setAccomUpperBarLimit(String accomUpperBarLimit) {
    this.accomUpperBarLimit = accomUpperBarLimit;
  }

  public String getAccomUpperBarLimit() {
    return accomUpperBarLimit;
  }

  public void setLivingBarLimit(String livingBarLimit) {
    this.livingBarLimit = livingBarLimit;
  }

  public String getLivingBarLimit() {
    return livingBarLimit;
  }

  public void setCostPint(String costPint) {
    this.costPint = costPint;
  }

  public String getCostPint() {
    return costPint;
  }

  public void setAccomToolTipFlag(String accomToolTipFlag) {
    this.accomToolTipFlag = accomToolTipFlag;
  }

  public String getAccomToolTipFlag() {
    return accomToolTipFlag;
  }

}
