package com.wuni.util.valueobject.contenthub;

public class StudentStatsVO {

  private String customizedFlag;
  private String statsDescription;
  private String statsValue1;
  private String statsValue2; 

  public void setCustomizedFlag(String customizedFlag) {
    this.customizedFlag = customizedFlag;
  }

  public String getCustomizedFlag() {
    return customizedFlag;
  }

  public void setStatsDescription(String statsDescription) {
    this.statsDescription = statsDescription;
  }

  public String getStatsDescription() {
    return statsDescription;
  }

  public void setStatsValue1(String statsValue1) {
    this.statsValue1 = statsValue1;
  }

  public String getStatsValue1() {
    return statsValue1;
  }

  public void setStatsValue2(String statsValue2) {
    this.statsValue2 = statsValue2;
  }

  public String getStatsValue2() {
    return statsValue2;
  }

}
