package com.wuni.util.valueobject.contenthub;

public class GalleryVO {

  private String mediaId;
  private String mediaPath;
  private String mediaTypeId;
  private String richFlag;
  private String thumbNailPath;
  private String mediaName;
  private String sectionId = null;
  private String promotionalText = null;

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setRichFlag(String richFlag) {
    this.richFlag = richFlag;
  }

  public String getRichFlag() {
    return richFlag;
  }

  public void setThumbNailPath(String thumbNailPath) {
    this.thumbNailPath = thumbNailPath;
  }

  public String getThumbNailPath() {
    return thumbNailPath;
  }

  public void setMediaName(String mediaName) {
    this.mediaName = mediaName;
  }

  public String getMediaName() {
    return mediaName;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }

  public String getSectionId() {
    return sectionId;
  }

    public void setPromotionalText(String promotionalText) {
        this.promotionalText = promotionalText;
    }

    public String getPromotionalText() {
        return promotionalText;
    }
}
