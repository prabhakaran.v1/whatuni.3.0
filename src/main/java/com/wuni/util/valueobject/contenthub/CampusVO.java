package com.wuni.util.valueobject.contenthub;

public class CampusVO {

  private String venueId = null;
  private String venueName = null;
  private String latitude = null;
  private String longitude = null;
  private String mediaId = null;
  private String mediaPath = null;
  private String mediaTypeId = null;
  private String thumbNailPath = null;
  private String onlineFlag = null;
  private String mediaName = null;
  private String sectionId = null;
  private String promotionalText = null;
  private String addressOne = null;
  private String addressTwo = null;
  private String countryState = null;
  private String postcode = null;

  public void setVenueId(String venueId) {
    this.venueId = venueId;
  }

  public String getVenueId() {
    return venueId;
  }

  public void setVenueName(String venueName) {
    this.venueName = venueName;
  }

  public String getVenueName() {
    return venueName;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setThumbNailPath(String thumbNailPath) {
    this.thumbNailPath = thumbNailPath;
  }

  public String getThumbNailPath() {
    return thumbNailPath;
  }

  public void setOnlineFlag(String onlineFlag) {
    this.onlineFlag = onlineFlag;
  }

  public String getOnlineFlag() {
    return onlineFlag;
  }

  public void setMediaName(String mediaName) {
    this.mediaName = mediaName;
  }

  public String getMediaName() {
    return mediaName;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }

  public String getSectionId() {
    return sectionId;
  }

    public void setPromotionalText(String promotionalText) {
        this.promotionalText = promotionalText;
    }

    public String getPromotionalText() {
        return promotionalText;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setCountryState(String countryState) {
        this.countryState = countryState;
    }

    public String getCountryState() {
        return countryState;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }
}
