package com.wuni.util.valueobject.contenthub;

public class UserDetailsVO {
 
  private String firstName = "";
  private String lastName = "";
  private String postCode = "";
  private String emailAddress = "";
  private String dateOfBirth = "";
  private String yeartoJoinCourse = "";
  private String day = "";
  private String month = "";
  private String year = "";
  
  private String marketingEmail = "N";
  private String solusEmail = "N";
  private String surveyEmail = "N"; 
  
  private String defaultYoe = "";
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }

  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }

  public void setDay(String day) {
    this.day = day;
  }

  public String getDay() {
    return day;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public String getMonth() {
    return month;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getYear() {
    return year;
  }
  public void setMarketingEmail(String marketingEmail) {
    this.marketingEmail = marketingEmail;
  }
  public String getMarketingEmail() {
    return marketingEmail;
  }
  public void setSolusEmail(String solusEmail) {
    this.solusEmail = solusEmail;
  }
  public String getSolusEmail() {
    return solusEmail;
  }
  public void setSurveyEmail(String surveyEmail) {
    this.surveyEmail = surveyEmail;
  }
  public String getSurveyEmail() {
    return surveyEmail;
  }

  public String getDefaultYoe() {
	return defaultYoe;
  }

  public void setDefaultYoe(String defaultYoe) {
	this.defaultYoe = defaultYoe;
  }
  
  
}
