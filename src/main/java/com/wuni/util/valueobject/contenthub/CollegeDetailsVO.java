package com.wuni.util.valueobject.contenthub;

public class CollegeDetailsVO {
 
  private String collegeId = null;  
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String reviewCount = null;
  private String overAllRating = null;
  private String collegeLogo = null;
  private String absoluteRating = null;

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }

  public String getOverAllRating() {
    return overAllRating;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setAbsoluteRating(String absoluteRating) {
    this.absoluteRating = absoluteRating;
  }

  public String getAbsoluteRating() {
    return absoluteRating;
  }

}
