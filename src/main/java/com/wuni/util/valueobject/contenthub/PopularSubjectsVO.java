package com.wuni.util.valueobject.contenthub;

public class PopularSubjectsVO {
  
  String subjectId = null;
  String subjectName = null;
  String imageSubjectName = null;
  String browseTextKey = null;
  String collegeId = null;
  String collegeName = null;
  String subjectOrderItemId = null;
  String subjectMyhcProfileId = null;
  String popularSubjectURL = null;
  String imageName = null;
  String advertName = null;
  
  String subjectCat = null;
  

  public void setImageSubjectName(String imageSubjectName) {
    this.imageSubjectName = imageSubjectName;
  }

  public String getImageSubjectName() {
    return imageSubjectName;
  }

  public void setBrowseTextKey(String browseTextKey) {
    this.browseTextKey = browseTextKey;
  }

  public String getBrowseTextKey() {
    return browseTextKey;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectOrderItemId(String subjectOrderItemId) {
    this.subjectOrderItemId = subjectOrderItemId;
  }

  public String getSubjectOrderItemId() {
    return subjectOrderItemId;
  }

  public void setSubjectMyhcProfileId(String subjectMyhcProfileId) {
    this.subjectMyhcProfileId = subjectMyhcProfileId;
  }

  public String getSubjectMyhcProfileId() {
    return subjectMyhcProfileId;
  }

  public void setPopularSubjectURL(String popularSubjectURL) {
    this.popularSubjectURL = popularSubjectURL;
  }

  public String getPopularSubjectURL() {
    return popularSubjectURL;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageName() {
    return imageName;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }
  public void setSubjectCat(String subjectCat) {
    this.subjectCat = subjectCat;
  }
  public String getSubjectCat() {
    return subjectCat;
  }
}
