package com.wuni.util.valueobject.contenthub;

public class OpendayVO {

  private String openDate;
  private String timing;
  private String openDayDesc;
  private String suborderItemId;
  private String eventId;
  private String openDayFlag;
  private String mediaPath;
  private String qualType;
  private String bookingUrl;
  private String collegeId;
  
  private String collegeLogo;
  private String collegeDispName;
  private String startDate;
  private String eventCalItemId;
  private String eventDesp;
  private String startTime;
  private String endTime;
  private String venue;
  private String qualId;
  private String qualDesc;
  private String networkId;
  private String websitePrice;
  private String monthYear;
  private String openday;
  
  private String opendayDetail;
  private String selectedTxt;
  
  private String headline;
  private String collegeLocation;
  private String opendayDescription;
  
  private String collegeName;
  private String opendaysProviderURL;
  private String opendayVenue;
  private String opendayDesc;
  private String endDate;
  private String collegeNameDisplay;  
  private String qualCode;
  private String stOpenDayStartDate; 
  private String stOpenDayEndDate;
  private String eventCategoryName;
  private String eventCategoryNameGa;
  private String eventCategoryId;
  private String totalOpenDays;
  
  
  
  public String getTotalOpenDays() {
	return totalOpenDays;
}

public void setTotalOpenDays(String totalOpenDays) {
	this.totalOpenDays = totalOpenDays;
}

public String getEventCategoryId() {
		return eventCategoryId;
	}

	public void setEventCategoryId(String eventCategoryId) {
		this.eventCategoryId = eventCategoryId;
	}
  
  public String getEventCategoryNameGa() {
	return eventCategoryNameGa;
	}
	
	public void setEventCategoryNameGa(String eventCategoryNameGa) {
		this.eventCategoryNameGa = eventCategoryNameGa;
	}

public String getEventCategoryName() {
		return eventCategoryName;
	}

	public void setEventCategoryName(String eventCategoryName) {
		this.eventCategoryName = eventCategoryName;
	}
  
  
  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }

  public String getOpenDate() {
    return openDate;
  }

  public void setTiming(String timing) {
    this.timing = timing;
  }

  public String getTiming() {
    return timing;
  }

  public void setOpenDayDesc(String openDayDesc) {
    this.openDayDesc = openDayDesc;
  }

  public String getOpenDayDesc() {
    return openDayDesc;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getEventId() {
    return eventId;
  }

  public void setOpenDayFlag(String openDayFlag) {
    this.openDayFlag = openDayFlag;
  }

  public String getOpenDayFlag() {
    return openDayFlag;
  }

  public void setSuborderItemId(String suborderItemId) {
    this.suborderItemId = suborderItemId;
  }

  public String getSuborderItemId() {
    return suborderItemId;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setQualType(String qualType) {
    this.qualType = qualType;
  }

  public String getQualType() {
    return qualType;
  }
  public void setBookingUrl(String bookingUrl) {
    this.bookingUrl = bookingUrl;
  }
  public String getBookingUrl() {
    return bookingUrl;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setCollegeDispName(String collegeDispName) {
    this.collegeDispName = collegeDispName;
  }
  public String getCollegeDispName() {
    return collegeDispName;
  }
  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }
  public String getStartDate() {
    return startDate;
  }
  public void setEventCalItemId(String eventCalItemId) {
    this.eventCalItemId = eventCalItemId;
  }
  public String getEventCalItemId() {
    return eventCalItemId;
  }
  public void setEventDesp(String eventDesp) {
    this.eventDesp = eventDesp;
  }
  public String getEventDesp() {
    return eventDesp;
  }
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }
  public String getStartTime() {
    return startTime;
  }
  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }
  public String getEndTime() {
    return endTime;
  }
  public void setVenue(String venue) {
    this.venue = venue;
  }
  public String getVenue() {
    return venue;
  }
  public void setQualId(String qualId) {
    this.qualId = qualId;
  }
  public String getQualId() {
    return qualId;
  }
  public void setQualDesc(String qualDesc) {
    this.qualDesc = qualDesc;
  }
  public String getQualDesc() {
    return qualDesc;
  }
  public void setOpendayDetail(String opendayDetail) {
    this.opendayDetail = opendayDetail;
  }
  public String getOpendayDetail() {
    return opendayDetail;
  }
  public void setSelectedTxt(String selectedTxt) {
    this.selectedTxt = selectedTxt;
  }
  public String getSelectedTxt() {
    return selectedTxt;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setMonthYear(String monthYear) {
    this.monthYear = monthYear;
  }
  public String getMonthYear() {
    return monthYear;
  }
  public void setOpenday(String openday) {
    this.openday = openday;
  }
  public String getOpenday() {
    return openday;
  }
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setOpendayDescription(String opendayDescription) {
    this.opendayDescription = opendayDescription;
  }
  public String getOpendayDescription() {
    return opendayDescription;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setOpendaysProviderURL(String opendaysProviderURL) {
    this.opendaysProviderURL = opendaysProviderURL;
  }
  public String getOpendaysProviderURL() {
    return opendaysProviderURL;
  }
  public void setHeadline(String headline) {
    this.headline = headline;
  }
  public String getHeadline() {
    return headline;
  }
  public void setOpendayVenue(String opendayVenue) {
    this.opendayVenue = opendayVenue;
  }
  public String getOpendayVenue() {
    return opendayVenue;
  }
  public void setOpendayDesc(String opendayDesc) {
    this.opendayDesc = opendayDesc;
  }
  public String getOpendayDesc() {
    return opendayDesc;
  }
  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
  public String getEndDate() {
    return endDate;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setQualCode(String qualCode) {
    this.qualCode = qualCode;
  }
  public String getQualCode() {
    return qualCode;
  }

  public void setStOpenDayStartDate(String stOpenDayStartDate) {
    this.stOpenDayStartDate = stOpenDayStartDate;
  }

  public String getStOpenDayStartDate() {
    return stOpenDayStartDate;
  }

  public void setStOpenDayEndDate(String stOpenDayEndDate) {
    this.stOpenDayEndDate = stOpenDayEndDate;
  }

  public String getStOpenDayEndDate() {
    return stOpenDayEndDate;
  }

}
