package com.wuni.util.valueobject.contenthub;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReviewVO {

  private String collegeId = null;
  private String reviewId = null;
  private String userId = null;
  private String reviewTitle = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String userName = null;
  private String overAllRating = null;
  private String atUni = null;
  private String gender = null;
  private String courseTitle = null;
  private String courseId = null;
  private String courseDeletedFlag = null;
  private String seoStudyLevelText = null;
  private String reviewText = null;
  private String mediaId = null;
  private String mediaTypeId = null;
  private String mediaPath = null;
  private String studentName = null;
  private String subjectOfStudy = null;
  private String studentView = null;
  private String studentShortDesc = null;
  private String thumbNailPath = null;
  private String courseDetailsPageURL = null;
  private String questionTitle = null;
  private String rating = null;
  private String roundRating = null;
  private String mediaName = null;
  private String sectionId = null;
  private String reviewedDate = null;
  private String userNameInitial = null;
  private String questionId = null; 
}
