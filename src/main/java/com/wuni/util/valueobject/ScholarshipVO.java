package com.wuni.util.valueobject;

public class ScholarshipVO {
  private String scholarshipId = null;
  private String scholarshipTitle = null;
  private String collegeId = null;
  private String collegeName = null;
  private String scholarshipDesc = null;
  private String scholarshipCnt = null;
  private String scholarshipInfoURL = null;
  
  
  public void init() {
    this.scholarshipId = "";
    this.scholarshipTitle = "";
    this.collegeId = "";
    this.collegeName = "";
  }
  public void flush() {
    this.scholarshipId = null;
    this.scholarshipTitle = null;
    this.collegeId = null;
    this.collegeName = null;
  }
  public void setScholarshipId(String scholarshipId) {
    this.scholarshipId = scholarshipId;
  }
  public String getScholarshipId() {
    return scholarshipId;
  }
  public void setScholarshipTitle(String scholarshipTitle) {
    this.scholarshipTitle = scholarshipTitle;
  }
  public String getScholarshipTitle() {
    return scholarshipTitle;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setScholarshipDesc(String scholarshipDesc) {
    this.scholarshipDesc = scholarshipDesc;
  }
  public String getScholarshipDesc() {
    return scholarshipDesc;
  }
  public void setScholarshipCnt(String scholarshipCnt) {
    this.scholarshipCnt = scholarshipCnt;
  }
  public String getScholarshipCnt() {
    return scholarshipCnt;
  }
  public void setScholarshipInfoURL(String scholarshipInfoURL) {
    this.scholarshipInfoURL = scholarshipInfoURL;
  }
  public String getScholarshipInfoURL() {
    return scholarshipInfoURL;
  }
}
