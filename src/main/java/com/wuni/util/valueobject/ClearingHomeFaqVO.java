package com.wuni.util.valueobject;


public class ClearingHomeFaqVO {
   private String questionId = null;
   private String questionTitle = null;
   private String answer = null;
   
   public String getQuestionId() {
      return questionId;
   }
   
   public void setQuestionId(String questionId) {
      this.questionId = questionId;
   }
   
   public String getQuestionTitle() {
      return questionTitle;
   }
   
   public void setQuestionTitle(String questionTitle) {
      this.questionTitle = questionTitle;
   }
   
   public String getAnswer() {
      return answer;
   }
   
   public void setAnswer(String answer) {
      this.answer = answer;
   }
}
