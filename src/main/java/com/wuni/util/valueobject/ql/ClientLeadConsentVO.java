package com.wuni.util.valueobject.ql;

public class ClientLeadConsentVO {
  private String consentText = null;
  private String clientMarketingConsent = null;
  private String basketId = null;
  private String collegeDisplayName = null;
  private String collegeConsentText = null;
  private String collegeId = null;
  public String getBasketId() {
	return basketId;
  }
  public void setBasketId(String basketId) {
	this.basketId = basketId;
  }
  public String getCollegeDisplayName() {
	return collegeDisplayName;
  }
  public String getCollegeId() {
	return collegeId;
}
public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
}
public void setCollegeDisplayName(String collegeDisplayName) {
	this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeConsentText() {
	return collegeConsentText;
  }
  public void setCollegeConsentText(String collegeConsentText) {
	this.collegeConsentText = collegeConsentText;
  }
  public String getConsentText() {
	return consentText;
  }
  public void setConsentText(String consentText) {
	this.consentText = consentText;
  }
  public String getClientMarketingConsent() {
	return clientMarketingConsent;
  }
  public void setClientMarketingConsent(String clientMarketingConsent) {
	this.clientMarketingConsent = clientMarketingConsent;
  }  
}
