package com.wuni.util.valueobject.ql;

public class BasicFormVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String courseId = null;
  private String courseName = null;
  private String firstName = null;
  private String lastName = null;
  private String address1 = null;
  private String address2 = null;
  private String town = null;
  private String postCode = null;
  private String emailAddress = null;
  boolean nonukresident = false;
  private String gender = null;
  private String nationality = null;
  private String dateOfBirth = null;
  private String nationalityValue = null;
  private String nationalityDesc = null;
  private String courseDesc = null; 
  private String studyLevelCode = null;
  private String studyMode = null;
  private String ucasCode = null;
  private String duration = null;
  private String collegeLocation = null;
  private String entryRequirements = null;
  private String startDate = null;
  private String tutionFees = null;
  private String qualification = null;
  private String ucasTariff = null;
  private String tutionFeesDesc = null;
  private String durationDesc = null;
  private String newsLetter = "N";
  private String yeartoJoinCourse = null;
  private String countryOfResId = null;
  private String gradeValue = null;
  private String gradeType = null;
  
  private String marketingEmail = null;
  private String solusEmail = null;
  private String surveyEmail = null; 
  
  private String defaultYoe = null;
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getAddress2() {
    return address2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setNonukresident(boolean nonukresident) {
    this.nonukresident = nonukresident;
  }

  public boolean isNonukresident() {
    return nonukresident;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setNationalityValue(String nationalityValue) {
    this.nationalityValue = nationalityValue;
  }

  public String getNationalityValue() {
    return nationalityValue;
  }

  public void setNationalityDesc(String nationalityDesc) {
    this.nationalityDesc = nationalityDesc;
  }

  public String getNationalityDesc() {
    return nationalityDesc;
  }
  
  public void setCourseDesc(String courseDesc) {
    this.courseDesc = courseDesc;
  }
  
  public String getCourseDesc() {
    return courseDesc;
  }
  
  public void setStudyLevelCode(String studyLevelCode) {
    this.studyLevelCode = studyLevelCode;
  }
  
  public String getStudyLevelCode() {
    return studyLevelCode;
  }
  
  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }
  
  public String getStudyMode() {
    return studyMode;
  }
  
  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }
  
  public String getUcasCode() {
    return ucasCode;
  }
  
  public void setDuration(String duration) {
    this.duration = duration;
  }
  
  public String getDuration() {
    return duration;
  }
  
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  
  public String getCollegeLocation() {
    return collegeLocation;
  }
  
  public void setEntryRequirements(String entryRequirements) {
    this.entryRequirements = entryRequirements;
  }
  
  public String getEntryRequirements() {
    return entryRequirements;
  }
  
  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }
  
  public String getStartDate() {
    return startDate;
  }
  
  public void setTutionFees(String tutionFees) {
    this.tutionFees = tutionFees;
  }
  
  public String getTutionFees() {
    return tutionFees;
  }
  public void setQualification(String qualification)
  {
    this.qualification = qualification;
  }
  public String getQualification()
  {
    return qualification;
  }
  public void setUcasTariff(String ucasTariff)
  {
    this.ucasTariff = ucasTariff;
  }
  public String getUcasTariff()
  {
    return ucasTariff;
  }
  public void setTutionFeesDesc(String tutionFeesDesc)
  {
    this.tutionFeesDesc = tutionFeesDesc;
  }
  public String getTutionFeesDesc()
  {
    return tutionFeesDesc;
  }
  public void setDurationDesc(String durationDesc)
  {
    this.durationDesc = durationDesc;
  }
  public String getDurationDesc()
  {
    return durationDesc;
  }
  public void setNewsLetter(String newsLetter)
  {
    this.newsLetter = newsLetter;
  }
  public String getNewsLetter()
  {
    return newsLetter;
  }
  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }
  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }

  public void setCountryOfResId(String countryOfResId)
  {
    this.countryOfResId = countryOfResId;
  }

  public String getCountryOfResId()
  {
    return countryOfResId;
  }
  public void setGradeValue(String gradeValue) {
    this.gradeValue = gradeValue;
}
  public String getGradeValue() {
    return gradeValue;
  }
  public void setGradeType(String gradeType) {
    this.gradeType = gradeType;
  }
  public String getGradeType() {
    return gradeType;
  }
  public void setMarketingEmail(String marketingEmail) {
    this.marketingEmail = marketingEmail;
  }
  public String getMarketingEmail() {
    return marketingEmail;
  }
  public void setSolusEmail(String solusEmail) {
    this.solusEmail = solusEmail;
  }
  public String getSolusEmail() {
    return solusEmail;
  }
  public void setSurveyEmail(String surveyEmail) {
    this.surveyEmail = surveyEmail;
  }
  public String getSurveyEmail() {
    return surveyEmail;
  }

  public String getDefaultYoe() {
	return defaultYoe;
  }

  public void setDefaultYoe(String defaultYoe) {
	this.defaultYoe = defaultYoe;
  }
  
  
}
