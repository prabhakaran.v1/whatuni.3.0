package com.wuni.util.valueobject.ql;

public class PostEnquiryVO {
  
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String collegeId = null;
  private String collegeLogo = null;
  private String orderItemId = null;
  private String subOrderItemId = null;
  private String myhcProfileId = null;
  private String profileId = null;
  private String profileType = null;
  private String advertName = null;
  private String cpeQualificationNetworkId = null;
  private String subOrderEmail = null;
  private String subOrderProspectus = null;
  private String subOrderWebsite = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectusWebform = null;
  private String reviewId = null;
  private String reviewText = null;
  private String courseId = null;
  private String courseTitle = null;
  private String whatUniSays = null;
  private String reviewURL = null;
  private String slotType = null;
  private String courseDetailsURL = null;
  private String uniHomeUrl = null;
  private String courseSeoStudyLevelText = null;
  private String courseStudyLevelCode = null;
  private String emailPrice = null;
  private String whatUniURL = null;
  private String dpURL = null;  
  private String enquiryType = null;
  private String interactionType = null;
  private String dpFlag = null;
  
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }

  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setUniHomeUrl(String uniHomeUrl) {
    this.uniHomeUrl = uniHomeUrl;
  }

  public String getUniHomeUrl() {
    return uniHomeUrl;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewText(String reviewText) {
    this.reviewText = reviewText;
  }

  public String getReviewText() {
    return reviewText;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setWhatUniSays(String whatUniSays) {
    this.whatUniSays = whatUniSays;
  }

  public String getWhatUniSays() {
    return whatUniSays;
  }

  public void setReviewURL(String reviewURL) {
    this.reviewURL = reviewURL;
  }

  public String getReviewURL() {
    return reviewURL;
  }

  public void setSlotType(String slotType) {
    this.slotType = slotType;
  }

  public String getSlotType() {
    return slotType;
  }

  public void setCourseDetailsURL(String courseDetailsURL) {
    this.courseDetailsURL = courseDetailsURL;
  }

  public String getCourseDetailsURL() {
    return courseDetailsURL;
  }

  public void setCourseStudyLevelCode(String courseStudyLevelCode) {
    this.courseStudyLevelCode = courseStudyLevelCode;
  }

  public String getCourseStudyLevelCode() {
    return courseStudyLevelCode;
  }

  public void setCourseSeoStudyLevelText(String courseSeoStudyLevelText) {
    this.courseSeoStudyLevelText = courseSeoStudyLevelText;
  }

  public String getCourseSeoStudyLevelText() {
    return courseSeoStudyLevelText;
  }

  public void setEmailPrice(String emailPrice) {
    this.emailPrice = emailPrice;
  }

  public String getEmailPrice() {
    return emailPrice;
  }

  public void setWhatUniURL(String whatUniURL) {
    this.whatUniURL = whatUniURL;
  }

  public String getWhatUniURL() {
    return whatUniURL;
  }
  public void setDpURL(String dpURL) {
    this.dpURL = dpURL;
  }
  public String getDpURL() {
    return dpURL;
  }
  public void setEnquiryType(String enquiryType) {
    this.enquiryType = enquiryType;
  }
  public String getEnquiryType() {
    return enquiryType;
  }
  public void setInteractionType(String interactionType) {
    this.interactionType = interactionType;
  }
  public String getInteractionType() {
    return interactionType;
  }
  public void setDpFlag(String dpFlag) {
    this.dpFlag = dpFlag;
  }
  public String getDpFlag() {
    return dpFlag;
  }
}
