package com.wuni.util.valueobject.ql;

public class CoursePodInfo {

  private String studyLevelCode = null;
  private String courseId = null;
  private String courseName = null;
  

  public void setStudyLevelCode(String studyLevelCode) {
    this.studyLevelCode = studyLevelCode;
  }

  public String getStudyLevelCode() {
    return studyLevelCode;
  }

}
