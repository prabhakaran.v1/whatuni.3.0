package com.wuni.util.valueobject;

public class UserTimeLineVO {
  private String timeLineId = "";
  private String parentTimeLineId = "";
  private String tlTitle = "";
  private String tlDescription = "";
  private String tlURL = "";
  private String tlImageURL = "";
  private String tlMandFlag = "";
  private String tlItemType = "";
  private String tlPostId = "";
  private String tlItemStatus = "";
  private String tlItemActionType = "";
  private String tlDeadLineFlag = "";
  private String tlYouAreHereId = "";
  private String tlYouShouldBeHereId = "";
  private String tlDispSeq = "";
  
  public void setTlTitle(String tlTitle) {
    this.tlTitle = tlTitle;
  }
  public String getTlTitle() {
    return tlTitle;
  }
  public void setTlDescription(String tlDescription) {
    this.tlDescription = tlDescription;
  }
  public String getTlDescription() {
    return tlDescription;
  }
  public void setTlURL(String tlURL) {
    this.tlURL = tlURL;
  }
  public String getTlURL() {
    return tlURL;
  }
  public void setTlImageURL(String tlImageURL) {
    this.tlImageURL = tlImageURL;
  }
  public String getTlImageURL() {
    return tlImageURL;
  }
  public void setTlMandFlag(String tlMandFlag) {
    this.tlMandFlag = tlMandFlag;
  }
  public String getTlMandFlag() {
    return tlMandFlag;
  }
  public void setTlItemType(String tlItemType) {
    this.tlItemType = tlItemType;
  }
  public String getTlItemType() {
    return tlItemType;
  }
  public void setTlPostId(String tlPostId) {
    this.tlPostId = tlPostId;
  }
  public String getTlPostId() {
    return tlPostId;
  }
  public void setTlItemStatus(String tlItemStatus) {
    this.tlItemStatus = tlItemStatus;
  }
  public String getTlItemStatus() {
    return tlItemStatus;
  }
  public void setTlDeadLineFlag(String tlDeadLineFlag) {
    this.tlDeadLineFlag = tlDeadLineFlag;
  }
  public String getTlDeadLineFlag() {
    return tlDeadLineFlag;
  }
  public void setTlYouAreHereId(String tlYouAreHereId) {
    this.tlYouAreHereId = tlYouAreHereId;
  }
  public String getTlYouAreHereId() {
    return tlYouAreHereId;
  }
  public void setTlYouShouldBeHereId(String tlYouShouldBeHereId) {
    this.tlYouShouldBeHereId = tlYouShouldBeHereId;
  }
  public String getTlYouShouldBeHereId() {
    return tlYouShouldBeHereId;
  }
  public void setTimeLineId(String timeLineId) {
    this.timeLineId = timeLineId;
  }
  public String getTimeLineId() {
    return timeLineId;
  }
  public void setParentTimeLineId(String parentTimeLineId) {
    this.parentTimeLineId = parentTimeLineId;
  }
  public String getParentTimeLineId() {
    return parentTimeLineId;
  }
  public void setTlItemActionType(String tlItemActionType) {
    this.tlItemActionType = tlItemActionType;
  }
  public String getTlItemActionType() {
    return tlItemActionType;
  }
  public void setTlDispSeq(String tlDispSeq) {
    this.tlDispSeq = tlDispSeq;
  }
  public String getTlDispSeq() {
    return tlDispSeq;
  }
}
