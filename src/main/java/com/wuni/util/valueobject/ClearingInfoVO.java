package com.wuni.util.valueobject;

public class ClearingInfoVO {

  private String clearingCollegeId = "";
  private String clearingCollegeName = "";
  private String clearingProfileCount = "";
  private String clearingProfileUrl = "";
  //
  private String subjectName = "";
  private String subjectUrl = "";
  private String categoryId = "";
  private String subjectText = "";
  private String collegeDisplayName = "";
  private String collegeLogo = "";
  private String domainCollegeLogo = "";
  private String sponsorReviewURL = "";
  
  private String orderItemId = null;
  private String subOrderEmail = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectus = null;
  private String subOrderProspectusWebform = null;
  private String subOrderWebsite = null;  
  private String subOrderItemId = null;  
  private String myhcProfileId = null;
  private String profileType = null;
  private String cpeQualificationNetworkId = null;  
  private String hotline = null;
  private String mediaId = null;
  private String mediaPath = null;
  private String mediaThumbPath = null;
  private String mediaType = null;
  private String videoThumbPath = null;
  private String advertName = null;  
  private String qlFlag = null;  
  private String websitePrice = null;  
  private String webformPrice = null;  
  private String mediaTypeId = null;  
  private String uniHomeURL = null;
  private String reviewCount = null;
  private String overallRating = null;
  private String overallRatingExact = null;
        
  public void flush() {
    clearingCollegeId = null;
    clearingCollegeName = null;
    clearingProfileCount = null;
    clearingProfileUrl = null;
  }

  public void setClearingCollegeId(String clearingCollegeId) {
    this.clearingCollegeId = clearingCollegeId;
  }

  public String getClearingCollegeId() {
    return clearingCollegeId;
  }

  public void setClearingCollegeName(String clearingCollegeName) {
    this.clearingCollegeName = clearingCollegeName;
  }

  public String getClearingCollegeName() {
    return clearingCollegeName;
  }

  public void setClearingProfileCount(String clearingProfileCount) {
    this.clearingProfileCount = clearingProfileCount;
  }

  public String getClearingProfileCount() {
    return clearingProfileCount;
  }

  public void setClearingProfileUrl(String clearingProfileUrl) {
    this.clearingProfileUrl = clearingProfileUrl;
  }

  public String getClearingProfileUrl() {
    return clearingProfileUrl;
  }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectUrl(String subjectUrl) {
        this.subjectUrl = subjectUrl;
    }

    public String getSubjectUrl() {
        return subjectUrl;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setSubjectText(String subjectText) {
        this.subjectText = subjectText;
    }

    public String getSubjectText() {
        return subjectText;
    }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setDomainCollegeLogo(String domainCollegeLogo) {
    this.domainCollegeLogo = domainCollegeLogo;
  }
  public String getDomainCollegeLogo() {
    return domainCollegeLogo;
  }
  public void setSponsorReviewURL(String sponsorReviewURL) {
    this.sponsorReviewURL = sponsorReviewURL;
  }
  public String getSponsorReviewURL() {
    return sponsorReviewURL;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setHotline(String hotline) {
    this.hotline = hotline;
  }
  public String getHotline() {
    return hotline;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }
  public String getMediaThumbPath() {
    return mediaThumbPath;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setVideoThumbPath(String videoThumbPath) {
    this.videoThumbPath = videoThumbPath;
  }
  public String getVideoThumbPath() {
    return videoThumbPath;
  }
  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }
  public String getAdvertName() {
    return advertName;
  }
  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }
  public String getQlFlag() {
    return qlFlag;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }
  public String getWebformPrice() {
    return webformPrice;
  }
  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }
  public String getMediaTypeId() {
    return mediaTypeId;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  public String getOverallRating() {
    return overallRating;
  }
  public void setOverallRatingExact(String overallRatingExact) {
    this.overallRatingExact = overallRatingExact;
  }
  public String getOverallRatingExact() {
    return overallRatingExact;
  }
}
