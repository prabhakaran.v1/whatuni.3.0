package com.wuni.util.valueobject;

public class HomePageImageDetailsVO {

  String imageId = "";
  String imageName = "";
  String longText = "";
  String smallText = "";
  String url = "";
  String userCount = ""; //26_AUG_2014
  String courseCount = "";
  String reviewCount = "";
  String mediaType = "";
  String displayOrder = "";
  String slots = "";
  String videoPath = "";
  String homeHeroImageId = "";
  String orderItemId = "";
  String collegeId = "";
  String collegeName = "";
  String collegeNameDisplay = "";
  String mediaId = "";
  String mediaTypeId = "";
  String mediaPath = "";
  String title = "";
  String subTitle = "";
  String imageUrl = "";
  String startDate = "";
  String expiryDate = "";
  String status = "";
  String createdDate = "";
  String updatedDate = "";
  String friendUrl = "";
  String friendActivityText = "";
  String privacyFlag = "";
  String imageDetails = "";
  String articleType = "";
  String collegeLogo = "";
  String collegeNameUrl = "";
  String futureOpendayCount = "";
  String defaultText = "";
  String defaultUrl = "";
  String mediaPath992 = "";
  String mediaPath480 = "";
  
  
  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageName() {
    return imageName;
  }

  public void setLongText(String longText) {
    this.longText = longText;
  }

  public String getLongText() {
    return longText;
  }

  public void setSmallText(String smallText) {
    this.smallText = smallText;
  }

  public String getSmallText() {
    return smallText;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setUserCount(String userCount) {
    this.userCount = userCount;
  }

  public String getUserCount() {
    return userCount;
  }

  public void setCourseCount(String courseCount) {
    this.courseCount = courseCount;
  }

  public String getCourseCount() {
    return courseCount;
  }

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setDisplayOrder(String displayOrder) {
    this.displayOrder = displayOrder;
  }

  public String getDisplayOrder() {
    return displayOrder;
  }

  public void setSlots(String slots) {
    this.slots = slots;
  }

  public String getSlots() {
    return slots;
  }

  public void setVideoPath(String videoPath) {
    this.videoPath = videoPath;
  }

  public String getVideoPath() {
    return videoPath;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
  }

  public String getExpiryDate() {
    return expiryDate;
  }

  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  public String getCreatedDate() {
    return createdDate;
  }

  public void setUpdatedDate(String updatedDate) {
    this.updatedDate = updatedDate;
  }

  public String getUpdatedDate() {
    return updatedDate;
  }

  public void setHomeHeroImageId(String homeHeroImageId) {
    this.homeHeroImageId = homeHeroImageId;
  }

  public String getHomeHeroImageId() {
    return homeHeroImageId;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public void setFriendUrl(String friendUrl) {
    this.friendUrl = friendUrl;
  }

  public String getFriendUrl() {
    return friendUrl;
  }

  public void setFriendActivityText(String friendActivityText) {
    this.friendActivityText = friendActivityText;
  }

  public String getFriendActivityText() {
    return friendActivityText;
  }

  public void setPrivacyFlag(String privacyFlag) {
    this.privacyFlag = privacyFlag;
  }

  public String getPrivacyFlag() {
    return privacyFlag;
  }

  public void setImageDetails(String imageDetails) {
    this.imageDetails = imageDetails;
  }

  public String getImageDetails() {
    return imageDetails;
  }
  public void setArticleType(String articleType) {
    this.articleType = articleType;
  }
  public String getArticleType() {
    return articleType;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setCollegeNameUrl(String collegeNameUrl) {
    this.collegeNameUrl = collegeNameUrl;
  }
  public String getCollegeNameUrl() {
    return collegeNameUrl;
  }
  public void setFutureOpendayCount(String futureOpendayCount) {
    this.futureOpendayCount = futureOpendayCount;
  }
  public String getFutureOpendayCount() {
    return futureOpendayCount;
  }
  public void setDefaultText(String defaultText) {
    this.defaultText = defaultText;
  }
  public String getDefaultText() {
    return defaultText;
  }
  public void setDefaultUrl(String defaultUrl) {
    this.defaultUrl = defaultUrl;
  }
  public String getDefaultUrl() {
    return defaultUrl;
  }
  public void setMediaPath992(String mediaPath992) {
    this.mediaPath992 = mediaPath992;
  }
  public String getMediaPath992() {
    return mediaPath992;
  }
  public void setMediaPath480(String mediaPath480) {
    this.mediaPath480 = mediaPath480;
  }
  public String getMediaPath480() {
    return mediaPath480;
  }
}
