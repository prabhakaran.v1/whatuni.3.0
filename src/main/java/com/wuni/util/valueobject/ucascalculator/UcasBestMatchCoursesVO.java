package com.wuni.util.valueobject.ucascalculator;


public class UcasBestMatchCoursesVO {

  private String collegeId = "";
  private String courseTitle = "";
  private String courseId = "";
  private String subOrderItemId = "";
  private String aLevel = "";
  private String sqaHigher = "";
  private String sqaAdv = "";
  private String ucasPoints = "";
  private String aLevelMax = "";
  private String sqaHigherMax = "";
  private String sqaAdvMax = "";
  private String ucasPointsMax = "";
  private String score = "";
  private String websitePrice = "";
  private String webformPrice = "";
  private String subOrderEmailWebform = "";
  private String subOrderEmail = "";
  private String courseDetailUrl = "";
  private String isSponsoredCollege = "";
  private String seoStudyLevelText = "";
  private String cpeQualificationNetworkId = "";
  private String courseUcasTariff = "";

  public void init() {
    this.collegeId = "";
    this.courseTitle = "";
    this.courseId = "";
    this.subOrderItemId = "";
    this.aLevel = "";
    this.sqaHigher = "";
    this.sqaAdv = "";
    this.ucasPoints = "";
    this.aLevelMax = "";
    this.sqaHigherMax = "";
    this.sqaAdvMax = "";    
    this.score = "";
  
  }

  public void flush() {
    this.collegeId = null;
    this.courseTitle = null;
    this.courseId = null;
    this.subOrderItemId = null;
    this.aLevel = null;
    this.sqaHigher = null;
    this.sqaAdv = null;
    this.ucasPoints = null;
    this.aLevelMax = null;
    this.sqaHigherMax = null;
    this.sqaAdvMax = null;
    
    this.score = null;
    
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }

  public void setALevel(String aLevel) {
    this.aLevel = aLevel;
  }
  public String getALevel() {
    return aLevel;
  }
  public void setSqaHigher(String sqaHigher) {
    this.sqaHigher = sqaHigher;
  }
  public String getSqaHigher() {
    return sqaHigher;
  }
  public void setSqaAdv(String sqaAdv) {
    this.sqaAdv = sqaAdv;
  }
  public String getSqaAdv() {
    return sqaAdv;
  }
  public void setUcasPoints(String ucasPoints) {
    this.ucasPoints = ucasPoints;
  }
  public String getUcasPoints() {
    return ucasPoints;
  }
  public void setALevelMax(String aLevelMax) {
    this.aLevelMax = aLevelMax;
  }
  public String getALevelMax() {
    return aLevelMax;
  }
  public void setSqaHigherMax(String sqaHigherMax) {
    this.sqaHigherMax = sqaHigherMax;
  }
  public String getSqaHigherMax() {
    return sqaHigherMax;
  }
  public void setSqaAdvMax(String sqaAdvMax) {
    this.sqaAdvMax = sqaAdvMax;
  }
  public String getSqaAdvMax() {
    return sqaAdvMax;
  }
 
  public void setScore(String score) {
    this.score = score;
  }
  public String getScore() {
    return score;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }
  public String getWebformPrice() {
    return webformPrice;
  }

  public void setUcasPointsMax(String ucasPointsMax) {
    this.ucasPointsMax = ucasPointsMax;
  }
  public String getUcasPointsMax() {
    return ucasPointsMax;
  }
  public void setCourseDetailUrl(String courseDetailUrl) {
    this.courseDetailUrl = courseDetailUrl;
  }
  public String getCourseDetailUrl() {
    return courseDetailUrl;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setIsSponsoredCollege(String isSponsoredCollege) {
    this.isSponsoredCollege = isSponsoredCollege;
  }
  public String getIsSponsoredCollege() {
    return isSponsoredCollege;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setCourseUcasTariff(String courseUcasTariff) {
    this.courseUcasTariff = courseUcasTariff;
  }
  public String getCourseUcasTariff() {
    return courseUcasTariff;
  }
}
