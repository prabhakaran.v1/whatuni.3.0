package com.wuni.util.valueobject;

public class VersionChangesVO {
  private String affliateId = null;
  private String pageType = null;

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPageType() {
        return pageType;
    }

    public void setAffliateId(String affliateId) {
        this.affliateId = affliateId;
    }

    public String getAffliateId() {
        return affliateId;
    }
}
