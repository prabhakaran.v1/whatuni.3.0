package com.wuni.util.valueobject.advert;

public class ClearingProfileVO {
  String userId = null;
  String sessionId = null;
  String collegeId = null;
  String requestDesc = null;
  String clientIp = null;
  String userAgent = null;
  String sectionName = null;
  String requestURL = null;
  String referelURL = null;
  String networkId = null;
  String basketId = null;
  String trackSessionId = null;
  //
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }
  public String getSessionId() {
    return sessionId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setRequestDesc(String requestDesc) {
    this.requestDesc = requestDesc;
  }
  public String getRequestDesc() {
    return requestDesc;
  }
  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }
  public String getClientIp() {
    return clientIp;
  }
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
  public String getUserAgent() {
    return userAgent;
  }
  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }
  public String getSectionName() {
    return sectionName;
  }
  public void setRequestURL(String requestURL) {
    this.requestURL = requestURL;
  }
  public String getRequestURL() {
    return requestURL;
  }
  public void setReferelURL(String referelURL) {
    this.referelURL = referelURL;
  }
  public String getReferelURL() {
    return referelURL;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
}
