package com.wuni.util.valueobject.advert;

public class CollegeProfileVO {
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String collegeImagePath = null;
  private String profileImagePath = null;
  private String profileDescription = null;
  private String showOverViewTab = null;
  private String showAccommodationTab = null;
  private String showEntertainmentTab = null;
  private String showCoursesFacilitiesTab = null;
  private String showWelfareTab = null;
  private String showFeesFundingTab = null;
  private String showJobProspectsTab = null;
  private String showContactTab = null;
  private String videoUrl = null;
  private String videoThumbnailUrl = null;
  private String jsStatsLogId = null;
  private String dLogTypeKey = null;
  private String videoId = null;
  private String selectedSectionName = null;
  private String originalSectionName = null;
  private String orderItemId = null;
  private String subOrderItemId = null;
  private String subOrderWebsite = null;
  private String subOrderEmail = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectus = null;
  private String subOrderProspectusWebform = null;
  private String videoMetaDuration = null;
  private String profileId = null;//3_JUN_2014_REL
  private String myhcProfileId = null;//3_JUN_2014_REL
  private String sectionName = null;//3_JUN_2014_REL
  private String mediaType = null; 
  //Added by Indumathi.S For unique content changes May_31_2016
  private String wuscaRank = null;
  private String profileRating = null;
  private String profileRoundRating = null;
  //End
  
  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.collegeImagePath = null;
    this.profileImagePath = null;
    this.profileDescription = null;
    this.showOverViewTab = null;
    this.showAccommodationTab = null;
    this.showEntertainmentTab = null;
    this.showCoursesFacilitiesTab = null;
    this.showWelfareTab = null;
    this.showFeesFundingTab = null;
    this.showJobProspectsTab = null;
    this.showContactTab = null;
    this.videoUrl = null;
    this.videoThumbnailUrl = null;
    this.jsStatsLogId = null;
    this.dLogTypeKey = null;
    this.videoId = null;
    this.selectedSectionName = null;
    this.orderItemId = null;
    this.subOrderItemId = null;
    this.subOrderWebsite = null;
    this.subOrderEmail = null;
    this.subOrderEmailWebform = null;
    this.subOrderProspectus = null;
    this.subOrderProspectusWebform = null;
  }
  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.collegeImagePath = "";
    this.profileImagePath = "";
    this.profileDescription = "";
    this.showOverViewTab = "";
    this.showAccommodationTab = "";
    this.showEntertainmentTab = "";
    this.showCoursesFacilitiesTab = "";
    this.showWelfareTab = "";
    this.showFeesFundingTab = "";
    this.showJobProspectsTab = "";
    this.showContactTab = "";
    this.videoUrl = "";
    this.videoThumbnailUrl = "";
    this.jsStatsLogId = "";
    this.dLogTypeKey = "";
    this.videoId = "";
    this.selectedSectionName = "";
    this.orderItemId = "";
    this.subOrderItemId = "";
    this.subOrderWebsite = "";
    this.subOrderEmail = "";
    this.subOrderEmailWebform = "";
    this.subOrderProspectus = "";
    this.subOrderProspectusWebform = "";
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeImagePath(String collegeImagePath) {
    this.collegeImagePath = collegeImagePath;
  }
  public String getCollegeImagePath() {
    return collegeImagePath;
  }
  public void setProfileImagePath(String profileImagePath) {
    this.profileImagePath = profileImagePath;
  }
  public String getProfileImagePath() {
    return profileImagePath;
  }
  public void setProfileDescription(String profileDescription) {
    this.profileDescription = profileDescription;
  }
  public String getProfileDescription() {
    return profileDescription;
  }
  public void setShowOverViewTab(String showOverViewTab) {
    this.showOverViewTab = showOverViewTab;
  }
  public String getShowOverViewTab() {
    return showOverViewTab;
  }
  public void setShowAccommodationTab(String showAccommodationTab) {
    this.showAccommodationTab = showAccommodationTab;
  }
  public String getShowAccommodationTab() {
    return showAccommodationTab;
  }
  public void setShowEntertainmentTab(String showEntertainmentTab) {
    this.showEntertainmentTab = showEntertainmentTab;
  }
  public String getShowEntertainmentTab() {
    return showEntertainmentTab;
  }
  public void setShowCoursesFacilitiesTab(String showCoursesFacilitiesTab) {
    this.showCoursesFacilitiesTab = showCoursesFacilitiesTab;
  }
  public String getShowCoursesFacilitiesTab() {
    return showCoursesFacilitiesTab;
  }
  public void setShowWelfareTab(String showWelfareTab) {
    this.showWelfareTab = showWelfareTab;
  }
  public String getShowWelfareTab() {
    return showWelfareTab;
  }
  public void setShowFeesFundingTab(String showFeesFundingTab) {
    this.showFeesFundingTab = showFeesFundingTab;
  }
  public String getShowFeesFundingTab() {
    return showFeesFundingTab;
  }
  public void setShowJobProspectsTab(String showJobProspectsTab) {
    this.showJobProspectsTab = showJobProspectsTab;
  }
  public String getShowJobProspectsTab() {
    return showJobProspectsTab;
  }
  public void setShowContactTab(String showContactTab) {
    this.showContactTab = showContactTab;
  }
  public String getShowContactTab() {
    return showContactTab;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
  public void setVideoThumbnailUrl(String videoThumbnailUrl) {
    this.videoThumbnailUrl = videoThumbnailUrl;
  }
  public String getVideoThumbnailUrl() {
    return videoThumbnailUrl;
  }
  public void setJsStatsLogId(String jsStatsLogId) {
    this.jsStatsLogId = jsStatsLogId;
  }
  public String getJsStatsLogId() {
    return jsStatsLogId;
  }
  public void setDLogTypeKey(String dLogTypeKey) {
    this.dLogTypeKey = dLogTypeKey;
  }
  public String getDLogTypeKey() {
    return dLogTypeKey;
  }
  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }
  public String getVideoId() {
    return videoId;
  }
  public void setSelectedSectionName(String selectedSectionName) {
    this.selectedSectionName = selectedSectionName;
  }
  public String getSelectedSectionName() {
    return selectedSectionName;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setVideoMetaDuration(String videoMetaDuration) {
    this.videoMetaDuration = videoMetaDuration;
  }
  public String getVideoMetaDuration() {
    return videoMetaDuration;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getProfileId() {
    return profileId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }
  public String getSectionName() {
    return sectionName;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }

  public void setWuscaRank(String wuscaRank) {
    this.wuscaRank = wuscaRank;
  }

  public String getWuscaRank() {
    return wuscaRank;
  }

  public void setProfileRating(String profileRating) {
    this.profileRating = profileRating;
  }

  public String getProfileRating() {
    return profileRating;
  }

  public void setProfileRoundRating(String profileRoundRating) {
    this.profileRoundRating = profileRoundRating;
  }

  public String getProfileRoundRating() {
    return profileRoundRating;
  }

  public void setOriginalSectionName(String originalSectionName) {
    this.originalSectionName = originalSectionName;
  }

  public String getOriginalSectionName() {
    return originalSectionName;
  }

}
