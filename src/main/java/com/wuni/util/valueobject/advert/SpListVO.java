package com.wuni.util.valueobject.advert;

public class SpListVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String advertName = null;
  private String advertDescShort = null;
  private String myhcProfileId = null;
  private String cpeQualificationNetworkId = null;
  private String profileId = null; //default = 0
  private String sectionDesc = null; //default = overview
  //
  private String spURL = null;
  private String spListHomeURL = null;
  private String imagePath = null;

  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.sectionDesc = null;
    this.profileId = null;
    this.myhcProfileId = null;
    this.cpeQualificationNetworkId = null;
    this.advertName = null;
    this.advertDescShort = null;
    this.spURL = null;
    this.spListHomeURL = null;
  }

  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.sectionDesc = "";
    this.profileId = "";
    this.myhcProfileId = "";
    this.cpeQualificationNetworkId = "";
    this.advertName = "";
    this.advertDescShort = "";
    this.spURL = "";
    this.spListHomeURL = "";
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setSectionDesc(String sectionDesc) {
    this.sectionDesc = sectionDesc;
  }

  public String getSectionDesc() {
    return sectionDesc;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setAdvertDescShort(String advertDescShort) {
    this.advertDescShort = advertDescShort;
  }

  public String getAdvertDescShort() {
    return advertDescShort;
  }

  public void setSpURL(String spURL) {
    this.spURL = spURL;
  }

  public String getSpURL() {
    return spURL;
  }

  public void setSpListHomeURL(String spListHomeURL) {
    this.spListHomeURL = spListHomeURL;
  }

  public String getSpListHomeURL() {
    return spListHomeURL;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
}
