package com.wuni.util.valueobject.advert;

import lombok.Data;

@Data
public class NewUniLandingVO {
  String userId = null;
  String sessionId = null;
  String collegeId = null;
  String requestDesc = null;
  String clientIp = null;
  String userAgent = null;
  String sectionName = null;
  String requestURL = null;
  String referelURL = null;
  String networkId = null;
  String basketId = null;
  String trackSessionId = null;
  String pageName = null;
  String myhcProfileId = null;
  String userJourney = null;
  String studyMode = null;
  private String nonAdvPageUrl = null;
  
}
