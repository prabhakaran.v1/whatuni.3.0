package com.wuni.util.valueobject.advert;

public class ProspectusVO {
  private String collegeId = null;
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String gaCollegeName  = null;
  private String subOrderItemId = null;
  private String orderItemId = null;
  private String timesRanking = null;
  private String collegeLogoPath = null;
  private String overallRating = null;
  private String overAllRatingRounded = null;
  private String studyLevelId = null;
  private String sort = null;
  private String categoryCode = null;
  private String type = null;
  private String subjectTimesRanking = null;
  private String emailPrice = null;
  private String regionName = null;
  private String collegeInBasket = null;
  private String userId = null;
  private String basketId = null;
  private String enqBasketId = null;
  private String qualification = null;
  private String sortBy = null;
  private String assocTypeCode = null;
  private String collegeNameDisplay = null;
  private String status = null;
  private String totalCount = null;
  private String displayName = null;
  private String openDays = null;
  private String openDaysUrl = null;
  private String carouselType = null; 
  private String courseId = null; 
  private String courseTitle = null; 
  private String courseDeletedFlag = null; 
  private String studyLevelDesc = null;  
  private String pageName = null;
  private String pageNo = null;  
  private String detailUserName = null;
  private String dateSent = null;  
  private String finalChoiceExistsFlag = null;  
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setTimesRanking(String timesRanking) {
    this.timesRanking = timesRanking;
  }
  public String getTimesRanking() {
    return timesRanking;
  }
  public void setCollegeLogoPath(String collegeLogoPath) {
    this.collegeLogoPath = collegeLogoPath;
  }
  public String getCollegeLogoPath() {
    return collegeLogoPath;
  }
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  public String getOverallRating() {
    return overallRating;
  }
  public void setOverAllRatingRounded(String overAllRatingRounded) {
    this.overAllRatingRounded = overAllRatingRounded;
  }
  public String getOverAllRatingRounded() {
    return overAllRatingRounded;
  }
  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }
  public String getStudyLevelId() {
    return studyLevelId;
  }
  public void setSort(String sort) {
    this.sort = sort;
  }
  public String getSort() {
    return sort;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setType(String type) {
    this.type = type;
  }
  public String getType() {
    return type;
  }
  public void setSubjectTimesRanking(String subjectTimesRanking) {
    this.subjectTimesRanking = subjectTimesRanking;
  }
  public String getSubjectTimesRanking() {
    return subjectTimesRanking;
  }
  public void setEmailPrice(String emailPrice) {
    this.emailPrice = emailPrice;
  }
  public String getEmailPrice() {
    return emailPrice;
  }
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  public String getRegionName() {
    return regionName;
  }
  public void setCollegeInBasket(String collegeInBasket) {
    this.collegeInBasket = collegeInBasket;
  }
  public String getCollegeInBasket() {
    return collegeInBasket;
  }
  public void setSortBy(String sortBy) {
    this.sortBy = sortBy;
  }
  public String getSortBy() {
    return sortBy;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setEnqBasketId(String enqBasketId) {
    this.enqBasketId = enqBasketId;
  }
  public String getEnqBasketId() {
    return enqBasketId;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setAssocTypeCode(String assocTypeCode) {
    this.assocTypeCode = assocTypeCode;
  }
  public String getAssocTypeCode() {
    return assocTypeCode;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  public String getStatus() {
    return status;
  }
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }
  public String getTotalCount() {
    return totalCount;
  }
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
  public String getDisplayName() {
    return displayName;
  }
  public void setOpenDays(String openDays) {
    this.openDays = openDays;
  }
  public String getOpenDays() {
    return openDays;
  }
  public void setOpenDaysUrl(String openDaysUrl) {
    this.openDaysUrl = openDaysUrl;
  }
  public String getOpenDaysUrl() {
    return openDaysUrl;
  }
  public void setCarouselType(String carouselType) {
    this.carouselType = carouselType;
  }
  public String getCarouselType() {
    return carouselType;
  }
  public void setGaCollegeName(String gaCollegeName) {
    this.gaCollegeName = gaCollegeName;
  }
  public String getGaCollegeName() {
    return gaCollegeName;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }
  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }
  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }
  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setDetailUserName(String detailUserName) {
    this.detailUserName = detailUserName;
  }
  public String getDetailUserName() {
    return detailUserName;
  }
  public void setDateSent(String dateSent) {
    this.dateSent = dateSent;
  }
  public String getDateSent() {
    return dateSent;
  }
  public void setFinalChoiceExistsFlag(String finalChoiceExistsFlag) {
    this.finalChoiceExistsFlag = finalChoiceExistsFlag;
  }
  public String getFinalChoiceExistsFlag() {
    return finalChoiceExistsFlag;
  }
}
