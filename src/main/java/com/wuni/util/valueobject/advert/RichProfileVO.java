package com.wuni.util.valueobject.advert;

import lombok.Data;

@Data
public class RichProfileVO {

  private String sessionId = null;
  private String userId = null;
  private String basketId = null;
  private String networkId = null;
  private String requestDesc = null;
  private String clientIp = null;
  private String userAgent = null;
  private String sectionName = null;
  private String requestURL = null;
  private String referelURL = null;
  private String metaPageName = null;
  private String metaPageFlag = null;
  private String trackSessionId = null;
  private String collegeId = null;
  private String collegeName = null;
  private String collgeDispName = null;
  private String mediaType = null;
  private String mediaId = null;//Added by Indumathi.S Apr_19_2016
  private String mediaPath = null;
  private String profileType = null;
  private String myhcProfileId = null;
  private String socialNetworkType = null;
  private String soicalNetworkURL = null;
  //Added by Indumathi For Cost of pint Nov-24-15 Rel
  private String accommodationCost = null;
  private String livingCost = null;
  private String maxAccommodationCost = null;
  private String maxLivingCost = null;
  private String livingBarLimit = null; 
  private String accommodationBarLimit = null;
  private String whatuniCostPint = null;
  private String accommodationLower = null;
  private String accommodationUpper = null;
  private String maxAccommodationLower = null;
  private String maxAccommodationUpper = null;
  private String accommodationLowerBarLimit = null;
  private String accommodationUpperBarLimit = null;
  private String userJourney = null;
  //End
  private String myhcAccommodationToolTipFlag = null;
  private String richOrSubPageUrl = null;
  
}
