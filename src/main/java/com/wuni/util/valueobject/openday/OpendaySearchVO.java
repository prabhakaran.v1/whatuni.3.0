package com.wuni.util.valueobject.openday;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
@Getter@Setter
public class OpendaySearchVO {

  private String collegeId = null;
  private String collegeName = null;
  private String searchKeyword = null;
  private String monthYear = null;
  private String qualification = null;
  private String searchLocation = null;
  private String pageNo = null;
  private String locationTypeStr = null;
  private String CampusBasedUniId  = null;
  private String russellGroupId = null;
  private String employmentRate = null;
  private String openDate = null;
  private String collegeDisplayName = null;
  private String collegeLogo = null;
  private String openDaysCount = null;
  private String town = null;
  private ArrayList openDaysSRList = null;
  private String locationId = null;
  private String venue = null;
  private String headlines = null;
  //private String suborderItemId = null;
  private String openDayBookingUrl = null;
  private String subOrderItemId = null;
  private String bookingUrl = null;
  private String pastDateFlag = null;
  private String openMonthYear = null;
  private String openDayEventId = null;
  private String userId = null;
  private String openDayExistsFlag = null;
  private String networkQualificationId = null;
  private String websitePrice = null;
  private String providerUrl = null;
  private String monthSearchFlag = null;
  private String collegeLandingUrl = null;
  private String addToFinalChoice = null;
  private String startDateDay;
  private String startDateMonth;
  private String startDateDD;
  private String openDay = null;
  private String openMonth = null;
  private String opendate = null;
  private String startTime = null;
  private String endTime = null;
  private ArrayList subOrderItemIdList = null;
  private String basketFlag = null;
  private String eventCategoryName;
  private String eventCategoryId;
  private String randomNumber;
  private String openDat;
  private String eventDate;
  //
}
