package com.wuni.util.valueobject;

public class CommonVO {
  private String studyModeId = null;
  private String studyModeDesc = null;
  private String studyModeValue = null;
  private String optionId = null;
  private String optionText = null;
  private String categoryCode = null;
  private String subjectName = null;
  private String categoryName = null;
  
  public void flush() {
    this.studyModeId = null;
    this.studyModeDesc = null;
    this.studyModeValue = null;
    this.optionId = null;
    this.optionText = null;
    this.categoryCode = null;
    this.subjectName = null;
    this.categoryName = null;
  }
  
  public void init() {
    this.studyModeId = "";
    this.studyModeDesc = "";
    this.studyModeValue = "";
    this.optionId = "";
    this.optionText = "";
    this.categoryCode = "";
    this.subjectName = "";
    this.categoryName = "";
  }
  
  public void setStudyModeId(String studyModeId) {
    this.studyModeId = studyModeId;
  }
  public String getStudyModeId() {
    return studyModeId;
  }
  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }
  public String getStudyModeDesc() {
    return studyModeDesc;
  }
  public void setStudyModeValue(String studyModeValue) {
    this.studyModeValue = studyModeValue;
  }
  public String getStudyModeValue() {
    return studyModeValue;
  }
  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }
  public String getOptionId() {
    return optionId;
  }
  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }
  public String getOptionText() {
    return optionText;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }
  public String getSubjectName() {
    return subjectName;
  }
  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }
  public String getCategoryName() {
    return categoryName;
  }
}
