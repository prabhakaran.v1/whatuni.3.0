package com.wuni.util.valueobject;

public class CollegeCpeInteractionDetailsWithMediaVO {
  //button details - from db
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeLocation = "";
  //
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String profileId = "";
  private String profileType = "";
  private String advertName = "";
  private String cpeQualificationNetworkId = "";
  private String subOrderEmail = "";
  private String subOrderProspectus = "";
  private String subOrderWebsite = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectusWebform = "";
  private String profileShortDesc = "";
  //media details - from db
  private String mediaId = "";
  private String mediaType = "";
  private String mediaPath = "";
  private String mediaPathThumb = "";
  //media details - customized according to db-media values
  private String isMediaAttached = "";
  private String imageId = "";
  private String imagePath = "";
  private String imagePathThumb = "";
  private String videoId = "";
  private String videoPathFlv = "";
  private String videoPath0001 = "";
  private String videoPath0002 = "";
  private String videoPath0003 = "";
  private String videoPath0004 = "";
  //profile count details - from db
  private String ugInstitutionProfileCount = "";
  private String pgInstitutionProfileCount = "";
  private String ugSubjectProfileCount = "";
  private String pgSubjectProfileCount = "";
  private String hotLineNo = "";
  private String profileDesc = null;
  private String ipExistsFlag = null;

  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.collegeLocation = null;
    this.orderItemId = null;
    this.subOrderItemId = null;
    this.myhcProfileId = null;
    this.profileId = null;
    this.profileType = null;
    this.advertName = null;
    this.cpeQualificationNetworkId = null;
    this.subOrderEmail = null;
    this.subOrderProspectus = null;
    this.subOrderWebsite = null;
    this.subOrderEmailWebform = null;
    this.subOrderProspectusWebform = null;
    this.profileShortDesc = null;
    this.mediaId = null;
    this.mediaType = null;
    this.mediaPath = null;
    this.mediaPathThumb = null;
    this.isMediaAttached = null;
    this.imageId = null;
    this.imagePath = null;
    this.imagePathThumb = null;
    this.videoId = null;
    this.videoPathFlv = null;
    this.videoPath0001 = null;
    this.videoPath0002 = null;
    this.videoPath0003 = null;
    this.videoPath0004 = null;
    this.ugInstitutionProfileCount = null;
    this.pgInstitutionProfileCount = null;
    this.ugSubjectProfileCount = null;
    this.pgSubjectProfileCount = null;
    this.hotLineNo = null;
  }

  public void init() {
    this.orderItemId = "";
    this.subOrderItemId = "";
    this.myhcProfileId = "";
    this.profileId = "";
    this.profileType = "";
    this.advertName = "";
    this.cpeQualificationNetworkId = "";
    this.subOrderEmail = "";
    this.subOrderProspectus = "";
    this.subOrderWebsite = "";
    this.subOrderEmailWebform = "";
    this.subOrderProspectusWebform = "";
    this.mediaId = "";
    this.mediaType = "";
    this.mediaPath = "";
    this.mediaPathThumb = "";
    this.isMediaAttached = "";
    this.imageId = "";
    this.imagePath = "";
    this.imagePathThumb = "";
    this.videoId = "";
    this.videoPathFlv = "";
    this.videoPath0001 = "";
    this.videoPath0002 = "";
    this.videoPath0003 = "";
    this.videoPath0004 = "";
    this.ugInstitutionProfileCount = "";
    this.pgInstitutionProfileCount = "";
    this.ugSubjectProfileCount = "";
    this.pgSubjectProfileCount = "";
    this.hotLineNo = "";
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaPathThumb(String mediaPathThumb) {
    this.mediaPathThumb = mediaPathThumb;
  }

  public String getMediaPathThumb() {
    return mediaPathThumb;
  }

  public void setIsMediaAttached(String isMediaAttached) {
    this.isMediaAttached = isMediaAttached;
  }

  public String getIsMediaAttached() {
    return isMediaAttached;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePathThumb(String imagePathThumb) {
    this.imagePathThumb = imagePathThumb;
  }

  public String getImagePathThumb() {
    return imagePathThumb;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  public String getVideoId() {
    return videoId;
  }

  public void setVideoPathFlv(String videoPathFlv) {
    this.videoPathFlv = videoPathFlv;
  }

  public String getVideoPathFlv() {
    return videoPathFlv;
  }

  public void setVideoPath0001(String videoPath0001) {
    this.videoPath0001 = videoPath0001;
  }

  public String getVideoPath0001() {
    return videoPath0001;
  }

  public void setVideoPath0002(String videoPath0002) {
    this.videoPath0002 = videoPath0002;
  }

  public String getVideoPath0002() {
    return videoPath0002;
  }

  public void setVideoPath0003(String videoPath0003) {
    this.videoPath0003 = videoPath0003;
  }

  public String getVideoPath0003() {
    return videoPath0003;
  }

  public void setVideoPath0004(String videoPath0004) {
    this.videoPath0004 = videoPath0004;
  }

  public String getVideoPath0004() {
    return videoPath0004;
  }

  public void setUgInstitutionProfileCount(String ugInstitutionProfileCount) {
    this.ugInstitutionProfileCount = ugInstitutionProfileCount;
  }

  public String getUgInstitutionProfileCount() {
    return ugInstitutionProfileCount;
  }

  public void setPgInstitutionProfileCount(String pgInstitutionProfileCount) {
    this.pgInstitutionProfileCount = pgInstitutionProfileCount;
  }

  public String getPgInstitutionProfileCount() {
    return pgInstitutionProfileCount;
  }

  public void setUgSubjectProfileCount(String ugSubjectProfileCount) {
    this.ugSubjectProfileCount = ugSubjectProfileCount;
  }

  public String getUgSubjectProfileCount() {
    return ugSubjectProfileCount;
  }

  public void setPgSubjectProfileCount(String pgSubjectProfileCount) {
    this.pgSubjectProfileCount = pgSubjectProfileCount;
  }

  public String getPgSubjectProfileCount() {
    return pgSubjectProfileCount;
  }

  public void setProfileShortDesc(String profileShortDesc) {
    this.profileShortDesc = profileShortDesc;
  }

  public String getProfileShortDesc() {
    return profileShortDesc;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

    public void setHotLineNo(String hotLineNo) {
        this.hotLineNo = hotLineNo;
    }

    public String getHotLineNo() {
        return hotLineNo;
    }
  public void setProfileDesc(String profileDesc) {
    this.profileDesc = profileDesc;
  }
  public String getProfileDesc() {
    return profileDesc;
  }
  public void setIpExistsFlag(String ipExistsFlag) {
    this.ipExistsFlag = ipExistsFlag;
  }
  public String getIpExistsFlag() {
    return ipExistsFlag;
  }
}
