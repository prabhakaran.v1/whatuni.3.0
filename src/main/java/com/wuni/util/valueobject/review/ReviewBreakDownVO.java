package com.wuni.util.valueobject.review;

public class ReviewBreakDownVO {
  
  private String questionTitle = null;
  private String fiveStarPercent = null;
  private String fourStarPercent = null;
  private String threeStarPercent = null;
  private String twoStarPercent = null;
  private String oneStarPercent = null;
  private String rating = null;
  private String reviewCount = null;
  private String reviewExact = null;

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setFiveStarPercent(String fiveStarPercent) {
        this.fiveStarPercent = fiveStarPercent;
    }

    public String getFiveStarPercent() {
        return fiveStarPercent;
    }

    public void setFourStarPercent(String fourStarPercent) {
        this.fourStarPercent = fourStarPercent;
    }

    public String getFourStarPercent() {
        return fourStarPercent;
    }

    public void setThreeStarPercent(String threeStarPercent) {
        this.threeStarPercent = threeStarPercent;
    }

    public String getThreeStarPercent() {
        return threeStarPercent;
    }

    public void setTwoStarPercent(String twoStarPercent) {
        this.twoStarPercent = twoStarPercent;
    }

    public String getTwoStarPercent() {
        return twoStarPercent;
    }

    public void setOneStarPercent(String oneStarPercent) {
        this.oneStarPercent = oneStarPercent;
    }

    public String getOneStarPercent() {
        return oneStarPercent;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewExact(String reviewExact) {
        this.reviewExact = reviewExact;
    }

    public String getReviewExact() {
        return reviewExact;
    }
}
