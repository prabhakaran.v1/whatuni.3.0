package com.wuni.util.valueobject.review;

import java.util.ArrayList;

public class UserReviewsVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String reviewId = null;
  private String reviewTitle = null;
  private String reviewerName = null;
  private String reviewDesc = null;
  private String reviewRatings = null;  
  private String reviewDetailsURL = null;  
  private String userId = null;    
  private String userName = null;
  private String courseName = null;
  private String courseDeletedFlag = null;
  private String courseId = null;
  private String overallRating = null;
  private String overallRatingComments = null;
  private String viewMore = null;
  private String uniHomeURL = null;
  private String courseDetailsURL = null;
  private String createdDate = null; 
  private String studyLevelText = null;  
  private String reviewQuestionId = null;
  private String questionTitle = null;
  private String answer = null;
  private String rating = null;
  private ArrayList userMoreReviewaList = null;
  private String overallQuesDesc = "";
  private String questionDesc = null;
  private String gender = null;
  private String seoStudyLevelText = null;
  private String reviewText = null;
  private String userNameInitial = null;
  private String userNameInitialColourcode = null;
  private String reviewDate = null;
  private String reviewCount = null;
  private String collegeLandingUrl = null;
  private String courseDetailsReviewURL = null;
  
  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.reviewId = null;
    this.reviewTitle = null;
    this.reviewerName = null;
    this.reviewDesc = null;
    this.reviewRatings = null;
    this.reviewDetailsURL = null;
  }

  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.reviewId = "";
    this.reviewTitle = "";
    this.reviewerName = "";
    this.reviewDesc = "";
    this.reviewRatings = "";
    this.reviewDetailsURL = "";
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  public String getReviewTitle() {
    return reviewTitle;
  }

  public void setReviewerName(String reviewerName) {
    this.reviewerName = reviewerName;
  }

  public String getReviewerName() {
    return reviewerName;
  }

  public void setReviewDesc(String reviewDesc) {
    this.reviewDesc = reviewDesc;
  }

  public String getReviewDesc() {
    return reviewDesc;
  }

  public void setReviewRatings(String reviewRatings) {
    this.reviewRatings = reviewRatings;
  }

  public String getReviewRatings() {
    return reviewRatings;
  }

  public void setReviewDetailsURL(String reviewDetailsURL) {
    this.reviewDetailsURL = reviewDetailsURL;
  }

  public String getReviewDetailsURL() {
    return reviewDetailsURL;
  }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseDeletedFlag(String courseDeletedFlag) {
        this.courseDeletedFlag = courseDeletedFlag;
    }

    public String getCourseDeletedFlag() {
        return courseDeletedFlag;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setOverallRating(String overallRating) {
        this.overallRating = overallRating;
    }

    public String getOverallRating() {
        return overallRating;
    }

    public void setOverallRatingComments(String overallRatingComments) {
        this.overallRatingComments = overallRatingComments;
    }

    public String getOverallRatingComments() {
        return overallRatingComments;
    }

    public void setViewMore(String viewMore) {
        this.viewMore = viewMore;
    }

    public String getViewMore() {
        return viewMore;
    }

    public void setUniHomeURL(String uniHomeURL) {
        this.uniHomeURL = uniHomeURL;
    }

    public String getUniHomeURL() {
        return uniHomeURL;
    }

    public void setCourseDetailsURL(String courseDetailsURL) {
        this.courseDetailsURL = courseDetailsURL;
    }

    public String getCourseDetailsURL() {
        return courseDetailsURL;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setStudyLevelText(String studyLevelText) {
        this.studyLevelText = studyLevelText;
    }

    public String getStudyLevelText() {
        return studyLevelText;
    }


    public void setReviewQuestionId(String reviewQuestionId) {
        this.reviewQuestionId = reviewQuestionId;
    }

    public String getReviewQuestionId() {
        return reviewQuestionId;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setUserMoreReviewaList(ArrayList userMoreReviewaList) {
        this.userMoreReviewaList = userMoreReviewaList;
    }

    public ArrayList getUserMoreReviewaList() {
        return userMoreReviewaList;
    }

  public void setOverallQuesDesc(String overallQuesDesc) {
    this.overallQuesDesc = overallQuesDesc;
  }

  public String getOverallQuesDesc() {
    return overallQuesDesc;
  }

  public void setQuestionDesc(String questionDesc) {
    this.questionDesc = questionDesc;
  }

  public String getQuestionDesc() {
    return questionDesc;
  }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setSeoStudyLevelText(String seoStudyLevelText) {
        this.seoStudyLevelText = seoStudyLevelText;
    }

    public String getSeoStudyLevelText() {
        return seoStudyLevelText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setUserNameInitial(String userNameInitial) {
        this.userNameInitial = userNameInitial;
    }

    public String getUserNameInitial() {
        return userNameInitial;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setUserNameInitialColourcode(String userNameInitialColourcode) {
      this.userNameInitialColourcode = userNameInitialColourcode;
    }

    public String getUserNameInitialColourcode() {
      return userNameInitialColourcode;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setCollegeLandingUrl(String collegeLandingUrl) {
        this.collegeLandingUrl = collegeLandingUrl;
    }

    public String getCollegeLandingUrl() {
        return collegeLandingUrl;
    }

    public void setCourseDetailsReviewURL(String courseDetailsReviewURL) {
        this.courseDetailsReviewURL = courseDetailsReviewURL;
    }

    public String getCourseDetailsReviewURL() {
        return courseDetailsReviewURL;
    }
}
