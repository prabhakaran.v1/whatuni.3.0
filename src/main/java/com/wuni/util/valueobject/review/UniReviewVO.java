package com.wuni.util.valueobject.review;

public class UniReviewVO {
  private String star = null;
  private String ratingPercent = null;
  private String reviewCount = null;
  public void flush() {
    this.star = null;
    this.ratingPercent = null;
    this.reviewCount = null;
  }
  public void init() {
    this.star = "";
    this.ratingPercent = "";
    this.reviewCount = "";
  }
  public void setStar(String star) {
    this.star = star;
  }
  public String getStar() {
    return star;
  }
  public void setRatingPercent(String ratingPercent) {
    this.ratingPercent = ratingPercent;
  }
  public String getRatingPercent() {
    return ratingPercent;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
}
