package com.wuni.util.valueobject.review;

import java.util.List;

public class ReviewListVO {
  private String pageNo = null;
  private String showRecordCount = null;
  private String orderBy = null;
  private String subjectCode = null;
  private String searchText = null;
  private String reviewQuestionId = null;
  private String seoQuestionText = null;
  private String reviewQuestion = null;
  private String overallRating = null;
  private String collegeId = null;
  private String maxRating = null;
  private String reviewAnswer = null;
  private String userId = "";
  private String basketId = "";
  private String reviewId = "";
    
  private String reviewid = "";  
  private String reviewtitle = "";
  private String collegename = "";
  private String collegeNameDisplay = "";
  private String username = "";
  private String coursename = "";
  private String courseDeletedFlag = "";
  private String courseId = "";
  private String overallrating = "";
  private String overallratingcomment = "";
  private String createdDate = "";
  private String seoStudyLevelText = "";  
  private String readDate = "";
  private String reviewCount = "";
  private List viewMoreRes = null;  
  private String pageName = "";
  private String questionDesc = null;
  private String studyLevel = null;
  
  public String getStudyLevel() {
	return studyLevel;
}
public void setStudyLevel(String studyLevel) {
	this.studyLevel = studyLevel;
}
public void init() {
    this.reviewQuestionId = "";
    this.seoQuestionText = "";
    this.reviewQuestion = "";
    this.overallRating = "";
    this.collegeId = "";
    this.maxRating = "";
  }
  public void flush() {
    this.reviewQuestionId = null;
    this.seoQuestionText = null;
    this.reviewQuestion = null;
    this.overallRating = null;
    this.collegeId = null;
    this.maxRating = null;
  }
  public void setReviewQuestionId(String reviewQuestionId) {
    this.reviewQuestionId = reviewQuestionId;
  }
  public String getReviewQuestionId() {
    return reviewQuestionId;
  }
  public void setSeoQuestionText(String seoQuestionText) {
    this.seoQuestionText = seoQuestionText;
  }
  public String getSeoQuestionText() {
    return seoQuestionText;
  }
  public void setReviewQuestion(String reviewQuestion) {
    this.reviewQuestion = reviewQuestion;
  }
  public String getReviewQuestion() {
    return reviewQuestion;
  }
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  public String getOverallRating() {
    return overallRating;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setMaxRating(String maxRating) {
    this.maxRating = maxRating;
  }
  public String getMaxRating() {
    return maxRating;
  }
  public void setShowRecordCount(String showRecordCount) {
    this.showRecordCount = showRecordCount;
  }
  public String getShowRecordCount() {
    return showRecordCount;
  }
  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }
  public String getOrderBy() {
    return orderBy;
  }
  public void setSubjectCode(String subjectCode) {
    this.subjectCode = subjectCode;
  }
  public String getSubjectCode() {
    return subjectCode;
  }
  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }
  public String getSearchText() {
    return searchText;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setReviewAnswer(String reviewAnswer) {
    this.reviewAnswer = reviewAnswer;
  }
  public String getReviewAnswer() {
    return reviewAnswer;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }
  public String getReviewId() {
    return reviewId;
  }
  public void setReviewid(String reviewid) {
    this.reviewid = reviewid;
  }
  public String getReviewid() {
    return reviewid;
  }
  public void setReviewtitle(String reviewtitle) {
    this.reviewtitle = reviewtitle;
  }
  public String getReviewtitle() {
    return reviewtitle;
  }
  public void setCollegename(String collegename) {
    this.collegename = collegename;
  }
  public String getCollegename() {
    return collegename;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getUsername() {
    return username;
  }
  public void setCoursename(String coursename) {
    this.coursename = coursename;
  }
  public String getCoursename() {
    return coursename;
  }
  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }
  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setOverallrating(String overallrating) {
    this.overallrating = overallrating;
  }
  public String getOverallrating() {
    return overallrating;
  }
  public void setOverallratingcomment(String overallratingcomment) {
    this.overallratingcomment = overallratingcomment;
  }
  public String getOverallratingcomment() {
    return overallratingcomment;
  }
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }
  public String getCreatedDate() {
    return createdDate;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setReadDate(String readDate) {
    this.readDate = readDate;
  }
  public String getReadDate() {
    return readDate;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setViewMoreRes(List viewMoreRes) {
    this.viewMoreRes = viewMoreRes;
  }
  public List getViewMoreRes() {
    return viewMoreRes;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }

  public void setQuestionDesc(String questionDesc) {
    this.questionDesc = questionDesc;
  }

  public String getQuestionDesc() {
    return questionDesc;
  }

}
