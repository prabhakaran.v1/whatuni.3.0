package com.wuni.util.valueobject.review;

public class CommonPhrasesVO {
  private String commonPhrases = null;

    public void setCommonPhrases(String commonPhrases) {
        this.commonPhrases = commonPhrases;
    }

    public String getCommonPhrases() {
        return commonPhrases;
    }
}
