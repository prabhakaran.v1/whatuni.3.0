package com.wuni.util.valueobject.coursesearch;


public class CourseSearchVO {
  
  //private ArrayList openDaysWeekInfoList = null;
  private String trackUserActionId = null;
  private String actionDate = null;
  private String actionKeyId = null;
  private String actionDetails = null;
  private String actionUrl = null;  
  private String mainSearch = null;
  private String location = null;  
  private String childCatId = null;
  private String childDesc = null;
  private String childCatCode = null;  
  private String parentCatId = null;
  private String parentDesc = null;
  private String parentCatCode = null;
  private String keyword = null;
  private String pageNo = null;
  private String collegeId = null;
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String grade = null;
  private String gradeValue = null;
  private String filter = null;
  private String studyMode = null;
  private String postcode = null;
  private String distance = null;
  private String moduleSearch = null;
  private String clearing = null;
  private String campusType = null;
  private String locationTypeStr = null;
  private String ucasCodeSrch = null;
  private String russellGroup = null;
  private String employmentRate = null;
  private String tariffRate = null;
  private String qualLevel = null;
  private String extraUrl = null;
  private String url = null;
  private String description = null;
  private String subjectGuideUrl = null;
  private String readCount = null;
  private String readCountPercent = null;
  private String totalCount = null;
  private String imagePath = null;
  private String imageName = null;  
  private String userId = null;  
  private String trackSessionId = null;    
  private String studyLevelId = null;  
  private String clearingFlag = null;
  //
  public void setTrackUserActionId(String trackUserActionId) {
    this.trackUserActionId = trackUserActionId;
  }
  public String getTrackUserActionId() {
    return trackUserActionId;
  }
  public void setActionDate(String actionDate) {
    this.actionDate = actionDate;
  }
  public String getActionDate() {
    return actionDate;
  }
  public void setActionKeyId(String actionKeyId) {
    this.actionKeyId = actionKeyId;
  }
  public String getActionKeyId() {
    return actionKeyId;
  }
  public void setActionDetails(String actionDetails) {
    this.actionDetails = actionDetails;
  }
  public String getActionDetails() {
    return actionDetails;
  }
  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }
  public String getActionUrl() {
    return actionUrl;
  }
  public void setMainSearch(String mainSearch) {
    this.mainSearch = mainSearch;
  }
  public String getMainSearch() {
    return mainSearch;
  }
  public void setLocation(String location) {
    this.location = location;
  }
  public String getLocation() {
    return location;
  }
  public void setChildDesc(String childDesc) {
    this.childDesc = childDesc;
  }
  public String getChildDesc() {
    return childDesc;
  }
  public void setChildCatId(String childCatId) {
    this.childCatId = childCatId;
  }
  public String getChildCatId() {
    return childCatId;
  }
  public void setParentCatId(String parentCatId) {
    this.parentCatId = parentCatId;
  }
  public String getParentCatId() {
    return parentCatId;
  }
  public void setParentDesc(String parentDesc) {
    this.parentDesc = parentDesc;
  }
  public String getParentDesc() {
    return parentDesc;
  }
  public void setParentCatCode(String parentCatCode) {
    this.parentCatCode = parentCatCode;
  }
  public String getParentCatCode() {
    return parentCatCode;
  }
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
  public String getKeyword() {
    return keyword;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setGrade(String grade) {
    this.grade = grade;
  }
  public String getGrade() {
    return grade;
  }
  public void setGradeValue(String gradeValue) {
    this.gradeValue = gradeValue;
  }
  public String getGradeValue() {
    return gradeValue;
  }
  public void setFilter(String filter) {
    this.filter = filter;
  }
  public String getFilter() {
    return filter;
  }
  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }
  public String getStudyMode() {
    return studyMode;
  }
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }
  public String getPostcode() {
    return postcode;
  }
  public void setDistance(String distance) {
    this.distance = distance;
  }
  public String getDistance() {
    return distance;
  }
  public void setModuleSearch(String moduleSearch) {
    this.moduleSearch = moduleSearch;
  }
  public String getModuleSearch() {
    return moduleSearch;
  }
  public void setClearing(String clearing) {
    this.clearing = clearing;
  }
  public String getClearing() {
    return clearing;
  }
  public void setCampusType(String campusType) {
    this.campusType = campusType;
  }
  public String getCampusType() {
    return campusType;
  }
  public void setLocationTypeStr(String locationTypeStr) {
    this.locationTypeStr = locationTypeStr;
  }
  public String getLocationTypeStr() {
    return locationTypeStr;
  }
  public void setUcasCodeSrch(String ucasCodeSrch) {
    this.ucasCodeSrch = ucasCodeSrch;
  }
  public String getUcasCodeSrch() {
    return ucasCodeSrch;
  }
  public void setRussellGroup(String russellGroup) {
    this.russellGroup = russellGroup;
  }
  public String getRussellGroup() {
    return russellGroup;
  }
  public void setEmploymentRate(String employmentRate) {
    this.employmentRate = employmentRate;
  }
  public String getEmploymentRate() {
    return employmentRate;
  }
  public void setTariffRate(String tariffRate) {
    this.tariffRate = tariffRate;
  }
  public String getTariffRate() {
    return tariffRate;
  }
  public void setQualLevel(String qualLevel) {
    this.qualLevel = qualLevel;
  }
  public String getQualLevel() {
    return qualLevel;
  }
  public void setExtraUrl(String extraUrl) {
    this.extraUrl = extraUrl;
  }
  public String getExtraUrl() {
    return extraUrl;
  }
  public void setUrl(String url) {
    this.url = url;
  }
  public String getUrl() {
    return url;
  }
  public void setChildCatCode(String childCatCode) {
    this.childCatCode = childCatCode;
  }
  public String getChildCatCode() {
    return childCatCode;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getDescription() {
    return description;
  }
  public void setSubjectGuideUrl(String subjectGuideUrl) {
    this.subjectGuideUrl = subjectGuideUrl;
  }
  public String getSubjectGuideUrl() {
    return subjectGuideUrl;
  }
  public void setReadCount(String readCount) {
    this.readCount = readCount;
  }
  public String getReadCount() {
    return readCount;
  }
  public void setReadCountPercent(String readCountPercent) {
    this.readCountPercent = readCountPercent;
  }
  public String getReadCountPercent() {
    return readCountPercent;
  }
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }
  public String getTotalCount() {
    return totalCount;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
  public void setImageName(String imageName) {
    this.imageName = imageName;
  }
  public String getImageName() {
    return imageName;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }
  public String getStudyLevelId() {
    return studyLevelId;
  }
  public void setClearingFlag(String clearingFlag) {
    this.clearingFlag = clearingFlag;
  }
  public String getClearingFlag() {
    return clearingFlag;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
}
