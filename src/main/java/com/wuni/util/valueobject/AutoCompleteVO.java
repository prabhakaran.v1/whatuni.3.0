package com.wuni.util.valueobject;

public class AutoCompleteVO {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameAlias = "";
  private String collegeNameDisplay = "";
  private String collegeLocation = "";
  //
  private String courseId = "";
  private String courseTitle = "";
  private String courseTitleAlias = "";
  private String courseTitleDisplay = "";
  
  private String schoolId = null;
  private String schoolName = null;
  private String schoolDisplayName = null;
  
  private String browseCatId = null;
  private String subject = null;
  private String catCode = null;
  
  private String url = null;
  
  private String ulQualSubjectCode = null;
  private String ulQualSubjectName = null;
  
  private String ulJobCode = null;
  private String ulJobDesc = null;
  private String ulJobFlag = null;//Added by Indumathi.S for IWantToBe redesign Feb-16-2016
  
  private String ulIndustryCode = null;
  private String ulIndustryDesc = null;
  
  private String uniOfInterestDescription = null;
  private String uniOfInterestCategoryCode = null;
  private String uniUkprn = null;
  private String opendaysUrl = null;  
  private String openDayMessage = null; 
  
  private String userId = null;
  private String description = null;   
  private String ord = null;
  private String trackSessionId = null;
  private String navQual = null;
  private String subjectName = null;
  private String reviewMessage = null;
  private String categoryCode = null;
  private String subjectId = null;
  private String keywordText = null;
  private String courseMessage = null;
  private String coursePageURL = null;
  private String courseCount = null;
  private String qualificationCode = null;
  
  public String getCourseCount() {
    return courseCount;
  }

  public void setCourseCount(String courseCount) {
    this.courseCount = courseCount;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameAlias = null;
    this.collegeNameDisplay = null;
    //
    this.courseId = null;
    this.courseTitle = null;
    this.courseTitleAlias = null;
    this.courseTitleDisplay = null;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameAlias(String collegeNameAlias) {
    this.collegeNameAlias = collegeNameAlias;
  }

  public String getCollegeNameAlias() {
    return collegeNameAlias;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCourseTitleAlias(String courseTitleAlias) {
    this.courseTitleAlias = courseTitleAlias;
  }

  public String getCourseTitleAlias() {
    return courseTitleAlias;
  }

  public void setCourseTitleDisplay(String courseTitleDisplay) {
    this.courseTitleDisplay = courseTitleDisplay;
  }

  public String getCourseTitleDisplay() {
    return courseTitleDisplay;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }
  public String getSchoolId() {
    return schoolId;
  }
  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }
  public String getSchoolName() {
    return schoolName;
  }
  public void setSchoolDisplayName(String schoolDisplayName) {
    this.schoolDisplayName = schoolDisplayName;
  }
  public String getSchoolDisplayName() {
    return schoolDisplayName;
  }
  public void setBrowseCatId(String browseCatId) {
    this.browseCatId = browseCatId;
  }
  public String getBrowseCatId() {
    return browseCatId;
  }
  public void setSubject(String subject) {
    this.subject = subject;
  }
  public String getSubject() {
    return subject;
  }
  public void setCatCode(String catCode) {
    this.catCode = catCode;
  }
  public String getCatCode() {
    return catCode;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUlQualSubjectCode(String ulQualSubjectCode)
  {
    this.ulQualSubjectCode = ulQualSubjectCode;
  }

  public String getUlQualSubjectCode()
  {
    return ulQualSubjectCode;
  }

  public void setUlQualSubjectName(String ulQualSubjectName)
  {
    this.ulQualSubjectName = ulQualSubjectName;
  }

  public String getUlQualSubjectName()
  {
    return ulQualSubjectName;
  }

  public void setUlJobCode(String ulJobCode)
  {
    this.ulJobCode = ulJobCode;
  }

  public String getUlJobCode()
  {
    return ulJobCode;
  }

  public void setUlJobDesc(String ulJobDesc)
  {
    this.ulJobDesc = ulJobDesc;
  }

  public String getUlJobDesc()
  {
    return ulJobDesc;
  }

  public void setUlIndustryCode(String ulIndustryCode)
  {
    this.ulIndustryCode = ulIndustryCode;
  }

  public String getUlIndustryCode()
  {
    return ulIndustryCode;
  }

  public void setUlIndustryDesc(String ulIndustryDesc)
  {
    this.ulIndustryDesc = ulIndustryDesc;
  }

  public String getUlIndustryDesc()
  {
    return ulIndustryDesc;
  }

    public void setUniOfInterestDescription(String uniOfInterestDescription) {
        this.uniOfInterestDescription = uniOfInterestDescription;
    }

    public String getUniOfInterestDescription() {
        return uniOfInterestDescription;
    }

    public void setUniOfInterestCategoryCode(String uniOfInterestCategoryCode) {
        this.uniOfInterestCategoryCode = uniOfInterestCategoryCode;
    }

    public String getUniOfInterestCategoryCode() {
        return uniOfInterestCategoryCode;
    }

    public void setUniUkprn(String uniUkprn) {
        this.uniUkprn = uniUkprn;
    }

    public String getUniUkprn() {
        return uniUkprn;
    }

    public void setOpendaysUrl(String opendaysUrl) {
        this.opendaysUrl = opendaysUrl;
    }

    public String getOpendaysUrl() {
        return opendaysUrl;
    }

  public void setUlJobFlag(String ulJobFlag) {
    this.ulJobFlag = ulJobFlag;
  }

  public String getUlJobFlag() {
    return ulJobFlag;
  }
  
  public void setOpenDayMessage(String openDayMessage) {
        this.openDayMessage = openDayMessage;
    }

    public String getOpenDayMessage() {
        return openDayMessage;
    }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setOrd(String ord) {
    this.ord = ord;
  }

  public String getOrd() {
    return ord;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }

  public String getTrackSessionId() {
    return trackSessionId;
  }

  public void setNavQual(String navQual) {
    this.navQual = navQual;
  }

  public String getNavQual() {
    return navQual;
  }

  public void setReviewMessage(String reviewMessage) {
    this.reviewMessage = reviewMessage;
  }

  public String getReviewMessage() {
    return reviewMessage;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectId() {
        return subjectId;
    }

  public void setKeywordText(String keywordText) {
    this.keywordText = keywordText;
  }

  public String getKeywordText() {
    return keywordText;
  }


public String getCourseMessage() {
   return courseMessage;
}


public void setCourseMessage(String courseMessage) {
   this.courseMessage = courseMessage;
}


public String getCoursePageURL() {
   return coursePageURL;
}


public void setCoursePageURL(String coursePageURL) {
   this.coursePageURL = coursePageURL;
}

}
