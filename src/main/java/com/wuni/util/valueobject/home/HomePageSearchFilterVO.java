package com.wuni.util.valueobject.home;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class HomePageSearchFilterVO {
  private String regionName = null;
  private String regionUrlText = null;
  private String regionId = null;
  private String keytopics = null;
  private String keyTopicUrl = null;
  private String qualDesc = null;
  private String qualCode = null;
  private String urlText = null;
}
