package com.wuni.util.valueobject.home;

public class SpotLightDetailsVO {
 private String orderItemId = null;
 private String collegeId = null;
 private String collegeName = null;
 private String collegeNameDisplay = null;
 private String title = null;
 private String titleUrl = null;
 private String subTitle = null;
 private String subTitleUrl = null;
 private String mediaId = null;
 private String mediaPath = null;
 private String mediaType = null;
 private String imagePath = null;
 private String description = null;
 private String callToActionText = null;
 private String callToActionUrl = null;
 private String position = null; 
 private String videoUrl = null;
 
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getDescription() {
    return description;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getTitle() {
    return title;
  }
  public void setTitleUrl(String titleUrl) {
    this.titleUrl = titleUrl;
  }
  public String getTitleUrl() {
    return titleUrl;
  }
  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }
  public String getSubTitle() {
    return subTitle;
  }
  public void setSubTitleUrl(String subTitleUrl) {
    this.subTitleUrl = subTitleUrl;
  }
  public String getSubTitleUrl() {
    return subTitleUrl;
  }
  public void setCallToActionText(String callToActionText) {
    this.callToActionText = callToActionText;
  }
  public String getCallToActionText() {
    return callToActionText;
  }
  public void setCallToActionUrl(String callToActionUrl) {
    this.callToActionUrl = callToActionUrl;
  }
  public String getCallToActionUrl() {
    return callToActionUrl;
  }
  public void setPosition(String position) {
    this.position = position;
  }
  public String getPosition() {
    return position;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
}
