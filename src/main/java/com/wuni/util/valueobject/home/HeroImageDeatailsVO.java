package com.wuni.util.valueobject.home;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class HeroImageDeatailsVO {

  private String h1Text = null;
  private String h2Text = null;
  private String heroImagePath = null;
}
