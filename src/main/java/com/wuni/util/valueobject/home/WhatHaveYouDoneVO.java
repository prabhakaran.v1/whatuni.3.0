package com.wuni.util.valueobject.home;

public class WhatHaveYouDoneVO {
 private String actionName = null;
 private String actionUrl = null;
 private String shortText = null;
 private String mainText = null;
 private String timeLineId = null;
 
  public void setActionName(String actionName) {
    this.actionName = actionName;
  }
  public String getActionName() {
    return actionName;
  }
  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }
  public String getActionUrl() {
    return actionUrl;
  }
  public void setShortText(String shortText) {
    this.shortText = shortText;
  }
  public String getShortText() {
    return shortText;
  }
  public void setMainText(String mainText) {
    this.mainText = mainText;
  }
  public String getMainText() {
    return mainText;
  }
  public void setTimeLineId(String timeLineId) {
    this.timeLineId = timeLineId;
  }
  public String getTimeLineId() {
    return timeLineId;
  }
}
