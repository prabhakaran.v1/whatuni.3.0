package com.wuni.util.valueobject.home;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecentSearchVO {
    
  private String subjectName = null;  
  private String actionDate = null;
  private String filterName = null;
  private String description = null;
}
