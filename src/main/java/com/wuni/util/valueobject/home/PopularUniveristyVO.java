package com.wuni.util.valueobject.home;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PopularUniveristyVO {
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String logoPath = null;
  private String exactRating = null;
  private String providerResultsUrl = null;
  private String uniHomeUrl = null;
}
