package com.wuni.util.valueobject.search;

public class UniStatsInfoVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String timesRank = null;
  private String fullPartTime = null;
  private String ukInternation = null;
  private String ugpg = null;
  private String schoolMature = null;
  private String noOfStudents = null;
  private String jobOrWork = null;
  private String rankOrdinal = null;
  
  private String uniHomeURL = null;
  private String totalTimesRanking = null; 
  

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setTimesRank(String timesRank) {
    this.timesRank = timesRank;
  }

  public String getTimesRank() {
    return timesRank;
  }

  public void setFullPartTime(String fullPartTime) {
    this.fullPartTime = fullPartTime;
  }

  public String getFullPartTime() {
    return fullPartTime;
  }

  public void setUkInternation(String ukInternation) {
    this.ukInternation = ukInternation;
  }

  public String getUkInternation() {
    return ukInternation;
  }

  public void setUgpg(String ugpg) {
    this.ugpg = ugpg;
  }

  public String getUgpg() {
    return ugpg;
  }

  public void setSchoolMature(String schoolMature) {
    this.schoolMature = schoolMature;
  }

  public String getSchoolMature() {
    return schoolMature;
  }

  public void setNoOfStudents(String noOfStudents) {
    this.noOfStudents = noOfStudents;
  }

  public String getNoOfStudents() {
    return noOfStudents;
  }

  public void setJobOrWork(String jobOrWork) {
    this.jobOrWork = jobOrWork;
  }

  public String getJobOrWork() {
    return jobOrWork;
  }
  public void setRankOrdinal(String rankOrdinal) {
    this.rankOrdinal = rankOrdinal;
  }
  public String getRankOrdinal() {
    return rankOrdinal;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }

  public void setTotalTimesRanking(String totalTimesRanking)
  {
    this.totalTimesRanking = totalTimesRanking;
  }

  public String getTotalTimesRanking()
  {
    return totalTimesRanking;
  }

}
