package com.wuni.util.valueobject.search;

public class LocationRefineVO {

  private String regionName = null;
  private String topRegionName = null;
  private String locationURL = null;
  private String resultCount = null;
  private String locationTextKey = null;
  private String regionTextKey = null;
  
  private String regionSelected = null;
  
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  public String getRegionName() {
    return regionName;
  }
  public void setTopRegionName(String topRegionName) {
    this.topRegionName = topRegionName;
  }
  public String getTopRegionName() {
    return topRegionName;
  }
  public void setLocationURL(String locationURL) {
    this.locationURL = locationURL;
  }
  public String getLocationURL() {
    return locationURL;
  }
  public void setResultCount(String resultCount) {
    this.resultCount = resultCount;
  }
  public String getResultCount() {
    return resultCount;
  }
  public void setLocationTextKey(String locationTextKey) {
    this.locationTextKey = locationTextKey;
  }
  public String getLocationTextKey() {
    return locationTextKey;
  }
  public void setRegionTextKey(String regionTextKey) {
    this.regionTextKey = regionTextKey;
  }
  public String getRegionTextKey() {
    return regionTextKey;
  }
  public void setRegionSelected(String regionSelected) {
    this.regionSelected = regionSelected;
  }
  public String getRegionSelected() {
    return regionSelected;
  }
}
