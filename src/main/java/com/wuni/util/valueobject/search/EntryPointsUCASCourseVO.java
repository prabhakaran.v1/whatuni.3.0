package com.wuni.util.valueobject.search;

public class EntryPointsUCASCourseVO {
  String opportunityId = null;
  String entryDesc = null;
  String entryPoints = null;
  String additionalInfo = null;
  public void setOpportunityId(String opportunityId) {
    this.opportunityId = opportunityId;
  }
  public String getOpportunityId() { 
    return opportunityId;
  }
  public void setEntryDesc(String entryDesc) {
    this.entryDesc = entryDesc;
  }
  public String getEntryDesc() {
    return entryDesc;
  }
  public void setEntryPoints(String entryPoints) {
    this.entryPoints = entryPoints;
  }
  public String getEntryPoints() {
    return entryPoints;
  }
  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }
  public String getAdditionalInfo() {
    return additionalInfo;
  }
}
