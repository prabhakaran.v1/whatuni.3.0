package com.wuni.util.valueobject.search;

public class CourseDetailsVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String collegeLocation = null;
  private String courseId = null;
  private String courseTitle = null;
  private String courseSummaryFull = null;
  private String studyLevelCode = null;
  private String wasThisCourseShortlisted = null;
  private String studyModes = null;
  private String durations = null;
  private String ucasCode = null;
  private String ukFees = null;
  private String departmentOrFaculty = null;
  private String admissionRequirement = null;
  private String internationalAdmissionRequirement = null;
  private String oppotunityId = null;
  private String studyModeId = null;
  private String qualification = null;
  private String startDate = null;
  //
  private String uniHomeUrl = null;
  private String entryText = null;
  private String courseInfoLabel = null;
  private String courseInfoValue  = null;
  private String accreditedBy = null;
  private String appyNowURL = null;
  private String applyNowURLCount = null;
  private String scholarshipCount = null;
  private String cdPageUrl = null;

  //for clearing-2013 work
  private String learnDirectQual = null; 
  private String ucasTariff = null;
  private String clearingPlacesAvail = null;
  private String headline = null; //19_Nov_2013_REL
  private String cityGuideLocation  = null;  //19_Nov_2013_REL
  private String cityGuideDispalyFlag = null;  //19_Nov_2013_REL
  private String courseSummary = null;  //9_DEC_2014_REL added by Priyaa for course details page
  private String myhcProfileId = null; 
  private String overAllRatingExact = null;
  private String overAllRating = null;
  private String reviewCount = null;
  private String profileURL = null;
  private String reviewURL = null;
  private String networkId = null;
  private String shortStartDate = null;
  private String addedToFinalChoice = null;
  private String clearingFlag = null;
  
  //Added for Clearing cd page by Sujitha V 0n 23_JUNE_2020
  private String hybridFlag = null;
  private String compressedFlag = null;
  
  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.collegeLocation = null;
    this.courseId = null;
    this.courseTitle = null;
    this.courseSummaryFull = null;
    this.studyLevelCode = null;
    this.wasThisCourseShortlisted = null;
    this.studyModes = null;
    this.durations = null;
    this.ucasCode = null;
    this.departmentOrFaculty = null;
    this.uniHomeUrl = null;
    this.admissionRequirement = null;
    this.internationalAdmissionRequirement = null;
    this.entryText = null;
  }

  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.collegeLocation = "";
    this.courseId = "";
    this.courseTitle = "";
    this.courseSummaryFull = "";
    this.studyLevelCode = "";
    this.wasThisCourseShortlisted = "";
    this.studyModes = "";
    this.durations = "";
    this.ucasCode = "";
    this.departmentOrFaculty = "";
    this.uniHomeUrl = "";
    this.admissionRequirement = "";
    this.internationalAdmissionRequirement = "";
    this.entryText = "";
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setStudyLevelCode(String studyLevelCode) {
    this.studyLevelCode = studyLevelCode;
  }

  public String getStudyLevelCode() {
    return studyLevelCode;
  }

  public void setWasThisCourseShortlisted(String wasThisCourseShortlisted) {
    this.wasThisCourseShortlisted = wasThisCourseShortlisted;
  }

  public String getWasThisCourseShortlisted() {
    return wasThisCourseShortlisted;
  }

  public void setStudyModes(String studyModes) {
    this.studyModes = studyModes;
  }

  public String getStudyModes() {
    return studyModes;
  }

  public void setDurations(String durations) {
    this.durations = durations;
  }

  public String getDurations() {
    return durations;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }


  public void setDepartmentOrFaculty(String departmentOrFaculty) {
    this.departmentOrFaculty = departmentOrFaculty;
  }

  public String getDepartmentOrFaculty() {
    return departmentOrFaculty;
  }

  public void setUniHomeUrl(String uniHomeUrl) {
    this.uniHomeUrl = uniHomeUrl;
  }

  public String getUniHomeUrl() {
    return uniHomeUrl;
  }

  public void setCourseSummaryFull(String courseSummaryFull) {
    this.courseSummaryFull = courseSummaryFull;
  }

  public String getCourseSummaryFull() {
    return courseSummaryFull;
  }

  public void setAdmissionRequirement(String admissionRequirement) {
    this.admissionRequirement = admissionRequirement;
  }

  public String getAdmissionRequirement() {
    return admissionRequirement;
  }

  public void setInternationalAdmissionRequirement(String internationalAdmissionRequirement) {
    this.internationalAdmissionRequirement = internationalAdmissionRequirement;
  }

  public String getInternationalAdmissionRequirement() {
    return internationalAdmissionRequirement;
  }
    public void setEntryText(String entryText) {
        this.entryText = entryText;
    }
    public String getEntryText() {
        return entryText;
    }

  public void setOppotunityId(String oppotunityId) {
    this.oppotunityId = oppotunityId;
  }

  public String getOppotunityId() {
    return oppotunityId;
  }

  public void setStudyModeId(String studyModeId) {
    this.studyModeId = studyModeId;
  }

  public String getStudyModeId() {
    return studyModeId;
  }

  public void setUkFees(String ukFees) {
    this.ukFees = ukFees;
  }

  public String getUkFees() {
    return ukFees;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setCourseInfoLabel(String courseInfoLabel) {
    this.courseInfoLabel = courseInfoLabel;
  }

  public String getCourseInfoLabel() {
    return courseInfoLabel;
  }

  public void setCourseInfoValue(String courseInfoValue) {
    this.courseInfoValue = courseInfoValue;
  }

  public String getCourseInfoValue() {
    return courseInfoValue;
  }

  public void setAccreditedBy(String accreditedBy) {
    this.accreditedBy = accreditedBy;
  }

  public String getAccreditedBy() {
    return accreditedBy;
  }

  public void setAppyNowURL(String appyNowURL) {
    this.appyNowURL = appyNowURL;
  }

  public String getAppyNowURL() {
    return appyNowURL;
  }

  public void setApplyNowURLCount(String applyNowURLCount) {
    this.applyNowURLCount = applyNowURLCount;
  }

  public String getApplyNowURLCount() {
    return applyNowURLCount;
  }

  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }

  public String getScholarshipCount() {
    return scholarshipCount;
  }
  public void setLearnDirectQual(String learnDirectQual) {
    this.learnDirectQual = learnDirectQual;
  }
  public String getLearnDirectQual() {
    return learnDirectQual;
  }
  public void setUcasTariff(String ucasTariff) {
    this.ucasTariff = ucasTariff;
  }
  public String getUcasTariff() {
    return ucasTariff;
  }
  public void setClearingPlacesAvail(String clearingPlacesAvail) {
    this.clearingPlacesAvail = clearingPlacesAvail;
  }
  public String getClearingPlacesAvail() {
    return clearingPlacesAvail;
  }
  public void setHeadline(String headline) {
    this.headline = headline;
  }
  public String getHeadline() {
    return headline;
  }
  public void setCityGuideDispalyFlag(String cityGuideDispalyFlag) {
    this.cityGuideDispalyFlag = cityGuideDispalyFlag;
  }
  public String getCityGuideDispalyFlag() {
    return cityGuideDispalyFlag;
  }
  public void setCityGuideLocation(String cityGuideLocation) {
    this.cityGuideLocation = cityGuideLocation;
  }
  public String getCityGuideLocation() {
    return cityGuideLocation;
  }
  public void setCourseSummary(String courseSummary) {
    this.courseSummary = courseSummary;
  }
  public String getCourseSummary() {
    return courseSummary;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setOverAllRatingExact(String overAllRatingExact) {
    this.overAllRatingExact = overAllRatingExact;
  }
  public String getOverAllRatingExact() {
    return overAllRatingExact;
  }
  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }
  public String getOverAllRating() {
    return overAllRating;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setProfileURL(String profileURL) {
    this.profileURL = profileURL;
  }
  public String getProfileURL() {
    return profileURL;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setReviewURL(String reviewURL) {
    this.reviewURL = reviewURL;
  }
  public String getReviewURL() {
    return reviewURL;
  }
  public void setShortStartDate(String shortStartDate) {
    this.shortStartDate = shortStartDate;
  }
  public String getShortStartDate() {
    return shortStartDate;
  }
  public void setAddedToFinalChoice(String addedToFinalChoice) {
    this.addedToFinalChoice = addedToFinalChoice;
  }
  public String getAddedToFinalChoice() {
    return addedToFinalChoice;
  }
  public void setCdPageUrl(String cdPageUrl) {
    this.cdPageUrl = cdPageUrl;
  }
  public String getCdPageUrl() {
    return cdPageUrl;
  }
  public void setClearingFlag(String clearingFlag) {
    this.clearingFlag = clearingFlag;
  }
  public String getClearingFlag() {
    return clearingFlag;
  }
  public String getHybridFlag() {
	return hybridFlag;
  }
  public void setHybridFlag(String hybridFlag) {
	this.hybridFlag = hybridFlag;
  }
  public String getCompressedFlag() {
	return compressedFlag;
  }
  public void setCompressedFlag(String compressedFlag) {
	this.compressedFlag = compressedFlag;
  }  
   
}
