package com.wuni.util.valueobject.search;

public class UniSpecificSearchResultsVO {
    //properties from DB
     private String collegeId = "";
     private String collegeName = "";
     private String collegeNameDisplay = "";
     private String overallRating = "";
     private String reviewCount = "";
     private String studentsSatisfaction = "";
     private String studentsJob = "";
     private String collegeLogo = "";
     private String timesRanking = "";
     private String collegeLatitudeAndLongitude = "";
     private String studentJob = "";
     private String genderRatio = "";
     private String timesRankingSub = "";
     private String regionName = "";
     private String NumberOfCoursesForThisCollege = null;
     private String wasThisCollegeShortlisted = null;
     //CPE related properties
     private String orderItemId = "";
     private String subOrderItemId = "";
     private String myhcProfileId = "";
     private String advertName = "";
     private String mediaPath = "";
     private String mediaTypeId = "";
     private String mediaId = "";
     private String thumbnailMediaPath = "";
     private String thumbnailMediaTypeId = "";
     private String interactivityId = "";
     private String whichProfleBoosted = "";
     private String isIpExists = "";
     private String isSpExists = "";
     private String spOrderItemId = "";
     private String ipOrderItemId = "";
     private String mychPrifileIdForSp = "";
     private String mychPrifileIdForIp = "";
     //Button related properties
     private String subOrderWebsite = "";
     private String subOrderEmail = "";
     private String subOrderEmailWebform = "";
     private String subOrderProspectus = "";
     private String subOrderProspectusWebform = "";
     private String nextOpenDay = null;
     private String lattitude = null;
     private String longtitude = null;
     private String regionSpecificUkMapCssClass = null;
     private String cpeQualificationNetworkId = null;
     private String reviewText = null;
     private String whatUniSays = null;
     private String reviewId = null;
     private String thumbNo = null;
     private String SearchThumbnail = null;
     private String collegeLocation = null;
     private String hotLineNo = null;
     private String qlFlag = null;
     private String uniHallFees = null;
     private String webformPrice = null;
     private String websitePrice = null;
     private String overallRatingExact = null;
     private String openDayUrl = null;
     private String profileDesc = null;
     private String questionID = "";
     private String position = "";
     private String imagePath = "";//3_JUN_2014
     private String videoId = "";//3_JUN_2014
     private String videoUrl = "";//3_JUN_2014
     private String videoThumbnailUrl = "";//3_JUN_2014
     private String videoMetaDuration = "";//3_JUN_2014
     private String awardImagePath = "";//22_APR_2014
     private String uniHomeUrl = "";
     private String clearingProfileURL = "";
     
     //Added for Clearing cd page by Sujitha V 0n 23_JUNE_2020
     private String accommodationFlag = null;
     private String scholarshipsFlag = null;
     private String mediaType = null;
     private String thumbnailPath = null;

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }
    public String getCollegeId() {
        return collegeId;
    }
    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }
    public String getCollegeName() {
        return collegeName;
    }
    public void setCollegeNameDisplay(String collegeNameDisplay) {
        this.collegeNameDisplay = collegeNameDisplay;
    }
    public String getCollegeNameDisplay() {
        return collegeNameDisplay;
    }
    public void setOverallRating(String overallRating) {
        this.overallRating = overallRating;
    }
    public String getOverallRating() {
        return overallRating;
    }
    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }
    public String getReviewCount() {
        return reviewCount;
    }
    public void setStudentsSatisfaction(String studentsSatisfaction) {
        this.studentsSatisfaction = studentsSatisfaction;
    }
    public String getStudentsSatisfaction() {
        return studentsSatisfaction;
    }
    public void setStudentsJob(String studentsJob) {
        this.studentsJob = studentsJob;
    }
    public String getStudentsJob() {
        return studentsJob;
    }
    public void setCollegeLogo(String collegeLogo) {
        this.collegeLogo = collegeLogo;
    }
    public String getCollegeLogo() {
        return collegeLogo;
    }
    public void setTimesRanking(String timesRanking) {
        this.timesRanking = timesRanking;
    }
    public String getTimesRanking() {
        return timesRanking;
    }
    public void setCollegeLatitudeAndLongitude(String collegeLatitudeAndLongitude) {
        this.collegeLatitudeAndLongitude = collegeLatitudeAndLongitude;
    }
    public String getCollegeLatitudeAndLongitude() {
        return collegeLatitudeAndLongitude;
    }
    public void setStudentJob(String studentJob) {
        this.studentJob = studentJob;
    }
    public String getStudentJob() {
        return studentJob;
    }
    public void setGenderRatio(String genderRatio) {
        this.genderRatio = genderRatio;
    }
    public String getGenderRatio() {
        return genderRatio;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setMyhcProfileId(String myhcProfileId) {
        this.myhcProfileId = myhcProfileId;
    }

    public String getMyhcProfileId() {
        return myhcProfileId;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setThumbnailMediaPath(String thumbnailMediaPath) {
        this.thumbnailMediaPath = thumbnailMediaPath;
    }

    public String getThumbnailMediaPath() {
        return thumbnailMediaPath;
    }

    public void setThumbnailMediaTypeId(String thumbnailMediaTypeId) {
        this.thumbnailMediaTypeId = thumbnailMediaTypeId;
    }

    public String getThumbnailMediaTypeId() {
        return thumbnailMediaTypeId;
    }

    public void setInteractivityId(String interactivityId) {
        this.interactivityId = interactivityId;
    }

    public String getInteractivityId() {
        return interactivityId;
    }

    public void setWhichProfleBoosted(String whichProfleBoosted) {
        this.whichProfleBoosted = whichProfleBoosted;
    }

    public String getWhichProfleBoosted() {
        return whichProfleBoosted;
    }

    public void setIsIpExists(String isIpExists) {
        this.isIpExists = isIpExists;
    }

    public String getIsIpExists() {
        return isIpExists;
    }

    public void setIsSpExists(String isSpExists) {
        this.isSpExists = isSpExists;
    }

    public String getIsSpExists() {
        return isSpExists;
    }

    public void setSpOrderItemId(String spOrderItemId) {
        this.spOrderItemId = spOrderItemId;
    }

    public String getSpOrderItemId() {
        return spOrderItemId;
    }

    public void setIpOrderItemId(String ipOrderItemId) {
        this.ipOrderItemId = ipOrderItemId;
    }

    public String getIpOrderItemId() {
        return ipOrderItemId;
    }

    public void setMychPrifileIdForSp(String mychPrifileIdForSp) {
        this.mychPrifileIdForSp = mychPrifileIdForSp;
    }

    public String getMychPrifileIdForSp() {
        return mychPrifileIdForSp;
    }

    public void setMychPrifileIdForIp(String mychPrifileIdForIp) {
        this.mychPrifileIdForIp = mychPrifileIdForIp;
    }

    public String getMychPrifileIdForIp() {
        return mychPrifileIdForIp;
    }

    public void setSubOrderWebsite(String subOrderWebsite) {
        this.subOrderWebsite = subOrderWebsite;
    }

    public String getSubOrderWebsite() {
        return subOrderWebsite;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setRegionSpecificUkMapCssClass(String regionSpecificUkMapCssClass) {    
        this.regionSpecificUkMapCssClass = regionSpecificUkMapCssClass;
    }

    public String getRegionSpecificUkMapCssClass() {
        return regionSpecificUkMapCssClass;
    }

    public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
        this.cpeQualificationNetworkId = cpeQualificationNetworkId;
    }

    public String getCpeQualificationNetworkId() {
        return cpeQualificationNetworkId;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setWhatUniSays(String whatUniSays) {
        this.whatUniSays = whatUniSays;
    }

    public String getWhatUniSays() {
        return whatUniSays;
    }

    public void setNumberOfCoursesForThisCollege(String numberOfCoursesForThisCollege) {
        this.NumberOfCoursesForThisCollege = numberOfCoursesForThisCollege;
    }

    public String getNumberOfCoursesForThisCollege() {
        return NumberOfCoursesForThisCollege;
    }

    public void setTimesRankingSub(String timesRankingSub) {
        this.timesRankingSub = timesRankingSub;
    }

    public String getTimesRankingSub() {
        return timesRankingSub;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setNextOpenDay(String nextOpenDay) {
        this.nextOpenDay = nextOpenDay;
    }

    public String getNextOpenDay() {
        return nextOpenDay;
    }

    public void setWasThisCollegeShortlisted(String wasThisCollegeShortlisted) {
        this.wasThisCollegeShortlisted = wasThisCollegeShortlisted;
    }

    public String getWasThisCollegeShortlisted() {
        return wasThisCollegeShortlisted;
    }

    public void setThumbNo(String thumbNo) {
        this.thumbNo = thumbNo;
    }

    public String getThumbNo() {
        return thumbNo;
    }

    public void setSearchThumbnail(String searchThumbnail) {
        this.SearchThumbnail = searchThumbnail;
    }

    public String getSearchThumbnail() {
        return SearchThumbnail;
    }

    public void setCollegeLocation(String collegeLocation) {
        this.collegeLocation = collegeLocation;
    }

    public String getCollegeLocation() {
        return collegeLocation;
    }

    public void setHotLineNo(String hotLineNo) {
        this.hotLineNo = hotLineNo;
    }

    public String getHotLineNo() {
        return hotLineNo;
    }

  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }

  public String getQlFlag() {
    return qlFlag;
  }
  public void setUniHallFees(String uniHallFees)
  {
    this.uniHallFees = uniHallFees;
  }
  public String getUniHallFees()
  {
    return uniHallFees;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setOverallRatingExact(String overallRatingExact) {
    this.overallRatingExact = overallRatingExact;
  }
  public String getOverallRatingExact() {
    return overallRatingExact;
  }
  public void setOpenDayUrl(String openDayUrl) {
    this.openDayUrl = openDayUrl;
  }
  public String getOpenDayUrl() {
    return openDayUrl;
  }
  public void setProfileDesc(String profileDesc) {
    this.profileDesc = profileDesc;
  }
  public String getProfileDesc() {
    return profileDesc;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
  public void setQuestionID(String questionID) {
    this.questionID = questionID;
  }
  public String getQuestionID() {
    return questionID;
  }
  public void setPosition(String position) {
    this.position = position;
  }
  public String getPosition() {
    return position;
  }
  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }
  public String getVideoId() {
    return videoId;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
  public void setVideoThumbnailUrl(String videoThumbnailUrl) {
    this.videoThumbnailUrl = videoThumbnailUrl;
  }
  public String getVideoThumbnailUrl() {
    return videoThumbnailUrl;
  }
  public void setVideoMetaDuration(String videoMetaDuration) {
    this.videoMetaDuration = videoMetaDuration;
  }
  public String getVideoMetaDuration() {
    return videoMetaDuration;
  }
  public void setAwardImagePath(String awardImagePath) {
    this.awardImagePath = awardImagePath;
  }
  public String getAwardImagePath() {
    return awardImagePath;
  }

public String getUniHomeUrl() {
   return uniHomeUrl;
}

public void setUniHomeUrl(String uniHomeUrl) {
   this.uniHomeUrl = uniHomeUrl;
}

public String getClearingProfileURL() {
   return clearingProfileURL;
}

public void setClearingProfileURL(String clearingProfileURL) {
   this.clearingProfileURL = clearingProfileURL;
}

  public String getAccommodationFlag() {
	return accommodationFlag;
  }
  public void setAccommodationFlag(String accommodationFlag) {
	this.accommodationFlag = accommodationFlag;
  }
  public String getScholarshipsFlag() {
	return scholarshipsFlag;
  }
  public void setScholarshipsFlag(String scholarshipsFlag) {
	this.scholarshipsFlag = scholarshipsFlag;
  }
  public String getMediaType() {
	return mediaType;
  }
  public void setMediaType(String mediaType) {
	this.mediaType = mediaType;
  }
  public String getThumbnailPath() {
	return thumbnailPath;
  }
  public void setThumbnailPath(String thumbnailPath) {
	this.thumbnailPath = thumbnailPath;
  }
  
}
