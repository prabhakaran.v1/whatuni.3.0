package com.wuni.util.valueobject.search;

public class TopCoursesVO {
  //from DB
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String courseId = null;
  private String courseTitle = null;
  private String studyLevelDesc = null;
  // cunstom
  private String courseDetailsPageURL = null;
  private String viewAllCoursesURL = null;
  private String collegeLandingUrl = null;
  
  private String orderItemId = null;
  private String subOrderItemId = null;
  private String myhcProfileId = null;
  private String profileId = null;
  private String profileType = null;
  private String advertName = null;
  private String cpeQualificationNetworkId = null;
  private String subOrderEmail = null;
  private String subOrderProspectus = null;
  private String subOrderWebsite = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectusWebform = null;
  private String mediaPath = null;
  private String mediaThumbPath = null;
  private String mediaId = null;
  private String mediaType = null;
  private String videoThumbPath = null;
  
  private String whatUniSays = null;
  private String uniHomeURL = null;
  private String websitePrice = null;
  private String webformPrice = null;
  private String gaCollegeName = null;
  private String shortListFlag = null;
  private String hotLineNo = null;
  private String hotLine = null;
  private String count = null;
  
  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.courseId = "";
    this.courseTitle = "";
    this.studyLevelDesc = "";
    this.courseDetailsPageURL = "";
    this.viewAllCoursesURL = "";
  }

  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.courseId = null;
    this.courseTitle = null;
    this.studyLevelDesc = null;
    this.courseDetailsPageURL = null;
    this.viewAllCoursesURL = null;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCourseDetailsPageURL(String courseDetailsPageURL) {
    this.courseDetailsPageURL = courseDetailsPageURL;
  }

  public String getCourseDetailsPageURL() {
    return courseDetailsPageURL;
  }

  public void setViewAllCoursesURL(String viewAllCoursesURL) {
    this.viewAllCoursesURL = viewAllCoursesURL;
  }

  public String getViewAllCoursesURL() {
    return viewAllCoursesURL;
  }

  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }

  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getProfileId() {
    return profileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }
  public String getAdvertName() {
    return advertName;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }
  public String getMediaThumbPath() {
    return mediaThumbPath;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setVideoThumbPath(String videoThumbPath) {
    this.videoThumbPath = videoThumbPath;
  }
  public String getVideoThumbPath() {
    return videoThumbPath;
  }
  public void setWhatUniSays(String whatUniSays) {
    this.whatUniSays = whatUniSays;
  }
  public String getWhatUniSays() {
    return whatUniSays;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }
  public String getWebformPrice() {
    return webformPrice;
  }
  public void setGaCollegeName(String gaCollegeName) {
    this.gaCollegeName = gaCollegeName;
  }
  public String getGaCollegeName() {
    return gaCollegeName;
  }
  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }
  public String getShortListFlag() {
    return shortListFlag;
  }
  public void setHotLineNo(String hotLineNo) {
    this.hotLineNo = hotLineNo;
  }
  public String getHotLineNo() {
    return hotLineNo;
  }
  public void setHotLine(String hotLine) {
    this.hotLine = hotLine;
  }
  public String getHotLine() {
    return hotLine;
  }
  public void setCount(String count) {
    this.count = count;
  }
  public String getCount() {
    return count;
  }
  public void setCollegeLandingUrl(String collegeLandingUrl) {
    this.collegeLandingUrl = collegeLandingUrl;
  }
  public String getCollegeLandingUrl() {
    return collegeLandingUrl;
  }
}
