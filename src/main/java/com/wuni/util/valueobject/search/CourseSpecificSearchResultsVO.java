    package com.wuni.util.valueobject.search;

import java.util.ArrayList;

public class CourseSpecificSearchResultsVO {
  // properties from DB
  private String collegeId = null;
  private String collegeName = null;
  private String collegeTextKey = null;
  private String collegePRUrl = null;
  private String collegeNameDisplay = null;
  private String numberOfCoursesForThisCollege = null;
  private String learndirQualsForThisCollege = null;
  private String wasThisCollegeShortlisted = null;
  private String collegeLogoPath = null;
  private ArrayList bestMatchCoursesList = null;
  private ArrayList clearingInfoList = null;
  // custom properties based on DB-properties
  private String uniHomeUrl = null;
  private String providerResultPageUrl = null;
  private String scholarShipCount = null;
  private String snipetText = null;
  private String overallRating = null;
  private String scholarShipCnt = null;
  private String overview = null;//Search redesign
  private String russelGroup = null;
  private String additionalCourses = null;
  private String exactRating = null;//25-Mar-2014
  private String jacsSubject = null;
  private String reviewCount = null;
  private String uniReviewsURL = null;
  private String currentRank = null;
  private String totalRank = null;
  private String isSponsoredCollege = null;

  private String networkId = null;
  private String opendaySuborderItemId = null;
  private String openDate = null;
  private String openMonthYear = null;
  private String bookingUrl = null;
  private String totalOpendays = null;
  private String providerId = null;
  
//addded newly
  private String subOrderWebsite = null;
  private String subOrderItemId = null;
  private String orderItemId = null;
  private String hotLineNo = null;
  private String myhcProfileId = null;
  private String profileType = null;
  private String webformPrice = null;
  private String websitePrice = null;
  private String courseRank = null;
  private String subOrderProspectusWebform = null;
  private String ordinalRank = null;
  
  private String sponsoredListFlag = null;
  private String eventCategoryName = null; 
  private String eventCategoryNameGa = null;
  private String eventCategoryId = null;  
  private String matchTypeDim = null;
  
  private String collegeNameSpecialCharRemove = null;
  private String opendayUrl = null;
  
  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.numberOfCoursesForThisCollege = "";
    this.learndirQualsForThisCollege = "";
    this.wasThisCollegeShortlisted = "";
    this.collegeLogoPath = "";
    this.bestMatchCoursesList = new ArrayList();
    this.clearingInfoList = new ArrayList();
    this.uniHomeUrl = "";
    this.providerResultPageUrl = "";
    this.scholarShipCount = "";
    this.snipetText = "";
  }
 

public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.numberOfCoursesForThisCollege = null;
    this.learndirQualsForThisCollege = null;
    this.wasThisCollegeShortlisted = null;
    this.collegeLogoPath = null;
    this.bestMatchCoursesList.clear();
    this.bestMatchCoursesList = null;
    this.clearingInfoList.clear();
    this.clearingInfoList = null;
    //
    this.uniHomeUrl = null;
    this.providerResultPageUrl = null;
    this.scholarShipCount = null;
    this.snipetText = null;
  }

  public String getEventCategoryId() {
	return eventCategoryId;
}


public void setEventCategoryId(String eventCategoryId) {
	this.eventCategoryId = eventCategoryId;
}


public String getEventCategoryName() {
	return eventCategoryName;
  }

  public void setEventCategoryName(String eventCategoryName) {
	this.eventCategoryName = eventCategoryName;
  }
  public String getEventCategoryNameGa() {
	return eventCategoryNameGa;
}


public void setEventCategoryNameGa(String eventCategoryNameGa) {
	this.eventCategoryNameGa = eventCategoryNameGa;
}


  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setWasThisCollegeShortlisted(String wasThisCollegeShortlisted) {
    this.wasThisCollegeShortlisted = wasThisCollegeShortlisted;
  }

  public String getWasThisCollegeShortlisted() {
    return wasThisCollegeShortlisted;
  }

  public void setBestMatchCoursesList(ArrayList bestMatchCoursesList) {
    this.bestMatchCoursesList = bestMatchCoursesList;
  }

  public ArrayList getBestMatchCoursesList() {
    return bestMatchCoursesList;
  }

  public void setClearingInfoList(ArrayList clearingInfoList) {
    this.clearingInfoList = clearingInfoList;
  }

  public ArrayList getClearingInfoList() {
    return clearingInfoList;
  }

  public void setCollegeLogoPath(String collegeLogoPath) {
    this.collegeLogoPath = collegeLogoPath;
  }

  public String getCollegeLogoPath() {
    return collegeLogoPath;
  }

  public void setLearndirQualsForThisCollege(String learndirQualsForThisCollege) {
    this.learndirQualsForThisCollege = learndirQualsForThisCollege;
  }

  public String getLearndirQualsForThisCollege() {
    return learndirQualsForThisCollege;
  }

  public void setNumberOfCoursesForThisCollege(String numberOfCoursesForThisCollege) {
    this.numberOfCoursesForThisCollege = numberOfCoursesForThisCollege;
  }

  public String getNumberOfCoursesForThisCollege() {
    return numberOfCoursesForThisCollege;
  }

  public void setUniHomeUrl(String uniHomeUrl) {
    this.uniHomeUrl = uniHomeUrl;
  }

  public String getUniHomeUrl() {
    return uniHomeUrl;
  }

  public void setProviderResultPageUrl(String providerResultPageUrl) {
    this.providerResultPageUrl = providerResultPageUrl;
  }

  public String getProviderResultPageUrl() {
    return providerResultPageUrl;
  }

  public void setScholarShipCount(String scholarShipCount) {
     this.scholarShipCount = scholarShipCount;
  }

  public String getScholarShipCount() {
    return scholarShipCount;
  }

  public void setSnipetText(String snipetText) {
     this.snipetText = snipetText;
  }

  public String getSnipetText() {
     return snipetText;
  }
  
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  
  public String getOverallRating() {
    return overallRating;
  }
  
  public void setScholarShipCnt(String scholarShipCnt) {
    this.scholarShipCnt = scholarShipCnt;
  }
  
  public String getScholarShipCnt() {
    return scholarShipCnt;
  }
  public void setOverview(String overview) {
    this.overview = overview;
  }
  public String getOverview() {
    return overview;
  }
  public void setRusselGroup(String russelGroup) {
    this.russelGroup = russelGroup;
  }
  public String getRusselGroup() {
    return russelGroup;
  }
  public void setAdditionalCourses(String additionalCourses) {
    this.additionalCourses = additionalCourses;
  }
  public String getAdditionalCourses() {
    return additionalCourses;
  }
  public void setExactRating(String exactRating) {
    this.exactRating = exactRating;
  }
  public String getExactRating() {
    return exactRating;
  }
  public void setJacsSubject(String jacsSubject) {
    this.jacsSubject = jacsSubject;
  }
  public String getJacsSubject() {
    return jacsSubject;
  }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setUniReviewsURL(String uniReviewsURL) {
        this.uniReviewsURL = uniReviewsURL;
    }

    public String getUniReviewsURL() {
        return uniReviewsURL;
    }
  public void setCurrentRank(String currentRank) {
    this.currentRank = currentRank;
  }
  public String getCurrentRank() {
    return currentRank;
  }
  public void setTotalRank(String totalRank) {
    this.totalRank = totalRank;
  }
  public String getTotalRank() {
    return totalRank;
  }
  public void setIsSponsoredCollege(String isSponsoredCollege) {
    this.isSponsoredCollege = isSponsoredCollege;
  }
  public String getIsSponsoredCollege() {
    return isSponsoredCollege;
  }
  public void setCollegeTextKey(String collegeTextKey) {
    this.collegeTextKey = collegeTextKey;
  }
  public String getCollegeTextKey() {
    return collegeTextKey;
  }
  public void setCollegePRUrl(String collegePRUrl) {
    this.collegePRUrl = collegePRUrl;
  }
  public String getCollegePRUrl() {
    return collegePRUrl;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setOpendaySuborderItemId(String opendaySuborderItemId) {
    this.opendaySuborderItemId = opendaySuborderItemId;
  }
  public String getOpendaySuborderItemId() {
    return opendaySuborderItemId;
  }
  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }
  public String getOpenDate() {
    return openDate;
  }
  public void setOpenMonthYear(String openMonthYear) {
    this.openMonthYear = openMonthYear;
  }
  public String getOpenMonthYear() {
    return openMonthYear;
  }
  public void setBookingUrl(String bookingUrl) {
    this.bookingUrl = bookingUrl;
  }
  public String getBookingUrl() {
    return bookingUrl;
  }
  public void setTotalOpendays(String totalOpendays) {
    this.totalOpendays = totalOpendays;
  }
  public String getTotalOpendays() {
    return totalOpendays;
  }

  public void setProviderId(String providerId) {
    this.providerId = providerId;
  }

  public String getProviderId() {
    return providerId;
  }


public String getSubOrderWebsite() {
   return subOrderWebsite;
}


public void setSubOrderWebsite(String subOrderWebsite) {
   this.subOrderWebsite = subOrderWebsite;
}


public String getSubOrderItemId() {
   return subOrderItemId;
}


public void setSubOrderItemId(String subOrderItemId) {
   this.subOrderItemId = subOrderItemId;
}


public String getOrderItemId() {
   return orderItemId;
}


public void setOrderItemId(String orderItemId) {
   this.orderItemId = orderItemId;
}


public String getHotLineNo() {
   return hotLineNo;
}


public void setHotLineNo(String hotLineNo) {
   this.hotLineNo = hotLineNo;
}


public String getMyhcProfileId() {
   return myhcProfileId;
}


public void setMyhcProfileId(String myhcProfileId) {
   this.myhcProfileId = myhcProfileId;
}


public String getProfileType() {
   return profileType;
}


public void setProfileType(String profileType) {
   this.profileType = profileType;
}


public String getWebformPrice() {
   return webformPrice;
}


public void setWebformPrice(String webformPrice) {
   this.webformPrice = webformPrice;
}


public String getWebsitePrice() {
   return websitePrice;
}


public void setWebsitePrice(String websitePrice) {
   this.websitePrice = websitePrice;
}


public String getCourseRank() {
   return courseRank;
}


public void setCourseRank(String courseRank) {
   this.courseRank = courseRank;
}


public String getSubOrderProspectusWebform() {
   return subOrderProspectusWebform;
}


public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
   this.subOrderProspectusWebform = subOrderProspectusWebform;
}


public String getOrdinalRank() {
   return ordinalRank;
}


public void setOrdinalRank(String ordinalRank) {
   this.ordinalRank = ordinalRank;
}

public String getSponsoredListFlag() {
	return sponsoredListFlag;
}

public void setSponsoredListFlag(String sponsoredListFlag) {
	this.sponsoredListFlag = sponsoredListFlag;
}
public String getMatchTypeDim() {
	return matchTypeDim;
}


public void setMatchTypeDim(String matchTypeDim) {
	this.matchTypeDim = matchTypeDim;
}


public String getCollegeNameSpecialCharRemove() {
	return collegeNameSpecialCharRemove;
}


public void setCollegeNameSpecialCharRemove(String collegeNameSpecialCharRemove) {
	this.collegeNameSpecialCharRemove = collegeNameSpecialCharRemove;
}


public String getOpendayUrl() {
	return opendayUrl;
}


public void setOpendayUrl(String opendayUrl) {
	this.opendayUrl = opendayUrl;
}


}
