package com.wuni.util.valueobject.search;

public class LocationsInfoVO {

  private String latitude = null;
  private String longitude = null;
  private String collegeNameDisplay = null;
  private String town = null;
  private String postcode = null;
  private String nearestStationName = null;
  private String distanceFromNearestStation = null;
  private String addLine1 = null;
  private String addLine2 = null;
  private String countryState = null;
  //Added By Sekhar for wu406_20120327 for showing outside london stations.
  private String isInLondon = null;
  private String nearestTubeStation = null;
  private String distanceFromTubeStation = null;
  private String addStr = null;//24_JUN_2014
  private String cityGuideDisplayFlag = null; //9-OCT-2014_REL 
  private String headLine = null;
  private String cityGuideUrl = null;
  private String location = null;

  public void flush() {
    this.latitude = null;
    this.longitude = null;
    this.collegeNameDisplay = null;
    this.town = null;
    this.postcode = null;
    this.nearestStationName = null;
    this.distanceFromNearestStation = null;
    this.addLine1 = null;
    this.addLine2 = null;
    this.countryState = null;
  }

  public void init() {
    this.latitude = "";
    this.longitude = "";
    this.collegeNameDisplay = "";
    this.town = "";
    this.postcode = "";
    this.nearestStationName = "";
    this.distanceFromNearestStation = "";
    this.addLine1 = "";
    this.addLine2 = "";
    this.countryState = "";
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setNearestStationName(String nearestStationName) {
    this.nearestStationName = nearestStationName;
  }

  public String getNearestStationName() {
    return nearestStationName;
  }

  public void setDistanceFromNearestStation(String distanceFromNearestStation) {
    this.distanceFromNearestStation = distanceFromNearestStation;
  }

  public String getDistanceFromNearestStation() {
    return distanceFromNearestStation;
  }

    public void setAddLine1(String addLine1) {
        this.addLine1 = addLine1;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public void setAddLine2(String addLine2) {
        this.addLine2 = addLine2;
    }

    public String getAddLine2() {
        return addLine2;
    }
    public void setCountryState(String countryState) {
        this.countryState = countryState;
    }
    public String getCountryState() {
        return countryState;
    }

    public void setIsInLondon(String isInLondon) {
        this.isInLondon = isInLondon;
    }

    public String getIsInLondon() {
        return isInLondon;
    }

    public void setNearestTubeStation(String nearestTubeStation) {
        this.nearestTubeStation = nearestTubeStation;
    }

    public String getNearestTubeStation() {
        return nearestTubeStation;
    }

    public void setDistanceFromTubeStation(String distanceFromTubeStation) {
        this.distanceFromTubeStation = distanceFromTubeStation;
    }

    public String getDistanceFromTubeStation() {
        return distanceFromTubeStation;
    }
  public void setAddStr(String addStr) {
    this.addStr = addStr;
  }
  public String getAddStr() {
    return addStr;
  }
  public void setCityGuideDisplayFlag(String cityGuideDisplayFlag) {
    this.cityGuideDisplayFlag = cityGuideDisplayFlag;
  }
  public String getCityGuideDisplayFlag() {
    return cityGuideDisplayFlag;
  }
  public void setHeadLine(String headLine) {
    this.headLine = headLine;
  }
  public String getHeadLine() {
    return headLine;
  }
  public void setLocation(String location) {
    this.location = location;
  }
  public String getLocation() {
    return location;
  }
  public void setCityGuideUrl(String cityGuideUrl) {
    this.cityGuideUrl = cityGuideUrl;
  }
  public String getCityGuideUrl() {
    return cityGuideUrl;
  }
}
