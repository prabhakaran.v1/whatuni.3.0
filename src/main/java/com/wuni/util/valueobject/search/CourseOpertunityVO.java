package com.wuni.util.valueobject.search;

public class CourseOpertunityVO {

  private String courseId = null;
  private String collegeId = null;
  private String domesticPrice = null;
  private String domesticPriceDesc = null;
  private String studyMode = null;
  private String startDate = null;
  private String startDateDesc = null;
  private String duration = null;
  private String durationDesc = null;
  private String isVenueBelongsToLondonBorough = null;
  private String applyNowUrl = null;
  private String applyNowUrlCount = null;
  private String clearingPlacesAvaiable = null;
  private String lastUpdatedDate = null;
  

  public void flush() {
    this.courseId = null;
    this.collegeId = null;
    this.domesticPrice = null;
    this.domesticPriceDesc = null;
    this.studyMode = null;
    this.startDate = null;
    this.startDateDesc = null;
    this.duration = null;
    this.durationDesc = null;
    this.isVenueBelongsToLondonBorough = null;
    this.applyNowUrl = null;
    this.applyNowUrlCount = null;
    this.clearingPlacesAvaiable = null;
    this.lastUpdatedDate = null;
  }

  public void init() {
    this.courseId = "";
    this.collegeId = "";
    this.domesticPrice = "";
    this.domesticPriceDesc = "";
    this.studyMode = "";
    this.startDate = "";
    this.startDateDesc = "";
    this.duration = "";
    this.durationDesc = "";
    this.isVenueBelongsToLondonBorough = "";
    this.applyNowUrl = "";
    this.applyNowUrlCount = "";
    this.clearingPlacesAvaiable = "";
    this.lastUpdatedDate = "";
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getDuration() {
    return duration;
  }

  public void setIsVenueBelongsToLondonBorough(String isVenueBelongsToLondonBorough) {
    this.isVenueBelongsToLondonBorough = isVenueBelongsToLondonBorough;
  }

  public String getIsVenueBelongsToLondonBorough() {
    return isVenueBelongsToLondonBorough;
  }

  public void setStartDateDesc(String startDateDesc) {
    this.startDateDesc = startDateDesc;
  }

  public String getStartDateDesc() {
    return startDateDesc;
  }

  public void setApplyNowUrl(String applyNowUrl) {
    this.applyNowUrl = applyNowUrl;
  }

  public String getApplyNowUrl() {
    return applyNowUrl;
  }

  public void setDomesticPrice(String domesticPrice) {
    this.domesticPrice = domesticPrice;
  }

  public String getDomesticPrice() {
    return domesticPrice;
  }

  public void setDomesticPriceDesc(String domesticPriceDesc) {
    this.domesticPriceDesc = domesticPriceDesc;
  }

  public String getDomesticPriceDesc() {
    return domesticPriceDesc;
  }

  public void setDurationDesc(String durationDesc) {
    this.durationDesc = durationDesc;
  }

  public String getDurationDesc() {
    return durationDesc;
  }

  public void setApplyNowUrlCount(String applyNowUrlCount) {
    this.applyNowUrlCount = applyNowUrlCount;
  }

  public String getApplyNowUrlCount() {
    return applyNowUrlCount;
  }

  public void setClearingPlacesAvaiable(String clearingPlacesAvaiable) {
    this.clearingPlacesAvaiable = clearingPlacesAvaiable;
  }

  public String getClearingPlacesAvaiable() {
    return clearingPlacesAvaiable;
  }

  public void setLastUpdatedDate(String lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getLastUpdatedDate() {
    return lastUpdatedDate;
  }

}
