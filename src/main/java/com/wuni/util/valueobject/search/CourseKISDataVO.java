package com.wuni.util.valueobject.search;
import java.util.List;
public class CourseKISDataVO {
  
  //How You'are Assessed Pod Properties
  private String courseWork = null;
  private String practicalWork = null;
  private String exam = null;
  private String learTeachingURL = null;
  private String financeSupportURL = null;
  private String accomodationLowerPrice = null;
  private String accomodationHigherPrice = null;
  private String scholarshipCount = null;
  private String ukFees = null;
  private String intlFees = null;
  private String feeWaiver = null;
  private String meansSupport = null;
  
  //StudyMode Tab Properties
  private String studyModeId = null;
  private String studyModeDesc = null;
  
  //Opportunities Dropdown Properties
  private String opportunityId = null;
  private String opportunityDesc = null;
  
  //Subject Data 
  private String subjectName = null;
  private String graduates = null;
  private String enrolledStudents = null;
  private String furtherStudy = null;
  private String avgSalaryAfterSixMonths = null;
  private String avgSalaryAfterFourtyMonths = null;
  private String avgSalAfterSixMonthsAtuni = null;
  private String graduatesHelp = null;
  private String enrolledHelp = null;
  private String furtherStudyHelp = null;
  private String salaryHelp = null;
  private List subjectList = null;
  private String kisSubject = null;
  private String heldByPercentage = null;
  private String graduateHelpFlag = null;
  private String enrollHelpFlag = null;
  private String furtherStudyHelpFlag = null;
  private String salaryHelpFlag = null;
  private String ukFeesDesc = null;
  private String intFeesDesc = null;
  private String  twoistooneRatio = null;
  private String dropOutRate = null;
  private String applicantsRate = null;
  private String successfulApplicants = null;
  private String successfulApplicantsStickMen = null;
  private String totalApplicants = null;
  private String totalApplicantsStickMen = null;
  private String regionalFees = null;
  private String euFees = null;
  //Matching Course
  private String matchingCourseURL = null;
  private String percentageSixMonths = null;
  private String percentage40Months = null;
  private String percentageSixMonthsAtUni = null;
  private String continuingCourse = null;
  private String completedCourse = null;
  private String courseContinuation = null;  
  private String intlFeesDuration = null;
  private String intlFeesCurrency = null;
  private String intlFeesState = null;
  private String euFeesDuration = null;
  private String euFeesCurrency = null;
  private String euFeesState = null;
  private String euFeesDesc = null;
  private String engFees = null;
  private String engFeesDuration = null;
  private String engFeesCurrency = null;
  private String engFeesState = null;
  private String engFeesDesc = null;
  private String scotFees = null;
  private String scotFeesDuration = null;
  private String scotFeesCurrency = null;
  private String scotFeesState = null;
  private String scotFeesDesc = null;
  private String walesFees = null;
  private String walesFeesDuration = null;
  private String walesFeesCurrency = null;
  private String walesFeesState = null;
  private String walesFeesDesc = null;
  private String northIreFees = null;
  private String northIreFeesDuration = null;
  private String northIreFeesCurrency = null;
  private String northIreFeesState = null;
  private String northIreFeesDesc = null;
  private String chnlIslFees = null;
  private String chnlIslFeesDuration = null;
  private String chnlIslFeesCurrency = null;
  private String chnlIslFeesState = null;
  private String chnlIslFeesDesc = null;
  private String domesticFees = null;
  private String domesticFeesDuration = null;
  private String domesticFeesCurrency = null;
  private String domesticFeesState = null;
  private String domesticFeesDesc = null;
  private String otherUKFees = null;
  private String otherUKFeesDuration = null;
  private String otherUKFeesCurrency = null;
  private String otherUKFeesState = null;
  private String otherUKFeesDesc = null;
  private String priceUrl = null;
  private String networkId = null;
  private String subOrderItemId = null;
  private String feesType = null;
  private String fees = null;
  private String duration = null;
  private String currency = null;
  private String feesDesc = null;
  private String state = null;
  private String url = null;
  private String sequenceNo = null;
  private String feesTypeTooltip = null;
  
  public void setCourseWork(String courseWork) {
    this.courseWork = courseWork;
  }

  public String getCourseWork() {
    return courseWork;
  }

  public void setPracticalWork(String practicalWork) {
    this.practicalWork = practicalWork;
  }

  public String getPracticalWork() {
    return practicalWork;
  }

  public void setExam(String exam) {
    this.exam = exam;
  }

  public String getExam() {
    return exam;
  }

  public void setLearTeachingURL(String learTeachingURL) {
    this.learTeachingURL = learTeachingURL;
  }

  public String getLearTeachingURL() {
    return learTeachingURL;
  }

  public void setAccomodationLowerPrice(String accomodationLowerPrice) {
    this.accomodationLowerPrice = accomodationLowerPrice;
  }

  public String getAccomodationLowerPrice() {
    return accomodationLowerPrice;
  }

  public void setAccomodationHigherPrice(String accomodationHigherPrice) {
    this.accomodationHigherPrice = accomodationHigherPrice;
  }

  public String getAccomodationHigherPrice() {
    return accomodationHigherPrice;
  }

  public void setFinanceSupportURL(String financeSupportURL) {
    this.financeSupportURL = financeSupportURL;
  }

  public String getFinanceSupportURL() {
    return financeSupportURL;
  }

  public void setStudyModeId(String studyModeId) {
    this.studyModeId = studyModeId;
  }

  public String getStudyModeId() {
    return studyModeId;
  }

  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }

  public String getStudyModeDesc() {
    return studyModeDesc;
  }

  public void setOpportunityId(String opportunityId) {
    this.opportunityId = opportunityId;
  }

  public String getOpportunityId() {
    return opportunityId;
  }

  public void setOpportunityDesc(String opportunityDesc) {
    this.opportunityDesc = opportunityDesc;
  }

  public String getOpportunityDesc() {
    return opportunityDesc;
  }

  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }

  public String getScholarshipCount() {
    return scholarshipCount;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setGraduates(String graduates) {
    this.graduates = graduates;
  }

  public String getGraduates() {
    return graduates;
  }

  public void setEnrolledStudents(String enrolledStudents) {
    this.enrolledStudents = enrolledStudents;
  }

  public String getEnrolledStudents() {
    return enrolledStudents;
  }

  public void setFurtherStudy(String furtherStudy) {
    this.furtherStudy = furtherStudy;
  }

  public String getFurtherStudy() {
    return furtherStudy;
  }

  public void setAvgSalaryAfterSixMonths(String avgSalaryAfterSixMonths) {
    this.avgSalaryAfterSixMonths = avgSalaryAfterSixMonths;
  }

  public String getAvgSalaryAfterSixMonths() {
    return avgSalaryAfterSixMonths;
  }

  public void setAvgSalaryAfterFourtyMonths(String avgSalaryAfterFourtyMonths) {
    this.avgSalaryAfterFourtyMonths = avgSalaryAfterFourtyMonths;
  }

  public String getAvgSalaryAfterFourtyMonths() {
    return avgSalaryAfterFourtyMonths;
  }

  public void setAvgSalAfterSixMonthsAtuni(String avgSalAfterSixMonthsAtuni) {
    this.avgSalAfterSixMonthsAtuni = avgSalAfterSixMonthsAtuni;
  }

  public String getAvgSalAfterSixMonthsAtuni() {
    return avgSalAfterSixMonthsAtuni;
  }

  public void setGraduatesHelp(String graduatesHelp) {
    this.graduatesHelp = graduatesHelp;
  }

  public String getGraduatesHelp() {
    return graduatesHelp;
  }

  public void setEnrolledHelp(String enrolledHelp) {
    this.enrolledHelp = enrolledHelp;
  }

  public String getEnrolledHelp() {
    return enrolledHelp;
  }

  public void setFurtherStudyHelp(String furtherStudyHelp) {
    this.furtherStudyHelp = furtherStudyHelp;
  }

  public String getFurtherStudyHelp() {
    return furtherStudyHelp;
  }

  public void setSalaryHelp(String salaryHelp) {
    this.salaryHelp = salaryHelp;
  }

  public String getSalaryHelp() {
    return salaryHelp;
  }

  public void setKisSubject(String kisSubject) {
    this.kisSubject = kisSubject;
  }

  public String getKisSubject() {
    return kisSubject;
  }

  public void setHeldByPercentage(String heldByPercentage) {
    this.heldByPercentage = heldByPercentage;
  }

  public String getHeldByPercentage() {
    return heldByPercentage;
  }

  public void setSubjectList(List subjectList) {
    this.subjectList = subjectList;
  }

  public List getSubjectList() {
    return subjectList;
  }

  public void setGraduateHelpFlag(String graduateHelpFlag) {
    this.graduateHelpFlag = graduateHelpFlag;
  }

  public String getGraduateHelpFlag() {
    return graduateHelpFlag;
  }

  public void setEnrollHelpFlag(String enrollHelpFlag) {
    this.enrollHelpFlag = enrollHelpFlag;
  }

  public String getEnrollHelpFlag() {
    return enrollHelpFlag;
  }

  public void setFurtherStudyHelpFlag(String furtherStudyHelpFlag) {
    this.furtherStudyHelpFlag = furtherStudyHelpFlag;
  }

  public String getFurtherStudyHelpFlag() {
    return furtherStudyHelpFlag;
  }

  public void setSalaryHelpFlag(String salaryHelpFlag) {
    this.salaryHelpFlag = salaryHelpFlag;
  }

  public String getSalaryHelpFlag() {
    return salaryHelpFlag;
  }

  public void setUkFees(String ukFees) {
    this.ukFees = ukFees;
  }

  public String getUkFees() {
    return ukFees;
  }

  public void setIntlFees(String intlFees) {
    this.intlFees = intlFees;
  }

  public String getIntlFees() {
    return intlFees;
  }

  public void setFeeWaiver(String feeWaiver) {
    this.feeWaiver = feeWaiver;
  }

  public String getFeeWaiver() {
    return feeWaiver;
  }

  public void setMeansSupport(String meansSupport) {
    this.meansSupport = meansSupport;
  }

  public String getMeansSupport() {
    return meansSupport;
  }
  public void setMatchingCourseURL(String matchingCourseURL) {
    this.matchingCourseURL = matchingCourseURL;
  }
  public String getMatchingCourseURL() {
    return matchingCourseURL;
  }
  public void setUkFeesDesc(String ukFeesDesc) {
    this.ukFeesDesc = ukFeesDesc;
  }
  public String getUkFeesDesc() {
    return ukFeesDesc;
  }
  public void setIntFeesDesc(String intFeesDesc) {
    this.intFeesDesc = intFeesDesc;
  }
  public String getIntFeesDesc() {
    return intFeesDesc;
  }
  public void setTwoistooneRatio(String twoistooneRatio) {
    this.twoistooneRatio = twoistooneRatio;
  }
  public String getTwoistooneRatio() {
    return twoistooneRatio;
  }
  public void setDropOutRate(String dropOutRate) {
    this.dropOutRate = dropOutRate;
  }
  public String getDropOutRate() {
    return dropOutRate;
  }
  public void setTotalApplicants(String totalApplicants) {
    this.totalApplicants = totalApplicants;
  }
  public String getTotalApplicants() {
    return totalApplicants;
  }
  public void setApplicantsRate(String applicantsRate) {
    this.applicantsRate = applicantsRate;
  }
  public String getApplicantsRate() {
    return applicantsRate;
  }
  public void setSuccessfulApplicants(String successfulApplicants) {
    this.successfulApplicants = successfulApplicants;
  }
  public String getSuccessfulApplicants() {
    return successfulApplicants;
  }
  public void setSuccessfulApplicantsStickMen(String successfulApplicantsStickMen) {
    this.successfulApplicantsStickMen = successfulApplicantsStickMen;
  }
  public String getSuccessfulApplicantsStickMen() {
    return successfulApplicantsStickMen;
  }
  public void setTotalApplicantsStickMen(String totalApplicantsStickMen) {
    this.totalApplicantsStickMen = totalApplicantsStickMen;
  }
  public String getTotalApplicantsStickMen() {
    return totalApplicantsStickMen;
  }
  public void setRegionalFees(String regionalFees) {
    this.regionalFees = regionalFees;
  }
  public String getRegionalFees() {
    return regionalFees;
  }
  public void setEuFees(String euFees) {
    this.euFees = euFees;
  }
  public String getEuFees() {
    return euFees;
  }
  public void setPercentageSixMonths(String percentageSixMonths) {
    this.percentageSixMonths = percentageSixMonths;
  }
  public String getPercentageSixMonths() {
    return percentageSixMonths;
  }
  public void setPercentage40Months(String percentage40Months) {
    this.percentage40Months = percentage40Months;
  }
  public String getPercentage40Months() {
    return percentage40Months;
  }
  public void setPercentageSixMonthsAtUni(String percentageSixMonthsAtUni) {
    this.percentageSixMonthsAtUni = percentageSixMonthsAtUni;
  }
  public String getPercentageSixMonthsAtUni() {
    return percentageSixMonthsAtUni;
  }
  public void setContinuingCourse(String continuingCourse) {
    this.continuingCourse = continuingCourse;
  }
  public String getContinuingCourse() {
    return continuingCourse;
  }
  public void setCompletedCourse(String completedCourse) {
    this.completedCourse = completedCourse;
  }
  public String getCompletedCourse() {
    return completedCourse;
  }
  public void setCourseContinuation(String courseContinuation) {
    this.courseContinuation = courseContinuation;
  }
  public String getCourseContinuation() {
    return courseContinuation;
  }

    public void setIntlFeesDuration(String intlFeesDuration) {
        this.intlFeesDuration = intlFeesDuration;
    }

    public String getIntlFeesDuration() {
        return intlFeesDuration;
    }

    public void setIntlFeesCurrency(String intlFeesCurrency) {
        this.intlFeesCurrency = intlFeesCurrency;
    }

    public String getIntlFeesCurrency() {
        return intlFeesCurrency;
    }

    public void setIntlFeesState(String intlFeesState) {
        this.intlFeesState = intlFeesState;
    }

    public String getIntlFeesState() {
        return intlFeesState;
    }

    public void setEuFeesDuration(String euFeesDuration) {
        this.euFeesDuration = euFeesDuration;
    }

    public String getEuFeesDuration() {
        return euFeesDuration;
    }

    public void setEuFeesCurrency(String euFeesCurrency) {
        this.euFeesCurrency = euFeesCurrency;
    }

    public String getEuFeesCurrency() {
        return euFeesCurrency;
    }

    public void setEuFeesState(String euFeesState) {
        this.euFeesState = euFeesState;
    }

    public String getEuFeesState() {
        return euFeesState;
    }

    public void setEuFeesDesc(String euFeesDesc) {
        this.euFeesDesc = euFeesDesc;
    }

    public String getEuFeesDesc() {
        return euFeesDesc;
    }

    public void setEngFees(String engFees) {
        this.engFees = engFees;
    }

    public String getEngFees() {
        return engFees;
    }

    public void setEngFeesDuration(String engFeesDuration) {
        this.engFeesDuration = engFeesDuration;
    }

    public String getEngFeesDuration() {
        return engFeesDuration;
    }

    public void setEngFeesCurrency(String engFeesCurrency) {
        this.engFeesCurrency = engFeesCurrency;
    }

    public String getEngFeesCurrency() {
        return engFeesCurrency;
    }

    public void setEngFeesState(String engFeesState) {
        this.engFeesState = engFeesState;
    }

    public String getEngFeesState() {
        return engFeesState;
    }

    public void setEngFeesDesc(String engFeesDesc) {
        this.engFeesDesc = engFeesDesc;
    }

    public String getEngFeesDesc() {
        return engFeesDesc;
    }

    public void setScotFees(String scotFees) {
        this.scotFees = scotFees;
    }

    public String getScotFees() {
        return scotFees;
    }

    public void setScotFeesDuration(String scotFeesDuration) {
        this.scotFeesDuration = scotFeesDuration;
    }

    public String getScotFeesDuration() {
        return scotFeesDuration;
    }

    public void setScotFeesCurrency(String scotFeesCurrency) {
        this.scotFeesCurrency = scotFeesCurrency;
    }

    public String getScotFeesCurrency() {
        return scotFeesCurrency;
    }

    public void setScotFeesState(String scotFeesState) {
        this.scotFeesState = scotFeesState;
    }

    public String getScotFeesState() {
        return scotFeesState;
    }

    public void setScotFeesDesc(String scotFeesDesc) {
        this.scotFeesDesc = scotFeesDesc;
    }

    public String getScotFeesDesc() {
        return scotFeesDesc;
    }

    public void setWalesFees(String walesFees) {
        this.walesFees = walesFees;
    }

    public String getWalesFees() {
        return walesFees;
    }

    public void setWalesFeesDuration(String walesFeesDuration) {
        this.walesFeesDuration = walesFeesDuration;
    }

    public String getWalesFeesDuration() {
        return walesFeesDuration;
    }

    public void setWalesFeesCurrency(String walesFeesCurrency) {
        this.walesFeesCurrency = walesFeesCurrency;
    }

    public String getWalesFeesCurrency() {
        return walesFeesCurrency;
    }

    public void setWalesFeesState(String walesFeesState) {
        this.walesFeesState = walesFeesState;
    }

    public String getWalesFeesState() {
        return walesFeesState;
    }

    public void setWalesFeesDesc(String walesFeesDesc) {
        this.walesFeesDesc = walesFeesDesc;
    }

    public String getWalesFeesDesc() {
        return walesFeesDesc;
    }

    public void setNorthIreFees(String northIreFees) {
        this.northIreFees = northIreFees;
    }

    public String getNorthIreFees() {
        return northIreFees;
    }

    public void setNorthIreFeesDuration(String northIreFeesDuration) {
        this.northIreFeesDuration = northIreFeesDuration;
    }

    public String getNorthIreFeesDuration() {
        return northIreFeesDuration;
    }

    public void setNorthIreFeesCurrency(String northIreFeesCurrency) {
        this.northIreFeesCurrency = northIreFeesCurrency;
    }

    public String getNorthIreFeesCurrency() {
        return northIreFeesCurrency;
    }

    public void setNorthIreFeesState(String northIreFeesState) {
        this.northIreFeesState = northIreFeesState;
    }

    public String getNorthIreFeesState() {
        return northIreFeesState;
    }

    public void setNorthIreFeesDesc(String northIreFeesDesc) {
        this.northIreFeesDesc = northIreFeesDesc;
    }

    public String getNorthIreFeesDesc() {
        return northIreFeesDesc;
    }

    public void setChnlIslFees(String chnlIslFees) {
        this.chnlIslFees = chnlIslFees;
    }

    public String getChnlIslFees() {
        return chnlIslFees;
    }

    public void setChnlIslFeesDuration(String chnlIslFeesDuration) {
        this.chnlIslFeesDuration = chnlIslFeesDuration;
    }

    public String getChnlIslFeesDuration() {
        return chnlIslFeesDuration;
    }

    public void setChnlIslFeesCurrency(String chnlIslFeesCurrency) {
        this.chnlIslFeesCurrency = chnlIslFeesCurrency;
    }

    public String getChnlIslFeesCurrency() {
        return chnlIslFeesCurrency;
    }

    public void setChnlIslFeesState(String chnlIslFeesState) {
        this.chnlIslFeesState = chnlIslFeesState;
    }

    public String getChnlIslFeesState() {
        return chnlIslFeesState;
    }

    public void setChnlIslFeesDesc(String chnlIslFeesDesc) {
        this.chnlIslFeesDesc = chnlIslFeesDesc;
    }

    public String getChnlIslFeesDesc() {
        return chnlIslFeesDesc;
    }

    public void setDomesticFees(String domesticFees) {
        this.domesticFees = domesticFees;
    }

    public String getDomesticFees() {
        return domesticFees;
    }

    public void setDomesticFeesDuration(String domesticFeesDuration) {
        this.domesticFeesDuration = domesticFeesDuration;
    }

    public String getDomesticFeesDuration() {
        return domesticFeesDuration;
    }

    public void setDomesticFeesCurrency(String domesticFeesCurrency) {
        this.domesticFeesCurrency = domesticFeesCurrency;
    }

    public String getDomesticFeesCurrency() {
        return domesticFeesCurrency;
    }

    public void setDomesticFeesState(String domesticFeesState) {
        this.domesticFeesState = domesticFeesState;
    }

    public String getDomesticFeesState() {
        return domesticFeesState;
    }

    public void setDomesticFeesDesc(String domesticFeesDesc) {
        this.domesticFeesDesc = domesticFeesDesc;
    }

    public String getDomesticFeesDesc() {
        return domesticFeesDesc;
    }

    public void setOtherUKFees(String otherUKFees) {
        this.otherUKFees = otherUKFees;
    }

    public String getOtherUKFees() {
        return otherUKFees;
    }

    public void setOtherUKFeesDuration(String otherUKFeesDuration) {
        this.otherUKFeesDuration = otherUKFeesDuration;
    }

    public String getOtherUKFeesDuration() {
        return otherUKFeesDuration;
    }

    public void setOtherUKFeesCurrency(String otherUKFeesCurrency) {
        this.otherUKFeesCurrency = otherUKFeesCurrency;
    }

    public String getOtherUKFeesCurrency() {
        return otherUKFeesCurrency;
    }

    public void setOtherUKFeesState(String otherUKFeesState) {
        this.otherUKFeesState = otherUKFeesState;
    }

    public String getOtherUKFeesState() {
        return otherUKFeesState;
    }

    public void setOtherUKFeesDesc(String otherUKFeesDesc) {
        this.otherUKFeesDesc = otherUKFeesDesc;
    }

    public String getOtherUKFeesDesc() {
        return otherUKFeesDesc;
    }

    public void setPriceUrl(String priceUrl) {
        this.priceUrl = priceUrl;
    }

    public String getPriceUrl() {
        return priceUrl;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setFeesType(String feesType) {
        this.feesType = feesType;
    }

    public String getFeesType() {
        return feesType;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getFees() {
        return fees;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setFeesDesc(String feesDesc) {
        this.feesDesc = feesDesc;
    }

    public String getFeesDesc() {
        return feesDesc;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setFeesTypeTooltip(String feesTypeTooltip) {
        this.feesTypeTooltip = feesTypeTooltip;
    }

    public String getFeesTypeTooltip() {
        return feesTypeTooltip;
    }
}
