package com.wuni.util.valueobject.search;

public class SimilarStudyLevelCoursesVO {

  private String categoryCode = null;
  private String categoryId = null;
  private String categoryDesc = null;
  private String browseMoneyPageUrl = null;
  private String collegeCount = null;//Search_redesign
  private String qualificationCode = null;
  public String getSelectedFlag() {
	return selectedFlag;
}

public void setSelectedFlag(String selectedFlag) {
	this.selectedFlag = selectedFlag;
}
private String subjectTextKey = null;  
  private String selectedFlag = null;

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return this.categoryCode;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }

  public String getCategoryDesc() {
    return this.categoryDesc;
  }

  public void setBrowseMoneyPageUrl(String browseMoneyPageUrl) {
    this.browseMoneyPageUrl = browseMoneyPageUrl;
  }

  public String getBrowseMoneyPageUrl() {
    return this.browseMoneyPageUrl;
  }
  public void setCollegeCount(String collegeCount) {
    this.collegeCount = collegeCount;
  }
  public String getCollegeCount() {
    return collegeCount;
  }
  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }
  public String getQualificationCode() {
    return qualificationCode;
  }
  public void setSubjectTextKey(String subjectTextKey) {
    this.subjectTextKey = subjectTextKey;
  }
  public String getSubjectTextKey() {
    return subjectTextKey;
  }
}
