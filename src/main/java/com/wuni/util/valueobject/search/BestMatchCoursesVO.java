package com.wuni.util.valueobject.search;

import java.util.ArrayList;

public class BestMatchCoursesVO {

  private String courseId = null;
  private String courseTitle = null;
  private String courseDomesticFees = null;
  private String courseUcasTariff = null;
  private String wasThisCourseShortlisted = null;
  private String studyLevelDesc = null;
  private String studyLevelDescDet = null;
  private String ucasCode = null;
  //
  private String subOrderItemId = null;
  private String orderItemId = null;
  private String subOrderEmail = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectus = null;
  private String subOrderProspectusWebform = null;
  private String subOrderWebsite = null;
  private String myhcProfileId = null;
  private String profileType = null;
  private String applyNowFlag = null;
  private String showTooltipFlag = null;
  private String mediaId = null;
  private String mediaPath = null;
  private String mediaType = null;
  private String cpeQualificationNetworkId = null;
  private String feeDuration = null;
  //
  private String courseDetailsPageURL = null;
  private String hotLineNo = null;
  private String pageName = null;
  private String courseClearingPlaces = null;
  private String institutionClearingPlaces = null;
  private String lastUpdated = null;
  private String qlFlag = null;
  private String webformPrice = null;
  private String websitePrice = null;
  private String moduleGroupDetails = null;//Search redesign
  private String employmentRate = null;
  private String courseRank = null;
  private String ordinalRank = null;
  private String moduleGroupId = null;
  private String moduleTitle = null;
  private String addedToFinalChoice = null;
  private ArrayList moduleDetailList = null;//Added by Priyaa for Jan 2016 release
  private String departmentOrFaculty = null;//Added by Indumathi.S 29-03-16 
  private String spUrl = null;
  
  private String matchPercentage = null;
  private String locationMatchFlag = null;
  private String locationTypeMatchFlag = null;
  private String studyModeMatchFlag = null;
  private String entryLevelMatchFlag = null;
  private String assessedMatchFlag = null;
  private String reviewSub1MatchFlag = null;
  private String reviewSub2MatchFlag = null;
  private String reviewSub3MatchFlag = null;
  private String qualificationMatchFlag = null;
  private String subjectMatchFlag = null;
  private String showApplyNowButton = null;
  private String numberOfCoursesForThisCollege=null;
  private String isSponsoredCollege = null;
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String applyUrl = null;
  private String opportunityId = null;

  public void init() {
    this.courseId = "";
    this.courseTitle = "";
    this.courseDomesticFees = "";
    this.courseUcasTariff = "";
    this.wasThisCourseShortlisted = "";
    this.subOrderItemId = "";
    this.studyLevelDesc = "";
    this.courseDetailsPageURL = "";
    this.ucasCode = "";
    this.orderItemId = "";
    this.subOrderEmail = "";
    this.subOrderEmailWebform = "";
    this.subOrderProspectus = "";
    this.subOrderProspectusWebform = "";
    this.subOrderWebsite = "";
    this.myhcProfileId = "";
    this.profileType = "";
    this.applyNowFlag = "";
    this.mediaId = "";
    this.mediaPath = "";
    this.mediaType = "";
    this.cpeQualificationNetworkId = "";
    this.hotLineNo = "";
    this.qlFlag = "";
    
    this.matchPercentage = "";
    this.locationMatchFlag = "";
    this.locationTypeMatchFlag = "";
    this.studyModeMatchFlag = "";
    this.entryLevelMatchFlag = "";
    this.assessedMatchFlag = "";
    this.reviewSub1MatchFlag = "";
    this.reviewSub2MatchFlag = "";
    this.reviewSub3MatchFlag = "";
    this.qualificationMatchFlag = "";
    this.subjectMatchFlag = "";
  }

  public void flush() {
    this.courseId = null;
    this.courseTitle = null;
    this.courseDomesticFees = null;
    this.courseUcasTariff = null;
    this.wasThisCourseShortlisted = null;
    this.subOrderItemId = null;
    this.courseDetailsPageURL = null;
    this.studyLevelDesc = null;
    this.ucasCode = null;
    this.orderItemId = null;
    this.subOrderEmail = null;
    this.subOrderEmailWebform = null;
    this.subOrderProspectus = null;
    this.subOrderProspectusWebform = null;
    this.subOrderWebsite = null;
    this.myhcProfileId = null;
    this.profileType = null;
    this.applyNowFlag = null;
    this.mediaId = null;
    this.mediaPath = null;
    this.mediaType = null;
    this.cpeQualificationNetworkId = null;
    this.hotLineNo = null;
    this.qlFlag = null;
    
    this.matchPercentage = null;
    this.locationMatchFlag = null;
    this.locationTypeMatchFlag = null;
    this.studyModeMatchFlag = null;
    this.entryLevelMatchFlag = null;
    this.assessedMatchFlag = null;
    this.reviewSub1MatchFlag = null;
    this.reviewSub2MatchFlag = null;
    this.reviewSub3MatchFlag = null;
    this.qualificationMatchFlag = null;
    this.subjectMatchFlag = null;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCourseDomesticFees(String courseDomesticFees) {
    this.courseDomesticFees = courseDomesticFees;
  }

  public String getCourseDomesticFees() {
    return courseDomesticFees;
  }

  public void setCourseUcasTariff(String courseUcasTariff) {
    this.courseUcasTariff = courseUcasTariff;
  }

  public String getCourseUcasTariff() {
    return courseUcasTariff;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setWasThisCourseShortlisted(String wasThisCourseShortlisted) {
    this.wasThisCourseShortlisted = wasThisCourseShortlisted;
  }

  public String getWasThisCourseShortlisted() {
    return wasThisCourseShortlisted;
  }

  public void setCourseDetailsPageURL(String courseDetailsPageURL) {
    this.courseDetailsPageURL = courseDetailsPageURL;
  }

  public String getCourseDetailsPageURL() {
    return courseDetailsPageURL;
  }

  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }

  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setApplyNowFlag(String applyNowFlag) {
    this.applyNowFlag = applyNowFlag;
  }

  public String getApplyNowFlag() {
    return applyNowFlag;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
    public void setFeeDuration(String feeDuration) {
        this.feeDuration = feeDuration;
    }
    public String getFeeDuration() {
        return feeDuration;
    }
    public void setStudyLevelDescDet(String studyLevelDescDet) {
        this.studyLevelDescDet = studyLevelDescDet;
    }
    public String getStudyLevelDescDet() {
        return studyLevelDescDet;
    }

    public void setHotLineNo(String hotLineNo) {
        this.hotLineNo = hotLineNo;
    }

    public String getHotLineNo() {
        return hotLineNo;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setCourseClearingPlaces(String courseClearingPlaces) {
        this.courseClearingPlaces = courseClearingPlaces;
    }

    public String getCourseClearingPlaces() {
        return courseClearingPlaces;
    }

    public void setInstitutionClearingPlaces(String institutionClearingPlaces) {
        this.institutionClearingPlaces = institutionClearingPlaces;
    }

    public String getInstitutionClearingPlaces() {
        return institutionClearingPlaces;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }

  public String getQlFlag() {
    return qlFlag;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setModuleGroupDetails(String moduleGroupDetails) {
    this.moduleGroupDetails = moduleGroupDetails;
  }
  public String getModuleGroupDetails() {
    return moduleGroupDetails;
  }
  public void setEmploymentRate(String employmentRate) {
    this.employmentRate = employmentRate;
  }
  public String getEmploymentRate() {
    return employmentRate;
  }
  public void setCourseRank(String courseRank) {
    this.courseRank = courseRank;
  }
  public String getCourseRank() {
    return courseRank;
  }
  public void setOrdinalRank(String ordinalRank) {
    this.ordinalRank = ordinalRank;
  }
  public String getOrdinalRank() {
    return ordinalRank;
  }
  public void setModuleGroupId(String moduleGroupId) {
    this.moduleGroupId = moduleGroupId;
  }
  public String getModuleGroupId() {
    return moduleGroupId;
  }
  public void setModuleTitle(String moduleTitle) {
    this.moduleTitle = moduleTitle;
  }
  public String getModuleTitle() {
    return moduleTitle;
  }
  public void setAddedToFinalChoice(String addedToFinalChoice) {
    this.addedToFinalChoice = addedToFinalChoice;
  }
  public String getAddedToFinalChoice() {
    return addedToFinalChoice;
  }
  public void setModuleDetailList(ArrayList moduleDetailList) {
    this.moduleDetailList = moduleDetailList;
  }
  public ArrayList getModuleDetailList() {
    return moduleDetailList;
  }

  public void setDepartmentOrFaculty(String departmentOrFaculty) {
    this.departmentOrFaculty = departmentOrFaculty;
  }

  public String getDepartmentOrFaculty() {
    return departmentOrFaculty;
  }

  public void setSpUrl(String spUrl) {
    this.spUrl = spUrl;
  }

  public String getSpUrl() {
    return spUrl;
  }
  public void setMatchPercentage(String matchPercentage) {
    this.matchPercentage = matchPercentage;
  }
  public String getMatchPercentage() {
    return matchPercentage;
  }
  public void setLocationMatchFlag(String locationMatchFlag) {
    this.locationMatchFlag = locationMatchFlag;
  }
  public String getLocationMatchFlag() {
    return locationMatchFlag;
  }
  public void setLocationTypeMatchFlag(String locationTypeMatchFlag) {
    this.locationTypeMatchFlag = locationTypeMatchFlag;
  }
  public String getLocationTypeMatchFlag() {
    return locationTypeMatchFlag;
  }
  public void setStudyModeMatchFlag(String studyModeMatchFlag) {
    this.studyModeMatchFlag = studyModeMatchFlag;
  }
  public String getStudyModeMatchFlag() {
    return studyModeMatchFlag;
  }
  public void setEntryLevelMatchFlag(String entryLevelMatchFlag) {
    this.entryLevelMatchFlag = entryLevelMatchFlag;
  }
  public String getEntryLevelMatchFlag() {
    return entryLevelMatchFlag;
  }
  public void setAssessedMatchFlag(String assessedMatchFlag) {
    this.assessedMatchFlag = assessedMatchFlag;
  }
  public String getAssessedMatchFlag() {
    return assessedMatchFlag;
  }
  public void setReviewSub1MatchFlag(String reviewSub1MatchFlag) {
    this.reviewSub1MatchFlag = reviewSub1MatchFlag;
  }
  public String getReviewSub1MatchFlag() {
    return reviewSub1MatchFlag;
  }
  public void setReviewSub2MatchFlag(String reviewSub2MatchFlag) {
    this.reviewSub2MatchFlag = reviewSub2MatchFlag;
  }
  public String getReviewSub2MatchFlag() {
    return reviewSub2MatchFlag;
  }
  public void setReviewSub3MatchFlag(String reviewSub3MatchFlag) {
    this.reviewSub3MatchFlag = reviewSub3MatchFlag;
  }
  public String getReviewSub3MatchFlag() {
    return reviewSub3MatchFlag;
  }
  public void setQualificationMatchFlag(String qualificationMatchFlag) {
    this.qualificationMatchFlag = qualificationMatchFlag;
  }
  public String getQualificationMatchFlag() {
    return qualificationMatchFlag;
  }
  public void setSubjectMatchFlag(String subjectMatch) {
    this.subjectMatchFlag = subjectMatch;
  }
  public String getSubjectMatchFlag() {
    return subjectMatchFlag;
  }


public String getShowTooltipFlag() {
   return showTooltipFlag;
}


public void setShowTooltipFlag(String showTooltipFlag) {
   this.showTooltipFlag = showTooltipFlag;
}


public String getShowApplyNowButton() {
   return showApplyNowButton;
}


public void setShowApplyNowButton(String showApplyNowButton) {
   this.showApplyNowButton = showApplyNowButton;
}


public String getNumberOfCoursesForThisCollege() {
   return numberOfCoursesForThisCollege;
}


public void setNumberOfCoursesForThisCollege(String numberOfCoursesForThisCollege) {
   this.numberOfCoursesForThisCollege = numberOfCoursesForThisCollege;
}


public String getIsSponsoredCollege() {
   return isSponsoredCollege;
}


public void setIsSponsoredCollege(String isSponsoredCollege) {
   this.isSponsoredCollege = isSponsoredCollege;
}


public String getCollegeId() {
   return collegeId;
}


public void setCollegeId(String collegeId) {
   this.collegeId = collegeId;
}


public String getCollegeName() {
   return collegeName;
}


public void setCollegeName(String collegeName) {
   this.collegeName = collegeName;
}


public String getCollegeNameDisplay() {
   return collegeNameDisplay;
}


public void setCollegeNameDisplay(String collegeNameDisplay) {
   this.collegeNameDisplay = collegeNameDisplay;
}


public String getApplyUrl() {
   return applyUrl;
}


public void setApplyUrl(String applyUrl) {
   this.applyUrl = applyUrl;
}


public String getOpportunityId() {
   return opportunityId;
}


public void setOpportunityId(String opportunityId) {
   this.opportunityId = opportunityId;
}
}
