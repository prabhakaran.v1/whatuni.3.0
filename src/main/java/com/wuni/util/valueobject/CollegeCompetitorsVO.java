package com.wuni.util.valueobject;

public class CollegeCompetitorsVO {
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String admissionEmail = null;
  private String collegeSite = null;
  private String addLine1 = null;
  private String addLine2 = null;
  private String town = null;
  private String county = null;
  private String postCode = null;
  private String telephone = null;
  private String fax = null;
  private String collegeLogo = null;
  private String cityName = null;
  private String uniHomeURL = null;
  private String profileText = null;
  
  private String orderItemId = null;
  private String subOrderItemId = null;
  private String myhcProfileId = null;
  private String profileId = null;
  private String profileType = null;
  private String advertName = null;
  private String cpeQualificationNetworkId = null;
  private String subOrderEmail = null;
  private String subOrderProspectus = null;
  private String subOrderWebsite = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectusWebform = null;
  private String mediaPath = null;
  private String mediaThumbPath = null;
  private String mediaId = null;
  private String mediaType = null;
  private String videoThumbPath = null;
  
  private String whatUniSays = null;
  private String websitePrice = null;
  private String webformPrice = null;
  private String gaCollegeName = null;
  
  private String shortListFlag = null;
  private String hotline = null;//24_JUN_2014_REL
  
  public void flush() {
    this.collegeId = null;
    this.collegeName = null;
    this.collegeNameDisplay = null;
    this.admissionEmail = null;
    this.collegeSite = null;
    this.addLine1 = null;
    this.addLine2 = null;
    this.town = null;
    this.county = null;
    this.postCode = null;
    this.telephone = null;
    this.fax = null;
    this.collegeLogo = null;
    this.cityName = null;
    this.uniHomeURL = null;
    this.profileText = null;
  }
  public void init() {
    this.collegeId = "";
    this.collegeName = "";
    this.collegeNameDisplay = "";
    this.admissionEmail = "";
    this.collegeSite = "";
    this.addLine1 = "";
    this.addLine2 = "";
    this.town = "";
    this.county = "";
    this.postCode = "";
    this.telephone = "";
    this.fax = "";
    this.collegeLogo = "";
    this.cityName = "";
    this.uniHomeURL = "";
    this.profileText = "";
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setAdmissionEmail(String admissionEmail) {
    this.admissionEmail = admissionEmail;
  }
  public String getAdmissionEmail() {
    return admissionEmail;
  }
  public void setCollegeSite(String collegeSite) {
    this.collegeSite = collegeSite;
  }
  public String getCollegeSite() {
    return collegeSite;
  }
  public void setAddLine1(String addLine1) {
    this.addLine1 = addLine1;
  }
  public String getAddLine1() {
    return addLine1;
  }
  public void setAddLine2(String addLine2) {
    this.addLine2 = addLine2;
  }
  public String getAddLine2() {
    return addLine2;
  }
  public void setTown(String town) {
    this.town = town;
  }
  public String getTown() {
    return town;
  }
  public void setCounty(String county) {
    this.county = county;
  }
  public String getCounty() {
    return county;
  }
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }
  public String getPostCode() {
    return postCode;
  }
  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }
  public String getTelephone() {
    return telephone;
  }
  public void setFax(String fax) {
    this.fax = fax;
  }
  public String getFax() {
    return fax;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setCityName(String cityName) {
    this.cityName = cityName;
  }
  public String getCityName() {
    return cityName;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setProfileText(String profileText) {
    this.profileText = profileText;
  }
  public String getProfileText() {
    return profileText;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getProfileId() {
    return profileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }
  public String getAdvertName() {
    return advertName;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }
  public String getMediaThumbPath() {
    return mediaThumbPath;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setVideoThumbPath(String videoThumbPath) {
    this.videoThumbPath = videoThumbPath;
  }
  public String getVideoThumbPath() {
    return videoThumbPath;
  }
  public void setWhatUniSays(String whatUniSays) {
    this.whatUniSays = whatUniSays;
  }
  public String getWhatUniSays() {
    return whatUniSays;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }
  public String getWebformPrice() {
    return webformPrice;
  }
  public void setGaCollegeName(String gaCollegeName) {
    this.gaCollegeName = gaCollegeName;
  }
  public String getGaCollegeName() {
    return gaCollegeName;
  }
  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }
  public String getShortListFlag() {
    return shortListFlag;
  }
  public void setHotline(String hotline) {
    this.hotline = hotline;
  }
  public String getHotline() {
    return hotline;
  }
}
