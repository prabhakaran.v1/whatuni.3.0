package com.wuni.util.uni;

import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.wuni.util.CommonValidator;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.CollegeNamesVO;

/**
 * will contain uni/college specific utilities
 *
 * @author  Mohamed Syed
 * @since   wu316_20110823
 *
 */
public class CollegeUtilities {

  /**
   * smilar to CommonFunction.getCollegeName(), but it is fetching all college names (collegeName, collegeNameAlias, collgeNameDisplay)
   *
   * @author  Mohamed Syed
   * @since   wu316_20110823
   *
   * @param collegeId
   * @param request
   * @return CollegeNamesVO
   *
   */
  public CollegeNamesVO getCollegeNames(String collegeId, HttpServletRequest request) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNullOrZero(collegeId)) {
      validate = null;
      return null;
    }
    //
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    CollegeNamesVO collegeNames = new CollegeNamesVO();
    Vector parameters = new Vector();
    parameters.add(collegeId);
    try {
      resultset = datamodel.getResultSet("wu_util_pkg.get_college_names_fn", parameters);
      while (resultset.next()) {
        collegeNames.setCollegeId(resultset.getString("college_id"));
        collegeNames.setCollegeName(resultset.getString("college_name"));
        collegeNames.setCollegeNameDisplay(resultset.getString("college_name_display"));
        collegeNames.setCollegeNameAlias(resultset.getString("college_name_alias"));
      }
      if (validate.isBlankOrNull(collegeNames.getCollegeName())) {
        return null;
      }
      //
      HttpSession session = request.getSession();
      session.removeAttribute("collegeName");
      session.removeAttribute("collegeNameDisplay");
      session.removeAttribute("collegeNameAlias");
      //
      session.setAttribute("collegeName", collegeNames.getCollegeName());
      session.setAttribute("collegeNameDisplay", collegeNames.getCollegeNameDisplay());
      session.setAttribute("collegeNameAlias", collegeNames.getCollegeNameAlias());
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close resultsets, and nullify unused objects.
      try {
        datamodel.closeCursor(resultset);
        if (resultset != null) {
          resultset.close();
        }
        resultset = null;
      } catch (Exception e) {
        e.printStackTrace();
      }
      //nullify unused objects
      datamodel = null;
      parameters.clear();
      parameters = null;
      validate = null;
    }
    return collegeNames;
  }

}
