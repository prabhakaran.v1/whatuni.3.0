package com.wuni.myfinalchoice.controller;

import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.text.pdf.codec.Base64;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.HomePageImageDetailsVO;

@Controller
public class MyFinalChoiceController {
	
  public ModelAndView getMyFinalChoice(@ModelAttribute("myFinalChoiceBean") MyFinalChoiceBean myChoiceBean, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    ServletContext context = request.getServletContext();
	    String forwardString = "mychoice";
	    String myAction = null;
	    String returnString = "";
	    if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
	      return new ModelAndView("forward: /userLogin.html");
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
	        String fromUrl = "/myfinalchoice.html";
	        if (session.getAttribute("fromUrl") == null) {
	          session.setAttribute("fromUrl", fromUrl);
	        }
	        return new ModelAndView("loginPage");
	      }
	    } catch (Exception secutityException) {
	      secutityException.printStackTrace();
	    }
	    request.setAttribute("curActiveMenu", "final5");
	    String sessionId = new SessionData().getData(request, "x");
	    sessionId = GenericValidator.isBlankOrNull(sessionId)? "16180339": sessionId;
	    String userId = new SessionData().getData(request, "y");
	    new CommonFunction().loadUserLoggedInformation(request, context, response);
	    String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0))? new CommonFunction().checkCookieStatus(request): "";
	    basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0))? basketId: "";
	    userId = (basketId != null && basketId.trim().length() > 0)? "": userId;
	    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
	      userId = "0";
	      basketId = "";
	    }
	    if (!GenericValidator.isBlankOrNull(request.getParameter("myaction"))) {
	      myAction = request.getParameter("myaction");
	      if (!GenericValidator.isBlankOrNull(request.getParameter("fnChoiceId"))) {
	        myChoiceBean.setFinalChoiceId(request.getParameter("fnChoiceId"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("collegeId"))) {
	        myChoiceBean.setCollegeId(request.getParameter("collegeId"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("courseId"))) {
	        myChoiceBean.setCourseId(request.getParameter("courseId"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("slotPosId"))) {
	        myChoiceBean.setSlotPositionId(request.getParameter("slotPosId"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("slotPosStatus"))) {
	        myChoiceBean.setSlotPositionStatus(request.getParameter("slotPosStatus"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("finalChId1"))) {
	        myChoiceBean.setSwapFianlChoiceId1(request.getParameter("finalChId1"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("finalChId2"))) {
	        myChoiceBean.setSwapFianlChoiceId2(request.getParameter("finalChId2"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("finalChPos1"))) {
	        myChoiceBean.setSwapChoicePosition1(request.getParameter("finalChPos1"));
	      }
	      if (!GenericValidator.isBlankOrNull(request.getParameter("finalChPos2"))) {
	        myChoiceBean.setSwapChoicePosition2(request.getParameter("finalChPos2"));
	      }
	      myChoiceBean.setUserId(userId);
	      if ("save".equalsIgnoreCase(myAction)) {
	        Map myChoiceSaveMap = commonBusiness.saveFinalChoiceData(myChoiceBean);
	        if (myChoiceSaveMap != null) {
	          String returnCodeStatus = (String)myChoiceSaveMap.get("p_return_code");
	          if (!GenericValidator.isBlankOrNull(returnCodeStatus)) {
	            String addedFromSrch = request.getParameter("addedFromSrch");
	            if ("SUCCESS".equalsIgnoreCase(returnCodeStatus) && addedFromSrch == null) {
	              String newFinalChoiceId = (String)myChoiceSaveMap.get("p_new_final_choice_id");
	              String collegeLogo = (String)myChoiceSaveMap.get("p_college_logo");
	              String usrInCompCnt = (String)myChoiceSaveMap.get("p_incomplete_action_cnt");
	              String usrFnchCompCnt = (String)myChoiceSaveMap.get("p_final_choices_cnt");
	              if (!GenericValidator.isBlankOrNull(myChoiceBean.getSlotPositionId())) {
	                if (!GenericValidator.isBlankOrNull(newFinalChoiceId)) {
	                  returnString = "SUCEESS###" + newFinalChoiceId;
	                }
	                if (!GenericValidator.isBlankOrNull(collegeLogo) && GenericValidator.isBlankOrNull(myChoiceBean.getFinalChoiceId())) {
	                  collegeLogo = new CommonUtil().getImgPath(collegeLogo, 0);
	                  returnString = returnString + "SUCEESS###" + collegeLogo;
	                }
	                if (!GenericValidator.isBlankOrNull(usrInCompCnt)) {
	                  returnString = returnString + "SUCEESS###" + usrInCompCnt;
	                }
	                if (!GenericValidator.isBlankOrNull(usrFnchCompCnt)) {
	                  returnString = returnString + "#_#CHOICECNT#_#" + usrFnchCompCnt;
	                }
	              } else {
	                returnString = returnCodeStatus;
	                if (!GenericValidator.isBlankOrNull(usrFnchCompCnt)) {
	                  returnString = returnString + "#_#CHOICECNT#_#" + usrFnchCompCnt;
	                }
	              }
	              response.getWriter().write(returnString);
	            } else {
	              if ("true".equals(addedFromSrch)) {
	                String usrFnchCompCnt = (String)myChoiceSaveMap.get("p_final_choices_cnt");
	                response.getWriter().write(returnCodeStatus + "#_#CHOICECNT#_#" + usrFnchCompCnt);
	              } else {
	                response.getWriter().write(returnCodeStatus);
	              }
	            }
	            ArrayList userInfoList = new CommonFunction().getUserInformation(new SessionData().getData(request, "y"), request, context);
	            if (userInfoList != null && userInfoList.size() > 0) {
	              session.setAttribute("userInfoList", userInfoList);
	            }
	          }
	        }
	        return null;
	      } else if ("userAction".equalsIgnoreCase(myAction)) {
	        Map usrActionStatusMap = commonBusiness.getFinalChUsrActionPod(myChoiceBean);
	        if (usrActionStatusMap != null) {
	          List usrStatusActionPod = (List)usrActionStatusMap.get("pc_final_choice_list");
	          request.setAttribute("usrStatusActionPod", usrStatusActionPod);
	          forwardString = "usrStatusAction";
	        }
	      } else if ("confirm".equalsIgnoreCase(myAction)) {
	        Map confirmChoiceMap = commonBusiness.confirmUsrActionPod(myChoiceBean);
	        if (confirmChoiceMap != null) {
	          String confirmStatus = (String)confirmChoiceMap.get("p_confirm_status");
	          if (!GenericValidator.isBlankOrNull(confirmStatus)) {
	            returnString = confirmStatus;
	            response.getWriter().write(returnString);
	          }
	        }
	        return null;
	      } else if ("loadSwapChoicePod".equalsIgnoreCase(myAction)) {
	        new CommonFunction().getMyFinalChoicePod(request);
	        forwardString = "loadSwapChoicePod";
	      } else if ("showFnchLightbox".equalsIgnoreCase(myAction)) {
	        new CommonFunction().getMyFinalChoicePod(request);
	        if (!GenericValidator.isBlankOrNull(request.getParameter("extParam"))) {
	          request.setAttribute("showLightBxEdit", "true");
	        }
	        forwardString = "mychoiceLightbox";
	      } else if ("showFnchTapView".equalsIgnoreCase(myAction)) {
	        new CommonFunction().getMyFinalChoicePod(request);
	        request.setAttribute("showMychTapView", "true");
	        forwardString = "myfchTapView";
	      } else if ("swapMyChoicePos".equalsIgnoreCase(myAction)) {
	        Map swapFinalchoicePosMap = commonBusiness.swapFinalChoicePod(myChoiceBean);
	        if (swapFinalchoicePosMap != null) {
	          String confirmStatus = (String)swapFinalchoicePosMap.get("p_status_code");
	          if (!GenericValidator.isBlankOrNull(confirmStatus)) {
	            response.getWriter().write(confirmStatus);
	          }
	        }
	        return null;
	      } else if ("fnchChSugestAjaxPod".equalsIgnoreCase(myAction)) {
	        Map mySuggestionAjxMap = commonBusiness.getFinalChoiceSuggestionList(myChoiceBean);
	        if (mySuggestionAjxMap != null) {
	          List mySuggestionAjxList = (List)mySuggestionAjxMap.get("pc_final_choice_Suggestion");
	          if (mySuggestionAjxList != null && mySuggestionAjxList.size() > 0) {
	            request.setAttribute("mySuggestionList", mySuggestionAjxList);
	          }
	          forwardString = "myfchAjxSuggestPod";
	        }
	      } else if ("homepageTapView".equalsIgnoreCase(myAction)) {
	        new CommonFunction().getMyFinalChoicePod(request);
	        forwardString = "loadHomepageTapView";
	      } else if ("editFnch".equalsIgnoreCase(myAction)) {
	        commonBusiness.editMyFinalChoicePod(myChoiceBean);
	        response.getWriter().write("SUCEESS");
	        return null;
	      }
	    } else {
	      new CommonFunction().getMyFinalChoicePod(request);
	      myChoiceBean.setUserId(userId);
	      Map mySuggestionMap = commonBusiness.getFinalChoiceSuggestionList(myChoiceBean);
	      if (mySuggestionMap != null) {
	        List mySuggestionList = (List)mySuggestionMap.get("pc_final_choice_Suggestion");
	        if (mySuggestionList != null && mySuggestionList.size() > 0) {
	          request.setAttribute("mySuggestionList", mySuggestionList);
	        }
	      }
	      getfriendslist(userId, request);
	    }
	    return new ModelAndView(forwardString);
	  }
	  public void getfriendslist(String userId, HttpServletRequest request) throws Exception {
	    String friendsId = "";
	    DataModel dataModel = new DataModel();
	    ResultSet resultset = null;
	    Vector vector = new Vector();
	    vector.clear();
	    ArrayList friendsActivityList = new ArrayList();
	    if (userId != null && !"0".equals(userId) && !GenericValidator.isBlankOrNull(userId)) {
	      friendsId = new CookieManager().getCookieValue(request, "FRIENDS_IDS");
	      if (!GenericValidator.isBlankOrNull(friendsId)) {
	        //Added below Base64 decoding code by Prabha on 31_07_2018
	        String friendsIds = URLDecoder.decode(friendsId, "UTF-8");
	        byte[] decodedFrndsId = Base64.decode(friendsIds);
	        friendsId = new String(decodedFrndsId);
	        //End of decoding cookie code
	      }
	      friendsId = (GenericValidator.isBlankOrNull(friendsId))? "": friendsId.trim();
	      try {
	        vector.add(userId);
	        vector.add(friendsId);
	        resultset = dataModel.getResultSet("WU_FRIENDS_ACTIVITY_PKG.get_friends_activity_fn", vector);
	        while (resultset.next()) {
	          HomePageImageDetailsVO homePageImageVO = new HomePageImageDetailsVO();
	          homePageImageVO.setFriendUrl(resultset.getString("URL"));
	          homePageImageVO.setFriendActivityText(resultset.getString("ACTIVITY_TEXT"));
	          homePageImageVO.setPrivacyFlag(resultset.getString("PRIVACY_FLAG"));
	          homePageImageVO.setImageDetails(resultset.getString("IMAGE_DETAILS"));
	          friendsActivityList.add(homePageImageVO);
	        }
	      } catch (SQLException e) {
	        e.printStackTrace();
	      } finally {
	        dataModel.closeCursor(resultset);
	      }
	      if (friendsActivityList != null && friendsActivityList.size() > 0) {
	        request.setAttribute("FRIENDS_ACTIVITY_LIST", friendsActivityList);
	      }
	      request.setAttribute("setMyFinalChoiceHome", "true");
	    }  
  }
}
