package com.wuni.myfinalchoice.form;

import org.apache.struts.action.ActionForm;

public class MyFinalChoiceBean {
  String userId = null;
  String finalChoiceId;
  String collegeId = null;
  String collegeDispName = null;
  String courseId = null;
  String courseTitle = null;
  String collegeLogo = null;
  String slotPositionId = null;
  String slotPositionStatus = null;
  String dispSlotPositionNo = null;
  String dispSlotPositionStyle = null;
  String inCompActionCnt = null;
  String actionMsg = null;
  String actionStatus = null;
  String actionURL = null;
  String swapFianlChoiceId1 = null;
  String swapFianlChoiceId2 = null;
  String swapChoicePosition1 = null;
  String swapChoicePosition2 = null;
  //
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeDispName(String collegeDispName) {
    this.collegeDispName = collegeDispName;
  }
  public String getCollegeDispName() {
    return collegeDispName;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setSlotPositionId(String slotPositionId) {
    this.slotPositionId = slotPositionId;
  }
  public String getSlotPositionId() {
    return slotPositionId;
  }
  public void setSlotPositionStatus(String slotPositionStatus) {
    this.slotPositionStatus = slotPositionStatus;
  }
  public String getSlotPositionStatus() {
    return slotPositionStatus;
  }
  public void setDispSlotPositionNo(String dispSlotPositionNo) {
    this.dispSlotPositionNo = dispSlotPositionNo;
  }
  public String getDispSlotPositionNo() {
    return dispSlotPositionNo;
  }
  public void setDispSlotPositionStyle(String dispSlotPositionStyle) {
    this.dispSlotPositionStyle = dispSlotPositionStyle;
  }
  public String getDispSlotPositionStyle() {
    return dispSlotPositionStyle;
  }
  public void setFinalChoiceId(String finalChoiceId) {
    this.finalChoiceId = finalChoiceId;
  }
  public String getFinalChoiceId() {
    return finalChoiceId;
  }
  public void setInCompActionCnt(String inCompActionCnt) {
    this.inCompActionCnt = inCompActionCnt;
  }
  public String getInCompActionCnt() {
    return inCompActionCnt;
  }
  public void setActionMsg(String actionMsg) {
    this.actionMsg = actionMsg;
  }
  public String getActionMsg() {
    return actionMsg;
  }
  public void setActionStatus(String actionStatus) {
    this.actionStatus = actionStatus;
  }
  public String getActionStatus() {
    return actionStatus;
  }
  public void setActionURL(String actionURL) {
    this.actionURL = actionURL;
  }
  public String getActionURL() {
    return actionURL;
  }
  public void setSwapFianlChoiceId1(String swapFianlChoiceId1) {
    this.swapFianlChoiceId1 = swapFianlChoiceId1;
  }
  public String getSwapFianlChoiceId1() {
    return swapFianlChoiceId1;
  }
  public void setSwapFianlChoiceId2(String swapFianlChoiceId2) {
    this.swapFianlChoiceId2 = swapFianlChoiceId2;
  }
  public String getSwapFianlChoiceId2() {
    return swapFianlChoiceId2;
  }
  public void setSwapChoicePosition1(String swapChoicePosition1) {
    this.swapChoicePosition1 = swapChoicePosition1;
  }
  public String getSwapChoicePosition1() {
    return swapChoicePosition1;
  }
  public void setSwapChoicePosition2(String swapChoicePosition2) {
    this.swapChoicePosition2 = swapChoicePosition2;
  }
  public String getSwapChoicePosition2() {
    return swapChoicePosition2;
  }
}
