package com.wuni.home.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FlickrAjaxController{
	
  @RequestMapping(value="/flickAjax", method=RequestMethod.POST)	
  public ModelAndView getFlickAjax(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String provFlickUserId = request.getParameter("provFlickUserId");
    String provFlickUserUrl = request.getParameter("provFlickUserUrl");
    String homeFlickrURL = request.getParameter("homeFlickrURL");
    if (provFlickUserId != null && !"".equals(provFlickUserId)) {
      request.setAttribute("providerFlickrUserId", provFlickUserId);
    }
    if (provFlickUserUrl != null && !"".equals(provFlickUserUrl)) {
      request.setAttribute("providerFlickrUrl", provFlickUserUrl);
    }
    if (homeFlickrURL != null && !"".equals(homeFlickrURL)) {//30_JUN_2015_REL
      request.setAttribute("homeFlickrURL", homeFlickrURL);
    }
    return new ModelAndView("flickResponse");
  }
}
