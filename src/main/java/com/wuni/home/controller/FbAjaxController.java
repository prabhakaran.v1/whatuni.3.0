package com.wuni.home.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * used to load the Ajax response of Fb in Iframe
  *
  * @author   Priyaa Parthasarathy
  * @version  1.0
  *
  */

@Controller
public class FbAjaxController{

  @RequestMapping(value="/fbAjax", method=RequestMethod.POST)	 
  public ModelAndView getFbAjax(HttpServletRequest request, HttpServletResponse response) throws Exception {
    if (request.getParameter("provFbUrl") != null && !"".equals(request.getParameter("provFbUrl"))) { //08_AUG_2014 Added by Amir to display provider Facebook related info
      request.setAttribute("providerFaceBookUrl", request.getParameter("provFbUrl"));
    }
    return new ModelAndView("fbResponse");
  }
}
