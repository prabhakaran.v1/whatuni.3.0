package com.wuni.home.controller;

import WUI.homepage.form.HomePageBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.itextpdf.text.pdf.codec.Base64;
import com.layer.business.ICommonBusiness;
import com.layer.business.IHomeBusiness;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.HomePageImageDetailsVO;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.util.GlobalMethods;

/**
  * used to display the information in the home page of whatuni. one DB call
  *
  * @author   Mohamed Syed
  * @since    wu314_20110712
  * @version  1.0
  *  Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 19-MAY-14      Amirtharaj                 5.41    Homepage is changed to responsive, commented mobile home and return user page
  * 21:07:2015     Yogeswari                          changes for drawing friends activity pod.
  * 21:11:2019     Sangeeth.S                         Added Home page UX(dynamic Hero Image text, search pod filters, recent search 
  *                                                     and recommed uni Pod) related changes 
  */
@Controller
public class whatuniHomeController {
  
  @Autowired
  private IHomeBusiness homeBusiness;

  @RequestMapping(value = {"/home","/newhome"}, method = RequestMethod.GET)
  public ModelAndView gethome(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    //commented by Amir for 19-May-15 release towards going to responsive homepage
    //    if (MobileUtils.mobileUserAgentCheck(request)) {
    //      new MobileHomeAction().execute(mapping, form, request, response);
    //      return mapping.findForward("mobilehomepage");
    //    }
  //  HttpSession session = request.getSession();
	CommonFunction common = new CommonFunction();
	SessionData sessionData = new SessionData();
    GlobalMethods globalmethods = new GlobalMethods();
    ServletContext context =  request.getServletContext();
    GlobalFunction global = new GlobalFunction();
    boolean setFriendsActivityPod = false;
    String displayFinalChoicePod = "";
    String clearingOnOffSysvar = common.getWUSysVarValue("CLEARING_ON_OFF");
    if(StringUtils.isNotBlank(clearingOnOffSysvar)) {
      //model.addAttribute("clearingHomeJsName", bundle.getString("wuni.common.clearing.home.js"));
      model.addAttribute("clearingOnOffSysvar", clearingOnOffSysvar);
    }
    
    //    
    String currentPageUrl = (common.getDomainName(request,"N") + request.getRequestURI());//Added by priyaa for https and www removal from code 1-sep-2015
    request.setAttribute("currentPageUrl", currentPageUrl);
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    global.removeSessionData(session, request, response);
    globalmethods.setCommonPodInformation(context, request, response);
    //
    String userId = sessionData.getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0? userId: "0";
    //
    String basketID = GenericValidator.isBlankOrNull(common.checkCookieStatus(request))? null: common.checkCookieStatus(request);
    String userType = "";
    String fwdUserHome = "shownewhomepage";
    if (userId != null && "0".equals(userId)) {      
      userType = "NEW_USER";
      fwdUserHome = "shownewhomepage";
    } else {
      userType = "LOGGED_USER";
      fwdUserHome = "showloggedinhomepage";
    }
   
    HomePageBean homepagebean = new HomePageBean();
    homepagebean.setUserId(userId);
    homepagebean.setBasketID(basketID);
    homepagebean.setUserType(userType);
    homepagebean.setTrackSessionId(sessionData.getData(request, "userTrackId"));//p_track_session
    /*if(StringUtils.isNotBlank(clearingOnOffSysvar) && StringUtils.equalsIgnoreCase(clearingOnOffSysvar, "ON")) {
      homepagebean.setClearingUserType(GlobalConstants.CLEARING_USER_TYPE_VAL);	
    }*/
    /**Set User Timepage**/
    Map newHomePageDetailsMap = homeBusiness.getNewHomePageData(homepagebean);
    if (newHomePageDetailsMap != null && newHomePageDetailsMap.size() > 0) {
      String awardsVideoPath = (String)newHomePageDetailsMap.get("p_awards_video");
      if (!GenericValidator.isBlankOrNull(awardsVideoPath)) {
        request.setAttribute("studentChoiceAwrdVideo", awardsVideoPath);
      }
      ////Added fby Priyaa for final 5 - 21-Jul-2015    
      displayFinalChoicePod = (String)newHomePageDetailsMap.get("p_display_final_choice_pod");
      if (!GenericValidator.isBlankOrNull(displayFinalChoicePod)) {
        request.setAttribute("displayFinalChoicePod", displayFinalChoicePod);
      }
      ArrayList countsList = (ArrayList)newHomePageDetailsMap.get("oc_wu_counts");
      if (countsList != null && countsList.size() > 0) {
        request.setAttribute("countsList", countsList);
        HomePageImageDetailsVO hpImageDetailsVO = (HomePageImageDetailsVO)countsList.get(0);
        if (!GenericValidator.isBlankOrNull(hpImageDetailsVO.getVideoPath())) {
          request.setAttribute("heroSliderVideo", hpImageDetailsVO.getVideoPath());
        }
      }
      List providerHeroImageList = getProviderHeroImageDetails(userType);
      if (providerHeroImageList != null && providerHeroImageList.size() > 0) {
        request.setAttribute("providerHeroImageList", providerHeroImageList);
      }
      ArrayList awardsList = (ArrayList)newHomePageDetailsMap.get("oc_wu_awards"); //16-SEP-2014
      if (awardsList != null && awardsList.size() > 0) {
        request.setAttribute("awardsList", awardsList);
      }
      ArrayList articleList = (ArrayList)newHomePageDetailsMap.get("oc_article_slots");
      if (articleList != null && articleList.size() > 0) {
        request.setAttribute("articleList", articleList);
      }
      //
      ArrayList spotLightCourseList = (ArrayList)newHomePageDetailsMap.get("oc_spotlight");
      if (spotLightCourseList != null && spotLightCourseList.size() > 0) {
        request.setAttribute("spotLightCourseList", spotLightCourseList);
      }
      ArrayList reviewList = (ArrayList)newHomePageDetailsMap.get("oc_review_list");
      if (reviewList != null && reviewList.size() > 0) {
        request.setAttribute("reviewList", reviewList);
      }
      ArrayList whatHaveYouDoneList = (ArrayList)newHomePageDetailsMap.get("oc_user_done"); //08_AUG_2014 Added by Priyaa for What have you done pod
      if (whatHaveYouDoneList != null && whatHaveYouDoneList.size() > 0) {
        request.setAttribute("whatHaveYouDoneList", whatHaveYouDoneList);
        request.setAttribute("whathaveudoneSize", String.valueOf(whatHaveYouDoneList.size()));
      }
      ArrayList nextDoneList = (ArrayList)newHomePageDetailsMap.get("oc_next_done");
      if (nextDoneList != null && nextDoneList.size() > 0) {
        request.setAttribute("nextDoneList", nextDoneList);
      }
      
      //request related Home page UX changes added by Sangeeth.S for 21_11_2019_rel
      ArrayList popularRecentList = (ArrayList)newHomePageDetailsMap.get("pc_popular_recent_details");
      if (!CommonUtil.isBlankOrNull(popularRecentList)) {
        request.setAttribute("popularRecentList", popularRecentList);
      }
      
      ArrayList popularUniversityList = (ArrayList)newHomePageDetailsMap.get("pc_popular_univ_list");
      if (!CommonUtil.isBlankOrNull(popularUniversityList)) {
        request.setAttribute("popularUniversityList", popularUniversityList);
      }
      
      ArrayList heroTextImageList = (ArrayList)newHomePageDetailsMap.get("pc_text_image_details");
      if (!CommonUtil.isBlankOrNull(heroTextImageList)) {
        request.setAttribute("heroTextImageList", heroTextImageList);
      }
      
      ArrayList studyLevelQualList = (ArrayList)newHomePageDetailsMap.get("pc_qualification_list");
      if (!CommonUtil.isBlankOrNull(studyLevelQualList)) {
        request.setAttribute("studyLevelQualList", studyLevelQualList);
      }
      
      ArrayList locationList = (ArrayList)newHomePageDetailsMap.get("pc_location_list");
      if (!CommonUtil.isBlankOrNull(locationList)) {
        request.setAttribute("locationList", locationList);
      }
      
      ArrayList adviceKeyTopicList = (ArrayList)newHomePageDetailsMap.get("pc_key_topics_list");
      if (!CommonUtil.isBlankOrNull(adviceKeyTopicList)) {
        request.setAttribute("adviceKeyTopicList", adviceKeyTopicList);
      }
      
      ArrayList recentSearchFilterList = (ArrayList)newHomePageDetailsMap.get("pc_recent_search_details");
      if (!CommonUtil.isBlankOrNull(recentSearchFilterList)) {
        request.setAttribute("recentSearchFilterList", recentSearchFilterList);
      }      
      
      String recentSearchFilterUrl = (String)newHomePageDetailsMap.get("p_filter_url");
      if (!GenericValidator.isBlankOrNull(recentSearchFilterUrl)) {
        request.setAttribute("recentSearchFilterUrl", recentSearchFilterUrl);
      }
      
      String recentSearchDate = (String)newHomePageDetailsMap.get("p_recent_search_date");
      if (!GenericValidator.isBlankOrNull(recentSearchDate)) {
        request.setAttribute("recentSearchDate", recentSearchDate);
      }
      //
      String currYear = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
      request.setAttribute("studAwdCurYear", currYear);
    }
    if (request.getAttribute("userTimeLinePodList") != null) {
      if (!GenericValidator.isBlankOrNull(displayFinalChoicePod) && "Y".equalsIgnoreCase(displayFinalChoicePod)) {
        new CommonFunction().getMyFinalChoicePod(request); // 21_JUL_15 Added by Amir to include Final Choice pod for loggedIn User
        if (request.getAttribute("myFinalChoiceList") != null) {
          request.removeAttribute("userTimeLinePage");
          request.setAttribute("myFnChHomePage", "true");
        } else {
          request.setAttribute("userTimeLinePage", "home"); //08_OCT_2014 Added by Amir for to not show Timeline pod in Header for HomePage
        }
      } else {
        request.setAttribute("userTimeLinePage", "home"); //08_OCT_2014 Added by Amir for to not show Timeline pod in Header for HomePage
      }
    } else {
      request.setAttribute("userTimeLineJquery", "noload"); //08_OCT_2014 Added by Amir for to not load Jquery in HomePage
    }
      //
    request.setAttribute("insightPageFlag", "yes");
    session.setAttribute("noSplashpopup", "true"); //26-Aug-2014
    request.setAttribute("showResponsiveTimeLine", "true");
    request.setAttribute("typeOfUser", userType);
    //

    return new ModelAndView(fwdUserHome);
  }

  /**
      *   This function is used to load the category list.
      * @param userType
      * @return Provider Hero Image List as Collection obejct.
      * @throws Exception
      */
  private List getProviderHeroImageDetails(String userType) throws Exception {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();    
    ResultSet resultset = null;
    HomePageImageDetailsVO HomePageImageVO = null;
    List providerHeroImageList = new ArrayList();
    vector.clear();
    vector.add(userType);
    //HOME_HEROIMAG_ID, ORDER_ITEM_ID, COLLEGE_ID, COLLEGE_NAME, COLLEGE_NAME_DISPLAY, MEDIA_ID, MEDIA_TYPE_ID, MEDIA_PATH, TITLE, SUB_TITLE,
    // IMAGE_URL, START_DATE, EXPIRY_DATE, STSTUS, CREATED_DATE, UPDATED_DATE
    try {
      resultset = dataModel.getResultSet("wuni_homepage_pkg.get_hero_image_fn", vector);
      while (resultset.next()) {
        HomePageImageVO = new HomePageImageDetailsVO();
        HomePageImageVO.setHomeHeroImageId(resultset.getString("HOME_HEROIMAGE_ID"));
        HomePageImageVO.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
        HomePageImageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
        HomePageImageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
        HomePageImageVO.setCollegeNameUrl(new SeoUrls().construnctUniHomeURL(HomePageImageVO.getCollegeId(), HomePageImageVO.getCollegeName()));
        HomePageImageVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
        HomePageImageVO.setMediaId(resultset.getString("MEDIA_ID"));
        HomePageImageVO.setMediaTypeId(resultset.getString("MEDIA_TYPE_ID"));
        HomePageImageVO.setMediaPath(resultset.getString("MEDIA_PATH"));
        if (!GenericValidator.isBlankOrNull(HomePageImageVO.getMediaPath())) {
          String splitMediaPath = HomePageImageVO.getMediaPath().substring(0, HomePageImageVO.getMediaPath().lastIndexOf("."));
          HomePageImageVO.setMediaPath992(splitMediaPath + "_992px"+HomePageImageVO.getMediaPath().substring(HomePageImageVO.getMediaPath().lastIndexOf("."), HomePageImageVO.getMediaPath().length()));
          HomePageImageVO.setMediaPath480(splitMediaPath + "_480px"+HomePageImageVO.getMediaPath().substring(HomePageImageVO.getMediaPath().lastIndexOf("."), HomePageImageVO.getMediaPath().length()));
        }
        HomePageImageVO.setTitle(resultset.getString("TITLE"));
        HomePageImageVO.setSubTitle(resultset.getString("SUB_TITLE"));
        HomePageImageVO.setImageUrl(resultset.getString("IMAGE_URL"));
        HomePageImageVO.setStartDate(resultset.getString("START_DATE"));
        HomePageImageVO.setExpiryDate(resultset.getString("EXPIRY_DATE"));
        HomePageImageVO.setStatus(resultset.getString("STATUS"));
        HomePageImageVO.setCreatedDate(resultset.getString("CREATED_DATE"));
        HomePageImageVO.setUpdatedDate(resultset.getString("UPDATED_DATE"));
        HomePageImageVO.setCollegeLogo(resultset.getString("COLLEGE_LOGO"));  //Added provider logo for 13_Dec_2016, by Thiyagu G. 
        HomePageImageVO.setDefaultText(resultset.getString("DEFAULT_TEXT"));
        HomePageImageVO.setDefaultUrl(resultset.getString("DEFAULT_URL"));
        providerHeroImageList.add(HomePageImageVO);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return providerHeroImageList;
  }
}
