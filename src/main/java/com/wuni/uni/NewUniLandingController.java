package com.wuni.uni;

import WUI.review.bean.ReviewListBean;
import WUI.search.form.RichProfileBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import mobile.util.MobileUtils;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.contenthub.controller.ContentHubController;
import com.wuni.util.seo.SeoPageNamesAndFlags;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.advert.NewUniLandingVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import com.wuni.util.valueobject.search.UniStatsInfoVO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.controller.clearing.ClearingProfileController;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.valueobject.interaction.CpeInteractionVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
  * @NewUniLanding
  * @author Amirtharaj
  * @version 1.0
  * @since 3.Nov.2015
  * @purpose This program is used as a common landing action for all the profile types eg: ClearingProfile,SP,UNI landing page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * Nov-03-15      Amirtharaj                1.0      Initial draft                                                546
  * Nov-24-15      Indumathi.S               1.1      Cost of Pint                                                 547
  * 19_Apr_16      Indumathi.S               1.2      Hero image on clearing page                                  551             
  * 20_Feb_16      Prabhakaran V.            1.3      Added collegeId/CollegeName for smart pixel tagging          573
  * 25_Sep_18      Sangeeth.S                1.4      Rediection of expired subject profile to profile page        581
  * 23_Oct_18      Sangeeth.S                1.5      Redirection of Clearing url when clearing is OFF             582
  * 18_Dec_18      Hema.S                    1.6      Added review breakdown and subject cursor                    584
  * 01_03_2019    Sangeeth.S                 1.7      Added for CMMT institution profile page
  * 14_05_2020    Sangeeth.S                 1.8      Added clearing profile related changes  
  * 14_06_2020    Sangeeth.S				 1.9      Added clearing show switch flag
  * 30_09_2020    Sujitha V                  1.10     Added post clearing sysvar for redirection
  * 20_10_2020    Sri Sankari R              1.11      Added dynamic meta details from Back office                wu_20201020
  */
@Controller
public class NewUniLandingController {

  @RequestMapping(value={"/university-profile/*/*","/subject-profile/*/*/*/*/*/*/subjectprofile","/university-profile/*/*/clearing"}, method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView newUniLanding(ModelMap model, HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    ICommonBusiness commonBusiness = (ICommonBusiness)SpringContextInjector.getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    RichProfileBean richProfileBean = new RichProfileBean();
    ContentHubVO contentHubVO = new ContentHubVO();
    NewUniLandingVO newUniLandingVO = new NewUniLandingVO();
    ClearingProfileVO clearingProfileVO = new ClearingProfileVO();
    CollegeUtilities uniUtils = new CollegeUtilities();
    RichProfileLanding richProfileLanding = new RichProfileLanding();
    ContentHubController contentHubAction = new ContentHubController();
    ClearingProfileController clearingProfileController = new ClearingProfileController();
    String isClearingProfile = request.getQueryString();
    CollegeNamesVO uniNames = null;
    int found = -1;
    String cpeQualificationNetworkId = (String)session.getAttribute("cpeQualificationNetworkId");
    String collegeId = "";
    String collegeName = "";
    String collegeNameDisplay = "";
    String urlCollegeName = "";
    String myhcProfileId = "0";
    boolean spProfile = false;
    String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    String[] urlArray = urlString.split("/");
    String clearingUrlFlag = "";
    String forwardString = "newUniLandingHome";
    String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN+request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "")+GlobalConstants.SLASH;
    canonicalUrl += (request.getQueryString()!= null) ? ("?"+request.getQueryString()): "";    
    CommonFunction common = new CommonFunction();
    String clearingOnOff = common.getWUSysVarValue("CLEARING_ON_OFF");
	String postClearingOnOff = common.getWUSysVarValue("POST_CLEARING_ON_OFF");
    model.addAttribute("POST_CLEARING_ON_OFF", postClearingOnOff);
    String constructedProfilePageUrl = "";
    String userJourneyFlag = "";
    boolean mobileFlag = new MobileUtils().userAgentCheck(request);
	if (StringUtils.equalsIgnoreCase("3", cpeQualificationNetworkId)) {
		model.addAttribute(SpringConstants.HIGH_LIGHT_TAB, GlobalConstants.PROFILE_PG_TAB);
      } else {
    	  model.addAttribute(SpringConstants.HIGH_LIGHT_TAB, GlobalConstants.PROFILE_UG_TAB);
      }
    //String userJourney = common.getClearingUserJourneyFlag(request); // Added for cmmt
    
    //Added Clearing url redirection when clearing is OFF for OCT_23_18 rel by Sangeeth.S
    if(GlobalConstants.OFF.equalsIgnoreCase(clearingOnOff) && StringUtils.equalsIgnoreCase(postClearingOnOff, GlobalConstants.OFF) && canonicalUrl.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1){
      String tempRedirectUrl = canonicalUrl.replace(("?"+GlobalConstants.SEO_PATH_CLEARING), "");
      tempRedirectUrl = tempRedirectUrl.replace((GlobalConstants.SEO_PATH_CLEARING+GlobalConstants.SLASH) , "");
      response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY); //setted redirection status 302      
      response.sendRedirect(tempRedirectUrl);
      return null;
    }
    if((GlobalConstants.ON.equalsIgnoreCase(clearingOnOff) || GlobalConstants.ON.equalsIgnoreCase(postClearingOnOff)) && canonicalUrl.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1 && !"3".equals(cpeQualificationNetworkId)){
	  if(GlobalConstants.ON.equalsIgnoreCase(clearingOnOff)) {
		  userJourneyFlag = GlobalConstants.CLEARING_PAGE_FLAG;  
	  }  
	  isClearingProfile = "clearing";
	  clearingUrlFlag = GlobalConstants.CLEARING_PAGE_FLAG;
	  String queryString = request.getQueryString();
	  if(!GenericValidator.isBlankOrNull(queryString) && queryString.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1) {
		  String tempRedirectUrl = canonicalUrl.replace(("?"+GlobalConstants.SEO_PATH_CLEARING), GlobalConstants.SEO_PATH_CLEARING+GlobalConstants.SLASH);
	      response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY); //setted redirection status 301      
	      response.sendRedirect(tempRedirectUrl);
	      return null;
	  }
    }
    //
    if (urlString != null) {
      found = urlString.indexOf(GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE);
    }
    if (found == -1) {
      found = urlString.indexOf(GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE);
      if (found == 0) {
        spProfile = true;
        myhcProfileId = urlArray[5];
      }
    }
    if (found != -1) {
      for (int i = 2; i < urlArray.length; i++) {
        if (i == 2) {
          urlCollegeName = urlArray[i];
        }
        if (i == 3) {
          collegeId = urlArray[i];
          uniNames = uniUtils.getCollegeNames(collegeId, request);
          if (uniNames != null) {
            collegeName = uniNames.getCollegeName().trim();
            collegeNameDisplay = uniNames.getCollegeNameDisplay();
          }
        }
      }
      if (!spProfile) {
        if (urlCollegeName != null && !urlCollegeName.equalsIgnoreCase(common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName))))) {
          request.setAttribute("reason_for_404", "The name of the college has been changed..");
          response.sendError(404, "404 error message");
          return null;
        } 
      } else {
        String isSpExpired = common.isSpProfExpired(myhcProfileId);
        if ("Y".equals(isSpExpired)) {
          constructedProfilePageUrl = new SeoUrls().construnctUniHomeURL(collegeId, collegeName);
          response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY); //setted redirection status 301 
          response.sendRedirect(GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN+constructedProfilePageUrl);//Added by Sangeeth.S for Aug_28_18 rel          
          return null;
        }
      }
    }
    request.setAttribute("spMyhcProfileId", myhcProfileId); //Added by Indumathi Nov-03-15
    String sessionId = new SessionData().getData(request, "x");
    sessionId = GenericValidator.isBlankOrNull(sessionId) ? "16180339" : sessionId;
    String userId = new SessionData().getData(request, "y");
    
    String basketId = "";
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    
    basketId = common.checkCookieStatus(request);
    if(GenericValidator.isBlankOrNull(basketId) && "0".equals(basketId)){
      basketId = null;
    }
    
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    //Added by Indumathi.S For unique content tab changes May_31_2016
    if (!spProfile) {
      newUniLandingVO.setCollegeId(collegeId);
      Map checkCourseExistsMap = commonBusiness.checkCourseExists(newUniLandingVO);
      ArrayList<NewUniLandingVO> studyLevelList = (ArrayList) checkCourseExistsMap.get("pc_profile_network_type");
      String crsAndAdvExistsUGFlag="N";
      String crsAndAdvExistsPGFlag="N";
      if(!CollectionUtils.isEmpty(studyLevelList)) {
    	  model.addAttribute("studyLevelList",studyLevelList);
    	  ArrayList<NewUniLandingVO> profileNetworkTypeList = studyLevelList;
    	  String profileNetworkType = profileNetworkTypeList.stream().map(ins ->(ins).getStudyMode()).collect(Collectors.joining("###"));
    	  if(StringUtils.indexOf(profileNetworkType, GlobalConstants.PROFILE_UG_TAB) > -1){
			  crsAndAdvExistsUGFlag = "Y";
		  }
		  if(StringUtils.indexOf(profileNetworkType, GlobalConstants.PROFILE_PG_TAB) > -1) {
			  crsAndAdvExistsPGFlag = "Y";
		  }
    	 if(StringUtils.equalsIgnoreCase("Y",crsAndAdvExistsPGFlag) && StringUtils.equalsIgnoreCase("N",crsAndAdvExistsUGFlag)) {
           cpeQualificationNetworkId = "3";
    	   model.addAttribute(SpringConstants.HIGH_LIGHT_TAB, GlobalConstants.PROFILE_PG_TAB);
    	 }
    	 if(StringUtils.equalsIgnoreCase("N",crsAndAdvExistsPGFlag) && StringUtils.equalsIgnoreCase("Y",crsAndAdvExistsUGFlag)) {
           cpeQualificationNetworkId = "2";
   	       model.addAttribute(SpringConstants.HIGH_LIGHT_TAB, GlobalConstants.PROFILE_UG_TAB);
    	 }
      }
    }
    
    //End May_31_2016
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
    String profileType = common.getInstProfileType(collegeId, StringUtils.equals(myhcProfileId, "0") ? null : myhcProfileId, cpeQualificationNetworkId, clearingUrlFlag);
	//Added Clearing url to in year url redirection when post clearing is ON for OCT_20_2020 rel by Vedha.R
    if((GlobalConstants.ON.equalsIgnoreCase(clearingOnOff)||StringUtils.equalsIgnoreCase(postClearingOnOff, GlobalConstants.ON)) && canonicalUrl.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1 && !"3".equals(cpeQualificationNetworkId) && !GlobalConstants.CLEARING_PROFILE.equalsIgnoreCase(profileType)){  	  
  		String tempRedirectUrl = canonicalUrl.replace(("?"+GlobalConstants.SEO_PATH_CLEARING), GlobalConstants.SEO_PATH_CLEARING+GlobalConstants.SLASH);
  		tempRedirectUrl = tempRedirectUrl.replace((GlobalConstants.SEO_PATH_CLEARING+GlobalConstants.SLASH) , "");
  		response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY); //setted redirection status 302      
  	    response.sendRedirect(tempRedirectUrl);
  	    return null;
    }
    if(StringUtils.equalsIgnoreCase(postClearingOnOff, GlobalConstants.ON)) {
    	model.addAttribute("pagePathGALog", canonicalUrl);
    }
    if (!GenericValidator.isBlankOrNull(isClearingProfile) && "clearing".equalsIgnoreCase(isClearingProfile)&& (("ON").equals(clearingOnOff) || GlobalConstants.ON.equalsIgnoreCase(postClearingOnOff))&& GlobalConstants.CLEARING_PROFILE.equalsIgnoreCase(profileType)) {
      //Added clearing profile related changes by Sangeeth.S for JUNE_20202 release  ver 1.8      
      request.setAttribute("newUniPageName", "CLEARING PROFILE");
      request.setAttribute("newUniParamFlag", "CLEARING PROFILE");
      request.setAttribute(SpringConstants.SET_PROFILE_TYPE, GlobalConstants.CLEARING_PROFILE);
	  model.addAttribute(SpringConstants.HIGH_LIGHT_TAB, GlobalConstants.PROFILE_CLEARING_TAB); //Added for clearingswitchOff updates
      request.setAttribute("studyLevelDesc", "Clearing");
      request.setAttribute(SpringConstants.CLEARING_HEADER, "TRUE");
      clearingProfileVO.setSessionId(sessionId);
      clearingProfileVO.setUserId("0".equals(userId) ? null : userId);
      clearingProfileVO.setBasketId(basketId);
      clearingProfileVO.setCollegeId(collegeId);
      clearingProfileVO.setNetworkId(GlobalConstants.CLEARING_NETWORK_ID);      
      clearingProfileVO.setClientIp(clientIp);
      clearingProfileVO.setClientBrowser(request.getHeader(SpringConstants.USER_AGENT));      
      clearingProfileVO.setRequestUrl(getFullURL(request, true));
      clearingProfileVO.setReferelUrl(request.getHeader(SpringConstants.REFERER));
      clearingProfileVO.setMetaPageName(SeoPageNamesAndFlags.CLEARING_PROFILE_PAGE_NAME);
      clearingProfileVO.setMetaPageFlag(SeoPageNamesAndFlags.CLEARING_PROFILE_PAGE_FLAG);
      clearingProfileVO.setTrackSession(new SessionData().getData(request, SpringConstants.USER_TRACK_ID));   
      clearingProfileVO.setClearingProfilePageURL(canonicalUrl);
      clearingProfileController.getClearingProfileDetails(clearingProfileVO, request, response,session);
      request.setAttribute(SpringConstants.USER_JOURNEY_FLAG, userJourneyFlag);
      if(GlobalConstants.ON.equalsIgnoreCase(postClearingOnOff) && canonicalUrl.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1) {
    	  model.addAttribute("inYearUrl", canonicalUrl.replace((GlobalConstants.SEO_PATH_CLEARING+GlobalConstants.SLASH) , ""));
      }
      return new ModelAndView("clearing-profile");
      // End of the clearing related changes
    }else if (!GenericValidator.isBlankOrNull(profileType) && "CH_INST_PROFILE".equalsIgnoreCase(profileType)) {
	  contentHubVO.setUserJourney(userJourneyFlag);
      contentHubVO.setSessionId(sessionId);
      contentHubVO.setUserId("0".equals(userId) ? null : userId);
      contentHubVO.setBasketId(basketId);
      contentHubVO.setCollegeId(collegeId);
      contentHubVO.setNetworkId(cpeQualificationNetworkId);
      contentHubVO.setRequestDesc("overview");
      contentHubVO.setClientIp(clientIp);
      contentHubVO.setClientBrowser(request.getHeader(SpringConstants.USER_AGENT));
      contentHubVO.setCollegeSection(null);
      contentHubVO.setRequestUrl(getFullURL(request, true));
      contentHubVO.setReferelUrl(request.getHeader(SpringConstants.REFERER));
      contentHubVO.setMetaPageName(SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG);
      contentHubVO.setMetaPageFlag(SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME);
      contentHubVO.setTrackSession(new SessionData().getData(request, SpringConstants.USER_TRACK_ID));
      contentHubVO.setUserJourney(userJourneyFlag);
      contentHubVO.setContentHubPageUrl(canonicalUrl);
      contentHubAction.getContentHubDetails(contentHubVO, request, response,session, model);
      request.setAttribute(SpringConstants.USER_JOURNEY_FLAG, userJourneyFlag);
      return new ModelAndView("content-hub");
    } else if (!GenericValidator.isBlankOrNull(profileType) && "RICH_INST_PROFILE".equalsIgnoreCase(profileType)) {
	richProfileBean.setUserJourney(userJourneyFlag);
      richProfileBean.setCollegeId(collegeId);
      richProfileBean.setCollegeName(collegeName);
      richProfileBean.setCollgeDispName(collegeNameDisplay);
      richProfileBean.setNetworkId(cpeQualificationNetworkId);
      richProfileBean.setProfileType("RICH_INST_PROFILE");
      richProfileBean.setMyhcProfileId(null);
      richProfileBean.setRichOrSubPageUrl(canonicalUrl);
      richProfileLanding.getRichProfileDetils(richProfileBean, request, response,session, model);
      request.setAttribute(SpringConstants.CANONICAL_URL, canonicalUrl);
      request.setAttribute(SpringConstants.USER_JOURNEY_FLAG, userJourneyFlag);
      request.setAttribute("mobileFlag", mobileFlag);
      return new ModelAndView("richProfileLanding");
    } else if (!GenericValidator.isBlankOrNull(profileType) && "RICH_SUB_PROFILE".equalsIgnoreCase(profileType)) {
	richProfileBean.setUserJourney(userJourneyFlag);
      richProfileBean.setCollegeId(collegeId);
      richProfileBean.setCollegeName(collegeName);
      richProfileBean.setCollgeDispName(collegeNameDisplay);
      richProfileBean.setNetworkId(cpeQualificationNetworkId);
      richProfileBean.setMyhcProfileId(myhcProfileId);
      richProfileBean.setProfileType("RICH_SUB_PROFILE");
      richProfileBean.setRichOrSubPageUrl(canonicalUrl);
      richProfileLanding.getRichProfileDetils(richProfileBean, request, response,session, model);
      request.setAttribute(SpringConstants.CANONICAL_URL, canonicalUrl);
      request.setAttribute(SpringConstants.USER_JOURNEY_FLAG, userJourneyFlag);
      request.setAttribute("mobileFlag", mobileFlag);
      return new ModelAndView("richProfileLanding");
    } else {
      if (!spProfile) {
        newUniLandingVO.setPageName("INST_PROFILE");
        request.setAttribute("newUniPageName", "COLLEGE LANDING PAGE");
        request.setAttribute("newUniParamFlag", "UNI_LANDING");
        if ("2".equals(cpeQualificationNetworkId)) {
          request.setAttribute(SpringConstants.HIGH_LIGHT_TAB, "UG");
        } else {
          request.setAttribute(SpringConstants.HIGH_LIGHT_TAB, "PG");
        }
      } else {
        newUniLandingVO.setPageName("SUB_PROFILE");
        newUniLandingVO.setMyhcProfileId(myhcProfileId);
      }
    }
    newUniLandingVO.setUserJourney(userJourneyFlag); // added for cmmt
    newUniLandingVO.setUserId(userId);
    newUniLandingVO.setSessionId(sessionId);
    newUniLandingVO.setCollegeId(collegeId);
    newUniLandingVO.setRequestDesc("overview"); // requestDescription
    newUniLandingVO.setClientIp(clientIp);
    newUniLandingVO.setUserAgent(request.getHeader(SpringConstants.USER_AGENT));
    newUniLandingVO.setSectionName("WUNI OVERVIEW"); //collegeSection
    newUniLandingVO.setRequestURL(getFullURL(request, true));
    newUniLandingVO.setReferelURL(request.getHeader(SpringConstants.REFERER));
    newUniLandingVO.setNetworkId(cpeQualificationNetworkId);
    newUniLandingVO.setBasketId(basketId);
    newUniLandingVO.setTrackSessionId(new SessionData().getData(request, SpringConstants.USER_TRACK_ID));
    newUniLandingVO.setNonAdvPageUrl(canonicalUrl);
    Map newUniLandingMap = commonBusiness.getNewUniLandingData(newUniLandingVO);
    //Added byIndumathi.S For Cost of Pint Nov-24-Rel
    if (!spProfile) {
      List costOfPint = (List)newUniLandingMap.get("PC_COST_OF_LIVING");
      if (costOfPint != null && !costOfPint.isEmpty()) {
        request.setAttribute("costOfPint", costOfPint);
        Iterator costOfPintItr = costOfPint.iterator();
        while (costOfPintItr.hasNext()) {
          RichProfileVO costOfPintVO = (RichProfileVO)costOfPintItr.next();
          String whatuniCostPint = costOfPintVO.getWhatuniCostPint();
          if (!GenericValidator.isBlankOrNull(whatuniCostPint)) {
            if (whatuniCostPint.contains("-")) {
              String[] costPint = whatuniCostPint.split("-");
              if (costPint.length > 1) {
                request.setAttribute("whatuniCostPintOne", costPint[0].trim());
                request.setAttribute("whatuniCostPintTwo", costPint[1].trim());
              }
            } else {
              request.setAttribute("whatuniCostPint", costOfPintVO.getWhatuniCostPint());
            }
          }
        }
      }
      //End
    }
    if (newUniLandingMap != null) {
      List listOfLatestReviews = (List)newUniLandingMap.get("pc_latest_uni_reviews");
      if (listOfLatestReviews != null && !listOfLatestReviews.isEmpty()) {
        request.setAttribute("listOfLatestReviews", listOfLatestReviews);
          ReviewListBean reviewListBean = (ReviewListBean)listOfLatestReviews.get(0);
          request.setAttribute("collegeId",reviewListBean.getCollegeId());
          request.setAttribute("collegeName",reviewListBean.getCollegeName());
          request.setAttribute("rating",reviewListBean.getReviewRatings());
        String newUniReviewUrl = new SeoUrls().constructReviewPageSeoUrl(collegeName, collegeId);
        request.setAttribute("newUniReviewUrl", newUniReviewUrl);
      }
      //Added review subject cursor for review redesign by Hema.S on 18_DEC_2018_REL
       List reviewSubjectList = (List)newUniLandingMap.get("PC_REVIEW_SUBJECT");
       if (reviewSubjectList != null && !reviewSubjectList.isEmpty()) {
         request.setAttribute("reviewSubjectList", reviewSubjectList);
       }
      //Added review breakdown cursor for review redesign by Hema.S on 18_DEC_2018_REL   
       List reviewBreakdownlist = (List)newUniLandingMap.get("PC_REVIEW_BREAKDOWN");
       if (reviewBreakdownlist != null && !reviewBreakdownlist.isEmpty()) {
         request.setAttribute("reviewBreakdownlist", reviewBreakdownlist);
         ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakdownlist.get(0);
         request.setAttribute("questionTitle",reviewBreakDownVO.getQuestionTitle());
        request.setAttribute("rating",reviewBreakDownVO.getRating());
        request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
        request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
         
       }
        ArrayList commonPhrasesList = (ArrayList)newUniLandingMap.get("PC_COMMON_PHRASES");
        if (commonPhrasesList != null && !commonPhrasesList.isEmpty()) { 
          CommonPhrasesVO  commonPhrasesVO = null;
          for(int i = 0; i < commonPhrasesList.size(); i++){
            commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(i);
            if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
              request.setAttribute("commonPhrasesList", commonPhrasesList);
              break;
            }
          }        
        }
      //
      List uniprofilecontent = (List)newUniLandingMap.get("pc_uni_profile_content");
      if (uniprofilecontent != null && !uniprofilecontent.isEmpty()) {
        request.setAttribute("collegeOverViewList", uniprofilecontent);
        
      }
        request.setAttribute("hitbox_college_name", collegeName);//Added this for Non-Advertiser GA logging(dimension 3) by Hema.S on 23_OCT_2018_REL
      //
      List cpeProfileInteractionDetails = (List)newUniLandingMap.get("pc_profile_interaction_details");
      if (cpeProfileInteractionDetails != null && !cpeProfileInteractionDetails.isEmpty()) {
        request.setAttribute("listOfCollegeInteractionDetailsWithMedia", cpeProfileInteractionDetails);
        Iterator profIntIter = cpeProfileInteractionDetails.iterator();
        while (profIntIter.hasNext()) {
          CpeInteractionVO cpeInteractionIter = (CpeInteractionVO)profIntIter.next();
          if (GenericValidator.isBlankOrNull(cpeInteractionIter.getSubOrderItemId())) {
            request.setAttribute(SpringConstants.INTERACTION_EXIST, GlobalConstants.FALSE);
            break;
          }
        }
      }
      //
      List listOfUniStats = (List)newUniLandingMap.get("pc_college_unistats_info");
      if (listOfUniStats != null && !listOfUniStats.isEmpty()) {
        request.setAttribute("listOfUniStats", listOfUniStats);
        Iterator uniStatsIter = listOfUniStats.iterator();
        while (uniStatsIter.hasNext()) {
          UniStatsInfoVO uniStatsVO = (UniStatsInfoVO)uniStatsIter.next();
          if (!GenericValidator.isBlankOrNull(uniStatsVO.getTimesRank()) || !GenericValidator.isBlankOrNull(uniStatsVO.getFullPartTime()) || !GenericValidator.isBlankOrNull(uniStatsVO.getUkInternation()) || !GenericValidator.isBlankOrNull(uniStatsVO.getSchoolMature())) {
            request.setAttribute("studStatsExist", "true");
          }
        }
      }
      //Added by Indumathi.S Apr_19_2016 For hero image in clearing page
      List heroImgList = (List)newUniLandingMap.get("pc_get_hero_image");
      if (heroImgList != null && !heroImgList.isEmpty()) {
        Iterator profileMediaItr = heroImgList.iterator();
        while (profileMediaItr.hasNext()) {
          RichProfileVO richProfileMediaVO = (RichProfileVO)profileMediaItr.next();
          if (!GenericValidator.isBlankOrNull(richProfileMediaVO.getMediaPath()) && GlobalConstants.CLEARING_PAGE_FLAG.equalsIgnoreCase(richProfileBean.getProfileType())) {
            request.setAttribute("profileImgPath",  CommonUtil.getImgPath(richProfileMediaVO.getMediaPath(), 0));
          }
          break;
        }
      }
      //End
      List scholarshipPodList = (List)newUniLandingMap.get("pc_scholarship");
      if (scholarshipPodList != null && !scholarshipPodList.isEmpty()) {
        String scholarShipViewAllUrl = new SeoUrls().constructUniSpecificScholarshipUrl(collegeId, collegeName);
        request.setAttribute("scholarshipPodList", scholarshipPodList);
        request.setAttribute("scholarShipViewAllUrl", scholarShipViewAllUrl);
      }
      //
      List locationInfoList = (List)newUniLandingMap.get("pc_admin_venue");
      if (locationInfoList != null && !locationInfoList.isEmpty()) {
        request.setAttribute("locationInfoList", locationInfoList);
        for (Iterator iterator = locationInfoList.iterator(); iterator.hasNext(); ) {
          LocationsInfoVO bean = (LocationsInfoVO)iterator.next();
          request.setAttribute("latitudeStr", bean.getLatitude());
          request.setAttribute("longitudeStr", bean.getLongitude());
          request.setAttribute("cityGudieHeadLine", bean.getHeadLine());
          request.setAttribute("cityGudieLocation", bean.getLocation());
        }
      }
      //
      List uniInfoList = (List)newUniLandingMap.get("pc_uni_info");
      if (uniInfoList != null && !uniInfoList.isEmpty()) {
        request.setAttribute("uniInfoList", uniInfoList);
        Iterator uniInfoIter = uniInfoList.iterator();
        while (uniInfoIter.hasNext()) {
          ReviewListBean uniInfoBean = (ReviewListBean)uniInfoIter.next();
          if (!GenericValidator.isBlankOrNull(uniInfoBean.getPullQuotes()) || !GenericValidator.isBlankOrNull(uniInfoBean.getAwardImage())) {
            request.setAttribute("provQuickInfo", "true");
          }
          if (GenericValidator.isBlankOrNull(uniInfoBean.getPullQuotes()) || !GenericValidator.isBlankOrNull(uniInfoBean.getAwardImage())) {
            request.setAttribute("awardExist", "true");
          }
          //Set request attribute for schema tag on 16_May_2017, By Thiyagu G
          request.setAttribute("collegeNameDisplay", uniInfoBean.getCollegeDisplayName());
          request.setAttribute("collegeName", uniInfoBean.getCollegeDisplayName());
          request.setAttribute("reviewCount", uniInfoBean.getReviewCount());
          request.setAttribute("overallRating", uniInfoBean.getOverallRatingExact());
          break;
        }
      }
      //
      List uniRankingList = (List)newUniLandingMap.get("pc_uni_raking");
      if (uniRankingList != null && !uniRankingList.isEmpty()) {
        request.setAttribute("uniRankingList", uniRankingList);
      }
      //
      List uniSPList = (List)newUniLandingMap.get("pc_uni_sp_list");
      if (uniSPList != null && !uniSPList.isEmpty()) {
        request.setAttribute("uniSPList", uniSPList);
        request.setAttribute("uniSPListSize", String.valueOf(uniSPList.size()));
      }
      //
      List compareSuggestionsList = (List)newUniLandingMap.get("pc_unis_u_may_like");
      if (compareSuggestionsList != null && !compareSuggestionsList.isEmpty()) {
        request.setAttribute("listOfTopCourses", compareSuggestionsList);
      }
      //
      String courseExistsFlag = (String)newUniLandingMap.get("p_course_exist_flag");
      if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
        String courseUrl = "";
        request.setAttribute("courseExistsFlag", courseExistsFlag);
        String collegeName1 = common.replaceURL(common.getCollegeName(collegeId, request)).toLowerCase();
        //Changed new all graduate courses url pettern, By Thiyagu G for 27_Jan_2016.
        String qual="degree";
        if (!GenericValidator.isBlankOrNull(newUniLandingVO.getNetworkId()) && "3".equals(newUniLandingVO.getNetworkId())) {
          qual="postgraduate";
        }
        //Added by Indumathi.S Jan-27-16 Rel 
        if("N".equalsIgnoreCase(courseExistsFlag)){
          qual = "all";
        }
        courseUrl = new SeoUrls().constructCourseUrl(qual, richProfileBean.getProfileType(), collegeName1);
        //End
        request.setAttribute("courseUrl", courseUrl);
      }
      //
      String openDaysExistFlag = (String)newUniLandingMap.get("p_open_days_exist_flag");
      if (!GenericValidator.isBlankOrNull(openDaysExistFlag) && "Y".equals(openDaysExistFlag)) {
        request.setAttribute("openDaysUrlExist", new SeoUrls().constructOpendaysSeoUrl(collegeName, collegeId));
      }
      //
      String collegeBasketFlag = (String)newUniLandingMap.get("p_college_in_basket");
      if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
        request.setAttribute("COLLEGE_IN_BASKET", collegeBasketFlag);
      }
      //
      String collegeLocation = (String)newUniLandingMap.get("p_uni_town");
      if (!GenericValidator.isBlankOrNull(collegeLocation)) {
        request.setAttribute("collegeLocation", collegeLocation);
      }
      //
      String studyAbroadFlag = (String)newUniLandingMap.get("p_study_abroad_flag");
      if (!GenericValidator.isBlankOrNull(studyAbroadFlag)) {
        request.setAttribute("studyAbroadFlag", studyAbroadFlag);
      }
      //Added breadCrumb Indumathi.S Feb-16-2016
      String  breadCrumb = new SeoUtilities().getBreadCrumb("UNI_PROFILE_PAGE", "", "", "", "", collegeId);
      if (breadCrumb != null && breadCrumb.trim().length() > 0) {
        request.setAttribute("breadCrumb", breadCrumb);
      }
      //
      if (!GenericValidator.isBlankOrNull(richProfileBean.getProfileType()) && GlobalConstants.CLEARING_PAGE_FLAG.equalsIgnoreCase(richProfileBean.getProfileType())) {
        if (!GenericValidator.isBlankOrNull((String)request.getAttribute(SpringConstants.INTERACTION_EXIST)) && GlobalConstants.FALSE.equalsIgnoreCase((String)request.getAttribute(SpringConstants.INTERACTION_EXIST))) {
          request.setAttribute(SpringConstants.SET_PROFILE_TYPE, "CLEARING_LANDING");
        } else {
          request.setAttribute(SpringConstants.SET_PROFILE_TYPE, GlobalConstants.CLEARING_PROFILE);
        }
        request.setAttribute(SpringConstants.NEW_PROVIDER_TYPE, GlobalConstants.CLEARING_PAGE_FLAG);
        request.setAttribute(SpringConstants.CLEARING_HEADER, "TRUE");
      } else {
        request.setAttribute(SpringConstants.CLEARING_HEADER, "FALSE");
        request.setAttribute(SpringConstants.NEW_PROVIDER_TYPE, newUniLandingVO.getPageName());
        if (!spProfile) {
          request.setAttribute(SpringConstants.SET_PROFILE_TYPE, "NORMAL");
        } else {
          request.setAttribute(SpringConstants.SET_PROFILE_TYPE, "SUB_PROFILE");
        }
      }
      //
      if (!"NO_COURSE".equalsIgnoreCase(courseExistsFlag) || request.getAttribute("provQuickInfo") != null || request.getAttribute("uniRankingList") != null || ("N".equals(courseExistsFlag) && request.getAttribute(SpringConstants.NEW_PROVIDER_TYPE) != null && "CLEARING".equals(request.getAttribute(SpringConstants.NEW_PROVIDER_TYPE)))) {
        request.setAttribute("showUniInfopod", "true");
      }
      //
      String pixelTracking = (String)newUniLandingMap.get("p_pixel_tracking_code");
      if (!GenericValidator.isBlankOrNull(pixelTracking)) {
        request.setAttribute("pixelTracking", pixelTracking);
      }
      //
      String addedToFinalChoice = (String)newUniLandingMap.get("p_final_choices_exist_flag");
      if (!GenericValidator.isBlankOrNull(addedToFinalChoice)) {
        request.setAttribute("addedToFinalChoice", addedToFinalChoice);
      }
      //
      String departmentName = (String)newUniLandingMap.get("p_dept_name");
      if (!GenericValidator.isBlankOrNull(departmentName)) {
        request.setAttribute("spdeptName", departmentName);
      }
      String advFlag = (String)newUniLandingMap.get("o_adv_flag");
      if (!GenericValidator.isBlankOrNull(advFlag) && !"Y".equals(advFlag)) {
        request.setAttribute("nonAdvertFlag", "true");
      } else {
        request.setAttribute("nonAdvertFlag", "false");
      }
      //Added non advisor provider's logo to show in registeration lightbox for 24_Nov_2015, by Thiyagu G. - Start
      String collegeLogo = (String)newUniLandingMap.get("p_college_logo");
      if (!GenericValidator.isBlankOrNull(collegeLogo)) {
        request.setAttribute("collegeLogo", collegeLogo);
      } else {
        request.setAttribute("collegeLogo", "");
      }
      //Added by Indumathi.S For Unique content changes May_31_2016
      String ugContentFlag = (String)newUniLandingMap.get("p_ug_content_flag");
      if("Y".equalsIgnoreCase(ugContentFlag)) {
        request.setAttribute("ugContentFlag",ugContentFlag);
      }
      //End
      //Added for CMMT institution profile page      
      String clearingCourseExistFlag = (String)newUniLandingMap.get("P_CL_COURSE_EXIST_FLAG");
      //      
      //Added for the top clearing switch pod For June_23_rel by sangeeth.s
      String showSwitchFlag = (String)newUniLandingMap.get("P_CLEARING_SWITCH_FLAG");
      
      // Added for dynamic meta data details from Back office, 20201020 rel by Sri Sankari
	  ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) newUniLandingMap.get("PC_META_DETAILS");
	  new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);  
      request.setAttribute("SHOW_SWITCH_FLAG", showSwitchFlag);
      //
      request.setAttribute(SpringConstants.USER_JOURNEY_FLAG, userJourneyFlag);
      request.setAttribute("CLEARING_COURSE_EXIST_FLAG", clearingCourseExistFlag);
      
      //Added non advisor provider's logo to show in registeration lightbox for 24_Nov_2015, by Thiyagu G. - End
      request.setAttribute("COLLEGEID_IN_INTERACTION", collegeId);
      request.setAttribute("interactionCollegeId", collegeId);
      request.setAttribute("interactionCollegeName", collegeName);
      request.setAttribute("interactionCollegeNameDisplay", collegeNameDisplay);
      /* Added college display name for smart pixel by Prabha on 20_02_2018 */
      if(!GenericValidator.isBlankOrNull(collegeName)){
        request.setAttribute("cDimCollegeDisName", ("\""+collegeName+"\""));
      }
      /* End od smart pixel code */
      request.setAttribute("hitbox_college_id", collegeId);
      request.setAttribute("collegeId", collegeId);
      request.setAttribute("cDimCollegeId", collegeId);
      request.setAttribute("cDimCollegeIds", ("'" + collegeId + "'")); //Added collegeId for smart pixel tagging by Prabha on 20.02.2018
      String getCurrentIpURL = new SeoUrls().construnctUniHomeURL(collegeId, collegeName);
      request.setAttribute("clearingprovUrl", getCurrentIpURL + "?clearing");
      request.setAttribute("switchProvUrl", getCurrentIpURL);
      request.setAttribute(SpringConstants.CANONICAL_URL, canonicalUrl);
      if ("2".equals(cpeQualificationNetworkId)) {
        new GlobalFunction().getStudyLevelDesc("M", request);
      } else {
        new GlobalFunction().getStudyLevelDesc("L", request);
      }
    }
    return new ModelAndView(forwardString);
  }

  private String getFullURL(HttpServletRequest request, boolean queryString) {
    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016
    String url = requestUrl;
      if (queryString && request.getQueryString() != null) {
        url += '?';
        url += request.getQueryString();
      }
    return url;
  }

}
