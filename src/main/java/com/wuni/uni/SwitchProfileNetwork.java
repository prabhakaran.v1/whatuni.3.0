package com.wuni.uni;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SwitchProfileNetwork {
	
 @RequestMapping(value = "/switch-profile-network", method={RequestMethod.GET,RequestMethod.POST})
  public ModelAndView switchProfileNetwork(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    String networkId = "";
    if (!GenericValidator.isBlankOrNull(request.getParameter("nwId"))) {
      networkId = request.getParameter("nwId");
      session.setAttribute("cpeQualificationNetworkId", networkId);
    }
    return null;
  }
}
