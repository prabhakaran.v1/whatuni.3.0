package com.wuni.uni;

import WUI.search.form.RichProfileBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import spring.valueobject.seo.SEOMetaDetailsVO;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoPageNamesAndFlags;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;
import com.wuni.util.valueobject.advert.CollegeProfileVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.openday.OpendaysVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;
import com.wuni.util.valueobject.review.ReviewSubjectVO;
import com.wuni.util.valueobject.review.UniOverallReviewsVO;
import com.wuni.util.valueobject.review.UserReviewsVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.ModelAndView;

/**
  * @UniLanding
  * @author Amirtharaj
  * @version 1.0
  * @since 13.JAN.2015
  * @purpose This program is used to display the university landing page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * Nov-24-15      Indumathi.S               1.0      Cost of Pint                                                 547
  * 
  * May-31-16      Prabhakaran V.            1.2      Added hide location param..                                  552
  * Mar-03-19      Sangeeth.S                1.3      Added for CMMT institution profile page  
  * Apr-24-20 	   Sangeeth.S				 1.4      Handled open day event type changes for dimension 18
  * 14_06_2020    Sangeeth.S				 1.5      Added clearing show switch flag
  * 20_10_2020    Sri Sankari R              1.6      Added dynamic meta details from Back office                           wu_20201020
  */
public class RichProfileLanding {
  public ModelAndView getRichProfileDetils(RichProfileBean richProfileBean, HttpServletRequest request, HttpServletResponse response,HttpSession session, ModelMap model) throws Exception {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    RichProfileVO richProfileVO = new RichProfileVO();
    SessionData sessionDataObj = new SessionData();
    String sessionId = sessionDataObj.getData(request, "x");
    sessionId = GenericValidator.isBlankOrNull(sessionId)? "16180339": sessionId;
    richProfileVO.setSessionId(sessionId);
    CommonFunction common = new CommonFunction();
    CommonUtil util = new CommonUtil();
    String userId = sessionDataObj.getData(request, "y");
    
    String basketId = "";
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    
    basketId = common.checkCookieStatus(request);
    if(GenericValidator.isBlankOrNull(basketId) && "0".equals(basketId)){
      basketId = null;
    }
    
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String cpeQualificationNetworkId = (String)session.getAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID);
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = GlobalConstants.UG_NETWORK_ID;
    }
    if (GlobalConstants.UG_NETWORK_ID.equals(cpeQualificationNetworkId)) {
      request.setAttribute("highlightTab", "UG");
    } else {
      request.setAttribute("highlightTab", "PG");
    }
    session.removeAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID);
    session.setAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID, cpeQualificationNetworkId);
    //richProfileVO.setUserJourney(userJourneyFlag); // added for cmmt
    richProfileVO.setUserJourney(richProfileBean.getUserJourney());
    richProfileVO.setUserId(userId);
    richProfileVO.setBasketId(basketId);
    richProfileVO.setCollegeId(richProfileBean.getCollegeId());
    richProfileVO.setNetworkId(richProfileBean.getNetworkId());
    richProfileVO.setRequestDesc("overview"); // requestDescription
    richProfileVO.setClientIp(clientIp); //    
    richProfileVO.setUserAgent(request.getHeader("user-agent"));
    richProfileVO.setSectionName("WUNI OVERVIEW"); //collegeSection
    richProfileVO.setRequestURL(CommonUtil.getRequestedURL(request)); //Change the getRequestURL by Prabha on 28_Jun_2016
    richProfileVO.setReferelURL(request.getHeader("referer"));
    richProfileVO.setMetaPageName(SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG);
    richProfileVO.setMetaPageFlag(SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME);
    richProfileVO.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
    richProfileVO.setProfileType(richProfileBean.getProfileType());
    richProfileVO.setRichOrSubPageUrl(richProfileBean.getRichOrSubPageUrl());
    if (richProfileBean.getProfileType() != null) {
      request.setAttribute("richProfileType", richProfileBean.getProfileType());
    }
    richProfileVO.setMyhcProfileId(richProfileBean.getMyhcProfileId());
    Map richProfileDataMap = commonBusiness.getRichProfileUniLandingData(richProfileVO);
    if (richProfileDataMap != null) {
      request.setAttribute(SpringConstants.COLLEGE_ID, richProfileBean.getCollegeId());
      request.setAttribute("collegeName", richProfileBean.getCollegeName());
      request.setAttribute("collegeNameDisplay", richProfileBean.getCollgeDispName());
      request.setAttribute("hitbox_college_id", richProfileBean.getCollegeId());
      request.setAttribute("hitbox_college_name", richProfileBean.getCollegeName());
      List listOfUniStats = (List)richProfileDataMap.get("oc_college_unistats_info");
      if (!CollectionUtils.isEmpty(listOfUniStats)) {
        request.setAttribute("listOfUniStats", listOfUniStats);
      }
      //
      List uniSPList = (List)richProfileDataMap.get("oc_uni_sp_list");
      if (!CollectionUtils.isEmpty(uniSPList)) {
        request.setAttribute("uniSPList", uniSPList);
        request.setAttribute("uniSPListSize", String.valueOf(uniSPList.size()));
      }
      //
      List listOfOpendayInfo = (List)richProfileDataMap.get("oc_open_day_info");
      if (!CollectionUtils.isEmpty(listOfOpendayInfo)) {
    	OpendaysVO opendaysVO = null;
        String openDayEventType = "";
    	for(int i=0;i < listOfOpendayInfo.size(); i++) {
    	  opendaysVO = (OpendaysVO)listOfOpendayInfo.get(i);    	  
    	  openDayEventType = util.setEventTypeValueGA(openDayEventType, opendaysVO.getEventCategoryName());    	  
        }
    	if(!GenericValidator.isBlankOrNull(openDayEventType)) {
    	  openDayEventType = openDayEventType.substring(0, openDayEventType.length() - 1);
    	  request.setAttribute("openDayEventType", openDayEventType);//For logging open day event type in dimension 18 by sangeeth.S in APR-24-20
    	}
    	request.setAttribute("listOfOpendayInfo", listOfOpendayInfo);
      }
      
      //
      List listOfLatestReviews = (List)richProfileDataMap.get("oc_latest_uni_reviews");
      if (!CollectionUtils.isEmpty(listOfLatestReviews)) {
        request.setAttribute("listOfLatestReviews", listOfLatestReviews);
          UserReviewsVO userReviewsVO = (UserReviewsVO)listOfLatestReviews.get(0);
          request.setAttribute(SpringConstants.COLLEGE_ID,userReviewsVO.getCollegeId());
          request.setAttribute("collegeName",userReviewsVO.getCollegeName());
        String newUniReviewUrl = new SeoUrls().constructReviewPageSeoUrl(richProfileBean.getCollegeName(), richProfileBean.getCollegeId());
        request.setAttribute("newUniReviewUrl", newUniReviewUrl);
      }
      //
       List reviewSubjectList = (List)richProfileDataMap.get("PC_REVIEW_SUBJECT");
       if (!CollectionUtils.isEmpty(reviewSubjectList)) {
         request.setAttribute("reviewSubjectList", reviewSubjectList);
           ReviewSubjectVO reviewSubjectVO = (ReviewSubjectVO)reviewSubjectList.get(0);
           request.setAttribute("firstSubjectName",reviewSubjectVO.getSubjectName());
       }
       //
        List reviewBreakdownlist = (List)richProfileDataMap.get("PC_REVIEW_BREAKDOWN");
        if (!CollectionUtils.isEmpty(reviewBreakdownlist)) {
          request.setAttribute("reviewBreakdownlist", reviewBreakdownlist);
          ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakdownlist.get(0);
          request.setAttribute("questionTitle",reviewBreakDownVO.getQuestionTitle());
          request.setAttribute("rating",reviewBreakDownVO.getRating());    
          request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
          request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
        }
        List commonPhrasesList = (List)richProfileDataMap.get("PC_COMMON_PHRASES");
        if (!CollectionUtils.isEmpty(commonPhrasesList)) { 
          CommonPhrasesVO  commonPhrasesVO = null;
          for(int i = 0; i < commonPhrasesList.size(); i++){
            commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(i);
            if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
              request.setAttribute("commonPhrasesList", commonPhrasesList);
              break;
            }
          }        
        }
        
      //
      List listOfCollegeInteractionDetailsWithMedia = (List)richProfileDataMap.get("oc_enquiry_info");
      if (!CollectionUtils.isEmpty(listOfCollegeInteractionDetailsWithMedia)) {
        Iterator collegeInteractionDet = listOfCollegeInteractionDetailsWithMedia.iterator();
        while (collegeInteractionDet.hasNext()) {
          CollegeCpeInteractionDetailsWithMediaVO collegeInteractionVO = (CollegeCpeInteractionDetailsWithMediaVO)collegeInteractionDet.next();
          request.setAttribute("ColgSubOrderItemId",collegeInteractionVO.getSubOrderItemId());
          break;
        }
        request.setAttribute("listOfCollegeInteractionDetailsWithMedia", listOfCollegeInteractionDetailsWithMedia);
      }
      //      
      List uniprofilecontent = (List)richProfileDataMap.get("oc_uni_profile_content");
      if (!CollectionUtils.isEmpty(uniprofilecontent)) {
        Iterator colgOverviewIter = uniprofilecontent.iterator();
        while (colgOverviewIter.hasNext()) {
          CollegeProfileVO collegeProfileVO = (CollegeProfileVO)colgOverviewIter.next();
          if("Overview".equalsIgnoreCase(collegeProfileVO.getSelectedSectionName())){
            request.setAttribute("profileId",collegeProfileVO.getProfileId());
            request.setAttribute("ColgSubOrderItemId",collegeProfileVO.getSubOrderItemId());
            break;
          }
        }
        request.setAttribute("collegeOverViewList", uniprofilecontent);
      }
      //
      ArrayList uniprofiletabs = (ArrayList)richProfileDataMap.get("oc_uni_profile_tabs");
      if (!CollectionUtils.isEmpty(uniprofiletabs)) {
        uniprofiletabs = new spring.helper.CommonHelper().customizeProfileTabsDetails(uniprofiletabs, richProfileVO.getNetworkId());
        request.setAttribute("showProfileTab", uniprofiletabs);
      }
      List locationInfoList = (List)richProfileDataMap.get("oc_admin_venue");
      if (!CollectionUtils.isEmpty(locationInfoList)) {
        request.setAttribute("locationInfoList", locationInfoList);
        for (Iterator iterator = locationInfoList.iterator(); iterator.hasNext(); ) {
          LocationsInfoVO bean = (LocationsInfoVO)iterator.next();
          request.setAttribute("latitudeStr", bean.getLatitude());
          request.setAttribute("longitudeStr", bean.getLongitude());
          request.setAttribute("cityGudieHeadLine", bean.getHeadLine());
          request.setAttribute("cityGudieLocation", bean.getLocation());
        }
      }
      //
      ArrayList pageMetaTagDetails = (ArrayList)richProfileDataMap.get("oc_page_meta_tags");
      if (!CollectionUtils.isEmpty(pageMetaTagDetails)) {
        String robotValue = "index,follow";
        ArrayList customizedPageMetaTagDetails = new spring.helper.CommonHelper().addRobotToMetaTagDetails(pageMetaTagDetails, robotValue, request);
        request.setAttribute("pageMetaTagDetails", customizedPageMetaTagDetails);
      }
      //
      List listOfUniOverallReviewsInfo = (List)richProfileDataMap.get("oc_overall_review");
      if (!CollectionUtils.isEmpty(listOfUniOverallReviewsInfo)) {
        request.setAttribute("listOfUniOverallReviewsInfo", listOfUniOverallReviewsInfo);
        //Set request attribute for schema tag on 16_May_2017, By Thiyagu G
        Iterator uniOverallReviewsInfo = listOfUniOverallReviewsInfo.iterator();
        while (uniOverallReviewsInfo.hasNext()) {
          UniOverallReviewsVO uniOverallReviewsVO = (UniOverallReviewsVO) uniOverallReviewsInfo.next();          
          request.setAttribute("collegeNameDisplay", uniOverallReviewsVO.getCollegeNameDisplay());
          request.setAttribute("reviewCount", uniOverallReviewsVO.getReviewCount());          
        }
      }
			//Added for WUSCA Rating in key stats pod by Prabha on 27_JAN_2015_REL
			List listOfWuscaRatingInfo = (List)richProfileDataMap.get("oc_WUSCA_ratings");
			if (!CollectionUtils.isEmpty(listOfWuscaRatingInfo)) {
				request.setAttribute("listOfWuscaRatingInfo", listOfWuscaRatingInfo);
			}
			//End of WUSCA Rating details
      List wuscaWinLogoList = (List)richProfileDataMap.get("oc_wusca_win_nom_logos");
      if (!CollectionUtils.isEmpty(wuscaWinLogoList)) {
        request.setAttribute("wuscaWinLogoList", wuscaWinLogoList);
      }
      //
      List richProfileMediaList = (List)richProfileDataMap.get("oc_rich_profile_media");
      if (!CollectionUtils.isEmpty(richProfileMediaList)) {
        Iterator richMediaIter = richProfileMediaList.iterator();
        while (richMediaIter.hasNext()) {
          RichProfileVO richProfileMediaVO = (RichProfileVO)richMediaIter.next();
          if (richProfileMediaVO.getMediaType() != null && "RICH_IMAGE".equalsIgnoreCase(richProfileMediaVO.getMediaType())) {
            request.setAttribute("richProfileImgPath", util.getImgPath(richProfileMediaVO.getMediaPath(), 0));
          }
          if (richProfileMediaVO.getMediaType() != null && "RICH_VIDEO".equalsIgnoreCase(richProfileMediaVO.getMediaType())) {
            if(!GenericValidator.isBlankOrNull(richProfileMediaVO.getMediaPath())){
              String[] mediaPath = richProfileMediaVO.getMediaPath().split("###");
              String mediaId = "";
              String videoTmbPath = "";
              if (mediaPath[0] != null && !"".equals(mediaPath[0])) {
                mediaId = mediaPath[0];
                videoTmbPath = util.getImgPath(new GlobalFunction().videoThumbnail(mediaId, "6"), 0);
                request.setAttribute("richProfileVidTmbPath", videoTmbPath);
                request.setAttribute("richProfileVidUrlPath", mediaPath[1]);
                request.setAttribute("richProfileVidoId", mediaId);
              }
            }
          }
        }
      }
      //
      List uniRankingList = (List)richProfileDataMap.get("oc_uni_raking");
      if (!CollectionUtils.isEmpty(uniRankingList)) {
        request.setAttribute("uniRankingList", uniRankingList);
      }
      //
      List socialNetworkList = (List)richProfileDataMap.get("oc_social_network_urls");
      if (!CollectionUtils.isEmpty(socialNetworkList)) {
        Iterator socialNetworkIter = socialNetworkList.iterator();
        while (socialNetworkIter.hasNext()) {
          RichProfileVO socialNetworkVO = (RichProfileVO)socialNetworkIter.next();
          if ("TWITTER".equalsIgnoreCase(socialNetworkVO.getSocialNetworkType())) {
            if (socialNetworkVO.getSoicalNetworkURL() != null && !"".equals(socialNetworkVO.getSoicalNetworkURL())) {
              String provTwitterName = "";
              if (socialNetworkVO.getSoicalNetworkURL().indexOf("https") != -1) {
                if (socialNetworkVO.getSoicalNetworkURL().indexOf("www.") != -1) {
                  provTwitterName = socialNetworkVO.getSoicalNetworkURL().replace("https://www.twitter.com/", "");
                } else {
                  provTwitterName = socialNetworkVO.getSoicalNetworkURL().replace("https://twitter.com/", "");
                }
              } else if (socialNetworkVO.getSoicalNetworkURL().indexOf("http") != -1) {
                if (socialNetworkVO.getSoicalNetworkURL().indexOf("www.") != -1) {
                  provTwitterName = socialNetworkVO.getSoicalNetworkURL().replace("http://www.twitter.com/", "");
                } else {
                  provTwitterName = socialNetworkVO.getSoicalNetworkURL().replace("http://twitter.com/", "");
                }
              }
              if (!"".equals(provTwitterName)) {
                request.setAttribute("providerTwitterName", provTwitterName);
              }
              request.setAttribute("providerTwitterUrl", socialNetworkVO.getSoicalNetworkURL());
            }
          } else if ("FACEBOOK".equalsIgnoreCase(socialNetworkVO.getSocialNetworkType())) {
            if (socialNetworkVO.getSoicalNetworkURL() != null && !"".equals(socialNetworkVO.getSoicalNetworkURL())) {
              request.setAttribute("providerFaceBookUrl", socialNetworkVO.getSoicalNetworkURL().replace("https://www.facebook.com/", ""));
            }
          } else if ("INSTAGRAM".equalsIgnoreCase(socialNetworkVO.getSocialNetworkType())) { //Added instagram url by Prabha on 31_May_16
            if (!GenericValidator.isBlankOrNull(socialNetworkVO.getSoicalNetworkURL())) {
              String instagramUrl = socialNetworkVO.getSoicalNetworkURL();
              if(instagramUrl.indexOf("https://www.instagram.com/") >= 0){
                instagramUrl = instagramUrl.replace("https://www.instagram.com/", "");
                request.setAttribute("instagramURL", instagramUrl.replace("/", ""));
              }
            }
          }
        }
      }
      //Added by Indumathi.S For Cost of Pint Nov-24-15 Rel
      List costOfPint = (List)richProfileDataMap.get("PC_COST_OF_LIVING");
      if (!CollectionUtils.isEmpty(costOfPint)) {
        request.setAttribute("costOfPint",costOfPint);
        Iterator costOfPintItr = costOfPint.iterator();
        while (costOfPintItr.hasNext()) {
          RichProfileVO costOfPintVO = (RichProfileVO)costOfPintItr.next();
          String whatuniCostPint = costOfPintVO.getWhatuniCostPint();
          if(!GenericValidator.isBlankOrNull(whatuniCostPint)){
            if(whatuniCostPint.contains("-")){
              String[] costPint = whatuniCostPint.split("-");
              if (costPint.length > 1) {
                request.setAttribute("whatuniCostPintOne",costPint[0].trim());
                request.setAttribute("whatuniCostPintTwo",costPint[1].trim());
              }
            } else {
              request.setAttribute("whatuniCostPint", costOfPintVO.getWhatuniCostPint());
            }
          }
        }
      }
      //End
      //
      String avgRating = (String)richProfileDataMap.get("o_avg_rating");
      if (!GenericValidator.isBlankOrNull(avgRating)) {
        request.setAttribute("averageReviewRating", avgRating);
        request.setAttribute("overallRating", avgRating);
      }
      //
      String pixelTracking = (String)richProfileDataMap.get("o_pixel_tracking_code");
      if (!GenericValidator.isBlankOrNull(pixelTracking)) {
        request.setAttribute("pixelTracking", pixelTracking); //Modified for wu_536 3-FEB-2015_REL by Karthi
      }
      String providerUCASCode = (String)richProfileDataMap.get("o_college_ucas_code");
      if (!GenericValidator.isBlankOrNull(providerUCASCode)) {
        request.setAttribute("providerUCASCode", providerUCASCode);
      }
      //
      String profileTabStr = (String)richProfileDataMap.get("o_profile_tab_list");
      if (!GenericValidator.isBlankOrNull(profileTabStr)) {
        request.setAttribute("profileTabStr", profileTabStr);
      }
      //
      String courseExistsFlag = (String)richProfileDataMap.get("o_course_exist_flag");
      if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
        request.setAttribute("courseExistsFlag", courseExistsFlag);
        String collegeName1 = common.replaceURL(common.getCollegeName(richProfileBean.getCollegeId(), request)).toLowerCase();        
        //Changed new all graduate courses url pettern, By Thiyagu G for 27_Jan_2016.
        String courseUrl = "";
        String qual="degree";
        if (!GenericValidator.isBlankOrNull(richProfileBean.getNetworkId()) && GlobalConstants.PG_NETWORK_ID.equals(richProfileBean.getNetworkId())) {
          qual="postgraduate";
        }
        //Added by Indumathi.S Jan-27-16 Rel
        if("N".equalsIgnoreCase(courseExistsFlag)){
          qual = "all";
        }
        courseUrl = new SeoUrls().constructCourseUrl(qual, richProfileBean.getProfileType(), collegeName1);  
        //End
        request.setAttribute("courseUrl", courseUrl);
      }
      //
      String studyAbroadFlag = (String)richProfileDataMap.get("o_study_abroad_flag");
      if (!GenericValidator.isBlankOrNull(studyAbroadFlag)) {
        request.setAttribute("studyAbroadFlag", studyAbroadFlag);
      }
      //
      String collegeBasketFlag = (String)richProfileDataMap.get("o_college_in_basket");
      if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
        request.setAttribute("COLLEGE_IN_BASKET", collegeBasketFlag);
      }
      //
      String addedToFinalChoice = (String)richProfileDataMap.get("p_final_choices_exist_flag"); //3_JUN_2014
      if (!GenericValidator.isBlankOrNull(addedToFinalChoice)) {
        request.setAttribute("addedToFinalChoice", addedToFinalChoice);
      }
      //
      String spdeptName = (String)richProfileDataMap.get("p_dept_name");
      if (!GenericValidator.isBlankOrNull(spdeptName)) {
        request.setAttribute("spdeptName", spdeptName); //30_SEP_2015 added by Amir for displaying department name
      }
      //Hide location flag added by Prabha on 31_May_2016
      String hideLocation = (String)richProfileDataMap.get("p_hide_locaion_flag");
      if (!GenericValidator.isBlankOrNull(hideLocation)) {
        request.setAttribute("hideLocationFlag", hideLocation);
      }
      //
      //Added breadCrumb Indumathi.S Feb-16-2016
      String  breadCrumb = new SeoUtilities().getBreadCrumb("UNI_PROFILE_PAGE", "", "", "", "", richProfileBean.getCollegeId());
      if (breadCrumb != null && breadCrumb.trim().length() > 0) {
       request.setAttribute("breadCrumb", breadCrumb);
      }
      //Added for CMMT institution profile page            
      String clearingCourseExistFlag = (String)richProfileDataMap.get("P_CL_COURSE_EXIST_FLAG");
      //  
      //Added for the top clearing switch pod For June_23_rel by sangeeth.s
      String showSwitchFlag = (String)richProfileDataMap.get("P_CLEARING_SWITCH_FLAG");
      
   // Added for dynamic meta data details from Back office, 20201020 rel by Sri Sankari
	  ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) richProfileDataMap.get("PC_META_DETAILS");
	  new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);  
      request.setAttribute("SHOW_SWITCH_FLAG", showSwitchFlag);
      //
      request.setAttribute("CLEARING_COURSE_EXIST_FLAG", clearingCourseExistFlag);
     
      request.setAttribute("COLLEGEID_IN_INTERACTION", richProfileBean.getCollegeId());
      request.setAttribute(SpringConstants.COLLEGE_ID, richProfileBean.getCollegeId());
      request.setAttribute("cid", richProfileBean.getCollegeId());
      request.setAttribute("interactionCollegeId", richProfileBean.getCollegeId());
      request.setAttribute("interactionCollegeName", richProfileBean.getCollegeName());
      request.setAttribute("interactionCollegeNameDisplay", richProfileBean.getCollgeDispName());
      /* Added college display name for smart pixel by Prabha on 20_02_2018 */
      if(!GenericValidator.isBlankOrNull(richProfileBean.getCollegeName())){
        request.setAttribute("cDimCollegeDisName", ("\""+richProfileBean.getCollegeName()+"\""));
      }
      /* End od smart pixel code */
      request.setAttribute("cDimCollegeId", GenericValidator.isBlankOrNull(richProfileBean.getCollegeId())? "": richProfileBean.getCollegeId().trim());
      if(!GenericValidator.isBlankOrNull(richProfileBean.getCollegeId())){
        request.setAttribute("cDimCollegeIds", ("'" + richProfileBean.getCollegeId().trim() + "'")); //Added smart pixel changes by Prabha on 20.02.2018
      }
      request.setAttribute("showBanner", "no");
      request.setAttribute("showResponsiveTimeLine", "true");
      if (GlobalConstants.UG_NETWORK_ID.equals(cpeQualificationNetworkId)) {
        new GlobalFunction().getStudyLevelDesc("M", request);
      } else {
        new GlobalFunction().getStudyLevelDesc("L", request);
      }
    }
    return null;
  }
  
}
