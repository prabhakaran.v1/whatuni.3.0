package com.wuni.uni;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoPageNamesAndFlags;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mobile.valueobject.ProfileVO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @UniLandingStatsLoggingAjax
  * @author Pryaa Parthasarathy
  * @version 1.0
  * @since 3-Jun-2014
  * @purpose This program is used to log the Stats and get stats key
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 23-Oct-2018    Sabapathi S.              1.3      Added screen width in stats logging          					wu_582
  * 09_MAR_2020    Sangeeth.S		  		 1.4	  Added lat and long for stats logging							wu_310320
  * 17_SEP_2020    Sri Sankari               1.5      Added stats log for tariff logging(UCAS point)                wu_20200917
  * 10_NOV_2020    Sujitha V                 1.6      Added session for YOE to log in d_session_log.	            wu_20201110
  */
@Controller
public class UniLandingStatsLoggingAjax{

  @RequestMapping(value = "/unilandingStatsAjax", method = RequestMethod.POST)
  public ModelAndView unilandingStatsAjax(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    CommonFunction common = new CommonFunction();
    SessionData sessionData = new SessionData();
    String x = sessionData.getData(request, "x");
    String y = sessionData.getData(request, "y");
    String basketId = (y != null && (y.equals("0") || y.trim().length() == 0)) ? common.checkCookieStatus(request) : "";
    basketId = (y != null && (y.equals("0") || y.trim().length() == 0)) ? basketId :"";
    y = (basketId != null && basketId.trim().length() > 0) ? "" : y;
    if ((y != null && !y.equals("0") && y.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      y = "0";
      basketId = "";
    }
    String clientIp = request.getHeader("CLIENTIP");    
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    } 
    //
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    //
    String tabName = request.getParameter("tabName");
    HashMap sectionmap = new HashMap();
    sectionmap.put("overview", GlobalConstants.OVERVIEW);
    sectionmap.put("student-accommodation", GlobalConstants.ACCOMADATION);
    sectionmap.put("entertainment", GlobalConstants.ENTERTAINMENT);
    sectionmap.put("facilities", GlobalConstants.COURSES_ACADEMICS_FACILITIES);
    sectionmap.put("student-welfare", GlobalConstants.WELFARE);
    sectionmap.put("funding", GlobalConstants.FEES_FUNDING);
    sectionmap.put("job-opportunities", GlobalConstants.JOB_PROSPECTS);
    sectionmap.put("contact", GlobalConstants.CONTACT);
    sectionmap.put("review", GlobalConstants.REVIEW);
    sectionmap.put("campus", GlobalConstants.CAMPUS);
    sectionmap.put("gallery", GlobalConstants.GALLERY);
    //
    sectionmap.put("covid-19", GlobalConstants.COVID_19);
    sectionmap.put("clearing-usps", GlobalConstants.CLEARING_USPS);
    sectionmap.put("our-clearing-guide", GlobalConstants.OUR_CLEARING_GUIDE);
    sectionmap.put("what-happens-next", GlobalConstants.WHAT_HAPPENS_NEXT);
    sectionmap.put("contact-details", GlobalConstants.CONTACT_DETAILS);    
    //
    HashMap requestdescnmap = new HashMap(); //used in db stats-logging
    requestdescnmap.put("overview", "overview");
    requestdescnmap.put("accommodation-and-location", "accommodation");
    requestdescnmap.put("student-union-and-entertainment", "entertainment");
    requestdescnmap.put("courses-academics-and-facilities", "c_facilities");
    requestdescnmap.put("welfare", "welfare");
    requestdescnmap.put("fees-and-funding", "fees_funding");
    requestdescnmap.put("job-prospects", "job_pros");
    requestdescnmap.put("contact-details", "contact");
    //
     String  tabsNamee =  !GenericValidator.isBlankOrNull(request.getParameter("tabName")) ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(request.getParameter("tabName")))) :"";   
     String requestDescription = (String)requestdescnmap.get(tabsNamee.toLowerCase());
      if (requestDescription == null || requestDescription.trim().length() == 0) {
        requestDescription = "overview"; //urlArray[4]; //we are sending section name from URL, with no formating   
      }        
    String collegeSection =  !GenericValidator.isBlankOrNull(tabsNamee) ? String.valueOf(sectionmap.get(tabsNamee.toLowerCase())) : "WUNI OVERVIEW"; 
    // wu582_20181023 - Sabapathi: College section needs to empty for Content hub stats logging on page load
    if(GenericValidator.isBlankOrNull(request.getParameter("tabName")) && "CH_INST_PROFILE".equals(request.getParameter("richProfileType"))) {
      collegeSection = "";
    }
    // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
    // wu_300320 - Sangeeth.S: Added lat and long for stats logging
    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
    String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
    String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
    String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
    ProfileVO profileVO = new ProfileVO();
    profileVO.setX(x);
    profileVO.setY(y);
    profileVO.setProfileId(request.getParameter("profileId"));
    profileVO.setMyhcProfileId(request.getParameter("myhcProfileId"));
    profileVO.setCollegeId(request.getParameter("collegeId"));
    profileVO.setNetwordId(request.getParameter("cpeQualificationNetworkId")); 
    profileVO.setRequestDesc(requestDescription);
    profileVO.setClientIp(clientIp);
    profileVO.setUserAgent(request.getHeader("user-agent"));
    profileVO.setCollegeSection(collegeSection); 
    profileVO.setRequestURL(request.getHeader("referer"));
    profileVO.setReferralURL(request.getHeader("referer"));
    profileVO.setJsLog("Y");
    profileVO.setJourneyType(request.getParameter("journeyType"));//24_JUN_2014
    profileVO.setMetaPageFlag(SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG);
    profileVO.setMetaPageName(SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME);
    profileVO.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
    profileVO.setMobileFlag(request.getParameter("mobileFlag"));//15_JUL_2014
    profileVO.setScreenWidth(screenWidth); // screen width
    profileVO.setLatitude(latitude); //latitude
    profileVO.setLongitude(longitude); //longitude
    profileVO.setUserUcasScore(userUcasScore); //P_UCAS_TARIFF
    if(request.getParameter("richProfileType")!=null && !"".equals(request.getParameter("richProfileType"))){ //Rich profile flag Added by Amir for 13_JAN_15 release
      profileVO.setRichProfileType(request.getParameter("richProfileType"));
    } 
    profileVO.setYearOfEntry(yearOfEntryForDSession);//P_YEAR_OF_ENTRY
    Map resultMap = commonBusiness.logProfileTabStats(profileVO);
    String statsLogId = (String)resultMap.get("p_log_status_id");
    String typeKey = (String)resultMap.get("p_type_key");
    request.setAttribute("d_log_type_key", typeKey);    
    if (statsLogId != null && statsLogId.trim().length() > 0) {
      request.getSession().setAttribute("stats_log_id", statsLogId);
    }
    return null;    
  }
}
