package com.wuni.studentsawards;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : StudentChoiceAwardsAction.java
 * Description   : This Action class is used to display TOP 10 Student Awards against all the categories
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Amirtharaj.A            1.0              20.11.2012      First draft   		
 * Indumathi.S             1.1              08-03-16        Added List of award year
 * Indumathi.S             1.2              29-03-16        URL restructuring for current year[2016] student choice awards
 * Prabhakaran V.          1.3              23-03-16        Added two new categories and added 2017 year changes
 * Sangeeth.S             1.4               26.02.19        Added 2019 year changes
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
@Controller
public class StudentChoiceAwardsController {

  @RequestMapping(value = { "/student-awards-winners/*" , "/ajax-studchoice-results" } , method = {RequestMethod.POST , RequestMethod.GET })
  public ModelAndView getStudentAwardsDetails(@ModelAttribute("studentAwardsBean") StudentAwardsBean  studentAwardsBean , ModelMap model, HttpServletRequest request, HttpServletResponse response , HttpSession session) throws Exception {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    
    CommonFunction common = new CommonFunction();
    String cpeQualificationNetworkId = (String)session.getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    String uniOfYearId = GlobalConstants.UNIVERSITY_OF_THE_YEAR_ID;//Added by Indumathi.S Mar_29_2016
    session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
    String year = "";
    String currYear = "";
    int lastYear = 0;
    String forwardString = "studResultsInfo";
    String studChoiceCateId = "";
    String studChoiceUniId = "";
    String pageNo = "1";
    String dbcollegeName = "";
    String dbcollegeNameDisplay = "";
    boolean loadAjaxRes = false;
 final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    if (new CommonFunction().checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    new GlobalFunction().removeSessionData(session, request, response);
    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length()))); //Added getSchemeName by Indumathi Mar-29-16
    //    
    currYear = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
    String urlArray[] = URL_STRING.split("/");
   

    if (urlArray.length == 3) {
      if (URL_STRING.contains("student-awards-winners")) {
        if("2020".equals(currYear)) {
           if(!GenericValidator.isBlankOrNull(urlArray[2]) && (GlobalConstants.CITY_LIFE.equalsIgnoreCase(urlArray[2]) || GlobalConstants.CLUBS_AND_SOCIETIES.equalsIgnoreCase(urlArray[2]))) {
             String redirectURL = "";
             redirectURL = StringUtils.replaceEach(URL_STRING, new String[]{GlobalConstants.CITY_LIFE,GlobalConstants.CLUBS_AND_SOCIETIES}, new String[]{GlobalConstants.LOCAL_LIFE+"/",GlobalConstants.SOCIETIES_AND_SPORTS+"/"});
             redirectURL = redirectURL.toLowerCase();
             response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
             response.setHeader("Location", redirectURL);
             response.setHeader("Connection", "close");
             return null;
           }
        }
        year = getAwardYear(urlArray[2].replace(".html", "") , currYear);//Added by Indumathi.S Mar_29_2016
      } else if (URL_STRING.contains("uk-best-university-ranking")) {
        year = getAwardYear(urlArray[2], currYear);//Added by Indumathi.S Mar_29_2016
      }
    } 
    if (!"inValidYear".equalsIgnoreCase(year)) { //Added by Indumathi.S Mar_29_2016
      /**Load TOP 10 Student Awards categories based on year**/
      if (!GenericValidator.isBlankOrNull(currYear) && currYear.equals(year) && Integer.parseInt(year) > 2015) {
        studChoiceCateId = getStudChoiceCateId(urlArray[2], request, year);//Added by Indumathi.S Mar_29_2016
      } else if (!GenericValidator.isBlankOrNull(request.getParameter("studCateId"))) {
        studChoiceCateId = request.getParameter("studCateId");
        loadAjaxRes = true;
      }
      if (!GenericValidator.isBlankOrNull(request.getParameter("loadYear"))) {
        year = request.getParameter("loadYear");
      }
      if (!GenericValidator.isBlankOrNull(request.getParameter("loadUnivId"))) {
        studChoiceUniId = request.getParameter("loadUnivId");
        request.setAttribute("removeFilter", "true");
        loadAjaxRes = true;
      }else if (!GenericValidator.isBlankOrNull(studentAwardsBean.getCollegeId())) { //Get the uni details for category search, By Thiyagu G for 21_March_2017.
        studChoiceUniId = studentAwardsBean.getCollegeId();
      }
      lastYear = (year != null) ? Integer.parseInt(year) - 1 : 0;
      Map studentAwardsDataMap = null;
      studentAwardsDataMap = commonBusiness.getAwardsYearList(studentAwardsBean);
      //S.Indumathi Mar-08-16 Award List year 
      if (studentAwardsDataMap != null) {
        ArrayList awardYearList = (ArrayList)studentAwardsDataMap.get("pc_award_year_list");
        if (awardYearList != null && awardYearList.size() > 0) {
          request.setAttribute("awardYearList", awardYearList);
        }
      }
      if (!GenericValidator.isBlankOrNull(year)) {
        if (GenericValidator.isBlankOrNull(studChoiceCateId)) {
          studChoiceCateId = uniOfYearId;
        }
        if (!GenericValidator.isBlankOrNull(request.getParameter("pageNo"))) {
          pageNo = request.getParameter("pageNo");
        }
        String uniId = "";
        String uniNameDisplay = "";
        uniId = studentAwardsBean.getCollegeId();
        uniNameDisplay = studentAwardsBean.getCollegeDisplayName();
        studentAwardsBean.setNetworkId(cpeQualificationNetworkId);
        studentAwardsBean.setPageNo(pageNo);
        studentAwardsBean.setCurYear(year);
        studentAwardsBean.setLastYear(String.valueOf(lastYear));
        studentAwardsBean.setQuestionId(studChoiceCateId);
        studentAwardsBean.setCollegeId(studChoiceUniId);        
        studentAwardsDataMap = commonBusiness.getWuscaRankListResult(studentAwardsBean);
        if (studentAwardsDataMap != null) {
          List studChoiceAwardsList = (List)studentAwardsDataMap.get("pc_uni_rank_list");
          if (studChoiceAwardsList != null && studChoiceAwardsList.size() > 0) {
            request.setAttribute("studChoiceAwardsList", studChoiceAwardsList);
          } else {
            CollegeNamesVO uniNames = null;
            if (!GenericValidator.isBlankOrNull(studChoiceUniId)) { //Changed the condition to studChoiceUniId alone for both parameter and form bean, By Thiyagu G for 21_March_2017.
              uniNames = new CollegeUtilities().getCollegeNames(studChoiceUniId, request);
              if (uniNames != null) {
                dbcollegeName = uniNames.getCollegeName();
                dbcollegeNameDisplay = uniNames.getCollegeNameDisplay();
              }
              request.setAttribute("stChUniHomeURL", new SeoUrls().construnctUniHomeURL(studChoiceUniId, dbcollegeName));
              request.setAttribute("stChUniHome", dbcollegeNameDisplay);
              request.setAttribute("stChReviewHome", new SeoUrls().constructReviewPageSeoUrl(dbcollegeName, studChoiceUniId));
              request.setAttribute("backToResults", studChoiceCateId);
              
            }
            String noResultCatName = getStudChoiceCateName(studChoiceCateId, currYear);
            request.setAttribute("noResultCatName", noResultCatName);
            
            if (studChoiceCateId != null && "10".equals(studChoiceCateId)) {
              request.setAttribute("intlSchErr", "true");
            }
          }
          List studChoiceCateList = (List)studentAwardsDataMap.get("pc_category_list");
          if (studChoiceCateList != null && studChoiceCateList.size() > 0) {
            request.setAttribute("studChoiceCateList", studChoiceCateList);
          }
          String sponserBy = (String)studentAwardsDataMap.get("p_sponser_by");
          if (!GenericValidator.isBlankOrNull(sponserBy)) {
            request.setAttribute("sponserBy", sponserBy);
          }
          String sponserURL = (String)studentAwardsDataMap.get("p_sponser_url");
          if (!GenericValidator.isBlankOrNull(sponserURL)) {
            request.setAttribute("sponserURL", sponserURL);
          }
          String studChoiceResCount = (String)studentAwardsDataMap.get("p_uni_cnt");
          if (!GenericValidator.isBlankOrNull(studChoiceResCount)) {
            request.setAttribute("studChoiceResCount", studChoiceResCount);
          }
          String studChoiceBreadCrumb = (String)studentAwardsDataMap.get("p_bread_crumb");
          if (!GenericValidator.isBlankOrNull(studChoiceBreadCrumb)) {
            request.setAttribute("breadCrumb", studChoiceBreadCrumb);
          }
          request.setAttribute("pageno", pageNo);
          request.setAttribute("uniId", uniId);
          request.setAttribute("uniNameDisplay", uniNameDisplay);
        }
        if (loadAjaxRes) {
          forwardString = "loadStudChoiceAjaxRes";
          request.setAttribute("ajaxResAppend", "true");
          if (!GenericValidator.isBlankOrNull(request.getParameter("ajaxSrch"))) {
            request.setAttribute("stdchAjaxSrch", "true");
          }
        } else {
          forwardString = "studChoiceResult";
        }
        request.setAttribute("previousYear", getPreviousYear(year));
        request.setAttribute("loadYear", year);
        request.setAttribute("showResponsiveTimeLine", "true");
        String GAAwardsPageName = "award-results-" + year + ".jsp";
        request.setAttribute("GAAwardsPageName", GAAwardsPageName);
        request.setAttribute("curActiveMenu", "reviews");
        /** Default Student choice home page **/
        if ("studResultsInfo".equals(forwardString)) {
          getStudentAwardsMediaPath();
          String mediaPath = getStudentAwardsMediaPath();
          if (!GenericValidator.isBlankOrNull(mediaPath)) {
            request.setAttribute("mediaPath", mediaPath);
          }
        }
      }
      request.setAttribute("currentAwardYear", currYear);
    } else {           
      request.setAttribute("resultPageFlag",year);
      forwardString = "newStudentAwards";
      String mediaPath = getStudentAwardsMediaPath();
      if (!GenericValidator.isBlankOrNull(mediaPath)) {
        request.setAttribute("mediaPath", mediaPath);
      }
    }
    return new  ModelAndView(forwardString , "StudentAwardsBean" , studentAwardsBean  );
  }

  /**
   * This function is used to load the Top course providers for all the review category.
   * @return Top 10 courseprovider as Collection object.
   */
  private String getStudentAwardsMediaPath() {
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    Vector vector = new Vector();
    String mediaPath = "";
    try {
      mediaPath = dataModel.getString("WU_REVIEWS_PKG.GET_STUDENT_AWARDS_PATH_FN ", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return mediaPath;
  }

  /**
   * @getpreviousYear to get the previous year
   * @param year
   * @return String
   * Change Log : Added previous year by Prabha on 23_Mar_2018
   */
  private String getPreviousYear(String year) {
    String previousYear = "";
    if(year.equals("2020")) {
    	previousYear = "2019";
    }else if(year.equals("2019")){//Added 2019 year changes
      previousYear = "2018";
    }else if(year.equals("2018")){
	  previousYear = "2017";
    }else if (year.equals("2017")) {
      previousYear = "2016";
    } else if (year.equals("2016")) {
      previousYear = "2015";
    } else if (year.equals("2015")) {
      previousYear = "2014";
    } else if (year.equals("2014")) {
      previousYear = "2012";
    }
    return previousYear;
  }

  /**
   * @getAwardYear to get the award year based on URL
   * @param urlText
   * @param currYear
   * @return String
   * Change Log : Added condition for two new categories by Prabha on 23_Mar_2018
   */
  private String getAwardYear(String urlText, String currYear) {
    String year = "inValidYear";
    if (GlobalConstants.UNIVERSITY_OF_THE_YEAR.equalsIgnoreCase(urlText) || GlobalConstants.AWD_JOB_PROSPECTS.equalsIgnoreCase(urlText) || GlobalConstants.COURSE_AND_LECTURERS.equalsIgnoreCase(urlText) || GlobalConstants.STUDENT_UNION.equalsIgnoreCase(urlText) || GlobalConstants.ACCOMMODATION.equalsIgnoreCase(urlText) || GlobalConstants.UNI_FACILITIES.equalsIgnoreCase(urlText) || ((!("2020".equals(currYear))) && GlobalConstants.CITY_LIFE.equalsIgnoreCase(urlText) || GlobalConstants.CLUBS_AND_SOCIETIES.equalsIgnoreCase(urlText)) || GlobalConstants.STUDENT_SUPPORT.equalsIgnoreCase(urlText) || GlobalConstants.GIVING_BACK.equalsIgnoreCase(urlText) || GlobalConstants.POSTGRADUATE.equalsIgnoreCase(urlText) || GlobalConstants.INTERNATIONAL.equalsIgnoreCase(urlText) || GlobalConstants.INDEPENDENT_HIGHER_EDUCATION.equalsIgnoreCase(urlText) || GlobalConstants.FURTHER_EDUCATION_COLLEGES.equalsIgnoreCase(urlText)|| ("2020".equals(currYear) && GlobalConstants.SOCIETIES_AND_SPORTS.equalsIgnoreCase(urlText)|| GlobalConstants.LOCAL_LIFE.equalsIgnoreCase(urlText))) {
      year = currYear;
    } else if (isInteger(urlText) && urlText.length() ==4 && GlobalConstants.AWARDS_YEAR_LIST.contains(urlText)){
      year = urlText;
    }
    return year;
  }

  /**
   * @getStudChoiceCateId to get student choice category id based on category name
   * @param category
   * @param request
   * @return String
   * Change Log : Added condition for two new categories by Prabha on 23_Mar_2018
   */
  private String getStudChoiceCateId(String category, HttpServletRequest request, String year) {
    String catId = GlobalConstants.UNIVERSITY_OF_THE_YEAR_ID;
    String catName = "University of the year";
    if (GlobalConstants.UNIVERSITY_OF_THE_YEAR.equalsIgnoreCase(category)) {
      catId = GlobalConstants.UNIVERSITY_OF_THE_YEAR_ID;
      catName = "University of the year";
    } else if (GlobalConstants.AWD_JOB_PROSPECTS.equalsIgnoreCase(category)) {
      catId = GlobalConstants.AWD_JOB_PROSPECTS_ID;
      catName = "Job Prospects";
    } else if (GlobalConstants.COURSE_AND_LECTURERS.equalsIgnoreCase(category)) {
      catId = GlobalConstants.COURSE_AND_LECTURERS_ID;
      catName = "Course & Lecturers";
    } else if (GlobalConstants.STUDENT_UNION.equalsIgnoreCase(category)) {
      catId = GlobalConstants.STUDENT_UNION_ID;
      catName = "Students' Union";
    } else if (GlobalConstants.ACCOMMODATION.equalsIgnoreCase(category)) {
      catId = GlobalConstants.ACCOMMODATION_ID;
      catName = "Accommodation";
    } else if (GlobalConstants.UNI_FACILITIES.equalsIgnoreCase(category)) {
      catId = GlobalConstants.UNI_FACILITIES_ID;
      catName = "Uni Facilities";
    } else if (GlobalConstants.CITY_LIFE.equalsIgnoreCase(category)|| GlobalConstants.LOCAL_LIFE.equalsIgnoreCase(category)) {
      catId = GlobalConstants.CITY_LIFE_ID;
      catName = "2020".equals(year) ? "Local Life" : "City Life";
    } else if (GlobalConstants.CLUBS_AND_SOCIETIES.equalsIgnoreCase(category) || GlobalConstants.SOCIETIES_AND_SPORTS.equalsIgnoreCase(category)) {
      catId = GlobalConstants.CLUBS_AND_SOCIETIES_ID;
      catName = "2020".equals(year) ? "Societies and Sports" : "Clubs & Societies";
    } else if (GlobalConstants.STUDENT_SUPPORT.equalsIgnoreCase(category)) {
      catId = GlobalConstants.STUDENT_SUPPORT_ID;
      catName = "Student Support";
    } else if (GlobalConstants.GIVING_BACK.equalsIgnoreCase(category)) {
      catId = GlobalConstants.GIVING_BACK_ID;
      catName = "Giving Back";
    } else if (GlobalConstants.POSTGRADUATE.equalsIgnoreCase(category)) {
      catId = GlobalConstants.POSTGRADUATE_ID;
      catName = "Postgraduate";
    } else if (GlobalConstants.INTERNATIONAL.equalsIgnoreCase(category)) {
      catId = GlobalConstants.INTERNATIONAL_ID;
      catName = "International";
    }else if(GlobalConstants.INDEPENDENT_HIGHER_EDUCATION.equalsIgnoreCase(category)){
      catId = GlobalConstants.INDEPENDENT_HIGHER_EDUCATION_ID;
      catName = "Independent Higher Education";
    }else if(GlobalConstants.FURTHER_EDUCATION_COLLEGES.equalsIgnoreCase(category)){
      catId = GlobalConstants.FURTHER_EDUCATION_COLLEGES_ID;
      catName = "Further Education College";
    }
    request.setAttribute("categorName", catName);
    return catId;
  }
  
  private String getStudChoiceCateName(String categoryId, String currYear) {
    String catId = GlobalConstants.UNIVERSITY_OF_THE_YEAR_ID;
    String catName = "University of the year";
    if (GlobalConstants.UNIVERSITY_OF_THE_YEAR_ID.equalsIgnoreCase(categoryId)) {
      catName = "University of the year";
    } else if (GlobalConstants.AWD_JOB_PROSPECTS_ID.equalsIgnoreCase(categoryId)) {
      catName = "Job Prospects";
    } else if (GlobalConstants.COURSE_AND_LECTURERS_ID.equalsIgnoreCase(categoryId)) {
      catName = "Course & Lecturers";
    } else if (GlobalConstants.STUDENT_UNION_ID.equalsIgnoreCase(categoryId)) {
      catName = "Student Union";
    } else if (GlobalConstants.ACCOMMODATION_ID.equalsIgnoreCase(categoryId)) {
      catName = "Accommodation";
    } else if (GlobalConstants.UNI_FACILITIES_ID.equalsIgnoreCase(categoryId)) {
      catName = "Uni Facilities";
    } else if (GlobalConstants.CITY_LIFE_ID.equalsIgnoreCase(categoryId)) {
      //catName = "Local Life";
      catName = "2020".equals(currYear) ? "Local Life" : "City Life";
    } else if (GlobalConstants.CLUBS_AND_SOCIETIES_ID.equalsIgnoreCase(categoryId)) {
      catName = "2020".equals(currYear) ? "Societies and Sports" : "Clubs & Societies";
    } else if (GlobalConstants.STUDENT_SUPPORT_ID.equalsIgnoreCase(categoryId)) {
      catName = "Student Support";
    } else if (GlobalConstants.GIVING_BACK_ID.equalsIgnoreCase(categoryId)) {
      catName = "Giving Back";
    } else if (GlobalConstants.POSTGRADUATE_ID.equalsIgnoreCase(categoryId)) {
      catName = "Postgraduate";
    } else if (GlobalConstants.INTERNATIONAL_ID.equalsIgnoreCase(categoryId)) {
       catName = "International";
    }else if(GlobalConstants.INDEPENDENT_HIGHER_EDUCATION_ID.equalsIgnoreCase(categoryId)){
       catName = "Independent Higher Education";
    }else if(GlobalConstants.FURTHER_EDUCATION_COLLEGES_ID.equalsIgnoreCase(categoryId)){
       catName = "Further Education College";
    }
    
    return catName;
  }
  

  /**
   * @isInteger to check the given string is integer or not
   * @param input
   * @return boolean
   */
  private boolean isInteger(String input) {
    try {
      Integer.parseInt(input);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

}
