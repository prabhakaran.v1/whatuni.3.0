package com.wuni.token;


import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class TokenFormController{
	
  private static final String TOKEN_KEY = "_synchronizerToken";
		  //"_synchronizerToken";
  
  public void saveToken(HttpServletRequest request){
	  
	HttpSession session = request.getSession();
	session.setAttribute(TokenFormController.getTokenKey(),TokenFormController.generateToken(request));
  }
  
  public boolean isTokenValid(HttpServletRequest request) {
    HttpSession session = request.getSession();
    String sessionToken = (String)session.getAttribute(getTokenKey());
    String requestToken = request.getParameter(getTokenKey());
    if ((sessionToken != null && requestToken != null) && sessionToken.equals(requestToken)) {
      return true;
    }
    return false;
  }
  
  public void resetToken(HttpServletRequest request){
	HttpSession session = request.getSession();
    session.removeAttribute(getTokenKey());
  }
  
  public static String generateToken(HttpServletRequest request) {
    long seed = System.currentTimeMillis(); 
    Random r = new Random();
    r.setSeed(seed);
    return Long.toString(seed) + Long.toString(Math.abs(r.nextLong()));
  }

  public static String getTokenKey() {
    return TOKEN_KEY;
  }
}
  
