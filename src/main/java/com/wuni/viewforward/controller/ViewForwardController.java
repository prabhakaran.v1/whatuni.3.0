package com.wuni.viewforward.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.registration.bean.UserLoginBean;


@Controller
public class ViewForwardController {

	
	@RequestMapping(value = {"/forgotPassword"}, method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView forgotPassword(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
	  return new ModelAndView("view-forward-forgot-password", "userLoginBean", new UserLoginBean());
	}
	
	@RequestMapping(value = {"/redirect"}, method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView redirect(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
	  return new ModelAndView("redirectPage");
	}
	
	@RequestMapping(value = {"/prospectuslink"}, method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView prospectuslink(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
	  return new ModelAndView("view-forward-prospectus-link");
	}
	
	@RequestMapping(value = {"/blockedUser"}, method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView blockedUser(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
	  return new ModelAndView("view-forward-blockedUser");
	}
	
	@RequestMapping(value = {"/redirect-prospectus-custom-url"}, method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView redirectProspectusCustomUrl(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
	  return new ModelAndView("view-forward-redirect-prospectus-custom-url");
	}
	
}
