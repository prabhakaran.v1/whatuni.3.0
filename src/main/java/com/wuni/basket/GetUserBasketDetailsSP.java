package com.wuni.basket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.basket.form.BasketBean;

public class GetUserBasketDetailsSP extends StoredProcedure
{

  public GetUserBasketDetailsSP(DataSource datasource)
  {
    setDataSource(datasource);
    setFunction(true);
    setSql("HOT_WUNI.WU_BASKET_PKG.GET_BASKET_DETAILS_FN");
    declareParameter(new SqlOutParameter("lc_get_basket_details", OracleTypes.CURSOR, new UserBasketDDInfoRowMapperImpl() ));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
  }
  
  public Map execute(String userId)
  {
    Map outMap = new HashMap();
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", userId);
    outMap = execute(inMap);
    return outMap;
  }
  
  private class UserBasketDDInfoRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
      BasketBean bean = new BasketBean();
      bean.setBasketId(resultset.getString("basket_id"));
      bean.setBasketName(resultset.getString("basket_name"));
      return bean;
    }
  }  
}
