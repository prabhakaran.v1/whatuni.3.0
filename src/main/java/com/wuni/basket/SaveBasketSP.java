package com.wuni.basket;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.basket.form.BasketBean;
import WUI.utilities.GlobalConstants;

/**
  * @Version :  .0
  * @May 17 2015  
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to save the basket
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 17-May-2015  Priyaa Parthasarathy         1.0
  */

public class SaveBasketSP extends StoredProcedure
{

  public SaveBasketSP(DataSource datssource)
  {
    setDataSource(datssource);
    setSql(GlobalConstants.SAVE_BASKET_FN);
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_session_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_comparison_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("o_flag", OracleTypes.VARCHAR));
  }
  
  public Map execute(BasketBean bean)
  {
    Map outMap = new HashMap();
    try{
    Map inMap = new HashMap();
    inMap.put("p_user_id", bean.getUserId());
    inMap.put("p_session_basket_id", bean.getCookieBasketId());
    inMap.put("p_comparison_name", bean.getBasketName());

    outMap = execute(inMap);
    }catch(Exception ex){
     ex.printStackTrace();
    }
    return outMap;
  }
}
