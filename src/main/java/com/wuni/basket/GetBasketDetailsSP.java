package com.wuni.basket;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.basket.form.BasketBean;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;

public class GetBasketDetailsSP extends StoredProcedure
{

  public GetBasketDetailsSP(DataSource datasource)
  {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.VIEW_COMPARE_TABLE_FN);
    declareParameter(new SqlOutParameter("lc_get_basket_pod", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    declareParameter(new SqlParameter("P_BASKET_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_ASSOC_TYPE_CODE", OracleTypes.VARCHAR));//Added for 19_MAY_2015_REL    
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));//Added for 19_MAY_2015_REL    
    declareParameter(new SqlOutParameter("p_basket_count", OracleTypes.VARCHAR));
    compile();
  }
  
  public Map execute(BasketBean bean)
  {
    Map outMap = new HashMap();
    try{
    Map inMap = new HashMap();
    inMap.put("P_BASKET_ID", bean.getBasketId());
    inMap.put("P_ASSOC_TYPE_CODE", bean.getAssociationType());//Added for 19_MAY_2015_REL  
    inMap.put("P_USER_ID", bean.getUserId());
    outMap = execute(inMap);
    }catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;    
  }

}
