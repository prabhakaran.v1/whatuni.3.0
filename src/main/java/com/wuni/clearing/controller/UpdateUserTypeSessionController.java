package com.wuni.clearing.controller;

import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : UpdateUserTypeSessionAction.java
 * Description   : This class used to setting session attribute for clearing splash screen.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author                    Ver               Modified On               Modification Details                 Change
 * *************************************************************************************************************************************
 * Yogeswari.S               1.0               09.06.2015                First draft
 *Sangeeth.S                1.1               20.05.2019                Added session value for navigation 
 *                                                                        journey of UG and PG tab in clearing 
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class UpdateUserTypeSessionController{
  @RequestMapping(value="/clearing/update-user-type-session", method=RequestMethod.POST)	
  public String clearingUpdateUserTypeSession(HttpServletRequest request, HttpServletResponse response) {
    String userType = request.getParameter("type");
    boolean isClearing = new GlobalFunction().isClearing(userType);
    HttpSession session = request.getSession();
    CommonFunction common = new CommonFunction();
    //Added by Sangeeth.S for navigation of UG and PG tab in clearing user journey
    String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");   
    if("ON".equalsIgnoreCase(clearingonoff)){
      String fromPage = request.getParameter("fromPage");    
      if("profile_page".equalsIgnoreCase(fromPage)){
        session.setAttribute("USER_TYPE_FROM_IP_PAGE", "CLEARING");
      }
    }
    //
    if (userType != null && userType.trim().length() > 0) {
      if (isClearing) {
        session.setAttribute("USER_TYPE", GlobalConstants.CLEARING_USER_TYPE_VAL);
        Cookie cookie = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.CLEARING_USER_TYPE_VAL);
        response.addCookie(cookie);
      } else {
         session.setAttribute("USER_TYPE", GlobalConstants.NON_CLEARING_USER_TYPE_VAL);
         Cookie cookie = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.NON_CLEARING_USER_TYPE_VAL);
         response.addCookie(cookie);
      }
      if (session.getAttribute("SHOW_CLEARING_SPLASH_SCREEN") == null) {
        request.getSession().setAttribute("SHOW_CLEARING_SPLASH_SCREEN", "NO");
      }
      if (!GenericValidator.isBlankOrNull(request.getParameter("navQual")) && "Postgraduate".equalsIgnoreCase(request.getParameter("navQual"))) { //Added by Amir for 3_NOV_15 release for setting networkID      
        session.setAttribute("cpeQualificationNetworkId", "3");
      } else {
        session.setAttribute("cpeQualificationNetworkId", "2");
      }
    }
    return null;
  }
}
