package com.wuni.search.controller;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.CourseDetailsBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import mobile.util.MobileUtils;

import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.SessionUtilities;
import com.wuni.util.seo.SeoPageNamesAndFlags;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.sql.DataModel;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.contenthub.SocialVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;
import com.wuni.util.valueobject.review.ReviewSubjectVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.util.valueobject.search.CourseKISDataVO;
import com.wuni.util.valueobject.search.EntryPointsUCASCourseVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.business.IPreClearingLandingBusiness;
import spring.util.GlobalMethods;
import spring.valueobject.search.TimesRankingVO;

/**
 * PAGE NEME:    KEYWORD MONEY PAGE (keyword search result page)
 *
 * @since        wu318_20111020 - redesign
 * @author       Mohamed Syed
 *
 * PAGE NEME:    Course details page
 * SINCE:        wu311_20110531
 * AUTHOUR:      Mohamed Syed
 * URL_PATTERN:  www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
 * URL_EXAMPLE:  ww.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/3769/cdetail.html
 * NOTE:         This definition moved from "struts-config.xml" during CODE_AUDIT & REDESIGN
 *
 * urlArray[0]:                                  - EMPTY
 * urlArray[1]: courses                          - CONSTANT
 * urlArray[2]: Postgraduate-details             - [STUDY LEVEL]-details
 * urlArray[3]: Accounting-MPhil-course-details  - [COURSE TITLE]-courses-details
 * urlArray[4]: 29837179                         - [COURSE_ID]
 * urlArray[5]: 3769                             - [COLLEGE_ID]
 * urlArray[6]: cdetail                          - CONSTANT
 *
 * Modification history:
 * *******************************************************************************************************************
 * Author           Relase Date              Modification Details                                             Rel Ver.
 * *******************************************************************************************************************
 * Yogeswari        09.06.2015               Changes for clearing Sticky Nav design.
 * Yogeswari        30.06.2015               changed for clearing/course detail page responsive task
 * Prabhakaran V.   16.02.2016               CD page URL restructure
 * Prabhakaran V.   29.03.2016               Remove cDimPostcode request for insights...                        wu_551
 * Prabhakaran V.   31.05.2016               Added newPostcode request for insights...                          wu_553
 * Prabhakaran V.   11.07.2017               Added partTimeMsgFlag for showing message on UCAS partime course   wu_566
 * Hema.S           20.02.2018               Modified new UCAS fees Structure                                   wu_573
 * Prabhakaran V.   20.02.2018               Added collegeId. collegeName for smart pixel tagging               wu_s73
 * URL_PATTERN:  www.whatuni.com/degrees/courses/[COURSE_TITLE]/[COLLEGE_TITLE]/cd/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
 * URL_EXAMPLE:  www.whatuni.com/degrees/courses/Accounting-and-Finance-BA-Hons/University-Of-Greenwich/cd/54930094/1039/cdetail.html  
 * Sangeeth.S       25.09.2018               Redirect of duplicate content of URL for updated college name      wu_581
 * Sangeeth.S       23.10.2018               Removed how you are assessed pod 
 * Hema.S           18.12.2018               Added Review breakdown and common phrases cursor                   wu_584
 * Jeyalakshmi.D    01.04.2019               CD page redesign for whatuni go journey        
 * Hemalatha.K      04.02.2020               changed the canonicalUrl
 * Kailash L        02.06.2020               Commented partTimeMsgFlag for UCAS partime course
 * Sujitha V        23.06.2020               Changes for clearing course detail page.
 * Sujitha V        30.09.2020               Clearing course page redirect when clearing switch is off
 */
@Controller
public class CourseDetailsController {
	
	@Autowired
	private ISearchBusiness searchBusiness;
	
	@Autowired 
	IPreClearingLandingBusiness preClearingLandingBusiness;
	
	@RequestMapping(value = {"/*/*/cd/*/*/cdpage","/courses/*/*/*/*/clcdetail","/loadKISDataAjax"}, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getCourseDetails(ModelMap model, CourseDetailsBean courseDetailBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    String collegeId = "";
	    String courseId = "";
	    String opportunityId = request.getParameter("opportunityid");
	    request.setAttribute("opportunityId",opportunityId);
	    String clearingOnOff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
	    
	    String pageName = "";
	    boolean isClearingCourse = false;
	    CommonFunction common = new CommonFunction();
	    CommonUtil util = new CommonUtil();
	    SessionUtilities sessionUtil = new SessionUtilities();
	    CollegeUtilities uniUtils = new CollegeUtilities();
	    String domainPathWithImgSrc = CommonUtil.getImgPath(SpringConstants.WU_CONT_IMAGE, 0);
	    String domainPathWithoutImgSrc = CommonUtil.getImgPath("", 0); 
	    boolean mobileFlag = MobileUtils.userAgentCheck(request);
	    String ajaxFlag = request.getParameter("ajaxFlag");
	    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
	    String urlArray[] = URL_STRING.split("/");
	    String clearingQueryString = request.getQueryString();
	    //Added redirection rule for clearing pages when clearing switch is off by Sujitha V on 30_09_2020 rel
	    if(StringUtils.equalsIgnoreCase(clearingOnOff, GlobalConstants.OFF) && StringUtils.isNotBlank(clearingQueryString) && StringUtils.indexOf(clearingQueryString, GlobalConstants.SEO_PATH_CLEARING) > -1) {
	      String tempRedirectUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE; 
	      response.setStatus(response.SC_MOVED_TEMPORARILY);
	      response.sendRedirect(tempRedirectUrl);
	      return null;
	    }
	    if (GenericValidator.isBlankOrNull(ajaxFlag)) {	      
	      String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + request.getRequestURI().replace(".html", "");  //Added by Sangeeth.S for 25_Sep_18 rel
	      canonicalUrl = canonicalUrl.indexOf("cdpage") > -1 ? canonicalUrl.replace("cdpage", "") : canonicalUrl;
	      
	      if (TimeTrackingConstants.URL_TRACKING_FLAG) {
	        new AdminUtilities().printUrlDetails(this.getClass().toString(), URL_STRING, urlArray);
	      }
	      new CommonFunction().loadUserLoggedInformation(request, request.getServletContext(), response);
	       if (urlArray != null && urlArray.length != 7) {//
	        request.setAttribute("reason_for_410", "1) DEPRECATED url: URL length not equal to 7");
	        response.sendError(410, "410 error message");
	        return null;
	      }
	      if (URL_STRING != null && URL_STRING.indexOf("/all-det/") != -1) {
	        request.setAttribute("reason_for_410", "2) Found 'all-det' in course URL");
	        response.sendError(410, "410 error message");
	        return null;
	      }
	     
	      String strCourseName = null;
	      courseId = urlArray[4];//
	     if (urlArray.length > 7) {
	        pageName = urlArray[7];
	      } else {
	        pageName = urlArray[6];
	      }
	      
	      if (urlArray.length > 7) {
	        collegeId = urlArray[6];
	        opportunityId = urlArray[5];
	      } else {
	        collegeId = urlArray[5];
	      }
	      String degreeCourseFlag = new CommonFunction().checkDegreeCourse(collegeId);
	      if("Y".equalsIgnoreCase(degreeCourseFlag)){
	        request.setAttribute("crsDetailIndex", "true");
	      }
	      String clearingFlag = "";
	      if (("ON").equals(clearingOnOff)) {
	        String queryStr = request.getQueryString();
	        if ("clearing".equals(queryStr)) {
	          pageName = "clcdetail";
	          clearingFlag = "Y";
	          request.setAttribute("studyLevelDesc","Clearing");//Added for the June_5_18 release by Sangeeth.S
	        }
	      }
	      if (courseId != null && courseId.trim().length() > 0) {
	        String courseDeletedStatus = "";
	        if ("clcdetail".equalsIgnoreCase(pageName)) {
	          courseDeletedStatus = new GlobalFunction().checkClearingCourseDeleted(courseId); // moved nmethod to global function 15_JUL_2014
	          /* ::START:: Yogeswari :: 19.06.2015 :: Changes for clearing Sticky Nav design. */
	          if (courseDeletedStatus != null && "NO".equalsIgnoreCase(courseDeletedStatus)) {
	            isClearingCourse = true;
	            request.setAttribute("CLEARING_COURSE", "TRUE");
	          }
	          /* ::END:: Yogeswari :: 19.06.2015 :: Changes for clearing Sticky Nav design.*/
	        } else {
	          courseDeletedStatus = new GlobalFunction().checkCourseDeleted(courseId);
	        }
	        if (courseDeletedStatus != null && !courseDeletedStatus.equalsIgnoreCase("NO")) {
	          request.setAttribute("reason_for_410", "3) This course has been " + courseDeletedStatus + " deleted (" + (String)courseId + ")");
	          response.sendError(410, "410 error message");
	          return null;
	        }
	        String urlCourseName = urlArray[1];//        
	        //Added by Sangeeth.S for Sep_25_18 rel :: Redirect of duplicate content of URL for updated college name 
	        String urlCollegeName = urlArray[2];
	        String strCollegeName = "";
	        boolean collegeNameChangeFlag = false;
	        CollegeNamesVO uniNames = null;
	        uniNames = uniUtils.getCollegeNames(collegeId, request);
	        if (uniNames != null && !GenericValidator.isBlankOrNull(uniNames.getCollegeName())) {          
	          strCollegeName = uniNames.getCollegeName().trim();          
	          strCollegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(strCollegeName)));
	        }        
	        if(urlCollegeName != null && !urlCollegeName.equalsIgnoreCase(strCollegeName)){          
	          collegeNameChangeFlag = true;
	        }
	        //
	        strCourseName = new CommonFunction().getCourseName(courseId, clearingFlag); //Changed new function on 16_May_2017, By Thiyagu G.
	        request.setAttribute("courseTitle", strCourseName);
	        strCourseName = strCourseName != null && strCourseName.trim().length() > 0 ? strCourseName.trim().replaceAll("\\/", "-") : "";
	        strCourseName = (strCourseName != null && strCourseName.trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(new CommonFunction().replaceSpecialCharacter(strCourseName))) : "");
	        //CD page url redirection code added by Prabha on 10_May_16
	        if ((!GenericValidator.isBlankOrNull(strCourseName) && !GenericValidator.isBlankOrNull(urlCourseName) && !urlCourseName.equalsIgnoreCase(strCourseName)) || collegeNameChangeFlag) {
	          String redirectURL = "";
	          redirectURL = GlobalConstants.WU_CONTEXT_PATH + URL_STRING.replace(urlCourseName, strCourseName.toLowerCase());
	          redirectURL = redirectURL.replace(urlCollegeName, strCollegeName.toLowerCase());
	          redirectURL = redirectURL.toLowerCase();
	          response.setStatus(response.SC_MOVED_PERMANENTLY);
	          if("clcdetail".equalsIgnoreCase(pageName)){redirectURL = redirectURL + "?clearing";}
	            response.setHeader("Location", redirectURL.replace("cdpage", ""));
	            response.setHeader("Connection", "close");
	          return null;
	        }
	        //End of the redirection code
	        if ((strCourseName != null && strCourseName.trim().length() > 0) && (urlCourseName != null && urlCourseName.trim().length() > 0)) {
	        //  urlCourseName = urlCourseName.replaceAll("-course-details", "");
	          urlCourseName = urlCourseName.toLowerCase();
	          strCourseName = strCourseName.toLowerCase();
	          if (!urlCourseName.equals(strCourseName)) {
	            request.setAttribute("courseTitle410", strCourseName);
	            request.setAttribute("collegeId410", collegeId);
	            request.setAttribute("reason_for_410", "4) Same course id with different course title");
	            response.sendError(410, "410 error message");
	            return null;
	          }
	        }
	      }
	      //
	      String currentPageUrl = (common.getDomainName(request,"N") + request.getRequestURI());
	      request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length())));//Added getSchemeName by Indumathi Mar-29-16
	      request.setAttribute("desktopURL", (request.getRequestURI())); //9_DEC_14 Added by Amir for switch to mobile site    
	      
	      request.setAttribute("canonicalUrl", canonicalUrl);     //Added by Sangeeth.S for 25_Sep_18 rel
	      sessionUtil.updateSessionData(request);
	      int found = -1;
	      if (URL_STRING != null) {
	        found = URL_STRING.indexOf(GlobalConstants.SEO_PATH_COURSEDETAIL_PAGE);
	      }
	      if (found != -1) {
	        courseDetailBean.setStudyLevelId("M,N,A,T,L");
	        courseDetailBean.setStudyModeId("");
	        courseDetailBean.setCourseName("Enter course keyword");
	      }
	    } else if ((!GenericValidator.isBlankOrNull("ajaxFlag")) && ("Y").equals(ajaxFlag)) {
	      collegeId = request.getParameter("collegeId");
	      courseId = request.getParameter("courseId");
	      opportunityId = request.getParameter("opportunityId");    
	      String clearingFlag = "";
	      //
	      if (("ON").equals(clearingOnOff)) {
	        String queryStr = request.getQueryString();
	        if (queryStr.contains("clearing")){
	          pageName = "clcdetail";
	          isClearingCourse = true;	           
	          request.setAttribute("CLEARING_COURSE", "TRUE");
	          clearingFlag = "Y";
	        }
	      }
	      new GlobalMethods().setCovid19SessionData(request, response);
	      String COVID19_SYSVAR = new SessionData().getData(request, GlobalConstants.COVID19_SYSVAR);
	      model.addAttribute("COVID19_SYSVAR", COVID19_SYSVAR);
	      String strCourseName = new CommonFunction().getCourseName(courseId, clearingFlag); //Changed new function on 16_May_2017, By Thiyagu G.
	      request.setAttribute("courseTitle", strCourseName);
	    }
	    //
	    request.setAttribute("pagename", pageName);
	    request.setAttribute("seoCourseId", courseId);
	    request.setAttribute("seoCollegeId", collegeId);
	    request.setAttribute("courseId", courseId); //Added for wu_536 3-FEB-2015_REL by Karthikeyan.M for logging dimension7 cpe lvl2
	    //
	    String x = new SessionData().getData(request, "x");
	    String y = new SessionData().getData(request, "y");
	    String basketId = "";
	 //   basketId = (y != null && (y.equals("0") || y.trim().length() == 0)) ? basketId : "";
	   // y = (basketId != null && basketId.trim().length() > 0) ? "" : y;
	    if ((y != null && !y.equals("0") && y.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
	      y = "0";
	      basketId = "";
	    }
	    //
	    basketId = common.checkCookieStatus(request);
	    if(GenericValidator.isBlankOrNull(basketId) && "0".equals(basketId)){
	      basketId = null;
	    }
	    String courseType = (isClearingCourse) ? "CLEARING_COURSE" : "NORMAL_COURSE"; // :: Yogeswari :: 19.06.2015 :: Changes for clearing Sticky Nav design.
	    List parameters = new ArrayList();
	    Map courseDetailsInfoMap = new HashMap();
	    parameters.add(courseId);
	    parameters.add(collegeId);
	    parameters.add(opportunityId);
	    parameters.add(x);
	    parameters.add(y);
	    parameters.add(basketId);
	    parameters.add(courseType); //
	    parameters.add(SeoPageNamesAndFlags.COURSE_DETAILS_PAGE_NAME); //page naem to fetch seo-meta-tag-descriptions
	    parameters.add(SeoPageNamesAndFlags.COURSE_DETAILS_PAGE_FLAG);   
	    parameters.add(request.getHeader("user-agent"));
	    parameters.add(new SessionData().getData(request, "userTrackId"));
	    parameters.add((String)session.getAttribute("subjectSessionId"));
	    //
	    if(StringUtils.equalsIgnoreCase(clearingOnOff, "ON") && isClearingCourse){
	      //System.out.println("inside clearing cd page");
	      courseDetailsInfoMap	= preClearingLandingBusiness.getClearingCourseDetails(parameters);
	      model.addAttribute("CLEARING_CD_PAGE", "CLEARING_CD_PAGE");
	    }else{
	    	//System.out.println("inside normal cd page");
	      courseDetailsInfoMap = searchBusiness.getCourseDetailsInfo(parameters);
	    }
	    String studyLevelCode = "";
	    ArrayList sideBarList = new ArrayList();
	    if (courseDetailsInfoMap != null) {
	      ArrayList courseInfoList = (ArrayList)courseDetailsInfoMap.get("oc_course_info");
	      if (courseInfoList != null && courseInfoList.size() > 0) {
	        request.setAttribute("courseInfoList", courseInfoList);
	        CourseDetailsVO cdVO = (CourseDetailsVO)courseInfoList.get(0);
	        
	        String strCourseName = cdVO.getCourseTitle();
	        String collegeName = cdVO.getCollegeName();
	        String collegeNameDisplay = cdVO.getCollegeNameDisplay();
	        request.setAttribute("interactive-college-id", collegeId);
	        request.setAttribute("interactionCollegeId", collegeId);
	        request.setAttribute("interactionCollegeName", collegeName);
	        request.setAttribute("interactionCollegeNameDisplay", collegeNameDisplay);
	        /* Added college display name for smart pixel by Prabha on 20_02_2018 */
	        if(!GenericValidator.isBlankOrNull(collegeName)){
	          request.setAttribute("cDimCollegeDisName", ("\""+collegeName+"\""));
	        }
	        /* End od smart pixel code */
	        request.setAttribute("collegeName", collegeName);
	        request.setAttribute("collegeNameDisplay", collegeNameDisplay);
	        if (!GenericValidator.isBlankOrNull(cdVO.getAppyNowURL())) {
	          request.setAttribute("learn_dir_url", cdVO.getAppyNowURL());
	        }
	        //for GA
	        strCourseName = strCourseName != null && strCourseName.trim().length() > 0 ? strCourseName.trim().replaceAll("\\/", "-") : "";
	        strCourseName = (strCourseName != null && strCourseName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(strCourseName))) : "");
	        request.setAttribute("hitbox_course_title", strCourseName);
	        request.setAttribute("hitbox_college_name", collegeName);
	        request.setAttribute("hitbox_college_id", collegeId);
	        cdVO.getCourseTitle();
	        //16-Sep-2014 - Insight
	        request.setAttribute("insightPageFlag", "yes");
	        if (cdVO.getCourseTitle() != null && cdVO.getCourseTitle().trim().length() > 0) {
	          request.setAttribute("cDimCourseTitle", new GlobalFunction().getReplacedString(cdVO.getCourseTitle()));
	        }
	        if (!GenericValidator.isBlankOrNull(cdVO.getStudyLevelCode())) {
	          studyLevelCode = cdVO.getStudyLevelCode();
	          request.setAttribute("qualification", studyLevelCode);
	          new GlobalFunction().getStudyLevelDesc(studyLevelCode, request);
	        }
	        String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
	        model.addAttribute("gaCollegeName", gaCollegeName);
	        if(StringUtils.equalsIgnoreCase(clearingOnOff, "ON") && isClearingCourse){
	         if(StringUtils.isNotBlank(cdVO.getHybridFlag())){
	        	model.addAttribute("hybridFlag", cdVO.getHybridFlag());
	          }
	         if(StringUtils.isNotBlank(cdVO.getCompressedFlag())){
		       model.addAttribute("compressedFlag", cdVO.getCompressedFlag());
		     }
	         if(StringUtils.isNotBlank(cdVO.getStartDate())){
		       model.addAttribute("startDate", cdVO.getStartDate());
		      }
	        }
	      }
	      //     
	      request.setAttribute("cDimCollegeId", GenericValidator.isBlankOrNull(collegeId) ? "" : collegeId.trim());
	      
	      if(!GenericValidator.isBlankOrNull(collegeId)){
	        request.setAttribute("cDimCollegeIds", ("'" + collegeId.trim() + "'")); //Added for Smart pixel tagging by Prabha on 20.02.2018
	      }
	      
	      //
	      ArrayList listOfCollegeInteractionDetailsWithMedia = (ArrayList)courseDetailsInfoMap.get("oc_enquiry_info");
	      if (listOfCollegeInteractionDetailsWithMedia != null && listOfCollegeInteractionDetailsWithMedia.size() > 0) {
	        request.setAttribute("listOfCollegeInteractionDetailsWithMedia", listOfCollegeInteractionDetailsWithMedia);
	        CollegeCpeInteractionDetailsWithMediaVO enquiryInfo = (CollegeCpeInteractionDetailsWithMediaVO)listOfCollegeInteractionDetailsWithMedia.get(0);
	        String profileType = enquiryInfo.getProfileType();
	        String subOrderItemId = enquiryInfo.getSubOrderItemId();
	        request.setAttribute("profileType", profileType);
	        request.setAttribute("subOrderItemId", subOrderItemId);
	        String clr_websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderItemId, "website_price");
	        model.addAttribute("clr_websitePrice", clr_websitePrice);
	      }
	      //
	      boolean keyStatsFlag = false;
	      ArrayList keyStatsList = (ArrayList)courseDetailsInfoMap.get("OC_KEY_STATS_INFO");
	      if (keyStatsList != null && keyStatsList.size() > 0) {
	        request.setAttribute("keyStatsList", keyStatsList);
	        keyStatsFlag = true;
	      }
	      ArrayList uniRankingFnList = (ArrayList)courseDetailsInfoMap.get("OC_UNI_RANKING_FN");
	      if (uniRankingFnList != null && uniRankingFnList.size() > 0) {
	        TimesRankingVO timesRankingVO = (TimesRankingVO)uniRankingFnList.get(0);
	        if ((!GenericValidator.isBlankOrNull(timesRankingVO.getUniTimesRanking())) || (!GenericValidator.isBlankOrNull(timesRankingVO.getStudentRanking())) || (timesRankingVO.getTimesSubRankingList() != null && timesRankingVO.getTimesSubRankingList().size() > 0)) {
	          request.setAttribute("uniRankingFnList", uniRankingFnList);
	          keyStatsFlag = true;
	        }
	      }
	      //
	      ArrayList getCourseDurationList = (ArrayList)courseDetailsInfoMap.get("OC_GET_COURSE_DURATIONS");
	      if (getCourseDurationList != null && getCourseDurationList.size() > 0) {
	        request.setAttribute("getCourseDurationList", getCourseDurationList);
	        keyStatsFlag = true;
	      }
	      //
	      String moduleInfo = (String)courseDetailsInfoMap.get("o_module_info");
	      if (!GenericValidator.isBlankOrNull(moduleInfo)) {
	        request.setAttribute("moduleInfo", moduleInfo);
	      }
	      //
	      ArrayList moduleDetailsList = (ArrayList)courseDetailsInfoMap.get("oc_module_details");
	      if (moduleDetailsList != null && moduleDetailsList.size() > 0) {
	        request.setAttribute("moduleList", moduleDetailsList);
	      }
	      /*/commented for Oct_23_18 rel by sangeeth.s
	      ArrayList getAssessedCourseList = (ArrayList)courseDetailsInfoMap.get("OC_GET_ASSESSED_COURSE");
	      if (getAssessedCourseList != null && getAssessedCourseList.size() > 0) {
	        CourseKISDataVO kissubjectdate = (CourseKISDataVO)getAssessedCourseList.get(0);
	        if ((!GenericValidator.isBlankOrNull(kissubjectdate.getCourseWork())) && (!GenericValidator.isBlankOrNull(kissubjectdate.getPracticalWork())) && (!GenericValidator.isBlankOrNull(kissubjectdate.getExam()))) {
	          request.setAttribute("getAssessedCourseList", getAssessedCourseList);
	          LabelValueBean lvBean = new LabelValueBean();
	          lvBean.setValue("div_how_assessed");
	          lvBean.setLabel("How you're assessed");
	          sideBarList.add(lvBean);
	        }
	      }
	      /*/
	      ArrayList ucasCourseDetailList = (ArrayList)courseDetailsInfoMap.get("oc_ucas_course_details");
	      if (ucasCourseDetailList != null && ucasCourseDetailList.size() > 0) {
	        request.setAttribute("ucasCourseDetailList", ucasCourseDetailList);
	        EntryPointsUCASCourseVO entryPointsUCASCourseVO = (EntryPointsUCASCourseVO)ucasCourseDetailList.get(0);
	        request.setAttribute("entryDesc",entryPointsUCASCourseVO.getEntryDesc());
	      }
	      
	      //Added this for clearing cd page by Sujitha V on 23_JUNE_2020
	      String minQuuivalentPoints = "";
	      if(StringUtils.equalsIgnoreCase(clearingOnOff, "ON") && isClearingCourse){
	    	String scoreLabel = (String) courseDetailsInfoMap.get("P_SCORE_LABEL");
	    	if(StringUtils.isNotBlank(scoreLabel)){
	          model.addAttribute("scoreLabel", scoreLabel);
	    	} 
	    	String scoreLevel = (String) courseDetailsInfoMap.get("P_SCORE_LEVEL");
	    	if(StringUtils.isNotBlank(scoreLevel)){
	          model.addAttribute("scoreLevel", scoreLevel);
	    	} 
	    	
	    	String scoreLabelTooltip = (String) courseDetailsInfoMap.get("P_SCORE_LABEL_TOOLTIP");
	    	if(StringUtils.isNotBlank(scoreLabelTooltip)){
	          model.addAttribute("scoreLabelTooltip", scoreLabelTooltip);
	    	} 
	    	
	    	String userScore = (String) courseDetailsInfoMap.get("P_USER_SCORE");
	    	if(StringUtils.isNotBlank(userScore)){
	          model.addAttribute("userScore", userScore);
	    	}
	    	
	        minQuuivalentPoints = (String) courseDetailsInfoMap.get("P_MIN_EQUIVALENT_POINTS");
	        //System.out.println("minQuuivalentPoints-->"+minQuuivalentPoints);
	    	if(StringUtils.isNotBlank(minQuuivalentPoints)){
	          model.addAttribute("minQuuivalentPoints", minQuuivalentPoints);
	    	} 	      	    	
	      }
	      
	      if(isClearingCourse) {
	        //
	    
	        //
	            if (courseInfoList != null && courseInfoList.size() > 0) {
	                LabelValueBean lvBean = new LabelValueBean();
	                lvBean.setLabel("Course info");
	                lvBean.setValue("div_course_info");
	                sideBarList.add(lvBean);
	            }
	            //
	             if (keyStatsFlag) {
	               request.setAttribute("keyStatsFlag", "true");
	               LabelValueBean lvBean = new LabelValueBean();
	               lvBean.setValue("div_key_stats");
	               lvBean.setLabel("Key stats");
	               sideBarList.add(lvBean);
	             }
	             //
	              if (!GenericValidator.isBlankOrNull(moduleInfo)) {
	                  LabelValueBean lvBean = new LabelValueBean();
	                  lvBean.setValue("div_module_info");
	                  lvBean.setLabel("Modules");
	                  sideBarList.add(lvBean);
	              }
	              //
	               if (moduleDetailsList != null && moduleDetailsList.size() > 0) {
	                   LabelValueBean lvBean = new LabelValueBean();
	                   lvBean.setValue("div_module_info");
	                   lvBean.setLabel("Modules");
	                   sideBarList.add(lvBean);
	               } 
	               if(StringUtils.isNotBlank(minQuuivalentPoints)){
	       	        LabelValueBean lvBean = new LabelValueBean();
	       	        lvBean.setLabel("Entry requirements");
	       	        lvBean.setValue("div_entry_req");
	       	        request.setAttribute("entryRequirmentPod","Yes");
	       	        sideBarList.add(lvBean);
	       	      }
	        }
	        
	          else if(!isClearingCourse) {
	            if (courseInfoList != null && courseInfoList.size() > 0) {
	                LabelValueBean lvBean = new LabelValueBean();
	                lvBean.setLabel("Course info");
	                lvBean.setValue("div_course_info");
	                sideBarList.add(lvBean);
	            }
	            //
	             if (keyStatsFlag) {
	               request.setAttribute("keyStatsFlag", "true");
	               LabelValueBean lvBean = new LabelValueBean();
	               lvBean.setValue("div_key_stats");
	               lvBean.setLabel("Key stats");
	               sideBarList.add(lvBean);
	             }
	             //
	              if (!GenericValidator.isBlankOrNull(moduleInfo)) {
	                  LabelValueBean lvBean = new LabelValueBean();
	                  lvBean.setValue("div_module_info");
	                  lvBean.setLabel("Modules");
	                  sideBarList.add(lvBean);
	              }
	              //
	               //
	                if (moduleDetailsList != null && moduleDetailsList.size() > 0) {
	                    LabelValueBean lvBean = new LabelValueBean();
	                    lvBean.setValue("div_module_info");
	                    lvBean.setLabel("Modules");
	                    sideBarList.add(lvBean);
	                }
	            //
	             if (ucasCourseDetailList != null && ucasCourseDetailList.size() > 0) {
	                 LabelValueBean lvBean = new LabelValueBean();
	                 lvBean.setLabel("Entry requirements");
	                 lvBean.setValue("div_entry_req");
	                 sideBarList.add(lvBean);
	             }
	             //
	          }
	      //
	      ArrayList getApplicantsData = (ArrayList)courseDetailsInfoMap.get("OC_APPLICANTS_DATA");
	      if (getApplicantsData != null && getApplicantsData.size() > 0) {
	        request.setAttribute("getApplicantsData", getApplicantsData);
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setLabel("Application rate");
	        lvBean.setValue("div_application_rate");
	        sideBarList.add(lvBean);
	      }
	      //
	      ArrayList previousSubjectList = (ArrayList)courseDetailsInfoMap.get("OC_PREVIOUS_STUDY_SUBJS");
	      if (previousSubjectList != null && previousSubjectList.size() > 0) {
	        request.setAttribute("previousSubjectList", previousSubjectList);
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setValue("div_previous_study");
	        lvBean.setLabel("Previous study");
	        sideBarList.add(lvBean);
	      }
	      //Modified by Hema.S on 2-2-2018 for UCAS new Fees Structure
	      ArrayList getFeesData = (ArrayList)courseDetailsInfoMap.get("OC_FEES_DATA");
	      CourseKISDataVO courseDetailKisVO = null;
	      if (getFeesData != null && getFeesData.size() > 0) {       
	          courseDetailKisVO = (CourseKISDataVO)getFeesData.get(0);  
	          request.setAttribute("subOrderItemId",courseDetailKisVO.getSubOrderItemId());        
	        request.setAttribute("getFeesData", getFeesData);
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setValue("div_tuition_fees");
	        lvBean.setLabel("Tuition fees");
	        sideBarList.add(lvBean);
	      }
	      //
	      ArrayList getReviewList = (ArrayList)courseDetailsInfoMap.get("OC_REVIEW_LIST_FN");
	      if (getReviewList != null && getReviewList.size() > 0) {
	        request.setAttribute("getReviewList", getReviewList);
	        ReviewListBean reviewListBean = (ReviewListBean)getReviewList.get(0);
	        request.setAttribute("collegeId", reviewListBean.getCollegeId());
	        request.setAttribute("collegeName", reviewListBean.getCollegeName());
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setValue("div_student_reviews");
	        lvBean.setLabel("Student reviews");
	        sideBarList.add(lvBean);
	      }
	      //Added this for review Redesign by Hema.S on 18_DEC_2018_REL
	      ArrayList reviewBreakDownList = (ArrayList)courseDetailsInfoMap.get("PC_REVIEW_BREAKDOWN");
	      if(reviewBreakDownList !=null && reviewBreakDownList.size() > 0){
	        request.setAttribute("reviewBreakDownList", reviewBreakDownList);
	        ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakDownList.get(0);
	        request.setAttribute("rating",reviewBreakDownVO.getRating());
	        request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
	        request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
	      }
	        ArrayList getReviewBreakdownSubjectList = (ArrayList)courseDetailsInfoMap.get("PC_SUB_REVIEW_BREAKDOWN");  
	        if (getReviewBreakdownSubjectList != null && getReviewBreakdownSubjectList.size() > 0) {
	          request.setAttribute("getReviewBreakdownSubjectList", getReviewBreakdownSubjectList);
	          ReviewBreakDownVO reviewSubjectBreakDownVO = (ReviewBreakDownVO)getReviewBreakdownSubjectList.get(0);
	          request.setAttribute("subjectDropDownRating",reviewSubjectBreakDownVO.getRating());
	          request.setAttribute("subjectDropDownReviewCount",reviewSubjectBreakDownVO.getReviewCount());
	          request.setAttribute("subjectReviewExact",reviewSubjectBreakDownVO.getReviewExact()); 
	        }       
	      //  
	        ArrayList commonPhrasesList = (ArrayList)courseDetailsInfoMap.get("PC_COMMON_PHRASES");
	        if (commonPhrasesList != null && commonPhrasesList.size() > 0) { 
	          CommonPhrasesVO  commonPhrasesVO = null;
	          for(int i = 0; i < commonPhrasesList.size(); i++){
	            commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(i);
	            if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
	              request.setAttribute("commonPhrasesList", commonPhrasesList);
	              break;
	            }
	          }        
	        }
	      //
	      ArrayList courseReviewSubjectList = (ArrayList)courseDetailsInfoMap.get("PC_CRS_REVIEW_SUB");
	      if(courseReviewSubjectList !=null && courseReviewSubjectList.size() > 0){
	        request.setAttribute("courseReviewSubjectList",courseReviewSubjectList);
	          ReviewSubjectVO reviewSubjectVO = (ReviewSubjectVO)courseReviewSubjectList.get(0);
	          request.setAttribute("firstSubjectName",reviewSubjectVO.getSubjectName());
	      }
	      //
	      ArrayList uniInfoList = (ArrayList)courseDetailsInfoMap.get("OC_UNI_INFO_FN");
	      if (uniInfoList != null && uniInfoList.size() > 0) {
	        request.setAttribute("uniInfoList", uniInfoList);
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setValue("div_uni_info");
	        lvBean.setLabel("Uni info");
	        sideBarList.add(lvBean);
	      }
	      //
	      ArrayList listOfLocationInfo = (ArrayList)courseDetailsInfoMap.get("OC_VENUE_DETAILS");
	      if (listOfLocationInfo != null && listOfLocationInfo.size() > 0) {
	        request.setAttribute("listOfLocationInfo", listOfLocationInfo);
	        LocationsInfoVO bean = (LocationsInfoVO)listOfLocationInfo.get(0);
	        request.setAttribute("latitudeStr", bean.getLatitude());
	        request.setAttribute("longitudeStr", bean.getLongitude());
	        request.setAttribute("cityGudieHeadLine", bean.getHeadLine());
	        request.setAttribute("cityGudieLocation", bean.getLocation());
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setLabel("Where you'll study");
	        lvBean.setValue("div_location");
	        sideBarList.add(lvBean);
	        String postcode = "";
	        String countyState = "";
	        String town = "";
	        postcode = bean.getPostcode();
	        town = bean.getTown();
	        countyState = bean.getCountryState();
	        //
	        if (town != null && !"".equals(town)) { //Changed location request attribute for 08_Mar_2016, By Thiyagu G.
	          request.setAttribute("location", new GlobalFunction().getReplacedString(town.toLowerCase()));
	          request.setAttribute("cDimCountry", "UK");
	        } else if (countyState != null && !"".equals(countyState)) {
	          request.setAttribute("location", new GlobalFunction().getReplacedString(countyState.toLowerCase()));
	          request.setAttribute("cDimCountry", "UK");
	        }
	        if(!GenericValidator.isBlankOrNull(postcode)){
	          request.setAttribute("newPostcode", postcode); //cDimPostcode added for insights by Prabha on 31_may_2016
	        }
	      }
	      //
	      ArrayList uniSPList = (ArrayList)courseDetailsInfoMap.get("oc_uni_sp_list");
	      if (uniSPList != null && uniSPList.size() > 0) {
	        request.setAttribute("uniSPList", uniSPList);
	        request.setAttribute("uniSPListSize", String.valueOf(uniSPList.size()));
	        LabelValueBean lvBean = new LabelValueBean();
	        lvBean.setLabel("Other departments");
	        lvBean.setValue("div_other_departments");
	        sideBarList.add(lvBean);
	      }
	      //
	      ArrayList oppDetailList = (ArrayList)courseDetailsInfoMap.get("OC_OPPORTUNITY_DETAILS");
	      if (oppDetailList != null && oppDetailList.size() > 0) {
	        request.setAttribute("oppDetailList", oppDetailList);
	      }
	      //
	      ArrayList pageMetaTagDetails = (ArrayList)courseDetailsInfoMap.get("OC_PAGE_META_TAGS");
	      if (pageMetaTagDetails != null && pageMetaTagDetails.size() > 0) {
	        String robotValue = "index,follow";
	        ArrayList customizedPageMetaTagDetails = new spring.helper.CommonHelper().addRobotToMetaTagDetails(pageMetaTagDetails, robotValue, request);
	        request.setAttribute("pageMetaTagDetails", customizedPageMetaTagDetails);
	      }
	      //
	      String pixelTracking = (String)courseDetailsInfoMap.get("o_pixel_tracking_code");
	      if (!GenericValidator.isBlankOrNull(pixelTracking)) {
	        request.setAttribute("pixelTracking", pixelTracking); //Modified for wu_536 3-FEB-2015_REL by Karthi
	      }
	      //Added non advisor provider's logo to show in registeration lightbox for 24_Nov_2015, by Thiyagu G. - Start
	      String collegeLogo = (String)courseDetailsInfoMap.get("p_college_logo");
	      if (!GenericValidator.isBlankOrNull(collegeLogo)) {
	        request.setAttribute("collegeLogo", collegeLogo);
	      }else{
	        request.setAttribute("collegeLogo", "");
	      }
	      //Added non advisor provider's logo to show in registeration lightbox for 24_Nov_2015, by Thiyagu G. - End
	      String advertiserFlag = (String)courseDetailsInfoMap.get("O_ADVERTISER_FLAG");
	      if (!GenericValidator.isBlankOrNull(advertiserFlag)) {
	        request.setAttribute("advertiserFlag", advertiserFlag);
	      }
	      //Added by Indumathi.S Feb-16-16 Rel
	      String degreeCourseFlag = (String)courseDetailsInfoMap.get("p_degree_course_flag");
	      if("Y".equalsIgnoreCase(degreeCourseFlag)){
	        request.setAttribute("crsDetailIndex", "true");
	      }
	      
	      //Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016 Start
	      String courseTypeText = (String)courseDetailsInfoMap.get("p_course_type");
	      if (!GenericValidator.isBlankOrNull(courseTypeText)) {
	        request.setAttribute("courseTypeText", courseTypeText);
	      }
	      String courseLastUpdatedDate = (String)courseDetailsInfoMap.get("p_course_last_updated_date");
	      if (!GenericValidator.isBlankOrNull(courseLastUpdatedDate)) {
	        request.setAttribute("courseLastUpdatedDate", courseLastUpdatedDate);
	      }    
	      //
	      String oppLastUpdatedDate = (String)courseDetailsInfoMap.get("p_opp_last_updated_date");
	      if (!GenericValidator.isBlankOrNull(oppLastUpdatedDate)) {
	         request.setAttribute("oppLastUpdatedDate", oppLastUpdatedDate);
	      }   
	      //Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016 End 
	      ArrayList listOfTopCourses = (ArrayList)courseDetailsInfoMap.get("OC_OTHER_COURSES_POD");
	      if (listOfTopCourses != null && listOfTopCourses.size() > 0) {
	        request.setAttribute("listOfTopCourses", listOfTopCourses);
	        LabelValueBean lvBean = new LabelValueBean();
	        if (!GenericValidator.isBlankOrNull(advertiserFlag) && "TRUE".equals(advertiserFlag)) {
	          lvBean.setLabel("Other courses at this uni you might like");
	        } else {
	          lvBean.setLabel("Other courses you might like");
	        }
	        lvBean.setValue("div_top_courses");
	        sideBarList.add(lvBean);
	      }
	     
	      
	      String qualificationCode = (String) courseDetailsInfoMap.get("P_QUALIFICATION_CODE");
	      qualificationCode = StringUtils.isNotBlank(qualificationCode) ? qualificationCode : "";
	    	
	      String switchQualType = new SeoUrls().getQualificationDesc(qualificationCode);
	      if(StringUtils.isNotBlank(switchQualType)){
	        model.addAttribute("switchQualType", switchQualType);
	      }
	          
	      String subjectName = (String) courseDetailsInfoMap.get("P_SUBJECT_NAME");
	      subjectName = StringUtils.isNotBlank(subjectName) ? subjectName : "";
	      request.setAttribute("switchSubName", subjectName);
	      String collegeName = "";
	      if (GenericValidator.isBlankOrNull(ajaxFlag)) {	
	        collegeName = urlArray[2];
	      }
	        String viewAllCoursesLink = new SeoUrls().constructCourseUrl(qualificationCode, GlobalConstants.CLEARING_USER_TYPE_VAL, collegeName, subjectName, "");
	        model.addAttribute("viewAllCourses", viewAllCoursesLink);
	      model.addAttribute("switchclearingPrUrl", viewAllCoursesLink);
	      String clearingSwitchFlag = (String) courseDetailsInfoMap.get("P_CLEARING_SWITCH_FLAG");
	      if(StringUtils.isNotBlank(clearingSwitchFlag)){
	    	request.setAttribute("SHOW_SWITCH_FLAG", clearingSwitchFlag); 
	      }
	      
	      //
	      String cdOpportunityId = (String)courseDetailsInfoMap.get("O_OPP_ID");
	      request.setAttribute("cdOpportunityId", cdOpportunityId);
	      String cdOpportunityText = (String)courseDetailsInfoMap.get("O_OPP_DET");
	      request.setAttribute("cdOpportunityText", cdOpportunityText);
	      request.setAttribute("sideBarList", sideBarList);
	      String checkApplyFlag = (String)courseDetailsInfoMap.get("p_show_apply_now_button");
	      if(!GenericValidator.isBlankOrNull(checkApplyFlag)){
	        request.setAttribute("checkApplyFlag", checkApplyFlag);
	        //System.out.println( "apply flag----------------------" + checkApplyFlag);
	      }
	      //fetch bread crumb        
	      if (request.getSession().getAttribute("bredqual") != null) {
	        String qual = (String)request.getSession().getAttribute("bredqual");
	        String catId = (String)request.getSession().getAttribute("bredcatcode");
	        String bredLoc = (String)request.getSession().getAttribute("bredlocation");
	        String cdBrcumb = "BROWSE_COURSE_DETAILS";
	        if ("clcdetail".equalsIgnoreCase(pageName)) {
	          cdBrcumb = "CLEARING_BROWSE_COURSE_DETAILS";
	        } 
	        String breadCrumb = new SeoUtilities().getBreadCrumb(cdBrcumb, qual, catId, "", bredLoc, courseId);
	        if (breadCrumb != null && breadCrumb.trim().length() > 0) {
	          request.setAttribute("breadCrumb", breadCrumb);
	        }
	      }
	    }
	    String cpeQualificationNetworkId = (String)session.getAttribute("cpeQualificationNetworkId");
	    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
	      cpeQualificationNetworkId = "2";
	    } 
	    //Added by Indumathi.S Mar_31_2016
	    if ("M".equalsIgnoreCase(studyLevelCode) || "N".equalsIgnoreCase(studyLevelCode) || "A".equalsIgnoreCase(studyLevelCode) || "T".equalsIgnoreCase(studyLevelCode)) {
	      cpeQualificationNetworkId = "2";
	    } else { //L or postgraduate-courses
	      cpeQualificationNetworkId = "3";
	    }
	      //request.setAttribute("networkidtr",cpeQualificationNetworkId);
	    //End
	    //Added by Prabha for showing message on part time course details on 11_Jul_2017
	    //Commented the code for JIRA 199 by Kailash on 02_June_2020
//	    String partTimeMsgFlag = (String)courseDetailsInfoMap.get("p_part_time_msg_flag"); 
//	    if(!GenericValidator.isBlankOrNull(partTimeMsgFlag)){
//	      request.setAttribute("partTimeMsgFlag", partTimeMsgFlag);
//	    }
	    //End of paer time message showing
	    String ldcsSubject = (String)courseDetailsInfoMap.get("p_ldcs_descriptions");
	    if(!GenericValidator.isBlankOrNull(ldcsSubject)){
	      request.setAttribute("ldcsSubject", ldcsSubject);
	    }
	    session.removeAttribute("cpeQualificationNetworkId");
	    session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
	    if(cpeQualificationNetworkId == "3"){
	     request.setAttribute("pgCourse","Yes");
	    }
	    //String isRichProfile = new CommonFunction().isRichProfileProv(collegeId, "RICH_INST_PROFILE", null, cpeQualificationNetworkId);
	    String profileType = common.getProfileType(collegeId, null, cpeQualificationNetworkId);
	    if (!"clcdetail".equalsIgnoreCase(pageName)) {
	      if (!"".equals(profileType) && ("RICH_INST_PROFILE".equalsIgnoreCase(profileType) || "CH_INST_PROFILE".equalsIgnoreCase(profileType))) {
	        request.setAttribute("isRichProfile", "true");
	      }
	    }
	    if(!GenericValidator.isBlankOrNull(collegeId)){
	      request.setAttribute("cDimCollegeIds", ("'" + collegeId.trim() + "'"));
	    } 
	    
	    model.addAttribute("domainPathWithImgSrc", domainPathWithImgSrc);
	    model.addAttribute("domainPathWithoutImgSrc", domainPathWithoutImgSrc);
	    model.addAttribute("mobileFlag", mobileFlag);
	    
	    if (("Y").equals(ajaxFlag)) {
	     return new ModelAndView("ajaxkisdata");
	    }
        	    
         return new ModelAndView("courseDetailsPage");
	  }

	  /**
	    *   This function is used to load similar categories for the given courseid.
	    * @param courseId
	    * @param request
	    * @return none.
	    */
	  private void loadSimilarCategoryPod(String courseId, HttpServletRequest request) {
	    ResultSet resultSet = null;
	    Vector vector = new Vector();
	    DataModel dataModel = new DataModel();
	    try {
	      String studyLevel = "";
	      String categoryCode = "";
	      String location = "";
	      vector.add(courseId);
	      resultSet = dataModel.getResultSet("Wu_Course_Search_Pkg.get_course_br_cat", vector);
	      while (resultSet.next()) {
	        studyLevel = resultSet.getString("study_level");
	        categoryCode = resultSet.getString("category_code");
	        location = resultSet.getString("location");
	      }
	      if ((categoryCode != null && !categoryCode.equalsIgnoreCase("null") && categoryCode.trim().length() > 0) && (location != null && !location.equalsIgnoreCase("null") && location.trim().length() > 0)) {
	        String subjectId = new CommonFunction().getJourneySubjectCategoryCode(new CommonUtil().toUpperCase(categoryCode), new CommonUtil().toUpperCase(studyLevel));
	        List subCategoryList = new GlobalFunction().getReleatedSubCategoryList(subjectId, (location != null && location.trim().length() > 0 ? new CommonUtil().toUpperCase(location) : ""), request, studyLevel);
	        if (subCategoryList != null && subCategoryList.size() > 0) {
	          request.setAttribute("subCategoryList", subCategoryList);
	        }
	      }
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    } finally {
	      dataModel.closeCursor(resultSet);
	    }
	  }
		
	
	
}

