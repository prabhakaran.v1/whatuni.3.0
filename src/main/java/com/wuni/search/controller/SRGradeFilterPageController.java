package com.wuni.search.controller;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import WUI.whatunigo.bean.GradeFilterBean;
import com.config.SpringContextInjector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.whatunigo.GradeFilterVO;
import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : SRGradeFilterPageAction.java
 * Description   : Action to get the grade filter page details from the SR page
 * @version      : 1.0
 * @author       : Jeyalakshmi.D
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Jeyalakshmi.D                  1.0              22.10.2019            Initial Draft
 * Sujitha V                      2.0              07.12.2019            Grade filter URL formation change
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
@Controller
public class SRGradeFilterPageController{
	
  @RequestMapping(value = "/uni-course-requirements-match", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getSRGradeFilter(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String forwardPage = "sr-grade-filter-page";
    String action = request.getParameter("action");
    CommonUtil commonUtil = new CommonUtil();
    GradeFilterBean gradeFilterBean = new GradeFilterBean();
    String subject = !GenericValidator.isBlankOrNull(request.getParameter("subject")) ? request.getParameter("subject") : "";
    String q = !GenericValidator.isBlankOrNull(request.getParameter("q")) ? request.getParameter("q") : "";
    String university = !GenericValidator.isBlankOrNull(request.getParameter("university")) ? request.getParameter("university") : "";
    String location = !GenericValidator.isBlankOrNull(request.getParameter("location")) ? request.getParameter("location") : "";
    String postCode = !GenericValidator.isBlankOrNull(request.getParameter("postcode")) ? request.getParameter("postcode") : "";
    String studyMode = !GenericValidator.isBlankOrNull(request.getParameter("study-mode")) ? request.getParameter("study-mode") : "";
    String pageName = !GenericValidator.isBlankOrNull(request.getParameter("page-name")) ? request.getParameter("page-name") : "";
    String ucasCode = !GenericValidator.isBlankOrNull(request.getParameter("ucas-code")) ? request.getParameter("ucas-code") : "";
    String locationType = !GenericValidator.isBlankOrNull(request.getParameter("location-type")) ? request.getParameter("location-type") : "";
    String campusType = !GenericValidator.isBlankOrNull(request.getParameter("campus-type")) ? request.getParameter("campus-type") : "";
    String russellGroup = !GenericValidator.isBlankOrNull(request.getParameter("russell-group")) ? request.getParameter("russell-group") : "";
    String module = !GenericValidator.isBlankOrNull(request.getParameter("module")) ? request.getParameter("module") : "";
    String srScore = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : "";
    if(!GenericValidator.isBlankOrNull(subject)) {
      request.setAttribute("subject",subject);
    }
    if(!GenericValidator.isBlankOrNull(q)) {
      request.setAttribute("q",q);
    }
    if(!GenericValidator.isBlankOrNull(university)) {
      request.setAttribute("university",university);
    } 
    if(!GenericValidator.isBlankOrNull(location)) {
      request.setAttribute("location", location);
    } 
    if(!GenericValidator.isBlankOrNull(postCode)) {
      request.setAttribute("postCode", postCode);
    } 
    if(!GenericValidator.isBlankOrNull(studyMode)) {
      request.setAttribute("studyMode", studyMode);
    } 
    if(!GenericValidator.isBlankOrNull(pageName)) {
      request.setAttribute("pageName", pageName);
    }    
    if(!GenericValidator.isBlankOrNull(ucasCode)) {
      request.setAttribute("ucasCode", ucasCode);
    } 
    if(!GenericValidator.isBlankOrNull(locationType)) {
      request.setAttribute("locationType", locationType);
    } 
    if(!GenericValidator.isBlankOrNull(campusType)) {
      request.setAttribute("campusType", campusType);
    } 
    if(!GenericValidator.isBlankOrNull(russellGroup)) {
      request.setAttribute("russellGroup", russellGroup);
    } 
    if(!GenericValidator.isBlankOrNull(module)) {
      request.setAttribute("module", module);
    } 
    if(!GenericValidator.isBlankOrNull(srScore)){
      request.setAttribute("srScore", srScore);	
    }
    String p_recentFilter = GenericValidator.isBlankOrNull(request.getParameter("rf")) ? "" : (request.getParameter("rf"));
    request.setAttribute("rf", p_recentFilter);
    String distance = GenericValidator.isBlankOrNull(request.getParameter("distance")) ? "25" : (request.getParameter("distance"));
    request.setAttribute("distance", distance);
    String p_emp_rate_max = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? "" : (request.getParameter("employment-rate-max"));
    String p_emp_rate_min = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? "" : (request.getParameter("employment-rate-min"));
    String empRate = (!GenericValidator.isBlankOrNull(p_emp_rate_min) ? p_emp_rate_min : "") + (!GenericValidator.isBlankOrNull(p_emp_rate_max) ? ("," + p_emp_rate_max) : "");
    request.setAttribute("empRateMax", p_emp_rate_max);
    request.setAttribute("empRateMin", p_emp_rate_min);
    request.setAttribute("empRate", empRate);
    String p_ucas_tariff_max = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max")) ? null : (request.getParameter("ucas-points-max"));
    String p_ucas_tariff_min = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min")) ? null : (request.getParameter("ucas-points-min"));
    String p_ucas_tariff = (!GenericValidator.isBlankOrNull(p_ucas_tariff_min) ? p_ucas_tariff_min : "") + (!GenericValidator.isBlankOrNull(p_ucas_tariff_max) ? ("," + p_ucas_tariff_max) : "");
    request.setAttribute("ucasTariff", p_ucas_tariff);
    String p_jacsCode = GenericValidator.isBlankOrNull(request.getParameter("jacs")) ? null : (request.getParameter("jacs"));
    String assessmentTypeFilter = !GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? (request.getParameter("assessment-type")) : "";
    String reviewCategoryFilter = !GenericValidator.isBlankOrNull(request.getParameter("your-pref")) ? (request.getParameter("your-pref")) : "";
    request.setAttribute("jacs", p_jacsCode);
    request.setAttribute("assessmentTypeFilter", assessmentTypeFilter);
    request.setAttribute("reviewCategoryFilter", reviewCategoryFilter);
    String referrer = !GenericValidator.isBlankOrNull(request.getHeader("referer")) ? request.getHeader("referer") : "";
    String refererURL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + "/"; 
    String selQual = request.getParameter("qualification");
    request.setAttribute("selected_qual", selQual);
    String scoreVal = request.getParameter("score");
    String qualCode = "M";
    String pageNameFromSR = !GenericValidator.isBlankOrNull(request.getParameter("pageName")) ? request.getParameter("pageName"): "";
    if("srGradeFilterPage".equalsIgnoreCase(pageNameFromSR)){
      String urlQual = "degree";
      if ("degree".equalsIgnoreCase(selQual)) {
        qualCode = "M";
        urlQual = "degree";
      } else if ("postgraduate".equalsIgnoreCase(selQual)) {
        qualCode = "L";
        urlQual = "postgraduate";
      } else if ("foundation-degree".equalsIgnoreCase(selQual)) {
        qualCode = "A";
        urlQual = "foundation-degree";
      } else if ("access-foundation".equalsIgnoreCase(selQual)) {
        qualCode = "T";
        urlQual = "access-foundation";
      } else if ("hnd-hnc".equalsIgnoreCase(selQual)) {
        qualCode = "N";
        urlQual = "hnd-hnc";
      } else if ("ucas_code".equalsIgnoreCase(selQual)) {
        qualCode = "UCAS_CODE";
        urlQual = "ucas_code";
      }
      request.setAttribute("qualCode", qualCode);
      request.setAttribute("score", scoreVal);
      request.setAttribute("urlQual", urlQual);
      if(!GenericValidator.isBlankOrNull(scoreVal)){
        //session.setAttribute("USER_UCAS_SCORE", scoreVal);
      }
      request.setAttribute("pageName", "srGradeFilterPage");
    }else{
  	  request.setAttribute("pageName", "mywhatuni");
    }
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    Map<String, Object> resultMap = null;
    String subjectSessionId = !GenericValidator.isBlankOrNull((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
    String userId = !GenericValidator.isBlankOrNull(new SessionData().getData(request, "y")) ? new SessionData().getData(request, "y") : "0";
    if("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
      String freeText = request.getParameter("free-text");
      Gson gson = new GsonBuilder().create();
      String responseAsString = commonUtil.getBody(request);
      gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
      Object[][] qualDetailsArr;
      ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
      if (qualSubList != null && qualSubList.size() > 0) {
        qualDetailsArr = new String[qualSubList.size()][5];
        for (int lp = 0; lp < qualSubList.size(); lp++) {
          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
        }
       gradeFilterBean.setQualDetailsArr(qualDetailsArr);
      } else{
        gradeFilterBean.setQualDetailsArr(null);
      }
      resultMap = commonBusiness.getSRSubjectlist(freeText, gradeFilterBean);
      ArrayList ajaxSubjectList = (ArrayList)resultMap.get("RetVal");
      if (ajaxSubjectList != null && ajaxSubjectList.size() > 0) {
        request.setAttribute("ajaxSubjectList", ajaxSubjectList);
      }
      forwardPage = "sr-ajax-subject-page";
    } 
    else if("CALCULATE_UCAS_POINTS".equalsIgnoreCase(action)) {
      Gson gson = new GsonBuilder().create();
      String responseAsString = commonUtil.getBody(request);
      gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
      Object[][] qualDetailsArr;
      ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
      if (qualSubList != null && qualSubList.size() > 0) {
        qualDetailsArr = new String[qualSubList.size()][5];
        for (int lp = 0; lp < qualSubList.size(); lp++) {
          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
        }
       gradeFilterBean.setQualDetailsArr(qualDetailsArr);
     } 
     resultMap = commonBusiness.getSRSubjectUcasPoints(gradeFilterBean);
     String ucasPoints = (String)resultMap.get("UCAS_POINTS");
     if(ucasPoints != null) {
       request.setAttribute("ucasPoints", ucasPoints);
     }
     String returnString = "";
     StringBuffer buffer = new StringBuffer();
     response.setContentType("text/html");
     response.setHeader("Cache-Control", "no-cache");
     returnString = "UCAS_POINT";
     returnString += "##SPLIT##" + ucasPoints;
     buffer.append(returnString);
     setResponseText(response, buffer);
     request.setAttribute("QualSubList", gradeFilterBean.getQualDetailsArr());
     return null;  
   }
   else if("QUAL_MATCHED_COURSE".equalsIgnoreCase(action)) {
     //String ucasPoints = request.getParameter("ucasPoints");
     gradeFilterBean.setSubjectName((String)request.getParameter("subject"));
     gradeFilterBean.setKeyword((String)request.getParameter("q"));
     //gradeFilterBean.setGradePoints(ucasPoints);
     gradeFilterBean.setCollegeName((String)request.getParameter("university"));
     gradeFilterBean.setQualificationCode(!GenericValidator.isBlankOrNull((String)request.getParameter("qualCode")) ? request.getParameter("qualCode") : (String)request.getAttribute("qualCode"));
     gradeFilterBean.setLocation(request.getParameter("location"));
     //
      gradeFilterBean.setSearchRange(distance);
      p_emp_rate_max = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? "" : (request.getParameter("employment-rate-max"));
      p_emp_rate_min = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? "" : (request.getParameter("employment-rate-min"));
      empRate = (!GenericValidator.isBlankOrNull(p_emp_rate_min) ? p_emp_rate_min : "") + (!GenericValidator.isBlankOrNull(p_emp_rate_max) ? ("," + p_emp_rate_max) : "");
      gradeFilterBean.setPageName(!GenericValidator.isBlankOrNull((String)request.getParameter("page-name")) ? request.getParameter("page-name") : (String)request.getAttribute("pageName"));
      gradeFilterBean.setStudyMode(!GenericValidator.isBlankOrNull((String)request.getParameter("study-mode")) ? request.getParameter("study-mode") : (String)request.getAttribute("studyMode"));
      gradeFilterBean.setPostcode(!GenericValidator.isBlankOrNull((String)request.getParameter("postcode")) ? request.getParameter("postcode") : (String)request.getAttribute("postCode"));
      gradeFilterBean.setUnivLocTypeName(!GenericValidator.isBlankOrNull((String)request.getParameter("location-type")) ? request.getParameter("location-type") : (String)request.getAttribute("locationType"));
      gradeFilterBean.setCampusTypeName(!GenericValidator.isBlankOrNull((String)request.getParameter("campus-type")) ? request.getParameter("campus-type") : (String)request.getAttribute("campusType"));
      gradeFilterBean.setRusselGroupName(!GenericValidator.isBlankOrNull((String)request.getParameter("russell-group")) ? request.getParameter("russell-group") : (String)request.getAttribute("russellGroup"));
      gradeFilterBean.setEmpRate(empRate);
      gradeFilterBean.setUcasCode(!GenericValidator.isBlankOrNull((String)request.getParameter("ucas-code")) ? request.getParameter("ucas-code") : (String)request.getAttribute("ucasCode"));
      gradeFilterBean.setUcasTariff(!GenericValidator.isBlankOrNull((String)request.getParameter("ucasPoints")) ? request.getParameter("ucasPoints") : (String)request.getAttribute("ucasPoints"));
      gradeFilterBean.setJacsCode(!GenericValidator.isBlankOrNull((String)request.getParameter("jacs")) ? request.getParameter("jacs") : (String)request.getAttribute("jacs"));
      gradeFilterBean.setAssessmentType(!GenericValidator.isBlankOrNull((String)request.getParameter("assessment-type")) ? request.getParameter("assessment-type") : (String)request.getAttribute("assessmentTypeFilter"));
      gradeFilterBean.setReviewSubjects(!GenericValidator.isBlankOrNull((String)request.getParameter("your-pref")) ? request.getParameter("your-pref") : (String)request.getAttribute("reviewCategoryFilter"));
     gradeFilterBean.setUserUcasScore(request.getParameter("userUcasScore"));
     gradeFilterBean.setModuleStr(!GenericValidator.isBlankOrNull((String)request.getParameter("module")) ? request.getParameter("module") : (String)request.getAttribute("module"));
     scoreVal =  request.getParameter("userUcasScore");
     if(!GenericValidator.isBlankOrNull(scoreVal)){
       session.setAttribute("USER_UCAS_SCORE", scoreVal);
     }
     resultMap = commonBusiness.getSRMatchingCourse(gradeFilterBean);
     String courseCount = (String)resultMap.get("P_COURSE_COUNT");
     String returnString = "";
     StringBuffer buffer = new StringBuffer();
     response.setContentType("text/html");
     response.setHeader("Cache-Control", "no-cache");
     returnString = "COURSE_COUNT";
     returnString += "##SPLIT##" + courseCount;
     buffer.append(returnString);
     setResponseText(response, buffer);
     return null;
   }
   else if("SAVE_REDIRECT".equalsIgnoreCase(action)) {
     String qualSubListtt = (String)request.getAttribute("QualSubList");
     Gson gson = new GsonBuilder().create();
     String responseAsString = commonUtil.getBody(request);
     gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
     Object[][] qualDetailsArr;
     ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
     if (qualSubList != null && qualSubList.size() > 0) {
       qualDetailsArr = new String[qualSubList.size()][5];
       for (int lp = 0; lp < qualSubList.size(); lp++) {
         GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
         qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
         qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
         qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
         qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
         qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
       }
       gradeFilterBean.setQualDetailsArr(qualDetailsArr);
     }
     gradeFilterBean.setUserId(userId);
     gradeFilterBean.setSessionId(subjectSessionId);
     resultMap = commonBusiness.saveSRQualGradeFilter(gradeFilterBean);     
     String userUCASPoint = (String)resultMap.get("P_USER_UCAS_POINTS");
     if(!GenericValidator.isBlankOrNull(userUCASPoint)){       
       session.setAttribute("USER_UCAS_SCORE", userUCASPoint);
     }
     return null;
   } else if("DELETE_RECORD".equalsIgnoreCase(action)) {
     gradeFilterBean.setUserId(userId);
     gradeFilterBean.setSessionId(subjectSessionId);
     
     if (session.getAttribute("USER_UCAS_SCORE") != null) {
       session.removeAttribute("USER_UCAS_SCORE");
       //System.out.println("removed session"+session.getAttribute("USER_UCAS_SCORE"));
     }
     resultMap = commonBusiness.deleteUserRecord(gradeFilterBean);
     String RetVal = (String)resultMap.get("RetVal");
     if(RetVal != null) {
     }
     String returnString = "";
     StringBuffer buffer = new StringBuffer();
     response.setContentType("text/html");
     response.setHeader("Cache-Control", "no-cache");
     returnString = "RetVal";
     returnString += "##SPLIT##" + RetVal;
     buffer.append(returnString);
     setResponseText(response, buffer);
     return null;  
   } else {
     gradeFilterBean.setUserId(userId);
     gradeFilterBean.setSessionId(subjectSessionId);
     gradeFilterBean.setSubjectName((String)request.getParameter("subject"));
     gradeFilterBean.setKeyword((String)request.getParameter("q"));
     gradeFilterBean.setCollegeName((String)request.getParameter("university"));
     gradeFilterBean.setQualificationCode(!GenericValidator.isBlankOrNull((String)request.getParameter("qualCode")) ? request.getParameter("qualCode") : (String)request.getAttribute("qualCode"));
     gradeFilterBean.setLocation(!GenericValidator.isBlankOrNull((String)request.getParameter("location")) ? request.getParameter("location") : (String)request.getAttribute("location"));
     //
     gradeFilterBean.setSearchRange(distance);
     gradeFilterBean.setPageName(!GenericValidator.isBlankOrNull((String)request.getParameter("page-name")) ? request.getParameter("page-name") : (String)request.getAttribute("pageName"));
     gradeFilterBean.setStudyMode(!GenericValidator.isBlankOrNull((String)request.getParameter("study-mode")) ? request.getParameter("study-mode") : (String)request.getAttribute("studyMode"));
     gradeFilterBean.setPostcode(!GenericValidator.isBlankOrNull((String)request.getParameter("postcode")) ? request.getParameter("postcode") : (String)request.getAttribute("postCode"));
     gradeFilterBean.setUnivLocTypeName(!GenericValidator.isBlankOrNull((String)request.getParameter("location-type")) ? request.getParameter("location-type") : (String)request.getAttribute("locationType"));
     gradeFilterBean.setCampusTypeName(!GenericValidator.isBlankOrNull((String)request.getParameter("campus-type")) ? request.getParameter("campus-type") : (String)request.getAttribute("campusType"));
     gradeFilterBean.setRusselGroupName(!GenericValidator.isBlankOrNull((String)request.getParameter("russell-group")) ? request.getParameter("russell-group") : (String)request.getAttribute("russellGroup"));
     gradeFilterBean.setEmpRate(!GenericValidator.isBlankOrNull((String)request.getParameter("empRate")) ? request.getParameter("empRate") : (String)request.getAttribute("empRate"));
     gradeFilterBean.setUcasCode(!GenericValidator.isBlankOrNull((String)request.getParameter("ucas-code")) ? request.getParameter("ucas-code") : (String)request.getAttribute("ucasCode"));
     gradeFilterBean.setUcasTariff(!GenericValidator.isBlankOrNull((String)request.getParameter("ucasTariff")) ? request.getParameter("ucasTariff") : (String)request.getAttribute("ucasTariff"));
     gradeFilterBean.setJacsCode(!GenericValidator.isBlankOrNull((String)request.getParameter("jacs")) ? request.getParameter("jacs") : (String)request.getAttribute("jacs"));
     gradeFilterBean.setAssessmentType(!GenericValidator.isBlankOrNull((String)request.getParameter("assessment-type")) ? request.getParameter("assessment-type") : (String)request.getAttribute("assessmentTypeFilter"));
     gradeFilterBean.setReviewSubjects(!GenericValidator.isBlankOrNull((String)request.getParameter("your-pref")) ? request.getParameter("your-pref") : (String)request.getAttribute("reviewCategoryFilter"));
     gradeFilterBean.setModuleStr(!GenericValidator.isBlankOrNull((String)request.getParameter("module")) ? request.getParameter("module") : (String)request.getAttribute("module"));
     resultMap = commonBusiness.getSRGradeFilterPage(gradeFilterBean);
     ArrayList<GradeFilterVO> qualList = (ArrayList<GradeFilterVO>)resultMap.get("PC_QUALIFICATION_LIST");
     ArrayList<UserQualDetailsVO> userQualList = (ArrayList<UserQualDetailsVO>)resultMap.get("PC_USER_QUALIFICATIONS");
     String ucasPoint = (String)resultMap.get("P_UCAS_POINTS");
     subjectSessionId = (String)resultMap.get("P_SESSION_ID");
     String ucasMaxPoints = (String)resultMap.get("P_MAX_UCAS_POINTS");
     session.setAttribute("subjectSessionId" , subjectSessionId);
     if (qualList != null && qualList.size() > 0) {
       request.setAttribute("qualificationList", qualList);
     }
     if (userQualList != null && userQualList.size() > 0) {
       request.setAttribute("userQualList", userQualList);
       request.setAttribute("userQualSize", String.valueOf(userQualList.size()));
     }
     if(!GenericValidator.isBlankOrNull(ucasPoint)) {
       request.setAttribute("ucasPoint", ucasPoint);
     }
     if(!GenericValidator.isBlankOrNull(ucasMaxPoints)) {
       request.setAttribute("ucasMaxPoints", ucasMaxPoints);
     }
     if(userQualList != null && userQualList.size() == 0) {
       forwardPage = "sr-new-user-grade-page";
     }
     else if(userQualList != null && userQualList.size() > 0) {
       forwardPage = "sr-grade-filter-page";
     }
   } 
   return new ModelAndView(forwardPage);
  }
  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
}
