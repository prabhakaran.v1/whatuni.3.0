package com.wuni.search.controller;

import java.sql.Clob;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;

@Controller
public class NotFoundController {

	@RequestMapping(value = "/notfound", method = RequestMethod.GET)
	public ModelAndView getNotFoundPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    session = request.getSession();
	    String subjectname = (String)session.getAttribute("gamKeywordOrSubject");
	    subjectname = subjectname != null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length() > 0? new CommonUtil().toTitleCase(subjectname): "";
	    String studylevelid = (String)session.getAttribute("FaqStudyLevel");
	    studylevelid = studylevelid != null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length() > 0? studylevelid: "";
	    String newstudylevelid = (String)session.getAttribute("err_study_level_id");
	    newstudylevelid = newstudylevelid != null && !newstudylevelid.equalsIgnoreCase("null") && newstudylevelid.trim().length() > 0? newstudylevelid: "";
	    String studylevelname = (String)session.getAttribute("gamStudyLevelDesc");
	    studylevelname = studylevelname != null && !studylevelname.equalsIgnoreCase("null") && studylevelname.trim().length() > 0? new CommonUtil().toLowerCase(studylevelname): "";
	    String location = (String)session.getAttribute("err_location");
	    location = location != null && !location.equalsIgnoreCase("null") && location.trim().length() > 0? new CommonFunction().replacePlus(location.trim()): "";
	    String providername = (String)session.getAttribute("err_provider_name");
	    providername = providername != null && !providername.equalsIgnoreCase("null") && providername.trim().length() > 0? providername.trim(): "";
	    String collegeId = (String)session.getAttribute("collegeId");
	    collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId: "0";
	    String noindexfollow = request.getAttribute("noindex") != null? "TRUE": "FALSE";
	    noindexfollow = request.getAttribute("unihomefrmsearch") != null? "noindex,nofollow": noindexfollow;
	    noindexfollow = session.getAttribute("otherindex") != null? String.valueOf(session.getAttribute("otherindex")): noindexfollow;
	    String categoryCode = session.getAttribute("FaqCategoryCode") != null? session.getAttribute("FaqCategoryCode").toString(): "";
	    String clSearchBarForNoResults = (String)session.getAttribute("clearingSearchBar");
	    String providerResult = (String)session.getAttribute("providerResult");
	    String articleKeyword = session.getAttribute("keyword") != null? session.getAttribute("keyword").toString(): "";
	    request.setAttribute("err_subject_name", subjectname);
	    request.setAttribute("err_study_level_id", studylevelid);
	    request.setAttribute("err_study_level_name", studylevelname);
	    request.setAttribute("err_location", location);
	    request.setAttribute("err_provider_name", providername);
	    request.setAttribute("collegeId", collegeId);
	    request.setAttribute("categoryCode", categoryCode);
	    request.setAttribute("providerResult", providerResult);
	    request.setAttribute("otherindex", noindexfollow);
	    request.setAttribute("clearingSearchBar", clSearchBarForNoResults);
	    request.setAttribute("articleKeyword", articleKeyword);
	    String pageName = "";
	    String lOneLevelList = null;
	    lOneLevelList = getMegamenuDynamicContent(newstudylevelid);
	    session.removeAttribute("err_location");
	    session.removeAttribute("collegeId");
	    session.removeAttribute("err_provider_name");
	    session.removeAttribute("clearingSearchBar");
	    session.removeAttribute("providerResult");
	    session.removeAttribute("articleKeyword");
	    if (!GenericValidator.isBlankOrNull(lOneLevelList)) {
	      request.setAttribute("lOneLevelHtmlContent", lOneLevelList);
	    }
	    return new ModelAndView("notfound");
	}
	
	  /**
     * This function will return the Mega menu dynamic contents..
     */
  public String getMegamenuDynamicContent(String newstudylevelid) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    Map getMegamenumap = commonBusiness.getMegaMenuDegreesList(newstudylevelid);
    Clob clob = (Clob)getMegamenumap.get("o_megamenu");
    String htmlMegamenu = new String();
    htmlMegamenu = CommonFunction.getClobValue(clob);
    return htmlMegamenu;
  }
}
