package com.wuni.search.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.SessionData;

@Controller
public class GradeFilterController {


	  @RequestMapping(value = "/mobile-grade-popup", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView mobileGradePopup(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		    String user_Id = new SessionData().getData(request, "y");
		    SearchController searchObject = new SearchController();
		    if (!GenericValidator.isBlankOrNull(user_Id)) {
		      searchObject.getMyGradesValue(user_Id, request); // Retaining user grades
		    }
		    String urlString = request.getParameter("urlString");
		    if (!GenericValidator.isBlankOrNull(urlString)) {
		      String urlArray[] = urlString.split("/");
		      if (urlArray.length > 4) {
		        String entryLevel = urlArray[urlArray.length - 3];
		        String entry_point[] = urlArray[urlArray.length - 2].split("-");
		        searchObject.setGrades(null, entryLevel, entryLevel, entry_point, request); // Retaining the selected grade
		      }
		    }
		    request.setAttribute("paramStudyLevelId", request.getParameter("studyLevelId"));
		    request.setAttribute("fromGradePopup", "found");
		    return new ModelAndView("gradeFilterPopup");	  
	  }
	
}
