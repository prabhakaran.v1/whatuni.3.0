package com.wuni.search.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mobile.valueobject.SearchVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import spring.controller.clearing.ClearingProviderResultController;
import spring.pojo.clearing.provider.ProviderResultsRequest;
import spring.valueobject.search.ProviderResultPageVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.seo.SEOMetaDetailsVO;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.valueobject.review.UserReviewsVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.search.form.SearchBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SearchUtilities;
import WUI.utilities.SessionData;
/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : ProviderResutlsController.java
* Description   :This controller is for the provider result page
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	  Modified On     	Modification Details 	             Change
* ************************************************************************************************************************************** 
* Sangeeth.S              		  29-04-2020       Modified                 Handled open day event type changes for dimension 18 and GA
* Sujitha V                       30-09-2020       Modified                 Removed one condition for clearing switch off redirect(before redirecting only to degree-courses)
* Sri Sankari             1.3     20-10-2020       Modified                 Added dynamic meta details from Back office
* */
@Controller
public class ProviderResutlsController {
  
  @Autowired
  ClearingProviderResultController clearingProviderResultController = null;

	@RequestMapping(value = "/*-courses/csearch", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView getProviderResultsPage(@RequestParam Map<String, String> requestParam, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		//
		final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
		final String URL_QUR_STRING = request.getQueryString();
		SearchBean uniSelectBean = new SearchBean();
		CommonFunction common = new CommonFunction();
		String prsearchUrl = "";
		prsearchUrl = URL_STRING + (GenericValidator.isBlankOrNull(URL_QUR_STRING) ? "" : ("?" + URL_QUR_STRING)); // 21_JUL_2015_REL Added by Priyaa for Course search logging
		// Added clearing url redirection when clearing is OFF for OCT_23_18 rel by Sangeeth.S
		String clearingOnOff = common.getWUSysVarValue("CLEARING_ON_OFF");		
		if (GlobalConstants.OFF.equalsIgnoreCase(clearingOnOff) && !GenericValidator.isBlankOrNull(URL_QUR_STRING) && StringUtils.indexOf(URL_QUR_STRING.toLowerCase(), GlobalConstants.SEO_PATH_CLEARING) > -1) {
		  String tempRedirectUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + prsearchUrl;
		  tempRedirectUrl = tempRedirectUrl.replace((GlobalConstants.SEO_PATH_CLEARING + "&"), "");
		  response.setStatus(response.SC_MOVED_TEMPORARILY);
		  response.sendRedirect(tempRedirectUrl);
		  return null;
		}
		//
		//Call different Action if it is come from clearing
		ProviderResultsRequest providerResultsInputBean = new ProviderResultsRequest();
		if(URL_QUR_STRING.contains("clearing")) {
			return clearingProviderResultController.providerResultsLanding(requestParam, model, providerResultsInputBean, request, response, session);
		}
		request.setAttribute("queryStr", URL_QUR_STRING);
		Long startActionTime = new Long(System.currentTimeMillis());
		ServletContext context = request.getServletContext();
		session = request.getSession();
		CommonUtil comUtil = new CommonUtil();
		SearchUtilities searchUtil = new SearchUtilities();
		
		String courseName = "";
		String categoryCode = "";
		String studyMode = "";
		String studyLevel = "";
		String location = "";
		String headerId = "";
		String entityText = "1";
		String collegeName = "";
		String collegeNameDisplay = "";
		String pagename = "csearch";
		String courseMappingPath = "csearch.html";
		String ucasCode = "";
		String collegeId = "";
		String jacsSubjectName = "";
		String URL_1 = "";
		String URL_2 = "";
		String seoFirstPageUrl = "";
		if (!GenericValidator.isBlankOrNull(URL_QUR_STRING) && (URL_QUR_STRING.contains("clearing"))) { // 30_JUN_2015_REL
			pagename = "clcsearch";
			request.setAttribute("searchClearing", "TRUE");
		}		
		boolean viewallcourses = false;
		boolean mysearchurlflag = true;
		boolean searchResultFlag = false;
		GlobalFunction globalFn = new GlobalFunction();				
		if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) {
		  return new ModelAndView("forward: /userLogin.html");
		}
		//
		request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com" + URL_STRING + "?" + URL_QUR_STRING));// Added getSchemeName by Indumathi Mar-29-16																
		request.setAttribute("desktopURL",(request.getContextPath() + request.getRequestURI().substring(request.getContextPath().length()) + ".html")); // 9_DEC_14 Added by Amir for switch to mobile site
		request.setAttribute("show_course_tab", "uni-view");
		try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
			if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
				String fromUrl = "/home.html";
				session.setAttribute("fromUrl", fromUrl);
				return new ModelAndView("loginPage");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		common.loadUserLoggedInformation(request, context, response);
		globalFn.removeSessionData(session, request, response);
		ArrayList courseList = null;
		String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
		String[] urlArray = urlString.split("/");
		//
		String requestUrl = CommonUtil.getRequestedURL(request);// Change the getRequestURL by Prabha on 28_Jun_2016
		String mappingRequestURL = !GenericValidator.isBlankOrNull(requestUrl) ? requestUrl : "";
		if (mappingRequestURL != null && mappingRequestURL.indexOf("rsearch.html") >= 0) { // Added for 25th August 2009 release
			courseMappingPath = "rsearch.html";
			request.setAttribute("courseMappingPath", "rsearch.html");
		}
		// Removed old url array code block and added code to get parameter
		// values for 27_Jan_2016, By Thiyagu G. Start
		String p_q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
		String searchKeyword = GenericValidator.isBlankOrNull(p_q) ? "" : p_q;
		String p_subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));
		searchKeyword = GenericValidator.isBlankOrNull(p_subject) ? "" : p_subject;
		model.addAttribute("subjectName", searchKeyword);
		String p_university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
		model.addAttribute("instNames", p_university);
		String searchWhat = "Z"; // SEARCH TYPE Z-COLLEGE O-COURSES
		String searchHow = !GenericValidator.isBlankOrNull(request.getParameter("sort")) ? request.getParameter("sort") : "R";
		String p_module_str = GenericValidator.isBlankOrNull(request.getParameter("module")) ? null : (request.getParameter("module"));
		String p_postCode = GenericValidator.isBlankOrNull(request.getParameter("postcode")) ? null : (request.getParameter("postcode"));
		String p_distance = GenericValidator.isBlankOrNull(request.getParameter("distance")) ? "25" : (request.getParameter("distance"));
		String p_loc_type = GenericValidator.isBlankOrNull(request.getParameter("location-type")) ? null : (request.getParameter("location-type"));
		location = GenericValidator.isBlankOrNull(request.getParameter("location")) ? "UNITED KINGDOM" : (request.getParameter("location"));
		String p_campus_type = GenericValidator.isBlankOrNull(request.getParameter("campus-type")) ? null : (request.getParameter("campus-type"));
		String p_study_mode = GenericValidator.isBlankOrNull(request.getParameter("study-mode")) ? null : (request.getParameter("study-mode"));
		String p_emp_rate_max = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? null : (request.getParameter("employment-rate-max"));
		String p_emp_rate_min = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? null : (request.getParameter("employment-rate-min"));
		String p_emp_rate = (!GenericValidator.isBlankOrNull(p_emp_rate_min) ? p_emp_rate_min: "") + (!GenericValidator.isBlankOrNull(p_emp_rate_max) ? ("," + p_emp_rate_max) : "");
		String p_russel = GenericValidator.isBlankOrNull(request.getParameter("russell-group")) ? null : (request.getParameter("russell-group"));
		String p_ucas_tariff_max = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max")) ? null : (request.getParameter("ucas-points-max"));
		String p_ucas_tariff_min = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min")) ? null : (request.getParameter("ucas-points-min"));
		String p_ucas_tariff = (!GenericValidator.isBlankOrNull(p_ucas_tariff_min) ? p_ucas_tariff_min : "") + (!GenericValidator.isBlankOrNull(p_ucas_tariff_max) ? ("," + p_ucas_tariff_max) : "");
		String p_jacsCode = GenericValidator.isBlankOrNull(request.getParameter("jacs")) ? null : (request.getParameter("jacs"));
		String scoreValue = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : ""; 
		// Added this for assessment filter but not used may be future
		// 23_Aug_2018, By Sabapathi.S
		String assessmentType = GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? null : (request.getParameter("assessment-type"));
		String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level")) ? "" : (request.getParameter("entry-level").replaceAll("-", "_"));
		String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points")) ? "" : (request.getParameter("entry-points"));
		ucasCode = !GenericValidator.isBlankOrNull(request.getParameter("ucas-code")) ? (request.getParameter("ucas-code")) : "";
		String pageNo = !GenericValidator.isBlankOrNull(request.getParameter("pageno")) ? (request.getParameter("pageno")) : "1";
		// Removed old url array code block and added code to get parameter
		// values for 27_Jan_2016, By Thiyagu G. End
		if ("".equals(ucasCode)) {
			if ("".equals(p_subject) && !"".equals(p_university) && GenericValidator.isBlankOrNull(p_jacsCode) && GenericValidator.isBlankOrNull(p_q)) {			
				searchWhat = "A";
				searchHow = !GenericValidator.isBlankOrNull(request.getParameter("sort")) ? request.getParameter("sort") : "TA";
			}
		}
		ICommonBusiness commonBusiness = (ICommonBusiness) new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		// To get the new college details By Thiyagu G on 24_jan_2017
		String providerId = "";
		String providerName = "";
		ArrayList newCollegeList = null;
		SearchVO inputVO = new SearchVO();
		inputVO.setUrlProviderName(p_university);
		Map rsMap = commonBusiness.getNewCollegeDetails(inputVO);
		if (rsMap != null && !rsMap.isEmpty()) {
			newCollegeList = (ArrayList) rsMap.get("p_college_details");
			if (!CollectionUtils.isEmpty(newCollegeList)) {
				Iterator csItr = newCollegeList.iterator();
				while (csItr.hasNext()) {
					SearchVO searchVO = (SearchVO) csItr.next();
					if (!GenericValidator.isBlankOrNull(searchVO.getNewCollegeName()) && !GenericValidator.isBlankOrNull(searchVO.getCollegeId())) {
						providerId = searchVO.getCollegeId();
						providerName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(searchVO.getNewCollegeName().trim().toLowerCase())));
					}
				}
				if (!GenericValidator.isBlankOrNull(p_university) && !GenericValidator.isBlankOrNull(providerName)) {
					if (!p_university.equals(providerName)) {
						String separator = "/";
						String newURL = separator + urlArray[1] + separator + urlArray[2];
						String qryReformatUrl = new SearchUtilities().constructProviderNameURLRewrite(request,providerName);
						if (!GenericValidator.isBlankOrNull(qryReformatUrl)) {
							newURL = newURL + qryReformatUrl;
						}
						response.setStatus(response.SC_MOVED_PERMANENTLY);
						response.setHeader("Location", newURL);
						response.setHeader("Connection", "close");
						return null;
					}
				}
			}
		}
		//Added by Jeyalakshmi.D for the delete filter ucas score generated through grade filter page in DEC_17_2019_rel 
	    if(!GenericValidator.isBlankOrNull(scoreValue)){
	      request.setAttribute("scoreValueGradeFilter", "YES");
	      request.setAttribute("gradeFilterUcasScorePoint", scoreValue);
	    }
		int found = -1;
		if (urlString != null) {
			found = urlString.indexOf("-courses");
		}
		if (found != -1) {
			if (TimeTrackingConstants.URL_TRACKING_FLAG) {
				new AdminUtilities().printUrlDetails(this.getClass().toString(), urlString, urlArray);
			}
			if (collegeId != null) {
				collegeId = collegeId.trim();
			}
		}
		URL_1 = URL_STRING;
		seoFirstPageUrl = URL_1 + "?" + URL_QUR_STRING;
		request.setAttribute("searchUrl", seoFirstPageUrl);
		request.setAttribute("sortingUrl", seoFirstPageUrl);
		request.setAttribute("SEO_FIRST_PAGE_URL", seoFirstPageUrl);
		try {
			// / GETS EXEXUTED WHEN USER CLICK THE SEARCH BUTTON FROM UNI
			// LANDING SEARCH PAGE //
			/*
			 * GETS ALL THE VALUES FROM BEAN AND SETS TO THE VARIABLE FOR
			 * GETTING SEARCH LIST
			 */
			String selQual = null;
			if (!GenericValidator.isBlankOrNull(urlArray[1])) {
				selQual = urlArray[1].substring(0,urlArray[1].indexOf("-courses"));
			}
			String qualCode = "M";
			if ("degree".equals(selQual)) {
				qualCode = "M";
			} else if ("postgraduate".equals(selQual)) {
				qualCode = "L";
			} else if ("foundation-degree".equals(selQual)) {
				qualCode = "A";
			} else if ("access-foundation".equals(selQual)) {
				qualCode = "T";
			} else if ("hnd-hnc".equals(selQual)) {
				qualCode = "N";
			} else if ("ucas_code".equals(selQual)) {
				qualCode = "UCAS_CODE";
			} else if ("Clearing".equals(selQual)) {
				qualCode = "M";
			} else if ("all".equals(selQual)) {
				qualCode = "";
			}
			if (GenericValidator.isBlankOrNull(p_university)) {
				request.setAttribute("reason_for_404","--(1.1)--> URL doesn't have university");
				response.sendError(404, "404 error message");
				return null;
			}
			if (!entryLevel.equalsIgnoreCase("")) {
				String entryPointsFilter = new GlobalFunction().getEntryPointsForGAM(entryPoints);
				if ("ucas_points".equalsIgnoreCase(entryLevel)) {
					request.setAttribute("entryPoints", entryPoints);
				} else {
					request.setAttribute("entryPoints", entryPointsFilter);
				}
				if (entryLevel.equalsIgnoreCase("A_LEVEL")) {
					request.setAttribute("entryLevelValue", "A-levels");
				} else if (entryLevel.equalsIgnoreCase("SQA_HIGHER")) {
					request.setAttribute("entryLevelValue", "SQA Highers");
				} else if (entryLevel.equalsIgnoreCase("SQA_ADV")) {
					request.setAttribute("entryLevelValue","SQA Advanced Highers");
				} else if (entryLevel.equalsIgnoreCase("BTEC")) {
					request.setAttribute("entryLevelValue", "BTEC");
				} else {
					request.setAttribute("entryLevelValue", "Tariff Points");
				}
			}
			courseName = searchKeyword;
			courseName = courseName != null && courseName.equalsIgnoreCase("Enter course keyword") ? "" : courseName;
			categoryCode = (uniSelectBean.getFilterId() != null && uniSelectBean.getFilterId().trim().length() > 0) ? uniSelectBean.getFilterId() : "";
			studyMode = p_study_mode;
			studyLevel = qualCode;
			if (request.getAttribute("searchResultBean") != null) {
				request.removeAttribute("searchResultBean");
			}
			// CHECK FOR THE HEADER_ID PRESENT IN URL, IF NOT THEN CALL ADV_SEARCH_DO AND POPULATE THE RESULT TO W_SEARCH_RESULTS_COL TABLE //
			Map providerResultDetails = null;
			SearchVO inputBean = new SearchVO();
			String phraseSearch = new CommonFunction().replacePlus(courseName); // SEARCH KEYWORD
			String qualification = comUtil.toUpperCase(qualCode);
			String townCity = new CommonFunction().replacePlus(comUtil.toUpperCase(location)); // LOCATION
			String postcode = new CommonFunction().replacePlus(comUtil.toUpperCase(location)); // SEARCH POST CODE TEXT
			String a = GlobalConstants.WHATUNI_AFFILATE_ID; // AFFLIATE ID
			String ucasCODE = comUtil.toUpperCase(ucasCode);
			String userAgent = request.getHeader("user-agent");
			//
			String x = new SessionData().getData(request, "x");
			x = x != null && !x.equals("0") && x.trim().length() >= 0 ? x : "16180339";
			String y = new SessionData().getData(request, "y");
			y = y != null && !y.equals("0") && y.trim().length() >= 0 ? y : "0";
			//
			if (pageNo == null || pageNo.trim().length() == 0) {
				pageNo = "1";
			}
			//
			String basketID = GenericValidator.isBlankOrNull(common.checkCookieStatus(request)) ? null : common.checkCookieStatus(request);
			//
			String cpeQualificationNetworkId = "2";
			if (qualification != null && qualification.equalsIgnoreCase("L")) {
				cpeQualificationNetworkId = "3";
			}
			request.getSession().removeAttribute("cpeQualificationNetworkId");
			request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
			inputBean.setX(x);
			inputBean.setY(y);
			inputBean.setSearchWhat(searchWhat);
			inputBean.setPhraseSearch(p_q);
			inputBean.setCollegeName(p_university);
			inputBean.setQualification(qualification);
			inputBean.setTownCity(townCity);
			inputBean.setPostcode(p_postCode);
			inputBean.setSearchRange(p_distance);
			inputBean.setStudyMode(p_study_mode);
			// input for DB call 23_Aug_2018, By Sabapathi.S
			inputBean.setAssessmentType(assessmentType);
			inputBean.setAffiliateId(a);
			inputBean.setSearchHOW(searchHow.toUpperCase());
			inputBean.setSearchCategory(p_subject);
			inputBean.setEntityText(entityText);
			inputBean.setUserAgent(userAgent);
			inputBean.setUcasCode(ucasCODE);
			inputBean.setBasketId(basketID);
			inputBean.setPageNo(pageNo);
			inputBean.setPageName("clcsearch".equals(pagename) ? "CLEARING" : "COURSE_SPECIFIC"); // 24_JUN_2014
			inputBean.setEntryLevel(entryLevel);
			inputBean.setEntryPoints(entryPoints);
			inputBean.setModuleStr(p_module_str);
			inputBean.setEmpRate(p_emp_rate);
			inputBean.setCampusTypeName(p_campus_type);
			inputBean.setUnivLocTypeName(p_loc_type);
			inputBean.setRusselGroupName(p_russel);
			if(!GenericValidator.isBlankOrNull(scoreValue)){
			  inputBean.setUcasTariff(scoreValue);	
			}else{
			  inputBean.setUcasTariff(p_ucas_tariff);
			}
			inputBean.setNetworkId(cpeQualificationNetworkId);
			if (!GenericValidator.isBlankOrNull(p_jacsCode)) { // 3_JUN_2014
				inputBean.setJacsCode(p_jacsCode);
				inputBean.setPhraseSearch("");
				inputBean.setCategoryCode("");
				inputBean.setUcasCode("");
			}
			//
			inputBean.setRequestURL(GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + prsearchUrl); // 21_JUL_2015_REL Added by Priyaa for Course search logging
			inputBean.setUserTrackSessionId(new SessionData().getData(request,"userTrackId"));
			providerResultDetails = commonBusiness.getProviderResultsDetails(inputBean);
			if (providerResultDetails == null) {
				request.setAttribute("reason_for_404","--(1.1)--> College doesn't exists");
				response.sendError(404, "404 error message");
				return null;
			}
			// Roll up search for wu557_29092016, By Thiyagu G. Start
			String invalidCategoryFlag = (String) providerResultDetails.get("p_invalid_category_flag");
			String rollupToCategory = (String) providerResultDetails.get("p_rollup_to_category");
			String rollupType = (String) providerResultDetails.get("p_rollup_type");
			//
			// wu582_20181023 - Sabapathi: Received studymode from DB object to log insights 2.0
			String studymodeId = (String) providerResultDetails.get("p_insights_study_mode_id");
			request.setAttribute("studymodeId", studymodeId);
			//
			String rollUpUrl = "";
			String rollUpQryString = "";
			if (!GenericValidator.isBlankOrNull(invalidCategoryFlag) && "Y".equals(invalidCategoryFlag)) {
				rollUpQryString = new SeoUrls().constructRollUpKeywordUrl(request, URL_QUR_STRING);
				rollUpUrl = URL_STRING + rollUpQryString;
			
				response.sendRedirect(rollUpUrl);
				return null;
			} else if (!GenericValidator.isBlankOrNull(invalidCategoryFlag) && "N".equals(invalidCategoryFlag) && "PR".equals(rollupType)) {
				rollUpQryString = new SeoUrls().constructRollUpBrowseUrl(request, rollupToCategory, "subject", URL_QUR_STRING);
				rollUpUrl = URL_STRING + rollUpQryString;
				
				response.sendRedirect(rollUpUrl);
				return null;
			} else if (!GenericValidator.isBlankOrNull(invalidCategoryFlag) && "N".equals(invalidCategoryFlag) && "SR".equals(rollupType)) {
				String finalQryString = "";
				if (URL_QUR_STRING.indexOf("clearing") > -1) {
					finalQryString = "?clearing";
				}
				finalQryString += (finalQryString.indexOf("?clearing") > -1) ? "&subject=" + rollupToCategory : "?subject=" + rollupToCategory;
				String qualCourses = urlArray[1];
				if ("all-courses".equals(qualCourses)) {
					qualCourses = "degree-courses";
				}
				rollUpUrl = "/" + qualCourses + "/search" + finalQryString;
				
				response.sendRedirect(rollUpUrl);
				return null;
			}
			// Roll up search for wu557_29092016, By Thiyagu G. End
			ArrayList listOfUniStats = (ArrayList) providerResultDetails.get("pc_college_unistats_info");
			if (!CollectionUtils.isEmpty(listOfUniStats)) {
				request.setAttribute("listOfUniStats", listOfUniStats);
			}
			// Added new cursor for key stats pod and user reviews section,
			// 24_Feb_2015 By Thiyagu G
			ArrayList uniRankingList = (ArrayList) providerResultDetails.get("pc_uni_raking");
			if (!CollectionUtils.isEmpty(uniRankingList)) {
				request.setAttribute("uniRankingList", uniRankingList);
			}
			ArrayList listOfUniOverallReviewsInfo = (ArrayList) providerResultDetails.get("pc_overall_review");
			if (!CollectionUtils.isEmpty(listOfUniOverallReviewsInfo)) {
				request.setAttribute("listOfUniOverallReviewsInfo",listOfUniOverallReviewsInfo);
			}
			//
			// Added for WUSCA Rating in key stats pod by Prabha on 27_JAN_2016_REL
			List listOfWuscaRatingInfo = (List) providerResultDetails.get("pc_WUSCA_ratings");
			if (!CollectionUtils.isEmpty(listOfWuscaRatingInfo)) {
				request.setAttribute("listOfWuscaRatingInfo",listOfWuscaRatingInfo);
			}
			// End of WUSCA Rating details
			String avgRating = (String) providerResultDetails.get("o_avg_rating");
			if (!GenericValidator.isBlankOrNull(avgRating)) {
				request.setAttribute("averageReviewRating", avgRating);
			}
			// Added by Indumathi.S for showing banners in advertiser page. Nov-03-15 Rel
			String advFlag = (String) providerResultDetails.get("o_adv_flag");
			if (!GenericValidator.isBlankOrNull(advFlag)) {
				request.setAttribute("advertiserFlag", advFlag);
			}
			//
			ArrayList listOfUserReviews = (ArrayList) providerResultDetails.get("pc_user_reviews");
			if (!CollectionUtils.isEmpty(listOfUserReviews)) {
				request.setAttribute("listOfUserReviews", listOfUserReviews);
				UserReviewsVO userReviewsVO = new UserReviewsVO();
				// To get the user name initial for display in view more for 19_DEC_18 rel by Jeyalakshmi.D
				String userNameColorClassName = "rev_grey";
				for (int i = 0; i < listOfUserReviews.size(); i++) {
					userReviewsVO = (UserReviewsVO) listOfUserReviews.get(i);
					// getting the user name color class name
					if (!GenericValidator.isBlankOrNull(userReviewsVO.getUserNameInitial())) {
				         userNameColorClassName = comUtil.getReviewUserNameColorCode(userReviewsVO.getUserNameInitial());
					}
					//
					userReviewsVO.setUserNameInitialColourcode(userNameColorClassName);
				}
				//

			}
			ArrayList listOfCollegeDetails = (ArrayList) providerResultDetails.get("pc_college_details");
			if (!CollectionUtils.isEmpty(listOfCollegeDetails)) {
				request.setAttribute("listOfCollegeDetails",listOfCollegeDetails);
				ProviderResultPageVO providerVO = (ProviderResultPageVO) listOfCollegeDetails.get(0);
				// Added opendays button related column and set the request by Prabha on 08_May_2018
				request.setAttribute("totalOpendays",providerVO.getTotalOpendays());
				request.setAttribute("networkIdOD", providerVO.getNetworkId());
				request.setAttribute("opendaySuborderItemId",providerVO.getOpendaySuborderItemId());
				request.setAttribute("openMonthYear",providerVO.getOpenMonthYear());
				request.setAttribute("openDate", providerVO.getOpenDate());
				request.setAttribute("bookingUrl", providerVO.getBookingUrl());
				request.setAttribute("eventCategoryNameGa", comUtil.setOpenDayEventGALabel(providerVO.getEventCategoryName()));
				request.setAttribute("openDayEventType",providerVO.getEventCategoryName());				
				// Added open day info from DB side to fix the broken schema in structured data for next open day pod in PR page by Sangeeth.S for 20_Nov_18 rel
				if (!GenericValidator.isBlankOrNull(providerVO.getNextOpenDay())) {
					request.setAttribute("listOfOpendayInfo",listOfCollegeDetails);
				}
				//
				// End of openday code
			}
			ArrayList listOfProviderResultsVO = (ArrayList) providerResultDetails.get("pc_search_results");
			courseList = listOfProviderResultsVO;
			List<ProviderResultPageVO> providerResultsList = courseList;
			String allCourseTitlewithHashSeperator = providerResultsList.stream().map(cour -> cour.getCourseTitle()).collect(Collectors.joining("###"));
			String allcourseId = providerResultsList.stream().map(cour -> cour.getCourseId()).collect(Collectors.joining("###"));
			String courseDomesticFee = providerResultsList.stream().map(cour -> cour.getFees()).collect(Collectors.joining("###"));
			
			model.addAttribute("all_course_title", allCourseTitlewithHashSeperator);
			model.addAttribute("all_course_id", allcourseId);
		    model.addAttribute("all_course_domestic_fee", courseDomesticFee);
			if (!CollectionUtils.isEmpty(courseList)) {
				ProviderResultPageVO providerVO = (ProviderResultPageVO) courseList.get(0);
				request.setAttribute("courseCount", providerVO.getTotalCount());
				if (!GenericValidator.isBlankOrNull(providerVO.getModuleGroupId())) {
					request.setAttribute("first_mod_group_id",providerVO.getModuleGroupId());
				}
				if (!GenericValidator.isBlankOrNull(providerVO.getJacsSubject())) {
					request.setAttribute("jacsSubjectName",providerVO.getJacsSubject());
					request.setAttribute("subjectText",providerVO.getJacsSubject());
					jacsSubjectName = providerVO.getJacsSubject();
				}
			}
			// Removed unwanted parameters and generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for27_Jan_2016, By Thiyagu G. Start
			ArrayList studyModeList = (ArrayList) providerResultDetails.get("pc_study_mode_refine");
			if (!CollectionUtils.isEmpty(studyModeList)) {
				request.setAttribute("studyModeList", searchUtil.customiseProviderStudyModeResult(urlString,studyModeList, request, courseMappingPath, ""));
			}
			ArrayList qualList = (ArrayList) providerResultDetails.get("pc_qualification_refine");
			if (!CollectionUtils.isEmpty(qualList)) {
				request.setAttribute("qualList", searchUtil.customiseProviderStudyLevelURL(urlString, qualList,request, "")); // 24_JUN_2014
			}
			// Forming FIlter URL for assessment filter 23_Aug_2018, By Sabapathi.S
			ArrayList assessmentList = (ArrayList) providerResultDetails.get("pc_assessment_type_filter");
			if (!CollectionUtils.isEmpty(assessmentList)) {
				request.setAttribute("assessmentList", searchUtil.customiseProviderAssessmentURL(URL_1, assessmentList,request, ""));
			}
			ArrayList subjectList = (ArrayList) providerResultDetails.get("pc_subject_refine");
			if (!CollectionUtils.isEmpty(subjectList)) {
				request.setAttribute("subjectList", searchUtil.customiseProviderSubjectURL(urlString, subjectList,request, ""));
			}
			// Removed unwanted parameters and generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. End
			String defaultModuleTitle = (String) providerResultDetails.get("p_suggested_module_text");
			if (!GenericValidator.isBlankOrNull(defaultModuleTitle)) {
				request.setAttribute("defaultModuleTitle", defaultModuleTitle);
			}
			//
			String courseExistsFlag = (String) providerResultDetails.get("p_reg_clear_result_exist_flag");
			if (!GenericValidator.isBlankOrNull(courseExistsFlag)) {
				request.setAttribute("courseExistsFlag", courseExistsFlag);
				request.setAttribute("SHOW_SWITCH_FLAG", courseExistsFlag);
				// 24_JUN_2014
				if (courseExistsFlag.equals("Y")) {
					String seoFirstPageUrl1 = (String) request.getAttribute("SEO_FIRST_PAGE_URL");
					request.setAttribute("courseExistsLink", seoFirstPageUrl1);
				}
			}
			//
			String pixelTracking = (String) providerResultDetails.get("o_pixel_tracking_code"); // 9_DEC_2014 Added by Priyaa for pixel tracking in email success page
			if (!GenericValidator.isBlankOrNull(pixelTracking)) {
				request.setAttribute("pixelTracking", pixelTracking); // Modified for wu_536 3-FEB-2015_REL by Karthi
			}
			 // Added for dynamic meta data details from Back office, 20201020 rel by Sri Sankari
	  	    ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) providerResultDetails.get("PC_META_DETAILS");
	  	    new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);
			// Get the stats logging details, 27_JAN_2016 By Thiyagu G
			String browseCatFlag = "";
			String browseCatDesc = "";
			String studyModeDesc = "";
			String browseCatId = "";
			String collegeLocation = "";
			ArrayList statsLog = (ArrayList) providerResultDetails.get("pc_stats_log");
			if (!CollectionUtils.isEmpty(statsLog)) {
				request.setAttribute("statsLog", statsLog);
				Iterator statsItr = statsLog.iterator();
				while (statsItr.hasNext()) {
					RefineByOptionsVO statsVO = (RefineByOptionsVO) statsItr.next();
					browseCatFlag = statsVO.getBrowseCatFlag();
					browseCatId = statsVO.getBrowseCatId();
					categoryCode = !GenericValidator.isBlankOrNull(statsVO.getBrowseCatCode()) ? statsVO.getBrowseCatCode() : "";
					browseCatDesc = statsVO.getBrowseCatDesc();
					studyModeDesc = statsVO.getStudyModeDesc();
					collegeId = statsVO.getCollegeId();
					collegeName = statsVO.getCollegeName();
					collegeNameDisplay = statsVO.getCollegeNameDisplay();
					collegeLocation = statsVO.getCollegeLocation();
				}
			}
			request.setAttribute("GTMLDCS", GenericValidator.isBlankOrNull(browseCatDesc) ? "" : new GlobalFunction().getReplacedString(browseCatDesc.trim().toLowerCase()));
			if (GenericValidator.isBlankOrNull(collegeId) && GenericValidator.isBlankOrNull(collegeName)) {
				request.setAttribute("reason_for_404","--(1.1)--> College doesn't exists");
				response.sendError(404, "404 error message");
				return null;
			}
			if ((collegeId != null && collegeId.trim().length() > 0)) {
				uniSelectBean.setCourseCollegeId(collegeId);
			}
			collegeId = uniSelectBean.getCourseCollegeId();
			if (collegeId != null && collegeId.trim().length() > 0) {
				request.setAttribute("interactive-college-id", collegeId);
				new GlobalFunction().getInteractivePodInfo(collegeId, request);
			}
			String searchCategory = comUtil.toUpperCase(categoryCode); // SUBJECT CATEGORYY ID
			// fetch bread crumb only for browse-provider-result page
			if (searchCategory != null && searchCategory.trim().length() > 0) {
				String breadCrumb = "";
				if ("clcsearch".equalsIgnoreCase(pagename)) {
					breadCrumb = new SeoUtilities().getBreadCrumb("CLEARING_PROVIDER_RESULT_PAGE", qualification, "",searchCategory, postcode, collegeId);
				} else {
					breadCrumb = new SeoUtilities().getBreadCrumb("PROVIDER_RESULT_PAGE", qualification, "",searchCategory, postcode, collegeId);
				}
				if (breadCrumb != null && breadCrumb.trim().length() > 0) {
					request.setAttribute("breadCrumb", breadCrumb);
				}
			}
			request.setAttribute("paramstudyModeValue", studyModeDesc);
			request.setAttribute("paramstudyModeDesc", studyModeDesc);
			if (!GenericValidator.isBlankOrNull(p_subject)) {
				request.setAttribute("SEARCH_TYPE", "BROWSE_SEARCH");
			} else {
				request.setAttribute("SEARCH_TYPE", "KEYWORD_SEARCH");
			}
			if ("clcsearch".equalsIgnoreCase(pagename)) {
				request.setAttribute("searchJourneyType", "CLEARING"); // 24_JUN_2014
			}
			if (providerResultDetails == null) { // 404 the page if no results
				response.sendError(404, "404 error message");
				return null;
			}
			if (!viewallcourses) { // TODO_TODO remove after data fix by selva
				request.removeAttribute("showFundingTab");
			}
			boolean showmessagepage = false;
			/*
			 * TO GET THE RESULTS STORED IN THE TEMP TABLE BY PASSING THE
			 * HEADER_ID
			 */
			if (searchResultFlag == false) {
				if (courseList != null && courseList.size() == 0) {
					searchResultFlag = true;
					showmessagepage = true;
				} else {
					session.removeAttribute("scholarship_no_result_message");
					session.setAttribute("courseHeaderId", headerId);
				}
			}
			request.setAttribute("providerName", collegeName);
			request.setAttribute("providerNameDisplay", collegeNameDisplay);
			request.setAttribute("hitbox_college_name", collegeName);
			request.setAttribute("hitbox_college_name_display",collegeNameDisplay);
			request.setAttribute("collegeName", collegeName);
			request.setAttribute("collegeNameDisplay", collegeNameDisplay);
			/*
			 * Added college display name for smart pixel by Prabha on
			 * 20_02_2018
			 */
			if (!GenericValidator.isBlankOrNull(collegeNameDisplay)) {
				request.setAttribute("cDimCollegeDisName",("\"" + collegeName + "\""));
			}
			/* End od smart pixel code */
			request.setAttribute("collegeLocation", collegeLocation);
			String parentCategoryDesc = (String) providerResultDetails.get("p_parent_cat_text_key");
			request.setAttribute("parentCategoryCode", parentCategoryDesc);
			request.setAttribute("parentCategoryDesc", parentCategoryDesc);
			String subjectL1 = null;
		    String subjectL2 = null;
		    if (GenericValidator.isBlankOrNull(parentCategoryDesc)) {
		      subjectL1 = searchKeyword;
		      subjectL2 = "";
		    }else{
		       subjectL1 = parentCategoryDesc;
		       subjectL2 = searchKeyword;
		     }
		    request.setAttribute("subjectL1", subjectL1);
			request.setAttribute("subjectL2", subjectL2);
			
			if (searchResultFlag) {
				if (request.getParameter("pno") != null && request.getParameter("pno").trim().length() > 0) {
					showmessagepage = true;
				} else {
					showmessagepage = true;
				}
				// Added by Indumathi.S Jan-27-16 Rel For URL redirection
				if (!GenericValidator.isBlankOrNull(parentCategoryDesc)) {
					String queryString = request.getQueryString();
					String newURL = "";
					if (!GenericValidator.isBlankOrNull(queryString)) {
						String[] splittedURL = queryString.split("&");
						for (int i = 0; i < splittedURL.length; i++) {
							if (splittedURL[i].indexOf("subject") > -1) {
								splittedURL[i] = "subject="+ parentCategoryDesc;
							}
							newURL += splittedURL[i];
							if (i + 1 != splittedURL.length) {
								newURL += "&";
							}
						}
					}
					String redirectingBrowseURL = URL_STRING + "?" + newURL;
					response.sendRedirect(redirectingBrowseURL);
				}
			}
			if (showmessagepage) {
				if (location != null && location.trim().length() > 0) {
					request.setAttribute("err_location",(location != null ? new CommonFunction().getTitleCaseFn(location) : location));
				}
				if (courseName != null && courseName.trim().length() > 0) {
					request.setAttribute("err_subject_name", courseName);
				}
				// added for 28th April 2009 release
				if ((categoryCode != null && categoryCode.trim().length() > 0) && (studyLevel != null && studyLevel.trim().length() > 0)) {
					String commonCagtegoryName = globalFn.getCommonCategoryName(categoryCode, studyLevel);
					if (commonCagtegoryName != null && commonCagtegoryName.trim().length() > 0) {
						request.setAttribute("err_subject_name",commonCagtegoryName);
						request.setAttribute("categoryCode", categoryCode);
					}
				}
				if (studyLevel != null && studyLevel.trim().length() > 0) {
					request.setAttribute("err_study_level_id", studyLevel);
				}
				String studyLevelName = studyLevel != null && studyLevel.trim().length() > 0 && !studyLevel.equalsIgnoreCase("") ? new CommonFunction().getStudyLevelDesc(comUtil.toUpperCase(studyLevel),request) : "";
				if (studyLevelName != null && studyLevelName.trim().length() > 0) {
					request.setAttribute("err_study_level_name", studyLevelName);
				}
				if (collegeId != null && collegeId.trim().length() > 0) {
					request.setAttribute("err_provider_name", collegeName);
					request.setAttribute("err_provider_name_display",collegeNameDisplay);
				}
				request.setAttribute("providerResult", "true");
				request.setAttribute("otherindex", "noindex,follow");
				if ("clcsearch".equalsIgnoreCase(pagename)) {
					request.setAttribute("clearingSearchBar", "YES");
					request.setAttribute("searchJourneyType", "CLEARING"); // 24_JUN_2014
					session.removeAttribute("clearingSearchBar");
					session.setAttribute("clearingSearchBar", "YES");
				}
				// 24-JUN-2014
				session.removeAttribute("err_location");
				session.removeAttribute("collegeId");
				session.removeAttribute("err_provider_name");
				session.setAttribute("err_location",request.getAttribute("err_location"));
				session.setAttribute("collegeId", collegeId);
				session.setAttribute("err_provider_name", collegeName);
				session.removeAttribute("providerResult");
				session.setAttribute("providerResult", "true");
				session.removeAttribute("otherindex");
				session.setAttribute("otherindex", "noindex,follow");
				response.sendRedirect("/degrees/notfound.html");
			}
			if (!GenericValidator.isBlankOrNull(selQual) && !"all".equalsIgnoreCase(selQual)) {
				request.setAttribute("paramStudyLevelId", qualCode);
			} else {
				request.setAttribute("paramStudyLevelId", "");
			}
			String studyLevelDesc = common.getStudyLevelDesc(comUtil.toUpperCase(studyLevel), request);
			request.setAttribute("studyLevelName",studyLevelDesc != null && studyLevelDesc.trim().length() > 0 && !studyLevelDesc.equalsIgnoreCase("") ? studyLevelDesc : "");
			String subjectText = request.getAttribute("subjectText") != null ? (String) request.getAttribute("subjectText") : "";
			if (GenericValidator.isBlankOrNull(subjectText)) {
				request.setAttribute("subjectText", browseCatDesc);
			}
			request.setAttribute("paramCategoryCode", categoryCode);
			request.setAttribute("collId", collegeId);
			if (ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0) {
				request.setAttribute("searchText", "UCAS code " + ucasCode);
			} else {
				request.setAttribute("searchText", courseName);
			}
			if (location != null && location.trim().length() > 0) {
				request.setAttribute("location", comUtil.toTitleCase(comUtil.getSeoFriendlyLocationName(common.replacePlus(location))));
			}
			// added for 28th April 2009 Release
			if (courseName != null && courseName.trim().length() > 0) {
				request.setAttribute("prospectus_search_name", courseName);
			}
			// added for 28th April 2009 release
			if ((categoryCode != null && categoryCode.trim().length() > 0) && (studyLevel != null && studyLevel.trim().length() > 0)) {
				String commonCagtegoryName = globalFn.getCommonCategoryName(categoryCode, studyLevel);
				if (commonCagtegoryName != null && commonCagtegoryName.trim().length() > 0) {
					request.setAttribute("prospectus_search_name",commonCagtegoryName);
				}
			}
			request.setAttribute("studyLevelDesc", studyLevelDesc);
			request.setAttribute("studyLevel", studyLevel);
			String gamKeywordOrSubject = "";
			//
			String courseHeader = (categoryCode != null && !categoryCode.equalsIgnoreCase("null") && categoryCode.trim().length() > 0 ? globalFn.getCategoryDescFromBrowseCategories(categoryCode,studyLevel) : ((courseName != null && !courseName.equalsIgnoreCase("null") && courseName.trim().length() > 0) ? comUtil.toTitleCase(courseName) : ((ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0) ? "UCAS code "+ comUtil.toUpperCase(ucasCode.replaceAll("-", " ")) : "")));
			if (!GenericValidator.isBlankOrNull(jacsSubjectName)) {
				courseHeader = jacsSubjectName;
			}
			courseHeader = (courseHeader != null && !courseHeader.equalsIgnoreCase("null") && courseHeader.trim().length() > 0 ? courseHeader.trim() : courseHeader);
			gamKeywordOrSubject = courseHeader;
			if (GenericValidator.isBlankOrNull(gamKeywordOrSubject) && !GenericValidator.isBlankOrNull(p_q)) {
				gamKeywordOrSubject = p_q.replaceAll("-", " ").toLowerCase();
			}
			if (!GenericValidator.isBlankOrNull(studyLevel)) {
				if ("Z".equalsIgnoreCase(searchWhat)) {

					studyLevelDesc = studyLevelDesc.toLowerCase();
					if (studyLevelDesc.indexOf("degree") == -1) {
						studyLevelDesc = "N".equalsIgnoreCase(studyLevel) ? studyLevelDesc.toUpperCase() : studyLevelDesc;
						studyLevelDesc = studyLevelDesc + " degrees";
					} else {
						studyLevelDesc = studyLevelDesc.replaceFirst("degree","degrees");
					}

					if ("clcsearch".equals(pagename)) { // 24_JUN_2014
						courseHeader = courseHeader + " Clearing & Adjustment " + studyLevelDesc;
					} else {
						if ("L".equalsIgnoreCase(studyLevel)) {
							courseHeader = courseHeader+ " "+ studyLevelDesc.replaceFirst("postgraduate degrees","Postgraduate Degrees");
						} else if ("M".equalsIgnoreCase(studyLevel)) {
							courseHeader = courseHeader+ " " + studyLevelDesc.replaceFirst("degrees","Undergraduate Degrees");
						} else {
							courseHeader = courseHeader + " " + studyLevelDesc;
						}
					}
				} else {
					if (ucasCode == null && ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() == 0) {
						if ("clcsearch".equals(pagename)) { // 24_JUN_2014
							courseHeader = ("N".equalsIgnoreCase(studyLevel) ? studyLevelDesc.toUpperCase() : common.getTitleCaseFn(studyLevelDesc))+ " Clearing & Adjustment courses";
						} else {
							courseHeader = ("N".equalsIgnoreCase(studyLevel) ? studyLevelDesc.toUpperCase() : common.getTitleCaseFn(studyLevelDesc))+ " courses";
						}
					} else {
						if ("clcsearch".equals(pagename)) { // 24_JUN_2014
							courseHeader = courseHeader+ " Clearing & Adjustment "+ studyLevelDesc.replaceFirst("Degree","degrees");
						} else if ("L".equalsIgnoreCase(studyLevel)) {
							courseHeader = courseHeader+ " "+ studyLevelDesc.replaceFirst("Postgraduate","Postgraduate Degrees");
						} else if ("M".equalsIgnoreCase(studyLevel)) {
							courseHeader = courseHeader+ " "+ studyLevelDesc.replaceFirst("Degree","Undergraduate Degrees");
						} else {
							courseHeader = courseHeader+ " "+ studyLevelDesc.replaceFirst("Degree","degrees");
						}
					}
				}
			} else {
				String ssubjectName = (String) request.getAttribute("subjectText");
				if ("clcsearch".equals(pagename)) { // 24_JUN_2014
					courseHeader = "All "+ (!GenericValidator.isBlankOrNull(ssubjectName) ? ssubjectName : "") + " Clearing courses";
				} else {
					courseHeader = "All "+ (!GenericValidator.isBlankOrNull(ssubjectName) ? ssubjectName : "") + "  courses";
				}
			}
		
			request.setAttribute("courseHeader", courseHeader);
			request.setAttribute("courseList", courseList);
			request.setAttribute("collegeId", collegeId);
			request.setAttribute("pageno", pageNo);
			request.setAttribute("searchWhat", searchWhat);

			session.removeAttribute("gamKeywordOrSubject");
			session.removeAttribute("gamStudyLevelDesc");
			session.setAttribute("gamKeywordOrSubject", gamKeywordOrSubject);
			request.setAttribute("gamKeywordOrSubject", gamKeywordOrSubject);// Added by Indumathi.S To get the keyword in googleAdSlots July_5_2016
            session.setAttribute("gamStudyLevelDesc", studyLevelDesc);
			if ((pageNo != null && pageNo.equals("1")) && mysearchurlflag) {
				String userId = new SessionData().getData(request, "y");
				userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
			}
			request.setAttribute("URL_STRING", URL_STRING);
			request.setAttribute("URL_1", URL_1);
			request.setAttribute("URL_2", URL_2);
			if (request.getParameter("unihomefrmsearch") != null) {
				request.setAttribute("unihomefrmsearch", "unihomefrmsearch");
			}
			if (studyLevel != null && studyLevel.equalsIgnoreCase("L")) {
				request.setAttribute("showpgbanner", "YES");
			} else {
				request.setAttribute("shownonpgbanner", "YES");
			}
			request.setAttribute("providerResult", "true"); // Oct 6th release
			String searchposition = new CookieManager().getCookieValue(request,"sresult_provider_position");
			if (searchposition != null && searchposition.trim().length() > 0) {
				request.setAttribute("sresult_provider_position",searchposition);
			}
			// added for 01st December 2009 Release
			if (pageNo != null && pageNo.equals("1")) {
				request.setAttribute("meta_robot", "index,follow");
			}
			if ((pageNo != null && pageNo.equals("1")) && (location != null && location.equalsIgnoreCase("united kingdom"))) {
				request.setAttribute("meta_robot", "index,follow");
			} else if ((pageNo != null && pageNo.equals("1")) && (location != null && location.trim().length() > 0)) {
				request.setAttribute("meta_robot", "noindex,follow");
			}
			if (pageNo != null && !pageNo.equals("1")) {
				request.setAttribute("meta_robot", "noindex,follow");
			}
			if (URL_QUR_STRING.contains("q=") || URL_QUR_STRING.contains("sort=") || URL_QUR_STRING.contains("module=") || URL_QUR_STRING.contains("jacs=") || URL_QUR_STRING.contains("assessment-type=") || URL_QUR_STRING.contains("your-pref=") || URL_QUR_STRING.contains("entry-level=")) { // Added condition to no index for the Oct_23_18 rel by sangeeth.S
			request.setAttribute("meta_robot", "noindex,follow");
			}
			if ("clcsearch".equalsIgnoreCase(pagename)) {
				if (URL_QUR_STRING.contains("clearing") || URL_QUR_STRING.contains("subject") || URL_QUR_STRING.contains("university")) {
					request.setAttribute("meta_robot", "index,follow");
				} else {
					request.setAttribute("meta_robot", "noindex,follow");
				}
				if (location != null && location.equalsIgnoreCase("united kingdom")) {
					request.setAttribute("meta_robot", "index,follow");
				} else if (location != null && location.trim().length() > 0) {
					request.setAttribute("meta_robot", "noindex,follow");
				} else {
					request.setAttribute("meta_robot", "noindex,follow");
				}
				if (URL_QUR_STRING.contains("q=") || URL_QUR_STRING.contains("sort=") || URL_QUR_STRING.contains("module=") || URL_QUR_STRING.contains("jacs=") || URL_QUR_STRING.contains("entry-level=")) {
					request.setAttribute("meta_robot", "noindex,follow");
				}
			}
			// end of code added for 01st December 2009
			// code added for 16th March 2010 Release
			if (studyLevel != null && studyLevel.trim().length() > 0) {
				ArrayList UniversityCategoryQualList = new GlobalFunction().getUniversityCategoryQualList(collegeId);
				if (!CollectionUtils.isEmpty(UniversityCategoryQualList)) {
					request.setAttribute("UniversityCategoryQualList",UniversityCategoryQualList);
					if (!studyLevel.equalsIgnoreCase("")) {
						Iterator uniqualiterator = UniversityCategoryQualList.iterator();
						while (uniqualiterator.hasNext()) {
							SearchBean uniqualbean = (SearchBean) uniqualiterator.next();
							if (studyLevel.equalsIgnoreCase(uniqualbean.getStudyLevelId())) {
								request.setAttribute("show-uni-subject-link","YES");
								break;
							}
						}
					}
				}
			}
			// end of the code added for 16th March 2010 Release
			// code added for 21th October 2009 Release for UCAS code search
			if (ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() != 0) {
				if ((pageNo != null && pageNo.equals("1")) && (location != null && location.equalsIgnoreCase("united+kingdom"))) {
					request.setAttribute("meta_robot", "index,follow");
				} else {
					request.setAttribute("meta_robot", "noindex,follow");
				}
				request.removeAttribute("showFundingTab");
			}
			/* DONT ALTER THIS ---> mainly used to track time taken details */
			if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
				new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request,this.getClass().toString(), startActionTime);
			} /* DONT ALTER THIS ---> mainly used to track time taken details */
			// 08_Oct_2014 - Insight start, added by Thiyagu G.
			request.setAttribute("insightPageFlag", "yes");
			if (categoryCode != null && categoryCode.trim().length() > 0) {
				request.setAttribute("dimCategoryCode", new GlobalFunction().getReplacedString(categoryCode));
			} else {
				if (!GenericValidator.isBlankOrNull(gamKeywordOrSubject)) {
					request.setAttribute("cDimKeyword", new GlobalFunction().getReplacedString(gamKeywordOrSubject.toLowerCase())); // Added for wu_536 3-FEB-2015_REL by  Karthi to set either GA dimension 18 or 6
				}
			}
			if (studyLevel != null && studyLevel.trim().length() > 0) {
				request.setAttribute("qualification", qualification);
				new GlobalFunction().getStudyLevelDesc(studyLevel, request);
			}
			request.setAttribute("cDimCollegeId", GenericValidator.isBlankOrNull(collegeId) ? "" : collegeId.trim());
			if (!GenericValidator.isBlankOrNull(collegeId)) {
				request.setAttribute("cDimCollegeIds",("'" + collegeId.trim() + "'"));
			}
			if (!GenericValidator.isBlankOrNull(location)) { // Added by Prabha for insight(dimension-13) on 29_Mar_2016
				request.setAttribute("location",common.replaceHypenWithSpace(location).toUpperCase());
			}
			// Added for June_5_18 release to no index the url pattern /all-courses/ by sangeeth.S
			if ("all".equalsIgnoreCase(selQual)) {
				request.setAttribute("meta_robot", "noindex,follow");
			}
			// Added for the self referencing canonical url for June 5 2018 release by Sangeeth.S
			if ("degree".equalsIgnoreCase(selQual)) {
				request.setAttribute("CANONICAL_URL", "CANONICAL_URL");
			}
			// Insight end
			request.setAttribute("isAdvertiser", new AdvertUtilities().isAdvertiser(collegeId, cpeQualificationNetworkId)); // 21_July_2015-Added this request to log events,by Thiyagu G. 
		if (!GenericValidator.isBlankOrNull(URL_QUR_STRING) && (URL_QUR_STRING.contains("clearing"))) { // 30_JUN_2015_REL
				request.setAttribute("getInsightName","clearingcoursesearchresult.jsp");
				return new ModelAndView("showClearingCourseResult");
			}
			return new ModelAndView("showCourseResult", "searchBean",uniSelectBean);
		} catch (Exception searchException) {
			searchException.printStackTrace();
		}
		request.setAttribute("collegeId", collegeId);
		/* DONT ALTER THIS ---> mainly used to track time taken details */
		if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
			new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
		} /* DONT ALTER THIS ---> mainly used to track time taken details */
		return new ModelAndView("showuniview");
	}

}
