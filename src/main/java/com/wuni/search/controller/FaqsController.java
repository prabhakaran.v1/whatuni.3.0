package com.wuni.search.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.wuni.util.seo.SeoUtilities;
import WUI.utilities.CommonFunction;

@Controller
public class FaqsController {
	
  @RequestMapping(value = "/faqs", method = {RequestMethod.GET, RequestMethod.POST}) 
  public ModelAndView getFaqs(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    session = request.getSession();
    String searchedSubject = (String)session.getAttribute("FaqSubject");
    String qualification = (String)session.getAttribute("FaqStudyLevel");
    String catCode = (String)session.getAttribute("FaqCategoryCode");
    CommonFunction common = new CommonFunction();
    if(!GenericValidator.isBlankOrNull(searchedSubject)){
      searchedSubject = common.replaceHypenWithSpace(searchedSubject);
      searchedSubject = common.getTitleCaseFn(searchedSubject);
      searchedSubject = searchedSubject.replaceAll(" And ", " and ");
      request.setAttribute("subjectDescription", searchedSubject);
      if(!GenericValidator.isBlankOrNull(catCode)){
        String breadCrumb = new SeoUtilities().getBreadCrumb("FAQ_PAGE", qualification, "", catCode, "", "");
        if (breadCrumb != null && breadCrumb.trim().length() > 0) {
          request.setAttribute("breadCrumb", breadCrumb);
        }
      }
    }   
    return new ModelAndView("searchFaqs");
  }
}
