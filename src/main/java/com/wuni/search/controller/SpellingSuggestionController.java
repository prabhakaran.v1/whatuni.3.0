package com.wuni.search.controller;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This action class is used to load the university employment info in page3. This is ajax call.
 */

@Controller
public class SpellingSuggestionController {
  
  @RequestMapping(value = "/did-you-mean", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getSearchPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String subjectname = (String) session.getAttribute("err_subject_name");    
          subjectname  = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
    
    String studylevelid = (String) session.getAttribute("err_study_level_id");    
          studylevelid  = studylevelid !=null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length()>0 ? studylevelid : "";
    
    String studylevelname = (String) session.getAttribute("err_study_level_name");    
          studylevelname  = studylevelname !=null && !studylevelname.equalsIgnoreCase("null") && studylevelname.trim().length()>0 ?  new CommonUtil().toLowerCase(studylevelname)  : "";
    
    String location = (String) session.getAttribute("err_location");
          location  = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonFunction().replacePlus(location.trim()) : "";
    
    String providername = (String) session.getAttribute("err_provider_name");    
          providername  = providername !=null && !providername.equalsIgnoreCase("null") && providername.trim().length()>0 ? providername.trim() : "";
    
    String collegeId =  (String) session.getAttribute("collegeId");    
          collegeId =  collegeId != null && collegeId.trim().length() > 0? collegeId : "0";
    
    String noindexfollow      =   request.getAttribute("noindex") !=null ? "TRUE"  : "FALSE";   
           noindexfollow      =   request.getAttribute("unihomefrmsearch") !=null ? "noindex,nofollow"  : noindexfollow;
           noindexfollow      =   session.getAttribute("otherindex") !=null ? String.valueOf(session.getAttribute("otherindex"))  : noindexfollow;
    
    String categoryCode=session.getAttribute("FaqCategoryCode") != null ? session.getAttribute("FaqCategoryCode").toString() : "";
    
    String clSearchBarForNoResults = (String)session.getAttribute("clearingSearchBar");
    
    String providerResult = (String)session.getAttribute("providerResult");
       
    ArrayList listOfSpellingSuggestions = (ArrayList)session.getAttribute("listOfSpellingSuggestions");
      
    request.setAttribute("err_subject_name", subjectname);
    request.setAttribute("err_study_level_id", studylevelid);
    request.setAttribute("err_study_level_name", studylevelname);
    request.setAttribute("err_location", location);
    request.setAttribute("err_provider_name", providername);
    request.setAttribute("collegeId", collegeId);    
    request.setAttribute("categoryCode", categoryCode);    
    request.setAttribute("providerResult", providerResult);
    request.setAttribute("otherindex", "noindex,follow");
    request.setAttribute("clearingSearchBar", clSearchBarForNoResults);
    request.setAttribute("listOfSpellingSuggestions", listOfSpellingSuggestions);
    
    request.setAttribute("cDimCollegeId", (GenericValidator.isBlankOrNull(collegeId) || "0".equals(collegeId)) ? "": collegeId.trim());   
    request.setAttribute("cDimKeyword", GenericValidator.isBlankOrNull(subjectname)? "": new GlobalFunction().getReplacedString(subjectname.trim().toLowerCase()));   
    request.setAttribute("dimCategoryCode", GenericValidator.isBlankOrNull(categoryCode)? "": new GlobalFunction().getReplacedString(categoryCode.trim().toUpperCase()));      
    if(!GenericValidator.isBlankOrNull(location) && location != null && !"UNITED KINGDOM".equalsIgnoreCase(location)){
        request.setAttribute("cDimCountry", "UK");
    }    
    if(!GenericValidator.isBlankOrNull(studylevelid)){//26-Aug-2014
     request.setAttribute("qualification", GenericValidator.isBlankOrNull(studylevelid)? "": studylevelid.trim());
      new GlobalFunction().getStudyLevelDesc(studylevelid, request);
    }
      
    session.removeAttribute("err_location");
    session.removeAttribute("collegeId");        
    session.removeAttribute("err_provider_name");
    session.removeAttribute("clearingSearchBar");
    session.removeAttribute("providerResult");
    session.removeAttribute("listOfSpellingSuggestions");
    
    return new ModelAndView("did-you-mean");
  }

}
