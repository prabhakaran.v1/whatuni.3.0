package com.wuni.search.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mobile.valueobject.SearchVO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import spring.controller.clearing.ClearingSearchResultController;
import spring.pojo.clearing.search.SearchResultsRequest;
import spring.valueobject.search.FeaturedBrandVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.CommonValidator;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.sql.DataModel;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;
import com.wuni.util.valueobject.search.BestMatchCoursesVO;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;
import WUI.search.form.SearchBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.OmnitureProperties;
import WUI.utilities.SearchUtilities;
import WUI.utilities.SessionData;
/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : SearchController.java
* Description   :This controller is for the search result page
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	             Change
* ************************************************************************************************************************************** 
* Sangeeth.S              			          24-02-2020          Modified                           Added study level text for the enter grade pod*  
* Sangeeth.S								  13-04-2020          Modified							 Added user ip in param for geo location tracking
* Kailash L									  21-JUL-2020		  Modified		                     Added out parameter for sponsored orderItemId - JIRA - 403
* Sujitha V                                   18-AUG-2020         Modified                           Added dynamic meta data details for SR page URLs in the Back office
* Sri Sankari		                          17_SEP_2020         Modified                           Added stats log for tariff logging(UCAS point)
* */
@Controller
public class SearchController{
  
  @Autowired
  ClearingSearchResultController clearingSearchResultController = null;
  
	@RequestMapping(value = "/*-courses/search/old", method = {RequestMethod.GET, RequestMethod.POST})
	  public ModelAndView getSearchPage(@RequestParam Map<String, String> requestParam, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
	    final String URL_QUR_STRING = request.getQueryString();
	    CommonFunction common = new CommonFunction();
	    String ClearingFlag = common.getWUSysVarValue("CLEARING_ON_OFF");  
	    request.setAttribute("queryStr", URL_QUR_STRING);
	    SearchBean searchBean = new SearchBean();
	    String filterparameters = (String)request.getAttribute("queryStr"); //8_OCT_2014 Added by Priyaa for What have you done pod
	    filterparameters = (!GenericValidator.isBlankOrNull(filterparameters) ? "?" + (String)request.getAttribute("queryStr") : "");
	    request.setAttribute("filterparameters", filterparameters);
	    String currentPageUrl = common.getSchemeName(request) + GlobalConstants.WHATUNI_DOMAIN + URL_STRING + filterparameters;//Added getSchemeName by Indumathi Mar-29-16
	    //Added clearing url redirection when clearing is OFF for OCT_23_18 REL by Sangeeth.S      
	    if(GlobalConstants.OFF.equalsIgnoreCase(ClearingFlag) && !GenericValidator.isBlankOrNull(filterparameters) && filterparameters.toLowerCase().indexOf(GlobalConstants.SEO_PATH_CLEARING) > -1){
	      String tempRedirectUrl = currentPageUrl.replace((GlobalConstants.SEO_PATH_CLEARING+"&"),"");
	      response.setStatus(response.SC_MOVED_TEMPORARILY);
	      response.sendRedirect(tempRedirectUrl);
	      return null;
	    }
	    SearchResultsRequest searchResultsInputBean = new SearchResultsRequest();
	    if(URL_QUR_STRING.contains("clearing")) {
		     return clearingSearchResultController.searchResultsLanding(requestParam, model, searchResultsInputBean, request, response, session);
	      // return new ClearingSearchController().getClearingSearch(model, searchBean, request, response);
	    }
	    //
	    String[] urlArray = URL_STRING.split("/");
	    SessionData sessiondata = new SessionData();
	    session = request.getSession();
	    ServletContext context = request.getServletContext();
	    SearchUtilities searchUtil = new SearchUtilities();
	    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // DB_CALL
	      return new ModelAndView("forward: /userLogin.html");
	    }
	    //  STORE'S PARAM VALUE OF X, Y, A TO HASMAP UNDER THE SESSION VARIABLE SESSIONDATA //
	    sessiondata.setParameterValuestoSessionData(request, response);
	    sessiondata.addData(request, response, "a", GlobalConstants.WHATUNI_AFFILATE_ID);
	    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
	      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
	        String fromUrl = "/home.html";
	        session.setAttribute("fromUrl", fromUrl);
	        return new ModelAndView("loginPage");
	      }
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    }
	    String referrer = request.getHeader("referer");
	    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com" + URL_STRING + "/")); //Added getSchemeName by Indumathi Mar-29-16   
	    request.setAttribute("SEARCH_TYPE", "BROWSE_SEARCH");
	    common.loadUserLoggedInformationForMoneyPage(request, context, response); //DB_CALL 
	    CommonUtil utilities = new CommonUtil();
	    GlobalFunction global = new GlobalFunction();
	    
	    String searchType = "COURSE_SPECIFIC";
	    String pageName = "page";
	    boolean isThisUcasSearch = false;
	    SearchUtilities searchutil = new SearchUtilities();    
	    if (!GenericValidator.isBlankOrNull(filterparameters) && (filterparameters.contains("?clearing"))) { //30_JUN_2015_REL
	      searchType = "CLEARING";
	      request.setAttribute("searchClearing", "TRUE");
	    }
	    //Removed old url array code block and added code to get parameter values for 27_Jan_2016, By Thiyagu G. Start
	    String p_q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? null : (request.getParameter("q"));
	    String searchKeyword = GenericValidator.isBlankOrNull(p_q) ? null : p_q;
	    String p_subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? null : (request.getParameter("subject"));
	    if(!GenericValidator.isBlankOrNull(p_q)){
	      p_subject = "";
	    }
	    String p_module = GenericValidator.isBlankOrNull(request.getParameter("module")) ? null : (request.getParameter("module"));
	    String p_loc_type = GenericValidator.isBlankOrNull(request.getParameter("location-type")) ? null : (request.getParameter("location-type"));
	    String p_loc = GenericValidator.isBlankOrNull(request.getParameter("location")) ? null : (request.getParameter("location"));
	    String p_postCode = GenericValidator.isBlankOrNull(request.getParameter("postcode")) ? null : (request.getParameter("postcode"));
	    String p_distance = GenericValidator.isBlankOrNull(request.getParameter("distance")) ? "25" : (request.getParameter("distance"));
	    String p_campus_type = GenericValidator.isBlankOrNull(request.getParameter("campus-type")) ? null : (request.getParameter("campus-type"));
	    String p_study_mode = GenericValidator.isBlankOrNull(request.getParameter("study-mode")) ? null : (request.getParameter("study-mode"));
	    String p_emp_rate_max = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? null : (request.getParameter("employment-rate-max"));
	    String p_emp_rate_min = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? null : (request.getParameter("employment-rate-min"));
	    String p_emp_rate = (!GenericValidator.isBlankOrNull(p_emp_rate_min) ? p_emp_rate_min : "") + (!GenericValidator.isBlankOrNull(p_emp_rate_max) ? ("," + p_emp_rate_max) : "");
	    String p_ucas_tariff_max = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max")) ? null : (request.getParameter("ucas-points-max"));
	    String p_ucas_tariff_min = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min")) ? null : (request.getParameter("ucas-points-min"));
	    String p_russel = GenericValidator.isBlankOrNull(request.getParameter("russell-group")) ? null : (request.getParameter("russell-group"));
	    String sortOrder = GenericValidator.isBlankOrNull(request.getParameter("sort")) ? "r" : (request.getParameter("sort"));
	    String p_recentFilter = GenericValidator.isBlankOrNull(request.getParameter("rf")) ? null : (request.getParameter("rf"));
	    String p_ucas_tariff = (!GenericValidator.isBlankOrNull(p_ucas_tariff_min) ? p_ucas_tariff_min : "") + (!GenericValidator.isBlankOrNull(p_ucas_tariff_max) ? ("," + p_ucas_tariff_max) : "");
	    String p_jacsCode = GenericValidator.isBlankOrNull(request.getParameter("jacs")) ? null : (request.getParameter("jacs"));
	    String hQueryString = GenericValidator.isBlankOrNull(request.getParameter("hidden_queryString")) ? null : (request.getParameter("hidden_queryString"));
	    String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level")) ? "" : (request.getParameter("entry-level").replaceAll("-", "_"));
	    String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points")) ? "" : (request.getParameter("entry-points"));
	    String pageno = !GenericValidator.isBlankOrNull(request.getParameter("pageno")) ? (request.getParameter("pageno")) : "1";
	    String ucasCode = !GenericValidator.isBlankOrNull(request.getParameter("ucas-code")) ? (request.getParameter("ucas-code")) : "";
	    //Added this for getting assessment and your pref from URL param 23_Aug_2018, By Sabapathi.S
	    String assessmentTypeFilter = !GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? (request.getParameter("assessment-type")) : "";
	    String reviewCategoryFilter = !GenericValidator.isBlankOrNull(request.getParameter("your-pref")) ? (request.getParameter("your-pref")) : "";
	    String scoreValue = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : "";
	    
	    //Removed old url array code block and added code to get parameter values for 27_Jan_2016, By Thiyagu G. End
	    String gamUcasCode = "";
	    //Added this showing if no result for user selected filter from Chatbot 23_Aug_2018, By Sabapathi.S
	    if("N".equalsIgnoreCase(p_recentFilter)){
	       request.setAttribute("noResultFlag", "Y");
	       p_recentFilter = "";
	    }
	    //
	    //Added code for redirecting 404 page if the url doesn't have the below listed parameters for 27_Jan_2016, By Thiyagu G. Start.
	    boolean qry404Flag = true;
	    String jacsSubjectName = "";
	    if (URL_QUR_STRING.contains("q=")) {
	      qry404Flag = false;
	    }
	    if (URL_QUR_STRING.contains("subject=")) {
	      qry404Flag = false;
	    }
	    if (URL_QUR_STRING.contains("ucas-code=")) {
	      qry404Flag = false;
	    }
	    if (URL_QUR_STRING.contains("jacs=")) {
	      qry404Flag = false;
	    }
	    if (qry404Flag) {
	      request.setAttribute("reason_for_404", "--(1.1)--> URL doesn't have q, subject, ucas code and jacs code");
	      response.sendError(404, "404 error message");
	      return null;
	    }
	    //Added code for redirecting 404 page if the url doesn't have the below listed parameters for 27_Jan_2016, By Thiyagu G. End.
	    //Merged entry level and entry points code from KeywordSearchAction for 27_Jan_2016, By Thiyagu G. Start
	    if (!"".equalsIgnoreCase(entryLevel)) {
	      request.setAttribute("entryGradeFilter", "YES");
	      String entryPointsFilter = new GlobalFunction().getEntryPointsForGAM(entryPoints);
	      if ("ucas_points".equalsIgnoreCase(entryLevel)) {
	        request.setAttribute("entryPoints", entryPoints);
	      } else {
	        request.setAttribute("entryPoints", entryPointsFilter);
	      }
	      if (entryLevel.equalsIgnoreCase("A_LEVEL")) {
	        request.setAttribute("entryLevelValue", "A-levels");
	        if (!entryPoints.equalsIgnoreCase("")) {
	          String entry_Point[] = entryPoints.split("-");
	          request.setAttribute("sBoxA*Value", entry_Point[0]);
	          request.setAttribute("sBoxAValue", entry_Point[1]);
	          request.setAttribute("sBoxBValue", entry_Point[2]);
	          request.setAttribute("sBoxCValue", entry_Point[3]);
	          request.setAttribute("sBoxDValue", entry_Point[4]);
	          request.setAttribute("sBoxEValue", entry_Point[5]);
	        }
	      } else if (entryLevel.equalsIgnoreCase("SQA_HIGHER")) {
	        request.setAttribute("entryLevelValue", "SQA Highers");
	        if (!entryPoints.equalsIgnoreCase("")) {
	          String entry_Point[] = entryPoints.split("-");
	          request.setAttribute("sBoxAValue", entry_Point[0]);
	          request.setAttribute("sBoxBValue", entry_Point[1]);
	          request.setAttribute("sBoxCValue", entry_Point[2]);
	          request.setAttribute("sBoxDValue", entry_Point[3]);
	        }
	      } else if (entryLevel.equalsIgnoreCase("SQA_ADV")) {
	        request.setAttribute("entryLevelValue", "SQA Advanced Highers");
	        if (!entryPoints.equalsIgnoreCase("")) {
	          String entry_Point[] = entryPoints.split("-");
	          request.setAttribute("sBoxAValue", entry_Point[0]);
	          request.setAttribute("sBoxBValue", entry_Point[1]);
	          request.setAttribute("sBoxCValue", entry_Point[2]);
	          request.setAttribute("sBoxDValue", entry_Point[3]);
	        }
	      } else if (entryLevel.equalsIgnoreCase("BTEC")) {
	        request.setAttribute("entryLevelValue", "BTEC");
	        if (!entryPoints.equalsIgnoreCase("")) {
	          String entry_Point[] = entryPoints.split("-");
	          request.setAttribute("sBoxD*Value", entry_Point[0]);
	          request.setAttribute("sBoxBtecDValue", entry_Point[1]);
	          request.setAttribute("sBoxMValue", entry_Point[2]);
	          request.setAttribute("sBoxPValue", entry_Point[3]);
	        }
	      } else {
	        request.setAttribute("entryLevelValue", "Tariff Points");
	        if (!entryPoints.equalsIgnoreCase("")) {
	          request.setAttribute("tariffPointValue", entryPoints);
	        }
	      }
	    }
	    if (!entryLevel.equalsIgnoreCase("")) {
	      request.setAttribute("entryGradeFilter", "YES");
	      String entryPointsFilter = new GlobalFunction().getEntryPointsForGAM(entryPoints);
	      if ("ucas_points".equalsIgnoreCase(entryLevel)) {
	        request.setAttribute("entryPoints", entryPoints);
	      } else {
	        request.setAttribute("entryPoints", entryPointsFilter);
	      }
	      if (entryLevel.equalsIgnoreCase("A_LEVEL")) {
	        request.setAttribute("entryLevelValue", "A-levels");
	      } else if (entryLevel.equalsIgnoreCase("SQA_HIGHER")) {
	        request.setAttribute("entryLevelValue", "SQA Highers");
	      } else if (entryLevel.equalsIgnoreCase("SQA_ADV")) {
	        request.setAttribute("entryLevelValue", "SQA Advanced Highers");
	      } else if (entryLevel.equalsIgnoreCase("BTEC")) {
	        request.setAttribute("entryLevelValue", "BTEC");
	      } else {
	        request.setAttribute("entryLevelValue", "Tariff Points");
	      }
	    }
	    //Added by Sangeeth.S for the delete filter ucas score generated through grade filter page in NOV_21_2019_rel 
	    if(!GenericValidator.isBlankOrNull(scoreValue)){
	      request.setAttribute("scoreValueGradeFilter", "YES");
	      request.setAttribute("gradeFilterUcasScorePoint", scoreValue);
	      //session.setAttribute("USER_UCAS_SCORE",scoreValue);
	    }
	    //
	    //Merged entry level and entry points code from KeywordSearchAction for 27_Jan_2016, By Thiyagu G. End
	    String gradesFlg = request.getParameter("updategradesflg");
	    if (!GenericValidator.isBlankOrNull(gradesFlg) && "YES".equalsIgnoreCase(gradesFlg)) {
	      updateGrades(request, entryLevel, entryPoints);
	    }
	    String location = "";
	    String subjectName = "";
	    String studyLevel = "";
	    String subjectId = "";
	    String subjectCode = "";
	    String country = "united-kingdom";    
	    String entityText = "1";
	    CollegeUtilities uniUtils = new CollegeUtilities();
	    CollegeNamesVO uniNames = null;    
	    try {
	      String seoSLText = common.replaceHypen(common.replaceURL(urlArray[1])); //Study level
	      String seoUrlSearchText = common.replaceHypen(common.replacePlus(searchKeyword));
	      //Merged qualification code from KeywordSearchAction for 27_Jan_2016, By Thiyagu G. Start.
	      String selQual = null;
	      if (!GenericValidator.isBlankOrNull(urlArray[1])) {
	        selQual = urlArray[1].substring(0, urlArray[1].indexOf("-courses"));
	      }
	      String qualCode = "M";	      
	      if ("degree".equals(selQual)) {
	        qualCode = "M";	        
	      } else if ("postgraduate".equals(selQual)) {
	        qualCode = "L";
	      } else if ("foundation-degree".equals(selQual)) {
	        qualCode = "A";	        
	      } else if ("access-foundation".equals(selQual)) {
	        qualCode = "T";	        
	      } else if ("hnd-hnc".equals(selQual)) {
	        qualCode = "N";	        
	      } else if ("ucas_code".equals(selQual)) {
	        qualCode = "UCAS_CODE";
	      } else if ("Clearing".equals(selQual)) {
	        qualCode = "M";	        
	      } else { //301 redirection for the not matched and in valid qual code to degrees for Jun_6_18 release by Sangeeth.S
	          String redirectUrlPath = URL_STRING;
	          String redirectURL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN  + redirectUrlPath.replaceFirst(selQual,"degree") + "?" + URL_QUR_STRING;          
	          response.sendRedirect(redirectURL);
	          return null;
	      }
	      String studyLevelTextForEnterGrade = utilities.getStudyLevelText(selQual);
	      request.setAttribute("studyLevelTextForEnterGrade", studyLevelTextForEnterGrade);
	      request.setAttribute("selected_qual", selQual);
	      //Merged qualification code from KeywordSearchAction for 27_Jan_2016, By Thiyagu G. End.
	      location = ((!GenericValidator.isBlankOrNull(p_loc)) && (!"".equals(p_loc))) ? p_loc.trim() : common.replacePlus(country);      
	      searchBean.setSubjectName(p_subject); 
	      searchBean.setCourseSearchText(searchKeyword);
	      searchBean.setStudyLevelId(qualCode != null && !"".equals(qualCode.trim()) ? utilities.toUpperCase(qualCode.trim()) : "");
	      searchBean.setOrderId(sortOrder != null && !"0".equals(sortOrder.trim()) ? utilities.toUpperCase(sortOrder.trim()) : "");     
	      String regionTitleCase = GenericValidator.isBlankOrNull(location) ? "" : new CommonUtil().toTitleCase(location.trim());
	      searchBean.setRegionId(regionTitleCase.trim());
	      searchBean.setPostCode(location);      
	      if(!GenericValidator.isBlankOrNull(ucasCode)){
	        gamUcasCode = ucasCode;
	        String tmpUcasCode = common.replaceHypenWithSpace(ucasCode);
	        request.setAttribute("ucasCodepresent", tmpUcasCode);
	        session.removeAttribute("ucasCodepresent");
	        session.setAttribute("ucasCodepresent", tmpUcasCode);        
	        ucasCode = global.checkValidUCASCode(tmpUcasCode); //TODO DB_CALL - combine to SPRING
	        if (ucasCode != null && ucasCode.equalsIgnoreCase("TRUE")) {
	          ucasCode = tmpUcasCode;
	          searchBean.setUcasCode(ucasCode);
	          request.setAttribute("paramUcasCode", ucasCode);
	          isThisUcasSearch = true;
	        } else {
	          ucasCode = "";
	          searchBean.setUcasCode("");
	        }
	        //nullify unsed objects for this scope
	        tmpUcasCode = null;
	      }
	      // URL having greater london insteed of london.        
	      if (location != null && location.equalsIgnoreCase("greater london")) {
	        request.setAttribute("reason_for_404", "--(1.1)--> URL having greater london insteed of london");
	        response.sendError(404, "404 error message");
	        return null;
	      }
	      //
	      common.loadUserLoggedInformationForMoneyPage(request, context, response); //DB_CALL 
	      global.removeSessionData(session, request, response);
	      String sc_categoryCode = "";
	      String URL_1 = "";
	      String URL_2 = "";
	      String seoFirstPageUrl = "";      
	      URL_1 = URL_STRING;            
	      seoFirstPageUrl = URL_1 + "?" + URL_QUR_STRING;
	      request.setAttribute("URL_STRING", URL_STRING);
	      request.setAttribute("URL_1", URL_1);
	      request.setAttribute("URL_2", URL_2);
	      //Latest canonical url modification for the November_21_2019 release
	      String modCanonicalUrl = URL_1 + "?";
	      if(!GenericValidator.isBlankOrNull(p_q)){
	        modCanonicalUrl = modCanonicalUrl + "q="+p_q;
	      }else{
	        modCanonicalUrl = modCanonicalUrl + "subject="+p_subject;  
	      }
	      if(!GenericValidator.isBlankOrNull(p_loc)){
	        modCanonicalUrl = modCanonicalUrl + "&location="+p_loc;  
	      }	      
	      request.setAttribute("modCanonicalUrl", modCanonicalUrl);//latest canonical url modification for the SR page
	      //
	      //
	      request.setAttribute("SEO_FIRST_PAGE_URL", seoFirstPageUrl);// Didnt modified this attribute since it is used various place like(sorting url,etc) apart from seo purpose
	      //
	      if (pageno != null && Integer.parseInt(pageno) > 1) {
	        request.setAttribute("noindex", "true");
	      }
	      //Setting noindex based on the parameters for 27_Jan_2016, By Thiyagu G. Start.
	      if (!GenericValidator.isBlankOrNull(p_q) || !GenericValidator.isBlankOrNull(ucasCode) || !GenericValidator.isBlankOrNull(p_jacsCode)) {
	        request.setAttribute("noindex", "true");
	      }
	      if (!GenericValidator.isBlankOrNull(request.getQueryString())) {
	        if(!URL_QUR_STRING.contains("clearing") && !URL_QUR_STRING.contains("subject=") && !URL_QUR_STRING.contains("location=")){
	          request.setAttribute("noindex", "true");
	        }
	        if(URL_QUR_STRING.contains("russell-group=") || URL_QUR_STRING.contains("campus-type=") || URL_QUR_STRING.contains("study-mode=") || URL_QUR_STRING.contains("postcode=") 
	        || URL_QUR_STRING.contains("employment-rate-max=") || URL_QUR_STRING.contains("ucas-points-max=") || URL_QUR_STRING.contains("sort=") || URL_QUR_STRING.contains("jacs=")
	        || URL_QUR_STRING.contains("ucas-code=") || URL_QUR_STRING.contains("location-type=") || URL_QUR_STRING.contains("module=") || URL_QUR_STRING.contains("assessment-type=")
	        || URL_QUR_STRING.contains("your-pref=") || URL_QUR_STRING.contains("entry-level=") || URL_QUR_STRING.contains("ip=")){ //Added condition to no index for the Oct_23_18 rel by sangeeth.S
	          request.setAttribute("noindex", "true");
	        }
	      }
	      //Setting noindex based on the parameters for 27_Jan_2016, By Thiyagu G. End.
	      if (!GenericValidator.isBlankOrNull(sortOrder)) {
	        if (!("r").equals(sortOrder)) {
	          request.setAttribute("noindex", "true");
	        }
	      }   
	      subjectName = searchBean.getSubjectName();
	      location = location != null ? utilities.toUpperCase(location) : location;
	      studyLevel = searchBean.getStudyLevelId();
	      studyLevel = studyLevel != null ? utilities.toUpperCase(studyLevel) : studyLevel;      
	      subjectId = searchBean.getSubjectId();
	      subjectId = subjectId != null ? utilities.toUpperCase(subjectId) : subjectId;
	      subjectCode = searchBean.getSubjectCode();
	      subjectCode = subjectCode != null ? utilities.toUpperCase(subjectCode) : subjectCode;      
	      if (studyLevel != null && studyLevel.trim().length() > 0 && studyLevel.equalsIgnoreCase("L")) {
	        String browseId = global.getBrowseTypeId(subjectCode); //DB_CALL
	        if (browseId != null && browseId.trim().length() > 0 && browseId.equals("3")) {
	          request.setAttribute("reason_for_404", "--(4)--> Redirect to 404 page for the postgraduate categodyid which belongs to hotcourses");
	          response.sendError(404, "404 error message");
	          return null;
	        }
	      }      
	      String commonCagtegoryName = subjectName;
	      commonCagtegoryName = subjectName; //DB_CALL
	      commonCagtegoryName = common.replaceHypen(common.replaceURL(commonCagtegoryName));
	      String studyLevelDesc = new CommonFunction().seoStudyLevelDescription(studyLevel, "courses", "SEARCH_LIST"); //common.getStudyLevelDesc(studyLevel, request);
	      studyLevelDesc = studyLevelDesc != null && !studyLevelDesc.equals("null") && studyLevelDesc.trim().length() > 0 ? studyLevelDesc.replaceAll("-courses", "") : "";      
	      String searchHow = sortOrder;
	      request.setAttribute("sortByValue", searchHow);
	      searchHow = searchHow != null ? utilities.toUpperCase(searchHow) : searchHow;
	      String searchtype = "/page.html"; //24_JUN_2014
	      if (("clcourse").equals(pageName)) {
	        searchtype = "/clcourse.html";
	      }
	      String orderByUrl = GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + seoSLText + "/" + seoUrlSearchText + "/" + studyLevel + "/" + (location != null && location.length() > 0 ? location.replaceAll(" ", "+") : "0") + "/" + searchHow + "/" + subjectCode + searchtype;      
	      String collegeName = ""; 
	      String collegeNameDisplay = "";
	      String collegeId = searchBean.getCollegeId();
	      uniNames = uniUtils.getCollegeNames(collegeId, request);
	      if (uniNames != null) {
	        collegeName = uniNames.getCollegeName();
	        collegeNameDisplay = uniNames.getCollegeNameDisplay();
	      }
	      collegeName = collegeName != null && collegeName.trim().equalsIgnoreCase("Enter uni name here") ? "" : collegeName;
	      String postCode = searchBean.getPostCode();
	      postCode = postCode != null && postCode.trim().equalsIgnoreCase("Enter postcode or town here") ? "" : postCode;
	      String courseSearchText = searchBean.getCourseSearchText();
	      courseSearchText = courseSearchText != null && courseSearchText.trim().equalsIgnoreCase("Enter course keyword") ? "" : courseSearchText;
	      String studyLevelValue = searchBean.getStudyLevelId();      
	      String locationValue = searchBean.getRegionId();
	      String postCodeText = postCode;
	      String countyId = searchBean.getCountyId();
	      String loc_id = "";
	      if (countyId != null && countyId.trim().length() > 0) {
	        locationValue = postCodeText != null && postCodeText.trim().length() > 0 ? utilities.toUpperCase(postCodeText) : postCodeText;
	        loc_id = postCodeText != null && postCodeText.trim().length() > 0 ? utilities.toUpperCase(postCodeText.trim()) + "|" + (countyId != null ? countyId.trim() : countyId) : "";
	      }
	      if (loc_id != null && loc_id.trim().length() > 0) {
	        postCodeText = "";
	      }
	      if (ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0) {
	        courseSearchText = "";
	      }
	      String x = new SessionData().getData(request, "x");
	      x = x != null && !x.equals("0") && x.trim().length() >= 0 ? x : "16180339";      
	      String y = new SessionData().getData(request, "y");
	      y = y != null && !y.equals("0") && y.trim().length() >= 0 ? y : "0";      
	      String userID = y;
	      userID = userID != null && !userID.equals("0") && userID.trim().length() >= 0 ? userID : "";
	      String basketID = GenericValidator.isBlankOrNull(common.checkCookieStatus(request)) ? null : common.checkCookieStatus(request);
	      //      
	      String qualification = studyLevel;
	      model.addAttribute("qualification1", qualification);
	      SearchVO parameterVO = new SearchVO();
	      String userAgent = request.getHeader("user-agent");
	      String subjectSessionId = !GenericValidator.isBlankOrNull((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
	      String clientIp = GenericValidator.isBlankOrNull(request.getParameter("ip")) ? new RequestResponseUtils().getRemoteIp(request) : (request.getParameter("ip"));//Added by Sangeeth.S for the 280420 release
	      parameterVO.setX(x);
	      parameterVO.setY(y);
	      parameterVO.setSearchWhat("Z"); // SEARCH TYPE Z-COLLEGE O-COURSES       
	      parameterVO.setPhraseSearch(p_q);
	      parameterVO.setCollegeName(""); // COLLEGE NAME            
	      parameterVO.setQualification(qualification); // STYDY LEVEL  - DEFAULT = M,N,A,T,L  /// M,N,A,T,L [OR] M  [OR] N [OR] A [OR] T [OR] L
	      parameterVO.setTownCity(location); // LOCATION       (BATH)
	      parameterVO.setPostcode(p_postCode); // SEARCH POST CODE TEXT
	      parameterVO.setSearchRange(p_distance); // RANGE TO SEARCH DEFAULT 25      
	      parameterVO.setStudyMode(p_study_mode); // STYDY MODE DEFAULT NULL must be // "A1"  [OR] "A2,A21,A22,A3,A23,A4,A5"  [OR] B,B1,B2,B3,B4,B5,B6,C,C1,C2,C3,C4,C5,C6"  [OR] "A6"
	      parameterVO.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID); // AFFLIATE ID 
	      parameterVO.setSearchHOW(searchHow); // DEFAULT 'R' VALUE MAY BE       
	      parameterVO.setSearchCol("");
	      parameterVO.setEntityText(entityText);
	      parameterVO.setUcasCode(new CommonUtil().toUpperCase(ucasCode));
	      parameterVO.setBasketId(basketID);
	      parameterVO.setPageNo(pageno);
	      parameterVO.setPageName(searchType);
	      parameterVO.setEntryLevel(entryLevel);
	      parameterVO.setEntryPoints(entryPoints);
	      parameterVO.setUserAgent(userAgent);
	      parameterVO.setSearchCategory(p_subject);
	      parameterVO.setModuleStr(p_module);
	      parameterVO.setEmpRate(p_emp_rate);
	      parameterVO.setCampusTypeName(p_campus_type);
	      parameterVO.setUnivLocTypeName(p_loc_type);
	      parameterVO.setRusselGroupName(p_russel);
	      if(!GenericValidator.isBlankOrNull(scoreValue)){
	        parameterVO.setUcasTariff(scoreValue);
	      }else{
	        parameterVO.setUcasTariff(p_ucas_tariff);
	      }
	      parameterVO.setBasketId(basketID);
	      parameterVO.setJacsCode(p_jacsCode);
	      parameterVO.setRequestURL(currentPageUrl); //8_OCT_2014 Added by Priyaa for What have you done pod
	      parameterVO.setUserTrackSessionId(new SessionData().getData(request, "userTrackId"));
	      //Set 2 new Parameters for DB call 23_Aug_2018, By Sabapathi.S
	      parameterVO.setAssessmentType(assessmentTypeFilter);
	      parameterVO.setReviewSubjects(reviewCategoryFilter);
	      parameterVO.setSubjectSessionId(subjectSessionId);
	      parameterVO.setRandomNumber(utilities.getSessionRandomNumber(request, session)); //Added by Sangeeth.S for the 030320 release
	      parameterVO.setClientIp(clientIp);
	      if(!GenericValidator.isBlankOrNull(p_jacsCode)){
	        parameterVO.setUcasCode("");
	        parameterVO.setPhraseSearch("");
	        parameterVO.setSearchCategory("");
	      }      
	      boolean searchResultFlag = false;
	      if (!GenericValidator.isBlankOrNull(p_recentFilter)) {
	        parameterVO.setRecentFilterApplied(p_recentFilter.toUpperCase());
	        request.setAttribute("recentFilter", p_recentFilter.toUpperCase());
	      }      
	      ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
	      Map rsMap = null;
	      try {
	        rsMap = searchBusiness.getSearchResults(parameterVO);
	      } catch (Exception sqlException) {
	        String errorMessage = sqlException.getMessage();
	        if (errorMessage != null) {
	          errorMessage = errorMessage.replaceAll("ORA-20001: ", "");
	          String errorCode[] = errorMessage.split(":");
	          request.setAttribute("SearchErrorMessage", errorMessage);
	          if (errorCode != null && errorCode.length > 2) {
	            request.setAttribute("SearchErrorCode", errorCode[2]);
	          }
	        }
	      }
	      if (request.getAttribute("SearchErrorCode") != null) {
	        searchResultFlag = true;
	        return new ModelAndView("errorcodepage");
	      }
	      ArrayList searchResultList = null;
	      if(rsMap == null){
	        request.setAttribute("reason_for_404", "--(5)--> Redirect to 404 page for empty results");
	        response.sendError(404, "404 error message");
	        return null;
	      }else if(rsMap != null && !rsMap.isEmpty()){
	        //Roll up search for wu557_29092016, By Thiyagu G. Start
	        String invalidCategoryFlag = (String)rsMap.get("p_invalid_category_flag"); 
	        if (!GenericValidator.isBlankOrNull(invalidCategoryFlag) && ("Y").equals(invalidCategoryFlag)) {          
	          String NEW_URL_QUR_STRING = "";          
	          if(!GenericValidator.isBlankOrNull(p_subject)){NEW_URL_QUR_STRING = (URL_QUR_STRING.indexOf("clearing") > -1) ? "?clearing&q="+ p_subject : "?q=" + p_subject;}          
	          String redirectingKeywordURL = URL_STRING + NEW_URL_QUR_STRING;
	                   
	          response.sendRedirect(redirectingKeywordURL);          
	          return null;
	        } 
	        //Roll up search for wu557_29092016, By Thiyagu G. End
	        String cDimCollegeIds = "";
	        searchResultList = (ArrayList)rsMap.get("pc_search_results");
	        List<CourseSpecificSearchResultsVO> searchArrayList = searchResultList;
	        if (searchArrayList != null && searchArrayList.size() > 0) {
	        String instIds = searchArrayList.stream().map(ins -> ins.getCollegeId()).collect(Collectors.joining("###"));
		   	String instNames = searchArrayList.stream().map(ins -> ins.getCollegeName()).collect(Collectors.joining("###"));		   	
		   	searchArrayList.stream().forEach(srchList -> srchList.setEventCategoryNameGa(utilities.setOpenDayEventGALabel(srchList.getEventCategoryName())));		   	 
		   	String openDayEventTypeDim18 = searchArrayList.stream().filter(ins -> !GenericValidator.isBlankOrNull(ins.getEventCategoryName())).map(ins -> ins.getEventCategoryName()).distinct().collect(Collectors.joining("|"));		   	
		   	request.setAttribute("openDayEventType", openDayEventTypeDim18);//For logging open day event type in dimension 18 by sangeeth.S
		   	/*Added the session for sponsored list and manual boosting GA logging by Sujitha V on 03_03_3030*/
		   	String sponsoredListing = "SPONSORED_LISTING";
		   	String manualBoosting = "MANUAL_BOOSTING";
		   	String sponsoredInstID = searchArrayList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getCollegeId).collect(Collectors.joining(""));
		   	String boostedInstID = searchArrayList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getCollegeId).collect(Collectors.joining(""));
		    String sponsoredInstFlag = searchArrayList.stream().filter(sponsoredFlag -> sponsoredListing.equalsIgnoreCase(sponsoredFlag.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getSponsoredListFlag).collect(Collectors.joining(""));
		   	String manualInstFlag = searchArrayList.stream().filter(manualFlag -> manualBoosting.equalsIgnoreCase(manualFlag.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getSponsoredListFlag).collect(Collectors.joining(""));
		   	String sponsoredInstName = searchArrayList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getCollegeName).collect(Collectors.joining(""));
		   	String boostedInstName = searchArrayList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getSponsoredListFlag())).map(CourseSpecificSearchResultsVO :: getCollegeName).collect(Collectors.joining(""));
		   	session.setAttribute("sponsoredInstId", sponsoredInstID);
		   	session.setAttribute("boostedInstId", boostedInstID); 
		    model.addAttribute("sponsoredListFlag", sponsoredInstFlag);
		    model.addAttribute("manualBoostingFlag", manualInstFlag);
		    model.addAttribute("sponsoredInstName", sponsoredInstName);
		    model.addAttribute("boostedInstName", boostedInstName);
		   	/*End of ga logging session*/
		   	   
		    model.addAttribute("instIds", instIds);
		   	model.addAttribute("instNames", instNames);
	        }
	        if (searchResultList != null && searchResultList.size() > 0) {
	          request.setAttribute("listOfSearchResults", searchResultList);
	          Iterator csItr = searchResultList.iterator();
	          ArrayList bmList = null;
	          while (csItr.hasNext()) {
	            CourseSpecificSearchResultsVO csVO = (CourseSpecificSearchResultsVO)csItr.next();
	            if(!GenericValidator.isBlankOrNull(csVO.getJacsSubject())){
	              request.setAttribute("jacsSubjectName", csVO.getJacsSubject());
	              jacsSubjectName = csVO.getJacsSubject();
	            }
	            bmList = csVO.getBestMatchCoursesList();
	            break;
	          }
	          /*Added by Prabha for smart pixel tagging on 20_Feb_2018*/
	          String cDimCollegeDisName = "";
	          csItr = searchResultList.iterator();
	          while (csItr.hasNext()) {
	            CourseSpecificSearchResultsVO csVO = (CourseSpecificSearchResultsVO)csItr.next();
	            if(!GenericValidator.isBlankOrNull(csVO.getCollegeName())){
	              cDimCollegeDisName += "\"" + csVO.getCollegeName() + "\", ";
	            }
	            //Added by sangeeth.S for smart pixel 
	            if(!GenericValidator.isBlankOrNull(csVO.getProviderId())){
	              cDimCollegeIds += "'" + csVO.getProviderId() + "', ";
	            }            
	          }
	          if(!GenericValidator.isBlankOrNull(cDimCollegeIds)){
	            cDimCollegeIds = cDimCollegeIds.substring(0, cDimCollegeIds.length()-2);              
	            cDimCollegeIds = "[" + cDimCollegeIds + "]";              
	            request.setAttribute("cDimCollegeIds", cDimCollegeIds);
	          }
	          //
	          if(!GenericValidator.isBlankOrNull(cDimCollegeDisName)){
	            cDimCollegeDisName = cDimCollegeDisName.substring(0, cDimCollegeDisName.length()-2);
	            if(searchResultList != null && searchResultList.size() > 1){
	              cDimCollegeDisName = ("[" + cDimCollegeDisName + "]");
	            }
	            request.setAttribute("cDimCollegeDisName", cDimCollegeDisName);
	          }
	          /*End of smart pixel logging*/
	          if (bmList != null && bmList.size() > 0) {
	            BestMatchCoursesVO bmVO = (BestMatchCoursesVO)bmList.get(0);
	            if (!GenericValidator.isBlankOrNull(bmVO.getModuleGroupId())) {
	              request.setAttribute("first_mod_group_id", bmVO.getModuleGroupId());
	            }
	          }          
	        }
	      
	        if ((searchResultList == null) || (searchResultList != null && searchResultList.size() == 0)) {
	          searchResultFlag = true;
	          // this content is added for 04 Feb 2009 release to show the exact error message in the message page 
	          if (postCodeText != null && postCodeText.trim().length() > 0) {
	            request.setAttribute("err_location", (postCodeText != null? common.getTitleCaseFn(postCodeText): postCodeText));
	          }
	          if (locationValue != null && locationValue.trim().length() > 0) {
	            request.setAttribute("err_location", (location != null? utilities.toTitleCase(common.replaceHypenWithSpace(location)).trim(): location));
	          }
	          String countyName = "";
	          if (countyId != null && countyId.trim().length() > 0) {
	            countyName = String.valueOf(common.getCountyName(postCode, countyId)); //DB_CALL
	            if (countyName != null && countyName.trim().length() > 0) {
	              String couNamePostCode = (postCode != null && !postCode.equalsIgnoreCase("null") && postCode.trim().length() > 0? common.getTitleCaseFn(postCode): "");
	              if (couNamePostCode != null && couNamePostCode.trim().length() > 0) {
	                request.setAttribute("err_location", (couNamePostCode != null? common.getTitleCaseFn(couNamePostCode): couNamePostCode));
	              }
	            }
	          }
	          //15-July-2014 
	          session.removeAttribute("err_subject_name");
	          session.removeAttribute("err_study_level_id");
	          session.removeAttribute("err_study_level_name");
	          session.removeAttribute("keyword");
	          if (subjectName != null && subjectName.trim().length() > 0) {
	            request.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(subjectName) ? utilities.toTitleCase(common.replaceHypenWithSpace(subjectName)).trim() : "");
	            session.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(subjectName) ? utilities.toTitleCase(common.replaceHypenWithSpace(subjectName)).trim() : "");          
	            session.setAttribute("keyword", !GenericValidator.isBlankOrNull(subjectName) ? utilities.toTitleCase(common.replaceHypenWithSpace(subjectName)).trim() : "");
	          } else {
	            request.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(courseSearchText) ? utilities.toTitleCase(common.replaceHypenWithSpace(courseSearchText)).trim() : "");
	            session.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(courseSearchText) ? utilities.toTitleCase(common.replaceHypenWithSpace(courseSearchText)).trim() : "");
	            session.setAttribute("keyword", !GenericValidator.isBlankOrNull(courseSearchText) ? utilities.toTitleCase(common.replaceHypenWithSpace(courseSearchText)).trim() : "");
	          }
	          if (studyLevel != null && studyLevel.trim().length() > 0) {
	            request.setAttribute("err_study_level_id", studyLevel);
	            session.setAttribute("err_study_level_id", studyLevel);
	          }
	          String studyLevelName = studyLevelValue != null && studyLevelValue.trim().length() > 0 && !studyLevelValue.equalsIgnoreCase("M,N,A,T,L")? common.getStudyLevelDesc(studyLevelValue, request): ""; //DB_CALL
	          if (studyLevelName != null && studyLevelName.trim().length() > 0) {
	            request.setAttribute("err_study_level_name", studyLevelName);
	            session.setAttribute("err_study_level_name", studyLevelName);
	          }
	          if (collegeName != null && collegeName.trim().length() > 0) {
	            request.setAttribute("err_provider_name", collegeName);
	            session.removeAttribute("err_provider_name");
	            session.setAttribute("err_provider_name", collegeName);
	          }
	          request.setAttribute("collegeId", collegeId);
	          request.setAttribute("uniSearchCollegeId", collegeId);
	          request.setAttribute("providerResult", "true");
	          request.setAttribute("otherindex", "noindex,nofollow");
	          //24-JUN-2014
	          session.removeAttribute("err_location");
	          session.removeAttribute("collegeId");
	          session.removeAttribute("providerResult");
	          session.setAttribute("err_location", request.getAttribute("err_location"));
	          session.setAttribute("collegeId", collegeId);
	          session.setAttribute("providerResult", "true");
	          if (!GenericValidator.isBlankOrNull(p_q)) {
	            //////////////////////////////////////////////// SPELLING SUGGESTIONS  //////////////////////////////////////////////////////////////
	            CommonValidator validate = new CommonValidator();
	            String tmpKeyword = validate.isNotBlankAndNotNull(subjectName)? subjectName: courseSearchText;
	            ArrayList listOfSpellingSuggestions = searchUtil.getSpellingSuggestions(tmpKeyword, studyLevelValue); //DB_CALL        
	            if (validate.isNonEmptyList(listOfSpellingSuggestions)) {
	              request.setAttribute("listOfSpellingSuggestions", listOfSpellingSuggestions);
	              //return mapping.findForward("spellingsuggestions");
	              //15-July-2014 
	              session.removeAttribute("listOfSpellingSuggestions");
	              session.setAttribute("listOfSpellingSuggestions", listOfSpellingSuggestions);
	              response.sendRedirect("/degrees/did-you-mean.html");
	            }
	          }
	        }      
	        if (searchResultFlag) {
	          request.setAttribute("providerResult", "true");
	          if ("clcourse".equalsIgnoreCase(pageName) || "cluniview".equalsIgnoreCase(pageName)) {
	            request.setAttribute("clearingSearchBar", "YES");
	            session.removeAttribute("clearingSearchBar");
	            session.setAttribute("clearingSearchBar", "YES");
	          }        
	          session.removeAttribute("err_location");
	          session.removeAttribute("providerResult");
	          session.setAttribute("err_location", request.getAttribute("err_location"));
	          session.setAttribute("providerResult", "true");         
	          //Added by Indumathi Jan-27-16 rel For 404 redirection the URL to parent
	          String parentCatDesc = (String)rsMap.get("p_parent_cat_text_key");
	          if (!GenericValidator.isBlankOrNull(parentCatDesc)) {
	            String queryString = request.getQueryString();
	            String newURL = "";
	            if (!GenericValidator.isBlankOrNull(queryString)) {
	              String splittedURL[] = queryString.split("&");
	              for (int i = 0; i < splittedURL.length; i++) {
	                if (splittedURL[i].indexOf("subject") > -1) {
	                  splittedURL[i] = "subject=" + parentCatDesc;
	                }
	                newURL += splittedURL[i];
	                if (i + 1 != splittedURL.length) {
	                  newURL += "&";
	                }
	              }
	            }
	            String redirectingBrowseURL = URL_STRING + "?" + newURL;
	            response.sendRedirect(redirectingBrowseURL);
	           } else {
	             response.sendRedirect(request.getContextPath() + "/notfound.html");
	           }        
	        }
	        String resultExists = (String)rsMap.get("p_result_exists");
	        request.setAttribute("resultExists", resultExists);      
	        if (!GenericValidator.isBlankOrNull(resultExists) && ("N").equals(resultExists)) {
	          if (!GenericValidator.isBlankOrNull("p_recentFilter") && ("m".equalsIgnoreCase(p_recentFilter))) {
	            String filterModule = searchutil.appendFilterVlaues("module", "", request);
	            request.setAttribute("queryStr", filterModule);
	            request.setAttribute("removeModuleParameter", "YES");
	          } else if (!GenericValidator.isBlankOrNull("p_recentFilter") && ("p".equalsIgnoreCase(p_recentFilter))) {
	            String filterModule = searchutil.appendFilterVlaues("postcode", "", request);
	            request.setAttribute("queryStr", filterModule);
	            request.setAttribute("removePostcodeParameter", "YES");
	          }
	        }      
	        int searchCount1 = Integer.parseInt(rsMap.get("p_record_count").toString());
	        String searchCount = new Integer(searchCount1).toString();
	        request.setAttribute("universityCount", searchCount);      
	        //Removed unwanted parameters and generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. Start
	        ArrayList studyModeList = (ArrayList)rsMap.get("pc_study_mode_refine");
	        if (studyModeList != null && studyModeList.size() > 0) {
	          request.setAttribute("studyModeList", searchutil.customiseStudyModeListForSR(URL_1, studyModeList, request, searchCount, resultExists, p_recentFilter, ""));
	        }
	        ArrayList qualList = (ArrayList)rsMap.get("pc_qualification_refine");
	        if (qualList != null && qualList.size() > 0) {
	          request.setAttribute("qualList", searchutil.customiseStudyLevelListForSR(URL_1, qualList, request, searchCount, resultExists, p_recentFilter, ""));
	        }
	        ArrayList subjectList = (ArrayList)rsMap.get("pc_subject_refine");
	        if (subjectList != null && subjectList.size() > 0) {
	          request.setAttribute("subjectList", searchutil.customiseSubjectListForSR(URL_1, subjectList, request, searchCount, resultExists, p_recentFilter, ""));
	        }
	        ArrayList locationList = (ArrayList)rsMap.get("pc_location_refine");
	        if (locationList != null && locationList.size() > 0) {
	          request.setAttribute("locationRefineList", searchutil.customiseLocationListForSR(URL_1, locationList, request, searchCount, resultExists, p_recentFilter, "")); //24_JUN_2014
	        }
	         //Added this for forming Filter URL 23_Aug_2018, By Sabapathi.S
	       ArrayList assesmentTypeList = (ArrayList)rsMap.get("pc_assessment_type_filter");
	       if (assesmentTypeList != null && assesmentTypeList.size() > 0) {
	         request.setAttribute("assesmentTypeList", searchutil.customiseAssessedListForSR(URL_1, assesmentTypeList, request, searchCount, resultExists, p_recentFilter, "")); //24_JUN_2014
	       }
	         //Added this for forming Filter URL 23_Aug_2018, By Sabapathi.S
	        ArrayList reviewCategoryList = (ArrayList)rsMap.get("pc_review_category_filter");
	        if (reviewCategoryList != null && reviewCategoryList.size() > 0) {
	          request.setAttribute("reviewCategoryList", searchutil.customiseReviewCatListForSR(URL_1, reviewCategoryList, request, searchCount, resultExists, p_recentFilter, "")); //24_JUN_2014
	        }
	        //Removed unwanted parameters and generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. End
	        ArrayList pcrefineList = (ArrayList)rsMap.get("pc_refines");
	        if (pcrefineList != null && pcrefineList.size() > 0) {
	          searchutil.optimiseRefineListForSR(pcrefineList, URL_1, request, searchCount, resultExists, p_recentFilter, ""); //
	        }
	        ArrayList collegeIdsForThisSearchVO = (ArrayList)rsMap.get("pc_search_college_ids");
	        if (collegeIdsForThisSearchVO != null && collegeIdsForThisSearchVO.size() > 0) {
	          request.setAttribute("collegeIdsForThisSearchVO", collegeIdsForThisSearchVO);
	        }      
	        
	        ArrayList institutionIdsForThisSearchVO = (ArrayList)rsMap.get("pc_search_institution_ids");
	              
	        String courseRankFoundFlag = (String)rsMap.get("p_course_rank_found_flag"); //15_JUL_2014
	        if (courseRankFoundFlag != null && ("N").equals(courseRankFoundFlag)) {
	          request.setAttribute("courseRankFoundFlag", courseRankFoundFlag);
	        }      
	        
	        int totalCourseCount = Integer.parseInt(rsMap.get("p_total_course_count").toString());
	        String totalCourseCount1 = new Integer(totalCourseCount).toString();
	        request.setAttribute("totalCourseCount", totalCourseCount1);      
	        String clearingBrowseLink = (String)rsMap.get("p_reg_clear_result_exist_flag");
	        request.setAttribute("SHOW_SWITCH_FLAG", clearingBrowseLink);
	        String cleraringBrowseURL = "";
	        if (!GenericValidator.isBlankOrNull((String)rsMap.get("p_parent_cat_desc"))) {
	          int parentCatID = Integer.parseInt(rsMap.get("p_parent_cat_id").toString());
	          String parentCat = new Integer(parentCatID).toString();
	          if ((parentCat != null)) {
	            int parentCatIdInt = Integer.parseInt(rsMap.get("p_parent_cat_id").toString());
	            String catCode = (String)rsMap.get("p_parent_cat_text_key");
	            String parentCatDesc = (String)rsMap.get("p_parent_cat_desc");
	            String parentCatId = new Integer(parentCatIdInt).toString();
	            if (!GenericValidator.isBlankOrNull(parentCatId)) {
	              request.setAttribute("parentCatId", parentCatId);
	              request.setAttribute("parentCatDesc", parentCatDesc);
	              request.setAttribute("catCode", catCode);
	            }
	          }
	        }
	        String defaultModuleTitle = (String)rsMap.get("p_suggested_module_text");
	        if (!GenericValidator.isBlankOrNull(defaultModuleTitle)) {
	          request.setAttribute("defaultModuleTitle", defaultModuleTitle);
	        }
	        //Get the stats logging details, 27_JAN_2016 By Thiyagu G
	        String browseCatFlag = "";
	        String browseCatDesc = "";
	        String studyModeDesc = "";
	        ArrayList statsLog = (ArrayList)rsMap.get("pc_stats_log");
	        if (statsLog != null && statsLog.size() > 0) {
	          request.setAttribute("statsLog", statsLog);
	          Iterator statsItr = statsLog.iterator();
	          while (statsItr.hasNext()) {
	            RefineByOptionsVO statsVO = (RefineByOptionsVO)statsItr.next();          
	            browseCatFlag = statsVO.getBrowseCatFlag();          
	            sc_categoryCode = statsVO.getBrowseCatId();
	            subjectCode =  !GenericValidator.isBlankOrNull(statsVO.getBrowseCatCode()) ? statsVO.getBrowseCatCode() : "";
	            browseCatDesc = statsVO.getBrowseCatDesc();
	            studyModeDesc = statsVO.getStudyModeDesc();          
	          }
	        }
	        //Added subject session id for 17-Dec-2019 release by Jeyalakshmi.D for generating session id entering in SR page.
	        subjectSessionId = (String)rsMap.get("p_subj_grades_session_id");
	        session.setAttribute("subjectSessionId" , subjectSessionId);
	        //Added qualification exist flag for the enter and edit qualification button for grade filter page by Sangeeth.S for NOV_21_19 Rel
	        String userQualificationExistFlag = (String)rsMap.get("p_user_qual_exist_flag");
          if (!GenericValidator.isBlankOrNull(userQualificationExistFlag)) {
            request.setAttribute("userQualificationExistFlag", userQualificationExistFlag);
          }
          
          String sponsoredOrderItemId= (String) rsMap.get("p_spons_order_item_id");
          
          if(StringUtils.isNotBlank(sponsoredOrderItemId)){
	        request.setAttribute("sponsoredOrderItemId", sponsoredOrderItemId);
          } else {
        	request.setAttribute("sponsoredOrderItemId", "");  
          }

	        //
            ArrayList featuredBrandList = (ArrayList)rsMap.get("pc_featured_brand");
            if (featuredBrandList != null && featuredBrandList.size() > 0) {
              FeaturedBrandVO featuredBrandVO = (FeaturedBrandVO) featuredBrandList.get(0);
              request.setAttribute("featuredCollegeName", featuredBrandVO.getCollegeName());
              request.setAttribute("featuredBrandList", featuredBrandList);
              //System.out.println("featuredBrandList----->" + featuredBrandList.size());
            }	       
            searchBean.setSubjectId(subjectCode);
	        searchBean.setSubjectCode(sc_categoryCode);
	        String searchCategory = searchBean.getSubjectId(); /// new code replaced after for SEO 
	        String formCanonicalURL = null; //24-JUN-2014
	        if (!GenericValidator.isBlankOrNull(studyLevel)) {
	          if ("CLEARING".equals(searchType)) {
	            formCanonicalURL = formRelCanonical(sc_categoryCode, location, sortOrder, studyLevel, "Y");
	          } else {
	            formCanonicalURL = formRelCanonical(sc_categoryCode, location, sortOrder, studyLevel, "N");
	          }
	          if (!GenericValidator.isBlankOrNull(formCanonicalURL)) {
	            request.setAttribute("pgsCanonicalURL", formCanonicalURL);
	          }
	        }      
	       /* Commented by Prabha on 13_Jun_17
	        Map categoryAvgRating = new HashMap();
	        categoryAvgRating = searchBusiness.getCategoryAvgRating(sc_categoryCode);
	        if (categoryAvgRating != null && categoryAvgRating.size() > 0) {
	          request.setAttribute("categoryavgrating", categoryAvgRating.get("p_average_rating"));
	          request.setAttribute("categorytotalreviews", categoryAvgRating.get("p_total_reviews"));
	          request.setAttribute("categoryActualRating", categoryAvgRating.get("p_average_ratings"));
	        }
	        */
	        if (searchBean.getSubjectId() != null && searchBean.getSubjectCode() != null) {
	          searchBean.setCourseSearchText("");
	        }
	        searchutil.getCategorySponsershipClearing(request, searchBean.getCourseSearchText(), searchBean.getSubjectId(), searchBean.getSubjectCode(), "S"); //24_JUN_2014
	        request.setAttribute("clearingBrowseLink", clearingBrowseLink); //5_AUG_2014
	        if (clearingBrowseLink != null && "Y".equalsIgnoreCase(clearingBrowseLink)) {        
	          cleraringBrowseURL = "/" + utilities.toLowerCase(studyLevelDesc) + "-courses/search";
	          request.setAttribute("cleraringBrowseURL", cleraringBrowseURL);
	        }      
	        String urlString = "/" + searchBean.getSubjectId() + "/" + (studyLevel != null && !studyLevel.equalsIgnoreCase("null") && studyLevel.trim().length() > 0 ? studyLevel : "0") + "/0/" + (location != null && !location.equalsIgnoreCase("null") && location.trim().length() > 0 ? location.replaceAll(" ", "+") : "0") + "/0/0/"; //changed on 29.10.2008 for URL shortening       
	        request.setAttribute("urlString", urlString != null ? utilities.toLowerCase(urlString) : urlString);      
	        request.setAttribute("subjectId", searchBean.getSubjectId());
	        request.setAttribute("subjectName", subjectName);      
	        String subjectL1 = null;
	        String subjectL2 = null;
	        if (GenericValidator.isBlankOrNull((String)rsMap.get("p_parent_cat_desc"))) {
	        	subjectL1 = subjectName;
	        	subjectL2 = "";
	        }else{
	        	subjectL1 = (String)rsMap.get("p_parent_cat_desc");
	        	subjectL2 = subjectName;
	        }
	        request.setAttribute("subjectL1", subjectL1);
	        request.setAttribute("subjectL2", subjectL2);
	        request.setAttribute("pageno", pageno);
	        request.setAttribute("subjectCode", p_subject);      
	        request.setAttribute("campusCode", p_campus_type);
	        request.setAttribute("russellgrpId", p_russel);
	        request.setAttribute("studyModeDisplay", studyModeDesc);
	        request.setAttribute("paramstudyModeValue", studyModeDesc);    
	        String tmpQual = searchBean.getStudyLevelId() != null ? searchBean.getStudyLevelId() : "M";
	        if (pageno != null && pageno.equals("1")) {
	          String sc_prob40_qualification = searchBean.getStudyLevelId() != null ? searchBean.getStudyLevelId() : "M";
	          Vector parameters = new Vector();
	          parameters.add(sc_categoryCode); //categoryId
	          OmnitureProperties omnitureProperties = utilities.getCpeCategory(parameters);
	          String sc_prob42_keyword = omnitureProperties.getProb42_keyword() != null ? omnitureProperties.getProb42_keyword() : "";        
	          String sc_prob43_category = omnitureProperties.getProb43_cpe_category() != null ? omnitureProperties.getProb43_cpe_category() : "";
	          String sc_prob44_location = searchBean.getRegionId() != null ? searchBean.getRegionId().toUpperCase() : "";
	          if (sc_prob44_location.equalsIgnoreCase("LONDON")) {
	            sc_prob44_location = "GREATER LONDON";
	          }
	          if (sc_prob43_category != null && sc_prob43_category.trim().length() > 1) {
	            sc_prob43_category = sc_prob43_category.trim().toLowerCase();
	            sc_prob42_keyword = "";
	          } else if (sc_prob42_keyword != null && ((sc_prob43_category == null) || (sc_prob43_category != null && sc_prob43_category.trim().length() < 1))) {
	            sc_prob43_category = "";
	            sc_prob42_keyword = sc_prob42_keyword.trim().toLowerCase();
	          }
	          if (sc_prob40_qualification.equalsIgnoreCase("M") || sc_prob40_qualification.equalsIgnoreCase("N") || sc_prob40_qualification.equalsIgnoreCase("A") || sc_prob40_qualification.equalsIgnoreCase("T")) {
	            sc_prob40_qualification = "ug";
	          } else { //L or postgraduate-courses
	            sc_prob40_qualification = "pg";
	          }        
	          if(!"Y".equalsIgnoreCase(browseCatFlag)){
	            sc_prob42_keyword = searchBean.getCourseSearchText() != null? new CommonFunction().replaceSpecialCharacter(searchBean.getCourseSearchText().toLowerCase()): "";
	          }        
	          //remove previous attributes
	          session.removeAttribute("sc_prob40_qualification");
	          session.removeAttribute("sc_prob42_keyword");
	          session.removeAttribute("sc_prob43_category");
	          session.removeAttribute("sc_prob44_location");
	          //set the values into session scope      
	          request.setAttribute("sc_pageno", pageno);
	          session.setAttribute("sc_prob40_qualification", sc_prob40_qualification.toLowerCase());
	          session.setAttribute("sc_prob42_keyword", sc_prob42_keyword.toLowerCase());
	          session.setAttribute("sc_prob43_category", sc_prob43_category.toLowerCase());
	          session.setAttribute("sc_prob44_location", sc_prob44_location.toUpperCase());
	          //setting values for eVar45 to eVar50 ---> wu_2.1.4 (2010-11-24) 
	          ArrayList listOfCollegeIdsForThisSearch = collegeIdsForThisSearchVO;
	          if (listOfCollegeIdsForThisSearch != null && listOfCollegeIdsForThisSearch.size() > 0) {
	            String tmp = "";
	            int propCount = 45;
	            int numberOfProbs = listOfCollegeIdsForThisSearch.size();
	            if (numberOfProbs > 6) {
	              numberOfProbs = 6;
	            }
	            OmnitureLoggingVO collegeIdsForThisSearch = null;
	            for (int i = 0; i < numberOfProbs; i++) {
	              tmp = "sc_prob" + propCount + "_collegeIds";
	              collegeIdsForThisSearch = (OmnitureLoggingVO)listOfCollegeIdsForThisSearch.get(i);
	              request.setAttribute(tmp, collegeIdsForThisSearch.getCollegeIdsForThisSearch());
	              propCount++;
	            }
	          }
	        }      
	        //Code changed to set the institution ids instead of college ids for dimension 3 in SR page for 31_May_2016, By Thiyagu G.
	         
	        if (institutionIdsForThisSearchVO != null && institutionIdsForThisSearchVO.size() > 0) {
	          
	          OmnitureLoggingVO institutionIdsForThisSearch = null;
	          institutionIdsForThisSearch = (OmnitureLoggingVO)institutionIdsForThisSearchVO.get(0);
	          request.setAttribute("cDimCollegeId", institutionIdsForThisSearch.getInstitutionIdsForThisSearch());                   
	        }       
	        
	        //End of dimension 3 code
	        //setting cpeQualificationNetworkId
	        String cpeQualificationNetworkId = "2";
	        if (tmpQual.equalsIgnoreCase("M") || tmpQual.equalsIgnoreCase("N") || tmpQual.equalsIgnoreCase("A") || tmpQual.equalsIgnoreCase("T")) {
	          cpeQualificationNetworkId = "2";
	        } else { //L or postgraduate-courses
	          cpeQualificationNetworkId = "3";
	        }
	        session.removeAttribute("cpeQualificationNetworkId");
	        session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
	        //fetch bread crumb        
	        request.getSession().setAttribute("bredqual", qualification);
	        request.getSession().setAttribute("bredcatcode", sc_categoryCode);
	        request.getSession().setAttribute("bredlocation", location);
	        String breadCrumb = "";      
	        String clBreadCrumbKey="CLEARING_BROWSE_MONEY_PAGE";
	        String breadCrumbKey="BROWSE_MONEY_PAGE";
	        if(!GenericValidator.isBlankOrNull(p_q) && GenericValidator.isBlankOrNull(p_subject)){
	          clBreadCrumbKey="CLEARING_BROWSE_KEYWORD_PAGE";
	          breadCrumbKey="BROWSE_KEYWORD_PAGE";
	        }
	        if ("CLEARING".equals(searchType)) {
	          breadCrumb = new SeoUtilities().getKeywordBreadCrumb(request, clBreadCrumbKey, qualification, sc_categoryCode, "", utilities.toTitleCase(common.replaceHypenWithSpace(location)), "", p_q);
	        } else {
	          breadCrumb = new SeoUtilities().getKeywordBreadCrumb(request, breadCrumbKey, studyLevelValue, sc_categoryCode, "", utilities.toTitleCase(common.replaceHypenWithSpace(location)), "", p_q);
	        }
	        if (breadCrumb != null && breadCrumb.trim().length() > 0) {
	          
	          request.setAttribute("breadCrumb", breadCrumb);
	        }      
	        boolean mysearchurlflag = true;      
	        if ((pageno != null && pageno.equals("1")) && mysearchurlflag) {
	          String userId = new SessionData().getData(request, "y");
	          userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";        
	        }      
	        if (session.getAttribute("refineCueShown") != null) {
	          if (!"TRUE".equalsIgnoreCase(session.getAttribute("refineCueShown").toString())) {
	            session.setAttribute("refineCueShown", "FALSE");
	          }
	        } else {
	          session.setAttribute("refineCueShown", "FALSE");
	        }
	        
	        //Setting GAM changes done by Sekhar K for wu319      
	        session.removeAttribute("gamKeywordOrSubject");
	        session.removeAttribute("gamStudyLevelDesc");
	        session.removeAttribute("FaqStudyLevel"); //25_MAR_2014
	        session.removeAttribute("FaqCategoryCode"); //25_MAR_2014
	        session.removeAttribute("FaqSubject");  
	        session.removeAttribute("entryRequirementsForGAM");  //Added for wu_536 3-FEB-2015_REL by Karthikeyan.M 
	        if (!"".equals(entryLevel) && entryLevel != null) {
	          if ("A_LEVEL".equalsIgnoreCase(entryLevel)) {
	            String entryPointsforGAM = global.getEntryPointsForGAM(entryPoints);
	            if (entryPointsforGAM != null && entryPointsforGAM.trim().length() > 0) {
	              session.setAttribute("entryPointsforGAM", entryPointsforGAM);
	            }
	          }
	          session.setAttribute("entryRequirementsForGAM", entryLevel);  //Added for wu_536 3-FEB-2015_REL by Karthikeyan.M
	        }
	        session.setAttribute("FaqStudyLevel", studyLevel); //25_MAR_2014
	        session.setAttribute("FaqCategoryCode", searchBean.getSubjectId()); //25_MAR_2014
	        session.setAttribute("FaqSubject", request.getAttribute("subjectName"));
	        request.setAttribute("seoStudyLevelText", common.seoStudyLevelDescription(studyLevel, "university", "COURSE_LIST"));
	        request.setAttribute("paramStudyLevelId", studyLevel);
	        request.setAttribute("paramSubjectCode", searchBean.getSubjectId());
	        request.setAttribute("paramlocationValue", (location != null && !location.equals("null") && location.trim().length() > 0 ? location : ""));
	        String description = studyLevel != null && studyLevel.trim().length() > 0 && !studyLevel.equalsIgnoreCase("M,N,A,T,L") ? common.getStudyLevelDesc(studyLevel, request) : "";
	        if (description != null && description.trim().length() > 0) {
	          request.setAttribute("studyLevelDesc", utilities.toLowerCase(description));
	          request.setAttribute("studyLevelDescInit", description); // added for 15th December 2009 Release
	        }      
	        String courseSearchTextDisp = !GenericValidator.isBlankOrNull(courseSearchText) ? utilities.toTitleCase(common.replaceHypenWithSpace(courseSearchText)).replaceAll(" And ", " and ") : "";      
	        if(!GenericValidator.isBlankOrNull(gamUcasCode)){
	          p_subject = gamUcasCode;
	        }
	        session.setAttribute("gamKeywordOrSubject", GenericValidator.isBlankOrNull(p_subject) ? p_q : p_subject);     
	        request.setAttribute("gamKeywordOrSubject", GenericValidator.isBlankOrNull(p_subject) ? p_q : p_subject);//Added by Indumathi.S To get the keyword in googleAdSlots July_5_2016
	        session.setAttribute("gamStudyLevelDesc", selQual);
	        //request.setAttribute("searchLoc", utilities.toLowerCase(location));
	        request.setAttribute("searchLocName", utilities.toTitleCase(common.replaceHypenWithSpace(location)));
	        String locationTitleCase = (location != null ? new CommonUtil().getSeoFriendlyLocationName(location) : location);
	        locationTitleCase = (locationTitleCase != null ? common.getTitleCaseFn(locationTitleCase) : locationTitleCase);
	        locationTitleCase = locationTitleCase != null ? locationTitleCase.replaceAll(" And ", " and ") : locationTitleCase;
	        String searchText = subjectName != null && subjectName.trim().length() > 0 ? subjectName : "";      
	        request.setAttribute("subjectDesc", GenericValidator.isBlankOrNull(browseCatDesc) ? courseSearchTextDisp : browseCatDesc );
	        request.setAttribute("keywordOrSubject", GenericValidator.isBlankOrNull(browseCatDesc) ? courseSearchTextDisp : browseCatDesc );
	        if(!GenericValidator.isBlankOrNull(ucasCode)){
	          request.setAttribute("keywordOrSubject", "UCAS code " + searchBean.getUcasCode().toUpperCase());
	        }
	        searchText = searchText + (description != null && description.trim().length() > 0 ? " " + utilities.toLowerCase(description) : "");
	        searchText = searchText + " courses, " + locationTitleCase;
	        request.setAttribute("searchText", searchText);      
	        String searchText1="";
	        searchText1 = searchText1 + ((searchCategory != null && !searchCategory.equalsIgnoreCase("null") && searchCategory.trim().length() > 0)? ((subjectName != null && !subjectName.equalsIgnoreCase("null") && subjectName.trim().length() > 0)? subjectName: ""): ((courseSearchText != null && !courseSearchText.equalsIgnoreCase("null") && courseSearchText.trim().length() > 0)? common.getTitleCaseFn(courseSearchText): ((ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0)? "UCAS code " + utilities.toUpperCase(ucasCode):   ((jacsSubjectName != null && !jacsSubjectName.equalsIgnoreCase("null") && jacsSubjectName.trim().length() > 0)?  (jacsSubjectName + " ") :   "" ))));
	        request.setAttribute("SEARCH_KEYWORD_OR_SUBJECT", searchText1.replaceAll("And","and"));
	        searchText1 = (searchText1 != null && searchText1.trim().length() > 0? searchText1 + (description != null && !description.equalsIgnoreCase("null") && description.trim().length() > 0? " " + utilities.toLowerCase(description): "") + " courses": searchText1) + (postCode != null && !postCode.equalsIgnoreCase("null") && postCode.trim().length() > 0? (searchText1 != null && !searchText.equalsIgnoreCase("null") && searchText1.trim().length() > 0? ", ": "") + common.getTitleCaseFn(utilities.getSeoFriendlyLocationName(postCode)).replaceAll(" And ", " and "): (searchBean.getRegionId() != null && searchBean.getRegionId().length() > 0? ", " + common.getTitleCaseFn(utilities.getSeoFriendlyLocationName(searchBean.getRegionId())).replaceAll(" And ", " and "): ""));      
	        request.setAttribute("SEARCH_LOCATION_OR_POSTCODE", utilities.toTitleCase(common.replaceHypenWithSpace(location)));
	        searchText1 = (searchText1 != null? searchText1.trim(): searchText1) + (collegeNameDisplay != null && !collegeNameDisplay.equalsIgnoreCase("null") && collegeNameDisplay.trim().length() > 0? ", " + collegeNameDisplay: "");
	        String countyName = "";
	        if (countyId != null && countyId.trim().length() > 0) {
	          countyName = String.valueOf(common.getCountyName(postCode, countyId)); //DB_CALL
	        }
	        String searchLocation = "";
	        if (!GenericValidator.isBlankOrNull(locationValue)) {
	            request.setAttribute("searchLoc", locationValue.trim().toLowerCase());
	        }
	        
	        if ((postCodeText != null && postCodeText.trim().length() > 0) || (locationValue != null && locationValue.trim().length() > 0)) {
	          if (postCodeText != null && postCodeText.trim().length() > 0) {
	            searchLocation = postCodeText != null? utilities.getSeoFriendlyLocationName(postCodeText): postCodeText;
	            searchLocation = searchLocation != null? utilities.toTitleCase(common.replaceHypenWithSpace(searchLocation)).trim().replaceAll(" And ", " and "): searchLocation;          
	            request.setAttribute("searchLocation", searchLocation);
	          }       
	        }
	        if (subjectName != null && subjectName.trim().length() > 0) {
	          request.setAttribute("hashRemovedCourseName", (subjectName + (courseSearchText != null && courseSearchText.trim().length() > 0? ", " + common.getTitleCaseFn(courseSearchText): "")).replaceAll("#", "-"));
	        } else if (ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0) { // added for UCAS code search
	          request.setAttribute("hashRemovedCourseName", ("UCAS code " + utilities.toUpperCase(ucasCode)).replaceAll("#", "-"));
	          postCode = "";
	        } else {
	          String titleCase = common.getTitleCaseFn(courseSearchText);
	          request.setAttribute("hashRemovedCourseName", ((courseSearchText != null && courseSearchText.trim().length() > 0? (titleCase != null? titleCase.trim(): titleCase): "") + (studyModeDesc != null && studyModeDesc.trim().length() > 0? ", " + studyModeDesc: "")).replaceAll("#", "-"));
	          if(!GenericValidator.isBlankOrNull(jacsSubjectName)){
	            String jacsTitleCase = common.getTitleCaseFn(jacsSubjectName);
	            request.setAttribute("hashRemovedCourseName", ((jacsSubjectName != null && jacsSubjectName.trim().length() > 0? (jacsTitleCase != null? jacsTitleCase.trim(): jacsTitleCase): "") + (studyModeDesc != null && studyModeDesc.trim().length() > 0? ", " + studyModeDesc: "")).replaceAll("#", "-"));
	          }
	        }
	        if (subjectName != null && subjectName.trim().length() > 0) {
	          request.setAttribute("displayText", common.getTitleCaseFn(subjectName) + (studyModeDesc != null && studyModeDesc.trim().length() > 0? " " + studyModeDesc: ""));
	        }
	        if (ucasCode != null && !ucasCode.equalsIgnoreCase("null") && ucasCode.trim().length() > 0) { // added for UCAS code search
	          request.setAttribute("displayText", "UCAS code " + utilities.toUpperCase(ucasCode) + (studyModeDesc != null && studyModeDesc.trim().length() > 0? " " + studyModeDesc: ""));
	        }
	        if (courseSearchText != null && courseSearchText.trim().length() > 0) {
	          request.setAttribute("displayText", common.getTitleCaseFn(courseSearchText) + (description != null && description.trim().length() > 0? " " + description: ""));
	        }      
	        String urlPostcode = postCode;
	        if (urlPostcode != null && urlPostcode.trim().length() == 0) {
	          urlPostcode = searchBean.getRegionId();
	        }      
	        String deemedUni = "uc";
	        request.setAttribute("deemedUni", deemedUni);      
	        locationTitleCase = locationTitleCase != null ? locationTitleCase.trim() : "";
	        String qryString = request.getQueryString();
	        qryString = GenericValidator.isBlankOrNull(qryString) ? "" : qryString;
	        String separator = "/";
	        String newURL = separator + urlArray[1] + separator + urlArray[2];
	        String sortingUrl = newURL + "?" + qryString;
	        request.setAttribute("sortingUrl", sortingUrl);
	        String queryStringValues = request.getQueryString();//Added this for pagination removal in URL by Hema.S on 12_FEB_2019_REL
	        request.setAttribute("queryStringValues",queryStringValues);
	        String faqURL = (orderByUrl != null ? utilities.toLowerCase(orderByUrl.trim()) : orderByUrl) + filterparameters;
	        //End of Setting GAM
	        //THIS CODE IS RELATED TO RETAINING USER GRADES ADDED BY ANBU.R
	        String user_Id = new SessionData().getData(request, "y");
	        getMyGradesValue(user_Id, request);
	        //16-Sep-2014 - Insight
	        request.setAttribute("insightPageFlag", "yes");      
	        if(!GenericValidator.isBlankOrNull(location)){ //Added by Prabha for insight(dimension13) on 29_mar_2016
	          request.setAttribute("location", common.replaceHypenWithSpace(location).toUpperCase());
	        }
	       /* Commented by Prabha on 29_Mar_2016
	        if (!GenericValidator.isBlankOrNull(p_postCode) || (!GenericValidator.isBlankOrNull(location) && location != null && !"UNITED KINGDOM".equals(location))) {
	          request.setAttribute("cDimCountry", "UK");
	        }*/
	        request.setAttribute("dimCategoryCode", GenericValidator.isBlankOrNull(searchBean.getSubjectId()) ? "" : new GlobalFunction().getReplacedString(searchBean.getSubjectId().trim().toUpperCase()));
	        if("Y".equalsIgnoreCase(browseCatFlag)){
	          /* Start Added for wu_536 3-FEB-2015_REL by Karthi for Logging dimension18 */
	          String navKeyword = request.getParameter("keyword");
	          navKeyword = (GenericValidator.isBlankOrNull(navKeyword) || ("Please enter subject or uni").equals(navKeyword) || ("Please enter UCAS code").equals(navKeyword)) ? "" : navKeyword;
	          /* End Added for wu_536 3-FEB-2015_REL by Karthi */        
	          if (!GenericValidator.isBlankOrNull(navKeyword)) {
	            request.setAttribute("cDimKeyword", new GlobalFunction().getReplacedString(navKeyword.trim().toLowerCase()));
	          }
	          request.setAttribute("cDimLDCS", GenericValidator.isBlankOrNull(browseCatDesc) ? "" : new GlobalFunction().getReplacedString(browseCatDesc.trim().toLowerCase()));
	          if (!GenericValidator.isBlankOrNull(searchBean.getStudyLevelId())) {
	            new GlobalFunction().getStudyLevelDesc(searchBean.getStudyLevelId(), request);
	          }  
	          request.setAttribute("setPageType", "browsemoneypageresults");
	        }else{
	          request.setAttribute("MONEY_PAGE_GEO_TARGET", "TRUE");        
	          request.setAttribute("cDimKeyword", GenericValidator.isBlankOrNull(courseSearchTextDisp)? "": new GlobalFunction().getReplacedString(courseSearchTextDisp.trim().toLowerCase()));      
	          //request.setAttribute("cDimJACS", GenericValidator.isBlankOrNull(p_jacsCode)? "": p_jacsCode.trim());      
	          if(!GenericValidator.isBlankOrNull(searchBean.getStudyLevelId())){
	            request.setAttribute("qualification", GenericValidator.isBlankOrNull(searchBean.getStudyLevelId())? "": searchBean.getStudyLevelId().trim());
	            new GlobalFunction().getStudyLevelDesc(searchBean.getStudyLevelId(), request);
	          }
	          request.setAttribute("setPageType", "moneypageresults");
	        }
	      }
	      //Added for June_5_18 release to no index the url pattern /all-courses/ by sangeeth.S
	      if("all".equalsIgnoreCase(selQual)){
	        request.setAttribute("noindex", "true");
	      }
	    //Added for 20200918 release to no index the url pattern /postgraduate/ by Sri Sankari
	      if("postgraduate".equalsIgnoreCase(selQual)){
	        request.setAttribute("noindex", "true");
	      }
	      /*Dynamic meta data details for SR page URLs in the Back office - added by Sujitha V on 18 AUG 2020 Rel*/
  	      ArrayList seoMetaDetailsList = (ArrayList) rsMap.get("pc_meta_details");
  		  if(!CollectionUtils.isEmpty(seoMetaDetailsList)){
  			model.addAttribute("seoMetaDetailsSRPage", seoMetaDetailsList);
  			SEOMetaDetailsVO seoMetaDetailsVO = (SEOMetaDetailsVO) seoMetaDetailsList.get(0);
  			model.addAttribute("META_TITLE", seoMetaDetailsVO.getMetaTitle());
  			model.addAttribute("META_DESC", seoMetaDetailsVO.getMetaDesc());
  			model.addAttribute("META_KEYWORDS", seoMetaDetailsVO.getMetaKeyword());
  			model.addAttribute("META_ROBOTS", seoMetaDetailsVO.getMetaRobots());
  			model.addAttribute("STUDY_DESC", seoMetaDetailsVO.getStudyDesc());
  			model.addAttribute("CATEGORY_DESC", seoMetaDetailsVO.getCategoryDesc());
  		  }
  		  /*End of Dynamic meta data details for SR page URLs in the Back office*/
		  
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    if (!GenericValidator.isBlankOrNull(filterparameters) && filterparameters.indexOf("?clearing") > -1) {
	      request.setAttribute("getInsightName", "clearingmoneypageresults.jsp");
	      return new ModelAndView("clearingbrowseMoneyPage","searchBean", searchBean);
	    }
	    //return new ModelAndView("clearingSearchResultsPage");
	    return new ModelAndView("browseMoneyPage","searchBean", searchBean);
	  }  

	  public String formRelCanonical(String browseCatId, String location, String sortOrder, String studyLevel, String clearingFlag) {
	    String canonicalUrl = "";
	    if (browseCatId == null || browseCatId.trim().length() == 0) {
	      return "";
	    }
	    DataModel dataModel = new DataModel();
	    Vector parameters = new Vector();
	    parameters.add(browseCatId);
	    parameters.add(location);
	    parameters.add(sortOrder);
	    parameters.add(studyLevel);
	    parameters.add(clearingFlag);
	    try {
	      canonicalUrl = dataModel.getString("wu_seo_pkg.get_pgs_canonical_url_fn", parameters);
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    } finally {
	      dataModel = null;
	      parameters.clear();
	      parameters = null;
	    }
	    return canonicalUrl;
	  }

	  public void updateGrades(HttpServletRequest request, String entryLevel, String entryPoints) throws Exception {
	    //THIS CODE IS RELATED TO UPDATE THE USER GRADES
	    String user_Id = new SessionData().getData(request, "y");
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    Object[][] attrValues = new Object[2][6];
	    if (!GenericValidator.isBlankOrNull(user_Id) && !"0".equals(user_Id)) {
	      String attributeValue = null;
	      String userId = new SessionData().getData(request, "y");
	      String optionId = getOptionId(entryLevel);
	      attributeValue = getFormatedXMLData(entryPoints, entryLevel);
	      attrValues[0][0] = null;
	      attrValues[0][1] = null;
	      attrValues[0][2] = "197";
	      attrValues[0][3] = null;
	      attrValues[0][4] = optionId;
	      attrValues[0][5] = null;
	      attrValues[1][0] = null;
	      attrValues[1][1] = null;
	      attrValues[1][2] = "210";
	      attrValues[1][3] = attributeValue;
	      attrValues[1][4] = null;
	      attrValues[1][5] = null;
	      MyWhatuniInputVO inputBean = new MyWhatuniInputVO();
	      inputBean.setUserId(userId);
	      inputBean.setUserAttributeType("UG");
	      commonBusiness.myWhatuniUserAttrValues(inputBean, attrValues);
	    }
	  }

	  /**
	     * This function to return the option id
	     * @param entryLevel
	     * @return String.
	     */
	  private String getOptionId(String entryLevel) {
	    String optionId = null;
	    if (!GenericValidator.isBlankOrNull(entryLevel) && "a_level".equalsIgnoreCase(entryLevel)) {
	      optionId = "74";
	    } else if (!GenericValidator.isBlankOrNull(entryLevel) && "SQA_HIGHER".equalsIgnoreCase(entryLevel)) {
	      optionId = "75";
	    } else if (!GenericValidator.isBlankOrNull(entryLevel) && "SQA_ADV".equalsIgnoreCase(entryLevel)) {
	      optionId = "76";
	    } else if (!GenericValidator.isBlankOrNull(entryLevel) && "BTEC".equalsIgnoreCase(entryLevel)) {
	      optionId = "555";
	    } else if (!GenericValidator.isBlankOrNull(entryLevel) && "ucas_points".equalsIgnoreCase(entryLevel)) {
	      optionId = "77";
	    }
	    return optionId;
	  }

	  /**
	    * This function is used to formating user grades to xml style
	    * @param entryPoints, entryLevel
	    * @return String.
	    */
	  private String getFormatedXMLData(String entryPoints, String entryLevel) {
	    StringBuffer gradesXML = new StringBuffer();
	    if (!GenericValidator.isBlankOrNull(entryPoints)) {
	      String[] gradesArr = entryPoints.split("-");
	      gradesXML.append("<entry-grades>");
	      if (gradesArr != null && "a_level".equalsIgnoreCase(entryLevel)) {
	        gradesXML.append("<A*>" + gradesArr[0] + "</A*>");
	        gradesXML.append("<A>" + gradesArr[1] + "</A>");
	        gradesXML.append("<B>" + gradesArr[2] + "</B>");
	        gradesXML.append("<C>" + gradesArr[3] + "</C>");
	        gradesXML.append("<D>" + gradesArr[4] + "</D>");
	        gradesXML.append("<E>" + gradesArr[5] + "</E>");
	      }
	      if (gradesArr != null && ("SQA_HIGHER".equalsIgnoreCase(entryLevel) || "SQA_ADV".equalsIgnoreCase(entryLevel))) {
	        gradesXML.append("<A>" + gradesArr[0] + "</A>");
	        gradesXML.append("<B>" + gradesArr[1] + "</B>");
	        gradesXML.append("<C>" + gradesArr[2] + "</C>");
	        gradesXML.append("<D>" + gradesArr[3] + "</D>");
	      }
	      if (gradesArr != null && ("BTEC".equalsIgnoreCase(entryLevel))) {
	        gradesXML.append("<D*>" + gradesArr[0] + "</D*>");
	        gradesXML.append("<D>" + gradesArr[1] + "</D>");
	        gradesXML.append("<M>" + gradesArr[2] + "</M>");
	        gradesXML.append("<P>" + gradesArr[3] + "</P>");
	      }
	      if (entryPoints != null && "ucas_points".equalsIgnoreCase(entryLevel)) {
	        gradesXML.append("<free-text>" + entryPoints + "</free-text>");
	      }
	      gradesXML.append("</entry-grades>");
	    }
	    return String.valueOf(gradesXML);
	  }
	  
	  /**
	    * This function is used to set user grades values to request for grades free population
	    * @param entryLevel, entryPoints, entry_Point, request
	    * @return String.
	    */
	  public void getMyGradesValue(String user_Id, HttpServletRequest request) {
	    //
	    if (!GenericValidator.isBlankOrNull(user_Id) && !"0".equals(user_Id)) {
	      boolean checkGrade197 = false;
	      boolean checkGrade210 = false;
	      String entryPoints = null;
	      String entryLevel = null;
	      String optionId = null;
	      SearchBean bean = new SearchBean();
	      bean.setUserId(user_Id);
	      ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
	      Map resultMap = searchBusiness.getPredictedGrades(bean);
	      ArrayList gradesList = (ArrayList)resultMap.get("lc_get_user_attr_val");
	      if (gradesList != null && gradesList.size() > 0) {
	        Iterator gradeItr = gradesList.iterator();
	        while (gradeItr.hasNext()) {
	          SearchBean sBean = (SearchBean)gradeItr.next();
	          if (!GenericValidator.isBlankOrNull(sBean.getAttributeId()) && sBean.getAttributeId().equals("210")) {
	            entryPoints = sBean.getAttributeValue();
	            checkGrade197 = true;
	          } else if (!GenericValidator.isBlankOrNull(sBean.getAttributeId()) && sBean.getAttributeId().equals("197")) {
	            entryLevel = sBean.getOptionDesc();
	            optionId = sBean.getOptionId();
	            checkGrade210 = true;
	          }
	        }
	        setGrades(optionId, entryLevel, entryPoints, getGradesValues(entryLevel, entryPoints), request);
	      }
	      if (checkGrade210 == true && checkGrade197 == true) {
	        request.setAttribute("ISEXISTSGRADE", "YES");
	      } else {
	        request.setAttribute("ISEXISTSGRADE", "NO");
	      }
	    }
	  }
	  
	  /**
	    * This function is used to split & get user grades values from xml string
	    * @param entryLevel, entryPoints
	    * @return String.
	    */
	  private String[] getGradesValues(String entryLevel, String multisetVal) {
	    String[] gradesArr = new String[6];
	    String astar = multisetVal != null && multisetVal.indexOf("A*") > -1? multisetVal.substring(multisetVal.indexOf("A*>") + 3, multisetVal.indexOf("A*>") + 4): "0";
	    String avalue = multisetVal != null && multisetVal.indexOf("<A>") > -1? multisetVal.substring(multisetVal.indexOf("A>") + 2, multisetVal.indexOf("A>") + 3): "0"; //Modified to fix bug id=23659
	    String bvalue = multisetVal != null && multisetVal.indexOf("B") > -1? multisetVal.substring(multisetVal.indexOf("B") + 2, multisetVal.indexOf("B") + 3): "0";
	    String cvalue = multisetVal != null && multisetVal.indexOf("C") > -1? multisetVal.substring(multisetVal.indexOf("C") + 2, multisetVal.indexOf("C") + 3): "0";
	    String dvalue = multisetVal != null && multisetVal.indexOf("D") > -1? multisetVal.substring(multisetVal.indexOf("D") + 2, multisetVal.indexOf("D") + 3): "0";
	    String evalue = multisetVal != null && multisetVal.indexOf("E") > -1? multisetVal.substring(multisetVal.indexOf("E") + 2, multisetVal.indexOf("E") + 3): "0";
	    
	    String dstar = multisetVal != null && multisetVal.indexOf("D*") > -1 ? multisetVal.substring(multisetVal.indexOf("D*>")+3, multisetVal.indexOf("D*>")+4) : "0";
	    String btecdvalue = multisetVal != null && multisetVal.indexOf("<D>") > -1 ? multisetVal.substring(multisetVal.indexOf("D>")+2, multisetVal.indexOf("D>")+3) : "0"; //Modified to fix bug id=23659
	    String mvalue = multisetVal != null && multisetVal.indexOf("M") > -1 ? multisetVal.substring(multisetVal.indexOf("M")+2, multisetVal.indexOf("M")+3) : "0";
	    String pvalue = multisetVal != null && multisetVal.indexOf("P") > -1 ? multisetVal.substring(multisetVal.indexOf("P")+2, multisetVal.indexOf("P")+3) : "0";
	    
	    String freeText = multisetVal != null && multisetVal.indexOf("free-text") > -1? multisetVal.substring(multisetVal.indexOf("<free-text>") + 11, multisetVal.indexOf("</free-text>")): "";
	    if ("A_LEVEL".equalsIgnoreCase(entryLevel)) {
	      gradesArr[0] = astar;
	      gradesArr[1] = avalue;
	      gradesArr[2] = bvalue;
	      gradesArr[3] = cvalue;
	      gradesArr[4] = dvalue;
	      gradesArr[5] = evalue;
	    } else if ("SQA_HIGHER".equalsIgnoreCase(entryLevel)) {
	      gradesArr[0] = avalue;
	      gradesArr[1] = bvalue;
	      gradesArr[2] = cvalue;
	      gradesArr[3] = dvalue;
	    } else if ("SQA_ADV".equalsIgnoreCase(entryLevel)) {
	      gradesArr[0] = avalue;
	      gradesArr[1] = bvalue;
	      gradesArr[2] = cvalue;
	      gradesArr[3] = dvalue;
	    } else if("BTEC".equalsIgnoreCase(entryLevel)){      
	       gradesArr[0] = dstar;
	       gradesArr[1] = btecdvalue;
	       gradesArr[2] = mvalue;
	       gradesArr[3] = pvalue; 
	     } else {
	      gradesArr[0] = freeText;
	    }
	    return gradesArr;
	  }
	  
	  /**
	    * This function is used to set user grades values to request for grades free population
	    * @param entryLevel, entryPoints, entry_Point, request
	    * @return String.
	    */
	  public void setGrades(String optionId, String entryLevel, String entryPoints, String[] entry_Point, HttpServletRequest request) {
	    if ("A_LEVEL".equalsIgnoreCase(entryLevel) && !GenericValidator.isBlankOrNull(entryPoints)) {
	      request.setAttribute("entryLevelValue", "A-levels");
	      request.setAttribute("sBoxA*Value", entry_Point[0]);
	      request.setAttribute("sBoxAValue", entry_Point[1]);
	      request.setAttribute("sBoxBValue", entry_Point[2]);
	      request.setAttribute("sBoxCValue", entry_Point[3]);
	      request.setAttribute("sBoxDValue", entry_Point[4]);
	      request.setAttribute("sBoxEValue", entry_Point[5]);
	    } else if ("SQA_HIGHER".equalsIgnoreCase(entryLevel) && !GenericValidator.isBlankOrNull(entryPoints)) {
	      request.setAttribute("entryLevelValue", "SQA Highers");
	      request.setAttribute("sBoxAValue", entry_Point[0]);
	      request.setAttribute("sBoxBValue", entry_Point[1]);
	      request.setAttribute("sBoxCValue", entry_Point[2]);
	      request.setAttribute("sBoxDValue", entry_Point[3]);
	    } else if ("SQA_ADV".equalsIgnoreCase(entryLevel) && !GenericValidator.isBlankOrNull(entryPoints)) {
	      request.setAttribute("entryLevelValue", "SQA Advanced Highers");
	      request.setAttribute("sBoxAValue", entry_Point[0]);
	      request.setAttribute("sBoxBValue", entry_Point[1]);
	      request.setAttribute("sBoxCValue", entry_Point[2]);
	      request.setAttribute("sBoxDValue", entry_Point[3]);
	    }else if("BTEC".equalsIgnoreCase(entryLevel) && !GenericValidator.isBlankOrNull(entryPoints)){
	          request.setAttribute("entryLevelValue", "BTEC");
	          if (!entryPoints.equalsIgnoreCase("")) {            
	            request.setAttribute("sBoxD*Value", entry_Point[0]);
	            request.setAttribute("sBoxBtecDValue", entry_Point[1]);
	            request.setAttribute("sBoxMValue", entry_Point[2]);
	            request.setAttribute("sBoxPValue", entry_Point[3]);
	          }
	        } else if ("77".equalsIgnoreCase(optionId) && entry_Point != null) {
	      request.setAttribute("entryLevelValue", "Tariff Points");
	      request.setAttribute("tariffPointValue", entry_Point[0]);
	    }
	}
}
