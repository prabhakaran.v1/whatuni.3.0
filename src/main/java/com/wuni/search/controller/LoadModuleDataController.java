package com.wuni.search.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Iterator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.layer.business.ISearchBusiness;
import com.wuni.valueobjects.ModuleVO;


@Controller
public class LoadModuleDataController {

	  @Autowired
	  private ISearchBusiness searchBusiness;
	
	  @RequestMapping(value = "/loadmoduledata", method = RequestMethod.POST)
	  public ModelAndView getLoadModuleData(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    //
	    ModuleVO moduleVO = new ModuleVO();
	    moduleVO.setCourseId(request.getParameter("courseId"));
	    String method = request.getParameter("method");
	    String mobileFlag = request.getParameter("mobileFlag");
	    moduleVO.setModuleGroupId(request.getParameter("moduleGroupId"));
	    if(!GenericValidator.isBlankOrNull(method) && ("loadModuleData").equals(method)){
	      String index = request.getParameter("index");
	      Map resultMap = searchBusiness.getModuleStageDetails(moduleVO);
	      ArrayList moduleDetailList = (ArrayList)resultMap.get("lc_mod_details");
	      if(moduleDetailList!= null && moduleDetailList.size()>0){
	        request.setAttribute("moduleDetailList", moduleDetailList);
	        Iterator moduleDetailListItr =moduleDetailList.iterator();
	        while(moduleDetailListItr.hasNext()){
	          ModuleVO modulevo = (ModuleVO)moduleDetailListItr.next();
	          request.setAttribute("moduleGroupDesc", modulevo.getModuleGroupDesc());
	          break;
	        }
	      }
	      request.setAttribute("courseId",request.getParameter("courseId"));
	      request.setAttribute("moduleindex" , index);
	      String returnString = "loadmoduledata";
	      if("Y".equalsIgnoreCase(mobileFlag))
	      {
	        returnString = "mobileLoadmoduledata";
	      }
	      return new ModelAndView(returnString);
	      
	    }else if(!GenericValidator.isBlankOrNull(method) && ("loadModuleDetail").equals(method)){
	      //
	      String modDetIndex = request.getParameter("index");
	      moduleVO.setModuleId(request.getParameter("moduleId"));
	      Map detailDescMap = searchBusiness.getModuleStageDetailDesc(moduleVO);
	      ArrayList detailDescList = (ArrayList)detailDescMap.get("lc_mod_details");
	      if(detailDescList!=null && detailDescList.size()>0){
	         request.setAttribute("detailDescList", detailDescList);
	      }
	      request.setAttribute("moduleindex" , modDetIndex);
	      String returnString = "loadModuleDetail";
	      if("Y".equalsIgnoreCase(mobileFlag))
	      {
	        returnString = "mobileLoadModuleDetail";
	      }
	      return new ModelAndView(returnString);
	    }
	    return null;
	  }
}
