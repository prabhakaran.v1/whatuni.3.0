package com.wuni.search.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.search.CourseDetailsVO;

public class GetCourseSchemaSP extends StoredProcedure {
  public GetCourseSchemaSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_COURSE_SCHEMA_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new CourseScemaRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_course_id", OracleTypes.VARCHAR));
    declareParameter(new SqlInOutParameter("p_college_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_clearing_flag", OracleTypes.VARCHAR)); //Added clearing flag for course title changes on 16_May_2017, By Thiyagu G.
    compile();
  }
  public Map execute(CourseDetailsVO courseVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_course_id", courseVO.getCourseId());
      inMap.put("p_college_id", courseVO.getCollegeId());
      inMap.put("p_clearing_flag", courseVO.getClearingFlag());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  public class CourseScemaRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CourseDetailsVO courseVO = new CourseDetailsVO(); 
      courseVO.setCourseTitle(rs.getString("course_title"));
      courseVO.setCourseSummary(rs.getString("course_summary"));
      courseVO.setUcasCode(rs.getString("ucas_code"));
      courseVO.setCollegeNameDisplay(rs.getString("college_name"));
      courseVO.setCdPageUrl(rs.getString("course_url"));
      return courseVO;
    }
  }
  }

