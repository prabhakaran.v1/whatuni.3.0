package com.wuni.interstitialsearch;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.interstitialsearch.GardeFilterVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.util.valueobject.interstitialsearch.QualificationVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;

import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;

/**
 * @InterstitialSearchController.java
 * @Version : initial draft
 * @since : 28-Aug-2018
 * @author : Sabapathi.S
 * @Purpose  : Action to do load the interstitial page
  * Modification history:
  * **************************************************************************************************************
  * Author         Release Date              Modification Details
  * **************************************************************************************************************
  * Hemalatha.K    21_Nov_2019              Prepopulation in advance search page
 */

@Controller
public class InterstitialSearchController {

	@RequestMapping(value="/course/advance-search", method=RequestMethod.GET)	
	 public ModelAndView interstitialSearchController(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		// ---(A)--- DECLARE local objects & varialbes
	    InterstitialSearchVO interstitialSearchVO = new InterstitialSearchVO();
	    ISearchBusiness searchBusiness = null;
	    Map outMap = null;
	    CommonUtil commonUtil = new CommonUtil();
	    ArrayList qualificationList = null;
	    ArrayList regionList = null;
	    ArrayList subRegionList = null;
	    ArrayList locationTypeList = null;
	    ArrayList studymodeList = null;
	    ArrayList gardeFilterList = null;
	    ArrayList clearinGardeFilterList = null;
	    ArrayList assessmentTypeList = null;
	    ArrayList reviewcategoryList = null;
	    String clearingOnOffFlag = null;
	    String defaultQualSelected = null;
	    String sub_name = "";
	    //
	    // ---(B) Business logic follow
	    //
	    try {
	      //
	      // --- (B.1) Setting inputs for DB call ---
	      //
	      String queryString = request.getQueryString();
	      defaultQualSelected = !GenericValidator.isBlankOrNull(request.getParameter("qual")) ? request.getParameter("qual") : "degree";
	      sub_name = !GenericValidator.isBlankOrNull(request.getParameter("sub_name")) ? request.getParameter("sub_name") : "";
	      if (!GenericValidator.isBlankOrNull(queryString) && queryString.indexOf("clearing") > -1) {
	        defaultQualSelected = "clearing";
	      }
	      interstitialSearchVO.setPageName("INTERSTITIAL_PAGE");
	      interstitialSearchVO.setDefaultQualSelected(defaultQualSelected);
	      
	      //Added additional input parameters for the below db call - Jeya 19th Nov rel
	      String subjectSessionId = !GenericValidator.isBlankOrNull((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
	      String userId = !GenericValidator.isBlankOrNull(new SessionData().getData(request, "y")) ? new SessionData().getData(request, "y") : "0";
	      interstitialSearchVO.setUserId(userId);
	      interstitialSearchVO.setSubjectSessionId(subjectSessionId);
	      //
	      // --- (B.1) Calling DB --
	      //
	      searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
	      outMap = searchBusiness.getInterstitialSearchFilters(interstitialSearchVO);
	      //
	      // --- (B.2) Get Cursors details from out map ---
	      //
	      defaultQualSelected = (String)outMap.get("P_DEFAULT_QUAL_SELECTED");
	      clearingOnOffFlag = (String)outMap.get("P_CLEARING_ON_OFF_FLAG");
	      qualificationList = (ArrayList)outMap.get("PC_QUALIFICATION_LIST");
	      regionList = (ArrayList)outMap.get("PC_REGION_LIST");
	      subRegionList = (ArrayList)outMap.get("PC_REGION_UNDER_ENGLAND");
	      locationTypeList = (ArrayList)outMap.get("PC_LOCATION_TYPE_LIST");
	      studymodeList = (ArrayList)outMap.get("PC_STUDY_MODE_LIST");
	      gardeFilterList = commonUtil.gradeListAnalysis((ArrayList)outMap.get("PC_GRADE_FILTER"));
	      //clearinGardeFilterList = commonUtil.gradeListAnalysis((ArrayList)outMap.get("PC_CLEARING_GRADE_FILTER"));
	      assessmentTypeList = (ArrayList)outMap.get("PC_ASSESSMENT_TYPE_LIST");
	      reviewcategoryList = (ArrayList)outMap.get("PC_REVIEW_CAT_LIST");
	      subjectSessionId = (String)outMap.get("P_USER_SUBJGRADES_SESSION_ID");
	      session.setAttribute("subjectSessionId" , subjectSessionId);
	      ArrayList<UserQualDetailsVO> userQualList = (ArrayList<UserQualDetailsVO>)outMap.get("PC_USER_QUALIFICATIONS");
	      //
	      // --- (B.2.1)
	      //
	      if (!CommonUtil.isBlankOrNull(gardeFilterList)) {
	        GardeFilterVO gardeFilterVO = null;
	        for (int i = 0; i < gardeFilterList.size(); i++) {
	          gardeFilterVO = (GardeFilterVO)gardeFilterList.get(i);
	          if (!GenericValidator.isBlankOrNull(gardeFilterVO.getGradeStr())) {
	            gardeFilterVO.setGradesList(Arrays.asList(gardeFilterVO.getGradeStr().split(",")));
	          }
	        }
	      }
	      //
	      if (!CommonUtil.isBlankOrNull(qualificationList)) {
	        QualificationVO qualificationVO = null;
	        for (int i = 0; i < qualificationList.size(); i++) {
	          qualificationVO = (QualificationVO)qualificationList.get(i);
	          if (!GenericValidator.isBlankOrNull(qualificationVO.getUrlText())) {
	            if ("clearing".equals(qualificationVO.getUrlText()) && "OFF".equals(clearingOnOffFlag)) {
	              qualificationList.remove(i);
	            }
	          }
	        }
	      }
	      //
	      // --- (B.3) Set in request scope for using view (JSP) ---
	      //
	      request.setAttribute("DEFAULT_QUAL_SELECTED", defaultQualSelected);
	      request.setAttribute("CLEARING_ON_OFF_FLAG", clearingOnOffFlag);
	      request.setAttribute("QUALIFICATION_LIST", qualificationList);
	      request.setAttribute("REGION_LIST", regionList);
	      request.setAttribute("SUB_REGION_LIST", subRegionList);
	      request.setAttribute("LOCATION_TYPE_LIST", locationTypeList);
	      request.setAttribute("STUDY_MODE_LIST", studymodeList);
	      request.setAttribute("GRADE_FILTER", gardeFilterList);
	      request.setAttribute("qualificationList", gardeFilterList);
	      /*if ("ON".equals(clearingOnOffFlag)) {
	        request.setAttribute("CLEARING_GRADE_FILTER", clearinGardeFilterList);
	      }*/
	      
      // Added by Hemalatha.K for NOV_21_19 REL 
	      request.setAttribute("pageName", "advanceSearchPage");
	      request.setAttribute("ASSESSMENT_TYPE_LIST", assessmentTypeList);
	      request.setAttribute("REVIEW_CAT_LIST", reviewcategoryList);
	      request.setAttribute("sub_id", request.getParameter("sub_id"));
      request.setAttribute("sub_url_text", request.getParameter("sub_url_text"));
      request.setAttribute("loc_name", request.getParameter("loc_name"));
      if(!GenericValidator.isBlankOrNull(sub_name)) {
        sub_name = URLDecoder.decode(queryString.substring(queryString.indexOf("sub_name"),queryString.indexOf("&sub_url_text")), "UTF-8");
        request.setAttribute("sub_name", sub_name.split("=")[1]);
      } 
	      //
	      if (userQualList != null && userQualList.size() > 0) {
	        request.setAttribute("userQualList", userQualList);
	        request.setAttribute("userQualSize", String.valueOf(userQualList.size()));
	      }
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    } finally {
	      //
	      // -- (D) Nullify object -- 
	      //
	      searchBusiness = null;
	      if (outMap != null) {
	        outMap.clear();
	        outMap = null;
	      }
	    }
	    //
	    return new  ModelAndView("interstitial-search-page");
	  }

}