package com.wuni.interstitialsearch;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.config.SpringContextInjector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;

import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;

import java.io.BufferedReader;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
  * @InterstitialCourseCountAction.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : Action to do get the count for selected filter values
  * Modification history:
  * **************************************************************************************************************
  * Author       Relase Date              Modification Details
  * **************************************************************************************************************
  * Sabapathi    25_Sep_2018              Received keyword parameter and sent to DB for calculating course count
  */

@Controller
public class InterstitialCourseCountController{

  @RequestMapping(value="/advance-search/course-count", method=RequestMethod.POST)	
  public String advanceSearchCourseCount(HttpServletRequest request, HttpServletResponse response) throws Exception {
    // ---(A)--- DECLARE local objects & varialbes
    InterstitialCourseCountVO interstitialCourseCountVO = new InterstitialCourseCountVO();
    ISearchBusiness searchBusiness = null;
    Map outMap = null;
    int courseCount = 0;
    String outStr = "";
    CommonUtil commonUtil = new CommonUtil();
    //
    // ---(B) Business logic follow
    //
    try {
      //
      // --- (B.1) Setting inputs for DB call ---
      //
      
      // When user enters keyword, we need to calculate course count. The below is decode logic, 25_Sep_2018 By Sabapathi
      String keyword = GenericValidator.isBlankOrNull(request.getParameter("keyword")) ? "" : request.getParameter("keyword");
      keyword = URLDecoder.decode(keyword, "UTF-8");
      
      Gson gson = new GsonBuilder().create();
      String responseAsString = commonUtil.getBody(request);
      interstitialCourseCountVO = gson.fromJson(responseAsString, InterstitialCourseCountVO.class);
      Object[][] qualDetailsArr = null;
      ArrayList<GradeFilterValidationVO> qualSubList = interstitialCourseCountVO.getQualSubList();
      if (qualSubList != null && qualSubList.size() > 0) {
        qualDetailsArr = new String[qualSubList.size()][5];
        for (int lp = 0; lp < qualSubList.size(); lp++) {
          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
        }
       interstitialCourseCountVO.setQualDetailsArr(qualDetailsArr);
      }
      interstitialCourseCountVO.setPageName(request.getParameter("pagename"));
      interstitialCourseCountVO.setQualification(request.getParameter("qual"));
      interstitialCourseCountVO.setSearchKeyword(keyword);
      interstitialCourseCountVO.setSearchCategory(request.getParameter("subject"));
      interstitialCourseCountVO.setLocationTypeString(request.getParameter("locationtype"));
      interstitialCourseCountVO.setTownCity(request.getParameter("country"));
      interstitialCourseCountVO.setStudymode(request.getParameter("studymode"));
      // Added by Hemalatha.K for NOV_21_19 REL 
      if(qualDetailsArr != null) {
        interstitialCourseCountVO.setEntryLevel("ucas_points");
      } else {
        interstitialCourseCountVO.setEntryLevel(request.getParameter("grade"));
      }
      interstitialCourseCountVO.setEntryPoints(request.getParameter("points"));
      interstitialCourseCountVO.setAssessmentType(request.getParameter("assessment"));
      interstitialCourseCountVO.setReviewCategory(request.getParameter("review"));
      //
       searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
      outMap = searchBusiness.getInterstitialCourseCount(interstitialCourseCountVO);
      //
      if (outMap.get("P_COURSE_COUNT") != null) {
         outStr = String.valueOf(Integer.parseInt((String)outMap.get("P_COURSE_COUNT")));
       }
       if (outMap.get("P_UCAS_EQUIV_TARIFF_POINTS") != null) {
         outStr = outStr + GlobalConstants.PROSPECTUS_SPLIT_CHAR + String.valueOf(Integer.parseInt((String)outMap.get("P_UCAS_EQUIV_TARIFF_POINTS")));
       }
       response.getWriter().print(outStr);
      //
    } catch (Exception ex) {
      //ex.printStackTrace();
       response.getWriter().print(courseCount);
    } finally {
      //
      // -- (D) Nullify object -- 
      //
      searchBusiness = null;
      if (outMap != null) {
        outMap.clear();
        outMap = null;
      }
    }
    //
    return null;
  }
}
