package com.wuni.interstitialsearch;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonUtil;

import com.wuni.util.autocomplete.AutoCompleteUtilities;
import com.wuni.util.valueobject.interstitialsearch.SubjectListVO;

@Controller
public class InterstitialSubjectSubCategoryController {
	
	@RequestMapping(value="/advance-search/subject-level-two-list", method = RequestMethod.POST)
	  public ModelAndView SubjectSubCategoryController(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		    // ---(A)--- DECLARE local objects & varialbes
		    String qualCode = GenericValidator.isBlankOrNull(request.getParameter("qual")) ? "" : request.getParameter("qual");
		    String l1Flag = GenericValidator.isBlankOrNull(request.getParameter("l1flag")) ? "" : request.getParameter("l1flag");
		    String pageName = GenericValidator.isBlankOrNull(request.getParameter("pagename")) ? "" : request.getParameter("pagename");
		    String parentCategoryId = GenericValidator.isBlankOrNull(request.getParameter("parentcatid")) ? "" : request.getParameter("parentcatid");
		    ArrayList l2SubjectList = null;
		    AutoCompleteUtilities autoCompleteUtilities = new AutoCompleteUtilities();
		    StringBuffer buffer = new StringBuffer();
		    //
		    // ---(B) Business logic follow
		    //
		    try {
		      //
		      // --- (B.1) Setting inputs for DB call ---
		      //
		      l2SubjectList = autoCompleteUtilities.populateInterstitialSubjectListAutoComplete("", qualCode, l1Flag, parentCategoryId, pageName, response);
		      //
		      buffer.append("<option value=\"-1\" selected=\"selected\">Please select</option>");
		      if (!CommonUtil.isBlankOrNull(l2SubjectList)) {
		        for (int i = 0; i < l2SubjectList.size(); i++) {
		          SubjectListVO subjectListVO = (SubjectListVO)l2SubjectList.get(i);
		          buffer.append("<option value=\""+subjectListVO.getUrlText()+"\">"+subjectListVO.getBrowseDescription()+"</option>");
		        }
		      }
		      //
		      response.getWriter().print(buffer.toString());
		      //
		    } catch (Exception ex) {
		      ex.printStackTrace();
		    } finally {
		      qualCode = null;
		      l1Flag = null;
		      pageName = null;
		      parentCategoryId = null;
		      autoCompleteUtilities = null;
		    }
		    //
		    return null;
		  }
}
