package com.config;

import org.springframework.context.ApplicationContext;

public class SpringContextInjector {

   public static Object getBeanFromSpringContext(String beanId) {
	ApplicationContext context = ApplicationListenerBean.getContext();
	Object beanObj = context.getBean(beanId);
	return beanObj;
   }
}
