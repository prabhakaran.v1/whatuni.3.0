package spring.form;

import org.apache.struts.action.ActionForm;

public class OpenDaysBean {
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String month = "";
  private String openDate = "";
  private String openDateIsoFormat = "";
  private String notes = "";
  private String collegeLocation = "";
  private String webSite = "";
  private String openDayStatus = ""; //LIVE , EXPIRED
  private String userId = null;
  private String openYear = null;
  private String dateExistFlg = null;
  private String openDay = null;
  private String openMonthText = null;
  private String eventId = null;
  private String userTrackSessionId = null;
  private String actionURL = null;
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setMonth(String month) {
    this.month = month;
  }
  public String getMonth() {
    return month;
  }
  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }
  public String getOpenDate() {
    return openDate;
  }
  public void setNotes(String notes) {
    this.notes = notes;
  }
  public String getNotes() {
    return notes;
  }
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }
  public String getWebSite() {
    return webSite;
  }
  public void setOpenDayStatus(String openDayStatus) {
    this.openDayStatus = openDayStatus;
  }
  public String getOpenDayStatus() {
    return openDayStatus;
  }

  public void setOpenDateIsoFormat(String openDateIsoFormat) {
    this.openDateIsoFormat = openDateIsoFormat;
  }

  public String getOpenDateIsoFormat() {
    return openDateIsoFormat;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  
  public void setUserId(String userId) {
    this.userId = userId;
  }
  
  public String getUserId() {
    return userId;
  }
  public void setOpenYear(String openYear)
  {
    this.openYear = openYear;
  }
  public String getOpenYear()
  {
    return openYear;
  }
  public void setDateExistFlg(String dateExistFlg)
  {
    this.dateExistFlg = dateExistFlg;
  }
  public String getDateExistFlg()
  {
    return dateExistFlg;
  }

  public void setOpenDay(String openDay) {
    this.openDay = openDay;
  }

  public String getOpenDay() {
    return openDay;
  }
  
  public void setOpenMonthText(String openMonthText) {
    this.openMonthText = openMonthText;
  }
  
  public String getOpenMonthText() {
    return openMonthText;
  }

  public void setEventId(String eventId)
  {
    this.eventId = eventId;
  }

  public String getEventId()
  {
    return eventId;
  }

  public void setUserTrackSessionId(String userTrackSessionId) {
    this.userTrackSessionId = userTrackSessionId;
  }

  public String getUserTrackSessionId() {
    return userTrackSessionId;
  }

  public void setActionURL(String actionURL) {
    this.actionURL = actionURL;
  }

  public String getActionURL() {
    return actionURL;
  }

}
