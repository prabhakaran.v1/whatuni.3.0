package spring.form.openday;

import lombok.Data;

@Data
public class OpenDayProviderLandingPageBean {
	
  private String collegeId = "";
  private String appFlage = "";
  private String displayCount = "";
  private String userId = "";
  private String trackSessionId = "";
  private String eventId = "";
  private String bookingFormFlag = "";
  private String randomNumer;
  private String networkId;
  private String pageUrl = null;
}
