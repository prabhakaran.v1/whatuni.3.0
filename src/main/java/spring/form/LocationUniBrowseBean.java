package spring.form;

import org.apache.struts.action.ActionForm;

public class LocationUniBrowseBean {
  public LocationUniBrowseBean() {
  }
  private String locationId = "";
  private String locationName = "";
  private String collegeNameDisplay = "";
  private String rowLevel = "";
  private String reviewCount = "";
  private String reviewRating = "";
  private String countyNameParam = "";
  private String collegeProfileFlag = "";
  private String subjectProfileFlag = "";
  private String subjectProfileCount = "";
  private String allCourseCount = "";
  private String pgCourseCount = "";
  private String ugCourseCount = "";
  private String parentRegionName = "";
  private String selectedCountyname = "";
  private String scholarshipCount = "";
  private String collegeName = "";
  private String collegeId = "";
  private String subjectCategoryCode = "";
  private String address = "";
  private String town = "";
  private String prospectusPaidFlag = "";
  private String totalCount = "";
  private String prospectusExternalUrl = "";
  private String collegeLogo = "";
  private String videoThumbnail = "";
  private String videoReviewId = "";
  private String videoUrl = "";
  private String nextOpenDay = "";
  private String collegeLocation = "";
  private String seoCollegeName = "";
  private String websitePurchaseFlag = "";
  private String webSite="";
  
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String subOrderProspectusTargetURL = "";
  private String profileType = "";
  private String myhcProfileId = "";
  private String cpeQualificationNetworkId = "";
  
  public void setCountyNameParam(String countyNameParam) {
    this.countyNameParam = countyNameParam;
  }
  public String getCountyNameParam() {
    return countyNameParam;
  }
  public void setCollegeProfileFlag(String collegeProfileFlag) {
    this.collegeProfileFlag = collegeProfileFlag;
  }
  public String getCollegeProfileFlag() {
    return collegeProfileFlag;
  }
  public void setSubjectProfileFlag(String subjectProfileFlag) {
    this.subjectProfileFlag = subjectProfileFlag;
  }
  public String getSubjectProfileFlag() {
    return subjectProfileFlag;
  }
  public void setAllCourseCount(String allCourseCount) {
    this.allCourseCount = allCourseCount;
  }
  public String getAllCourseCount() {
    return allCourseCount;
  }
  public void setPgCourseCount(String pgCourseCount) {
    this.pgCourseCount = pgCourseCount;
  }
  public String getPgCourseCount() {
    return pgCourseCount;
  }
  public void setParentRegionName(String parentRegionName) {
    this.parentRegionName = parentRegionName;
  }
  public String getParentRegionName() {
    return parentRegionName;
  }
  public void setSelectedCountyname(String selectedCountyname) {
    this.selectedCountyname = selectedCountyname;
  }
  public String getSelectedCountyname() {
    return selectedCountyname;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setUgCourseCount(String ugCourseCount) {
    this.ugCourseCount = ugCourseCount;
  }
  public String getUgCourseCount() {
    return ugCourseCount;
  }
  public void setSubjectCategoryCode(String subjectCategoryCode) {
    this.subjectCategoryCode = subjectCategoryCode;
  }
  public String getSubjectCategoryCode() {
    return subjectCategoryCode;
  }
  public void setLocationId(String locationId) {
    this.locationId = locationId;
  }
  public String getLocationId() {
    return locationId;
  }
  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }
  public String getLocationName() {
    return locationName;
  }
  public void setRowLevel(String rowLevel) {
    this.rowLevel = rowLevel;
  }
  public String getRowLevel() {
    return rowLevel;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setReviewRating(String reviewRating) {
    this.reviewRating = reviewRating;
  }
  public String getReviewRating() {
    return reviewRating;
  }
  public void setAddress(String address) {
    this.address = address;
  }
  public String getAddress() {
    return address;
  }
  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }
  public String getScholarshipCount() {
    return scholarshipCount;
  }
  public void setSubjectProfileCount(String subjectProfileCount) {
    this.subjectProfileCount = subjectProfileCount;
  }
  public String getSubjectProfileCount() {
    return subjectProfileCount;
  }
  public void setTown(String town) {
    this.town = town;
  }
  public String getTown() {
    return town;
  }
  public void setProspectusPaidFlag(String prospectusPaidFlag) {
    this.prospectusPaidFlag = prospectusPaidFlag;
  }
  public String getProspectusPaidFlag() {
    return prospectusPaidFlag;
  }
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }
  public String getTotalCount() {
    return totalCount;
  }
  public void setProspectusExternalUrl(String prospectusExternalUrl) {
    this.prospectusExternalUrl = prospectusExternalUrl;
  }
  public String getProspectusExternalUrl() {
    return prospectusExternalUrl;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setVideoThumbnail(String videoThumbnail) {
    this.videoThumbnail = videoThumbnail;
  }
  public String getVideoThumbnail() {
    return videoThumbnail;
  }
  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }
  public String getVideoReviewId() {
    return videoReviewId;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
  public void setNextOpenDay(String nextOpenDay) {
    this.nextOpenDay = nextOpenDay;
  }
  public String getNextOpenDay() {
    return nextOpenDay;
  }
  public void setWebsitePurchaseFlag(String websitePurchaseFlag) {
    this.websitePurchaseFlag = websitePurchaseFlag;
  }
  public String getWebsitePurchaseFlag() {
    return websitePurchaseFlag;
  }
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setSeoCollegeName(String seoCollegeName) {
    this.seoCollegeName = seoCollegeName;
  }
  public String getSeoCollegeName() {
    return seoCollegeName;
  }
  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }
  public String getWebSite() {
    return webSite;
  }

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setSubOrderWebsite(String subOrderWebsite) {
        this.subOrderWebsite = subOrderWebsite;
    }

    public String getSubOrderWebsite() {
        return subOrderWebsite;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }

    public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
        this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
    }

    public String getSubOrderProspectusTargetURL() {
        return subOrderProspectusTargetURL;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setMyhcProfileId(String myhcProfileId) {
        this.myhcProfileId = myhcProfileId;
    }

    public String getMyhcProfileId() {
        return myhcProfileId;
    }

    public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
        this.cpeQualificationNetworkId = cpeQualificationNetworkId;
    }

    public String getCpeQualificationNetworkId() {
        return cpeQualificationNetworkId;
    }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
