package spring.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class CourseJourneyBean {
  private String subjectId = "";
  private String subjectName = "";
  private String courseId = "";
  private String courseName = "";
  private String regionId = "";
  private String regionName = "";
  private String countyId = "";
  private String countyName = "";
  private String webName = "";
  private String rowStatus = "";
  private String studyLevelDesc = "";
  private String studyLevelText = "";
  private String categoryCode = "";
  private String studyLevelId = "";
  private String countyCourseCount = "";
  private String regionCourseCount = "";
  private List subCategoryList = null;
  private String subCategoryCount = "";
  private List countyList = null;
  private String countyCount = "";
  private String categoryCodeLength = "";
  private String categoryType = "";
  private String hyperLinkUrl = "";
  private String seoSubjectName = "";
  private String parentCategoryId = "";
  private String categoryId = "";
  private String seoStudyLevelText = "";
  
  private String jobId = null;
  private String jobName = null;
  private String industryId = null;
  private String industryName = null;
  private String schoolId = null;
  private String schoolName = null;
  private String schoolNameWCSID = null;
  private String schoolNameFreeText = null;
  private String schoolNameFreeTextWCSID = null;
  private String intendedYear = null;
  private String intendedYearWCSID = null;
  private String schoolCheckFlag = null;
  private ArrayList searchResultsList = new ArrayList();
  private String iwantSearchType = null;
  private String iwantSearchCode = null;
  private String iwantSearchName = null;
  private String logId = null;
  private String allGradEmpPercenrage = null;
  private String salaryRange = null;
  private String pageNo = null;
  private String totalCount = null;
  private String sortByWhich = null;
  private String sortByHow = null; 
  private String intendedYearText = null; 
  private String intendedYearTextWCSID = null;
  
  private String qualification = null;
  private String qualValue = null;
  private String qualDesc = null;
  private List qualList = new ArrayList();
  private String qualSub1 = null;
  private String qualSub2 = null;
  private String qualSub3 = null;
  private String qualSub4 = null;
  private String qualSub5 = null;
  private String qualSub6 = null;
  private String qualSubjName1 = null;
  private String qualSubjName2 = null;
  private String qualSubjName3 = null;
  private String qualSubjName4 = null;
  private String qualSubjName5 = null;
  private String qualSubjName6 = null;
  private String resultsHeading = null;
  private String returnCodeTwoHeading = null;
  private String seoSubjectsText = null;

  private String qualGrades1 = null;
  private String qualGrades2 = null;
  private String qualGrades3 = null;
  private String qualGrades4 = null;
  private String qualGrades5 = null;
  private String qualGrades6 = null;
  private String qualUCASPoints1 = null;
  private String qualUCASPoints2 = null;
  private String qualUCASPoints3 = null;

  private List qualGradeList = new ArrayList();  
  private String jacsCode = null;
  private String searchQualification = null;
  private String searchGrades = null;
  private String equivalentTariffPoints = null;
  private String yearOfEntry = null;
  private String uniId = null;
  private String uniUkprn = null;    
  private List jacsSubjectList = new ArrayList();
  
  
  //Employment Info
  private String JacsSubGradEmpRate = null;
  private String allGradEmpRate = null;
  private String subjectLowerSalary = null;
  private String subjectHigherSalary = null;
  private String allLowerSalary = null;
  private String allHigherSalary = null;
  private String empJacsSubjectName = null;
  
  private String industryCode = null;
  private String industryDescription = null;
  private String industryWisePerncentage = null;
  
  private String jobCode = null;
  private String jobDescription = null;
  private String jobWisePercentage = null;
  
  private String collegeId = null;
  private String collegeNameDisplay = null;
  private String subjectWiseEmpRate = null;
  private String lowerPerncentage = null;
  private String higherPercentage = null;
  private List industryInfoList = new ArrayList();
  private List jobInfoList = new ArrayList();
  private List universitiesEmpRateList = new ArrayList();
  private String userId = null;
  
  public CourseJourneyBean() {
  }
  private List studyLevelList = null;
  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }
  public List getStudyLevelList() {
    return studyLevelList;
  }
  public void setRegionId(String regionId) {
    this.regionId = regionId;
  }
  public String getRegionId() {
    return regionId;
  }
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  public String getRegionName() {
    return regionName;
  }
  public void setCountyId(String countyId) {
    this.countyId = countyId;
  }
  public String getCountyId() {
    return countyId;
  }
  public void setCountyName(String countyName) {
    this.countyName = countyName;
  }
  public String getCountyName() {
    return countyName;
  }
  public void setWebName(String webName) {
    this.webName = webName;
  }
  public String getWebName() {
    return webName;
  }
  public void setRowStatus(String rowStatus) {
    this.rowStatus = rowStatus;
  }
  public String getRowStatus() {
    return rowStatus;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }
  public String getSubjectName() {
    return subjectName;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }
  public String getCourseName() {
    return courseName;
  }
  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }
  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }
  public void setStudyLevelText(String studyLevelText) {
    this.studyLevelText = studyLevelText;
  }
  public String getStudyLevelText() {
    return studyLevelText;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }
  public String getStudyLevelId() {
    return studyLevelId;
  }
  public void setCountyCourseCount(String countyCourseCount) {
    this.countyCourseCount = countyCourseCount;
  }
  public String getCountyCourseCount() {
    return countyCourseCount;
  }
  public void setRegionCourseCount(String regionCourseCount) {
    this.regionCourseCount = regionCourseCount;
  }
  public String getRegionCourseCount() {
    return regionCourseCount;
  }
  public void setSubCategoryList(List subCategoryList) {
    this.subCategoryList = subCategoryList;
  }
  public List getSubCategoryList() {
    return subCategoryList;
  }
  public void setSubCategoryCount(String subCategoryCount) {
    this.subCategoryCount = subCategoryCount;
  }
  public String getSubCategoryCount() {
    return subCategoryCount;
  }
  public void setCountyList(List countyList) {
    this.countyList = countyList;
  }
  public List getCountyList() {
    return countyList;
  }
  public void setCountyCount(String countyCount) {
    this.countyCount = countyCount;
  }
  public String getCountyCount() {
    return countyCount;
  }
  public void setCategoryCodeLength(String categoryCodeLength) {
    this.categoryCodeLength = categoryCodeLength;
  }
  public String getCategoryCodeLength() {
    return categoryCodeLength;
  }
  public void setCategoryType(String categoryType) {
    this.categoryType = categoryType;
  }
  public String getCategoryType() {
    return categoryType;
  }
  public void setHyperLinkUrl(String hyperLinkUrl) {
    this.hyperLinkUrl = hyperLinkUrl;
  }
  public String getHyperLinkUrl() {
    return hyperLinkUrl;
  }
  public void setSeoSubjectName(String seoSubjectName) {
    this.seoSubjectName = seoSubjectName;
  }
  public String getSeoSubjectName() {
    return seoSubjectName;
  }
  public void setParentCategoryId(String parentCategoryId) {
    this.parentCategoryId = parentCategoryId;
  }
  public String getParentCategoryId() {
    return parentCategoryId;
  }
  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }
  public String getCategoryId() {
    return categoryId;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setJobId(String jobId) {
    this.jobId = jobId;
  }
  public String getJobId() {
    return jobId;
  }
  public void setJobName(String jobName) {
    this.jobName = jobName;
  }
  public String getJobName() {
    return jobName;
  }
  public void setIndustryId(String industryId) {
    this.industryId = industryId;
  }
  public String getIndustryId() {
    return industryId;
  }
  public void setIndustryName(String industryName) {
    this.industryName = industryName;
  }
  public String getIndustryName() {
    return industryName;
  }
  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }
  public String getSchoolId() {
    return schoolId;
  }
  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }
  public String getSchoolName() {
    return schoolName;
  }
  public void setSchoolNameFreeText(String schoolNameFreeText) {
    this.schoolNameFreeText = schoolNameFreeText;
  }
  public String getSchoolNameFreeText() {
    return schoolNameFreeText;
  }
  public void setIntendedYear(String intendedYear) {
    this.intendedYear = intendedYear;
  }
  public String getIntendedYear() {
    return intendedYear;
  }
  public void setSchoolCheckFlag(String schoolCheckFlag) {
    this.schoolCheckFlag = schoolCheckFlag;
  }
  public String getSchoolCheckFlag() {
    return schoolCheckFlag;
  }
  public void setSearchResultsList(ArrayList searchResultsList) {
    this.searchResultsList = searchResultsList;
  }
  public ArrayList getSearchResultsList() {
    return searchResultsList;
  }
  public void setIwantSearchType(String iwantSearchType) {
    this.iwantSearchType = iwantSearchType;
  }
  public String getIwantSearchType() {
    return iwantSearchType;
  }
  public void setIwantSearchCode(String iwantSearchCode) {
    this.iwantSearchCode = iwantSearchCode;
  }
  public String getIwantSearchCode() {
    return iwantSearchCode;
  }
  public void setIwantSearchName(String iwantSearchName) {
    this.iwantSearchName = iwantSearchName;
  }
  public String getIwantSearchName() {
    return iwantSearchName;
  }
  public void setLogId(String logId) {
    this.logId = logId;
  }
  public String getLogId() {
    return logId;
  }
  public void setAllGradEmpPercenrage(String allGradEmpPercenrage) {
    this.allGradEmpPercenrage = allGradEmpPercenrage;
  }
  public String getAllGradEmpPercenrage() {
    return allGradEmpPercenrage;
  }
  public void setSalaryRange(String salaryRange) {
    this.salaryRange = salaryRange;
  }
  public String getSalaryRange() {
    return salaryRange;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }
  public String getTotalCount() {
    return totalCount;
  }
  public void setSortByWhich(String sortByWhich) {
    this.sortByWhich = sortByWhich;
  }
  public String getSortByWhich() {
    return sortByWhich;
  }
  public void setSortByHow(String sortByHow) {
    this.sortByHow = sortByHow;
  }
  public String getSortByHow() {
    return sortByHow;
  }
  public void setIntendedYearText(String intendedYearText) {
    this.intendedYearText = intendedYearText;
  }
  public String getIntendedYearText() {
    return intendedYearText;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setQualValue(String qualValue) {
    this.qualValue = qualValue;
  }
  public String getQualValue() {
    return qualValue;
  }
  public void setQualDesc(String qualDesc) {
    this.qualDesc = qualDesc;
  }
  public String getQualDesc() {
    return qualDesc;
  }
  public void setQualList(List qualList) {
    this.qualList = qualList;
  }
  public List getQualList() {
    return qualList;
  }
  public void setQualSub1(String qualSub1) {
    this.qualSub1 = qualSub1;
  }
  public String getQualSub1() {
    return qualSub1;
  }
  public void setQualSub2(String qualSub2) {
    this.qualSub2 = qualSub2;
  }
  public String getQualSub2() {
    return qualSub2;
  }
  public void setQualSub3(String qualSub3) {
    this.qualSub3 = qualSub3;
  }
  public String getQualSub3() {
    return qualSub3;
  }
  public void setQualSub4(String qualSub4) {
    this.qualSub4 = qualSub4;
  }
  public String getQualSub4() {
    return qualSub4;
  }
  public void setQualSub5(String qualSub5) {
    this.qualSub5 = qualSub5;
  }
  public String getQualSub5() {
    return qualSub5;
  }
  public void setQualSub6(String qualSub6) {
    this.qualSub6 = qualSub6;
  }
  public String getQualSub6() {
    return qualSub6;
  }
  public void setQualSubjName1(String qualSubjName1) {
    this.qualSubjName1 = qualSubjName1;
  }
  public String getQualSubjName1() {
    return qualSubjName1;
  }
  public void setQualSubjName2(String qualSubjName2) {
    this.qualSubjName2 = qualSubjName2;
  }
  public String getQualSubjName2() {
    return qualSubjName2;
  }
  public void setQualSubjName3(String qualSubjName3) {
    this.qualSubjName3 = qualSubjName3;
  }
  public String getQualSubjName3() {
    return qualSubjName3;
  }
  public void setQualSubjName4(String qualSubjName4) {
    this.qualSubjName4 = qualSubjName4;
  }
  public String getQualSubjName4() {
    return qualSubjName4;
  }
  public void setQualSubjName5(String qualSubjName5) {
    this.qualSubjName5 = qualSubjName5;
  }
  public String getQualSubjName5() {
    return qualSubjName5;
  }
  public void setQualSubjName6(String qualSubjName6) {
    this.qualSubjName6 = qualSubjName6;
  }
  public String getQualSubjName6() {
    return qualSubjName6;
  }
  public void setResultsHeading(String resultsHeading) {
    this.resultsHeading = resultsHeading;
  }
  public String getResultsHeading() {
    return resultsHeading;
  }
  public void setReturnCodeTwoHeading(String returnCodeTwoHeading) {
    this.returnCodeTwoHeading = returnCodeTwoHeading;
  }
  public String getReturnCodeTwoHeading() {
    return returnCodeTwoHeading;
  }
  public void setSeoSubjectsText(String seoSubjectsText) {
    this.seoSubjectsText = seoSubjectsText;
  }
  public String getSeoSubjectsText() {
    return seoSubjectsText;
  }
  public void setQualGrades1(String qualGrades1) {
    this.qualGrades1 = qualGrades1;
  }
  public String getQualGrades1() {
    return qualGrades1;
  }
  public void setQualGrades2(String qualGrades2) {
    this.qualGrades2 = qualGrades2;
  }
  public String getQualGrades2() {
    return qualGrades2;
  }
  public void setQualGrades3(String qualGrades3) {
    this.qualGrades3 = qualGrades3;
  }
  public String getQualGrades3() {
    return qualGrades3;
  }
  public void setQualGrades4(String qualGrades4) {
    this.qualGrades4 = qualGrades4;
  }
  public String getQualGrades4() {
    return qualGrades4;
  }
  public void setQualGrades5(String qualGrades5) {
    this.qualGrades5 = qualGrades5;
  }
  public String getQualGrades5() {
    return qualGrades5;
  }
  public void setQualGrades6(String qualGrades6) {
    this.qualGrades6 = qualGrades6;
  }
  public String getQualGrades6() {
    return qualGrades6;
  }
  public void setQualUCASPoints1(String qualUCASPoints1) {
    this.qualUCASPoints1 = qualUCASPoints1;
  }
  public String getQualUCASPoints1() {
    return qualUCASPoints1;
  }
  public void setQualUCASPoints2(String qualUCASPoints2) {
    this.qualUCASPoints2 = qualUCASPoints2;
  }
  public String getQualUCASPoints2() {
    return qualUCASPoints2;
  }
  public void setQualUCASPoints3(String qualUCASPoints3) {
    this.qualUCASPoints3 = qualUCASPoints3;
  }
  public String getQualUCASPoints3() {
    return qualUCASPoints3;
  }
  public void setQualGradeList(List qualGradeList) {
    this.qualGradeList = qualGradeList;
  }
  public List getQualGradeList() {
    return qualGradeList;
  }
  public void setJacsCode(String jacsCode) {
    this.jacsCode = jacsCode;
  }
  public String getJacsCode() {
    return jacsCode;
  }
  public void setSearchQualification(String searchQualification) {
    this.searchQualification = searchQualification;
  }
  public String getSearchQualification() {
    return searchQualification;
  }
  public void setSearchGrades(String searchGrades) {
    this.searchGrades = searchGrades;
  }
  public String getSearchGrades() {
    return searchGrades;
  }
  public void setEquivalentTariffPoints(String equivalentTariffPoints) {
    this.equivalentTariffPoints = equivalentTariffPoints;
  }
  public String getEquivalentTariffPoints() {
    return equivalentTariffPoints;
  }
  public void setYearOfEntry(String yearOfEntry) {
    this.yearOfEntry = yearOfEntry;
  }
  public String getYearOfEntry() {
    return yearOfEntry;
  }
  public void setUniId(String uniId) {
    this.uniId = uniId;
  }
  public String getUniId() {
    return uniId;
  }
  public void setUniUkprn(String uniUkprn) {
    this.uniUkprn = uniUkprn;
  }
  public String getUniUkprn() {
    return uniUkprn;
  }
  public void setJacsSubjectList(List jacsSubjectList) {
    this.jacsSubjectList = jacsSubjectList;
  }
  public List getJacsSubjectList() {
    return jacsSubjectList;
  }
  public void setJacsSubGradEmpRate(String jacsSubGradEmpRate) {
    this.JacsSubGradEmpRate = jacsSubGradEmpRate;
  }
  public String getJacsSubGradEmpRate() {
    return JacsSubGradEmpRate;
  }
  public void setAllGradEmpRate(String allGradEmpRate) {
    this.allGradEmpRate = allGradEmpRate;
  }
  public String getAllGradEmpRate() {
    return allGradEmpRate;
  }
  public void setSubjectLowerSalary(String subjectLowerSalary) {
    this.subjectLowerSalary = subjectLowerSalary;
  }
  public String getSubjectLowerSalary() {
    return subjectLowerSalary;
  }
  public void setSubjectHigherSalary(String subjectHigherSalary) {
    this.subjectHigherSalary = subjectHigherSalary;
  }
  public String getSubjectHigherSalary() {
    return subjectHigherSalary;
  }
  public void setAllLowerSalary(String allLowerSalary) {
    this.allLowerSalary = allLowerSalary;
  }
  public String getAllLowerSalary() {
    return allLowerSalary;
  }
  public void setAllHigherSalary(String allHigherSalary) {
    this.allHigherSalary = allHigherSalary;
  }
  public String getAllHigherSalary() {
    return allHigherSalary;
  }
  public void setEmpJacsSubjectName(String empJacsSubjectName) {
    this.empJacsSubjectName = empJacsSubjectName;
  }
  public String getEmpJacsSubjectName() {
    return empJacsSubjectName;
  }
  public void setIndustryCode(String industryCode) {
    this.industryCode = industryCode;
  }
  public String getIndustryCode() {
    return industryCode;
  }
  public void setIndustryDescription(String industryDescription) {
    this.industryDescription = industryDescription;
  }
  public String getIndustryDescription() {
    return industryDescription;
  }
  public void setIndustryWisePerncentage(String industryWisePerncentage) {
    this.industryWisePerncentage = industryWisePerncentage;
  }
  public String getIndustryWisePerncentage() {
    return industryWisePerncentage;
  }
  public void setJobCode(String jobCode) {
    this.jobCode = jobCode;
  }
  public String getJobCode() {
    return jobCode;
  }
  public void setJobDescription(String jobDescription) {
    this.jobDescription = jobDescription;
  }
  public String getJobDescription() {
    return jobDescription;
  }
  public void setJobWisePercentage(String jobWisePercentage) {
    this.jobWisePercentage = jobWisePercentage;
  }
  public String getJobWisePercentage() {
    return jobWisePercentage;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setSubjectWiseEmpRate(String subjectWiseEmpRate) {
    this.subjectWiseEmpRate = subjectWiseEmpRate;
  }
  public String getSubjectWiseEmpRate() {
    return subjectWiseEmpRate;
  }
  public void setLowerPerncentage(String lowerPerncentage) {
    this.lowerPerncentage = lowerPerncentage;
  }
  public String getLowerPerncentage() {
    return lowerPerncentage;
  }
  public void setHigherPercentage(String higherPercentage) {
    this.higherPercentage = higherPercentage;
  }
  public String getHigherPercentage() {
    return higherPercentage;
  }
  public void setIndustryInfoList(List industryInfoList) {
    this.industryInfoList = industryInfoList;
  }
  public List getIndustryInfoList() {
    return industryInfoList;
  }
  public void setJobInfoList(List jobInfoList) {
    this.jobInfoList = jobInfoList;
  }
  public List getJobInfoList() {
    return jobInfoList;
  }
  public void setUniversitiesEmpRateList(List universitiesEmpRateList) {
    this.universitiesEmpRateList = universitiesEmpRateList;
  }
  public List getUniversitiesEmpRateList() {
    return universitiesEmpRateList;
  }
  public void setSchoolNameWCSID(String schoolNameWCSID) {
    this.schoolNameWCSID = schoolNameWCSID;
  }
  public String getSchoolNameWCSID() {
    return schoolNameWCSID;
  }
  public void setSchoolNameFreeTextWCSID(String schoolNameFreeTextWCSID) {
    this.schoolNameFreeTextWCSID = schoolNameFreeTextWCSID;
  }
  public String getSchoolNameFreeTextWCSID() {
    return schoolNameFreeTextWCSID;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setIntendedYearWCSID(String intendedYearWCSID) {
    this.intendedYearWCSID = intendedYearWCSID;
  }
  public String getIntendedYearWCSID() {
    return intendedYearWCSID;
  }
  public void setIntendedYearTextWCSID(String intendedYearTextWCSID) {
    this.intendedYearTextWCSID = intendedYearTextWCSID;
  }
  public String getIntendedYearTextWCSID() {
    return intendedYearTextWCSID;
  }
}
