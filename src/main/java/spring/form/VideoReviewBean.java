package spring.form;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import WUI.utilities.CommonFunction;

public class VideoReviewBean {
  public VideoReviewBean() {
  }
  private String collegeId = "";
  private String collegeName = "";
  private String courseid = "";
  private String coursetitle = "";
  private String videoUrl = "";
  private String videoTitle = "";
  private String videoDescription = "";
  private String termsAndCondition = "";
  private FormFile fileUpload = null;
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
  public void setVideoTitle(String videoTitle) {
    this.videoTitle = videoTitle;
  }
  public String getVideoTitle() {
    return videoTitle;
  }
  public void setVideoDescription(String videoDescription) {
    this.videoDescription = videoDescription;
  }
  public String getVideoDescription() {
    return videoDescription;
  }
  public void setCourseid(String courseid) {
    this.courseid = courseid;
  }
  public String getCourseid() {
    return courseid;
  }
  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }
  public String getCoursetitle() {
    return coursetitle;
  }
  public void setTermsAndCondition(String termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }
  public String getTermsAndCondition() {
    return termsAndCondition;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String validVideoContent = bundle.getString("wnui.limelight.validcontenttypes");
    HttpSession session = request.getSession();
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equals("Submit review")) {
      if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) {
        return errors;
      }
      if (collegeId != null && collegeId.trim().length() == 0) {
        errors.add("invalid_college", new ActionMessage("wuni.error.invalid_college"));
      }
      if (fileUpload != null && fileUpload.getFileName() != null && fileUpload.getFileName().trim().length() == 0) {
        errors.add("blank_upload_videourl", new ActionMessage("wnui.error.blank_upload_videourl"));
      }
      if (videoTitle != null && videoTitle.trim().length() == 0) {
        errors.add("invalid_videotitle", new ActionMessage("wnui.error.invalid_videotitle"));
      }
      if (videoDescription != null && videoDescription.trim().length() == 0) {
        errors.add("invalid_videodesc", new ActionMessage("wnui.error.invalid_videodesc"));
      }
      if (termsAndCondition != null && termsAndCondition.trim().length() == 0) {
        errors.add("invalid_termscond", new ActionMessage("wuni.error.video_tcondition"));
      }
      String returnError = new CommonFunction().checkCapsSwearing(request, getVideoDescription());
      if (returnError != null && returnError.equals("swearing")) {
        errors.add("swearingerror", new ActionMessage("wuni.error.swearing"));
      }
      if (errors.size() > 0) {
        return errors;
      }
      if (fileUpload != null && fileUpload.getFileName() != null && fileUpload.getFileName().trim().length() > 0) {
        errors.clear();
        if (fileUpload != null && fileUpload.getFileSize() == 0) {
          errors.add("blank_upload_videourl", new ActionMessage("wnui.error.blank_upload_videourl"));
          if (errors.size() > 0) {
            return errors;
          }
        }
        if (fileUpload != null && fileUpload.getFileSize() >= 52428800) {
          errors.add("large_file_size", new ActionMessage("wnui.limelight.largefileerror"));
          if (errors.size() > 0) {
            return errors;
          }
        }
        if (fileUpload != null && fileUpload.getContentType() != null && validVideoContent != null && validVideoContent.trim().length() > 0) {
          boolean errorFlag = true;
          String format[] = validVideoContent.split("##");
          if (format != null) {
            for (int i = 0; i < format.length; i++) {
              if (format[i] != null && format[i].trim().equals(fileUpload.getContentType())) {
                errorFlag = false;
              }
            }
            if (errorFlag) {
              errors.add("invalid_file_name", new ActionMessage("wnui.limelight.invalidcontenterror"));
            }
          }
        }
      }
    }
    return errors;
  }*/
  public boolean checkURL(String videoUrl) {
    boolean errorFlag = false;
    String responseQueryString = "";
    int response = 0;
    try {
      URL url = new URL(videoUrl);
      URLConnection connection = url.openConnection();
      if (connection instanceof HttpURLConnection) {
        HttpURLConnection httpConnection = (HttpURLConnection)connection;
        httpConnection.connect();
        response = httpConnection.getResponseCode();
        URL _url = httpConnection.getURL();
        responseQueryString = _url.getQuery();
        if (!(_url.getHost().equals("www.youtube.com") || _url.getHost().equals("uk.youtube.com"))) {
          errorFlag = true;
        } else if (response != 200) {
          errorFlag = true;
        }
        if (responseQueryString == null) {
          errorFlag = true;
        }
      } else {
        errorFlag = true;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      errorFlag = true;
    }
    return errorFlag;
  }
  public void setFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }
  public FormFile getFileUpload() {
    return fileUpload;
  }
}

//////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////    
