package spring.form.advice;

import org.apache.struts.action.ActionForm;

public class AdviceBean {
  public AdviceBean() {
  }
    //Advice home related details
    private String postTitle = "";
    private String postId = "";
    private String postUrl = "";
    private String postShortText = "";
    private String mediaPath = "";
    private String mediaAltText = "";
    private String videoUrl = "";
    private String updatedDate = "";
    private String totalCount = "";
    private String readCount = "";
    private String parentCategoryName = ""; 
    private String subCategoryName = "";
    private String articleViewedCount = "";    
    private String adviceDetailURL = "";
    private String imageName = "";
    
    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostShortText(String postShortText) {
        this.postShortText = postShortText;
    }

    public String getPostShortText() {
        return postShortText;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaAltText(String mediaAltText) {
        this.mediaAltText = mediaAltText;
    }

    public String getMediaAltText() {
        return mediaAltText;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    public String getReadCount() {
        return readCount;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setArticleViewedCount(String articleViewedCount) {
        this.articleViewedCount = articleViewedCount;
    }

    public String getArticleViewedCount() {
        return articleViewedCount;
    }

    public void setAdviceDetailURL(String adviceDetailURL) {
        this.adviceDetailURL = adviceDetailURL;
    }

    public String getAdviceDetailURL() {
        return adviceDetailURL;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }
}
