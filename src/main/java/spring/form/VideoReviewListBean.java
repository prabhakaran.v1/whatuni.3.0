package spring.form;

import org.apache.struts.action.ActionForm;

public class VideoReviewListBean {
  public VideoReviewListBean() {
  }
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseId = "";
  private String courseName = "";
  private String userId = "";
  private String userName = "";
  private String videoReviewId = "";
  private String videoReviewTitle = "";
  private String videoReviewDesc = "";
  private String overallRating = "";
  private String videoUrl = "";
  private String videoLength = "";
  private String videoCategory = "";
  private String noOfHits = "";
  private String thumbnailUrl = "";
  private String responseCode = "";
  private String userNationality = "";
  private String userGender = "";
  private String userAtUni = "";
  private String videoType = "";
  private String profileOverviewFlag = "";
  private String userType = "";
  private String subjectId = "";
  private String collegeProfileFlag = "";
  private String myhcProfileId = "";
  private String profileType = "";
  private String collegeLogo="";  
  
  private String orderItemIdForIp = "";
  private String subOrderEmailIp = "";
  private String subOrderEmailWebformIp = "";
  private String subOrderProspectusIp = "";
  private String subOrderProspectusWebformIp = "";
  private String subOrderWebsiteIp = "";
  private String subOrderItemIdForIp = "";
  private String profileTypeIp = "";
  private String myhcProfileIdIp = "";
  private String cpeQualificationNetworkIdforIp = "";

  private String orderItemIdForSp = ""; 
  private String subOrderEmailSp = "";
  private String subOrderEmailWebformSp = "";
  private String subOrderProspectusSp = "";
  private String subOrderProspectusWebformSp = ""; 
  private String subOrderWebsiteSp = "";
  private String profileTypeSp = "";
  private String myhcProfileIdSp = "";
  private String subOrderItemIdForSp = "";
  private String cpeQualificationNetworkIdforSp = "";
  private String mediaId = null;
  private String hotlineIp = "";  
  private String hotlineSp = "";  
  
   //26-Aug-2014
    private String position = null;
    private String positionId = null;
    private String reviewQuestionId = null;
    private String awardImage = null;
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }
  public String getCourseName() {
    return courseName;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }
  public String getVideoReviewId() {
    return videoReviewId;
  }
  public void setVideoReviewTitle(String videoReviewTitle) {
    this.videoReviewTitle = videoReviewTitle;
  }
  public String getVideoReviewTitle() {
    return videoReviewTitle;
  }
  public void setVideoReviewDesc(String videoReviewDesc) {
    this.videoReviewDesc = videoReviewDesc;
  }
  public String getVideoReviewDesc() {
    return videoReviewDesc;
  }
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  public String getOverallRating() {
    return overallRating;
  }
  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
  public String getVideoUrl() {
    return videoUrl;
  }
  public void setVideoLength(String videoLength) {
    this.videoLength = videoLength;
  }
  public String getVideoLength() {
    return videoLength;
  }
  public void setVideoCategory(String videoCategory) {
    this.videoCategory = videoCategory;
  }
  public String getVideoCategory() {
    return videoCategory;
  }
  public void setNoOfHits(String noOfHits) {
    this.noOfHits = noOfHits;
  }
  public String getNoOfHits() {
    return noOfHits;
  }
  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }
  public String getThumbnailUrl() {
    return thumbnailUrl;
  }
  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }
  public String getResponseCode() {
    return responseCode;
  }
  public void setUserNationality(String userNationality) {
    this.userNationality = userNationality;
  }
  public String getUserNationality() {
    return userNationality;
  }
  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }
  public String getUserGender() {
    return userGender;
  }
  public void setUserAtUni(String userAtUni) {
    this.userAtUni = userAtUni;
  }
  public String getUserAtUni() {
    return userAtUni;
  }
  public void setVideoType(String videoType) {
    this.videoType = videoType;
  }
  public String getVideoType() {
    return videoType;
  }
  public void setProfileOverviewFlag(String profileOverviewFlag) {
    this.profileOverviewFlag = profileOverviewFlag;
  }
  public String getProfileOverviewFlag() {
    return profileOverviewFlag;
  }
  public void setUserType(String userType) {
    this.userType = userType;
  }
  public String getUserType() {
    return userType;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setCollegeProfileFlag(String collegeProfileFlag) {
    this.collegeProfileFlag = collegeProfileFlag;
  }
  public String getCollegeProfileFlag() {
    return collegeProfileFlag;
  }

    public void setMyhcProfileId(String myhcProfileId) {
        this.myhcProfileId = myhcProfileId;
    }

    public String getMyhcProfileId() {
        return myhcProfileId;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileTypeIp(String profileTypeIp) {
        this.profileTypeIp = profileTypeIp;
    }

    public String getProfileTypeIp() {
        return profileTypeIp;
    }

    public void setMyhcProfileIdIp(String myhcProfileIdIp) {
        this.myhcProfileIdIp = myhcProfileIdIp;
    }

    public String getMyhcProfileIdIp() {
        return myhcProfileIdIp;
    }

    public void setSubOrderItemIdForIp(String subOrderItemIdForIp) {
        this.subOrderItemIdForIp = subOrderItemIdForIp;
    }

    public String getSubOrderItemIdForIp() {
        return subOrderItemIdForIp;
    }

    public void setProfileTypeSp(String profileTypeSp) {
        this.profileTypeSp = profileTypeSp;
    }

    public String getProfileTypeSp() {
        return profileTypeSp;
    }

    public void setMyhcProfileIdSp(String myhcProfileIdSp) {
        this.myhcProfileIdSp = myhcProfileIdSp;
    }

    public String getMyhcProfileIdSp() {
        return myhcProfileIdSp;
    }

    public void setSubOrderItemIdForSp(String subOrderItemIdForSp) {
        this.subOrderItemIdForSp = subOrderItemIdForSp;
    }

    public String getSubOrderItemIdForSp() {
        return subOrderItemIdForSp;
    }

   
    public void setCpeQualificationNetworkIdforIp(String cpeQualificationNetworkIdforIp) {
        this.cpeQualificationNetworkIdforIp = cpeQualificationNetworkIdforIp;
    }

    public String getCpeQualificationNetworkIdforIp() {
        return cpeQualificationNetworkIdforIp;
    }

    public void setCpeQualificationNetworkIdforSp(String cpeQualificationNetworkIdforSp) {
        this.cpeQualificationNetworkIdforSp = cpeQualificationNetworkIdforSp;
    }

    public String getCpeQualificationNetworkIdforSp() {
        return cpeQualificationNetworkIdforSp;
    }

    public void setOrderItemIdForIp(String orderItemIdForIp) {
        this.orderItemIdForIp = orderItemIdForIp;
    }

    public String getOrderItemIdForIp() {
        return orderItemIdForIp;
    }

    public void setSubOrderEmailIp(String subOrderEmailIp) {
        this.subOrderEmailIp = subOrderEmailIp;
    }

    public String getSubOrderEmailIp() {
        return subOrderEmailIp;
    }

    public void setSubOrderEmailWebformIp(String subOrderEmailWebformIp) {
        this.subOrderEmailWebformIp = subOrderEmailWebformIp;
    }

    public String getSubOrderEmailWebformIp() {
        return subOrderEmailWebformIp;
    }

    public void setSubOrderProspectusIp(String subOrderProspectusIp) {
        this.subOrderProspectusIp = subOrderProspectusIp;
    }

    public String getSubOrderProspectusIp() {
        return subOrderProspectusIp;
    }

    public void setSubOrderProspectusWebformIp(String subOrderProspectusWebformIp) {
        this.subOrderProspectusWebformIp = subOrderProspectusWebformIp;
    }

    public String getSubOrderProspectusWebformIp() {
        return subOrderProspectusWebformIp;
    }

    public void setSubOrderWebsiteIp(String subOrderWebsiteIp) {
        this.subOrderWebsiteIp = subOrderWebsiteIp;
    }

    public String getSubOrderWebsiteIp() {
        return subOrderWebsiteIp;
    }

    public void setOrderItemIdForSp(String orderItemIdForSp) {
        this.orderItemIdForSp = orderItemIdForSp;
    }

    public String getOrderItemIdForSp() {
        return orderItemIdForSp;
    }

    public void setSubOrderEmailSp(String subOrderEmailSp) {
        this.subOrderEmailSp = subOrderEmailSp;
    }

    public String getSubOrderEmailSp() {
        return subOrderEmailSp;
    }

    public void setSubOrderEmailWebformSp(String subOrderEmailWebformSp) {
        this.subOrderEmailWebformSp = subOrderEmailWebformSp;
    }

    public String getSubOrderEmailWebformSp() {
        return subOrderEmailWebformSp;
    }

    public void setSubOrderProspectusSp(String subOrderProspectusSp) {
        this.subOrderProspectusSp = subOrderProspectusSp;
    }

    public String getSubOrderProspectusSp() {
        return subOrderProspectusSp;
    }

    public void setSubOrderProspectusWebformSp(String subOrderProspectusWebformSp) {
        this.subOrderProspectusWebformSp = subOrderProspectusWebformSp;
    }

    public String getSubOrderProspectusWebformSp() {
        return subOrderProspectusWebformSp;
    }

    public void setSubOrderWebsiteSp(String subOrderWebsiteSp) {
        this.subOrderWebsiteSp = subOrderWebsiteSp;
    }

    public String getSubOrderWebsiteSp() {
        return subOrderWebsiteSp;
    }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setMediaId(String mediaId)
  {
    this.mediaId = mediaId;
  }

  public String getMediaId()
  {
    return mediaId;
  }

    //26-Aug-2014
    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setReviewQuestionId(String reviewQuestionId) {
        this.reviewQuestionId = reviewQuestionId;
    }

    public String getReviewQuestionId() {
        return reviewQuestionId;
    }

    public void setAwardImage(String awardImage) {
        this.awardImage = awardImage;
    }

    public String getAwardImage() {
        return awardImage;
    }
  public void setHotlineIp(String hotlineIp) {
    this.hotlineIp = hotlineIp;
  }
  public String getHotlineIp() {
    return hotlineIp;
  }
  public void setHotlineSp(String hotlineSp) {
    this.hotlineSp = hotlineSp;
  }
  public String getHotlineSp() {
    return hotlineSp;
  }
}
//////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////    
