package spring.sp.advice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.advice.AdviceDetailInfoVO;
import WUI.utilities.GlobalConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AdviceInfoAjaxSP.java
 * Description   : This class used to get the trending and most popular advice page detail on scorll
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Prabhakaran V.          1.0             13.10.2015           First draft   		
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class CheckArticleAndPrimaryCategorySP extends StoredProcedure {
  public CheckArticleAndPrimaryCategorySP(DataSource datasource) {
    //
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_ARTICLE_AVILABLE_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_POST_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_PARENT_CATEGORY_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUB_CATEGORY_NAME", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_PRIMARY_CATEGORY_NAME", Types.VARCHAR));
    compile();
  }

   /**
    * This function is used to map input parameters for the procedure call.
    * @param parameters
    * @return outMap
    */
  public Map execute(AdviceDetailInfoVO adviceDetailInfoVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_POST_ID", adviceDetailInfoVO.getPostId());
    inMap.put("P_PARENT_CATEGORY_NAME", adviceDetailInfoVO.getPrimaryCategoryName());
    inMap.put("P_SUB_CATEGORY_NAME", adviceDetailInfoVO.getSubCategoryName());
    try {
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
