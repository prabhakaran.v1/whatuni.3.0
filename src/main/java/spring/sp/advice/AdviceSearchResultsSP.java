package spring.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.advice.MostPopularAdviceRowMapper;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;

/**
 * @AdviceSearchResultsSP
 * @since        wu333_20141209
 * @author       Thiyagu G
 * Change Log
 * ******************************************************************************************************
 * author	              Ver 	              Modified On     	     Modification Details 	                
 * ******************************************************************************************************
 * Sujitha V              1.1                 2021_JAN_19            Added SEM cursor
 * 
*/

 
public class AdviceSearchResultsSP extends StoredProcedure {

  public AdviceSearchResultsSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setSql("wuni_articles_pkg.get_search_results_prc");        
    declareParameter(new SqlParameter("p_keyword", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.NUMERIC));
    declareParameter(new SqlOutParameter("pc_search_results", OracleTypes.CURSOR, new AdviceSearchResultsRowMapper()));
    declareParameter(new SqlOutParameter("pc_most_popular_articles", OracleTypes.CURSOR, new MostPopularAdviceRowMapper())); //Added popular articles cursor to show the article search page for 11_Aug_2015, By Thiyagu G.
    declareParameter(new SqlOutParameter("pc_trending_popular_articles", OracleTypes.CURSOR, new MostPopularAdviceRowMapper())); //Added trending articles cursor to show the article search page for 11_Aug_2015, By Thiyagu G.
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl()));
  }

  public Map execute(List parameters, String categoryFlag) {
    Map outMap = null;
    try{
      Map inMap = new HashMap();    
      inMap.put("p_keyword", parameters.get(0));    
      inMap.put("p_page_no", parameters.get(1));    
      inMap.put("P_URL", parameters.get(2));  
      outMap = execute(inMap);
      if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
        new AdminUtilities().logDbCallParameterDetails("wuni_articles_pkg.get_search_results_prc", inMap);
      }
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
    
  private class AdviceSearchResultsRowMapper implements RowMapper {    
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("post_title"));
      adviceHomeInfoVO.setPostId(rs.getString("post_id"));
      adviceHomeInfoVO.setPostUrl(rs.getString("post_url"));
      adviceHomeInfoVO.setMediaPath(rs.getString("media_path"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("video_url"));
      if(!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())){
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140_92px.jpg"),rowNum));
        if(!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getVideoUrl())){
         adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140px.jpg"),rowNum));
        }else{          
          adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140_92px.jpg"),rowNum));
        }
      }
      adviceHomeInfoVO.setMediaAltText(rs.getString("media_alt_text"));
     
      adviceHomeInfoVO.setUpdatedDate(rs.getString("updated_date"));
      adviceHomeInfoVO.setTotalCount(rs.getString("total_count")); 
      adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
      adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name"));
      adviceHomeInfoVO.setArticleViewedCount(rs.getString("article_viewed_count"));
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());
      return adviceHomeInfoVO;
    }
  }
 
}
