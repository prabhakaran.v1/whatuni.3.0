package spring.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonUtil;
import com.wuni.util.seo.SeoUrls;

/**
 * Class for Article Home SP
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 *
 * Modification history:
 * **************************************************************************************************************
 * Author              Relase Date              Modification Details
 * **************************************************************************************************************
 * Sri Sankari R       06.03.2020               Added teacher category 
 * Sujitha V           19.01.2021               Added SEM cursor(Meta Details) 
 */ 

public class AdviceHomeInfoSP extends StoredProcedure {

  public AdviceHomeInfoSP(DataSource datasource) {
    
    setDataSource(datasource);
    setSql("wuni_articles_pkg.advice_land_page_prc");    
    declareParameter(new SqlOutParameter("oc_editors_pick_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_research_prep_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_stud_life_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_blog_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_news_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_guides_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("oc_clearing_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("o_bread_crumb", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_parents_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); //Added parents category on 08_Aug_2017, By Thiyagu G
    declareParameter(new SqlOutParameter("oc_accommoadation_feature_list", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); //Added accommodation category on 08_Aug_2017, By Thiyagu G
    declareParameter(new SqlOutParameter("OC_TEACHER_FEATURE_LIST", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); //Added teacher category on 06_Mar_2020, By Sri Sankari R
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl()));
  }

  public Map execute(List parameters) {
  
    Map outMap = null;
    Map inMap = new HashMap();    
    inMap.put("P_URL", parameters.get(6));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("wuni_articles_pkg.advice_land_page_prc", inMap);
    }
    return outMap;
    
  }
  
  //This is used to show the article category pods in home page.
  private class AdviceHomeInfoRowMapper implements RowMapper {    
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        
    
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("post_title"));
      adviceHomeInfoVO.setPostId(rs.getString("post_id"));
      adviceHomeInfoVO.setPostUrl(rs.getString("post_url"));
      adviceHomeInfoVO.setPostShortText(rs.getString("post_short_text"));
      adviceHomeInfoVO.setMediaPath(rs.getString("media_path"));
      if(!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())){
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        if(rowNum==0){        
             adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum)); //Change the image extention by Prabha 28_Jun_16
        }else if(rowNum > 0 && rowNum < 7){
            adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_225px" + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum));//Change the image extention by Prabha 28_Jun_16
        }else if(rowNum > 6){
            adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140_92px"+ adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum));//Change the image extention by Prabha 28_Jun_16
        }                  
      }
      adviceHomeInfoVO.setMediaAltText(rs.getString("media_alt_text"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("video_url"));
      adviceHomeInfoVO.setUpdatedDate(rs.getString("updated_date"));
      adviceHomeInfoVO.setReadCount(rs.getString("read_count"));        
      adviceHomeInfoVO.setTotalCount(rs.getString("total_count"));
      adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name")); 
      if(rs.getString("sub_category_name")!=null){
          adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name").toUpperCase());  
      }
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());
      if(!GenericValidator.isBlankOrNull(rs.getString("sponsored_by_details"))){//9_June_2015_rel
        String sponsorshipdtls = rs.getString("sponsored_by_details");
        String sponsorDtls[] =  sponsorshipdtls.split("##SP##");
        adviceHomeInfoVO.setCollegeId(sponsorDtls[0]);
        adviceHomeInfoVO.setCollegeDisplayName(sponsorDtls[1]);
        adviceHomeInfoVO.setSponsorUrl(sponsorDtls[2]);
        adviceHomeInfoVO.setSponsorLogo(new CommonUtil().getImgPath(sponsorDtls[3], rowNum)); //Added sponsor's logo for 16_May_2017, Bt Thiyagu G.
      }
      return adviceHomeInfoVO;    
    }      
  }
}