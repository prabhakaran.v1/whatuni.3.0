package spring.sp.advice;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.advice.MostPopularAdviceRowMapper;
import WUI.utilities.GlobalConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AdviceInfoAjaxSP.java
 * Description   : This class used to get the trending and most popular advice page detail on scorll
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Prabhakaran V.          1.0             13.10.2015           First draft   		
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class AdviceInfoAjaxSP extends StoredProcedure {
  public AdviceInfoAjaxSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_POPULAR_ARTICLE_FN);
    declareParameter(new SqlOutParameter("OC_ARTICLE_DETAILS_INFO", OracleTypes.CURSOR, new MostPopularAdviceRowMapper()));
    declareParameter(new SqlParameter("p_post_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_type_of_popular", Types.VARCHAR));
    declareParameter(new SqlParameter("p_primary_category_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_pagename", Types.VARCHAR));
    declareParameter(new SqlParameter("p_parent_category_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_sub_category_name", Types.VARCHAR));    
    compile();
  }

   /**
    * This function is used to map input parameters for the procedure call.
    * @param parameters
    * @return outMap
    */
  public Map execute(List parameters) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_post_id", parameters.get(0));
    inMap.put("p_type_of_popular", parameters.get(1));
    inMap.put("p_primary_category_name", parameters.get(2));
    inMap.put("p_user_id", parameters.get(3));
    inMap.put("p_track_session_id", parameters.get(4));
    inMap.put("p_pagename", parameters.get(5));
    inMap.put("p_parent_category_name", parameters.get(6));
    inMap.put("p_sub_category_name", parameters.get(7));
    try {
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
