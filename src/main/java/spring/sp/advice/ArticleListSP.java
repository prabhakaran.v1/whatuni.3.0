package spring.sp.advice;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonUtil;
import com.wuni.util.seo.SeoUrls;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.valueobject.advice.AdviceHomeInfoVO;

/**
 * @ArticleListSP.java
 * @Version : initial draft
 * @since : 23-Apr-2019
 * @author : Sabapathi.S
 * @Purpose  : SP to get the Article list to show in Clearing landing success
 */

public class ArticleListSP extends StoredProcedure {
	
  public ArticleListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("wuni_articles_pkg.get_page_featured_post_fn");
    //
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper()));
    declareParameter(new SqlParameter("p_featured_post_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_no_of_article", Types.VARCHAR));
  }
  
  public Map execute(List parameters) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_featured_post_type", parameters.get(0));
    inMap.put("p_no_of_article", parameters.get(1));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("wuni_articles_pkg.get_page_featured_post_fn", inMap);
    }
    return outMap;
  }
  //This is used to show the article category pods in home page.
  
  private class AdviceHomeInfoRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("post_title"));
      adviceHomeInfoVO.setPostId(rs.getString("post_id"));
      adviceHomeInfoVO.setPostUrl(rs.getString("post_url"));
      adviceHomeInfoVO.setPostShortText(rs.getString("post_short_text"));
      adviceHomeInfoVO.setMediaPath(rs.getString("media_path"));
      if (!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())) {
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        if (rowNum == 0) {
          adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum)); //Change the image extention by Prabha 28_Jun_16
        } else if (rowNum > 0 && rowNum < 7) {
          adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + "_225px" + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum)); //Change the image extention by Prabha 28_Jun_16
        } else if (rowNum > 6) {
          adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + "_140_92px" + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum)); //Change the image extention by Prabha 28_Jun_16
        }
      }
      adviceHomeInfoVO.setMediaAltText(rs.getString("media_alt_text"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("video_url"));
      adviceHomeInfoVO.setUpdatedDate(rs.getString("updated_date"));
      adviceHomeInfoVO.setReadCount(rs.getString("read_count"));
      adviceHomeInfoVO.setTotalCount(rs.getString("total_count"));
      adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
      if (rs.getString("sub_category_name") != null) {
        adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name").toUpperCase());
      }
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());
      if (!GenericValidator.isBlankOrNull(rs.getString("sponsored_by_details"))) { //9_June_2015_rel
        String sponsorshipdtls = rs.getString("sponsored_by_details");
        String sponsorDtls[] = sponsorshipdtls.split("##SP##");
        adviceHomeInfoVO.setCollegeId(sponsorDtls[0]);
        adviceHomeInfoVO.setCollegeDisplayName(sponsorDtls[1]);
        adviceHomeInfoVO.setSponsorUrl(sponsorDtls[2]);
        adviceHomeInfoVO.setSponsorLogo(new CommonUtil().getImgPath(sponsorDtls[3], rowNum)); //Added sponsor's logo for 16_May_2017, Bt Thiyagu G.
      }
      return adviceHomeInfoVO;
    }
  }
}