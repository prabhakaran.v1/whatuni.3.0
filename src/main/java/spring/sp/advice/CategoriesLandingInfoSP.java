package spring.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonUtil;
import com.wuni.util.seo.SeoUrls;

/**
 * @CategoriesLandingInfoSP
 * @since        wu333_20141209
 * @author       Thiyagu G
 * Change Log
 * ******************************************************************************************************
 * author	              Ver 	              Modified On     	     Modification Details 	                
 * ******************************************************************************************************
 * Sujitha V              1.1                 2021_JAN_19            Added SEM cursor(Meta Details)
 * 
*/ 
 
public class CategoriesLandingInfoSP extends StoredProcedure {

  public CategoriesLandingInfoSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setSql("wuni_articles_pkg.category_land_page_prc");    
    //
    declareParameter(new SqlParameter("P_PARENT_CATEGORY_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUB_CATEGORY_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.NUMERIC));   
    declareParameter(new SqlOutParameter("OC_CATEGORY_ARTICLE_LIST", OracleTypes.CURSOR, new AdviceHomeInfoRowMapper())); 
    declareParameter(new SqlOutParameter("o_bread_crumb", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("OC_DEFAULT_COVID_SNIPPET", OracleTypes.CURSOR, new CovidSnippetInfoRowMapper()));
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl()));
  }

  public Map execute(List parameters, String categoryFlag) {
    Map outMap = null;
    Map inMap = new HashMap();
    if("WELLBEING".equals(parameters.get(0))){
      inMap.put("P_PARENT_CATEGORY_NAME", "STUDENT LIFE");  
    }else{
      inMap.put("P_PARENT_CATEGORY_NAME", parameters.get(0));
    }    
    if("primary".equals(categoryFlag)){
      if("WELLBEING".equals(parameters.get(0))){
        inMap.put("P_SUB_CATEGORY_NAME", parameters.get(0));
      }else{
        inMap.put("P_SUB_CATEGORY_NAME", null);
      }
        inMap.put("P_PAGE_NO", parameters.get(1));
        inMap.put("P_URL", parameters.get(2));
    }else{
        inMap.put("P_SUB_CATEGORY_NAME", parameters.get(1));
        inMap.put("P_PAGE_NO", parameters.get(2));
        inMap.put("P_URL", parameters.get(3));
    }    
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("wuni_articles_pkg.category_land_page_prc", inMap);
    }
    return outMap;
  }
    
  private class AdviceHomeInfoRowMapper implements RowMapper {    
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {    
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("post_title"));
      adviceHomeInfoVO.setPostId(rs.getString("post_id"));
      adviceHomeInfoVO.setPostUrl(rs.getString("post_url"));
      adviceHomeInfoVO.setPostShortText(rs.getString("post_short_text"));
      adviceHomeInfoVO.setMediaPath(rs.getString("media_path"));
      if(!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())){
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        if("1".equals(rs.getString("page_no"))){
            if(rowNum==0){                
                adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum)); //Change the image extention by Prabha 28_Jun_16
            }else if(rowNum > 0 && rowNum < 7){
                adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_225px"+ adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum)); //Change the image extention by Prabha 28_Jun_16
            }else if(rowNum > 6){
                adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140_92px"+ adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())),rowNum)); //Change the image extention by Prabha 28_Jun_16
            }    
        }else{
            adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath+"_140_92px.jpg"),rowNum));
        }         
      }      
      adviceHomeInfoVO.setMediaAltText(rs.getString("media_alt_text"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("video_url"));
      adviceHomeInfoVO.setUpdatedDate(rs.getString("updated_date"));
      adviceHomeInfoVO.setReadCount(rs.getString("read_count"));        
      adviceHomeInfoVO.setTotalCount(rs.getString("total_count")); 
      adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name")); 
      adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name"));  
      if(rs.getString("sub_category_name")!=null){
          adviceHomeInfoVO.setSubCategoryNameCapital(rs.getString("sub_category_name").toUpperCase());  
      } 
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());
      
      if(!GenericValidator.isBlankOrNull(rs.getString("sponsored_by_details"))){//9_June_2015_rel
        String sponsorshipdtls = rs.getString("sponsored_by_details");
        String sponsorDtls[] =  sponsorshipdtls.split("##SP##");
        adviceHomeInfoVO.setCollegeId(sponsorDtls[0]);
        adviceHomeInfoVO.setCollegeDisplayName(sponsorDtls[1]);
        adviceHomeInfoVO.setSponsorUrl(sponsorDtls[2]); 
        adviceHomeInfoVO.setSponsorLogo(new CommonUtil().getImgPath(sponsorDtls[3], rowNum)); //Added sponsor's logo for 16_May_2017, Bt Thiyagu G.
      }
      return adviceHomeInfoVO;
    }    
  }  
 
  private class CovidSnippetInfoRowMapper implements RowMapper {    
	    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {    
	      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
	      adviceHomeInfoVO.setPostText(rs.getString("POST_TEXT"));
	      adviceHomeInfoVO.setArticleDate(rs.getString("ARTICLE_DATE"));
	      adviceHomeInfoVO.setArticleTime(rs.getString("ARTICLE_TIME"));
	      adviceHomeInfoVO.setCovidNewsTotalCount(rs.getString("TOTAL_COUNT"));
	      return adviceHomeInfoVO;
	    }    
	  }
}
