package spring.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.SpringConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ArticlesPodSP.java
 * Description   : This class is used to get the articles pod information.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Thiyagu G.              1.0             13.06.2017       First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class ArticlesPodSP extends StoredProcedure {
  
  public ArticlesPodSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql(SpringConstants.GET_ARTICLE_POD_PRC);    
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_TRACK_SESSION_ID", Types.NUMERIC));    
    declareParameter(new SqlParameter("P_PAGENAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_OPPORTUNITY_ID", Types.NUMERIC));    
    declareParameter(new SqlParameter("P_POST_ID", Types.NUMERIC));    
    declareParameter(new SqlParameter("P_PRIMARY_CATEGORY_NAME", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("PC_ARTICLE_POD", OracleTypes.CURSOR, new TopArticlesPodRowMapperImpl()));
    compile();
  }
  
  public Map execute(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", adviceHomeInfoVO.getUserId());
    inMap.put("P_TRACK_SESSION_ID", adviceHomeInfoVO.getUserTrackId());
    inMap.put("P_PAGENAME", adviceHomeInfoVO.getPageName());
    inMap.put("P_OPPORTUNITY_ID", adviceHomeInfoVO.getOpportunityId());
    inMap.put("P_POST_ID", adviceHomeInfoVO.getPostId());
    inMap.put("P_PRIMARY_CATEGORY_NAME", adviceHomeInfoVO.getPrimaryCategoryName());    
    outMap = execute(inMap);
    return outMap;
  }
  
  private class TopArticlesPodRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("ARTICLE_TITLE"));      
      adviceHomeInfoVO.setMediaPath(rs.getString("MEDIA_PATH"));
      if (!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())) {
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + GlobalConstants.IMAGE225Px + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum));
      }
      adviceHomeInfoVO.setPostShortText(rs.getString("PERSONALIZED_TEXT"));
      adviceHomeInfoVO.setPostUrl(rs.getString("URL"));      
      adviceHomeInfoVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
      return adviceHomeInfoVO;
    }
  }
  
}
