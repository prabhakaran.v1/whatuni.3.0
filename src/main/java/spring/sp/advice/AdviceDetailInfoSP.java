package spring.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.advice.MostPopularAdviceRowMapper;
import spring.valueobject.advice.AdviceDetailInfoVO;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

import com.wuni.util.seo.SeoUrls;

/**
 * Class for Article detail SP
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 *
 * Modification history:
 * **************************************************************************************************************
 * Author     Relase Date              Modification Details
 * **************************************************************************************************************
 * Yogeswari  19.05.2015               Added  tracking_session_id
 * Prabhakaran V. 03.11.2015           Commented the most popular and trending article out cursor
 * Prabhakaran V. 21.02.2017           Added p_amp_flag param for AMP page
 * Hema.S         20.11.2018           Added spons_tracking_code for sponsor article tracking
 */
public class AdviceDetailInfoSP extends StoredProcedure {

  public AdviceDetailInfoSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setSql("wuni_articles_pkg.get_article_details_info_prc");
    declareParameter(new SqlParameter("p_post_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_parent_category_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_sub_category_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_primary_category_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_basket_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_preview_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC)); // Yogeswari :: 19.05.2015 :: Added  tracking_session_id.
    
    declareParameter(new SqlOutParameter("OC_ARTICLE_DETAILS_INFO", OracleTypes.CURSOR, new AdviceDetailInfoRowMapper()));
    declareParameter(new SqlOutParameter("OC_MOST_POPULAR_ARTICLES", OracleTypes.CURSOR, new MostPopularAdviceRowMapper())); //Uncommented by Prabha on 21_Feb_2017 
    //Commented by Prabha on 03_NOV_2015_REL
    /*declareParameter(new SqlOutParameter("OC_TRENDING_POPULAR_ARTICLES", OracleTypes.CURSOR, new MostPopularAdviceRowMapper()));*/
    declareParameter(new SqlOutParameter("oc_other_courses", OracleTypes.CURSOR, new OtherCoursesAdviceRowMapper()));
    declareParameter(new SqlOutParameter("oc_univs_in_city", OracleTypes.CURSOR, new UniversityInCityAdviceRowMapper()));
    declareParameter(new SqlOutParameter("oc_similar_article", OracleTypes.CURSOR, new SimilarArticleListRowMapper())); // Yogeswari :: 19.05.2015 :: Added  tracking_session_id.
    declareParameter(new SqlOutParameter("o_bread_crumb", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_cl_course_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_browse_search_url", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_tags", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_sponsors_details", OracleTypes.CURSOR, new SponsoredArticleInfoRowMapper()));//9_June_2015_rel
    declareParameter(new SqlParameter("p_amp_flag", Types.VARCHAR));    
    declareParameter(new SqlOutParameter("oc_top_article", OracleTypes.CURSOR, new MostPopularAdviceRowMapper())); //Added top articles cursor for amp page on 13_June_2017. By Thiyagu G.
  }

  public Map execute(List parameters) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_post_id", parameters.get(0));
    inMap.put("p_parent_category_name", parameters.get(1));
    inMap.put("p_sub_category_name", parameters.get(2));
    inMap.put("p_primary_category_name", parameters.get(3));
    inMap.put("p_user_id", parameters.get(4));
    inMap.put("p_basket_id", parameters.get(5));
    inMap.put("p_network_id", parameters.get(6));
    inMap.put("p_preview_flag", parameters.get(7));
    inMap.put("p_track_session_id", parameters.get(8)); // Yogeswari :: 19.05.2015 :: Added  tracking_session_id.
    inMap.put("p_amp_flag", parameters.get(9));
    try {            
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("wuni_articles_pkg.get_article_details_info_prc", inMap);
    }
    return outMap;
  }
  //This is used to show the article category pods in home page.

  private class AdviceDetailInfoRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceDetailInfoVO adviceDetailInfoVO = new AdviceDetailInfoVO();
      try {
        adviceDetailInfoVO.setPostId(rs.getString("post_id"));
        adviceDetailInfoVO.setAuthorId(rs.getString("author_id"));
        adviceDetailInfoVO.setAuthorName(rs.getString("author_name"));
        adviceDetailInfoVO.setAuthorMediaPath(rs.getString("author_media_path"));
        if (!GenericValidator.isBlankOrNull(adviceDetailInfoVO.getAuthorMediaPath())) {
          String splitAuthorMediaPath = adviceDetailInfoVO.getAuthorMediaPath().substring(0, adviceDetailInfoVO.getAuthorMediaPath().lastIndexOf("."));
          adviceDetailInfoVO.setAuthorImageName(new CommonUtil().getImgPath((splitAuthorMediaPath + "_60px.jpg"), rowNum));
        }
        adviceDetailInfoVO.setAuthorGoogleAccount(rs.getString("author_google_account"));
        adviceDetailInfoVO.setPostTitle(rs.getString("post_title"));
        adviceDetailInfoVO.setPostUrl(rs.getString("post_url"));
        adviceDetailInfoVO.setMetaTitle(rs.getString("meta_title"));
        adviceDetailInfoVO.setMetaDescription(rs.getString("meta_description"));
        adviceDetailInfoVO.setMetaKeywords(rs.getString("meta_keywords"));
        adviceDetailInfoVO.setArticleLdcs1(rs.getString("article_ldcs1"));
        adviceDetailInfoVO.setArticleGroupQualId(rs.getString("article_group_qual_id"));
        adviceDetailInfoVO.setFirstParagraph(rs.getString("first_paragraph"));
        adviceDetailInfoVO.setLastParagraph(rs.getString("last_paragraph"));
        adviceDetailInfoVO.setMediaId(rs.getString("media_id"));
        adviceDetailInfoVO.setMediaPath(rs.getString("media_path"));
        if (!GenericValidator.isBlankOrNull(adviceDetailInfoVO.getMediaPath())) {
          adviceDetailInfoVO.setImageName(new CommonUtil().getImgPath((rs.getString("media_path")), rowNum));
        }
        adviceDetailInfoVO.setMediaAltText(rs.getString("media_alt_text"));
        adviceDetailInfoVO.setVideoUrl(rs.getString("video_url"));
        adviceDetailInfoVO.setMediaType(rs.getString("media_type"));
        adviceDetailInfoVO.setUpdatedFlag(rs.getString("updated_flag"));
        adviceDetailInfoVO.setReadCount(rs.getString("read_count"));
        adviceDetailInfoVO.setPrimaryParentCategoryName(rs.getString("primary_parent_category_name"));
        adviceDetailInfoVO.setPrimarySubCategoryName(rs.getString("primary_sub_category_name"));
        adviceDetailInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
        adviceDetailInfoVO.setSubCategoryName(rs.getString("sub_category_name"));
        adviceDetailInfoVO.setCreatedDate(rs.getString("created_date"));
        adviceDetailInfoVO.setUpdatedDate(rs.getString("updated_date"));
        adviceDetailInfoVO.setPullQuote(rs.getString("pull_quote"));
        ResultSet pdfDetailsRS = (ResultSet)rs.getObject("guide_pdf");
        try {
          if (pdfDetailsRS != null) {
            while (pdfDetailsRS.next()) {
              adviceDetailInfoVO.setPdfId(pdfDetailsRS.getString("pdf_id"));
              adviceDetailInfoVO.setGuideName(pdfDetailsRS.getString("guide_name"));
              adviceDetailInfoVO.setGuideType(pdfDetailsRS.getString("guide_type"));
              adviceDetailInfoVO.setFileName(pdfDetailsRS.getString("file_name"));
              adviceDetailInfoVO.setPdfUrl(CommonUtil.getPdfPath(pdfDetailsRS.getString("file_name")));
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          try {
            pdfDetailsRS.close();
          } catch (Exception e) {
            throw new SQLException(e.getMessage());
          }
        }
        ResultSet nextAdviceDetailsRS = (ResultSet)rs.getObject("next_articles");
        try {
          if (nextAdviceDetailsRS != null) {
            while (nextAdviceDetailsRS.next()) {
              adviceDetailInfoVO.setNextPostId(nextAdviceDetailsRS.getString("post_id"));
              adviceDetailInfoVO.setNextPostTitle(nextAdviceDetailsRS.getString("post_title"));
              adviceDetailInfoVO.setNextPostUrl(nextAdviceDetailsRS.getString("post_url"));
              adviceDetailInfoVO.setNextParentCategoryName(nextAdviceDetailsRS.getString("parent_category_name"));
              adviceDetailInfoVO.setNextSubCategoryName(nextAdviceDetailsRS.getString("sub_category_name"));
              adviceDetailInfoVO.setNextPrimaryCategoryName(nextAdviceDetailsRS.getString("primary_category_name"));
              adviceDetailInfoVO.setNextAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceDetailInfoVO.getNextPrimaryCategoryName(), adviceDetailInfoVO.getNextPostUrl(), adviceDetailInfoVO.getNextPostId()).toLowerCase());
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          try {
            nextAdviceDetailsRS.close();
          } catch (Exception e) {
            throw new SQLException(e.getMessage());
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      return adviceDetailInfoVO;
    }

  }
  //

  private class SimilarArticleListRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
      adviceHomeInfoVO.setPostTitle(rs.getString("post_title"));
      adviceHomeInfoVO.setPostId(rs.getString("post_id"));
      adviceHomeInfoVO.setPostUrl(rs.getString("post_url"));
      adviceHomeInfoVO.setPostShortText(rs.getString("post_short_text"));
      adviceHomeInfoVO.setMediaPath(rs.getString("media_path"));
      if (!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())) {
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + "_225px" + adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum)); //Change the image extention by Prabha 28_Jun_16
      }
      adviceHomeInfoVO.setMediaAltText(rs.getString("media_alt_text"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("video_url"));
      adviceHomeInfoVO.setUpdatedDate(rs.getString("updated_date"));
      adviceHomeInfoVO.setReadCount(rs.getString("read_count"));
      adviceHomeInfoVO.setTotalCount(rs.getString("total_count"));
      adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
      adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name"));
      if (rs.getString("sub_category_name") != null) {
        adviceHomeInfoVO.setSubCategoryNameCapital(rs.getString("sub_category_name").toUpperCase());
      }
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
      adviceHomeInfoVO.setPersonalisedText(rs.getString("personalized_text"));  //Added personalised text on 11_July_2017, By Thiyagu G
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());      
      return adviceHomeInfoVO;
    }

  }
  //This is used to show the article category pods in home page.

  private class OtherCoursesAdviceRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceDetailInfoVO adviceDetailInfoVO = new AdviceDetailInfoVO();
      SeoUrls seoUrl = new SeoUrls();
      adviceDetailInfoVO.setCourseId(rs.getString("course_id"));
      adviceDetailInfoVO.setCourseTitle(rs.getString("course_title"));
      adviceDetailInfoVO.setCollegeId(rs.getString("college_id"));
      adviceDetailInfoVO.setCollegeName(rs.getString("college_name"));
      adviceDetailInfoVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      adviceDetailInfoVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(rs.getString("study_level_desc"), adviceDetailInfoVO.getCourseTitle(), adviceDetailInfoVO.getCourseId(), adviceDetailInfoVO.getCollegeId(), rs.getString("p_page_name")));
      adviceDetailInfoVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(adviceDetailInfoVO.getCollegeName()));
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("enquiry_details");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            adviceDetailInfoVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            adviceDetailInfoVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            adviceDetailInfoVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            adviceDetailInfoVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            adviceDetailInfoVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            adviceDetailInfoVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            adviceDetailInfoVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            adviceDetailInfoVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderProspectusWebform(tmpUrl);
            adviceDetailInfoVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
            String splitMediaThumbPath = "";
            adviceDetailInfoVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
            if (!GenericValidator.isBlankOrNull(adviceDetailInfoVO.getMediaThumbPath())) {
              splitMediaThumbPath = adviceDetailInfoVO.getMediaThumbPath().substring(0, adviceDetailInfoVO.getMediaThumbPath().lastIndexOf("."));
              adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
            } else {
              if (enquiryDetailsRS.getString("media_type_id") != null && "16".equals(enquiryDetailsRS.getString("media_type_id"))) {
                splitMediaThumbPath = adviceDetailInfoVO.getMediaPath().substring(0, adviceDetailInfoVO.getMediaPath().lastIndexOf("."));
                adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((adviceDetailInfoVO.getMediaPath().replace(".", "_140px.")), rowNum));
              } else {
                splitMediaThumbPath = adviceDetailInfoVO.getMediaPath().substring(0, adviceDetailInfoVO.getMediaPath().lastIndexOf("."));
                adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((adviceDetailInfoVO.getMediaPath().replace(".", "_116px.")), rowNum));
              }
            }
            adviceDetailInfoVO.setMediaType(enquiryDetailsRS.getString("media_type"));
            adviceDetailInfoVO.setMediaId(enquiryDetailsRS.getString("media_id"));
            if ("VIDEO".equalsIgnoreCase(adviceDetailInfoVO.getMediaType())) {
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(adviceDetailInfoVO.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
              adviceDetailInfoVO.setVideoThumbPath(thumbnailPath);
            }
            adviceDetailInfoVO.setHotline(enquiryDetailsRS.getString("hotline"));
            adviceDetailInfoVO.setQlFlag(enquiryDetailsRS.getString("ql_flag"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          enquiryDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      adviceDetailInfoVO.setWebsitePrice(rs.getString("website_price"));
      adviceDetailInfoVO.setWebformPrice(rs.getString("webform_price"));
      adviceDetailInfoVO.setCourseInBasket(rs.getString("course_in_basket"));
      adviceDetailInfoVO.setStudyLeveDesc(rs.getString("study_level_desc"));
      adviceDetailInfoVO.setPageName(rs.getString("p_page_name"));
      if ("CLEARING_COURSE".equalsIgnoreCase(rs.getString("p_page_name")) || "CLEARING".equalsIgnoreCase(rs.getString("p_page_name"))) {
        CommonFunction comFn = new CommonFunction();
        if (GenericValidator.isBlankOrNull(adviceDetailInfoVO.getSubOrderItemId()) || ("0").equals(adviceDetailInfoVO.getSubOrderItemId())) {
          adviceDetailInfoVO.setUniHomeURL("/university-clearing-" + GlobalConstants.CLEARING_YEAR + "/" + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(adviceDetailInfoVO.getCollegeName().toLowerCase()))) + "/" + adviceDetailInfoVO.getCollegeId() + ".html");
        } else {
          adviceDetailInfoVO.setUniHomeURL("/university-clearing-" + GlobalConstants.CLEARING_YEAR + "/" + comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(adviceDetailInfoVO.getCollegeName().toLowerCase()))) + "-clearing-overview/" + adviceDetailInfoVO.getCollegeId() + "/" + "page.html");
        }
      } else {
        adviceDetailInfoVO.setUniHomeURL(seoUrl.construnctUniHomeURL(adviceDetailInfoVO.getCollegeId(), adviceDetailInfoVO.getCollegeName()).toLowerCase());
      }
      return adviceDetailInfoVO;
    }

  }

  private class UniversityInCityAdviceRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceDetailInfoVO adviceDetailInfoVO = new AdviceDetailInfoVO();
      SeoUrls seoUrl = new SeoUrls();
      adviceDetailInfoVO.setCollegeId(rs.getString("college_id"));
      adviceDetailInfoVO.setCollegeName(rs.getString("college_name"));
      adviceDetailInfoVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      adviceDetailInfoVO.setUniHomeURL(seoUrl.construnctUniHomeURL(adviceDetailInfoVO.getCollegeId(), adviceDetailInfoVO.getCollegeName()));
      adviceDetailInfoVO.setCollegeLogo(rs.getString("college_logo"));
      adviceDetailInfoVO.setProfileText(rs.getString("profile_text"));
      adviceDetailInfoVO.setWhatTheUniSay(rs.getString("what_the_uni_say"));
      adviceDetailInfoVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(adviceDetailInfoVO.getCollegeName()));
      adviceDetailInfoVO.setCollegeInBasket(rs.getString("college_in_basket"));
      adviceDetailInfoVO.setCityName(rs.getString("city_name"));
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("enquiry_details");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            adviceDetailInfoVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            adviceDetailInfoVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderEmailWebform(tmpUrl);
            adviceDetailInfoVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderProspectusWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            adviceDetailInfoVO.setSubOrderWebsite(tmpUrl);
            adviceDetailInfoVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            adviceDetailInfoVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            adviceDetailInfoVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            adviceDetailInfoVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            adviceDetailInfoVO.setHotline(enquiryDetailsRS.getString("hotline"));
            adviceDetailInfoVO.setMediaId(enquiryDetailsRS.getString("media_id"));
            adviceDetailInfoVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
            adviceDetailInfoVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
            String splitMediaThumbPath = "";
            if (!GenericValidator.isBlankOrNull(adviceDetailInfoVO.getMediaThumbPath())) {
              splitMediaThumbPath = adviceDetailInfoVO.getMediaThumbPath().substring(0, adviceDetailInfoVO.getMediaThumbPath().lastIndexOf("."));
              adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
            } else {
              if (enquiryDetailsRS.getString("media_type_id") != null && "16".equals(enquiryDetailsRS.getString("media_type_id"))) {
                splitMediaThumbPath = adviceDetailInfoVO.getMediaPath().substring(0, adviceDetailInfoVO.getMediaPath().lastIndexOf("."));
                adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((adviceDetailInfoVO.getMediaPath().replace(".", "_140px.")), rowNum));
              } else {
                splitMediaThumbPath = adviceDetailInfoVO.getMediaPath().substring(0, adviceDetailInfoVO.getMediaPath().lastIndexOf("."));
                adviceDetailInfoVO.setThumbImageName(new CommonUtil().getImgPath((adviceDetailInfoVO.getMediaPath().replace(".", "_116px.")), rowNum));
              }
            }
            adviceDetailInfoVO.setMediaType(enquiryDetailsRS.getString("media_type"));
            if ("VIDEO".equalsIgnoreCase(adviceDetailInfoVO.getMediaType())) {
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(adviceDetailInfoVO.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
              adviceDetailInfoVO.setVideoThumbPath(thumbnailPath);
            }
            adviceDetailInfoVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            adviceDetailInfoVO.setQlFlag(enquiryDetailsRS.getString("ql_flag"));
            adviceDetailInfoVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
            adviceDetailInfoVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          enquiryDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return adviceDetailInfoVO;
    }

  }
  
  private class SponsoredArticleInfoRowMapper implements RowMapper {//9_June_2015_rel
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdviceDetailInfoVO adviceDetailInfoVO = new AdviceDetailInfoVO();
      adviceDetailInfoVO.setCollegeId(rs.getString("spons_college_id"));
      adviceDetailInfoVO.setSponsorUrl(rs.getString("spons_url"));
      adviceDetailInfoVO.setCollegeNameDisplay(rs.getString("spons_college_name_display"));
      adviceDetailInfoVO.setMediaId(rs.getString("spons_media_type_id"));
      adviceDetailInfoVO.setMediaPath(rs.getString("spons_media_path"));
      adviceDetailInfoVO.setSponsPixelTracking(rs.getString("spons_tracking_code")); //Added this for sponsor article pixel tracking by Hema.S on 20_NOV_2018_REL
      return adviceDetailInfoVO;
    }
  }
  
}
