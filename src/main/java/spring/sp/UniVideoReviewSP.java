package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.videoreview.bean.VideoReviewListBean;

/**
  * @UniVideoReviewSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the video review page for course provider.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class UniVideoReviewSP extends StoredProcedure {
  public UniVideoReviewSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_UNI_VIDEO_DATA_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_collegeid", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_show_record", Types.VARCHAR));
    declareParameter(new SqlParameter("p_order_by", Types.VARCHAR));
    declareParameter(new SqlParameter("p_review_status", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_related_members", OracleTypes.CURSOR, new CommonMapper.collegeRelatedMembersMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_video_reviews", OracleTypes.CURSOR, new videoReviewsListMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_video_reviews_cnt", Types.VARCHAR));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_collegeid", inputList.get(0));
    inMap.put("p_page_no", inputList.get(1));
    inMap.put("p_show_record", inputList.get(2));
    inMap.put("p_order_by", inputList.get(3));
    inMap.put("p_review_status", inputList.get(4));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_UNI_VIDEO_DATA_PROC", inMap);
    }
    return outMap;
  }

  /**
    * This class is used to build the video review page for the given collegeid.
    */
  private class videoReviewsListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
      VideoReviewListBean bean = new VideoReviewListBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setUserId(rs.getString("user_id"));
      bean.setUserName(rs.getString("user_name"));
      bean.setVideoReviewId(rs.getString("review_id"));
      bean.setVideoReviewTitle(rs.getString("review_title"));
      bean.setVideoCategory("v_category");
      bean.setNoOfHits(rs.getString("no_hit"));
      bean.setUserNationality(rs.getString("nationality"));
      bean.setUserGender(rs.getString("gender"));
      bean.setUserAtUni(rs.getString("at_uni"));
      bean.setProfileOverviewFlag(rs.getString("profile_overview_flag"));
      String videoDesc = rs.getString("review_desc");
      videoDesc = (videoDesc != null && !videoDesc.equalsIgnoreCase("null") && videoDesc.trim().length() > 180)? videoDesc.substring(0, 178) + "...": videoDesc;
      bean.setVideoReviewDesc(videoDesc);
      //      String length = rs.getString("length");
      //      length = length != null && length.trim().length() > 0? length: "";
      //      bean.setVideoLength(length);
      bean.setVideoType(rs.getString("video_type"));
      String videoUrl = rs.getString("video_url");
      String videoType = rs.getString("video_type");
      if (videoType != null && (videoType.equalsIgnoreCase("B") || videoType.equalsIgnoreCase("U"))) { //Backoffice video
        bean.setUserName(GlobalConstants.LIMELIGHT_VIDEO_PROVIDER_NAME);
      }
      if (videoType != null && videoType.equalsIgnoreCase("V")) { //User video
        bean.setVideoUrl(videoUrl);
        bean.setThumbnailUrl(rs.getString("thumbnail_assoc_text"));
      } else {
        bean.setVideoUrl(limeLightPath + videoUrl);
        bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + videoUrl, "0"));
      }
      bean.setOverallRating(rs.getString("overall_rating"));
      return bean;
    }
  }
}
