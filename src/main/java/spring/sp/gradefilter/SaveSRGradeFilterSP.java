package spring.sp.gradefilter;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import spring.gradefilter.bean.SRGradeFilterBean;

public class SaveSRGradeFilterSP extends StoredProcedure{
	
  public SaveSRGradeFilterSP(DataSource dataSource) {
	setDataSource(dataSource);
	setSql(SpringConstants.SR_SAVE_QUAL_DATA_PRC);
	declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_SESSION_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_USER_QUAL_STR", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_GRADE_STR", Types.VARCHAR));  
	compile();
  }
	public Map execute(SRGradeFilterBean srgradeFilterBean) {
		 Map outMap = null;
		    try{
		      Map inMap = new HashMap();
		      inMap.put("P_USER_ID", srgradeFilterBean.getUserId());
			  inMap.put("P_SESSION_ID", srgradeFilterBean.getUserTrackSessionId());//subject session id    
			  inMap.put("P_USER_QUAL_STR", srgradeFilterBean.getUserStudyLevelEntry());    
			  inMap.put("P_USER_GRADE_STR", srgradeFilterBean.getUserEntryPoint());     
		      outMap = execute(inMap); 
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		    return outMap;
	   }
}



 

   