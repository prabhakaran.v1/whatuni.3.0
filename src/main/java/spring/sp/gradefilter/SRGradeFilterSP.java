package spring.sp.gradefilter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import oracle.jdbc.OracleTypes;
import spring.gradefilter.bean.SRGradeFilterBean;
import spring.valueobject.gradefilter.SRGradeFilterVO;

public class SRGradeFilterSP extends StoredProcedure {
	public SRGradeFilterSP(DataSource dataSource) {
		setDataSource(dataSource);
		setSql(SpringConstants.SR_GET_QUALIFICATION_LIST_PRC);
		declareParameter(new SqlInOutParameter("P_SESSION_ID", Types.VARCHAR));    
	    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
	    declareParameter(new SqlParameter("P_GRADE_QUAL_ID", Types.NUMERIC));
	    declareParameter(new SqlParameter("P_AFFILIATE_ID", Types.NUMERIC));
	    declareParameter(new SqlOutParameter("PC_QUALIFICATION_LIST", OracleTypes.CURSOR, new GetGradeFilterInfoRowmapperImpl()));
	    declareParameter(new SqlOutParameter("P_USER_QUAL_STR", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_USER_GRADE_STR", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_UCAS_POINTS", Types.VARCHAR));
	    compile();
	}
	public Map execute(SRGradeFilterBean srgradeFilterBean){
	    Map outMap = null;
	    try{
	      Map inMap = new HashMap();
	        inMap.put("P_SESSION_ID", srgradeFilterBean.getUserTrackSessionId());
		    inMap.put("P_USER_ID", srgradeFilterBean.getUserId());
		    inMap.put("P_GRADE_QUAL_ID", srgradeFilterBean.getQualId());    
		    inMap.put("P_AFFILIATE_ID", srgradeFilterBean.getAffiliateId());    		    
	      outMap = execute(inMap); 
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    return outMap;
	  }
	 public class GetGradeFilterInfoRowmapperImpl implements RowMapper<Object> {
		    @Override
		    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		       SRGradeFilterVO gradeFilterVO = new SRGradeFilterVO();
		       gradeFilterVO.setQualId(rs.getString("QUAL_ID"));
		       gradeFilterVO.setQualification(rs.getString("QUALIFICATION"));
		       gradeFilterVO.setQualificationUrl(rs.getString("QUALIFICATION_URL"));
		       gradeFilterVO.setParentQualification(rs.getString("PARENT_QUALIFICATION"));
		       gradeFilterVO.setGradeOptions(rs.getString("GRADE_STR"));
		       gradeFilterVO.setMaxPoint(rs.getString("MAX_POINT"));
		       gradeFilterVO.setMaxTotalPoint(rs.getString("MAX_TOTAL_POINT"));
		       gradeFilterVO.setGradeOptionflag(rs.getString("GRADE_OPTION_FLAG"));
		       return gradeFilterVO;
		    }
		   }
		}
