package spring.sp.gradefilter;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import oracle.jdbc.OracleTypes;
import spring.gradefilter.bean.SRGradeFilterBean;

public class DeleteSRGradeFilterSP extends StoredProcedure{
  public DeleteSRGradeFilterSP(DataSource datasource) {
	setDataSource(datasource);
	setSql(SpringConstants.SR_DELETE_USER_SUBJ_GRADE_FN);
	setFunction(true);
	declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));    
	declareParameter(new SqlParameter("P_SESSION_ID",  OracleTypes.NUMBER));    
	declareParameter(new SqlParameter("P_USER_ID",  OracleTypes.NUMBER));
	//declareParameter(new SqlParameter("P_AFFILIATE_ID", Types.NUMERIC));
	compile();
  }
  public Map execute(SRGradeFilterBean srgradeFilterBean) {
	Map outMap = null;
	try {
		Map inMap = new HashMap();
	      inMap.put("P_SESSION_ID", srgradeFilterBean.getUserTrackSessionId());//subject session id
	      inMap.put("P_USER_ID", srgradeFilterBean.getUserId());    
		  outMap = execute(inMap);
	} catch (Exception e) {
		 e.printStackTrace();
	}
	return outMap;
	  
	  
  }
}
