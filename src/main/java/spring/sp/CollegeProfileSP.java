package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import spring.valueobject.interaction.CpeInteractionVO;
import spring.valueobject.profile.ProfileTabsVO;
import WUI.search.form.CollegeProfileBean;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.videoreview.bean.VideoReviewListBean;

import com.layer.util.rowmapper.advert.SpListRowMapperImpl;
import com.layer.util.rowmapper.advert.UniProfileInfoRowMapperImpl;
import com.layer.util.rowmapper.common.StudyLevelListRowMapperImpl;
import com.layer.util.rowmapper.common.StudyModeListMapperImpl;
import com.layer.util.rowmapper.openday.OpenDayInfoRowMapperImpl;
import com.layer.util.rowmapper.review.CollegeOverallRatingMapperImpl;
import com.layer.util.rowmapper.review.GoodOrBadReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniReviewRowMapperImpl;
import com.layer.util.rowmapper.scholarship.ScholarshipRowMapperImpl;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.layer.util.rowmapper.search.UniversityDPPodRowMapperImpl;


/**
  * @CollegeProfileSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the college profile page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                               Rel Ver.
  * *************************************************************************************************************************
  * 02.09.2009   Selvakumar              whatuni_1_60   Changed the video and image for the profile point to w_media table
  */
public class CollegeProfileSP extends StoredProcedure {
  public CollegeProfileSP() {
  }
  public CollegeProfileSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_COLLEGE_PROFILE_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_userid", Types.VARCHAR));
    declareParameter(new SqlParameter("p_sessionid", Types.VARCHAR));
    declareParameter(new SqlParameter("p_collegeid", Types.VARCHAR));
    declareParameter(new SqlParameter("p_review_status", Types.VARCHAR));
    declareParameter(new SqlParameter("p_request_description", Types.VARCHAR));
    declareParameter(new SqlParameter("p_client_ip", Types.VARCHAR));
    declareParameter(new SqlParameter("p_client_browser", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_section", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_latest_uni_reviews", OracleTypes.CURSOR, new CommonMapper.collegeLatestReviewsMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_profile_content", OracleTypes.CURSOR, new collegeProfileContentMapperImpl()));      
    declareParameter(new SqlOutParameter("o_uni_profile_tabs", OracleTypes.CURSOR, new CollegeProfileTabsRowMapper()));    
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referel_url", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_myhc_profile_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_profile_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_cpe_qual_network_id", Types.VARCHAR));    
    declareParameter(new SqlOutParameter("o_profile_interaction_details", OracleTypes.CURSOR, new ProfileInteractionDetailsRowMapper()));
    declareParameter(new SqlOutParameter("o_uniprofile_info", OracleTypes.CURSOR, new UniProfileInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_unsitats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_sp_list", OracleTypes.CURSOR, new SpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_open_day_info", OracleTypes.CURSOR, new OpenDayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_students_rating", OracleTypes.CURSOR, new UniReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_related_members", OracleTypes.CURSOR, new CommonMapper.collegeRelatedMembersMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_level_list", OracleTypes.CURSOR, new StudyLevelListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_mode_list", OracleTypes.CURSOR, new StudyModeListMapperImpl()));
    declareParameter(new SqlOutParameter("o_scholarship", OracleTypes.CURSOR, new ScholarshipRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_good_bad_reviews", OracleTypes.CURSOR, new GoodOrBadReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_overall_rating", OracleTypes.CURSOR, new CollegeOverallRatingMapperImpl()));
    declareParameter(new SqlOutParameter("o_video_count", Types.NUMERIC));
    declareParameter(new SqlOutParameter("o_university_dppod", OracleTypes.CURSOR, new UniversityDPPodRowMapperImpl()));//wu411_20120612
    declareParameter(new SqlOutParameter("o_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_avg_rating",Types.VARCHAR));    
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_college_ucas_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_uni_video_reviews", OracleTypes.CURSOR, new videoReviewsListMapperImpl()));//21-Jan-2014
    declareParameter(new SqlParameter("p_track_session", OracleTypes.VARCHAR));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {  
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_userid", inputList.get(0));
    inMap.put("p_sessionid", inputList.get(1));
    inMap.put("p_collegeid", inputList.get(2));
    inMap.put("p_review_status", inputList.get(3));
    inMap.put("p_request_description", inputList.get(4));
    inMap.put("p_client_ip", inputList.get(5));
    inMap.put("p_client_browser", inputList.get(6));
    inMap.put("p_college_section", inputList.get(7));
    inMap.put("p_request_url", inputList.get(8));
    inMap.put("p_referel_url", inputList.get(9));
    inMap.put("p_myhc_profile_id", inputList.get(10));
    inMap.put("p_profile_id", inputList.get(11));
    inMap.put("p_cpe_qual_network_id", inputList.get(12));
    inMap.put("p_track_session", inputList.get(13));    
    outMap = execute(inMap);
    return outMap;
  }

  /**
   *  This class is used to load all the tabs in college profile tabs.
   */
  private class CollegeProfileTabsRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ProfileTabsVO profileTabsVO = new ProfileTabsVO();
      profileTabsVO.setMyhcProfileId(rs.getString("MYHC_PROFILE_ID"));
      profileTabsVO.setProfileId(rs.getString("PROFILE_ID"));            
      profileTabsVO.setButtonLabel(rs.getString("BUTTON_LABEL"));
      return profileTabsVO;
    }
  }
  
  /**
   *  This class is used to load the collegeprofile content for a specific college.
   */
  private class collegeProfileContentMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CollegeProfileBean bean = new CollegeProfileBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setProfileImagePath(rs.getString("image_path"));
      bean.setProfileDescription(rs.getString("description"));
      bean.setSelectedSectionName(rs.getString("button_lable"));      
      // changed for 22nd September 2009 release to get the image from w_media table after CMI migration
      bean.setCollegeImagePath("");
      String dataArray[] = null;
      String durationArray[] = null;
      if (rs.getString("image_path") != null && rs.getString("image_path").trim().length() > 0) {
        dataArray = rs.getString("image_path").split("###", 3);
      }
      String imagePath = "";
      int lenth = dataArray != null ? dataArray.length : 0;
      if (lenth == 1) {
        imagePath = dataArray[0];
        bean.setCollegeImagePath(imagePath);
      } else if (lenth > 1) {
        bean.setVideoId(dataArray[0]);
        bean.setVideoUrl(dataArray[1]);
//        bean.setVideoThumbnailUrl(new GlobalFunction().getVideoThumbPath(dataArray[1],dataArray[0]));
       if(bean.getVideoUrl().indexOf(".flv") > -1){
         String videoDuration = dataArray[2]; //wu505_23042013 release
          if(!GenericValidator.isBlankOrNull(videoDuration)){
            durationArray = videoDuration.split(":", 2);
            int length1 = durationArray != null? durationArray.length: 0;
            if(length1 > 1){
              String minutes = durationArray[0];
              String seconds = durationArray[1];
              bean.setVideoMetaDuration("T"+minutes+"M"+seconds+"S");
            }
          }
       }
        //bean.setVideoThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(dataArray[1], "4"));
         String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(dataArray[0], "4"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
         bean.setVideoThumbnailUrl(thumbnailPath);
        
      }      
      if (rs.getString("js_stats_log_id") != null && rs.getString("js_stats_log_id").trim().length() > 0) {
        bean.setJsStatsLogId(rs.getString("js_stats_log_id"));
      }
      bean.setDLogTypeKey(rs.getString("d_log_type_key"));
      return bean;
    }
  }
  
  /**
   *  This class is used to load the interaaction details for a profile based on suborderitemid.
   */
  private class ProfileInteractionDetailsRowMapper implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CpeInteractionVO cpeInteractionVO = new CpeInteractionVO();
      cpeInteractionVO.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
      cpeInteractionVO.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      cpeInteractionVO.setSubOrderWebsite(resultset.getString("WEBSITE"));
      cpeInteractionVO.setSubOrderEmail(resultset.getString("EMAIL"));
      cpeInteractionVO.setSubOrderEmailWebform(resultset.getString("EMAIL_WEBFORM"));
      cpeInteractionVO.setSubOrderProspectus(resultset.getString("PROSPECTUS"));
      cpeInteractionVO.setSubOrderProspectusWebform(resultset.getString("PROSPECTUS_WEBFORM")); 
      cpeInteractionVO.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID")); 
      cpeInteractionVO.setProfileType(resultset.getString("PROFILE_TYPE"));      
      cpeInteractionVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
      return cpeInteractionVO; 
    }
  }
  
  /**
    * This class is used to build the video review page for the given collegeid.
    */
  private class videoReviewsListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
      VideoReviewListBean bean = new VideoReviewListBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setUserId(rs.getString("user_id"));
      bean.setUserName(rs.getString("user_name"));
      bean.setVideoReviewId(rs.getString("review_id"));
      bean.setVideoReviewTitle(rs.getString("review_title"));
      bean.setVideoCategory("v_category");
      bean.setNoOfHits(rs.getString("no_hit"));
      bean.setUserNationality(rs.getString("nationality"));
      bean.setUserGender(rs.getString("gender"));
      bean.setUserAtUni(rs.getString("at_uni"));
      bean.setProfileOverviewFlag(rs.getString("profile_overview_flag"));
      String videoDesc = rs.getString("review_desc");
      videoDesc = (videoDesc != null && !videoDesc.equalsIgnoreCase("null") && videoDesc.trim().length() > 180)? videoDesc.substring(0, 178) + "...": videoDesc;
      bean.setVideoReviewDesc(videoDesc);
      //      String length = rs.getString("length");
      //      length = length != null && length.trim().length() > 0? length: "";
      //      bean.setVideoLength(length);
      bean.setVideoType(rs.getString("video_type"));
      String videoUrl = rs.getString("video_url");
      String videoType = rs.getString("video_type");
      if (videoType != null && (videoType.equalsIgnoreCase("B") || videoType.equalsIgnoreCase("U"))) { //Backoffice video
        bean.setUserName(GlobalConstants.LIMELIGHT_VIDEO_PROVIDER_NAME);
      }
      if (videoType != null && videoType.equalsIgnoreCase("V")) { //User video
        bean.setVideoUrl(videoUrl);
        bean.setThumbnailUrl(rs.getString("thumbnail_assoc_text"));
      } else {
        bean.setVideoUrl(limeLightPath + videoUrl);
        bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + videoUrl, "0"));
      }
      bean.setOverallRating(rs.getString("overall_rating"));
      return bean;
    }
  }
}
