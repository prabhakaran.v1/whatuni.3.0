package spring.sp;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;

/**
  * @OpenDaysInfoSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the open day information for a specific course provider.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class OpenDaysInfoSP extends StoredProcedure {
  public OpenDaysInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_OPENDAYS_INFO_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_collegeid", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_related_members", OracleTypes.CURSOR, new CommonMapper.collegeRelatedMembersMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_unsitats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));    
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", OracleTypes.VARCHAR));//9_DEC_2014 Added by Priyaa for pixel tracking    
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_collegeid", inputList.get(0)); //college-id  
    outMap = execute(inMap);
    return outMap;
  }
}
