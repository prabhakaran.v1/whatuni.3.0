package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.seo.BreadcrumbVO;
import WUI.utilities.GlobalConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : BreadcrumbSP.java
 * Description   : Getting breadcrumb content..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             21.03.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class BreadcrumbSP extends StoredProcedure {
  public BreadcrumbSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_BREADCRUMB_PRC);
    declareParameter(new SqlParameter("p_calling_from", Types.VARCHAR));
    declareParameter(new SqlParameter("p_qualcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_cat_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_cat_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_location", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_search_keyword", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_breadcrumbs", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_bc_schema_tagging", OracleTypes.CURSOR, new BreadcrumbSchemaRowMapperImpl()));
    compile();
  }
  public Map execute(BreadcrumbVO breadcrumbVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_calling_from", breadcrumbVO.getPageName());
    inMap.put("p_qualcode", breadcrumbVO.getQualCode());
    inMap.put("p_cat_id", breadcrumbVO.getCatId());
    inMap.put("p_cat_code", breadcrumbVO.getCatCode());
    inMap.put("p_location", breadcrumbVO.getLocation());
    inMap.put("p_college_id", breadcrumbVO.getCollegeId());
    inMap.put("p_search_keyword", breadcrumbVO.getSearchKeyword());
    outMap = execute(inMap);
    return outMap;
  }
  public class BreadcrumbSchemaRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      BreadcrumbVO breadcrumbVO = new BreadcrumbVO();
      breadcrumbVO.setBreadcrumbUrl(rs.getString("bc_url"));
      breadcrumbVO.setBradcrumbText(rs.getString("bc_url_desc"));
      breadcrumbVO.setItemId(rs.getString("display_seq"));
      return breadcrumbVO;
    }
  }
}
