package spring.sp.coursejourney;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.common.RandomSubjectListRowMapperImpl;
import spring.rowmapper.common.StaticSubjectListRowMapperImpl;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.GlobalConstants;

/**
  * @JourneyCategorySP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the category landing page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class JourneyCategorySP extends StoredProcedure {

  public JourneyCategorySP() {
  }

  public JourneyCategorySP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_BROWSE_CATEGORY_LIST_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_study_level_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_study_level_desc", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_random_subject_list", OracleTypes.CURSOR, new RandomSubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_static_subject_list", OracleTypes.CURSOR, new StaticSubjectListRowMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_study_level_id", inputList.get(0));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_BROWSE_CATEGORY_LIST_PROC", inMap);
    }
    return outMap;
  }

}
