package spring.sp.coursejourney;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.LocationUniBrowseBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

/**
  * @LocationBrowseSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the location browse page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class LocationBrowseSP extends StoredProcedure {
  public LocationBrowseSP() {
  }
  public LocationBrowseSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_UNI_LOCATION_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_uni_location_list", OracleTypes.CURSOR, new browseLocationMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    outMap = execute(inMap);
    return outMap;
  }

  /**
   *  This class is used to build the browse location list.
   */
  private class browseLocationMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      LocationUniBrowseBean bean = new LocationUniBrowseBean();
      bean.setLocationId(rs.getString("location_id"));
      bean.setLocationName(rs.getString("location_name"));
      bean.setRowLevel(rs.getString("row_level"));
      bean.setCountyNameParam(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("location_name").replaceAll("&", "and"))).toLowerCase());
      return bean;
    }
  }
}
