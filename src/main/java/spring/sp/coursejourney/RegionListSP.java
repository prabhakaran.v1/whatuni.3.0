package spring.sp.coursejourney;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.CourseJourneyBean;
import spring.rowmapper.common.RandomSubjectListRowMapperImpl;
import spring.rowmapper.common.StaticSubjectListRowMapperImpl;
import spring.valueobject.articles.ArticleVO;
import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;

public class RegionListSP extends StoredProcedure {

  public RegionListSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql("WUNI_SPRING_PKG.GET_BROWSE_PAGE_DATA_PRC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_browse_cat_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_qual_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_loc_list", OracleTypes.CURSOR, new locationListMapperImpl()));
    declareParameter(new SqlOutParameter("o_randomize_subjects", OracleTypes.CURSOR, new RandomSubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_static_subject_list", OracleTypes.CURSOR, new StaticSubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_article_pod", OracleTypes.CURSOR, new ArticleRowMapper()));
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", inputList.get(0));
    inMap.put("p_browse_cat_id", inputList.get(1));
    inMap.put("p_qual_code", inputList.get(2));
    outMap = execute(inMap);
    return outMap;
  }

  private class locationListMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int i) throws SQLException {
      CommonFunction common = new CommonFunction();
      CourseJourneyBean bean = new CourseJourneyBean();
      bean.setSubjectId(resultset.getString("course_id"));
      bean.setSubjectName(resultset.getString("course_name"));
      bean.setSeoSubjectName(common.replaceHypen(common.replaceURL(resultset.getString("course_name"))));
      bean.setCategoryCode(resultset.getString("category_code"));
      bean.setStudyLevelId(resultset.getString("study_level"));
      bean.setCategoryCodeLength(resultset.getString("cat_code_length"));
      bean.setParentCategoryId(resultset.getString("parent_category_code"));
      /*if (bean.getSubjectId() != null && subCategoryId.equalsIgnoreCase(bean.getSubjectId())) {
              request.setAttribute("categoryName", resultset.getString("course_name"));
            } */
      return bean;
    }

  }
  //WU408_24042012_Sekhar , This one used to show the article pod in the regionList page.
  private class ArticleRowMapper implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
        ArticleVO articleVO = new ArticleVO();
        articleVO.setArticleGroupUrlSeoName(rs.getString("article_group_url_seo_name"));
        articleVO.setArticleHeading(rs.getString("article_heading"));
        articleVO.setArticleUrlSeoTitle(rs.getString("article_url_seo_title"));
        articleVO.setArticleGroupDescription(rs.getString("article_short_desc"));
        articleVO.setArticleUpdatedDate(rs.getString("article_updated_date"));
        articleVO.setArticleMediaThumbPath(rs.getString("article_thumb_path"));
        if(rs.getString("article_group_url_seo_name") != null){
            articleVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(rs.getString("article_url_seo_title"),articleVO.getArticleGroupUrlSeoName()).toLowerCase());      
        }else{
            articleVO.setArticleUrl("");      
        }
        return articleVO;
    }
  }
}
