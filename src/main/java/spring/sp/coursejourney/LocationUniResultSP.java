package spring.sp.coursejourney;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.LocationUniBrowseBean;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

/**
  * @LocationUniResultSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the location based provider page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class LocationUniResultSP extends StoredProcedure {
  public LocationUniResultSP() {
  }
  public LocationUniResultSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_BROWSE_LOCATION_UNI_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_record_cnt", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_location_list", OracleTypes.CURSOR, new browseLocationMapperImpl()));
    declareParameter(new SqlOutParameter("o_university_list", OracleTypes.CURSOR, new locationUniversityResultMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_location_id", inputList.get(0));
    inMap.put("p_page_no", inputList.get(1));
    inMap.put("p_record_cnt", inputList.get(2));    
    outMap = execute(inMap);
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_BROWSE_LOCATION_UNI_PROC", inMap);
    }
    return outMap;
  }

  /**
   * This class is used to build the location list.
   */
  private class browseLocationMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      LocationUniBrowseBean bean = new LocationUniBrowseBean();
      bean.setLocationId(rs.getString("location_id"));
      bean.setLocationName(rs.getString("location_name"));
      bean.setRowLevel(rs.getString("row_level"));
      bean.setCountyNameParam(new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("location_name").replaceAll("&", "and"))).toLowerCase());
      return bean;
    }
  }

  /**
   * This class is used to build the location based course provider list.
   */
  private class locationUniversityResultMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    String address = "";
     
    LocationUniBrowseBean bean = new LocationUniBrowseBean();

        bean.setParentRegionName(rs.getString("parent_location_name"));
        bean.setSelectedCountyname(rs.getString("location_name"));
        bean.setTotalCount(rs.getString("total_count"));
        bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
        bean.setCollegeId(rs.getString("college_id"));
        bean.setCollegeProfileFlag(rs.getString("profile_flag")); //CODE-AUDIT_Syed_20110427: not used in front end, can be removed during code-cleanup
        bean.setSubjectProfileCount(rs.getString("sub_profile_cnt"));
        bean.setAllCourseCount(rs.getString("total_cnt"));
        bean.setUgCourseCount(rs.getString("ug_cnt"));
        bean.setPgCourseCount(rs.getString("pg_cnt"));
        bean.setReviewCount(rs.getString("review_count"));
        bean.setReviewRating(rs.getString("review_rating"));
        bean.setScholarshipCount(rs.getString("scholarship_cnt"));
        String tmpUrl = "";
        ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
        try{
          if(enquiryDetailsRS != null){
            while(enquiryDetailsRS.next()){
              bean.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID")); 
              bean.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
              bean.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
              bean.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
              bean.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
              bean.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
              
              tmpUrl = enquiryDetailsRS.getString("WEBSITE");
              if(tmpUrl != null && tmpUrl.trim().length() > 0){
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else { tmpUrl = ""; }
              bean.setSubOrderWebsite(tmpUrl);
              
              tmpUrl =  enquiryDetailsRS.getString("EMAIL_WEBFORM");
              if(tmpUrl != null && tmpUrl.trim().length() > 0){
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else { tmpUrl = ""; }
              bean.setSubOrderEmailWebform(tmpUrl);
              
              tmpUrl =enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
              if(tmpUrl != null && tmpUrl.trim().length() > 0){
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else { tmpUrl = ""; }
              bean.setSubOrderProspectusWebform(tmpUrl);                
            }
          }
        } catch(Exception e) {
          e.printStackTrace();
        } finally {
          try {
            if(enquiryDetailsRS != null){
              enquiryDetailsRS.close();
            }
          } catch(Exception e) {
            throw new SQLException(e.getMessage());
          }
        }
      
      if (rs.getString("town") != null && rs.getString("town").trim().length() > 0) {
        bean.setTown(rs.getString("town"));
      } else {
        bean.setTown("&nbsp;");
      }            
      
      if (rs.getString("address_line_1") != null && rs.getString("address_line_1").trim().length() > 0) {
        address = rs.getString("address_line_1");
      }
      if (rs.getString("address_line_2") != null && rs.getString("address_line_2").trim().length() > 0) {
        address += ", " + rs.getString("address_line_2");
      }
      if (rs.getString("town") != null && rs.getString("town").trim().length() > 0) {
        address += ", " + rs.getString("town");
      }
      if (rs.getString("county_state") != null && rs.getString("county_state").trim().length() > 0) {
        address += ", " + rs.getString("county_state");
      }
      if (rs.getString("postcode_zip_prefix") != null && rs.getString("postcode_zip_prefix").trim().length() > 0) {
        address += ", " + rs.getString("postcode_zip_prefix");
      }
      bean.setAddress(address);
      //bean.setProspectusPaidFlag(rs.getString("prospectus_pur_flag"));
      String prospectus_pur_flag[] =  rs.getString("prospectus_pur_flag").split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
      bean.setProspectusPaidFlag(prospectus_pur_flag[0]);
      String prospectusurl = "";
      if(prospectus_pur_flag !=null && prospectus_pur_flag.length>1) {
         prospectusurl = prospectus_pur_flag[1];
      }
      bean.setProspectusExternalUrl(prospectusurl);
      
      // added for 01st December 2009 Release
      bean.setCollegeLogo("");
      bean.setVideoThumbnail("");
      String videoCollegeLogo = rs.getString("college_logo");
      if (videoCollegeLogo != null && videoCollegeLogo.trim().length() > 0 && (videoCollegeLogo.indexOf(".flv") >= 0 || videoCollegeLogo.indexOf(".mp4") > -1)) {
        String college_video_url[] = videoCollegeLogo.split("###");
        if (college_video_url != null && college_video_url.length > 1) {
          bean.setVideoReviewId(college_video_url[1]); // videoreviewid 
        }
        if (college_video_url != null && college_video_url.length > 2) {
          bean.setVideoUrl(college_video_url[2]); // video URL 
          bean.setVideoThumbnail(new GlobalFunction().videoThumbnail(college_video_url[1], "2"));
        }
      } else if (videoCollegeLogo != null && videoCollegeLogo.trim().length() > 0) {
        bean.setCollegeLogo(videoCollegeLogo);
      }
      bean.setNextOpenDay(rs.getString("next_open_date"));
      bean.setWebsitePurchaseFlag(rs.getString("website_pur_flag"));
      bean.setCollegeLocation(new CommonFunction().replaceHypen(rs.getString("college_location")));
      String collegenamelower = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("college_name")));
             collegenamelower = collegenamelower !=null ? collegenamelower.toLowerCase() :"";
      bean.setSeoCollegeName(collegenamelower);
      
      tmpUrl = rs.getString("college_website");
      if(!GenericValidator.isBlankOrNull(tmpUrl)){
        if(tmpUrl.indexOf("whatuni.com") >=0 ) {
          tmpUrl = (tmpUrl.indexOf("https://") >= 0) ? tmpUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (tmpUrl.indexOf("http://") >= 0) ? tmpUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + tmpUrl;
        } else {
          tmpUrl = (tmpUrl.indexOf("http://") >= 0 || tmpUrl.indexOf("https://") >= 0) ? tmpUrl : "http://" + tmpUrl;
        }
      } else { tmpUrl = ""; }
      bean.setWebSite(tmpUrl);   
      
      return bean;
    }
  }
}
