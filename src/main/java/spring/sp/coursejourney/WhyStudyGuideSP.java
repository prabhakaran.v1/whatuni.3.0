package spring.sp.coursejourney;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.CourseJourneyBean;
import WUI.search.form.SearchBean;
import WUI.utilities.CommonFunction;

import com.layer.util.rowmapper.search.TopCoursesRowMapperImpl;

/**
  * @WhyStudyGuideSP
  * @author Priyaa Parthasarathy
  * @version 1.0
  * @since 17.10.2013
  * @purpose This program is used call the database procedure to get the why study content page
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class WhyStudyGuideSP extends StoredProcedure {

  public WhyStudyGuideSP() {
  }

  public WhyStudyGuideSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_html_editor_pkg.get_subject_desc_prc");
    declareParameter(new SqlParameter("p_category_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
   // declareParameter(new SqlOutParameter("o_location", OracleTypes.CLOB));
    declareParameter(new SqlOutParameter("o_subject_desc", OracleTypes.CLOB));    
    declareParameter(new SqlOutParameter("oc_related_courses", OracleTypes.CURSOR, new TopCoursesRowMapperImpl()));
    //declareParameter(new SqlOutParameter("oc_loc_list", OracleTypes.CURSOR, new L2CategoryListRowMapperImpl())); 
   // declareParameter(new SqlOutParameter("oc_subject_list", OracleTypes.CURSOR, new L1subjectcategoryRowMapperImpl())); 
    declareParameter(new SqlOutParameter("p_level1_flag", OracleTypes.VARCHAR));// Modified for 10_DEC_2013_REL      
    declareParameter(new SqlOutParameter("p_cl_course_flag", OracleTypes.VARCHAR));// Modified for 24_JUN_2014_REL      
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(SearchBean searchbean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try{
    inMap.put("p_category_code", searchbean.getSubjectCode());
    inMap.put("p_user_id", searchbean.getUserId());
    inMap.put("p_basket_id", searchbean.getBasketId());
    outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }   
    return outMap;
  }

 public class L2CategoryListRowMapperImpl implements RowMapper {   
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
     CourseJourneyBean bean = new CourseJourneyBean();
     CommonFunction common = new CommonFunction();
     bean.setSubjectId(resultset.getString("course_id"));
     bean.setSubjectName(resultset.getString("course_name"));
     bean.setSeoSubjectName(common.replaceHypen(common.replaceURL(resultset.getString("course_name"))));
     bean.setCategoryCode(resultset.getString("category_code"));
     bean.setStudyLevelId(resultset.getString("study_level"));
     bean.setCategoryCodeLength(resultset.getString("cat_code_length"));
     bean.setParentCategoryId(resultset.getString("parent_category_code"));
     bean.setHyperLinkUrl(resultset.getString("browse_url"));
     return bean;
   }
 }
 
  public class L1subjectcategoryRowMapperImpl implements RowMapper {   
     public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
       SearchBean sbean = new SearchBean();
        sbean.setSubjectName(resultset.getString("description"));
        sbean.setHyperLinkUrl(resultset.getString("url"));   
        return sbean;
     }
   }
}
