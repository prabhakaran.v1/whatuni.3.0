package spring.sp.coursejourney;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.SearchBean;
import WUI.utilities.GlobalConstants;

/**
  * @JourneyHomeSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the browse journey page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class JourneyHomeSP extends StoredProcedure {
  public JourneyHomeSP() {
  }
  public JourneyHomeSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_COURSE_STUDY_LEVEL_FN");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_course_studylevel_list", OracleTypes.CURSOR, new browseStudyLevelMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    outMap = execute(inMap);
    return outMap;
  }

  /**
   * This class is used to load the browse sdtudylevels.
   */
  private class browseStudyLevelMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setOptionId(rs.getString("qualification_code"));
      bean.setOptionText(rs.getString("description"));
      bean.setOptionSeqId(rs.getString("seo_studylevel_text"));
      /* http://www.whatuni.com/degrees/courses/Degree-UK/qualification/M/list.html */
      String urlData = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + rs.getString("seo_studylevel_text") + "/qualification/" + rs.getString("qualification_code") + "/list.html";
      bean.setHyperLinkUrl(urlData);
      return bean;
    }
  }
}
