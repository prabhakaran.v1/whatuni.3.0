package spring.sp.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import WUI.basket.form.BasketBean;
import WUI.utilities.GlobalConstants;

/**
  * @ReviewHomeSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the review homepage.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class ReviewHomeSP extends StoredProcedure {

  public ReviewHomeSP() {
  }

  public ReviewHomeSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_REVIEW_HOME_DATA_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_latest_reviews", OracleTypes.CURSOR, new CommonMapper.latestReviewMapperImpl()));
    declareParameter(new SqlOutParameter("o_overall_stud_rating", OracleTypes.CURSOR, new overAllStudenRatingMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    outMap = execute(inMap);
    return outMap;
  }

  /**
    * This class is used to load the Over all rating information to display in review home page .
    */
  private class overAllStudenRatingMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      BasketBean bean = new BasketBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setOverAllRating(rs.getString("rating"));
      bean.setQuestionTitle(rs.getString("question_title"));
      bean.setQuestionId(rs.getString("review_question_id"));
      return bean;
    }

  }

}
