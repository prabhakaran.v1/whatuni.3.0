package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.SubjectProfileBean;
import WUI.utilities.GlobalConstants;

/**
  * @SubjectProfileListSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the subject profile list page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class SubjectProfileListSP extends StoredProcedure {
  public SubjectProfileListSP() {
  }
  public SubjectProfileListSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_SUBJECT_PROFILE_LIST_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_collegeid", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_subject_profile_list", OracleTypes.CURSOR, new subjectProfileListMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_collegeid", inputList.get(0));
    outMap = execute(inMap);
    return outMap;
  }

  /**
    * This class is used to load all the subject profile list for a given college.
    */
  private class subjectProfileListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SubjectProfileBean bean = new SubjectProfileBean();
      bean.setSubjectId(rs.getString("category_code"));
      bean.setSectionId(rs.getString("section_id"));
      bean.setCollegeId(rs.getString("college_id"));
      bean.setSubjectDesc(rs.getString("subject_title"));
      bean.setProfileId(rs.getString("profile_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setSubjectVideoUrl(rs.getString("sub_video_url"));
      return bean;
    }
  }
}
