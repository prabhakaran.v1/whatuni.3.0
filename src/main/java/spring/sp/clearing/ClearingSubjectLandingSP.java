 /**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ClearingSubjectLandingSP.java
 * Description   : Class used to call the stored procedure.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author                         Ver                     Modified On            Modification Details                   Change
 * *************************************************************************************************************************************
 * Sekhar Kasala                  1.0                     03.05.2012             First draft                            wu409_09052012     
 *
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

package spring.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.articles.ArticleVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.ClearingInfoVO;

public class ClearingSubjectLandingSP  extends StoredProcedure{
    public ClearingSubjectLandingSP(DataSource datasource){
        setDataSource(datasource);
        setSql("WU_CLEARING_PKG.clearing_subject_page_prc");
        declareParameter(new SqlParameter("p_subject_id", Types.VARCHAR));
        declareParameter(new SqlOutParameter("o_article_teaser", OracleTypes.CURSOR, new StudentArticleGroupRowMapper()));
        declareParameter(new SqlOutParameter("o_clearing_subjects", OracleTypes.CURSOR, new ClearingSubjects()));
        declareParameter(new SqlOutParameter("o_subj",OracleTypes.CURSOR, new ClearingSubjectLanding()));
        
    }
    public Map execute(List parameters){
        Map outMap = null;
        Map inMap = new HashMap();
        inMap.put("p_subject_id", parameters.get(0));
        outMap = execute(inMap);
        if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
            new AdminUtilities().logDbCallParameterDetails("wu_articles_pkg.clearing_subject_page_prc", inMap);
        }
        return outMap;
    }
    // This is used for the atilce details in clearing hoem page.
    private class StudentArticleGroupRowMapper implements RowMapper {

      public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        ArticleVO articleVO = new ArticleVO();
        articleVO.setArticleGroupId(rs.getString("article_group_id"));
        articleVO.setArticleGroupUrlSeoName(rs.getString("article_group_url_seo_name"));
        articleVO.setArticleId(rs.getString("article_id"));
        articleVO.setArticleHeading(rs.getString("article_heading"));
        articleVO.setArticleUrlSeoTitle(rs.getString("article_url_seo_title"));
        articleVO.setArticleUpdatedDate(rs.getString("article_updated_date"));
        articleVO.setAuthorName(rs.getString("author_name"));
        articleVO.setArticleGroupDescription(rs.getString("article_short_desc"));
        articleVO.setArticleMediaPath(rs.getString("article_media_path"));
        articleVO.setArticleMediaThumbPath(rs.getString("article_thumb_path"));
        articleVO.setArticleMediaThumbPath0001(rs.getString("article_medium_path"));
        articleVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(rs.getString("article_url_seo_title"),new CommonFunction().replaceSpecialCharacter(articleVO.getArticleGroupUrlSeoName()).toLowerCase()));
        articleVO.setArticleGroupHtmlName(rs.getString("article_group_html"));
        return articleVO;
      }
    }
    //This is used to display the right side in the clearing home page.
    private class ClearingSubjects implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
            ClearingInfoVO clearingVO = new ClearingInfoVO();
            clearingVO.setSubjectName(rs.getString("subject_name"));
            clearingVO.setCategoryId(rs.getString("category_id"));
            if(rs.getString("subject_name") != null){
                clearingVO.setSubjectUrl(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(rs.getString("subject_name"))).toLowerCase());
            }else{
                clearingVO.setSubjectUrl("");
            }
            return clearingVO;
        }
    }
    private class ClearingSubjectLanding implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
            ClearingInfoVO clearingVO = new ClearingInfoVO();
            clearingVO.setSubjectName(rs.getString("subject_name"));
            clearingVO.setSubjectText(rs.getString("subject_text"));
            return clearingVO;
        }
    }

}
