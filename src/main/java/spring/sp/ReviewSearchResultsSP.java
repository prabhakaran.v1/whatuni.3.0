package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.review.bean.ReviewListBean;
import WUI.utilities.GlobalConstants;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.UniReviewResultsRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.ReviewListVO;

/**
  * @ReviewSearchResultsSP
  * @author Amirtharaj
  * @version 1.0
  * @since 3.02.2015
  * @purpose This program is used call the database procedure to build search result reviews/university reviews page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
 	* 21.12.2015     Prabhakaran V.            wu_548   Commented replacing '&' to 'and' in category title
  *                                                   Added live rating details cursor                            27_JAN_2016
  * 7.12.2018      Sangeeth.S                wu_584   Added common phrases and removed unused cursor for review
  *                                                                                                 redesign      19_DEC_2018
  */
public class ReviewSearchResultsSP extends StoredProcedure {
  public ReviewSearchResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.GET_REVIEW_RESULT_PRC);
    declareParameter(new SqlParameter("P_AFFILIATE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGEID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SHOW_RECORD", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ORDER_BY", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUBJECT_CODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SEARCH_TEXT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REVIEW_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_STUDY_LEVEL", Types.VARCHAR));
    declareParameter(new SqlOutParameter("oc_uni_reviews", OracleTypes.CURSOR, new UniReviewResultsRowMapperImpl()));    
    declareParameter(new SqlOutParameter("oc_uni_info", OracleTypes.CURSOR, new UniInfoRowMapperImpl()));    
    declareParameter(new SqlOutParameter("o_bread_crumb", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_course_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_college_in_basket", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_subject_name", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_uni_overall_winner_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_final_choices_exist_flag", OracleTypes.VARCHAR));//Added fby Priyaa for final 5 - 21-Jul-2015    
    declareParameter(new SqlOutParameter("PC_COMMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
  }
  public Map execute(ReviewListVO reviewListVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_AFFILIATE_ID", GlobalConstants.WHATUNI_AFFILATE_ID);
      inMap.put("P_COLLEGEID", reviewListVO.getCollegeId());
      inMap.put("P_PAGE_NO", reviewListVO.getPageNo());
      inMap.put("P_SHOW_RECORD", reviewListVO.getShowRecordCount());
      inMap.put("P_ORDER_BY", reviewListVO.getOrderBy());
      inMap.put("P_SUBJECT_CODE", reviewListVO.getSubjectCode());
      inMap.put("P_SEARCH_TEXT", reviewListVO.getSearchText());
      inMap.put("P_USER_ID", reviewListVO.getUserId());
      inMap.put("P_BASKET_ID", reviewListVO.getBasketId());
      inMap.put("P_REVIEW_ID", reviewListVO.getReviewId());    
      inMap.put("P_STUDY_LEVEL", reviewListVO.getStudyLevel());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class UniInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewBean = new ReviewListBean();
      reviewBean.setCollegeId(rs.getString("college_id"));
      reviewBean.setCollegeName(rs.getString("college_name"));
      reviewBean.setCollegeDisplayName(rs.getString("college_name_display"));
      reviewBean.setReviewCount(rs.getString("review_count"));
      reviewBean.setOverAllRating(rs.getString("overall_rating"));
      reviewBean.setOverallRatingExact(rs.getString("overall_rating_exact"));      
      reviewBean.setSponsorURL(new SeoUrls().construnctUniHomeURL(reviewBean.getCollegeId(), reviewBean.getCollegeName()));
      return reviewBean;
    }
  }
			
}
