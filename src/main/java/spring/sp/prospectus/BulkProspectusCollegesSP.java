package spring.sp.prospectus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.prospectus.BulkProspectusVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

/**
    * @author Latha
    * @version 1.0
    * @since 2010-Aug-10
    * This will return College List to get bulk prospectus
    *
    * previously WU_bulk prospectus colleges was bulit based on the following steps
    * 1) call to "hot_wuni.whatuni_search.adv_col_do " will return headerId
    * 2) using that headerId call another DB "Wu_prospectus_pkg.show_prospectus_col", this will retun college list for bulk prospectus
    *
    * now we are going to get these main details from one DB-call
    *
    * Change Log
    * *************************************************************************************************************************
    * Date           Name                      Ver.         Changes desc                                              Rel Ver.
    * *************************************************************************************************************************
    * 2010-Aug-10   Latha                      1.0         Initial draft                                               WU_2.0.9
    *
    */
public class BulkProspectusCollegesSP extends StoredProcedure {
    public BulkProspectusCollegesSP() { }
    
    public BulkProspectusCollegesSP(DataSource datasource) {
      setDataSource(datasource);
      setSql("WHATUNI_SEARCH3.GET_BULK_PROSPECTUS_COLLEGES");    
      declareParameter(new SqlParameter("x", Types.VARCHAR));
      declareParameter(new SqlParameter("y", Types.VARCHAR));
      declareParameter(new SqlParameter("search_what", Types.VARCHAR));
      declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
      declareParameter(new SqlParameter("college_name", Types.VARCHAR));    
      declareParameter(new SqlParameter("qualification", Types.VARCHAR));
      declareParameter(new SqlParameter("country", Types.VARCHAR));
      declareParameter(new SqlParameter("town_city", Types.VARCHAR));
      declareParameter(new SqlParameter("postcode", Types.VARCHAR));
      declareParameter(new SqlParameter("search_range", Types.VARCHAR));
      declareParameter(new SqlParameter("postflag", Types.VARCHAR));
      declareParameter(new SqlParameter("location_id", Types.VARCHAR));
      declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
      declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
      declareParameter(new SqlParameter("lucky", Types.VARCHAR));
      declareParameter(new SqlParameter("crs_search", Types.VARCHAR));
      declareParameter(new SqlParameter("submit", Types.VARCHAR));
      declareParameter(new SqlParameter("a", Types.VARCHAR));
      declareParameter(new SqlParameter("search_how", Types.VARCHAR));
      declareParameter(new SqlParameter("s_type", Types.VARCHAR));
      declareParameter(new SqlParameter("search_category", Types.VARCHAR));
      declareParameter(new SqlParameter("search_career", Types.VARCHAR));
      declareParameter(new SqlParameter("career_id", Types.VARCHAR));
      declareParameter(new SqlParameter("ref_id", Types.VARCHAR));
      declareParameter(new SqlParameter("nrp", Types.VARCHAR));
      declareParameter(new SqlParameter("area", Types.VARCHAR));
      declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
      declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
      declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
      declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
      declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
      declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
      declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
      declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
      declareParameter(new SqlParameter("p_deemed_uni", Types.VARCHAR));
      declareParameter(new SqlParameter("p_search_action", Types.VARCHAR));
      declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
      declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
      declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
      declareParameter(new SqlOutParameter("o_search_results", OracleTypes.CURSOR, new SearchResultsRowMapper()));
      declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
      declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    }

    public Map execute(List inputList) {     
      Map outMap = null; 
      Map inMap = new HashMap(); 
      
      try{              
        inMap.put("x", inputList.get(0)); 
        inMap.put("y", inputList.get(1)); 
        inMap.put("search_what", inputList.get(2));
        inMap.put("phrase_search", inputList.get(3));
        inMap.put("college_name", inputList.get(4));    
        inMap.put("qualification", inputList.get(5));
        inMap.put("country", inputList.get(6));
        inMap.put("town_city", inputList.get(7));
        inMap.put("postcode", inputList.get(8));
        inMap.put("search_range", inputList.get(9));
        inMap.put("postflag", inputList.get(10));
        inMap.put("location_id", inputList.get(11));
        inMap.put("study_mode", inputList.get(12));
        inMap.put("course_duration", inputList.get(13));
        inMap.put("lucky", inputList.get(14));
        inMap.put("crs_search", inputList.get(15));
        inMap.put("submit", inputList.get(16));
        inMap.put("a", inputList.get(17));
        inMap.put("search_how", inputList.get(18));
        inMap.put("s_type", inputList.get(19));
        inMap.put("search_category", inputList.get(20));
        inMap.put("search_career", inputList.get(21));
        inMap.put("career_id", inputList.get(22));
        inMap.put("ref_id", inputList.get(23));
        inMap.put("nrp", inputList.get(24));
        inMap.put("area", inputList.get(25));
        inMap.put("p_pg_type", inputList.get(26));
        inMap.put("p_search_col", inputList.get(27));
        inMap.put("p_county_id", inputList.get(28));
        inMap.put("p_entity_text", inputList.get(29));
        inMap.put("p_search_where", inputList.get(30));
        inMap.put("p_user_agent", inputList.get(31));
        inMap.put("p_search_browse_link", inputList.get(32));
        inMap.put("p_ucas_code", inputList.get(33));
        inMap.put("p_deemed_uni", inputList.get(34));
        inMap.put("p_search_action", inputList.get(35));
        inMap.put("p_page_no", inputList.get(36));
        inMap.put("p_basket_id", inputList.get(37));
        inMap.put("p_search_cat_id", inputList.get(38));
        outMap = execute(inMap);
      }catch(Exception e){
        e.printStackTrace();    
      }
      if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
        new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_BULK_PROSPECTUS_COLLEGES", inMap);
      }
      return outMap;
    }     
    
    /**
    * this row mapper used to map mid content detalis
    */
  private class SearchResultsRowMapper implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      BulkProspectusVO bulkprospectusVO = new BulkProspectusVO();
      bulkprospectusVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      bulkprospectusVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      bulkprospectusVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      bulkprospectusVO.setWebsitePurchaseFlag(resultset.getString("WEBSITE_PUR_FLAG"));
      bulkprospectusVO.setProspectusPurchaseFlag("TRUE");

      //enquiryDetails related properties
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
      try{
        if(enquiryDetailsRS != null){
          while(enquiryDetailsRS.next()){
            bulkprospectusVO.setSubOrderWebsite(enquiryDetailsRS.getString("WEBSITE"));
            bulkprospectusVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            bulkprospectusVO.setSubOrderEmailWebform(enquiryDetailsRS.getString("EMAIL_WEBFORM"));
            bulkprospectusVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            bulkprospectusVO.setSubOrderProspectusWebform(enquiryDetailsRS.getString("PROSPECTUS_WEBFORM"));
            bulkprospectusVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            bulkprospectusVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));
            bulkprospectusVO.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
            bulkprospectusVO.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));            
          }
        }        
      } catch(Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if(enquiryDetailsRS != null){
            enquiryDetailsRS.close();
          }
        } catch(Exception e) {
          throw new SQLException(e.getMessage());
        }  
      }
      
      return bulkprospectusVO;    
    }
  }

  }


