package spring.sp.browsebylocation;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.articles.StudyLevelArticlesRowMapper;
import spring.rowmapper.browsebylocation.BrowseUgCoursesByLocationRowMapperImpl;
import spring.rowmapper.browsebylocation.BrowseUgCoursesBySubjectRowMapperImpl;
import spring.rowmapper.common.RandomSubjectListRowMapperImpl;
import spring.rowmapper.common.StaticSubjectListRowMapperImpl;
import spring.rowmapper.common.StudentChoiceAwardsRowMapperImpl;
import spring.rowmapper.common.SubjectCategoriesRowMapperImpl;
import spring.rowmapper.common.TickerTapeRowMapperImpl;
import spring.rowmapper.common.UserCompareBasketRowMapperImpl;
import spring.rowmapper.common.UserInfoRowMapperImpl;
import spring.rowmapper.common.UserSearchBasketRowMapperImpl;
import spring.rowmapper.seo.PageMetaTagsRowMapperImpl;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

public class BrowseUgCoursesBySubjectSP extends StoredProcedure {

  public BrowseUgCoursesBySubjectSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setSql("WU_BROWSE_BY_LOCATION_PKG.GET_BROWSE_BY_SUBJECT_PRC");
    //
    declareParameter(new SqlParameter("P_LOCATION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_QUAL_CODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.VARCHAR));
    declareParameter(new SqlParameter("P_RECORD_COUNT", Types.VARCHAR));
    //
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_FLAG", Types.VARCHAR));
    //
    declareParameter(new SqlOutParameter("O_SUBJECT_LIST", OracleTypes.CURSOR, new BrowseUgCoursesBySubjectRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_STUDY_LEVEL_DESC", Types.VARCHAR));
    declareParameter(new SqlOutParameter("O_RANDOM_SUBJECT_LIST", OracleTypes.CURSOR, new RandomSubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_STATIC_SUBJECT_LIST", OracleTypes.CURSOR, new StaticSubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_TICKER_TAPE", OracleTypes.CURSOR, new TickerTapeRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_USER_COMPARE_BASKET", OracleTypes.CURSOR, new UserCompareBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_USER_SEARCH_BASKET", OracleTypes.CURSOR, new UserSearchBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_USER_INFO", OracleTypes.CURSOR, new UserInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_SUBJECT_CATEGORIES", OracleTypes.CURSOR, new SubjectCategoriesRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_STUDENT_CHOICE_AWARDS", OracleTypes.CURSOR, new StudentChoiceAwardsRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_PAGE_META_TAGS", OracleTypes.CURSOR, new PageMetaTagsRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_RELATED_LOCATIONS", OracleTypes.CURSOR, new BrowseUgCoursesByLocationRowMapperImpl()));
    //
    declareParameter(new SqlOutParameter("O_LOCATION_BASED_ARTICLES", OracleTypes.CURSOR, new StudyLevelArticlesRowMapper()));//wu314_20110712_Syed
    //
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_LOCATION", inputList.get(0));
    inMap.put("P_QUAL_CODE", inputList.get(1));
    inMap.put("P_PAGE_NO", inputList.get(2));
    inMap.put("P_RECORD_COUNT", inputList.get(3));
    inMap.put("P_USER_ID", inputList.get(4));
    inMap.put("P_BASKET_ID", inputList.get(5));
    inMap.put("P_META_PAGE_NAME", inputList.get(6));
    inMap.put("P_META_PAGE_FLAG", inputList.get(7));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WU_BROWSE_BY_LOCATION_PKG.GET_BROWSE_BY_SUBJECT_PRC", inMap);
    }
    return outMap;
  }

}//end of class
