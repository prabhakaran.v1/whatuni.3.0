package spring.sp.blogs;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.blogs.BestVideoReviewRowMapper;
import spring.rowmapper.blogs.LatestPostListRowMapper;
import spring.rowmapper.blogs.PostDetailsRowMapper;
import spring.rowmapper.blogs.TickerTapeVideoRowMapper;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

/**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Nov-04
   *
   * This will return post home pages all details at one shot
   *
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   * 2010-Aug-04   Syed                        1.0     Initial draft                                                WU_2.0.9
   *
   */
public class PostHomeInfoSP extends StoredProcedure {

  public PostHomeInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_BLOGS_PKG.GET_POST_HOME_INFO_PRC");
    declareParameter(new SqlParameter("post_url_seo_title", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_post_home_info", OracleTypes.CURSOR, new PostDetailsRowMapper())); //used in mid content  
    declareParameter(new SqlOutParameter("o_latest_post_list", OracleTypes.CURSOR, new LatestPostListRowMapper())); //used in left-side-latest-pod    
    declareParameter(new SqlOutParameter("o_editors_rambling_post_list", OracleTypes.CURSOR, new LatestPostListRowMapper())); //used in left-side-rambling-pod  
    declareParameter(new SqlOutParameter("o_best_video", OracleTypes.CURSOR, new BestVideoReviewRowMapper()));
    declareParameter(new SqlOutParameter("o_get_video_ticker", OracleTypes.CURSOR, new TickerTapeVideoRowMapper()));
    //    declareParameter(new SqlOutParameter("o_blog_specific_posts_list", OracleTypes.CURSOR, new LatestPostListRowMapper())); //used in left-side-rambling-pod  
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("post_url_seo_title", inputList.get(0));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WU_BLOGS_PKG.GET_POST_HOME_INFO_PRC", inMap);
    }
    return outMap;
  }

}//end of class