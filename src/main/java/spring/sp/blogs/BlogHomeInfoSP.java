package spring.sp.blogs;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.blogs.BestVideoReviewRowMapper;
import spring.rowmapper.blogs.LatestPostListRowMapper;
import spring.rowmapper.blogs.PostDetailsRowMapper;
import spring.rowmapper.blogs.PremiumPostListRowMapper;
import spring.rowmapper.blogs.TickerTapeVideoRowMapper;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

/**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Nov-04
   *
   * This will return blog home pages all details at one shot
   *
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   * 2010-Aug-04   Mohamed Syed               1.0      Initial draft                                                WU_2.1.5
   *
   */
public class BlogHomeInfoSP extends StoredProcedure {

  public BlogHomeInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_BLOGS_PKG.GET_BLOG_HOME_INFO_PRC");
    declareParameter(new SqlParameter("p_website_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_premium_post_list", OracleTypes.CURSOR, new PremiumPostListRowMapper())); //used in mid_content_premium post                                              
    declareParameter(new SqlOutParameter("o_latest_post_list", OracleTypes.CURSOR, new LatestPostListRowMapper())); //used in left-side-latest-pod    
    declareParameter(new SqlOutParameter("o_editors_rambling_post_list", OracleTypes.CURSOR, new LatestPostListRowMapper())); //used in left-side-rambling-pod    
    declareParameter(new SqlOutParameter("o_editors_rambling_post", OracleTypes.CURSOR, new PostDetailsRowMapper())); //used in middle content rambling-post
    declareParameter(new SqlOutParameter("o_best_video", OracleTypes.CURSOR, new BestVideoReviewRowMapper())); //featured video
    declareParameter(new SqlOutParameter("o_get_video_ticker", OracleTypes.CURSOR, new TickerTapeVideoRowMapper())); // video ticker  
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_website_id", inputList.get(0));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WU_BLOGS_PKG.GET_BLOG_HOME_INFO_PRC", inMap);
    }
    return outMap;
  }

}//end of class