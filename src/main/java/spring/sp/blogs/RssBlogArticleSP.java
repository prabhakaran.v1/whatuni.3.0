package spring.sp.blogs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.articles.RssBlogArticleVO;
import WUI.utilities.CommonFunction;

public class RssBlogArticleSP extends StoredProcedure {
  public RssBlogArticleSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql("HOT_WUNI.WU_RSS_XML_PRC ");
    declareParameter(new SqlOutParameter("O_GET_RSS_XML", OracleTypes.CURSOR, new RssArticleBlogRowMapperImpl()));
    compile();
  }
  public Map execute() {
    Map outMap = null;
    try {
      Map inMap = new HashMap();      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class RssArticleBlogRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RssBlogArticleVO rssBlogArticleVO = new RssBlogArticleVO();     
      rssBlogArticleVO.setChannel(resultSet.getString("blog_name"));
      rssBlogArticleVO.setRssXMLData(CommonFunction.getClobValue(resultSet.getClob("rss_xml")));
      return rssBlogArticleVO;
    }
  }
}
