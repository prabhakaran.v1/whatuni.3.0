package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.search.form.SearchBean;
import WUI.utilities.CommonFunction;

/**
  * @JourneyHomeSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the browse journey page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class CollegeBrowseSubjectListSP extends StoredProcedure {
  public CollegeBrowseSubjectListSP() {
  }
  public CollegeBrowseSubjectListSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_COLLEGE_SUBJECT_LIST_PRC");
    declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_qual_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_disp_cnt", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_subject_list", OracleTypes.CURSOR, new collegeSubjectListMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_college_id", inputList.get(0));
    inMap.put("p_qual_code", inputList.get(1));
    inMap.put("p_page_no", inputList.get(2));
    inMap.put("p_disp_cnt", inputList.get(3));
    outMap = execute(inMap);
    
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_COLLEGE_SUBJECT_LIST_PRC", inMap);  
    }
    
    return outMap;
  }

  /**
   * This class is used to load the browse subject list for the college.
   * http://www.whatuni.com/degrees/courses/access-foundation-university/accountancy-courses-at-newcastle-college/ak./t/0/united+kingdom/7934/csearch.html
   */
  private class collegeSubjectListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setSubjectId(rs.getString("category_code"));
      bean.setSubjectName(rs.getString("category_desc"));
      bean.setSubjectNameWithoutHypen(new CommonFunction().replaceHypenWithSpace(rs.getString("category_desc")));  
      bean.setStudyLevelId(rs.getString("qualification_code"));
      bean.setSeoStudyLevel(rs.getString("study_level_desc"));
      bean.setSubjectCount(rs.getString("subject_count"));
      return bean;
    }
  }
}
