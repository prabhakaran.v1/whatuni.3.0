package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.opendays.OpenDayCalendarRowMapperImpl;
import spring.valueobject.opendays.OpenDaysCalendarVO;
import WUI.utilities.GlobalConstants;

/**
  * @OpenDaysBrowseSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the open days browse page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class OpenDaysBrowseSP extends StoredProcedure {
  public OpenDaysBrowseSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_OPENDAYS_BROWSE_DATA_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_character", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    //declareParameter(new SqlOutParameter("o_openday_browse_list", OracleTypes.CURSOR, new OpenDaysBrowseListRowMapperImpl()));
    //declareParameter(new SqlOutParameter("o_atoz_link", Types.VARCHAR)); 
    declareParameter(new SqlOutParameter("o_openday_calendar", OracleTypes.CURSOR, new OpenDayCalendarRowMapperImpl())); 
    //declareParameter(new SqlOutParameter("O_OPENDAY_RIGHT_POD", OracleTypes.CURSOR, new OpenDayRightPodRowMapperImpl())); 
    declareParameter(new SqlOutParameter("O_AVAILABLE_OPENDAYS", OracleTypes.CURSOR, new availableOpenDays()));

        
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_search_character", inputList.get(0)); //college-id  
    inMap.put("p_network_id", inputList.get(1)); //college-id  
    inMap.put("p_user_id", inputList.get(2));
    outMap = execute(inMap);
    return outMap;
  }
  /**
   * This row mapper class is used for the right side pod of open day page.
   * Added by Sekhar K on wu_320
   */
  private class OpenDayRightPodRowMapperImpl implements RowMapper{
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      
      OpenDaysCalendarVO openDaysCalendarVO = new OpenDaysCalendarVO();
      openDaysCalendarVO.setCollegeId(rs.getString("college_id"));
      openDaysCalendarVO.setCollegeName(rs.getString("college_name"));
      openDaysCalendarVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      openDaysCalendarVO.setOpenDayCount(rs.getString("openday_count"));
      openDaysCalendarVO.setCollegeLocation(rs.getString("college_location"));
      return openDaysCalendarVO;
  }
 }
 //wu412_20120626 Added by Sekhar for highliting the available openday in calendar.
 private class availableOpenDays implements RowMapper{
     public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
         OpenDaysCalendarVO openDaysCalendarVO = new OpenDaysCalendarVO();
         openDaysCalendarVO.setOpenDate_day(rs.getString("open_date"));
         openDaysCalendarVO.setPastFutureDate(rs.getString("past_future_date"));
         return openDaysCalendarVO;
     }
 }
}
