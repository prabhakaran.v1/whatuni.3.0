package spring.sp.members;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.members.form.MembersSearchBean;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

/**
  * @ViewAllMembersSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the view all members page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class ViewAllMembersSP extends StoredProcedure {
  public ViewAllMembersSP() {
  }
  public ViewAllMembersSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_VIEW_ALL_MEMBERS_LIST_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_order_by", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_type", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_latest_members_list", OracleTypes.CURSOR, new latestMembersMapperImpl()));
    declareParameter(new SqlOutParameter("o_latest_members_cnt", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_user_type_list", OracleTypes.CURSOR, new userTypeListMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_page_no", inputList.get(0));
    inMap.put("p_order_by", inputList.get(1));
    inMap.put("p_user_type", inputList.get(2));
    outMap = execute(inMap);
    return outMap;
  }

  /**
    *  This class is used to load the latest members details.
    */
  private class latestMembersMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MembersSearchBean bean = new MembersSearchBean();
      bean.setUserName(rs.getString("username"));
      bean.setForeName(rs.getString("forename"));
      bean.setGender(rs.getString("gender"));
      bean.setUserId(rs.getString("user_id"));
      bean.setNationality(rs.getString("nationality"));
      bean.setYearOfGraduation(rs.getString("year_of_graduation"));
      bean.setIama(rs.getString("graduation"));
      //bean.setUserImage(rs.getString("user_photo"));
      bean.setSubjectLinkList(rs.getString("subject_basket_link"));
      //NEED TODO - need to remove for Ram's reference
      /*String userImageName = rs.getString("user_photo");
      String userThumbName = (userImageName != null? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()): userImageName);
      bean.setUserImage(userThumbName);
      bean.setUserLargeImage(userImageName);*/
      return bean;
    }
  }

  /**
    * This class is used to load the user type list.
    */
  private class userTypeListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MembersSearchBean bean = new MembersSearchBean();
      bean.setOptionId(rs.getString("Value"));
      String description = rs.getString("description");
      description = description != null? description.trim().concat("s"): description;
      bean.setOptionText(new CommonUtil().toTitleCase(description));
      return bean;
    }
  }
}
