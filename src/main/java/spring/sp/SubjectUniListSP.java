package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.search.form.SubjectProfileBean;
import WUI.utilities.GlobalConstants;

/**
  * @SubjectUniListSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the subject unilist page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class SubjectUniListSP extends StoredProcedure {
  public SubjectUniListSP() {
  }
  public SubjectUniListSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_SUBJECT_UNI_LIST_PROC");
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_subject_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_subject_uni_list", OracleTypes.CURSOR, new subjectUniversityListMapperImpl()));
  }
  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("a", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_subject_id", inputList.get(0));
    outMap = execute(inMap);
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_SUBJECT_UNI_LIST_PROC", inMap);
    }    
    return outMap;
  }
  /**
    * This class is used to load the subject based university list.
    */
  private class subjectUniversityListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SubjectProfileBean bean = new SubjectProfileBean();
      bean.setSubjectId(rs.getString("category_code"));
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setSubjectDesc(rs.getString("subject_name"));
      return bean;
    }
  }
}
