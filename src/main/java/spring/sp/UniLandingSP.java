package spring.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import WUI.admin.bean.AdminOpenDaysBean;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.videoreview.bean.VideoReviewListBean;

/**
  * @UniLandingSP
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used call the database procedure to build the unilanding page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class UniLandingSP extends StoredProcedure {
  public UniLandingSP() {
  }
  public UniLandingSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_UNI_LANDING_DATA_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_collegeid", Types.VARCHAR));
    declareParameter(new SqlParameter("p_iam_question", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_related_members", OracleTypes.CURSOR, new CommonMapper.collegeRelatedMembersMapperImpl()));
    declareParameter(new SqlOutParameter("o_latest_uni_reviews", OracleTypes.CURSOR, new CommonMapper.collegeLatestReviewsMapperImpl()));
    declareParameter(new SqlOutParameter("o_latest_uni_video", OracleTypes.CURSOR, new collegeLatestVideoMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_overall_rating", OracleTypes.CURSOR, new collegeOverallRatingMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_mode_list", OracleTypes.CURSOR, new studyModeListMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_level_list", OracleTypes.CURSOR, new studyLevelListMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_rand_cat_list", OracleTypes.CURSOR, new uniRandomizeCategoryMapperImpl()));
    declareParameter(new SqlOutParameter("o_google_key", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_get_unihome_image", OracleTypes.CURSOR, new imageDetailsImpl()));
    declareParameter(new SqlOutParameter("o_unistats_info", OracleTypes.CURSOR, new unistatsCollegeImpl()));
    declareParameter(new SqlOutParameter("o_unilanding_video", OracleTypes.CURSOR, new unilandMapperImpl()));
    declareParameter(new SqlOutParameter("o_overview_flag", Types.VARCHAR));//CODE-AUDIT_Syed_20110427: not used any more, can be removed
    declareParameter(new SqlOutParameter("p_qual_network_id", Types.VARCHAR));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    inMap.put("p_collegeid", inputList.get(0)); //college-id  
    inMap.put("p_iam_question", inputList.get(1)); //iama
    inMap.put("p_page_no", inputList.get(2)); //page-no
    inMap.put("p_record_count", inputList.get(3)); //record-count    
    inMap.put("p_qual_network_id", inputList.get(4)); //qualification network id    
    outMap = execute(inMap); 
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WUNI_SPRING_PKG.GET_UNI_LANDING_DATA_PROC", inMap);
    }
    return outMap;
  }

  /**
    *  This class is used to load the latest college video.
    */
  private class collegeLatestVideoMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
      String videoFormat = GlobalConstants.LIMELIGHT_VIDEO_FILE_EXT;
      VideoReviewListBean bean = new VideoReviewListBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setUserId(rs.getString("user_id"));
      bean.setUserName(rs.getString("user_name"));
      bean.setVideoReviewId(rs.getString("review_id"));
      bean.setVideoReviewTitle(rs.getString("review_title"));
      bean.setVideoReviewDesc(rs.getString("review_desc"));
      bean.setVideoCategory(rs.getString("v_category"));
      bean.setNoOfHits(rs.getString("no_hit"));
      String length = rs.getString("pic_length");
      length = length != null && length.trim().length() > 0? length: "";
      bean.setVideoLength(length);
      String videoUrl = rs.getString("youtube_video_assoc_text");
      if (videoUrl != null && !videoUrl.equalsIgnoreCase("null") && videoUrl.trim().length() > 0) {
        bean.setVideoType("YOUTUBE");
        bean.setVideoUrl(videoUrl);
        bean.setThumbnailUrl(rs.getString("thumbnail_assoc_text"));
      } else {
        if (rs.getString("limelight_video_assoc_text") != null && rs.getString("limelight_video_assoc_text").trim().length() > 0) {
          videoUrl = rs.getString("limelight_video_assoc_text");
          bean.setVideoUrl(limeLightPath + videoUrl + videoFormat);
          bean.setThumbnailUrl(limeLightPath + videoUrl + "_tmb/0000.jpg");
          bean.setVideoType("LIMELIGHT");
        }
      }
      bean.setOverallRating(rs.getString("overall_rating"));
      return bean;
    }
  }

  /**
    *  This class is used to load the overall rating information.
    */
  private class collegeOverallRatingMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean bean = new ReviewListBean();
      bean.setReviewQuestion(rs.getString("question_title"));
      bean.setOverallRating(rs.getString("rating"));
      bean.setReviewQuestionId(rs.getString("question_id"));
      bean.setCollegeId(rs.getString("college_id"));
      bean.setMaxRating("10");
      String questionTitle = rs.getString("question_title");
      String seoText = new CommonFunction().seoQuestionTitle(questionTitle);
      bean.setSeoQuestionText(seoText);
      return bean;
    }
  }

  /**
    *  This class is used to load the studymode list.
    */
  private class studyModeListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setStudyModeId(rs.getString("stdymde_opt_id"));
      bean.setStudyModeDesc(rs.getString("stdymde_webdesc"));
      bean.setStudyModeValue(rs.getString("stdymde_value"));
      return bean;
    }
  }

  /**
    *  This class is used to load the study level list.
    */
  private class studyLevelListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setOptionId(rs.getString("qual_opt_id"));
      bean.setOptionText(rs.getString("display_name"));
      return bean;
    }
  }

  /**
    *  This class is used to load the random category uni list.
    */
  private class uniRandomizeCategoryMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setSubjectId(rs.getString("category_code"));
      bean.setSeoStudyLevel(rs.getString("seo_qual_desc"));
      bean.setStudyLevelId(rs.getString("qual_code"));
      String categoryName = rs.getString("category_desc");
      if (categoryName != null && !categoryName.equals("null") && categoryName.trim().length() > 0 && categoryName.indexOf("#") >= 0) {
        categoryName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(categoryName.replaceAll("#", "")));
      } else {
        categoryName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("category_desc")));
      }
      bean.setSubjectName(rs.getString("category_desc"));
      String url = "/courses/" + rs.getString("seo_qual_desc");
      url = url + "/" + categoryName + "-courses-at-" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(rs.getString("college_name"))));
      url = url + "/" + rs.getString("category_code");
      url = url + "/" + rs.getString("qual_code");
      url = url + "/0";
      url = url + "/" + new CommonFunction().replaceSpacePlus(rs.getString("location"));
      url = url + "/" + rs.getString("college_id");
      url = url + "/csearch.html";
      url = new CommonUtil().toLowerCase(url);
      bean.setHyperLinkUrl(url);
      return bean;
    }
  }

  /**
    *  This class is used to load the uni landing data.
    */
  private class unilandMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AdminOpenDaysBean bean = new AdminOpenDaysBean();
      bean.setType(rs.getString("type"));
      bean.setVideoReviewId(rs.getString("review_id"));
      bean.setCollegeId(rs.getString("college_id"));
      bean.setShortNotes(rs.getString("title"));
      
      if (rs.getString("type") != null && (rs.getString("type").equalsIgnoreCase("street") || rs.getString("type").equalsIgnoreCase("google_map"))) {
        String latLangValue = rs.getString("logo_path");
        if (latLangValue != null && latLangValue.indexOf(",") > 0) {
          String mapdata[] =  latLangValue.split(",");
          bean.setLatitudeValue(mapdata[0]);
          bean.setLongtitudeValue(mapdata[1]);
        }else{
          bean.setPath("/degrees/img/whatuni/unihome_default.png");
          bean.setType("default");
        }
      } else if (rs.getString("type") != null && rs.getString("type").equalsIgnoreCase("video")) {        
        String tmp = rs.getString("logo_path");
        if(tmp != null && tmp.trim().length() > 0){
          String[] tmpArr = tmp.split("###");        
          if(bean.getVideoReviewId() != null && bean.getVideoReviewId().trim().length() > 0) { //CONDITION FOR VIDEO_REVIEWs
            bean.setWhichVideoType("");//no value should not passed to this type of video
            bean.setPath(GlobalConstants.LIMELIGHT_VIDEO_PATH + "college/video/" + rs.getString("college_id") + GlobalConstants.LIMELIGHT_VIDEO_FILE_EXT);                       
            bean.setVideoThumbUrl(GlobalConstants.LIMELIGHT_VIDEO_PATH + "college/video/" + rs.getString("college_id") + "_tmb/0002.jpg");
          } else {//CONDITION FOR IP/SP videos
            bean.setWhichVideoType("B");//always be 'B' for this kind of videos
            bean.setVideoReviewId(tmpArr[0]);  
            bean.setPath(tmpArr[1]);
            bean.setVideoThumbUrl(new GlobalFunction().videoThumbnailFormatChange(tmpArr[1], "4"));
          }
        } else {
          bean.setType("picture");
          bean.setPath("/degrees/img/whatuni/unihome_default.png");
        }
      } else if (rs.getString("type") != null && rs.getString("type").equalsIgnoreCase("picture")) {
        String tmp = rs.getString("logo_path");
        if(tmp != null && tmp.indexOf(GlobalConstants.WHATUNI_SCHEME_NAME) > -1 ){//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
          bean.setPath(tmp);
        }else {
          bean.setPath(new CommonFunction().getWUSysVarValue("IMAGE_DISPLAY_PATH") + "college/image/" + rs.getString("college_id") + "/" + tmp);
        }
      } else {
        bean.setPath("/degrees/img/whatuni/unihome_default.png");
      }
     
      return bean;
    }
  }

  /**
    *  This class is used to load the college image details.
    */
  private class imageDetailsImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchResultBean bean = new SearchResultBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeLogo(rs.getString("college_logo_path"));
      bean.setImageName(rs.getString("image_name"));
      bean.setAuthor(rs.getString("author"));
      String imgShortName = rs.getString("image_name");
      imgShortName = imgShortName != null && imgShortName.trim().length() >= 25? imgShortName.substring(0, 17) + "...": imgShortName;
      bean.setImageShortName(imgShortName);
      if (rs.getString("college_logo_path") != null && rs.getString("college_logo_path").lastIndexOf(".") > 0) {
        int lastindex = rs.getString("college_logo_path").lastIndexOf(".");
        String thumbimage = rs.getString("college_logo_path");
        thumbimage = thumbimage.substring(0, lastindex) + "_thumb" + thumbimage.substring(lastindex);
        bean.setImageThumbPath(thumbimage);
      }
      
      return bean;
    }
  }

  /**
    *  This class is used to load the unistats details for the given collegeid.
    */
  private class unistatsCollegeImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchResultBean bean = new SearchResultBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setStudentsSatisfication(rs.getString("students_satisfied"));
      bean.setStudentsJob(rs.getString("students_job"));
      bean.setIntlStudRatio(rs.getString("intl_students"));
      bean.setMaleFemaleRatio(rs.getString("gender_ratio"));
      bean.setDropoutRate(rs.getString("drop_out_rate"));
      bean.setAvgUcasPoints(rs.getString("ucas_point"));
      bean.setAccomodation(rs.getString("average_weekly_rent"));
      bean.setScholarshipCount(rs.getString("scholarship_count"));
      bean.setTimesLeaguePosition(rs.getString("times_ranking"));
      bean.setStudentStaffRatio(rs.getString("student_staff_ratio"));
      bean.setShowMoreLink("NO");
      if
        //(rs.getString("ucas_point") !=null && rs.getString("ucas_point").trim().length()>0 && !rs.getString("ucas_point").equalsIgnoreCase("no data")) ||
        ((rs.getString("student_staff_ratio") != null && rs.getString("student_staff_ratio").trim().length() > 0) || (rs.getString("students_satisfied") != null && rs.getString("students_satisfied").trim().length() > 0) || (rs.getString("students_job") != null && rs.getString("students_job").trim().length() > 0) || (rs.getString("intl_students") != null && rs.getString("intl_students").trim().length() > 0) || (rs.getString("gender_ratio") != null && rs.getString("gender_ratio").trim().length() > 0) || (rs.getString("drop_out_rate") != null && rs.getString("drop_out_rate").trim().length() > 0) || (rs.getString("average_weekly_rent") != null && rs.getString("average_weekly_rent").trim().length() > 0) || (rs.getString("scholarship_count") != null && rs.getString("scholarship_count").trim().length() > 0 && !rs.getString("scholarship_count").equals("0"))) {
        bean.setShowMoreLink("YES");
      }
      return bean;
    }
  }
}
