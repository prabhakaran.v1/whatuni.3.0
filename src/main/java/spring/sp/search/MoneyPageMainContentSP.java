package spring.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.MoneyPageVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.form.WhatStudentsAreSayingAboutThisCollegeBean;

import com.wuni.util.seo.SeoUrlsForAffiliateSiteLinks;
import com.wuni.util.valueobject.ClearingInfoVO;

/**
    * @author Mohamed Syed
    * @version 1.0
    * @since 2010-Aug-04
    * This will return money-page's main details(headerId, mid-content, refineByContent, numberOfTotalRecordsForThatSearchSynario)
    *
    * previously WU money-page-main-details was bulit based on the fllowing steps
    * 1) call to "hot_wuni.whatuni_search.adv_col_do " will return headerId
    * 2) using that headerId another DB-call for "wu_course_search_pkg.show_results_col", this will retun money page's mid content
    * 3) using headerId another DB-call for "WU_UTIL_PKG.GET_REFINE_POD_FN" will return REFINE_BY options
    * 4) using headerId another DB-call for "wu_course_search_pkg.show_results_col_count" will return numberOfTotalRecordsForThatSearchSynario
    *
    * now we are going to get these main details from theis one DB-call
    *
    * Change Log
    * *************************************************************************************************************************
    * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
    * *************************************************************************************************************************
    * 2010-Aug-04   Syed                        1.0     Initial draft                                                WU_2.0.9
    *
    */
public class MoneyPageMainContentSP extends StoredProcedure {

  public MoneyPageMainContentSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT"); 
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("lucky", Types.VARCHAR));
    declareParameter(new SqlParameter("crs_search", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("ref_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_deemed_uni", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_action", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_search_results", OracleTypes.CURSOR, new SearchResultsRowMapper()));
    declareParameter(new SqlOutParameter("o_refine_by_options", OracleTypes.CURSOR, new RefineByOptionsRowMapper()));
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_search_college_ids", OracleTypes.CURSOR, new OmnitureLoggingRowMapper()));
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("x", inputList.get(0));
      inMap.put("y", inputList.get(1));
      inMap.put("search_what", inputList.get(2));
      inMap.put("phrase_search", inputList.get(3));
      inMap.put("college_name", inputList.get(4));
      inMap.put("qualification", inputList.get(5));
      inMap.put("country", inputList.get(6));
      inMap.put("town_city", inputList.get(7));
      inMap.put("postcode", inputList.get(8));
      inMap.put("search_range", inputList.get(9));
      inMap.put("postflag", inputList.get(10));
      inMap.put("location_id", inputList.get(11));
      inMap.put("study_mode", inputList.get(12));
      inMap.put("course_duration", inputList.get(13));
      inMap.put("lucky", inputList.get(14));
      inMap.put("crs_search", inputList.get(15));
      inMap.put("submit", inputList.get(16));
      inMap.put("a", inputList.get(17));
      inMap.put("search_how", inputList.get(18));
      inMap.put("s_type", inputList.get(19));
      inMap.put("search_category", inputList.get(20));
      inMap.put("search_career", inputList.get(21));
      inMap.put("career_id", inputList.get(22));
      inMap.put("ref_id", inputList.get(23));
      inMap.put("nrp", inputList.get(24));
      inMap.put("area", inputList.get(25));
      inMap.put("p_pg_type", inputList.get(26));
      inMap.put("p_search_col", inputList.get(27));
      inMap.put("p_county_id", inputList.get(28));
      inMap.put("p_entity_text", inputList.get(29));
      inMap.put("p_search_where", inputList.get(30));
      inMap.put("p_user_agent", inputList.get(31));
      inMap.put("p_search_browse_link", inputList.get(32));
      inMap.put("p_ucas_code", inputList.get(33));
      inMap.put("p_deemed_uni", inputList.get(34));
      inMap.put("p_search_action", inputList.get(35));
      inMap.put("p_page_no", inputList.get(36));
      inMap.put("p_basket_id", inputList.get(37));
      inMap.put("p_search_cat_id", inputList.get(38));
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT", inMap);
    }
    return outMap;
  }

  /**
   * this row mapper used to map refine by pod's content detalis
   */
  private class OmnitureLoggingRowMapper implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
      omnitureLoggingVO.setCollegeIdsForThisSearch(resultset.getString("college_ids"));
      return omnitureLoggingVO;
    }

  }

  /**
   * this row mapper used to map refine by pod's content detalis
   */
  private class RefineByOptionsRowMapper implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RefineByOptionsVO refineByOptionsVO = new RefineByOptionsVO();
      refineByOptionsVO.setRefineOrder(resultset.getString("REFINE_ORDER"));
      refineByOptionsVO.setRefineCode(resultset.getString("REFINE_CODE"));
      refineByOptionsVO.setRefineDesc(resultset.getString("REFINE_DESC"));
      refineByOptionsVO.setCategoryId(resultset.getString("CATEGORY_ID"));
      refineByOptionsVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      refineByOptionsVO.setDisplaySequence(resultset.getString("DISP_SEQ"));
      refineByOptionsVO.setParentCountryName(resultset.getString("PARENT_COUNTRY_NAME"));
      refineByOptionsVO.setNumberOfChildLocations(resultset.getString("CHILD_LOCATION_COUNT"));
      return refineByOptionsVO;
    }

  }

  /**
   * this row mapper used to map mid content detalis
   */
  private class SearchResultsRowMapper implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      MoneyPageVO moneyPageVO = new MoneyPageVO();
      moneyPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      moneyPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      moneyPageVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      moneyPageVO.setWebsite(resultset.getString("WEBSITE"));
      moneyPageVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
      moneyPageVO.setLearndirQualsForThisCollege(resultset.getString("LEARNDIR_QUALS")); //wu312_20110614_Syed: all related qualification fetch to form non link text, PANDA fix      
      moneyPageVO.setOverallRating(resultset.getString("OVERALL_RATING"));
      moneyPageVO.setReviewCount(resultset.getString("REVIEW_COUNT"));
      moneyPageVO.setWasThisCollegeShortListed(resultset.getString("COLLEGE_IN_BASKET"));
      moneyPageVO.setSubjectProfilePurchageFlag(resultset.getString("SUB_PROFILE_PUR_FLAG"));
      moneyPageVO.setProspectusPurchageFlag(resultset.getString("PROSPECTUS_PUR_FLAG"));
      moneyPageVO.setEmailPurchageFlag(resultset.getString("EMAIL_PUR_FLAG"));
      moneyPageVO.setWebsitePurchageFlag(resultset.getString("WEBSITE_PUR_FLAG"));
      moneyPageVO.setScholarshipCount(resultset.getString("SCHOLARSHIP_COUNT"));
      moneyPageVO.setStudentsSatisfaction(resultset.getString("STUDENTS_SATISFACTION"));
      moneyPageVO.setStudentsJob(resultset.getString("STUDENTS_JOB"));
      moneyPageVO.setCollegeProfileFlag(resultset.getString("COLLEGE_PROFILE_FLAG"));
      moneyPageVO.setCollegeLogo(resultset.getString("COLLEGE_LOGO"));
      moneyPageVO.setTotalProfileCount(resultset.getString("TOTAL_PROFILE_COUNT"));
      moneyPageVO.setSubjectCategoryCode(resultset.getString("SUBJECT_CATEGORY_CODE"));
      moneyPageVO.setTimesRanking(resultset.getString("TIMES_RANKING"));
      moneyPageVO.setSearchAction(resultset.getString("SEARCH_ACTION"));
      moneyPageVO.setThumbNo(resultset.getString("THUMB_NO"));
      moneyPageVO.setCollegeLatitudeAndLongitude(resultset.getString("COL_LAT_LONG"));
      moneyPageVO.setUcasPoint(resultset.getString("UCAS_POINT"));
      moneyPageVO.setStudentStaffRatio(resultset.getString("STUDENT_STAFF_RATIO"));
      moneyPageVO.setDropOutRate(resultset.getString("DROP_OUT_RATE"));
      moneyPageVO.setStudentJob(resultset.getString("STUDENT_JOB"));
      moneyPageVO.setIntStudents(resultset.getString("INT_STUDENTS"));
      moneyPageVO.setGenderRatio(resultset.getString("GENDER_RATIO"));
      moneyPageVO.setAccomodationFee(resultset.getString("ACCOMODATION_FEE"));
      moneyPageVO.setTimesRankingSubject(resultset.getString("TIMES_RANKING_SUBJECT"));
      moneyPageVO.setRegionName(resultset.getString("REGION_NAME"));
      moneyPageVO.setNextOpenDay(resultset.getString("NEXT_COL_OPENDAY")); //wu310_20110517_Syed: newly added for PANDA related fix
      //profileInfo related properties
      ResultSet profileInfoRS = (ResultSet)resultset.getObject("PROFILE_INFO");
      try {
        if (profileInfoRS != null) {
          while (profileInfoRS.next()) {
            moneyPageVO.setOrderItemId(profileInfoRS.getString("ORDER_ITEM_ID"));
            moneyPageVO.setSubOrderItemId(profileInfoRS.getString("SUB_ORDER_ITEM_ID"));
            moneyPageVO.setMyhcProfileId(profileInfoRS.getString("MYHC_PROFILE_ID"));
            moneyPageVO.setAdvertName(profileInfoRS.getString("ADVERT_NAME"));
            moneyPageVO.setMediaPath(profileInfoRS.getString("MEDIA_PATH"));
            moneyPageVO.setMediaTypeId(profileInfoRS.getString("MEDIA_TYPE_ID"));
            moneyPageVO.setMediaId(profileInfoRS.getString("MEDIA_ID"));
            moneyPageVO.setThumbnailMediaPath(profileInfoRS.getString("THUMBNAIL_MEDIA_PATH"));
            moneyPageVO.setThumbnailMediaTypeId(profileInfoRS.getString("THUMBNAIL_MEDIA_TYPE_ID"));
            moneyPageVO.setInteractivityId(profileInfoRS.getString("INTERACTIVITY_ID"));
            moneyPageVO.setWhichProfleBoosted(profileInfoRS.getString("BOOSTED_PROFILE_FLAG"));
            moneyPageVO.setIsIpExists(profileInfoRS.getString("IP_EXISTS_FLAG"));
            moneyPageVO.setIsSpExists(profileInfoRS.getString("SP_EXISTS_FLAG"));
            moneyPageVO.setSpOrderItemId(profileInfoRS.getString("SP_ORDER_ITEM_ID"));
            moneyPageVO.setIpOrderItemId(profileInfoRS.getString("IP_ORDER_ITEM_ID"));
            moneyPageVO.setMychPrifileIdForIp(profileInfoRS.getString("IP_MYHC_PROFILE_ID"));
            moneyPageVO.setMychPrifileIdForSp(profileInfoRS.getString("SP_MYHC_PROFILE_ID"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (profileInfoRS != null) {
            profileInfoRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      //enquiryDetails related properties
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            moneyPageVO.setSubOrderWebsite(enquiryDetailsRS.getString("WEBSITE"));
            moneyPageVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            moneyPageVO.setSubOrderEmailWebform(enquiryDetailsRS.getString("EMAIL_WEBFORM"));
            moneyPageVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            moneyPageVO.setSubOrderProspectusWebform(enquiryDetailsRS.getString("PROSPECTUS_WEBFORM"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      //wu310_20110517_Syed: uni-random-reviews related properties - for PANDA fixes
      ResultSet uniRandomReviewsRS = (ResultSet)resultset.getObject("UNI_RANDOM_REVIEWS");
      try {
        if (uniRandomReviewsRS != null) {
          ArrayList whatStudentsAreSayingAboutThisCollegeList = new ArrayList();
          WhatStudentsAreSayingAboutThisCollegeBean whatStudentsAreSaying = null;
          while (uniRandomReviewsRS.next()) {
            whatStudentsAreSaying = new WhatStudentsAreSayingAboutThisCollegeBean();
            whatStudentsAreSaying.setReviewId(uniRandomReviewsRS.getString("review_id"));
            whatStudentsAreSaying.setReviewTitle(uniRandomReviewsRS.getString("review_title"));
            whatStudentsAreSaying.setReviewerName(uniRandomReviewsRS.getString("reviewer_name"));
            whatStudentsAreSaying.setReviewRating(uniRandomReviewsRS.getString("review_ratings"));
            whatStudentsAreSaying.setCollegeId(uniRandomReviewsRS.getString("college_id"));
            whatStudentsAreSaying.setCollegeName(uniRandomReviewsRS.getString("college_name"));
            whatStudentsAreSayingAboutThisCollegeList.add(whatStudentsAreSaying);
          }
          moneyPageVO.setWhatStudentsAreSayingAboutThisCollegeList(whatStudentsAreSayingAboutThisCollegeList);
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (uniRandomReviewsRS != null) {
            uniRandomReviewsRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
      ResultSet clearingRS = (ResultSet)resultset.getObject("clearing_info");
      try {
        if (clearingRS != null) {
          ArrayList clearingInfoList = new ArrayList();
          ClearingInfoVO clearingInfoVO = null;
          SeoUrlsForAffiliateSiteLinks affiliateSiteLinks = new SeoUrlsForAffiliateSiteLinks();
          while (clearingRS.next()) {
            clearingInfoVO = new ClearingInfoVO();
            clearingInfoVO.setClearingCollegeId(clearingRS.getString("college_id"));
            clearingInfoVO.setClearingCollegeName(clearingRS.getString("college_name"));
            clearingInfoVO.setClearingProfileCount(clearingRS.getString("clearing_profile_count"));
            if (clearingInfoVO.getClearingProfileCount() != null && (Integer.parseInt(String.valueOf(clearingInfoVO.getClearingProfileCount())) == 1)) {
              clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcPoviderClearingURL(clearingInfoVO.getClearingCollegeId(), clearingInfoVO.getClearingCollegeName()));
            } else {
              clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcClearingHomeURL());
            }
            clearingInfoList.add(clearingInfoVO);
          }
          moneyPageVO.setClearingInfoList(clearingInfoList);
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (clearingRS != null) {
            clearingRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return moneyPageVO;
    }

  }

}
