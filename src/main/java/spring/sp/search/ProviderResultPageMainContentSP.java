package spring.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.CpeWorstPerformersVO;
import spring.valueobject.search.ProviderResultPageVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

/**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Aug-04
   *
   * This will return provider-result-page's main details(headerId, mid-content, numberOfTotalRecordsForThatSearchSynario)
   *
   * previously WU provider-result-page-main-details was bulit based on the fllowing steps
   * 1) call to "hot_wuni.whatuni_search.adv_search_do" will return headerId
   * 2) using that headerId another DB-call for "Wu_Course_Search_Pkg.show_results_crs", this will retun money page's mid content
   * 3) using headerId another DB-call for "wu_course_search_pkg.show_results_course_count" will return numberOfTotalRecordsForThatSearchSynario
   *
   * now we are going to get these main details from theis one DB-call
   *
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.      Changes desc                                                  Rel Ver.
   * *************************************************************************************************************************
   * 2010-Aug-04   Syed                        1.0      Initial draft                                                 wu_2.0.9
   * 2010-Dec-07   Syed                        1.1      added "o_cpe_worst_performers" related changes                wu_2.1.5
   *
   */
    
public class ProviderResultPageMainContentSP extends StoredProcedure {

  public ProviderResultPageMainContentSP() {}
  
  public ProviderResultPageMainContentSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WHATUNI_SEARCH3.GET_PROVIDER_RESULT_PAGE");    
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));    
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("aui", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("z", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pheader_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));    
    declareParameter(new SqlOutParameter("o_search_results", OracleTypes.CURSOR, new ProviderResultsRowMapper()));
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_cpe_worst_performers", OracleTypes.CURSOR, new CpeWorstPerformersRowMapper()));
  }

  public Map execute(List inputList) {     
    Map outMap = null;
    Map inMap = new HashMap(); 
    try{           
      inMap.put("x", inputList.get(0));
      inMap.put("y", inputList.get(1));
      inMap.put("search_what", inputList.get(2));
      inMap.put("phrase_search", inputList.get(3));
      inMap.put("college_name", inputList.get(4));    
      inMap.put("qualification", inputList.get(5));
      inMap.put("country", inputList.get(6));
      inMap.put("town_city", inputList.get(7));
      inMap.put("postcode", inputList.get(8));
      inMap.put("search_range", inputList.get(9));
      inMap.put("postflag", inputList.get(10));
      inMap.put("location_id", inputList.get(11));
      inMap.put("study_mode", inputList.get(12));
      inMap.put("course_duration", inputList.get(13));
      inMap.put("submit", inputList.get(14));
      inMap.put("a", inputList.get(15));
      inMap.put("aui", inputList.get(16));
      inMap.put("search_how", inputList.get(17));
      inMap.put("z", inputList.get(18));
      inMap.put("s_type", inputList.get(19));
      inMap.put("search_category", inputList.get(20));
      inMap.put("search_career", inputList.get(21));
      inMap.put("career_id", inputList.get(22));
      inMap.put("nrp", inputList.get(23));
      inMap.put("area", inputList.get(24));
      inMap.put("p_pg_type", inputList.get(25));
      inMap.put("p_search_col", inputList.get(26));
      inMap.put("p_county_id", inputList.get(27));
      inMap.put("p_pheader_id", inputList.get(28));
      inMap.put("p_entity_text", inputList.get(29));
      inMap.put("p_search_where", inputList.get(30));
      inMap.put("p_user_agent", inputList.get(31));
      inMap.put("p_search_browse_link", inputList.get(32));
      inMap.put("p_ucas_code", inputList.get(33));
      inMap.put("p_basket_id", inputList.get(34));
      inMap.put("p_page_no", inputList.get(35));     
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();    
    }
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_PROVIDER_RESULT_PAGE", inMap);  
    }
    return outMap;
  }

  /**
   * this row mapper used to map mid cpe-worst-performers-details  detalis
   */
  private class CpeWorstPerformersRowMapper implements RowMapper {
    public Object mapRow(ResultSet cpeWorstPerformersRS, int rowNum) throws SQLException {
      CpeWorstPerformersVO cpeWorstPerformersVO = new CpeWorstPerformersVO();
      cpeWorstPerformersVO.setCollegeId(cpeWorstPerformersRS.getString("COLLEGE_ID"));
      cpeWorstPerformersVO.setCollegeName(cpeWorstPerformersRS.getString("COLLEGE_NAME"));
      cpeWorstPerformersVO.setCollegeNameDisplay(cpeWorstPerformersRS.getString("COLLEGE_NAME_DISPLAY"));
      cpeWorstPerformersVO.setLdcs1Code(cpeWorstPerformersRS.getString("LDCS1_CODE"));
      cpeWorstPerformersVO.setLdcs1Description(cpeWorstPerformersRS.getString("LDCS1_DESCRIPTION"));     
      if(cpeWorstPerformersVO.getLdcs1Description() != null){
        cpeWorstPerformersVO.setHashRemovedLdcs1Description(cpeWorstPerformersVO.getLdcs1Description().replaceAll("#","-"));
      }
      cpeWorstPerformersVO.setQualificationCode(cpeWorstPerformersRS.getString("QUALIFICATION_CODE"));
      cpeWorstPerformersVO.setQualificationDescription(cpeWorstPerformersRS.getString("QUALIFICATION_DESCRIPTION"));
      
      ResultSet cpeWorstPerformersEnquiryDetailsRS = (ResultSet)cpeWorstPerformersRS.getObject("ENQUIRY_DETAILS");
      try{
        if(cpeWorstPerformersEnquiryDetailsRS != null){
          while(cpeWorstPerformersEnquiryDetailsRS.next()){
            cpeWorstPerformersVO.setOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("ORDER_ITEM_ID"));
            cpeWorstPerformersVO.setSubOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            cpeWorstPerformersVO.setMyhcProfileId(cpeWorstPerformersEnquiryDetailsRS.getString("MYHC_PROFILE_ID"));
            cpeWorstPerformersVO.setProfileType(cpeWorstPerformersEnquiryDetailsRS.getString("PROFILE_TYPE"));        
            cpeWorstPerformersVO.setSubOrderEmail(cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL"));
            cpeWorstPerformersVO.setSubOrderProspectus(cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS"));
            cpeWorstPerformersVO.setCpeQualificationNetworkId(cpeWorstPerformersEnquiryDetailsRS.getString("NETWORK_ID"));
            
            String tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("WEBSITE");
            if(tmpUrl != null && tmpUrl.trim().length() > 0){
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else { tmpUrl = ""; }
            cpeWorstPerformersVO.setSubOrderWebsite(tmpUrl); 
            
            tmpUrl =  cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL_WEBFORM");
            if(tmpUrl != null && tmpUrl.trim().length() > 0){
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else { tmpUrl = ""; }
            cpeWorstPerformersVO.setSubOrderEmailWebform(tmpUrl); 
            
            tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS_WEBFORM");  
            if(tmpUrl != null && tmpUrl.trim().length() > 0){
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else { tmpUrl = ""; }
            cpeWorstPerformersVO.setSubOrderProspectusWebform(tmpUrl); 
            
            //collecting media informations
            cpeWorstPerformersVO.setMediaId(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_ID"));
            cpeWorstPerformersVO.setMediaPath(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_PATH"));
            cpeWorstPerformersVO.setMediaType(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_TYPE"));
            cpeWorstPerformersVO.setMediaThumbPath(cpeWorstPerformersEnquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
            if(cpeWorstPerformersVO.getMediaId() != null && cpeWorstPerformersVO.getMediaPath() != null && cpeWorstPerformersVO.getMediaType() != null ){              
              if (cpeWorstPerformersVO.getMediaType().equalsIgnoreCase("video")) {
                cpeWorstPerformersVO.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(cpeWorstPerformersVO.getMediaPath(), "3"));           
              } else if (cpeWorstPerformersVO.getMediaType().equalsIgnoreCase("picture")) {       
//                if(cpeWorstPerformersVO.getMediaPath().indexOf("http://") > -1 ){
//                  cpeWorstPerformersVO.setMediaPath(cpeWorstPerformersVO.getMediaPath());
//                } else {
//                  cpeWorstPerformersVO.setMediaPath(new CommonFunction().getWUSysVarValue("IMAGE_DISPLAY_PATH") + "college/image/" + cpeWorstPerformersVO.getCollegeId() + "/" + cpeWorstPerformersVO.getMediaPath());
//                }
             } else {
               cpeWorstPerformersVO.setMediaPath("/img/whatuni/unihome_default.png");
             }
            }else{
              cpeWorstPerformersVO.setMediaId("-999");
              cpeWorstPerformersVO.setMediaPath("/img/whatuni/unihome_default.png");
              cpeWorstPerformersVO.setMediaType("PICTURE");
            }
          }
        }        
      } catch(Exception e) {
          e.printStackTrace();
      } finally {
        try {
          if(cpeWorstPerformersEnquiryDetailsRS != null){
            cpeWorstPerformersEnquiryDetailsRS.close();
          }
        } catch(Exception e) {
          e.printStackTrace();
        }  
      }      
      return cpeWorstPerformersVO;    
    }
  }

  
  /**
   * this row mapper used to map mid content detalis
   */
  private class ProviderResultsRowMapper implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      ProviderResultPageVO providerResultPageVO = new ProviderResultPageVO();      
      providerResultPageVO.setCourseId(resultset.getString("COURSE_ID"));
      providerResultPageVO.setCourseDescription(resultset.getString("COURSE_DESC"));
      providerResultPageVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
      providerResultPageVO.setCourseDuration(resultset.getString("DURATION"));
      providerResultPageVO.setLearndDirQual(resultset.getString("LEARNDIR_QUAL"));
      providerResultPageVO.setCostDescription(resultset.getString("COST_DESC"));
      providerResultPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      providerResultPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      providerResultPageVO.setWebsite(resultset.getString("WEBSITE"));
      providerResultPageVO.setProfile(resultset.getString("PROFILE"));
      providerResultPageVO.setVenueName(resultset.getString("VENUE_NAME"));
      providerResultPageVO.setPostcode(resultset.getString("POSTCODE"));
      providerResultPageVO.setUcasCode(resultset.getString("UCAS_CODE"));
      providerResultPageVO.setUcasFlag(resultset.getString("UCAS_FLAG"));
      providerResultPageVO.setUcasPoint(resultset.getString("UCAS_TARIFF")); 
      providerResultPageVO.setStudyMode(resultset.getString("STUDY_MODE"));
      providerResultPageVO.setWasThisCourseShortListed(resultset.getString("COURSE_IN_BASKET"));
      providerResultPageVO.setScholarshipCount(resultset.getString("SCHOLORSHIP_COUNT"));
      providerResultPageVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
      providerResultPageVO.setWebsitePurchaseFlag(resultset.getString("WEBSITE_PUR_FLAG"));
      providerResultPageVO.setProviderUrl(resultset.getString("PROVIDER_URL"));
      providerResultPageVO.setDepartmentOrFaculty(resultset.getString("DEPARTMENT_OR_FACULTY"));
      providerResultPageVO.setAvailableStudyModes(resultset.getString("AVAILABLE_STUDY_MODES"));       
      //
      //Button Details
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
      try{
        if(enquiryDetailsRS != null){
          while(enquiryDetailsRS.next()){
            providerResultPageVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));                
            providerResultPageVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            providerResultPageVO.setSubOrderWebsite(enquiryDetailsRS.getString("WEBSITE"));                
            providerResultPageVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            providerResultPageVO.setSubOrderEmailWebform(enquiryDetailsRS.getString("EMAIL_WEBFORM"));
            providerResultPageVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            providerResultPageVO.setSubOrderProspectusWebform(enquiryDetailsRS.getString("PROSPECTUS_WEBFORM"));                
          }
        }        
      } catch(Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if(enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch(Exception e) {
          throw new SQLException(e.getMessage());
        }  
      }
      return providerResultPageVO;    
    }
  }
}
