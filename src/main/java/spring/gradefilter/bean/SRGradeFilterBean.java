package spring.gradefilter.bean;

public class SRGradeFilterBean {
	
  private String userTrackSessionId;
  private String userId;
  private String qualId;
  private String affiliateId;
  private String userStudyLevelEntry;
  private String userEntryPoint;
  private String ucasPoint;

public String getUcasPoint() {
	return ucasPoint;
}
public void setUcasPoint(String ucasPoint) {
	this.ucasPoint = ucasPoint;
}
public String getUserStudyLevelEntry() {
	return userStudyLevelEntry;
}
public void setUserStudyLevelEntry(String userStudyLevelEntry) {
	this.userStudyLevelEntry = userStudyLevelEntry;
}
public String getUserEntryPoint() {
	return userEntryPoint;
}
public void setUserEntryPoint(String userEntryPoint) {
	this.userEntryPoint = userEntryPoint;
}
public String getUserTrackSessionId() {
	return userTrackSessionId;
}
public void setUserTrackSessionId(String userTrackSessionId) {
	this.userTrackSessionId = userTrackSessionId;
}
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
public String getQualId() {
	return qualId;
}
public void setQualId(String qualId) {
	this.qualId = qualId;
}
public String getAffiliateId() {
	return affiliateId;
}
public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
}
}
