package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class LocationListVO {

	private String regionName = null;
	private String regionTextKey = null;
	private String selectedFlag = null;
}
