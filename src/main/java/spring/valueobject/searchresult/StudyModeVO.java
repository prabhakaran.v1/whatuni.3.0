package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class StudyModeVO {

	private String refineCode = null;
	private String refineDesc = null;
	private String refineTextKey = null;
	private String selectedFlag = null;
}
