package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class CourseSpecificListVO {
  private String courseId = null;
  private String courseTitle = null;
  private String courseUcasTariff = null;
  private String courseDomesticFees = null;	 
  private String wasThisCourseShortlisted = null;	  
  private String studyLevelDesc = null;
  private String studyLevelDescDet = null;	  
  private String ucasCode = null;
  private String subOrderItemId = null;
  private String orderItemId = null;	  
  private String subOrderEmail = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectus = null;
  private String subOrderProspectusWebform = null;
  private String subOrderWebsite = null;
  private String feeDuration = null;  
  private String myhcProfileId = null;	  
  private String profileType = null;
  private String applyNowFlag = null;
  private String mediaId = null;
  private String mediaPath = null;
  private String mediaType = null;
  private String cpeQualificationNetworkId = null;
  private String pageName = null;
  private String courseDetailsPageURL = null;
  private String webformPrice = null;
  private String websitePrice = null;
  private String employmentRate = null;
  private String courseRank = null;
  private String addedToFinalChoice = null;
  private String courseTitleSpecialCharRemove = null;
	  
}
