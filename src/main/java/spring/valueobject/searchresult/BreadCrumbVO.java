package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class BreadCrumbVO {

	private String bradcrumbText = null;
	private String breadcrumbUrl = null;
	private String itemId = null;
}
