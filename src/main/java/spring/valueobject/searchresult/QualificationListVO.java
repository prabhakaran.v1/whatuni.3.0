package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class QualificationListVO {

	private String refineCode = null;
	private String refineDesc = null;
	private String refineTextKey = null;
	private String refineDisplayDesc = null;;
	private String categoryId = null;
	private String categoryCode = null;
	private String categoryName = null;
	private String browseCatTextKey = null;
	private String selectedFlag = null;
}
