package spring.valueobject.searchresult;

import java.sql.Clob;
import lombok.Data;

@Data
public class FooterSectionVO {

	private Clob footerHtmlText = null;
}
