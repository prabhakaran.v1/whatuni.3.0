package spring.valueobject.searchresult;

import java.util.ArrayList;
import lombok.Data;

@Data
public class SearchResultContentsVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameSpecialCharRemove = null;
  private String collegeTextKey = null;
  private String collegePRUrl = null;
  private String collegeNameDisplay = null;
  private String uniHomeUrl = null;
  private String overallRating = null;
  private String exactRating = null;
  private String opendayUrl = null;
  private String numberOfCoursesForThisCollege = null;
  private String wasThisCollegeShortlisted = null;
  private String collegeLogoPath = null;
  private String russelGroup = null;
  private String jacsSubject = null;
  private String currentRank = null;
  private String totalRank = null;
  private String networkId = null;
  private String opendaySuborderItemId = null;
  private String openDate = null;
  private String openMonthYear = null;
  private String bookingUrl = null;
  private String totalOpendays = null;
  private String providerId = null;
  private String sponsoredListFlag = null;
  private String eventCategoryId = null; 
  private String eventCategoryName = null; 	  
  private String reviewCount = null;
  private String uniReviewsURL = null;
  private String eventCategoryNameGa = null;
  private ArrayList<CourseSpecificListVO> bestMatchCoursesList = null;

}
