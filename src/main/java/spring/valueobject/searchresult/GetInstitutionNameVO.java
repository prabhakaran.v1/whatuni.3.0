package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class GetInstitutionNameVO {

  private String keywordText = null;
  private String pageName = null;
  private String collegeName = null;
  private String collegeNameAlias = null;
  private String collegeNameDisplay = null;
  private String collegeLocation = null;
  private String collegeId = null;
  private String odMsg = null;
  private String reviewMsg = null;
  private String collegeTextKey = null;
}
