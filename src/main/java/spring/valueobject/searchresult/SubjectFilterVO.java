package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class SubjectFilterVO {

	  private String categoryCode = null;
	  private String categoryId = null;
	  private String categoryDesc = null;
	  private String browseMoneyPageUrl = null;
	  private String collegeCount = null;//Search_redesign
	  private String qualificationCode = null;
	  private String subjectTextKey = null;  
	  private String selectedFlag = null;

}
