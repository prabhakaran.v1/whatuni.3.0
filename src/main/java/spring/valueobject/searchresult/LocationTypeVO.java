package spring.valueobject.searchresult;

import lombok.Data;

@Data
public class LocationTypeVO {

	private String optionId = null;
	private String optionDesc = null;
	private String optionType = null;
	private String optionTextKey = null;
	private String selectedFlag = null;
}
