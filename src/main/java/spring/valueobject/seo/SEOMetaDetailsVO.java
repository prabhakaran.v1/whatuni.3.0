package spring.valueobject.seo;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class SEOMetaDetailsVO {

  private String metaTitle;
  private String metaDesc;
  private String metaKeyword;
  private String metaRobots;
  private String categoryDesc;
  private String studyDesc;
}

