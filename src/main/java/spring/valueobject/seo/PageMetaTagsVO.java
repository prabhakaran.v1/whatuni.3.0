package spring.valueobject.seo;

public class PageMetaTagsVO {
  //main
  private String metaTitle = "";
  private String metaDescription = "";
  private String metaKeywords = "";
  private String metaRobots = "";
  //misc
  private String metaCategoryDesc = "";
  private String metaStudylevelDesc = "";

  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  public String getMetaTitle() {
    return metaTitle;
  }

  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  public String getMetaDescription() {
    return metaDescription;
  }

  public void setMetaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
  }

  public String getMetaKeywords() {
    return metaKeywords;
  }

  public void setMetaRobots(String metaRobots) {
    this.metaRobots = metaRobots;
  }

  public String getMetaRobots() {
    return metaRobots;
  }

  public void setMetaCategoryDesc(String metaCategoryDesc) {
    this.metaCategoryDesc = metaCategoryDesc;
  }

  public String getMetaCategoryDesc() {
    return metaCategoryDesc;
  }

  public void setMetaStudylevelDesc(String metaStudylevelDesc) {
    this.metaStudylevelDesc = metaStudylevelDesc;
  }

  public String getMetaStudylevelDesc() {
    return metaStudylevelDesc;
  }

}
