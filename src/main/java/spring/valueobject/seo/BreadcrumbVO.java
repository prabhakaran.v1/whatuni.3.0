package spring.valueobject.seo;

public class BreadcrumbVO {
  private String collegeId = null;
  private String pageName = null;
  private String qualCode = null;
  private String catId = null;
  private String catCode = null;
  private String location = null;
  private String searchKeyword = null;
  private String bradcrumbText = null;
  private String breadcrumbUrl = null;
  private String breadcrumbName = null;
  private String itemId = null;
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setQualCode(String qualCode) {
    this.qualCode = qualCode;
  }
  public String getQualCode() {
    return qualCode;
  }
  public void setCatId(String catId) {
    this.catId = catId;
  }
  public String getCatId() {
    return catId;
  }
  public void setCatCode(String catCode) {
    this.catCode = catCode;
  }
  public String getCatCode() {
    return catCode;
  }
  public void setLocation(String location) {
    this.location = location;
  }
  public String getLocation() {
    return location;
  }
  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }
  public String getSearchKeyword() {
    return searchKeyword;
  }
  public void setBradcrumbText(String bradcrumbText) {
    this.bradcrumbText = bradcrumbText;
  }
  public String getBradcrumbText() {
    return bradcrumbText;
  }
  public void setBreadcrumbUrl(String breadcrumbUrl) {
    this.breadcrumbUrl = breadcrumbUrl;
  }
  public String getBreadcrumbUrl() {
    return breadcrumbUrl;
  }
  public void setBreadcrumbName(String breadcrumbName) {
    this.breadcrumbName = breadcrumbName;
  }
  public String getBreadcrumbName() {
    return breadcrumbName;
  }
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }
  public String getItemId() {
    return itemId;
  }
}
