package spring.valueobject.advice;

public class AdviceDetailInfoVO {
    //Advice detail onfo    
    private String postId = "";
    private String authorId = "";
    private String authorName = "";
    private String authorMediaPath = "";
    private String authorGoogleAccount = "";
    private String postTitle = "";
    private String postUrl = "";
    private String metaTitle = "";
    private String metaDescription = "";
    private String metaKeywords = "";
    private String articleLdcs1 = "";
    private String articleGroupQualId = "";
    private String firstParagraph = "";
    private String lastParagraph = "";
    private String mediaId = "";
    private String mediaPath = "";
    private String mediaAltText = "";
    private String videoUrl = "";
    private String mediaType = "";
    private String updatedFlag = "";
    private String nextArticleUrl = "";
    private String readCount = "";
    private String primaryParentCategoryName = "";
    private String primarySubCategoryName = "";
    private String parentCategoryName = "";
    private String subCategoryName = "";
    private String primaryCategoryName = "";
    private String createdDate = "";
    private String updatedDate = "";
    private String mediaUrl = "";
    private String adviceDetailURL = "";
    private String nextAdviceDetailURL = "";
    
    private String courseId = "";
    private String courseTitle = "";
    private String collegeId = "";
    private String collegeName = "";    
    private String collegeNameDisplay = "";
    private String courseInBasket = "";
    private String studyLeveDesc = "";
    private String cnt = "";
    private String websitePrice = "";
    private String webformPrice = "";
    private String pageName = "";
    
    private String orderItemId = null;
    private String subOrderItemId = null;
    private String myhcProfileId = null;
    private String profileId = null;
    private String profileType = null;
    private String advertName = null;
    private String cpeQualificationNetworkId = null;
    private String subOrderEmail = null;
    private String subOrderProspectus = null;
    private String subOrderWebsite = null;
    private String subOrderEmailWebform = null;
    private String subOrderProspectusWebform = null;
    private String mediaThumbPath = null;
    private String videoThumbPath = null;
    private String imageName = null;
    private String thumbImageName = null;
    private String AuthorImageName = null;
    private String pullQuote = null;
    private String pdfId = null;
    private String guideName = null;
    private String guideType = null;
    private String fileName = null;
    private String hotline = null;
    private String qlFlag = null;
    private String gaCollegeName = null;   
    
    private String nextPostId = null;
    private String nextPostTitle = null;
    private String nextPostUrl = null;
    private String nextParentCategoryName = null;
    private String nextSubCategoryName = null;
    private String nextPrimaryCategoryName = null;
    private String courseDetailsPageURL = null;
    private String uniHomeURL = null;
    
    private String collegeLogo = null;
    private String profileText = null;
    private String whatTheUniSay = null;
    private String collegeInBasket = null;    
    private String pdfUrl = null;    
    private String cityName = null;    
    private String tags = null;    
    private String sponsorUrl = null;
    private String sponsorLogo = null; 
    private String articleViewedCount = null;
    private String postShortText = null; 
    private String sponsPixelTracking = null;
    
    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorMediaPath(String authorMediaPath) {
        this.authorMediaPath = authorMediaPath;
    }

    public String getAuthorMediaPath() {
        return authorMediaPath;
    }

    public void setAuthorGoogleAccount(String authorGoogleAccount) {
        this.authorGoogleAccount = authorGoogleAccount;
    }

    public String getAuthorGoogleAccount() {
        return authorGoogleAccount;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setArticleLdcs1(String articleLdcs1) {
        this.articleLdcs1 = articleLdcs1;
    }

    public String getArticleLdcs1() {
        return articleLdcs1;
    }

    public void setArticleGroupQualId(String articleGroupQualId) {
        this.articleGroupQualId = articleGroupQualId;
    }

    public String getArticleGroupQualId() {
        return articleGroupQualId;
    }

    public void setFirstParagraph(String firstParagraph) {
        this.firstParagraph = firstParagraph;
    }

    public String getFirstParagraph() {
        return firstParagraph;
    }

    public void setLastParagraph(String lastParagraph) {
        this.lastParagraph = lastParagraph;
    }

    public String getLastParagraph() {
        return lastParagraph;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaAltText(String mediaAltText) {
        this.mediaAltText = mediaAltText;
    }

    public String getMediaAltText() {
        return mediaAltText;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setUpdatedFlag(String updatedFlag) {
        this.updatedFlag = updatedFlag;
    }

    public String getUpdatedFlag() {
        return updatedFlag;
    }

    public void setNextArticleUrl(String nextArticleUrl) {
        this.nextArticleUrl = nextArticleUrl;
    }

    public String getNextArticleUrl() {
        return nextArticleUrl;
    }

    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    public String getReadCount() {
        return readCount;
    }

    public void setPrimaryParentCategoryName(String primaryParentCategoryName) {
        this.primaryParentCategoryName = primaryParentCategoryName;
    }

    public String getPrimaryParentCategoryName() {
        return primaryParentCategoryName;
    }

    public void setPrimarySubCategoryName(String primarySubCategoryName) {
        this.primarySubCategoryName = primarySubCategoryName;
    }

    public String getPrimarySubCategoryName() {
        return primarySubCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setAdviceDetailURL(String adviceDetailURL) {
        this.adviceDetailURL = adviceDetailURL;
    }

    public String getAdviceDetailURL() {
        return adviceDetailURL;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeNameDisplay(String collegeNameDisplay) {
        this.collegeNameDisplay = collegeNameDisplay;
    }

    public String getCollegeNameDisplay() {
        return collegeNameDisplay;
    }

    public void setCourseInBasket(String courseInBasket) {
        this.courseInBasket = courseInBasket;
    }

    public String getCourseInBasket() {
        return courseInBasket;
    }

    public void setStudyLeveDesc(String studyLeveDesc) {
        this.studyLeveDesc = studyLeveDesc;
    }

    public String getStudyLeveDesc() {
        return studyLeveDesc;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getCnt() {
        return cnt;
    }

    public void setWebsitePrice(String websitePrice) {
        this.websitePrice = websitePrice;
    }

    public String getWebsitePrice() {
        return websitePrice;
    }

    public void setWebformPrice(String webformPrice) {
        this.webformPrice = webformPrice;
    }

    public String getWebformPrice() {
        return webformPrice;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setMyhcProfileId(String myhcProfileId) {
        this.myhcProfileId = myhcProfileId;
    }

    public String getMyhcProfileId() {
        return myhcProfileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
        this.cpeQualificationNetworkId = cpeQualificationNetworkId;
    }

    public String getCpeQualificationNetworkId() {
        return cpeQualificationNetworkId;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderWebsite(String subOrderWebsite) {
        this.subOrderWebsite = subOrderWebsite;
    }

    public String getSubOrderWebsite() {
        return subOrderWebsite;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }

    public void setMediaThumbPath(String mediaThumbPath) {
        this.mediaThumbPath = mediaThumbPath;
    }

    public String getMediaThumbPath() {
        return mediaThumbPath;
    }

    public void setVideoThumbPath(String videoThumbPath) {
        this.videoThumbPath = videoThumbPath;
    }

    public String getVideoThumbPath() {
        return videoThumbPath;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setAuthorImageName(String authorImageName) {
        this.AuthorImageName = authorImageName;
    }

    public String getAuthorImageName() {
        return AuthorImageName;
    }

    public void setPullQuote(String pullQuote) {
        this.pullQuote = pullQuote;
    }

    public String getPullQuote() {
        return pullQuote;
    }

    public void setPdfId(String pdfId) {
        this.pdfId = pdfId;
    }

    public String getPdfId() {
        return pdfId;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public String getHotline() {
        return hotline;
    }

    public void setQlFlag(String qlFlag) {
        this.qlFlag = qlFlag;
    }

    public String getQlFlag() {
        return qlFlag;
    }

    public void setThumbImageName(String thumbImageName) {
        this.thumbImageName = thumbImageName;
    }

    public String getThumbImageName() {
        return thumbImageName;
    }

    public void setGaCollegeName(String gaCollegeName) {
        this.gaCollegeName = gaCollegeName;
    }

    public String getGaCollegeName() {
        return gaCollegeName;
    }

    public void setNextPostId(String nextPostId) {
        this.nextPostId = nextPostId;
    }

    public String getNextPostId() {
        return nextPostId;
    }

    public void setNextPostTitle(String nextPostTitle) {
        this.nextPostTitle = nextPostTitle;
    }

    public String getNextPostTitle() {
        return nextPostTitle;
    }

    public void setNextPostUrl(String nextPostUrl) {
        this.nextPostUrl = nextPostUrl;
    }

    public String getNextPostUrl() {
        return nextPostUrl;
    }

    public void setNextParentCategoryName(String nextParentCategoryName) {
        this.nextParentCategoryName = nextParentCategoryName;
    }

    public String getNextParentCategoryName() {
        return nextParentCategoryName;
    }

    public void setNextSubCategoryName(String nextSubCategoryName) {
        this.nextSubCategoryName = nextSubCategoryName;
    }

    public String getNextSubCategoryName() {
        return nextSubCategoryName;
    }

    public void setCourseDetailsPageURL(String courseDetailsPageURL) {
        this.courseDetailsPageURL = courseDetailsPageURL;
    }

    public String getCourseDetailsPageURL() {
        return courseDetailsPageURL;
    }

    public void setUniHomeURL(String uniHomeURL) {
        this.uniHomeURL = uniHomeURL;
    }

    public String getUniHomeURL() {
        return uniHomeURL;
    }

    public void setGuideType(String guideType) {
        this.guideType = guideType;
    }

    public String getGuideType() {
        return guideType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setCollegeLogo(String collegeLogo) {
        this.collegeLogo = collegeLogo;
    }

    public String getCollegeLogo() {
        return collegeLogo;
    }

    public void setProfileText(String profileText) {
        this.profileText = profileText;
    }

    public String getProfileText() {
        return profileText;
    }

    public void setWhatTheUniSay(String whatTheUniSay) {
        this.whatTheUniSay = whatTheUniSay;
    }

    public String getWhatTheUniSay() {
        return whatTheUniSay;
    }

    public void setCollegeInBasket(String collegeInBasket) {
        this.collegeInBasket = collegeInBasket;
    }

    public String getCollegeInBasket() {
        return collegeInBasket;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setNextAdviceDetailURL(String nextAdviceDetailURL) {
        this.nextAdviceDetailURL = nextAdviceDetailURL;
    }

    public String getNextAdviceDetailURL() {
        return nextAdviceDetailURL;
    }

    public void setNextPrimaryCategoryName(String nextPrimaryCategoryName) {
        this.nextPrimaryCategoryName = nextPrimaryCategoryName;
    }

    public String getNextPrimaryCategoryName() {
        return nextPrimaryCategoryName;
    }

    public void setPrimaryCategoryName(String primaryCategoryName) {
        this.primaryCategoryName = primaryCategoryName;
    }

    public String getPrimaryCategoryName() {
        return primaryCategoryName;
    }
  public void setSponsorUrl(String sponsorUrl) {
    this.sponsorUrl = sponsorUrl;
  }
  public String getSponsorUrl() {
    return sponsorUrl;
  }
  public void setSponsorLogo(String sponsorLogo) {
    this.sponsorLogo = sponsorLogo;
  }
  public String getSponsorLogo() {
    return sponsorLogo;
  }
  public void setArticleViewedCount(String articleViewedCount) {
    this.articleViewedCount = articleViewedCount;
  }
  public String getArticleViewedCount() {
    return articleViewedCount;
  }
  public void setPostShortText(String postShortText) {
    this.postShortText = postShortText;
  }
  public String getPostShortText() {
    return postShortText;
  }

    public void setSponsPixelTracking(String sponsPixelTracking) {
        this.sponsPixelTracking = sponsPixelTracking;
    }

    public String getSponsPixelTracking() {
        return sponsPixelTracking;
    }
}
