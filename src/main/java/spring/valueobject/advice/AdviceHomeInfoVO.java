package spring.valueobject.advice;

public class AdviceHomeInfoVO {
    //Advice home related details
    private String postTitle = "";
    private String postId = "";
    private String postUrl = "";
    private String postShortText = "";
    private String mediaPath = "";
    private String mediaAltText = "";
    private String videoUrl = "";
    private String updatedDate = "";
    private String totalCount = "";
    private String readCount = "";
    private String parentCategoryName = ""; 
    private String subCategoryName = "";
    private String subCategoryNameCapital = "";
    private String articleViewedCount = "";    
    private String adviceDetailURL = "";
    private String imageName = "";
    private String primaryCategoryName = "";
    private String collegeId = "";//9_June_2015_rel
    private String sponsorUrl = "";
    private String collegeDisplayName = "";
    private String sponsoredByDetails = ""; 
    private String sponsorLogo = "";   
    private String userId = "";
    private String userTrackId = "";
    private String pageName = "";
    private String opportunityId = "";  
    private String personalisedText = "";
    private String covidNewsTotalCount="";
  private String pageNo = "";
    private String postText="";
    private String articleDate="";
    private String articleTime="";
    
    public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getArticleDate() {
		return articleDate;
	}

	public void setArticleDate(String articleDate) {
		this.articleDate = articleDate;
	}

	public String getArticleTime() {
		return articleTime;
	}

	public void setArticleTime(String articleTime) {
		this.articleTime = articleTime;
	}

	public String getCovidNewsTotalCount() {
	return covidNewsTotalCount;
}

public void setCovidNewsTotalCount(String covidNewsTotalCount) {
	this.covidNewsTotalCount = covidNewsTotalCount;
}

	public String getPageNo() {
	return pageNo;
}

public void setPageNo(String pageNo) {
	this.pageNo = pageNo;
}

	public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostShortText(String postShortText) {
        this.postShortText = postShortText;
    }

    public String getPostShortText() {
        return postShortText;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaAltText(String mediaAltText) {
        this.mediaAltText = mediaAltText;
    }

    public String getMediaAltText() {
        return mediaAltText;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    public String getReadCount() {
        return readCount;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setArticleViewedCount(String articleViewedCount) {
        this.articleViewedCount = articleViewedCount;
    }

    public String getArticleViewedCount() {
        return articleViewedCount;
    }

    public void setAdviceDetailURL(String adviceDetailURL) {
        this.adviceDetailURL = adviceDetailURL;
    }

    public String getAdviceDetailURL() {
        return adviceDetailURL;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setSubCategoryNameCapital(String subCategoryNameCapital) {
        this.subCategoryNameCapital = subCategoryNameCapital;
    }

    public String getSubCategoryNameCapital() {
        return subCategoryNameCapital;
    }

    public void setPrimaryCategoryName(String primaryCategoryName) {
        this.primaryCategoryName = primaryCategoryName;
    }

    public String getPrimaryCategoryName() {
        return primaryCategoryName;
    }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setSponsorUrl(String sponsorUrl) {
    this.sponsorUrl = sponsorUrl;
  }
  public String getSponsorUrl() {
    return sponsorUrl;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setSponsoredByDetails(String sponsoredByDetails) {
    this.sponsoredByDetails = sponsoredByDetails;
  }
  public String getSponsoredByDetails() {
    return sponsoredByDetails;
  }
  public void setSponsorLogo(String sponsorLogo) {
    this.sponsorLogo = sponsorLogo;
  }
  public String getSponsorLogo() {
    return sponsorLogo;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setUserTrackId(String userTrackId) {
    this.userTrackId = userTrackId;
  }
  public String getUserTrackId() {
    return userTrackId;
  }
  public void setOpportunityId(String opportunityId) {
    this.opportunityId = opportunityId;
  }
  public String getOpportunityId() {
    return opportunityId;
  }
  public void setPersonalisedText(String personalisedText) {
    this.personalisedText = personalisedText;
  }
  public String getPersonalisedText() {
    return personalisedText;
  }
}
