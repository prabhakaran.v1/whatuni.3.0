package spring.valueobject;

import org.apache.struts.action.ActionForm;

public class HomeVO  {
  public HomeVO() {
  }
  private String rowNumner = "";
  private String collegeId = "";
  private String courseId = "";
  private String subjectId = "";
  private String collegeName = "";
  private String shortCollegeName = "";
  private String reviewId = "";
  private String reviewTitle = "";
  private String courseTitle = "";
  private String userId = "";
  private String userName = "";
  private String userGender = "";
  private String gender = "";
  private String userGraduate = "";
  private String courseName = "";
  private String nationality = "";
  private String userImage = "";
  private String userImageLarge = "";
  private String overAllRating = "";
  private String studCurrentStatus = "";
  private String overallRatingComment = "";
  private String seoStudyLevelText = "";
  private String courseDeletedFlag = "";
  public void setRowNumner(String rowNumner) {
    this.rowNumner = rowNumner;
  }
  public String getRowNumner() {
    return rowNumner;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setShortCollegeName(String shortCollegeName) {
    this.shortCollegeName = shortCollegeName;
  }
  public String getShortCollegeName() {
    return shortCollegeName;
  }
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }
  public String getReviewId() {
    return reviewId;
  }
  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }
  public String getReviewTitle() {
    return reviewTitle;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }
  public String getUserGender() {
    return userGender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public String getGender() {
    return gender;
  }
  public void setUserGraduate(String userGraduate) {
    this.userGraduate = userGraduate;
  }
  public String getUserGraduate() {
    return userGraduate;
  }
  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }
  public String getCourseName() {
    return courseName;
  }
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  public String getNationality() {
    return nationality;
  }
  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }
  public String getUserImage() {
    return userImage;
  }
  public void setUserImageLarge(String userImageLarge) {
    this.userImageLarge = userImageLarge;
  }
  public String getUserImageLarge() {
    return userImageLarge;
  }
  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }
  public String getOverAllRating() {
    return overAllRating;
  }
  public void setStudCurrentStatus(String studCurrentStatus) {
    this.studCurrentStatus = studCurrentStatus;
  }
  public String getStudCurrentStatus() {
    return studCurrentStatus;
  }
  public void setOverallRatingComment(String overallRatingComment) {
    this.overallRatingComment = overallRatingComment;
  }
  public String getOverallRatingComment() {
    return overallRatingComment;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }
  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }
}
