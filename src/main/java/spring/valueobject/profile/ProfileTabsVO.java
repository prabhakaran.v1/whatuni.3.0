package spring.valueobject.profile;

public class ProfileTabsVO {

  public ProfileTabsVO() {}
  
  //used in DB layed
  private String profileId = "";
  private String myhcProfileId = "";  
  private String sectionId = "";
  private String buttonLabel = "";
  private String cpeQualificationNetworkId = "";
  
  //used in display layer
  private String seoButtonLabel = "";
  private String customButtonLabel = "";
  private String seoCustomButtonLabel = "";
  private String collegeDisplayName = "";

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }

  public String getSectionId() {
    return sectionId;
  }

  public void setButtonLabel(String buttonLabel) {
    this.buttonLabel = buttonLabel;
  }

  public String getButtonLabel() {
    return buttonLabel;
  }

  public void setSeoButtonLabel(String seoButtonLabel) {
    this.seoButtonLabel = seoButtonLabel;
  }

  public String getSeoButtonLabel() {
    return seoButtonLabel;
  }

  public void setCustomButtonLabel(String customButtonLabel) {
    this.customButtonLabel = customButtonLabel;
  }

  public String getCustomButtonLabel() {
    return customButtonLabel;
  }

  public void setSeoCustomButtonLabel(String seoCustomButtonLabel) {
    this.seoCustomButtonLabel = seoCustomButtonLabel;
  }

  public String getSeoCustomButtonLabel() {
    return seoCustomButtonLabel;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }

  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }

}
