package spring.valueobject.prospectus;

public class BulkProspectusVO {

  public BulkProspectusVO() {
  }
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String prospectusPurchaseFlag = "";
  private String websitePurchaseFlag = "";
  boolean collegeChecked = false;
  //CPE related properties
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String profileType = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setProspectusPurchaseFlag(String prospectusPurchaseFlag) {
    this.prospectusPurchaseFlag = prospectusPurchaseFlag;
  }

  public String getProspectusPurchaseFlag() {
    return prospectusPurchaseFlag;
  }

  public void setWebsitePurchaseFlag(String websitePurchaseFlag) {
    this.websitePurchaseFlag = websitePurchaseFlag;
  }

  public String getWebsitePurchaseFlag() {
    return websitePurchaseFlag;
  }

  public void setCollegeChecked(boolean collegeChecked) {
    this.collegeChecked = collegeChecked;
  }

  public boolean isCollegeChecked() {
    return collegeChecked;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
