package spring.valueobject.search;

import java.util.ArrayList;

public class ProviderResultPageVO {
  public ProviderResultPageVO() {
  }
  private String courseId = "";
  private String courseTitle = "";
  private String courseDescription = "";
  private String courseDuration = "";
  private String learndDirQual = "";
  private String costDescription = "";
  private String profile = "";
  private String venueName = "";
  private String postcode = "";
  private String studyMode = "";
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String ucasCode = "";
  private String ucasFlag = "";
  private String ucasPoint = "";
  private String website = "";
  private String seoStudyLevelText = "";
  private String wasThisCourseShortListed = "";
  private String websitePurchaseFlag = "";
  private String scholarshipCount = "";
  private String providerUrl = "";
  private String availableStudyModes = "";
  private String departmentOrFaculty = "";
  //Interaction Button 
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String advertiserflag = "";
  private String myhcProfileId = null;
  private String profileType = null;
  private String profileId = null;
  private String feeDuration = "";
  //
  private String fees = "";
  private String courseDetailUrl = "";
  private String hotLineNo = "";
  private String placesAvailable = "";
  private String lastUpdatedDate = "";
  private String networkId = "";
  private String webformPrice = null;
  private String websitePrice = null;
  private String opportunityId = null;
  
  
  private String totalCount = null;
  private String employmentRate = null;
  private String courseRank = null;
  private String entryRquirements = null;
  private String moduleGroupDetails = null;
  private String collegeOverview = null;
  private String collegeStudentRating = null;
  private String nextOpenDay = null;
  private String collegeLogo = null;
  private String collegeLoaction = null;
  private String opendateDay = null;
  private String opendateDate = null;
  private String opendateMonth = null;
  private String openDayType = null;
  private String spURL = null;
  private String moduleGroupId = null;
  private String moduleTitle = null;
  private String gaCollegeName = null;
  private String uniHomeURL = null;
  private String rankOrdinal = null;
  private String actualRating = null;
  private String jacsSubject = null;
  private String pageName = null;//24_JUN_2014
  private String reviewCount = null;//03_Feb_2015
  private String uniReviewsURL = null;//03_Feb_2015   
  private String addedToFinalChoice = null;
  private ArrayList moduleDetailList = null;//added by priyaa for jan 27th release
  
   private String matchPercentage = null;
   private String locationMatchFlag = null;
   private String locationTypeMatchFlag = null;
   private String studyModeMatchFlag = null;
   private String entryLevelMatchFlag = null;
   private String assessedMatchFlag = null;
   private String reviewSub1MatchFlag = null;
   private String reviewSub2MatchFlag = null;
   private String reviewSub3MatchFlag = null;
   private String qualificationMatchFlag = null;
   private String subjectMatchFlag = null;
   
  private String opendaySuborderItemId = null;
  private String openDate = null;
  private String openMonthYear = null;
  private String bookingUrl = null;
  private String totalOpendays = null;
  
  private String headline = "";  
  private String opendayDesc = "";
  private String startDate = "";
  private String endDate = "";
  private String opendaysProviderURL = "";
  private String opendayVenue = "";
  private String openDayUrl = "";
  private String stOpenDayStartDate = ""; 
  private String stOpenDayEndDate = "";
  private String eventCategoryName = null;
  private String eventCategoryId;
  
  private String courseUcasTariff = null;
  private String showApplyNowButton = null;
  private String showTooltipFlag = null;
  private String applyUrl = null;
  private String matchTypeDim = null;
  
  public String getMatchTypeDim() {
	return matchTypeDim;
}
public void setMatchTypeDim(String matchTypeDim) {
	this.matchTypeDim = matchTypeDim;
}
public String getEventCategoryId() {
	return eventCategoryId;
}
public void setEventCategoryId(String eventCategoryId) {
	this.eventCategoryId = eventCategoryId;
}
public String getEventCategoryName() {
	return eventCategoryName;
  }
  public void setEventCategoryName(String eventCategoryName) {
	this.eventCategoryName = eventCategoryName;
  }
public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCourseDescription(String courseDescription) {
    this.courseDescription = courseDescription;
  }
  public String getCourseDescription() {
    return courseDescription;
  }
  public void setCourseDuration(String courseDuration) {
    this.courseDuration = courseDuration;
  }
  public String getCourseDuration() {
    return courseDuration;
  }
  public void setLearndDirQual(String learndDirQual) {
    this.learndDirQual = learndDirQual;
  }
  public String getLearndDirQual() {
    return learndDirQual;
  }
  public void setCostDescription(String costDescription) {
    this.costDescription = costDescription;
  }
  public String getCostDescription() {
    return costDescription;
  }
  public void setProfile(String profile) {
    this.profile = profile;
  }
  public String getProfile() {
    return profile;
  }
  public void setVenueName(String venueName) {
    this.venueName = venueName;
  }
  public String getVenueName() {
    return venueName;
  }
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }
  public String getPostcode() {
    return postcode;
  }
  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }
  public String getStudyMode() {
    return studyMode;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }
  public String getUcasCode() {
    return ucasCode;
  }
  public void setUcasFlag(String ucasFlag) {
    this.ucasFlag = ucasFlag;
  }
  public String getUcasFlag() {
    return ucasFlag;
  }
  public void setUcasPoint(String ucasPoint) {
    this.ucasPoint = ucasPoint;
  }
  public String getUcasPoint() {
    return ucasPoint;
  }
  public void setWebsite(String website) {
    this.website = website;
  }
  public String getWebsite() {
    return website;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setWasThisCourseShortListed(String wasThisCourseShortListed) {
    this.wasThisCourseShortListed = wasThisCourseShortListed;
  }
  public String getWasThisCourseShortListed() {
    return wasThisCourseShortListed;
  }
  public void setWebsitePurchaseFlag(String websitePurchaseFlag) {
    this.websitePurchaseFlag = websitePurchaseFlag;
  }
  public String getWebsitePurchaseFlag() {
    return websitePurchaseFlag;
  }
  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }
  public String getScholarshipCount() {
    return scholarshipCount;
  }
  public void setProviderUrl(String providerUrl) {
    this.providerUrl = providerUrl;
  }
  public String getProviderUrl() {
    return providerUrl;
  }
  public void setAvailableStudyModes(String availableStudyModes) {
    this.availableStudyModes = availableStudyModes;
  }
  public String getAvailableStudyModes() {
    return availableStudyModes;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setAdvertiserflag(String advertiserflag) {
    this.advertiserflag = advertiserflag;
  }
  public String getAdvertiserflag() {
    return advertiserflag;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setDepartmentOrFaculty(String departmentOrFaculty) {
    this.departmentOrFaculty = departmentOrFaculty;
  }
  public String getDepartmentOrFaculty() {
    return departmentOrFaculty;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setFees(String fees) {
    this.fees = fees;
  }
  public String getFees() {
    return fees;
  }
  public void setCourseDetailUrl(String courseDetailUrl) {
    this.courseDetailUrl = courseDetailUrl;
  }
  public String getCourseDetailUrl() {
    return courseDetailUrl;
  }
    public void setFeeDuration(String feeDuration) {
        this.feeDuration = feeDuration;
    }
    public String getFeeDuration() {
        return feeDuration;
    }

    public void setHotLineNo(String hotLineNo) {
        this.hotLineNo = hotLineNo;
    }

    public String getHotLineNo() {
        return hotLineNo;
    }


  public void setPlacesAvailable(String placesAvailable) {
    this.placesAvailable = placesAvailable;
  }

  public String getPlacesAvailable() {
    return placesAvailable;
  }

  public void setLastUpdatedDate(String lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setOpportunityId(String opportunityId) {
    this.opportunityId = opportunityId;
  }
  public String getOpportunityId() {
    return opportunityId;
  }

  public void setTotalCount(String totalCount)
  {
    this.totalCount = totalCount;
  }

  public String getTotalCount()
  {
    return totalCount;
  }

  public void setEmploymentRate(String employmentRate)
  {
    this.employmentRate = employmentRate;
  }

  public String getEmploymentRate()
  {
    return employmentRate;
  }

  public void setCourseRank(String courseRank)
  {
    this.courseRank = courseRank;
  }

  public String getCourseRank()
  {
    return courseRank;
  }

  public void setEntryRquirements(String entryRquirements)
  {
    this.entryRquirements = entryRquirements;
  }

  public String getEntryRquirements()
  {
    return entryRquirements;
  }

  public void setModuleGroupDetails(String moduleGroupDetails)
  {
    this.moduleGroupDetails = moduleGroupDetails;
  }

  public String getModuleGroupDetails()
  {
    return moduleGroupDetails;
  }

  public void setCollegeOverview(String collegeOverview)
  {
    this.collegeOverview = collegeOverview;
  }

  public String getCollegeOverview()
  {
    return collegeOverview;
  }

  public void setCollegeStudentRating(String collegeStudentRating)
  {
    this.collegeStudentRating = collegeStudentRating;
  }

  public String getCollegeStudentRating()
  {
    return collegeStudentRating;
  }

  public void setNextOpenDay(String nextOpenDay)
  {
    this.nextOpenDay = nextOpenDay;
  }

  public String getNextOpenDay()
  {
    return nextOpenDay;
  }

  public void setCollegeLogo(String collegeLogo)
  {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo()
  {
    return collegeLogo;
  }

  public void setCollegeLoaction(String collegeLoaction)
  {
    this.collegeLoaction = collegeLoaction;
  }

  public String getCollegeLoaction()
  {
    return collegeLoaction;
  }

  public void setOpendateDay(String opendateDay)
  {
    this.opendateDay = opendateDay;
  }

  public String getOpendateDay()
  {
    return opendateDay;
  }

  public void setOpendateDate(String opendateDate)
  {
    this.opendateDate = opendateDate;
  }

  public String getOpendateDate()
  {
    return opendateDate;
  }

  public void setOpendateMonth(String opendateMonth)
  {
    this.opendateMonth = opendateMonth;
  }

  public String getOpendateMonth()
  {
    return opendateMonth;
  }

  public void setMyhcProfileId(String myhcProfileId)
  {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId()
  {
    return myhcProfileId;
  }

  public void setProfileType(String profileType)
  {
    this.profileType = profileType;
  }

  public String getProfileType()
  {
    return profileType;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }

  public String getProfileId()
  {
    return profileId;
  }

  public void setSpURL(String spURL)
  {
    this.spURL = spURL;
  }

  public String getSpURL()
  {
    return spURL;
  }

  public void setModuleGroupId(String moduleGroupId)
  {
    this.moduleGroupId = moduleGroupId;
  }

  public String getModuleGroupId()
  {
    return moduleGroupId;
  }

  public void setModuleTitle(String moduleTitle)
  {
    this.moduleTitle = moduleTitle;
  }

  public String getModuleTitle()
  {
    return moduleTitle;
  }

  public void setGaCollegeName(String gaCollegeName)
  {
    this.gaCollegeName = gaCollegeName;
  }

  public String getGaCollegeName()
  {
    return gaCollegeName;
  }

  public void setUniHomeURL(String uniHomeURL)
  {
    this.uniHomeURL = uniHomeURL;
  }

  public String getUniHomeURL()
  {
    return uniHomeURL;
  }

  public void setRankOrdinal(String rankOrdinal)
  {
    this.rankOrdinal = rankOrdinal;
  }

  public String getRankOrdinal()
  {
    return rankOrdinal;
  }
  public void setActualRating(String actualRating) {
    this.actualRating = actualRating;
  }
  public String getActualRating() {
    return actualRating;
  }
  public void setJacsSubject(String jacsSubject) {
    this.jacsSubject = jacsSubject;
  }
  public String getJacsSubject() {
    return jacsSubject;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setUniReviewsURL(String uniReviewsURL) {
        this.uniReviewsURL = uniReviewsURL;
    }

    public String getUniReviewsURL() {
        return uniReviewsURL;
    }
  public void setAddedToFinalChoice(String addedToFinalChoice) {
    this.addedToFinalChoice = addedToFinalChoice;
  }
  public String getAddedToFinalChoice() {
    return addedToFinalChoice;
  }
  public void setModuleDetailList(ArrayList moduleDetailList) {
    this.moduleDetailList = moduleDetailList;
  }
  public ArrayList getModuleDetailList() {
    return moduleDetailList;
  }
  public void setOpenDayType(String openDayType) {
    this.openDayType = openDayType;
  }
  public String getOpenDayType() {
    return openDayType;
  }
  public void setMatchPercentage(String matchPercentage) {
    this.matchPercentage = matchPercentage;
  }
  public String getMatchPercentage() {
    return matchPercentage;
  }
  public void setLocationMatchFlag(String locationMatchFlag) {
    this.locationMatchFlag = locationMatchFlag;
  }
  public String getLocationMatchFlag() {
    return locationMatchFlag;
  }
  public void setLocationTypeMatchFlag(String locationTypeMatchFlag) {
    this.locationTypeMatchFlag = locationTypeMatchFlag;
  }
  public String getLocationTypeMatchFlag() {
    return locationTypeMatchFlag;
  }
  public void setStudyModeMatchFlag(String studyModeMatchFlag) {
    this.studyModeMatchFlag = studyModeMatchFlag;
  }
  public String getStudyModeMatchFlag() {
    return studyModeMatchFlag;
  }
  public void setEntryLevelMatchFlag(String entryLevelMatchFlag) {
    this.entryLevelMatchFlag = entryLevelMatchFlag;
  }
  public String getEntryLevelMatchFlag() {
    return entryLevelMatchFlag;
  }
  public void setAssessedMatchFlag(String assessedMatchFlag) {
    this.assessedMatchFlag = assessedMatchFlag;
  }
  public String getAssessedMatchFlag() {
    return assessedMatchFlag;
  }
  public void setReviewSub1MatchFlag(String reviewSub1MatchFlag) {
    this.reviewSub1MatchFlag = reviewSub1MatchFlag;
  }
  public String getReviewSub1MatchFlag() {
    return reviewSub1MatchFlag;
  }
  public void setReviewSub2MatchFlag(String reviewSub2MatchFlag) {
    this.reviewSub2MatchFlag = reviewSub2MatchFlag;
  }
  public String getReviewSub2MatchFlag() {
    return reviewSub2MatchFlag;
  }
  public void setReviewSub3MatchFlag(String reviewSub3MatchFlag) {
    this.reviewSub3MatchFlag = reviewSub3MatchFlag;
  }
  public String getReviewSub3MatchFlag() {
    return reviewSub3MatchFlag;
  }
  public void setQualificationMatchFlag(String qualificationMatchFlag) {
    this.qualificationMatchFlag = qualificationMatchFlag;
  }
  public String getQualificationMatchFlag() {
    return qualificationMatchFlag;
  }
  public void setSubjectMatchFlag(String subjectMatchFlag) {
    this.subjectMatchFlag = subjectMatchFlag;
  }
  public String getSubjectMatchFlag() {
    return subjectMatchFlag;
  }
  public void setOpendaySuborderItemId(String opendaySuborderItemId) {
    this.opendaySuborderItemId = opendaySuborderItemId;
  }
  public String getOpendaySuborderItemId() {
    return opendaySuborderItemId;
  }
  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }
  public String getOpenDate() {
    return openDate;
  }
  public void setOpenMonthYear(String openMonthYear) {
    this.openMonthYear = openMonthYear;
  }
  public String getOpenMonthYear() {
    return openMonthYear;
  }
  public void setBookingUrl(String bookingUrl) {
    this.bookingUrl = bookingUrl;
  }
  public String getBookingUrl() {
    return bookingUrl;
  }
  public void setTotalOpendays(String totalOpendays) {
    this.totalOpendays = totalOpendays;
  }
  public String getTotalOpendays() {
    return totalOpendays;
  }

  public void setHeadline(String headline) {
    this.headline = headline;
  }

  public String getHeadline() {
    return headline;
  }

  public void setOpendayDesc(String opendayDesc) {
    this.opendayDesc = opendayDesc;
  }

  public String getOpendayDesc() {
    return opendayDesc;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setOpendaysProviderURL(String opendaysProviderURL) {
    this.opendaysProviderURL = opendaysProviderURL;
  }

  public String getOpendaysProviderURL() {
    return opendaysProviderURL;
  }

  public void setOpendayVenue(String opendayVenue) {
    this.opendayVenue = opendayVenue;
  }

  public String getOpendayVenue() {
    return opendayVenue;
  }

  public void setOpenDayUrl(String openDayUrl) {
    this.openDayUrl = openDayUrl;
  }

  public String getOpenDayUrl() {
    return openDayUrl;
  }

  public void setStOpenDayStartDate(String stOpenDayStartDate) {
    this.stOpenDayStartDate = stOpenDayStartDate;
  }

  public String getStOpenDayStartDate() {
    return stOpenDayStartDate;
  }

  public void setStOpenDayEndDate(String stOpenDayEndDate) {
    this.stOpenDayEndDate = stOpenDayEndDate;
  }

  public String getStOpenDayEndDate() {
    return stOpenDayEndDate;
  }

public String getCourseUcasTariff() {
   return courseUcasTariff;
}

public void setCourseUcasTariff(String courseUcasTariff) {
   this.courseUcasTariff = courseUcasTariff;
}

public String getShowApplyNowButton() {
   return showApplyNowButton;
}

public void setShowApplyNowButton(String showApplyNowButton) {
   this.showApplyNowButton = showApplyNowButton;
}

public String getShowTooltipFlag() {
   return showTooltipFlag;
}

public void setShowTooltipFlag(String showTooltipFlag) {
   this.showTooltipFlag = showTooltipFlag;
}

public String getApplyUrl() {
   return applyUrl;
}

public void setApplyUrl(String applyUrl) {
   this.applyUrl = applyUrl;
}

}
