package spring.valueobject.search;

public class RefineByOptionsVO {

  public RefineByOptionsVO() {
  }
  private String refineOrder = "";
  private String refineCode = "";
  private String refineDesc = "";
  private String categoryId = "";
  private String categoryCode = "";
  private String displaySequence = "";
  private String parentCountryName = "";
  private String numberOfChildLocations = "";
  private String filterURL = "";
  private String categoryName = "";
  private String refineTextKey = "";
  private String browseCatTextKey = "";
  private String optionTextKey = "";
  private String browseCatFlag = "";
  private String browseCatId = "";
  private String browseCatDesc = "";
  private String studyModeDesc = "";
  private String browseCatCode = "";
  private String collegeId = "";
  private String collegeNameDisplay = "";
  private String collegeName = "";
  private String collegeLocation = "";
  private String studyLevel = "";
  
  private String selectedLocationType = null;
  
  private String selectedStudyMode = null;
  //
  private String browseMoneyageLocationRefineUrl = "";
  private String openDayURL = "";
  
  private String location;
  private String locationValue;
  private String locationSelectedFlag;
  private String refineDisplayDesc;

  public String getRefineDisplayDesc() {
    return refineDisplayDesc;
  }

  public void setRefineDisplayDesc(String refineDisplayDesc) {
    this.refineDisplayDesc = refineDisplayDesc;
  }

  public void setRefineOrder(String refineOrder) {
    this.refineOrder = refineOrder;
  }

  public String getRefineOrder() {
    return refineOrder;
  }

  public void setRefineCode(String refineCode) {
    this.refineCode = refineCode;
  }

  public String getRefineCode() {
    return refineCode;
  }

  public void setRefineDesc(String refineDesc) {
    this.refineDesc = refineDesc;
  }

  public String getRefineDesc() {
    return refineDesc;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setParentCountryName(String parentCountryName) {
    this.parentCountryName = parentCountryName;
  }

  public String getParentCountryName() {
    return parentCountryName;
  }

  public void setNumberOfChildLocations(String numberOfChildLocations) {
    this.numberOfChildLocations = numberOfChildLocations;
  }

  public String getNumberOfChildLocations() {
    return numberOfChildLocations;
  }

  public void setDisplaySequence(String displaySequence) {
    this.displaySequence = displaySequence;
  }

  public String getDisplaySequence() {
    return displaySequence;
  }

  public void setBrowseMoneyageLocationRefineUrl(String browseMoneyageLocationRefineUrl) {
    this.browseMoneyageLocationRefineUrl = browseMoneyageLocationRefineUrl;
  }

  public String getBrowseMoneyageLocationRefineUrl() {
    return browseMoneyageLocationRefineUrl;
  }
  public void setFilterURL(String filterURL) {
    this.filterURL = filterURL;
  }
  public String getFilterURL() {
    return filterURL;
  }
  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }
  public String getCategoryName() {
    return categoryName;
  }
  public void setOpenDayURL(String openDayURL) {
    this.openDayURL = openDayURL;
  }
  public String getOpenDayURL() {
    return openDayURL;
  }
  public void setRefineTextKey(String refineTextKey) {
    this.refineTextKey = refineTextKey;
  }
  public String getRefineTextKey() {
    return refineTextKey;
  }
  public void setBrowseCatTextKey(String browseCatTextKey) {
    this.browseCatTextKey = browseCatTextKey;
  }
  public String getBrowseCatTextKey() {
    return browseCatTextKey;
  }
  public void setOptionTextKey(String optionTextKey) {
    this.optionTextKey = optionTextKey;
  }
  public String getOptionTextKey() {
    return optionTextKey;
  }
  public void setBrowseCatFlag(String browseCatFlag) {
    this.browseCatFlag = browseCatFlag;
  }
  public String getBrowseCatFlag() {
    return browseCatFlag;
  }
  public void setBrowseCatId(String browseCatId) {
    this.browseCatId = browseCatId;
  }
  public String getBrowseCatId() {
    return browseCatId;
  }
  public void setBrowseCatDesc(String browseCatDesc) {
    this.browseCatDesc = browseCatDesc;
  }
  public String getBrowseCatDesc() {
    return browseCatDesc;
  }
  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }
  public String getStudyModeDesc() {
    return studyModeDesc;
  }
  public void setBrowseCatCode(String browseCatCode) {
    this.browseCatCode = browseCatCode;
  }
  public String getBrowseCatCode() {
    return browseCatCode;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }
  public String getStudyLevel() {
    return studyLevel;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setSelectedStudyMode(String selectedStudyMode) {
    this.selectedStudyMode = selectedStudyMode;
  }
  public String getSelectedStudyMode() {
    return selectedStudyMode;
  }
  public void setSelectedLocationType(String selectedLocationType) {
    this.selectedLocationType = selectedLocationType;
  }
  public String getSelectedLocationType() {
    return selectedLocationType;
  }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocationValue(String locationValue) {
        this.locationValue = locationValue;
    }

    public String getLocationValue() {
        return locationValue;
    }

    public void setLocationSelectedFlag(String locationSelectedFlag) {
        this.locationSelectedFlag = locationSelectedFlag;
    }

    public String getLocationSelectedFlag() {
        return locationSelectedFlag;
    }
}
