package spring.valueobject.search;

public class OmnitureLoggingVO {
  
  private String commonProp = "";
  
  private String collegeIdsForThisSearch = ""; //commo separated colleges-ids which will be logged into eVar45 to eVar50 = "";
  private String institutionIdsForThisSearch = ""; //commo separated colleges-ids
  
  private String prop45 = "";  
  private String prop46 = "";  
  private String prop47 = "";  
  private String prop48 = "";  
  private String prop49 = "";  
  private String prop50 = "";

  public void setProp45(String prop45) {
    this.prop45 = prop45;
  }

  public String getProp45() {
    return prop45;
  }

  public void setProp46(String prop46) {
    this.prop46 = prop46;
  }

  public String getProp46() {
    return prop46;
  }

  public void setProp47(String prop47) {
    this.prop47 = prop47;
  }

  public String getProp47() {
    return prop47;
  }

  public void setProp48(String prop48) {
    this.prop48 = prop48;
  }

  public String getProp48() {
    return prop48;
  }

  public void setProp49(String prop49) {
    this.prop49 = prop49;
  }

  public String getProp49() {
    return prop49;
  }

  public void setProp50(String prop50) {
    this.prop50 = prop50;
  }

  public String getProp50() {
    return prop50;
  }

  public void setCommonProp(String commonProp) {
    this.commonProp = commonProp;
  }

  public String getCommonProp() {
    return commonProp;
  }

  public void setCollegeIdsForThisSearch(String collegeIdsForThisSearch) {
    this.collegeIdsForThisSearch = collegeIdsForThisSearch;
  }

  public String getCollegeIdsForThisSearch() {
    return collegeIdsForThisSearch;
  }
  public void setInstitutionIdsForThisSearch(String institutionIdsForThisSearch) {
    this.institutionIdsForThisSearch = institutionIdsForThisSearch;
  }
  public String getInstitutionIdsForThisSearch() {
    return institutionIdsForThisSearch;
  }
}
