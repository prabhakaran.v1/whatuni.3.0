package spring.valueobject.search;

public class ReviewCatFilterVO {
  private String reviewCatId = null;
  private String reviewCatName = null;
  private String reviewCatTextKey = null;
  private String reviewSelectText = null;
  private String browseMoneyReviewCatRefineUrl = null;
  public void setReviewCatId(String reviewCatId) {
    this.reviewCatId = reviewCatId;
  }
  public String getReviewCatId() {
    return reviewCatId;
  }
  public void setReviewCatName(String reviewCatName) {
    this.reviewCatName = reviewCatName;
  }
  public String getReviewCatName() {
    return reviewCatName;
  }
  public void setReviewCatTextKey(String reviewCatTextKey) {
    this.reviewCatTextKey = reviewCatTextKey;
  }
  public String getReviewCatTextKey() {
    return reviewCatTextKey;
  }
  public void setReviewSelectText(String reviewSelectText) {
    this.reviewSelectText = reviewSelectText;
  }
  public String getReviewSelectText() {
    return reviewSelectText;
  }
  public void setBrowseMoneyReviewCatRefineUrl(String browseMoneyReviewCatRefineUrl) {
    this.browseMoneyReviewCatRefineUrl = browseMoneyReviewCatRefineUrl;
  }
  public String getBrowseMoneyReviewCatRefineUrl() {
    return browseMoneyReviewCatRefineUrl;
  }
}
