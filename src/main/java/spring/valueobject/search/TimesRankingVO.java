package spring.valueobject.search;

import java.util.ArrayList;

public class TimesRankingVO {

  public TimesRankingVO() {
  }
  private String subjectName = "";
  private String currentRanking = "";
  private String totalRanking = "";
  private String rankOrdinal = "";
  private String uniTimesRanking = "";//9_DEC_2014 Adde by Priyaa For Course dteail page
  private String studentRanking = "";
  private String totalStudentRanking = "";
  private ArrayList timesSubRankingList = null;
  private String timesRanking = null;
  private String thAndRdValue = null;
  private String cugSubjectRank = null;
  //
  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }
  public String getSubjectName() {
    return subjectName;
  }
  public void setCurrentRanking(String currentRanking) {
    this.currentRanking = currentRanking;
  }
  public String getCurrentRanking() {
    return currentRanking;
  }
  public void setTotalRanking(String totalRanking) {
    this.totalRanking = totalRanking;
  }
  public String getTotalRanking() {
    return totalRanking;
  }
  public void setRankOrdinal(String rankOrdinal) {
    this.rankOrdinal = rankOrdinal;
  }
  public String getRankOrdinal() {
    return rankOrdinal;
  }
  public void setUniTimesRanking(String uniTimesRanking) {
    this.uniTimesRanking = uniTimesRanking;
  }
  public String getUniTimesRanking() {
    return uniTimesRanking;
  }
  public void setStudentRanking(String studentRanking) {
    this.studentRanking = studentRanking;
  }
  public String getStudentRanking() {
    return studentRanking;
  }
  public void setTotalStudentRanking(String totalStudentRanking) {
    this.totalStudentRanking = totalStudentRanking;
  }
  public String getTotalStudentRanking() {
    return totalStudentRanking;
  }
  public void setTimesSubRankingList(ArrayList timesSubRankingList) {
    this.timesSubRankingList = timesSubRankingList;
  }
  public ArrayList getTimesSubRankingList() {
    return timesSubRankingList;
  }

    public void setTimesRanking(String timesRanking) {
        this.timesRanking = timesRanking;
    }

    public String getTimesRanking() {
        return timesRanking;
    }

    public void setThAndRdValue(String thAndRdValue) {
        this.thAndRdValue = thAndRdValue;
    }

    public String getThAndRdValue() {
        return thAndRdValue;
    }

    public void setCugSubjectRank(String cugSubjectRank) {
        this.cugSubjectRank = cugSubjectRank;
    }

    public String getCugSubjectRank() {
        return cugSubjectRank;
    }
}
