package spring.valueobject.search;

public class AssessedFilterVO {
  private String assessmentTypeId = null;
  private String assessmentTypeName = null;
  private String selectedAssessment = null;
  private String browseMoneyAssessedRefineUrl = null;
  public void setAssessmentTypeId(String assessmentTypeId) {
    this.assessmentTypeId = assessmentTypeId;
  }
  public String getAssessmentTypeId() {
    return assessmentTypeId;
  }
  public void setAssessmentTypeName(String assessmentTypeName) {
    this.assessmentTypeName = assessmentTypeName;
  }
  public String getAssessmentTypeName() {
    return assessmentTypeName;
  }
  public void setSelectedAssessment(String selectedAssessment) {
    this.selectedAssessment = selectedAssessment;
  }
  public String getSelectedAssessment() {
    return selectedAssessment;
  }
  public void setBrowseMoneyAssessedRefineUrl(String browseMoneyAssessedRefineUrl) {
    this.browseMoneyAssessedRefineUrl = browseMoneyAssessedRefineUrl;
  }
  public String getBrowseMoneyAssessedRefineUrl() {
    return browseMoneyAssessedRefineUrl;
  }
}
