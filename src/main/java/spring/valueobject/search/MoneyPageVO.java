package spring.valueobject.search;

import java.util.ArrayList;

public class MoneyPageVO {

  public MoneyPageVO() {
  }
  //will be used for moneypage mid content.
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String website = "";
  private String numberOfCoursesForThisCollege = "";
  private String learndirQualsForThisCollege = "";//wu312_20110614_Syed: all related qualification fetch to form non link text, PANDA fix      
  private String overallRating = "";
  private String reviewCount = "";
  private String wasThisCollegeShortListed = "";
  private String subjectProfilePurchageFlag = "";
  private String prospectusPurchageFlag = "";
  private String emailPurchageFlag = "";
  private String websitePurchageFlag = "";
  private String scholarshipCount = "";
  private String studentsSatisfaction = "";
  private String studentsJob = "";
  private String collegeProfileFlag = "";
  private String collegeLogo = "";
  private String totalProfileCount = "";
  private String subjectCategoryCode = "";
  private String timesRanking = "";
  private String searchAction = "";
  private String thumbNo = "";
  private String collegeLatitudeAndLongitude = "";
  private String ucasPoint = "";
  private String studentStaffRatio = "";
  private String dropOutRate = "";
  private String studentJob = "";
  private String intStudents = "";
  private String genderRatio = "";
  private String accomodationFee = "";
  private String timesRankingSubject = "";
  //money page region specific uk map related: wu303_20110222
  private String regionName = "";
  //CPE related properties
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String advertName = "";
  private String mediaPath = "";
  private String mediaTypeId = "";
  private String mediaId = "";
  private String thumbnailMediaPath = "";
  private String thumbnailMediaTypeId = "";
  private String interactivityId = "";
  private String whichProfleBoosted = "";
  private String isIpExists = "";
  private String isSpExists = "";
  private String spOrderItemId = "";
  private String ipOrderItemId = "";
  private String mychPrifileIdForSp = "";
  private String mychPrifileIdForIp = "";
  //Button related properties
  //private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  //wu310_20110517_Syed: for PANDA related fix: next-open-day will be shown in money page: 
  private String nextOpenDay = "";
  private ArrayList whatStudentsAreSayingAboutThisCollegeList = null;
  //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
  private ArrayList clearingInfoList = null;

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getWebsite() {
    return website;
  }

  public void setNumberOfCoursesForThisCollege(String numberOfCoursesForThisCollege) {
    this.numberOfCoursesForThisCollege = numberOfCoursesForThisCollege;
  }

  public String getNumberOfCoursesForThisCollege() {
    return numberOfCoursesForThisCollege;
  }

  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }

  public String getOverallRating() {
    return overallRating;
  }

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setWasThisCollegeShortListed(String wasThisCollegeShortListed) {
    this.wasThisCollegeShortListed = wasThisCollegeShortListed;
  }

  public String getWasThisCollegeShortListed() {
    return wasThisCollegeShortListed;
  }

  public void setSubjectProfilePurchageFlag(String subjectProfilePurchageFlag) {
    this.subjectProfilePurchageFlag = subjectProfilePurchageFlag;
  }

  public String getSubjectProfilePurchageFlag() {
    return subjectProfilePurchageFlag;
  }

  public void setProspectusPurchageFlag(String prospectusPurchageFlag) {
    this.prospectusPurchageFlag = prospectusPurchageFlag;
  }

  public String getProspectusPurchageFlag() {
    return prospectusPurchageFlag;
  }

  public void setEmailPurchageFlag(String emailPurchageFlag) {
    this.emailPurchageFlag = emailPurchageFlag;
  }

  public String getEmailPurchageFlag() {
    return emailPurchageFlag;
  }

  public void setWebsitePurchageFlag(String websitePurchageFlag) {
    this.websitePurchageFlag = websitePurchageFlag;
  }

  public String getWebsitePurchageFlag() {
    return websitePurchageFlag;
  }

  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }

  public String getScholarshipCount() {
    return scholarshipCount;
  }

  public void setStudentsSatisfaction(String studentsSatisfaction) {
    this.studentsSatisfaction = studentsSatisfaction;
  }

  public String getStudentsSatisfaction() {
    return studentsSatisfaction;
  }

  public void setStudentsJob(String studentsJob) {
    this.studentsJob = studentsJob;
  }

  public String getStudentsJob() {
    return studentsJob;
  }

  public void setCollegeProfileFlag(String collegeProfileFlag) {
    this.collegeProfileFlag = collegeProfileFlag;
  }

  public String getCollegeProfileFlag() {
    return collegeProfileFlag;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setTotalProfileCount(String totalProfileCount) {
    this.totalProfileCount = totalProfileCount;
  }

  public String getTotalProfileCount() {
    return totalProfileCount;
  }

  public void setSubjectCategoryCode(String subjectCategoryCode) {
    this.subjectCategoryCode = subjectCategoryCode;
  }

  public String getSubjectCategoryCode() {
    return subjectCategoryCode;
  }

  public void setTimesRanking(String timesRanking) {
    this.timesRanking = timesRanking;
  }

  public String getTimesRanking() {
    return timesRanking;
  }

  public void setSearchAction(String searchAction) {
    this.searchAction = searchAction;
  }

  public String getSearchAction() {
    return searchAction;
  }

  public void setThumbNo(String thumbNo) {
    this.thumbNo = thumbNo;
  }

  public String getThumbNo() {
    return thumbNo;
  }

  public void setCollegeLatitudeAndLongitude(String collegeLatitudeAndLongitude) {
    this.collegeLatitudeAndLongitude = collegeLatitudeAndLongitude;
  }

  public String getCollegeLatitudeAndLongitude() {
    return collegeLatitudeAndLongitude;
  }

  public void setUcasPoint(String ucasPoint) {
    this.ucasPoint = ucasPoint;
  }

  public String getUcasPoint() {
    return ucasPoint;
  }

  public void setStudentStaffRatio(String studentStaffRatio) {
    this.studentStaffRatio = studentStaffRatio;
  }

  public String getStudentStaffRatio() {
    return studentStaffRatio;
  }

  public void setDropOutRate(String dropOutRate) {
    this.dropOutRate = dropOutRate;
  }

  public String getDropOutRate() {
    return dropOutRate;
  }

  public void setStudentJob(String studentJob) {
    this.studentJob = studentJob;
  }

  public String getStudentJob() {
    return studentJob;
  }

  public void setIntStudents(String intStudents) {
    this.intStudents = intStudents;
  }

  public String getIntStudents() {
    return intStudents;
  }

  public void setGenderRatio(String genderRatio) {
    this.genderRatio = genderRatio;
  }

  public String getGenderRatio() {
    return genderRatio;
  }

  public void setAccomodationFee(String accomodationFee) {
    this.accomodationFee = accomodationFee;
  }

  public String getAccomodationFee() {
    return accomodationFee;
  }

  public void setTimesRankingSubject(String timesRankingSubject) {
    this.timesRankingSubject = timesRankingSubject;
  }

  public String getTimesRankingSubject() {
    return timesRankingSubject;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setThumbnailMediaPath(String thumbnailMediaPath) {
    this.thumbnailMediaPath = thumbnailMediaPath;
  }

  public String getThumbnailMediaPath() {
    return thumbnailMediaPath;
  }

  public void setThumbnailMediaTypeId(String thumbnailMediaTypeId) {
    this.thumbnailMediaTypeId = thumbnailMediaTypeId;
  }

  public String getThumbnailMediaTypeId() {
    return thumbnailMediaTypeId;
  }

  public void setInteractivityId(String interactivityId) {
    this.interactivityId = interactivityId;
  }

  public String getInteractivityId() {
    return interactivityId;
  }

  public void setWhichProfleBoosted(String whichProfleBoosted) {
    this.whichProfleBoosted = whichProfleBoosted;
  }

  public String getWhichProfleBoosted() {
    return whichProfleBoosted;
  }

  public void setIsIpExists(String isIpExists) {
    this.isIpExists = isIpExists;
  }

  public String getIsIpExists() {
    return isIpExists;
  }

  public void setIsSpExists(String isSpExists) {
    this.isSpExists = isSpExists;
  }

  public String getIsSpExists() {
    return isSpExists;
  }

  public void setSpOrderItemId(String spOrderItemId) {
    this.spOrderItemId = spOrderItemId;
  }

  public String getSpOrderItemId() {
    return spOrderItemId;
  }

  public void setIpOrderItemId(String ipOrderItemId) {
    this.ipOrderItemId = ipOrderItemId;
  }

  public String getIpOrderItemId() {
    return ipOrderItemId;
  }

  public void setMychPrifileIdForSp(String mychPrifileIdForSp) {
    this.mychPrifileIdForSp = mychPrifileIdForSp;
  }

  public String getMychPrifileIdForSp() {
    return mychPrifileIdForSp;
  }

  public void setMychPrifileIdForIp(String mychPrifileIdForIp) {
    this.mychPrifileIdForIp = mychPrifileIdForIp;
  }

  public String getMychPrifileIdForIp() {
    return mychPrifileIdForIp;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setNextOpenDay(String nextOpenDay) {
    this.nextOpenDay = nextOpenDay;
  }

  public String getNextOpenDay() {
    return nextOpenDay;
  }

  public void setWhatStudentsAreSayingAboutThisCollegeList(ArrayList whatStudentsAreSayingAboutThisCollegeList) {
    this.whatStudentsAreSayingAboutThisCollegeList = whatStudentsAreSayingAboutThisCollegeList;
  }

  public ArrayList getWhatStudentsAreSayingAboutThisCollegeList() {
    return whatStudentsAreSayingAboutThisCollegeList;
  }

  public void setLearndirQualsForThisCollege(String learndirQualsForThisCollege) {
    this.learndirQualsForThisCollege = learndirQualsForThisCollege;
  }

  public String getLearndirQualsForThisCollege() {
    return learndirQualsForThisCollege;
  }

  public void setClearingInfoList(ArrayList clearingInfoList) {
    this.clearingInfoList = clearingInfoList;
  }

  public ArrayList getClearingInfoList() {
    return clearingInfoList;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
