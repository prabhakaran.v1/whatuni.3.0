package spring.valueobject.search;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class FeaturedBrandVO {
  private String collegeId = null;
  private String collegeName = null;
  private String mediaId = null;
  private String mediaPath = null;
  private String thumbnailPath = null;
  private String mediaType = null;
  private String headline = null;
  private String profileHeadlineUrl = null;
  private String tagline = null;
  private String reviewDisplayflag = null;
  private String reviewCount = null;
  private String overallRating = null;
  private String overallRatingExact = null;
  private String navigationText = null;
  private String navigationUrl = null;
  private String uniReviewUrl = null;
  private String orderItemId = null;

}