package spring.valueobject.blog;

import java.util.ArrayList;

public class BlogVO {

  //Blog related details
  private String blogId = "";
  private String blogName = "";
  private String blogUrlSeoName = "";//
  private String blogDescription = "";
  private String blogMediaId = "";
  private String blogMediaType = "";
  private String blogMediaPath = "";
  private String blogMediaThumbPath = "";
  private String totalPostForThisBlog= "";  
  private ArrayList listOfPostsBelongsToThisBlog = null;
  
  //Author related details
  private String authorId = "";
  private String authorName = "";//
  private String authorShortBio = "";
  private String authorMediaId = "";
  private String authorMediaType = "";
  private String authorMediaPath = "";//
  private String authorMediaThumbPath = "";
  
  //Post related details
  private String postUrl = ""; //this will be populated using given parameters
  private String postUrlSeoTitle = "";//
  
  private String postId = "";  //
  private String postHeading = "";//  
  private String postShortDescription = "";//
  private String postUpdatedDate = "";//
  private String postMediaId = "";
  private String postMediaType = "";
  private String postMediaPath = "";
  private String postMediaThumbPath = "";

  public void setBlogId(String blogId) {
    this.blogId = blogId;
  }

  public String getBlogId() {
    return blogId;
  }

  public void setBlogDescription(String blogDescription) {
    this.blogDescription = blogDescription;
  }

  public String getBlogDescription() {
    return blogDescription;
  }

  public void setBlogMediaId(String blogMediaId) {
    this.blogMediaId = blogMediaId;
  }

  public String getBlogMediaId() {
    return blogMediaId;
  }

  public void setBlogMediaType(String blogMediaType) {
    this.blogMediaType = blogMediaType;
  }

  public String getBlogMediaType() {
    return blogMediaType;
  }

  public void setBlogMediaPath(String blogMediaPath) {
    this.blogMediaPath = blogMediaPath;
  }

  public String getBlogMediaPath() {
    return blogMediaPath;
  }

  public void setBlogMediaThumbPath(String blogMediaThumbPath) {
    this.blogMediaThumbPath = blogMediaThumbPath;
  }

  public String getBlogMediaThumbPath() {
    return blogMediaThumbPath;
  }

  public void setAuthorId(String authorId) {
    this.authorId = authorId;
  }

  public String getAuthorId() {
    return authorId;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorShortBio(String authorShortBio) {
    this.authorShortBio = authorShortBio;
  }

  public String getAuthorShortBio() {
    return authorShortBio;
  }

  public void setAuthorMediaId(String authorMediaId) {
    this.authorMediaId = authorMediaId;
  }

  public String getAuthorMediaId() {
    return authorMediaId;
  }

  public void setAuthorMediaType(String authorMediaType) {
    this.authorMediaType = authorMediaType;
  }

  public String getAuthorMediaType() {
    return authorMediaType;
  }

  public void setAuthorMediaPath(String authorMediaPath) {
    this.authorMediaPath = authorMediaPath;
  }

  public String getAuthorMediaPath() {
    return authorMediaPath;
  }

  public void setAuthorMediaThumbPath(String authorMediaThumbPath) {
    this.authorMediaThumbPath = authorMediaThumbPath;
  }

  public String getAuthorMediaThumbPath() {
    return authorMediaThumbPath;
  }

  public void setPostId(String postId) {
    this.postId = postId;
  }

  public String getPostId() {
    return postId;
  }

  public void setPostHeading(String postHeading) {
    this.postHeading = postHeading;
  }

  public String getPostHeading() {
    return postHeading;
  }

  public void setPostUrlSeoTitle(String postUrlSeoTitle) {
    this.postUrlSeoTitle = postUrlSeoTitle;
  }

  public String getPostUrlSeoTitle() {
    return postUrlSeoTitle;
  }

  public void setPostUpdatedDate(String postUpdatedDate) {
    this.postUpdatedDate = postUpdatedDate;
  }

  public String getPostUpdatedDate() {
    return postUpdatedDate;
  }

  public void setPostMediaId(String postMediaId) {
    this.postMediaId = postMediaId;
  }

  public String getPostMediaId() {
    return postMediaId;
  }

  public void setPostMediaType(String postMediaType) {
    this.postMediaType = postMediaType;
  }

  public String getPostMediaType() {
    return postMediaType;
  }

  public void setPostMediaPath(String postMediaPath) {
    this.postMediaPath = postMediaPath;
  }

  public String getPostMediaPath() {
    return postMediaPath;
  }

  public void setPostMediaThumbPath(String postMediaThumbPath) {
    this.postMediaThumbPath = postMediaThumbPath;
  }

  public String getPostMediaThumbPath() {
    return postMediaThumbPath;
  }

  public void setPostShortDescription(String postShortDescription) {
    this.postShortDescription = postShortDescription;
  }

  public String getPostShortDescription() {
    return postShortDescription;
  }

  public void setPostUrl(String postUrl) {
    this.postUrl = postUrl;
  }

  public String getPostUrl() {
    return postUrl;
  }

  public void setTotalPostForThisBlog(String totalPostForThisBlog) {
    this.totalPostForThisBlog = totalPostForThisBlog;
  }

  public String getTotalPostForThisBlog() {
    return totalPostForThisBlog;
  }

  public void setBlogName(String blogName) {
    this.blogName = blogName;
  }

  public String getBlogName() {
    return blogName;
  }

  public void setBlogUrlSeoName(String blogUrlSeoName) {
    this.blogUrlSeoName = blogUrlSeoName;
  }

  public String getBlogUrlSeoName() {
    return blogUrlSeoName;
  }

  public void setListOfPostsBelongsToThisBlog(ArrayList listOfPostsBelongsToThisBlog) {
    this.listOfPostsBelongsToThisBlog = listOfPostsBelongsToThisBlog;
  }

  public ArrayList getListOfPostsBelongsToThisBlog() {
    return listOfPostsBelongsToThisBlog;
  }

} //end of class

