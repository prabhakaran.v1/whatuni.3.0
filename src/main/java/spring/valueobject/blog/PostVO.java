package spring.valueobject.blog;

public class PostVO {
  //Blog related details
  private String blogId = "";
  private String blogHeading = "";
  private String blogUrlSeoName = "";
  private String blogDescription = "";
  private String blogMediaId = "";
  private String blogMediaType = "";
  private String blogMediaPath = "";
  private String blogMediaThumbPath = "";
  //Author related details
  private String authorId = "";
  private String authorName = ""; //
  private String authorShortBio = "";
  private String authorMediaId = "";
  private String authorMediaType = "";
  private String authorMediaPath = ""; //
  private String authorMediaThumbPath = "";
  //Post related details
  private String postId = ""; //
  private String postHeading = ""; //  
  private String postShortDescription = ""; //
  private String postFullDescription = ""; //
  private String postUpdatedDate = ""; //  
  //post seo details
  private String postUrl = "";
  private String postUrlSeoTitle = ""; //
  private String postMetaTitle = "";
  private String postMetaDescription = "";
  private String postMetaKeywords = "";
  //post other details
  private String postLdcsCode = "";
  private String postLdcsDesc = "";
  private String postBlogQualId = "";
  private String postBlogQualDesc = "";
  private String postNetworkQualId = "";
  private String postNetworkQualDesc = "";
  private String postBlogCategoryId = "";
  private String postBlogCategoryDesc = "";
  //post media details
  private String postMediaId = "";
  private String postMediaType = "";
  private String postMediaPath = "";
  private String postMediaThumbPath = "";
  //post' next & previous details
  private String nextPostUrlSeoTitle = "";
  private String previousPostUrlSeoTitle = "";
  private String nextPostUrl = "";
  private String previousPostUrl = "";

  public void setBlogId(String blogId) {
    this.blogId = blogId;
  }

  public String getBlogId() {
    return blogId;
  }

  public void setBlogHeading(String blogHeading) {
    this.blogHeading = blogHeading;
  }

  public String getBlogHeading() {
    return blogHeading;
  }

  public void setBlogDescription(String blogDescription) {
    this.blogDescription = blogDescription;
  }

  public String getBlogDescription() {
    return blogDescription;
  }

  public void setBlogMediaId(String blogMediaId) {
    this.blogMediaId = blogMediaId;
  }

  public String getBlogMediaId() {
    return blogMediaId;
  }

  public void setBlogMediaType(String blogMediaType) {
    this.blogMediaType = blogMediaType;
  }

  public String getBlogMediaType() {
    return blogMediaType;
  }

  public void setBlogMediaPath(String blogMediaPath) {
    this.blogMediaPath = blogMediaPath;
  }

  public String getBlogMediaPath() {
    return blogMediaPath;
  }

  public void setBlogMediaThumbPath(String blogMediaThumbPath) {
    this.blogMediaThumbPath = blogMediaThumbPath;
  }

  public String getBlogMediaThumbPath() {
    return blogMediaThumbPath;
  }

  public void setAuthorId(String authorId) {
    this.authorId = authorId;
  }

  public String getAuthorId() {
    return authorId;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorShortBio(String authorShortBio) {
    this.authorShortBio = authorShortBio;
  }

  public String getAuthorShortBio() {
    return authorShortBio;
  }

  public void setAuthorMediaId(String authorMediaId) {
    this.authorMediaId = authorMediaId;
  }

  public String getAuthorMediaId() {
    return authorMediaId;
  }

  public void setAuthorMediaType(String authorMediaType) {
    this.authorMediaType = authorMediaType;
  }

  public String getAuthorMediaType() {
    return authorMediaType;
  }

  public void setAuthorMediaPath(String authorMediaPath) {
    this.authorMediaPath = authorMediaPath;
  }

  public String getAuthorMediaPath() {
    return authorMediaPath;
  }

  public void setAuthorMediaThumbPath(String authorMediaThumbPath) {
    this.authorMediaThumbPath = authorMediaThumbPath;
  }

  public String getAuthorMediaThumbPath() {
    return authorMediaThumbPath;
  }

  public void setPostId(String postId) {
    this.postId = postId;
  }

  public String getPostId() {
    return postId;
  }

  public void setPostHeading(String postHeading) {
    this.postHeading = postHeading;
  }

  public String getPostHeading() {
    return postHeading;
  }

  public void setPostShortDescription(String postShortDescription) {
    this.postShortDescription = postShortDescription;
  }

  public String getPostShortDescription() {
    return postShortDescription;
  }

  public void setPostFullDescription(String postFullDescription) {
    this.postFullDescription = postFullDescription;
  }

  public String getPostFullDescription() {
    return postFullDescription;
  }

  public void setPostUpdatedDate(String postUpdatedDate) {
    this.postUpdatedDate = postUpdatedDate;
  }

  public String getPostUpdatedDate() {
    return postUpdatedDate;
  }

  public void setPostUrl(String postUrl) {
    this.postUrl = postUrl;
  }

  public String getPostUrl() {
    return postUrl;
  }

  public void setPostUrlSeoTitle(String postUrlSeoTitle) {
    this.postUrlSeoTitle = postUrlSeoTitle;
  }

  public String getPostUrlSeoTitle() {
    return postUrlSeoTitle;
  }

  public void setPostMetaTitle(String postMetaTitle) {
    this.postMetaTitle = postMetaTitle;
  }

  public String getPostMetaTitle() {
    return postMetaTitle;
  }

  public void setPostMetaDescription(String postMetaDescription) {
    this.postMetaDescription = postMetaDescription;
  }

  public String getPostMetaDescription() {
    return postMetaDescription;
  }

  public void setPostMetaKeywords(String postMetaKeywords) {
    this.postMetaKeywords = postMetaKeywords;
  }

  public String getPostMetaKeywords() {
    return postMetaKeywords;
  }

  public void setPostLdcsCode(String postLdcsCode) {
    this.postLdcsCode = postLdcsCode;
  }

  public String getPostLdcsCode() {
    return postLdcsCode;
  }

  public void setPostLdcsDesc(String postLdcsDesc) {
    this.postLdcsDesc = postLdcsDesc;
  }

  public String getPostLdcsDesc() {
    return postLdcsDesc;
  }

  public void setPostBlogQualId(String postBlogQualId) {
    this.postBlogQualId = postBlogQualId;
  }

  public String getPostBlogQualId() {
    return postBlogQualId;
  }

  public void setPostBlogQualDesc(String postBlogQualDesc) {
    this.postBlogQualDesc = postBlogQualDesc;
  }

  public String getPostBlogQualDesc() {
    return postBlogQualDesc;
  }

  public void setPostNetworkQualId(String postNetworkQualId) {
    this.postNetworkQualId = postNetworkQualId;
  }

  public String getPostNetworkQualId() {
    return postNetworkQualId;
  }

  public void setPostNetworkQualDesc(String postNetworkQualDesc) {
    this.postNetworkQualDesc = postNetworkQualDesc;
  }

  public String getPostNetworkQualDesc() {
    return postNetworkQualDesc;
  }

  public void setPostBlogCategoryId(String postBlogCategoryId) {
    this.postBlogCategoryId = postBlogCategoryId;
  }

  public String getPostBlogCategoryId() {
    return postBlogCategoryId;
  }

  public void setPostBlogCategoryDesc(String postBlogCategoryDesc) {
    this.postBlogCategoryDesc = postBlogCategoryDesc;
  }

  public String getPostBlogCategoryDesc() {
    return postBlogCategoryDesc;
  }

  public void setPostMediaId(String postMediaId) {
    this.postMediaId = postMediaId;
  }

  public String getPostMediaId() {
    return postMediaId;
  }

  public void setPostMediaType(String postMediaType) {
    this.postMediaType = postMediaType;
  }

  public String getPostMediaType() {
    return postMediaType;
  }

  public void setPostMediaPath(String postMediaPath) {
    this.postMediaPath = postMediaPath;
  }

  public String getPostMediaPath() {
    return postMediaPath;
  }

  public void setPostMediaThumbPath(String postMediaThumbPath) {
    this.postMediaThumbPath = postMediaThumbPath;
  }

  public String getPostMediaThumbPath() {
    return postMediaThumbPath;
  }

  public void setNextPostUrlSeoTitle(String nextPostUrlSeoTitle) {
    this.nextPostUrlSeoTitle = nextPostUrlSeoTitle;
  }

  public String getNextPostUrlSeoTitle() {
    return nextPostUrlSeoTitle;
  }

  public void setPreviousPostUrlSeoTitle(String previousPostUrlSeoTitle) {
    this.previousPostUrlSeoTitle = previousPostUrlSeoTitle;
  }

  public String getPreviousPostUrlSeoTitle() {
    return previousPostUrlSeoTitle;
  }

  public void setNextPostUrl(String nextPostUrl) {
    this.nextPostUrl = nextPostUrl;
  }

  public String getNextPostUrl() {
    return nextPostUrl;
  }

  public void setPreviousPostUrl(String previousPostUrl) {
    this.previousPostUrl = previousPostUrl;
  }

  public String getPreviousPostUrl() {
    return previousPostUrl;
  }

  public void setBlogUrlSeoName(String blogUrlSeoName) {
    this.blogUrlSeoName = blogUrlSeoName;
  }

  public String getBlogUrlSeoName() {
    return blogUrlSeoName;
  }

}
