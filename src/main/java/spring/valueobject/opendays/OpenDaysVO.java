package spring.valueobject.opendays;

public class OpenDaysVO {
    
  private String openDate = "";
  private String opendayDetails = "";
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeLogo = "";
  private String collegeLocation = "";
  private String totalCount = "";


    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpendayDetails(String opendayDetails) {
        this.opendayDetails = opendayDetails;
    }

    public String getOpendayDetails() {
        return opendayDetails;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeNameDisplay(String collegeNameDisplay) {
        this.collegeNameDisplay = collegeNameDisplay;
    }

    public String getCollegeNameDisplay() {
        return collegeNameDisplay;
    }

    public void setCollegeLogo(String collegeLogo) {
        this.collegeLogo = collegeLogo;
    }

    public String getCollegeLogo() {
        return collegeLogo;
    }

    public void setCollegeLocation(String collegeLocation) {
        this.collegeLocation = collegeLocation;
    }

    public String getCollegeLocation() {
        return collegeLocation;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getTotalCount() {
        return totalCount;
    }
}
