package spring.valueobject.common;

public class YearOfEntryVO {

  private String whatYear;
  private String yearofEntry;

  public void setWhatYear(String whatYear) {
    this.whatYear = whatYear;
  }

  public String getWhatYear() {
    return whatYear;
  }

  public void setYearofEntry(String yearofEntry) {
    this.yearofEntry = yearofEntry;
  }

  public String getYearofEntry() {
    return yearofEntry;
  }

}
