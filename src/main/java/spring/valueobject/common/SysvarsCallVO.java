package spring.valueobject.common;

import lombok.Data;

/**
 * @SysvarCallsVO Used for SYSVAR call details.
 * @author Sri Sankari R
 * @version 1.0
 * @since 19/01/2021
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 *                                                       
 */
@Data
public class SysvarsCallVO {
  
  private String name = null;
  private String value = null;
}