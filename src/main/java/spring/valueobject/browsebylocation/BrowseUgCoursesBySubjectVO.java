package spring.valueobject.browsebylocation;


public class BrowseUgCoursesBySubjectVO {
  //
  private String subjectId = "";
  private String subjectCode = "";
  private String subjectDesc = "";
  private String totalSubjectCountForThisLocation = "";
  //  
  private String location = "";
  private String linkToBrowseMoneyPage = "";
  //

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectCode(String subjectCode) {
    this.subjectCode = subjectCode;
  }

  public String getSubjectCode() {
    return subjectCode;
  }

  public void setSubjectDesc(String subjectDesc) {
    this.subjectDesc = subjectDesc;
  }

  public String getSubjectDesc() {
    return subjectDesc;
  }

  public void setLinkToBrowseMoneyPage(String linkToBrowseMoneyPage) {
    this.linkToBrowseMoneyPage = linkToBrowseMoneyPage;
  }

  public String getLinkToBrowseMoneyPage() {
    return linkToBrowseMoneyPage;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLocation() {
    return location;
  }

  public void setTotalSubjectCountForThisLocation(String totalSubjectCountForThisLocation) {
    this.totalSubjectCountForThisLocation = totalSubjectCountForThisLocation;
  }

  public String getTotalSubjectCountForThisLocation() {
    return totalSubjectCountForThisLocation;
  }

}
