package spring.valueobject.browsebylocation;

import java.util.ArrayList;

public class BrowseUgCoursesByLocationVO {
  //
  private String countryName = "";
  private String countryUrlToSubjectListingPage = "";
  //
  private String childLocationCount = "";
  private ArrayList listOfChildLocations = null;

  public void setChildLocationCount(String childLocationCount) {
    this.childLocationCount = childLocationCount;
  }

  public String getChildLocationCount() {
    return childLocationCount;
  }

  public void setListOfChildLocations(ArrayList listOfChildLocations) {
    this.listOfChildLocations = listOfChildLocations;
  }

  public ArrayList getListOfChildLocations() {
    return listOfChildLocations;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryUrlToSubjectListingPage(String countryUrlToSubjectListingPage) {
    this.countryUrlToSubjectListingPage = countryUrlToSubjectListingPage;
  }

  public String getCountryUrlToSubjectListingPage() {
    return countryUrlToSubjectListingPage;
  }

}
