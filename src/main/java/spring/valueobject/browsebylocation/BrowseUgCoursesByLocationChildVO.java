package spring.valueobject.browsebylocation;

public class BrowseUgCoursesByLocationChildVO {
  //
  private String parentCountryName = "";
  //
  private String childLocationName = "";
  private String childLocationUrlToSubjectListingPage = "";
  private String childLocationCount = "";

  public void setChildLocationName(String childLocationName) {
    this.childLocationName = childLocationName;
  }

  public String getChildLocationName() {
    return childLocationName;
  }

  public void setChildLocationCount(String childLocationCount) {
    this.childLocationCount = childLocationCount;
  }

  public String getChildLocationCount() {
    return childLocationCount;
  }

  public void setParentCountryName(String parentCountryName) {
    this.parentCountryName = parentCountryName;
  }

  public String getParentCountryName() {
    return parentCountryName;
  }

  public void setChildLocationUrlToSubjectListingPage(String childLocationUrlToSubjectListingPage) {
    this.childLocationUrlToSubjectListingPage = childLocationUrlToSubjectListingPage;
  }

  public String getChildLocationUrlToSubjectListingPage() {
    return childLocationUrlToSubjectListingPage;
  }

}
