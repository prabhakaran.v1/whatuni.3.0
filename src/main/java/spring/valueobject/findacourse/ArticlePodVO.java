package spring.valueobject.findacourse;

import lombok.Getter;
import lombok.Setter;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ArticlePodVO.java
 * Description   : Getting articles list related content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author                  Ver              Modified On      Modification Details    Change
 * *************************************************************************************************************************************
 * Keerthana.E             1.0            08-DEC-2020        Added article title, media path, text, url and display order for article pod
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Getter @Setter
public class ArticlePodVO {
  
  private String articleTitle = null;
  private String mediaPath = null;
  private String personalizedText = null;
  private String url = null;
  private String displayOrder = null;

}
