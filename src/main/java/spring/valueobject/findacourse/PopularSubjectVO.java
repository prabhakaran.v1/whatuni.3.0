package spring.valueobject.findacourse;

import lombok.Getter;
import lombok.Setter;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : PopularSubjectVO.java
 * Description   : Getting popular subjects related content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author                  Ver              Modified On     Modification Details    Change
 * *************************************************************************************************************************************
 * Keerthana.E             1.0            08-DEC-2020        Added subject name and subject url for popular subjects
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Getter @Setter
public class PopularSubjectVO {
  
  private String subjectName = null;
  private String subjectUrl = null;

}
