package spring.valueobject.articles;

import java.util.ArrayList;

public class ArticleGroupVO {
  //Blog related details
  private String articleGroupId = "";
  private String articleGroupName = "";
  private String articleGroupUrlSeoName = ""; //
  private String articleGroupDescription = "";
  private String articleGroupMediaId = "";
  private String articleGroupMediaType = "";
  private String articleGroupMediaPath = "";
  private String articleGroupMediaThumbPath = "";
  private String totalArticlesForThisArticleGroup = "";
  private ArrayList listOfArticlesBelongsToThisArticleGroup = null;
  //Author related details
  private String authorId = "";
  private String authorName = ""; //
  private String authorShortBio = "";
  private String authorMediaId = "";
  private String authorMediaType = "";
  private String authorMediaPath = ""; //
  private String authorMediaThumbPath = "";
  //article related details
  private String articleUrl = ""; //this will be populated using given parameters
  private String articleUrlSeoTitle = ""; //  
  private String articleId = ""; //
  private String articleHeading = ""; //  
  private String articleShortDescription = ""; //
  private String articleUpdatedDate = ""; //
  private String articleMediaId = "";
  private String articleMediaType = "";
  private String articleMediaPath = "";
  private String articleMediaThumbPath = "";

  public void setArticleGroupId(String articleGroupId) {
    this.articleGroupId = articleGroupId;
  }

  public String getArticleGroupId() {
    return articleGroupId;
  }

  public void setArticleGroupName(String articleGroupName) {
    this.articleGroupName = articleGroupName;
  }

  public String getArticleGroupName() {
    return articleGroupName;
  }

  public void setArticleGroupUrlSeoName(String articleGroupUrlSeoName) {
    this.articleGroupUrlSeoName = articleGroupUrlSeoName;
  }

  public String getArticleGroupUrlSeoName() {
    return articleGroupUrlSeoName;
  }

  public void setArticleGroupDescription(String articleGroupDescription) {
    this.articleGroupDescription = articleGroupDescription;
  }

  public String getArticleGroupDescription() {
    return articleGroupDescription;
  }

  public void setArticleGroupMediaId(String articleGroupMediaId) {
    this.articleGroupMediaId = articleGroupMediaId;
  }

  public String getArticleGroupMediaId() {
    return articleGroupMediaId;
  }

  public void setArticleGroupMediaType(String articleGroupMediaType) {
    this.articleGroupMediaType = articleGroupMediaType;
  }

  public String getArticleGroupMediaType() {
    return articleGroupMediaType;
  }

  public void setArticleGroupMediaPath(String articleGroupMediaPath) {
    this.articleGroupMediaPath = articleGroupMediaPath;
  }

  public String getArticleGroupMediaPath() {
    return articleGroupMediaPath;
  }

  public void setArticleGroupMediaThumbPath(String articleGroupMediaThumbPath) {
    this.articleGroupMediaThumbPath = articleGroupMediaThumbPath;
  }

  public String getArticleGroupMediaThumbPath() {
    return articleGroupMediaThumbPath;
  }

  public void setTotalArticlesForThisArticleGroup(String totalArticlesForThisArticleGroup) {
    this.totalArticlesForThisArticleGroup = totalArticlesForThisArticleGroup;
  }

  public String getTotalArticlesForThisArticleGroup() {
    return totalArticlesForThisArticleGroup;
  }

  public void setListOfArticlesBelongsToThisArticleGroup(ArrayList listOfArticlesBelongsToThisArticleGroup) {
    this.listOfArticlesBelongsToThisArticleGroup = listOfArticlesBelongsToThisArticleGroup;
  }

  public ArrayList getListOfArticlesBelongsToThisArticleGroup() {
    return listOfArticlesBelongsToThisArticleGroup;
  }

  public void setAuthorId(String authorId) {
    this.authorId = authorId;
  }

  public String getAuthorId() {
    return authorId;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorShortBio(String authorShortBio) {
    this.authorShortBio = authorShortBio;
  }

  public String getAuthorShortBio() {
    return authorShortBio;
  }

  public void setAuthorMediaId(String authorMediaId) {
    this.authorMediaId = authorMediaId;
  }

  public String getAuthorMediaId() {
    return authorMediaId;
  }

  public void setAuthorMediaType(String authorMediaType) {
    this.authorMediaType = authorMediaType;
  }

  public String getAuthorMediaType() {
    return authorMediaType;
  }

  public void setAuthorMediaPath(String authorMediaPath) {
    this.authorMediaPath = authorMediaPath;
  }

  public String getAuthorMediaPath() {
    return authorMediaPath;
  }

  public void setAuthorMediaThumbPath(String authorMediaThumbPath) {
    this.authorMediaThumbPath = authorMediaThumbPath;
  }

  public String getAuthorMediaThumbPath() {
    return authorMediaThumbPath;
  }

  public void setArticleUrl(String articleUrl) {
    this.articleUrl = articleUrl;
  }

  public String getArticleUrl() {
    return articleUrl;
  }

  public void setArticleUrlSeoTitle(String articleUrlSeoTitle) {
    this.articleUrlSeoTitle = articleUrlSeoTitle;
  }

  public String getArticleUrlSeoTitle() {
    return articleUrlSeoTitle;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleHeading(String articleHeading) {
    this.articleHeading = articleHeading;
  }

  public String getArticleHeading() {
    return articleHeading;
  }

  public void setArticleShortDescription(String articleShortDescription) {
    this.articleShortDescription = articleShortDescription;
  }

  public String getArticleShortDescription() {
    return articleShortDescription;
  }

  public void setArticleUpdatedDate(String articleUpdatedDate) {
    this.articleUpdatedDate = articleUpdatedDate;
  }

  public String getArticleUpdatedDate() {
    return articleUpdatedDate;
  }

  public void setArticleMediaId(String articleMediaId) {
    this.articleMediaId = articleMediaId;
  }

  public String getArticleMediaId() {
    return articleMediaId;
  }

  public void setArticleMediaType(String articleMediaType) {
    this.articleMediaType = articleMediaType;
  }

  public String getArticleMediaType() {
    return articleMediaType;
  }

  public void setArticleMediaPath(String articleMediaPath) {
    this.articleMediaPath = articleMediaPath;
  }

  public String getArticleMediaPath() {
    return articleMediaPath;
  }

  public void setArticleMediaThumbPath(String articleMediaThumbPath) {
    this.articleMediaThumbPath = articleMediaThumbPath;
  }

  public String getArticleMediaThumbPath() {
    return articleMediaThumbPath;
  }

}
