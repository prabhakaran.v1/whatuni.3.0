package spring.valueobject.articles;

import java.util.List;

public class RssBlogArticleVO {
  
  private String affiliateId = null;
  private String contentChannel = null;
  private String channel = null;
  private List rssXMLList = null;
  private String rssXMLData = null;
  
  public void setAffiliateId(String affiliateId) {
    this.affiliateId = affiliateId;
  }
  public String getAffiliateId() {
    return affiliateId;
  }
  public void setContentChannel(String contentChannel) {
    this.contentChannel = contentChannel;
  }
  public String getContentChannel() {
    return contentChannel;
  }
  public void setChannel(String channel) {
    this.channel = channel;
  }
  public String getChannel() {
    return channel;
  }
  public void setRssXMLList(List rssXMLList) {
    this.rssXMLList = rssXMLList;
  }
  public List getRssXMLList() {
    return rssXMLList;
  }
  public void setRssXMLData(String rssXMLData) {
    this.rssXMLData = rssXMLData;
  }
  public String getRssXMLData() {
    return rssXMLData;
  }
}
