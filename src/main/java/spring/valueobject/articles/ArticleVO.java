package spring.valueobject.articles;

public class ArticleVO {
  //articleGroup related details
  private String articleGroupId = "";
  private String articleGroupHeading = "";
  private String articleGroupUrlSeoName = "";
  private String articleGroupDescription = "";
  private String articleGroupMediaId = "";
  private String articleGroupMediaType = "";
  private String articleGroupMediaPath = "";
  private String articleGroupMediaThumbPath = "";
  //Author related details
  private String authorId = "";
  private String authorName = ""; //
  private String authorShortBio = "";
  private String authorMediaId = "";
  private String authorMediaType = "";
  private String authorMediaPath = ""; //
  private String authorMediaThumbPath = "";
  //article related details
  private String articleId = ""; //
  private String articleHeading = ""; //  
  private String articleShortDescription = ""; //
  private String articleFullDescription = ""; //
  private String articleUpdatedDate = ""; //  
  //article seo details
  private String articleUrl = "";
  private String articleUrlSeoTitle = ""; //
  private String articleMetaTitle = "";
  private String articleMetaDescription = "";
  private String articleMetaKeywords = "";
  //article other details
  private String articleLdcsCode = "";
  private String articleLdcsDesc = "";
  private String articleArticleGroupQualId = "";
  private String articleArticleGroupQualDesc = "";
  private String articleNetworkQualId = "";
  private String articleNetworkQualDesc = "";
  private String articleArticleGroupCategoryId = "";
  private String articleArticleGroupCategoryDesc = "";
  //article media details
  private String articleMediaId = "";
  private String articleMediaType = "";
  private String articleMediaPath = "";
  private String articleMediaThumbPath = "";
  private String articleMediaThumbPath0001 = "";
  //article' next & previous details
  private String nextArticleUrlSeoTitle = "";
  private String previousArticleUrlSeoTitle = "";
  private String nextArticleUrl = "";
  private String previousArticleUrl = "";
  private String articleGroupHtmlName = "";
  private String totalRecords = "";
  private String articleCreatedDate = "";
  private String authorGoogleAccount = "";
  private String breadcrumb = "";
  private String firstParagraph = "";
  private String lastParagraph = "";
  private String updatedFlag = "";

  public void setArticleGroupId(String articleGroupId) {
    this.articleGroupId = articleGroupId;
  }

  public String getArticleGroupId() {
    return articleGroupId;
  }

  public void setArticleGroupHeading(String articleGroupHeading) {
    this.articleGroupHeading = articleGroupHeading;
  }

  public String getArticleGroupHeading() {
    return articleGroupHeading;
  }

  public void setArticleGroupUrlSeoName(String articleGroupUrlSeoName) {
    this.articleGroupUrlSeoName = articleGroupUrlSeoName;
  }

  public String getArticleGroupUrlSeoName() {
    return articleGroupUrlSeoName;
  }

  public void setArticleGroupDescription(String articleGroupDescription) {
    this.articleGroupDescription = articleGroupDescription;
  }

  public String getArticleGroupDescription() {
    return articleGroupDescription;
  }

  public void setArticleGroupMediaId(String articleGroupMediaId) {
    this.articleGroupMediaId = articleGroupMediaId;
  }

  public String getArticleGroupMediaId() {
    return articleGroupMediaId;
  }

  public void setArticleGroupMediaType(String articleGroupMediaType) {
    this.articleGroupMediaType = articleGroupMediaType;
  }

  public String getArticleGroupMediaType() {
    return articleGroupMediaType;
  }

  public void setArticleGroupMediaPath(String articleGroupMediaPath) {
    this.articleGroupMediaPath = articleGroupMediaPath;
  }

  public String getArticleGroupMediaPath() {
    return articleGroupMediaPath;
  }

  public void setArticleGroupMediaThumbPath(String articleGroupMediaThumbPath) {
    this.articleGroupMediaThumbPath = articleGroupMediaThumbPath;
  }

  public String getArticleGroupMediaThumbPath() {
    return articleGroupMediaThumbPath;
  }

  public void setAuthorId(String authorId) {
    this.authorId = authorId;
  }

  public String getAuthorId() {
    return authorId;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorShortBio(String authorShortBio) {
    this.authorShortBio = authorShortBio;
  }

  public String getAuthorShortBio() {
    return authorShortBio;
  }

  public void setAuthorMediaId(String authorMediaId) {
    this.authorMediaId = authorMediaId;
  }

  public String getAuthorMediaId() {
    return authorMediaId;
  }

  public void setAuthorMediaType(String authorMediaType) {
    this.authorMediaType = authorMediaType;
  }

  public String getAuthorMediaType() {
    return authorMediaType;
  }

  public void setAuthorMediaPath(String authorMediaPath) {
    this.authorMediaPath = authorMediaPath;
  }

  public String getAuthorMediaPath() {
    return authorMediaPath;
  }

  public void setAuthorMediaThumbPath(String authorMediaThumbPath) {
    this.authorMediaThumbPath = authorMediaThumbPath;
  }

  public String getAuthorMediaThumbPath() {
    return authorMediaThumbPath;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleHeading(String articleHeading) {
    this.articleHeading = articleHeading;
  }

  public String getArticleHeading() {
    return articleHeading;
  }

  public void setArticleShortDescription(String articleShortDescription) {
    this.articleShortDescription = articleShortDescription;
  }

  public String getArticleShortDescription() {
    return articleShortDescription;
  }

  public void setArticleFullDescription(String articleFullDescription) {
    this.articleFullDescription = articleFullDescription;
  }

  public String getArticleFullDescription() {
    return articleFullDescription;
  }

  public void setArticleUpdatedDate(String articleUpdatedDate) {
    this.articleUpdatedDate = articleUpdatedDate;
  }

  public String getArticleUpdatedDate() {
    return articleUpdatedDate;
  }

  public void setArticleUrl(String articleUrl) {
    this.articleUrl = articleUrl;
  }

  public String getArticleUrl() {
    return articleUrl;
  }

  public void setArticleUrlSeoTitle(String articleUrlSeoTitle) {
    this.articleUrlSeoTitle = articleUrlSeoTitle;
  }

  public String getArticleUrlSeoTitle() {
    return articleUrlSeoTitle;
  }

  public void setArticleMetaTitle(String articleMetaTitle) {
    this.articleMetaTitle = articleMetaTitle;
  }

  public String getArticleMetaTitle() {
    return articleMetaTitle;
  }

  public void setArticleMetaDescription(String articleMetaDescription) {
    this.articleMetaDescription = articleMetaDescription;
  }

  public String getArticleMetaDescription() {
    return articleMetaDescription;
  }

  public void setArticleMetaKeywords(String articleMetaKeywords) {
    this.articleMetaKeywords = articleMetaKeywords;
  }

  public String getArticleMetaKeywords() {
    return articleMetaKeywords;
  }

  public void setArticleLdcsCode(String articleLdcsCode) {
    this.articleLdcsCode = articleLdcsCode;
  }

  public String getArticleLdcsCode() {
    return articleLdcsCode;
  }

  public void setArticleLdcsDesc(String articleLdcsDesc) {
    this.articleLdcsDesc = articleLdcsDesc;
  }

  public String getArticleLdcsDesc() {
    return articleLdcsDesc;
  }

  public void setArticleArticleGroupQualId(String articleArticleGroupQualId) {
    this.articleArticleGroupQualId = articleArticleGroupQualId;
  }

  public String getArticleArticleGroupQualId() {
    return articleArticleGroupQualId;
  }

  public void setArticleArticleGroupQualDesc(String articleArticleGroupQualDesc) {
    this.articleArticleGroupQualDesc = articleArticleGroupQualDesc;
  }

  public String getArticleArticleGroupQualDesc() {
    return articleArticleGroupQualDesc;
  }

  public void setArticleNetworkQualId(String articleNetworkQualId) {
    this.articleNetworkQualId = articleNetworkQualId;
  }

  public String getArticleNetworkQualId() {
    return articleNetworkQualId;
  }

  public void setArticleNetworkQualDesc(String articleNetworkQualDesc) {
    this.articleNetworkQualDesc = articleNetworkQualDesc;
  }

  public String getArticleNetworkQualDesc() {
    return articleNetworkQualDesc;
  }

  public void setArticleArticleGroupCategoryId(String articleArticleGroupCategoryId) {
    this.articleArticleGroupCategoryId = articleArticleGroupCategoryId;
  }

  public String getArticleArticleGroupCategoryId() {
    return articleArticleGroupCategoryId;
  }

  public void setArticleArticleGroupCategoryDesc(String articleArticleGroupCategoryDesc) {
    this.articleArticleGroupCategoryDesc = articleArticleGroupCategoryDesc;
  }

  public String getArticleArticleGroupCategoryDesc() {
    return articleArticleGroupCategoryDesc;
  }

  public void setArticleMediaId(String articleMediaId) {
    this.articleMediaId = articleMediaId;
  }

  public String getArticleMediaId() {
    return articleMediaId;
  }

  public void setArticleMediaType(String articleMediaType) {
    this.articleMediaType = articleMediaType;
  }

  public String getArticleMediaType() {
    return articleMediaType;
  }

  public void setArticleMediaPath(String articleMediaPath) {
    this.articleMediaPath = articleMediaPath;
  }

  public String getArticleMediaPath() {
    return articleMediaPath;
  }

  public void setArticleMediaThumbPath(String articleMediaThumbPath) {
    this.articleMediaThumbPath = articleMediaThumbPath;
  }

  public String getArticleMediaThumbPath() {
    return articleMediaThumbPath;
  }

  public void setNextArticleUrlSeoTitle(String nextArticleUrlSeoTitle) {
    this.nextArticleUrlSeoTitle = nextArticleUrlSeoTitle;
  }

  public String getNextArticleUrlSeoTitle() {
    return nextArticleUrlSeoTitle;
  }

  public void setPreviousArticleUrlSeoTitle(String previousArticleUrlSeoTitle) {
    this.previousArticleUrlSeoTitle = previousArticleUrlSeoTitle;
  }

  public String getPreviousArticleUrlSeoTitle() {
    return previousArticleUrlSeoTitle;
  }

  public void setNextArticleUrl(String nextArticleUrl) {
    this.nextArticleUrl = nextArticleUrl;
  }

  public String getNextArticleUrl() {
    return nextArticleUrl;
  }

  public void setPreviousArticleUrl(String previousArticleUrl) {
    this.previousArticleUrl = previousArticleUrl;
  }

  public String getPreviousArticleUrl() {
    return previousArticleUrl;
  }

  public void setArticleMediaThumbPath0001(String articleMediaThumbPath0001) {
    this.articleMediaThumbPath0001 = articleMediaThumbPath0001;
  }

  public String getArticleMediaThumbPath0001() {
    return articleMediaThumbPath0001;
  }

    public void setArticleGroupHtmlName(String articleGroupHtmlName) {
        this.articleGroupHtmlName = articleGroupHtmlName;
    }

    public String getArticleGroupHtmlName() {
        return articleGroupHtmlName;
    }

  public void setTotalRecords(String totalRecords) {
    this.totalRecords = totalRecords;
  }

  public String getTotalRecords() {
    return totalRecords;
  }
  public void setArticleCreatedDate(String articleCreatedDate) {
    this.articleCreatedDate = articleCreatedDate;
  }
  public String getArticleCreatedDate() {
    return articleCreatedDate;
  }
  public void setAuthorGoogleAccount(String authorGoogleAccount) {
    this.authorGoogleAccount = authorGoogleAccount;
  }
  public String getAuthorGoogleAccount() {
    return authorGoogleAccount;
  }
  public void setBreadcrumb(String breadcrumb) {
    this.breadcrumb = breadcrumb;
  }
  public String getBreadcrumb() {
    return breadcrumb;
  }
  public void setFirstParagraph(String firstParagraph) {
    this.firstParagraph = firstParagraph;
  }
  public String getFirstParagraph() {
    return firstParagraph;
  }
  public void setLastParagraph(String lastParagraph) {
    this.lastParagraph = lastParagraph;
  }
  public String getLastParagraph() {
    return lastParagraph;
  }

    public void setUpdatedFlag(String updatedFlag) {
        this.updatedFlag = updatedFlag;
    }

    public String getUpdatedFlag() {
        return updatedFlag;
    }
}
