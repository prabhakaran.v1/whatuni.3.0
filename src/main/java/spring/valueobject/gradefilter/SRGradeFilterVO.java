package spring.valueobject.gradefilter;

public class SRGradeFilterVO {
  private String qualId;
  private String qualification;
  private String qualificationUrl;
  private String parentQualification;
  private String gradeOptions;
  private String maxPoint;
  private String maxTotalPoint;
  private String gradeOptionflag;
  
public String getQualId() {
	return qualId;
}
public void setQualId(String qualId) {
	this.qualId = qualId;
}
public String getQualification() {
	return qualification;
}
public void setQualification(String qualification) {
	this.qualification = qualification;
}
public String getQualificationUrl() {
	return qualificationUrl;
}
public void setQualificationUrl(String qualificationUrl) {
	this.qualificationUrl = qualificationUrl;
}
public String getParentQualification() {
	return parentQualification;
}
public void setParentQualification(String parentQualification) {
	this.parentQualification = parentQualification;
}
public String getGradeOptions() {
	return gradeOptions;
}
public void setGradeOptions(String gradeOptions) {
	this.gradeOptions = gradeOptions;
}
public String getMaxPoint() {
	return maxPoint;
}
public void setMaxPoint(String maxPoint) {
	this.maxPoint = maxPoint;
}
public String getMaxTotalPoint() {
	return maxTotalPoint;
}
public void setMaxTotalPoint(String maxTotalPoint) {
	this.maxTotalPoint = maxTotalPoint;
}
public String getGradeOptionflag() {
	return gradeOptionflag;
}
public void setGradeOptionflag(String gradeOptionflag) {
	this.gradeOptionflag = gradeOptionflag;
}
}
