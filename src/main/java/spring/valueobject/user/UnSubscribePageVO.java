package spring.valueobject.user;

import lombok.Data;

@Data
public class UnSubscribePageVO {
  private String newsletter;
  private String remainder;
  private String universityUpdates;
  private String survey;
  private String encryptedId;
  private String userId;
  private String reviewSurveyFlag = null;
}
