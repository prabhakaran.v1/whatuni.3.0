package spring.dao;

import java.util.Map;

import spring.pojo.user.UnSubcribeForm;

public interface IUserDAO {

	Map getUnSubscribePage(UnSubcribeForm unSubcribeForm) throws Exception;
	Map saveUserPreference(UnSubcribeForm unSubcribeForm) throws Exception;
	
}
