package spring.dao;

import java.util.Map;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.pojo.searchresult.GetInstitutionNameBean;
import spring.pojo.searchresult.SearchResultsBean;

public interface ISearchResultsDAO {

	Map<String, Object> getSearchResultsList(SearchResultsBean searchResultsBean) throws Exception;
    Map<String, Object> getInstitutionNameList(GetInstitutionNameBean institutionNameBean) throws Exception; 
    Map<String, Object> getFooterSectionHtml(GetFooterSectionBean getFooterSectionBean) throws Exception;
}
