package spring.dao;

import java.util.List;
import java.util.Map;

public interface IPreClearingLandingDAO {

  public Map getEbookAboutWhatuniContents();//Added to get pre clearing About whatuni SHTML contents details - Sujitha V for 31 March 2020
  
  public Map getClearingHomePageDetails(List inputList); // This will the Clearing Home Page Data.
  
  public Map getClearingSubjectAjaxList(String subjectKeywordSearch) throws Exception;

  public Map getClearingUniAjaxList(String uniKeywordSearch) throws Exception;
  
  public Map getClearingCourseDetails(List inputList);
	
}
