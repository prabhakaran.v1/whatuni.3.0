package spring.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface IRestDAO {
  public String callWebServices(String URL, String jsonString);
  public String callContentfulWebServices(String URL);
}
