package spring.dao;

import java.util.Map;
import spring.dao.util.valueobject.cpc.CPCLightboxVO;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ICPCLandingDAO.java
 * Description   : Getting cpc related content..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Hema s.                   1.0               04.02.2020        First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public interface ICPCLandingDAO {
  public Map getCpcTypeValues(CPCPageVO cPCPageVO) throws Exception;//Added this for getting campaign type by Hema.S on 04_02_2020_REL
  public Map getMultipleLandingPageValues(CPCPageVO cPCPageVO) throws Exception;//Added this for getting multiple landing page values by Hema.S on 04_02_2020_REL
  public Map getSingleLandingPageValues(CPCPageVO cPCPageVO) throws Exception;//Added this for getting single landing page values by Jeyalakshmi.D on 04_02_2020_REL
  public Map getCPCLightboxValues(CPCLightboxVO cPCLightboxVO) throws Exception;//Added this for getting lightbox values for both single and multiple CPC pages by Jeyalakshmi.D on 04_02_2020_REL
}
