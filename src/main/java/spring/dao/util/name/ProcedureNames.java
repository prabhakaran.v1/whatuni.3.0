package spring.dao.util.name;

/**
 * contains spring specific constants
 * 
 * @author     Hema.S
 * @since      wu598_20200204
 *
 */

public interface ProcedureNames {  
  public static final String GET_CPC_TYPE_VALUE = "HOT_WUNI.CPC_LANDING_PAGE_PKG.GET_LANDING_PAGE_TYPE_PRC";//Added this for getting cpc page type by Hema.S on 04_02_2020_REL
  public static final String GET_MULTIPLE_CPC_PAGE_VALUE = "HOT_WUNI.CPC_LANDING_PAGE_PKG.MULTIPLE_CLIENT_LANDING_PRC";//Added this for getting Multiple cpc landing page values by Hema.S on 04_02_2020_REL
  public static final String GET_SINGLE_CPC_PAGE_VALUE = "HOT_WUNI.CPC_LANDING_PAGE_PKG.SINGLE_CLIENT_LANDING_PAGE_PRC";//Added this for getting Single cpc landing page values by Hema.S on 04_02_2020_REL
  public static final String GET_LANDING_LIGHTBOX_PRC = "HOT_WUNI.CPC_LANDING_PAGE_PKG.GET_LANDING_LIGHTBOX_PRC";
  
  //Added for pre clearing About whatuni SHTML contents details - Sujitha V for 31 March 2020
  public static final String GET_PRE_CLEARING_ABOUT_WU_PRC = "HOT_WUNI.GET_PRE_CLEARING_ABOUT_WU_PRC";
  
  public static final String GET_COVID19_DATA_PRC = "HOT_WUNI.GET_COVID_19_PAGE_DETAILS_PRC ";
  //Open days provider landing page, 28_April_2020 by Sujitha V
  public final String GET_UNI_OPENDAY_LANDING_PRC = "HOT_WUNI.WU_OPEN_DAY_LANDING_PAGE_PKG.GET_UNI_OPENDAY_LANDING_PRC";
  public static final String GET_PROVIDER_OPENDAYS_LIST_FN= "HOT_WUNI.WU_OPEN_DAY_LANDING_PAGE_PKG.GET_PROVIDER_OPENDAYS_FN";
  //Added for clearing home page ajax - Kailash L 23_Jun_2020_rel
  public static final String GET_CLEARING_SUBJECT_AJAX_LIST_PRC= "HOT_WUNI.WU_CLEARING_PKG.GET_HOME_PAGE_SUBJECT_AJAX_PRC";
  public static final String GET_CLEARING_UNIVERSITY_AJAX_LIST_FN= "HOT_WUNI.WU_CLEARING_PKG.GET_HOME_PAGE_UNI_AJAX_FN";
  //Added for clearing cd page - Sujitha V for 23_Jun_2020_rel
  public static final String GET_CL_COURSE_DETAILS_PAGE_PRC = "HOT_WUNI.WU_COURSE_DETAILS_PKG.GET_CL_COURSE_DETAILS_PAGE_PRC";
  public static final String GET_SYSVAR_VALUES_FN = "HOT_WUNI.GET_SYSVAR_VALUES_FN";
  public static final String GET_AUTOCOMPLETE_COLLEGE_VALUES_FN = "WU_UTIL_PKG.AUTOCOMPLETE_COLLEGE_FN";
  public final String GET_SEARCH_RESULT_PAGE_PRC = "WHATUNI_SEARCH_PKG.GET_SEARCH_RESULT_PAGE_PRC";   
  public final String GET_FOOTER_SECTION_FN = "WU_HTML_EDITOR_PKG.GET_HTML_EDITOR_FN";
}
