package spring.dao.util.valueobject.common;

import lombok.Data;

@Data
public class Covid19VO {
  private String sysvarName;
  private String sysvarText;
  private String headerText;
  private String coursePageText;
  private String articlePageText;
  private String opendayPageText;
  private String covid19Sysvar;
}
