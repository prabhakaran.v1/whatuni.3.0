package spring.dao.util.valueobject.clearing;

import lombok.Data;

@Data
public class ClearingHomeVO {
  
	private String description;
	private String url;
	private String categoryCode;
	private String browseCategoryId;
	private String matchedText;
	private String orderSequence;
	private String regionName;
	private String regionId;
	private String regionUrlText;
	private String sdMessage;
	
}
