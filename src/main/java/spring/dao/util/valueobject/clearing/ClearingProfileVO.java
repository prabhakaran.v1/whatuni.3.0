package spring.dao.util.valueobject.clearing;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClearingProfileVO {
  private String sessionId = null;
  private String userId = null;
  private String basketId = null;
  private String collegeId = null;
  private String networkId = null;
  private String requestDesc = null;
  private String clientIp = null;
  private String clientBrowser = null;
  private String collegeSection = null;
  private String requestUrl = null;
  private String referelUrl = null;
  private String metaPageName = null;
  private String metaPageFlag = null;
  private String trackSession = null;
  private String profileType = null;
  private String orderItemId = null;
  private String email = null;
  private String emailWebform = null;
  private String prospectus = null;
  private String prospectusWebform = null;
  private String website = null;
  private String suborderItemId = null;
  private String myhcProfileId = null;
  private String userJourney = null;
  private String clearingProfilePageURL = null;
}
