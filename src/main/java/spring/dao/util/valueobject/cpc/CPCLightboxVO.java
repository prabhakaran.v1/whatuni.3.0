package spring.dao.util.valueobject.cpc;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : CPCPageVO.java
 * Description   : Getting cpc related content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Hema s.                 1.0            04.02.2020        First draft
 * Keerthana.E             2.0            21.07.2020        Added hotline no and profile media flag for clearing
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Getter @Setter
public class CPCLightboxVO {
  private String landingId;
  private String collegeId;
  private String collegeNameDisplay;
  private String collegeLogo;
  private String mediaId;
  private String mediaPath;
  private String mediaType;
  private String thumbnailPath;
  private String reviewCount;
  private String overAllRating;
  private String overAllRatingExact;
  private String email;
  private String emailWebForm;
  private String prospectus;
  private String prospectusWebForm;
  private String website;
  private String subOrderItemId;
  private String webformprice;
  private String websitePrice;
  private String courseCount;
  private String collegeName;
  private String reviewDisplayFlag;
  private String courseName;
  private String courseDeletedFlag;
  private String createdDate;
  private String studyLevelText;
  private String reviewQuestionId;
  private String questionTitle;
  private String answer;
  private String rating;
  private String ordValue;
  private String qualificationId;
  private String subjectCode;
  private String regionId;
  private String contentHubFlag;
  private String buttonLabel;
  private String openDate;
  private String openDay;
  private String openMonthYear;
  private String bookingUrl;
  private String totalOpenDays;
  private String reviewId;
  private String userId;
  private String reviewTitle;
  private String questionDesc;
  private String userName;
  private String courseId;
  private String userNameInt;
  private String userNameInitialColourcode;
  private ArrayList userMoreReviewaList;
  private String uniHomeURL;
  private String courseDetailsURL;
  private String uniReviewUrl;
  private String orderItemId;
  private String networkId;
  private String previewFlag;
  private String browseCatId;
  private String urlFlagOnUni;
  private String urlFormingCode;
  private String qualCode;
  private String subjectName;
  private String regionName;
  private String providerResultsURL;
  private String studyLevel;
  private String hotlineNo;
  private String clearingProfileMediaFlag;
}

