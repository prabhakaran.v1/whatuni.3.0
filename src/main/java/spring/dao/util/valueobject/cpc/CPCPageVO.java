package spring.dao.util.valueobject.cpc;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : CPCPageVO.java
 * Description   : Getting cpc related content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Hema s.                 1.0            04.02.2020        First draft
 * Keerthana.E             2.0            21.07.2020        Added Dynamic random no and profile media flag for clearing
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Getter @Setter
public class CPCPageVO {
  private String campaignId;
  private String collegeId;
  private String collegeNameDisplay;
  private String collegeLogo;
  private String mediaId;
  private String mediaPath;
  private String thumbnailPath;
  private String mediaType;
  private String reviewCount;
  private String overAllRating;
  private String overAllRatingExact;
  private String orderItemId;
  private String email;
  private String emailWebForm;
  private String prospectus;
  private String prospectusWebForm;
  private String website;
  private String subOrderItemId;
  private String myhcProfileId;
  private String profileType;
  private String applyNowFlag;
  private String networkId;
  private String hotlineNo;
  private String qlFlag;
  private String webformprice;
  private String websitePrice;
  private String openDaysCount;
  private String courseCount;
  private String collegeName;
  private String subTitle;
  private String reviewRating;
  private String editorialMediaPath;
  private String editorialMediaType;
  private String editorialContent;
  private String browseCatId;
  private String browseCatDesc;
  private String selectedSubjectFlag;
  private String browseRegionId;
  private String browseRegionDesc;
  private String selectedRegionFlag;
  private String courseId;
  private String courseTitle;
  private String studyLevel;
  private String entryRequirments;
  private String pageNo;
  private String reviewFlag;
  private String ajaxFlag;
  private ArrayList enquiryDetailsList;
  private String uniHomeURL;
  private String title;
  private String status;
  private String fallbackURL;
  private String subjectCode;
  private String subjectName;
  private String regionName;
  private String regionId;
  private String sortingCode;
  private String contentHubMediadFlag;
  private String openDate;
  private String openDay;
  private String openMonthYear;
  private String bookingURL;
  private String openDaySubOrderItemId;
  private String parentCategoryCode;
  private String heroImagePath;
  private String heroImageType;
  private String totalOpenDay;
  private String viewCoursesFlag;
  private String landingId;
  private String sortBy;
  private String previewFlag;
  private String parentReginName;
  private String seoStudyLevelText;
  private String blogCategoryId;
  private String blogCategoryName;
  private String blogUrl;
  private String courseDetailUrl;
  private String heroImageThumbnail;
  private String viewCourseFlag;
  private String urlFormCode;
  private String qualCode;
  private String subjectNameURL;
  private String regionNameURL;
  private String providerResultsURL;
  private String dynamicRandomNumber;
  private String clearingProfileMediaFlag;
}

