package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class QualificationInfoVO {

	private String qualificationName;
	private String qualificationId;
	private String selectedFlag;
	private String qualCode;
}
