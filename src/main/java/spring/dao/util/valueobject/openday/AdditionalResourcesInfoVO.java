package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdditionalResourcesInfoVO {
	  
  private String pdfName;
  private String pdfPath;
}
