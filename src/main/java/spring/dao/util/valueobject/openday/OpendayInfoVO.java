package spring.dao.util.valueobject.openday;

import lombok.Data;


@Data
public class OpendayInfoVO {
  private String eventItemId;
  private String startDate;
  private String startTime;
  private String venue;
  private String qualType;
  private String eventType;
  private String eventDate;
  private String totalOpenDayCount;
  private String moreOpenDayFlag;
  private String collegeId;
  private String collegeNameDisplay;
  private String collegeName;
  private String collegeLogo;
  private String open_Date;
  private String openDay;
  private String openDate;
  private String openMonth;
  private String openMonthYear;
  private String endTime;
  private String qualDesc;
  private String qualId;
  private String bookingUrl;
  private String bookingFormFlag;
  private String subOrderItemId;
  private String networkId;
  private String websitePrice;
  private String eventCategoryName;
  private String eventCategoryId;
  private String qualTypeGA;
  private String selectedFlag;
  private String selectedOpenDate;

}
