package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class ConsentInfoVO {

	private String consentText;
	private String consentFlag;
}
