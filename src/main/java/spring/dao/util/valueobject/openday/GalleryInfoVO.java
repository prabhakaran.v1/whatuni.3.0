package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class GalleryInfoVO {
	
  private String mediaId;
  private String collegeNameDisplay;
  private String mediaPath;
  private String mediaType;
  private String thumbnailPath;
  private String contentHubFlag;
  private String mediaName;
}
