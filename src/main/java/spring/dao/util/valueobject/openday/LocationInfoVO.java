package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LocationInfoVO {

  private String latitude;
  private String longitude;
  private String collegeNameDisplay;
  private String addressLineOne;
  private String addreeslineTwo;
  private String town;
  private String postcode;
  private String countryName;
  private String isInLondon;
  private String nearestTrainStation;
  private String nearestTubeStation;
  private String distanceFromTrainStation;
  private String distanceFromTubeStation;
  private String cityGuideFlag;
  private String cityGuideLocation;
  private String articleCategory;
  private String articleId;
  private String cityGuideUrl;
}
