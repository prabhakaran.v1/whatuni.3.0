package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class OpenDayProviderLandingPageVO {

  private String collegeId;
  private String collegeName;
  private String collegeNameDisplay;
  private String reviewCount;
  private String overallRating;
  private String overallRatingExact;
  private String eventItemId;
  private String startDate;
  private String startTime;
  private String venue;
  private String qualType;
  private String totalOpenDayCount;
  private String moreOpenDayFlag;
  private String mediaId;
  private String sectionId;
  private String sectionValue;
  private String addressLineOne;
  private String addreeslineTwo;
  private String countryName;
  private String latitude;
  private String longitude;
  private String nearestTrainStation;
  private String distanceTrainStation;
  private String cityGuideFlag;
  private String filePath;
}
