package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class UserInfoVO {

	private String firstName;
	private String lastName;
	private String userEmail;
	private String password;
	private String mobileNumber;
	private String marketingFlag;
	private String solusFlag;
	private String surveyFlag;
	private String isIpBlocked;
	private String isEmailIdBlocked;
	private String isUserIdBlocked;
}
