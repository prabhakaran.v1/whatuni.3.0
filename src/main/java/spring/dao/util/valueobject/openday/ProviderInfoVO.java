package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProviderInfoVO {
  private String collegeId;
  private String collegeName;
  private String collegeNameDisplay;
  private String reviewCount;
  private String overallRating;
  private String overallRatingExact;
  private String collegeLogo;
  private String  qualType;
  private String startDate;
  private String startTime;
  private String eventVenue;
  private String eventId;
  private String collegeLandingUrl;
  private String mediaPath;
  private String uniReviewsURL;
  private String eventType;
  private String eventDate;
  private String subjectName;
  private String qualCode;
  private String description;
  private String categoryCode;
  private String browseCategoryId;
  
}
