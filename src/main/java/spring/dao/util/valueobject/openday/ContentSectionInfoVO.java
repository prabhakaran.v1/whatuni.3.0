package spring.dao.util.valueobject.openday;

import java.util.ArrayList;

import lombok.Data;

@Data
public class ContentSectionInfoVO {

  private String sectionName;
  private String sectionValue;
  private String updatedDate;
  private String title;
  private String contentSectionIcon;
  private ArrayList contentSectionBulletValuesList = null; 
  private String selectedOpenDate;
}
