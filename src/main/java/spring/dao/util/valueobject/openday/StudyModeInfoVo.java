package spring.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class StudyModeInfoVo {

	private String studyMode;
	private String studyModeId;
	private String selectedFlag;
}
