package spring.dao.sp.searchresult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.rowmapper.search.OmnitureLoggingRowMapperImpl;
import com.layer.util.rowmapper.search.StatsLogRowMapperImpl;
import com.wuni.util.seo.SeoUrls;

import WUI.utilities.CommonFunction;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.pojo.searchresult.SearchResultsBean;
import spring.valueobject.search.FeaturedBrandVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.searchresult.BreadCrumbVO;
import spring.valueobject.searchresult.CourseSpecificListVO;
import spring.valueobject.searchresult.LocationListVO;
import spring.valueobject.searchresult.LocationTypeVO;
import spring.valueobject.searchresult.QualificationListVO;
import spring.valueobject.searchresult.SearchResultContentsVO;
import spring.valueobject.searchresult.StudyModeVO;
import spring.valueobject.searchresult.SubjectFilterVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
 * @SearchResultSP
 * @author   Kailash.Lakshmanan
 * @author   Sujitha V
 * @author   Sri Sankari R
 * @version  1.0
 * @since    19_01_2021   
 * @purpose  This SP is used to get the data to Search results page.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
*/

public class SearchResultSP extends StoredProcedure {

	  public SearchResultSP(DataSource datasource) {
	    setDataSource(datasource);
	    setSql(ProcedureNames.GET_SEARCH_RESULT_PAGE_PRC);
	    declareParameter(new SqlParameter("x", Types.VARCHAR));
	    declareParameter(new SqlParameter("y", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_SEARCH_WHAT", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_PHRASE_SEARCH", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_COLLEGE_NAME", Types.VARCHAR));    
	    declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_TOWN_CITY", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
	    declareParameter(new SqlParameter("A", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_SEARCH_HOW", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_SEARCH_CATEGORY", Types.VARCHAR));   
	    declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_PAGE_NO", Types.NUMERIC));
	    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));    
	    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_ENTRY_LEVEL", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_ENTRY_POINTS", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_UNIV_LOC_TYPE_NAME_STR", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_CAMPUS_BASED_UNIV_NAME", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_RUSSELL_GROUP_NAME", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_EMPLOYMENT_RATE", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_UCAS_CODE", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_UCAS_TARIFF_RANGE", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_JACS_CODE", Types.VARCHAR));    
	    declareParameter(new SqlParameter("P_SEARCH_URL", Types.VARCHAR)); 
	    declareParameter(new SqlParameter("P_TRACK_SESSION_ID", Types.NUMERIC));
	    declareParameter(new SqlOutParameter("PC_SEARCH_RESULTS", OracleTypes.CURSOR, new SearchResultsContentsRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_STUDY_MODE_REFINE", OracleTypes.CURSOR, new RefinRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_QUALIFICATION_REFINE", OracleTypes.CURSOR, new QualificationRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_SUBJECT_REFINE", OracleTypes.CURSOR, new SubjectRefineRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_REFINES", OracleTypes.CURSOR, new RefineRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_LOCATION_REFINE", OracleTypes.CURSOR,  new LocationRefineRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_STATS_LOG", OracleTypes.CURSOR, new StatsLogRowMapperImpl())); 
	    declareParameter(new SqlOutParameter("P_RECORD_COUNT", Types.NUMERIC));
	    declareParameter(new SqlOutParameter("P_TOTAL_COURSE_COUNT", Types.NUMERIC));   
	    declareParameter(new SqlOutParameter("PC_SEARCH_COLLEGE_IDS", OracleTypes.CURSOR, new OmnitureLoggingRowMapperImpl()));
	    declareParameter(new SqlOutParameter("PC_SEARCH_INSTITUTION_IDS", OracleTypes.CURSOR, new InstitutionIdsLoggingRowMapperImpl()));    
	    declareParameter(new SqlOutParameter("P_RESULT_EXISTS", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_PARENT_CAT_ID", Types.NUMERIC));
	    declareParameter(new SqlOutParameter("P_PARENT_CAT_TEXT_KEY", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_PARENT_CAT_DESC", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_COURSE_RANK_FOUND_FLAG", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_INVALID_CATEGORY_FLAG", Types.VARCHAR)); 
	    declareParameter(new SqlInOutParameter("P_SUBJ_GRADES_SESSION_ID", Types.VARCHAR));
	    declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", Types.NUMERIC));
	    declareParameter(new SqlOutParameter("P_USER_QUAL_EXIST_FLAG", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("PC_FEATURED_BRAND", OracleTypes.CURSOR, new FeaturedBrandRowMapperImpl()));
	    declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_SPONS_ORDER_ITEM_ID", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SEODetails()));
	    declareParameter(new SqlOutParameter("P_BROWSE_CAT_DESC", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_SEARCH_PHRASE", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_SNIPPET_CONTENT", OracleTypes.CLOB));
	    declareParameter(new SqlOutParameter("P_BREADCRUMBS", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("PC_BC_SCHEMA_TAGGING", OracleTypes.CURSOR, new BreadCrumbSchemaTagRowMapperImpl()));
	    declareParameter(new SqlOutParameter("P_STUDY_LEVEL_DISPLAY_TEXT", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_TERMS_AND_CONDITION_URL", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_PRIVACY_POLICY_URL", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_BASKET_COUNT", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_CANONICAL_URL", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("P_PRIVACY_LATEST_DATE", Types.VARCHAR));
	    declareParameter(new SqlOutParameter("PC_CPE_CATEGORY", OracleTypes.CURSOR));

	  }
	  public Map<String, Object> execute(SearchResultsBean searchResultsBean) throws Exception {
	    Map<String, Object> outMap = null;
		Map<String, String> inMap = new HashMap<>();
	      inMap.put("x", searchResultsBean.getX());
	      inMap.put("y", searchResultsBean.getY());
	      inMap.put("P_SEARCH_WHAT", searchResultsBean.getSearchWhat());
	      inMap.put("P_PHRASE_SEARCH", searchResultsBean.getPhraseSearch());
	      inMap.put("P_COLLEGE_NAME", searchResultsBean.getCollegeName());      
	      inMap.put("P_QUALIFICATION", searchResultsBean.getQualification());
	      inMap.put("P_TOWN_CITY", searchResultsBean.getTownCity());
	      inMap.put("P_STUDY_MODE", searchResultsBean.getStudyMode());
	      inMap.put("A", searchResultsBean.getAffiliateId());
	      inMap.put("P_SEARCH_HOW", searchResultsBean.getSearchHow());
	      inMap.put("P_SEARCH_CATEGORY", searchResultsBean.getSearchCategory());
	      inMap.put("P_USER_AGENT", searchResultsBean.getUserAgent());    
	      inMap.put("P_PAGE_NO", searchResultsBean.getPageNo());
	      inMap.put("P_BASKET_ID", searchResultsBean.getBasketId());      
	      inMap.put("P_PAGE_NAME", searchResultsBean.getPageName());
	      inMap.put("P_ENTRY_LEVEL", searchResultsBean.getEntryLevel());
	      inMap.put("P_ENTRY_POINTS", searchResultsBean.getEntryPoints());
	      inMap.put("P_UNIV_LOC_TYPE_NAME_STR", searchResultsBean.getUnivLocTypeName());
	      inMap.put("P_CAMPUS_BASED_UNIV_NAME", searchResultsBean.getCampusTypeName());
	      inMap.put("P_RUSSELL_GROUP_NAME", searchResultsBean.getRusselGroupName());
	      inMap.put("P_EMPLOYMENT_RATE", searchResultsBean.getEmpRate());
	      inMap.put("P_UCAS_CODE", searchResultsBean.getUcasCode());
	      inMap.put("P_UCAS_TARIFF_RANGE", searchResultsBean.getUcasTariff());
	      inMap.put("P_JACS_CODE", searchResultsBean.getJacsCode());
	      inMap.put("P_SEARCH_URL", searchResultsBean.getRequestURL());
	      inMap.put("P_TRACK_SESSION_ID", searchResultsBean.getUserTrackSessionId());
	      inMap.put("P_SUBJ_GRADES_SESSION_ID", searchResultsBean.getSubjectSessionId());
	      inMap.put("P_DYNAMIC_RANDOM_NUMBER", searchResultsBean.getRandomNumber());
	      inMap.put("P_USER_IP", searchResultsBean.getClientIp());
	      outMap = execute(inMap);      
	      return outMap;
	  }
	  private class RefinRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	      StudyModeVO studyModeVO = new StudyModeVO();
	      studyModeVO.setRefineDesc(resultSet.getString("REFINE_DESC"));
	      studyModeVO.setRefineCode(resultSet.getString("REFINE_CODE")); 
	      studyModeVO.setRefineTextKey(resultSet.getString("REFINE_TEXT_KEY")); 
	      studyModeVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	      return studyModeVO;
	    }
	  }
	  
	  private class SubjectRefineRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	      SeoUrls seoUrls = new SeoUrls();
	      SubjectFilterVO subjectVO = new SubjectFilterVO();
	      subjectVO.setCategoryCode(resultSet.getString("CATEGORY_CODE"));
	      subjectVO.setCategoryDesc(resultSet.getString("SUBJECT"));
	      subjectVO.setSubjectTextKey(resultSet.getString("SUBJECT_TEXT_KEY"));      
	      subjectVO.setCollegeCount(resultSet.getString("RESULT_COUNT"));
	      subjectVO.setCategoryId(resultSet.getString("BROWSE_CAT_ID"));
	      subjectVO.setQualificationCode(resultSet.getString("QUALIFICATION"));
	      subjectVO.setBrowseMoneyPageUrl(seoUrls.construnctNewBrowseMoneyPageURL(subjectVO.getQualificationCode(), subjectVO.getCategoryId(), subjectVO.getCategoryDesc(), "PAGE"));
	      subjectVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	      return subjectVO;
	    }
	  }

	  private class LocationRefineRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	      LocationListVO locationListVO = new LocationListVO();
	      locationListVO.setRegionName(resultSet.getString("REGION_NAME"));
	      locationListVO.setRegionTextKey(resultSet.getString("REGION_TEXT_KEY"));
	      locationListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	      return locationListVO;
	    }
	  }
	  //
	  private class QualificationRowMapperImpl implements RowMapper {
	     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	       QualificationListVO qualificationListVO = new QualificationListVO();
	       qualificationListVO.setRefineDesc(resultSet.getString("REFINE_DESC"));
	       qualificationListVO.setRefineCode(resultSet.getString("REFINE_CODE"));
	       qualificationListVO.setRefineTextKey(resultSet.getString("REFINE_TEXT_KEY"));       
	       qualificationListVO.setCategoryId(resultSet.getString("BROWSE_CAT_ID"));
	       qualificationListVO.setCategoryName(resultSet.getString("BROWSE_CAT_DESC"));
	       qualificationListVO.setBrowseCatTextKey(resultSet.getString("BROWSE_CAT_TEXT_KEY"));       
	       qualificationListVO.setCategoryCode(resultSet.getString("BROWSE_CATEGORY_CODE"));
	       qualificationListVO.setRefineDisplayDesc(resultSet.getString("REFINE_DISPLAY_DESC"));
	       qualificationListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	       return qualificationListVO;
	     }
	   }
	  //
	  private class RefineRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	     LocationTypeVO locationTypeVO = new LocationTypeVO();
	     locationTypeVO.setOptionId(resultSet.getString("OPTION_ID"));      
	     locationTypeVO.setOptionDesc(resultSet.getString("OPTION_DESC"));
	     locationTypeVO.setOptionTextKey(resultSet.getString("OPTION_TEXT_KEY"));      
	     locationTypeVO.setOptionType(resultSet.getString("OPTION_TYPE"));
	     locationTypeVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	     return locationTypeVO;
	    }  
	  }
	  //
	  private class SearchResultsContentsRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
	    SearchResultContentsVO searchResultContentsVO = new SearchResultContentsVO();
	      CommonFunction commonFunction = new CommonFunction();
	      searchResultContentsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
	      searchResultContentsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
	      searchResultContentsVO.setCollegeNameSpecialCharRemove(commonFunction.replaceSpecialCharacter(resultset.getString("COLLEGE_NAME")));
	      searchResultContentsVO.setCollegeTextKey(resultset.getString("COLLEGE_TEXT_KEY"));
	      searchResultContentsVO.setCollegePRUrl(resultset.getString("COLLEGE_PR_URL"));
	      searchResultContentsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
	      searchResultContentsVO.setUniHomeUrl(resultset.getString("PROFILE_PAGE_URL"));
	      searchResultContentsVO.setOverallRating(resultset.getString("RATING"));
	      searchResultContentsVO.setExactRating(resultset.getString("EXACT_RATING"));
	      searchResultContentsVO.setOpendayUrl(resultset.getString("OPENDAY_PAGE_URL"));
	      searchResultContentsVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
	      searchResultContentsVO.setWasThisCollegeShortlisted((resultset.getString("COLLEGE_IN_BASKET")));
	      searchResultContentsVO.setCollegeLogoPath(resultset.getString("COLLEGE_LOGO"));
	      searchResultContentsVO.setRusselGroup(resultset.getString("RUSSELL_GROUP_FLAG"));
	      searchResultContentsVO.setJacsSubject(resultset.getString("JACS_SUBJECT"));
	      searchResultContentsVO.setCurrentRank(resultset.getString("CURRENT_RANK_IN_CATEGORY"));
	      searchResultContentsVO.setTotalRank(resultset.getString("TOTAL_RANK_IN_CATEGORY"));
	      searchResultContentsVO.setNetworkId(resultset.getString("NETWORK_ID"));
	      searchResultContentsVO.setOpendaySuborderItemId(resultset.getString("OPEN_DAY_SUBORDER_ITEM_ID"));
	      searchResultContentsVO.setOpenDate(resultset.getString("OPEN_DAY"));
	      searchResultContentsVO.setOpenMonthYear(resultset.getString("OPEN_MONTH_YEAR"));
	      searchResultContentsVO.setBookingUrl(resultset.getString("BOOKING_URL"));
	      searchResultContentsVO.setTotalOpendays(resultset.getString("TOTAL_OPEN_DAYS"));
	      //End of opendays button code
	      searchResultContentsVO.setProviderId(resultset.getString("PROVIDER_ID"));//Added by Sangeeth.S for the smart pixel logging college id      
	      searchResultContentsVO.setSponsoredListFlag(resultset.getString("STD_ADVERT_TYPE"));
	      searchResultContentsVO.setEventCategoryId(resultset.getString("EVENT_CATEGORY_ID"));
	      searchResultContentsVO.setEventCategoryName(resultset.getString("EVENT_CATEGORY_NAME"));     
	      ResultSet bestMatchCoursesRS = (ResultSet)resultset.getObject("BEST_MATCH_COURSES");
	      try {
	        if (bestMatchCoursesRS != null) {
	          ArrayList bestMatchCoursesList = new ArrayList();
	          CourseSpecificListVO courseSpecificListVO = null;
	          while (bestMatchCoursesRS.next()) {
	            courseSpecificListVO = new CourseSpecificListVO();
	            courseSpecificListVO.setCourseId(bestMatchCoursesRS.getString("COURSE_ID"));
	            courseSpecificListVO.setCourseTitle(bestMatchCoursesRS.getString("COURSE_TITLE"));
	            courseSpecificListVO.setCourseUcasTariff(bestMatchCoursesRS.getString("UCAS_TARIFF"));
	            courseSpecificListVO.setCourseDomesticFees(bestMatchCoursesRS.getString("COURSE_DOM_FEES"));
	            courseSpecificListVO.setWasThisCourseShortlisted(bestMatchCoursesRS.getString("COURSE_IN_BASKET"));
	            courseSpecificListVO.setStudyLevelDesc(bestMatchCoursesRS.getString("SEO_STUDY_LEVEL_TEXT"));
	            courseSpecificListVO.setStudyLevelDescDet(bestMatchCoursesRS.getString("STUDY_LEVEL_DESC"));
	            courseSpecificListVO.setUcasCode(bestMatchCoursesRS.getString("UCAS_CODE"));
	            courseSpecificListVO.setSubOrderItemId(bestMatchCoursesRS.getString("SUBORDER_ITEM_ID"));
	            courseSpecificListVO.setOrderItemId(bestMatchCoursesRS.getString("ORDER_ITEM_ID"));
	            courseSpecificListVO.setSubOrderEmail(bestMatchCoursesRS.getString("EMAIL"));
	            courseSpecificListVO.setSubOrderProspectus(bestMatchCoursesRS.getString("PROSPECTUS"));
	            courseSpecificListVO.setFeeDuration(bestMatchCoursesRS.getString("FEE_DURATION"));
	            courseSpecificListVO.setSubOrderWebsite(bestMatchCoursesRS.getString("WEBSITE"));
	            courseSpecificListVO.setSubOrderEmailWebform(bestMatchCoursesRS.getString("EMAIL_WEBFORM"));
	            courseSpecificListVO.setSubOrderProspectusWebform(bestMatchCoursesRS.getString("PROSPECTUS_WEBFORM"));
	            courseSpecificListVO.setMyhcProfileId(bestMatchCoursesRS.getString("MYHC_PROFILE_ID"));
	            courseSpecificListVO.setProfileType(bestMatchCoursesRS.getString("PROFILE_TYPE"));
	            courseSpecificListVO.setApplyNowFlag(bestMatchCoursesRS.getString("APPLY_NOW_FLAG"));
	            courseSpecificListVO.setMediaId(bestMatchCoursesRS.getString("MEDIA_ID"));
	            courseSpecificListVO.setMediaPath(bestMatchCoursesRS.getString("MEDIA_PATH"));
	            courseSpecificListVO.setMediaType(bestMatchCoursesRS.getString("MEDIA_TYPE"));
	            courseSpecificListVO.setCpeQualificationNetworkId(bestMatchCoursesRS.getString("NETWORK_ID"));
	            courseSpecificListVO.setPageName(bestMatchCoursesRS.getString("PAGE_NAME"));
	            courseSpecificListVO.setCourseDetailsPageURL(bestMatchCoursesRS.getString("CD_PAGE_URL"));
	            courseSpecificListVO.setWebformPrice(bestMatchCoursesRS.getString("WEBFORM_PRICE"));
	            courseSpecificListVO.setWebsitePrice(bestMatchCoursesRS.getString("WEBSITE_PRICE"));	          
	            courseSpecificListVO.setEmploymentRate(bestMatchCoursesRS.getString("EMPLOYMENT_RATE"));
	            courseSpecificListVO.setCourseRank(bestMatchCoursesRS.getString("COURSE_RANK"));
	            courseSpecificListVO.setAddedToFinalChoice(bestMatchCoursesRS.getString("FINAL_CHOICES_EXIST_FLAG"));  	           
	            bestMatchCoursesList.add(courseSpecificListVO);               
	          }          
	          searchResultContentsVO.setBestMatchCoursesList(bestMatchCoursesList);
	        }
	        searchResultContentsVO.setReviewCount(resultset.getString("REVIEW_COUNT"));
	        searchResultContentsVO.setUniReviewsURL(resultset.getString("REVIEW_PAGE_URL"));
	      } catch (Exception e) {
	        e.printStackTrace();
	      } finally {
	        try {
	          if (bestMatchCoursesRS != null) {
	            bestMatchCoursesRS.close();
	            bestMatchCoursesRS = null;
	          }
	        } catch (Exception e) {
	          throw new SQLException(e.getMessage());
	        }
	      }
	      return searchResultContentsVO;
	    }
	  }
	  //
	  private class InstitutionIdsLoggingRowMapperImpl implements RowMapper {
	    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	      OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
	      omnitureLoggingVO.setInstitutionIdsForThisSearch(resultSet.getString("INSTITUTION_IDS"));
	      return omnitureLoggingVO;
	    }  
	  }
	   //
	   private class FeaturedBrandRowMapperImpl implements RowMapper {
		 public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		   FeaturedBrandVO featuredBrandVO = new FeaturedBrandVO();
		   featuredBrandVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
		   featuredBrandVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
		   featuredBrandVO.setMediaId(resultSet.getString("MEDIA_ID")); 
		   featuredBrandVO.setMediaPath(resultSet.getString("MEDIA_PATH"));
		   featuredBrandVO.setThumbnailPath(resultSet.getString("THUMBNAIL_PATH"));
		   featuredBrandVO.setMediaType(resultSet.getString("MEDIA_TYPE"));
		   featuredBrandVO.setHeadline(resultSet.getString("HEADLINE"));
		   featuredBrandVO.setProfileHeadlineUrl(resultSet.getString("PROFILE_HEADLINE_URL"));
		   featuredBrandVO.setTagline(resultSet.getString("TAGLINE"));
		   featuredBrandVO.setReviewDisplayflag(resultSet.getString("REVIEW_DISPLAY_FLAG"));
		   featuredBrandVO.setReviewCount(resultSet.getString("REVIEW_COUNT"));
		   featuredBrandVO.setOverallRating(resultSet.getString("OVERALL_RATING"));
		   featuredBrandVO.setOverallRatingExact(resultSet.getString("OVERALL_RATING_EXACT"));
	       featuredBrandVO.setNavigationText(resultSet.getString("NAVIGATION_TEXT"));
		   featuredBrandVO.setNavigationUrl(resultSet.getString("NAVIGATION_URL"));
		   featuredBrandVO.setOrderItemId(resultSet.getString("ORDER_ITEM_ID"));
		   featuredBrandVO.setUniReviewUrl(new SeoUrls().constructReviewPageSeoUrl(featuredBrandVO.getCollegeName(), featuredBrandVO.getCollegeId()));
		   return featuredBrandVO;
		 }
	   }
	   
	   private class SEODetails implements RowMapper {
	     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	       SEOMetaDetailsVO	seoMetaDetailsVO = new SEOMetaDetailsVO();
	       seoMetaDetailsVO.setMetaTitle(resultSet.getString("META_TITLE")); 
	       seoMetaDetailsVO.setMetaDesc(resultSet.getString("META_DESC"));
	       seoMetaDetailsVO.setMetaKeyword(resultSet.getString("META_KEYWORD"));
	       seoMetaDetailsVO.setMetaRobots(resultSet.getString("META_ROBO"));
	       seoMetaDetailsVO.setCategoryDesc(resultSet.getString("CATEGORY_DESC"));
	       seoMetaDetailsVO.setStudyDesc(resultSet.getString("STUDY_DESC"));
	       return seoMetaDetailsVO;
	     }
	  }
	   private class BreadCrumbSchemaTagRowMapperImpl implements RowMapper {
		 public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		   BreadCrumbVO	breadCrumbVO = new BreadCrumbVO();
		   breadCrumbVO.setBreadcrumbUrl(resultSet.getString("BC_URL")); 
		   breadCrumbVO.setBradcrumbText(resultSet.getString("BC_URL_DESC"));
		   breadCrumbVO.setItemId(resultSet.getString("DISPLAY_SEQ"));
	       return breadCrumbVO;
		  }
	   }
}
