package spring.dao.sp.searchresult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.pojo.searchresult.GetInstitutionNameBean;
import spring.valueobject.searchresult.GetInstitutionNameVO;

public class GetInstitutionNameAjaxSP extends StoredProcedure{

	public GetInstitutionNameAjaxSP (DataSource dataSource) {
	
		setDataSource(dataSource);
		setFunction(true);
		setSql(ProcedureNames.GET_AUTOCOMPLETE_COLLEGE_VALUES_FN);
		declareParameter(new SqlOutParameter("LC_GET_COLLEGE", OracleTypes.CURSOR, new GetInstitutionNameRowMapperImpl()));
		declareParameter(new SqlParameter("P_TEXT", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_PAGE_NAME", OracleTypes.VARCHAR));
		
		compile();
	}
	
	public Map<String, Object> execute(GetInstitutionNameBean institutionNameBean) {
		
		Map<String, Object> outMap = null;
		Map<String,String> inMap = new HashMap<>();
		try {
		inMap.put("P_TEXT", institutionNameBean.getKeywordText());
		inMap.put("P_PAGE_NAME", institutionNameBean.getPageName()); System.out.println(inMap + "inmap");
		outMap = execute(inMap); System.out.println(outMap + "outmap");
	} catch (Exception e) {
	      e.printStackTrace();
	    }
		return outMap;
	}
	
	public class GetInstitutionNameRowMapperImpl implements RowMapper<Object> {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			GetInstitutionNameVO institutionNameVO = new GetInstitutionNameVO();	
			institutionNameVO.setCollegeId(rs.getString("COLLEGE_ID"));
			institutionNameVO.setCollegeName(rs.getString("COLLEGE_NAME"));
			institutionNameVO.setCollegeNameAlias(rs.getString("COLLEGE_NAME_ALIAS"));
			institutionNameVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
			institutionNameVO.setCollegeLocation(rs.getString("COLLEGE_LOCATION"));
			institutionNameVO.setOdMsg(rs.getString("OD_MSG"));
			institutionNameVO.setReviewMsg(rs.getString("REVIEW_MSG"));
			institutionNameVO.setCollegeTextKey(rs.getString("COLLEGE_TEXT_KEY"));
			return institutionNameVO;
		}
	}
}