package spring.dao.sp.searchresult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.valueobject.searchresult.FooterSectionVO;

public class GetFooterSectionSP extends StoredProcedure {

	public GetFooterSectionSP(DataSource dataSource) {

		setDataSource(dataSource);
		setFunction(true);
		setSql(ProcedureNames.GET_FOOTER_SECTION_FN);
		declareParameter(new SqlOutParameter("LC_TEXT_ID", OracleTypes.CURSOR, new FooterContentsRowMapperImpl()));
		declareParameter(new SqlParameter("P_TYPE", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.VARCHAR));
		compile();
	}

	public Map<String, Object> execute(GetFooterSectionBean getFooterSectionBean) throws Exception {

		Map<String, String> inMap = new HashMap<>();
		inMap.put("P_TYPE", getFooterSectionBean.getType());
		inMap.put("P_COLLEGE_ID", getFooterSectionBean.getCollegeId());
		return execute(inMap);
	}

	private class FooterContentsRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			FooterSectionVO footerSectionVO = new FooterSectionVO();
			footerSectionVO.setFooterHtmlText(resultSet.getClob("HTML_TEXT"));
			return footerSectionVO;
		}
	}
}
