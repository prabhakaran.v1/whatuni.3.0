package spring.dao.sp.advice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import oracle.jdbc.OracleTypes;
import spring.valueobject.advice.AdviceHomeInfoVO;

public class CovidSnippetPodSP extends StoredProcedure {

	public CovidSnippetPodSP(DataSource datasource) {
	    
	    setDataSource(datasource);
	    setSql(SpringConstants.COVID_SNIPPET_AJAX);    
	    declareParameter(new SqlParameter("P_VIEW_MORE", Types.NUMERIC));   
	    declareParameter(new SqlOutParameter("OC_COVID_SNIPPET", OracleTypes.CURSOR, new CovidSnippetInfoRowMapper()));
	    compile();
	  }
	
	 public Map execute(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception {

		    Map inMap = new HashMap();
		    inMap.put("P_VIEW_MORE", adviceHomeInfoVO.getPageNo());
		    return execute(inMap);
		  }
	 
	 private class CovidSnippetInfoRowMapper implements RowMapper {    
		    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {    
		      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
		      adviceHomeInfoVO.setPostText(rs.getString("POST_TEXT"));
		      adviceHomeInfoVO.setArticleDate(rs.getString("ARTICLE_DATE"));
		      adviceHomeInfoVO.setArticleTime(rs.getString("ARTICLE_TIME"));
		      adviceHomeInfoVO.setCovidNewsTotalCount(rs.getString("TOTAL_COUNT"));
		      return adviceHomeInfoVO;
		    }    
		  }
}
