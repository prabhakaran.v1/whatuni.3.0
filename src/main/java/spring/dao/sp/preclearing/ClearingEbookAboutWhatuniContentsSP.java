package spring.dao.sp.preclearing;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : ClearingEbookAboutWhatuniContentsSP.java
* Description   : This class is used to get the SHTML contents from Database
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	             Change
* ************************************************************************************************************************************** 
* Sujitha.V               1.0			     
* */

public class ClearingEbookAboutWhatuniContentsSP extends StoredProcedure{

  public ClearingEbookAboutWhatuniContentsSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(ProcedureNames.GET_PRE_CLEARING_ABOUT_WU_PRC);
    declareParameter(new SqlOutParameter("P_HTML_CONTENT", OracleTypes.CLOB));
    compile();
  }
  public Map execute() {
    Map outMap = null;
    Map inMap = new HashMap();
    outMap = execute(inMap);
    return outMap;
  }
}
