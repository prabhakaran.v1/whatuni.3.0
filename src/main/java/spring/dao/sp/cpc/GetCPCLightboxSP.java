package spring.dao.sp.cpc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuni.util.seo.SeoUrls;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.cpc.CPCLightboxVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : GetCPCLightboxSP.java
 * Description   : Getting lightbox contents for both single and multiple cpc landing pages.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Jeyalakshmi.D           1.0             06.02.2020        First draft
 * Keerthana.E             2.0             21.07.2020        Added Hotline No and Profile Media Flag for clearing
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public class GetCPCLightboxSP extends StoredProcedure {
  SeoUrls seoUrls = new SeoUrls();

  public GetCPCLightboxSP(DataSource datasource) throws Exception {
    setDataSource(datasource);
	setSql(ProcedureNames.GET_LANDING_LIGHTBOX_PRC);
	declareParameter(new SqlParameter("P_LANDING_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_QUALIFICATION_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_BROWSE_CAT_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_PREVIEW_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("PC_IMAGE_DETAILS", OracleTypes.CURSOR,new ImageListRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_UNI_DETAILS", OracleTypes.CURSOR, new UniDetailsListRowMapperImpl()));      
	declareParameter(new SqlOutParameter("PC_LATEST_REVIEW", OracleTypes.CURSOR, new LatestReviewRowMapperImpl()));
	compile();
  }

  public Map execute(CPCLightboxVO cPCLightboxVO) throws Exception {
    Map outMap = null;
    Map inputMap = new HashMap();
    inputMap.put("P_LANDING_ID", cPCLightboxVO.getLandingId());
    inputMap.put("P_COLLEGE_ID", cPCLightboxVO.getCollegeId());
    inputMap.put("P_QUALIFICATION_ID", cPCLightboxVO.getQualificationId());
    inputMap.put("P_BROWSE_CAT_ID", cPCLightboxVO.getBrowseCatId());
    inputMap.put("P_REGION_ID", cPCLightboxVO.getRegionId());
    inputMap.put("P_PREVIEW_FLAG", cPCLightboxVO.getPreviewFlag());
	
    outMap = execute(inputMap);
	
    return outMap;
  }
  private class ImageListRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCLightboxVO cPCLightboxVO = new CPCLightboxVO();
	  cPCLightboxVO.setMediaId(rs.getString("MEDIA_ID"));
	  cPCLightboxVO.setMediaPath(rs.getString("MEDIA_PATH"));
	  cPCLightboxVO.setThumbnailPath(rs.getString("THUMBNAIL_PATH"));
	  cPCLightboxVO.setMediaType(rs.getString("MEDIA_TYPE"));
	  cPCLightboxVO.setContentHubFlag(rs.getString("CONTENTHUB_FLAG"));
	  cPCLightboxVO.setClearingProfileMediaFlag(rs.getString("CLEARING_PROFILE_MEDIA_FLAG"));
	  return cPCLightboxVO;
	}
  }
  private class UniDetailsListRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCLightboxVO cPCLightboxVO = new CPCLightboxVO();
	  cPCLightboxVO.setCollegeId(rs.getString("COLLEGE_ID"));
	  cPCLightboxVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
	  cPCLightboxVO.setCollegeName(rs.getString("COLLEGE_NAME"));
	  cPCLightboxVO.setOverAllRating(rs.getString("OVERALL_RATING")); 
	  cPCLightboxVO.setOverAllRatingExact(rs.getString("OVERALL_RATING_EXACT")); 
	  cPCLightboxVO.setReviewCount(rs.getString("REVIEW_COUNT"));
	  cPCLightboxVO.setUrlFormingCode(rs.getString("URL_FORMING_CODE"));
	  cPCLightboxVO.setQualCode(rs.getString("QUAL_CODE"));
	  cPCLightboxVO.setSubjectName(rs.getString("SUBJECT_NAME"));
	  cPCLightboxVO.setRegionName(rs.getString("REGION_NAME"));
	  cPCLightboxVO.setButtonLabel(rs.getString("BUTTON_LABEL"));
	  cPCLightboxVO.setEmail(rs.getString("EMAIL"));
      cPCLightboxVO.setEmailWebForm(rs.getString("EMAIL_WEBFORM"));
      cPCLightboxVO.setProspectus(rs.getString("PROSPECTUS"));
      cPCLightboxVO.setProspectusWebForm(rs.getString("PROSPECTUS_WEBFORM"));
      cPCLightboxVO.setWebsite(rs.getString("WEBSITE"));
      cPCLightboxVO.setHotlineNo(rs.getString("HOTLINE"));
      cPCLightboxVO.setSubOrderItemId(rs.getString("SUBORDER_ITEM_ID"));
      cPCLightboxVO.setOrderItemId(rs.getString("ORDER_ITEM_ID"));
      cPCLightboxVO.setNetworkId(rs.getString("NETWORK_ID"));
      cPCLightboxVO.setWebformprice(rs.getString("WEBFORM_PRICE"));
      cPCLightboxVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
      cPCLightboxVO.setOpenDate(rs.getString("OPEN_DATE"));
      cPCLightboxVO.setOpenDay(rs.getString("OPEN_DAY"));
      cPCLightboxVO.setOpenMonthYear(rs.getString("OPEN_MONTH_YEAR"));
      cPCLightboxVO.setBookingUrl(rs.getString("BOOKING_URL"));
      cPCLightboxVO.setTotalOpenDays(rs.getString("TOTAL_OPEN_DAYS"));
      cPCLightboxVO.setCourseCount(rs.getString("COURSE_COUNT"));
      cPCLightboxVO.setStudyLevel(rs.getString("QUAL_DESC"));
      cPCLightboxVO.setProviderResultsURL(seoUrls.constructCourseUrl(cPCLightboxVO.getQualCode(), "NORMAL", cPCLightboxVO.getCollegeName(), cPCLightboxVO.getSubjectName(), cPCLightboxVO.getRegionName()));
	  cPCLightboxVO.setUniReviewUrl(new SeoUrls().constructReviewPageSeoUrl(cPCLightboxVO.getCollegeName(), cPCLightboxVO.getCollegeId()));
	  if(!GenericValidator.isBlankOrNull(cPCLightboxVO.getCollegeId())) {
		cPCLightboxVO.setUniHomeURL(seoUrls.construnctUniHomeURL(cPCLightboxVO.getCollegeId(), cPCLightboxVO.getCollegeName()));
	  }
	  return cPCLightboxVO;
	}
  }
  private class LatestReviewRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCLightboxVO cPCLightboxVO = new CPCLightboxVO();
	  cPCLightboxVO.setCollegeId(rs.getString("COLLEGE_ID"));
	  cPCLightboxVO.setReviewId(rs.getString("REVIEW_ID"));
	  cPCLightboxVO.setUserId(rs.getString("USER_ID"));
	  cPCLightboxVO.setReviewTitle(rs.getString("REVIEW_TITLE"));
	  cPCLightboxVO.setQuestionDesc(rs.getString("QUESTION_DESC"));
	  cPCLightboxVO.setCollegeName(rs.getString("COLLEGE_NAME"));
	  cPCLightboxVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
	  cPCLightboxVO.setUserName(rs.getString("USER_NAME"));
	  cPCLightboxVO.setCourseName(rs.getString("COURSE_NAME"));
	  cPCLightboxVO.setCourseDeletedFlag(rs.getString("COURSE_DELETED_FLAG"));
      cPCLightboxVO.setCourseId(rs.getString("COURSE_ID"));
	  cPCLightboxVO.setCreatedDate(rs.getString("CREATED_DATE"));
	  cPCLightboxVO.setStudyLevelText(rs.getString("STUDY_LEVEL_TEXT"));
	  if(!GenericValidator.isBlankOrNull(cPCLightboxVO.getCollegeId())) {
		cPCLightboxVO.setUniHomeURL(seoUrls.construnctUniHomeURL(cPCLightboxVO.getCollegeId(), cPCLightboxVO.getCollegeName()));
	  }
	  if((!GenericValidator.isBlankOrNull(cPCLightboxVO.getCourseId()) || !"0".equalsIgnoreCase(cPCLightboxVO.getCourseId()))){
	    cPCLightboxVO.setCourseDetailsURL(seoUrls.courseDetailsPageURL(cPCLightboxVO.getCourseName(), cPCLightboxVO.getCourseId(), cPCLightboxVO.getCollegeId()));
      }
	  ResultSet viewMoreRS = (ResultSet)rs.getObject("VIEW_MORE");
	  if(viewMoreRS != null) {
	    ArrayList userMoreReviewaList = new ArrayList();
	    try {
	      CPCLightboxVO cpcInnerReviewsVO = null;
	      while (viewMoreRS.next()) {
	    	cpcInnerReviewsVO = new CPCLightboxVO();
	    	cpcInnerReviewsVO.setReviewQuestionId(viewMoreRS.getString("REVIEW_QUESTION_ID"));
	    	cpcInnerReviewsVO.setQuestionTitle(viewMoreRS.getString("QUESTION_TITLE"));
	    	cpcInnerReviewsVO.setQuestionDesc(viewMoreRS.getString("QUESTION_DESC"));
	    	cpcInnerReviewsVO.setAnswer(viewMoreRS.getString("ANSWER"));
	    	cpcInnerReviewsVO.setRating(viewMoreRS.getString("RATING"));
	    	cpcInnerReviewsVO.setOrdValue(viewMoreRS.getString("ORD_VALUE"));
	    	userMoreReviewaList.add(cpcInnerReviewsVO);
          }
          cPCLightboxVO.setUserMoreReviewaList(userMoreReviewaList);
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          if (viewMoreRS != null) {
            viewMoreRS.close();
          }         
        }
      }
      cPCLightboxVO.setUserNameInt(rs.getString("USER_NAME_INT"));
      return cPCLightboxVO;
    }
  }
}	

