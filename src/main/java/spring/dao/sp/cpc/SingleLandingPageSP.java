package spring.dao.sp.cpc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.seo.SeoUrls;

import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : SingleLandingPageSP.java
 * Description   : Getting Single page content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Jeyalakshmi.D             1.0               04.02.2020        First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public class SingleLandingPageSP extends StoredProcedure {
  SeoUrls seoUrls = new SeoUrls();

  public SingleLandingPageSP(DataSource datasource) throws Exception {
    setDataSource(datasource);
	setSql(ProcedureNames.GET_SINGLE_CPC_PAGE_VALUE);
	declareParameter(new SqlParameter("P_LANDING_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_BROWSE_CAT_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_SORT_BY", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_PAGE_NO", OracleTypes.NUMERIC));
	declareParameter(new SqlParameter("P_PREVIEW_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_AJAX_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_WHATUNI_LOGO_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_HAMBURGER_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_FILTERS_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_SORTING_DROPDOWN_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_COURSE_COUNT_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_COURSE_COUNT", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("PC_TOP_NAV_ARTICLE_LINKS", OracleTypes.CURSOR, new TopNavArticlesListRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_SUBJECT_LIST", OracleTypes.CURSOR,new subjectListRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_PROVIDER_DETAILS", OracleTypes.CURSOR, new ProviderDetailsListRowMapperImpl()));      
	declareParameter(new SqlOutParameter("PC_COURSE_DETAILS", OracleTypes.CURSOR, new courseDetailsRowMapperImpl()));
    compile();
  }

  public Map execute(CPCPageVO cPCPageVO) throws Exception {
    Map outMap = null;
    Map inputMap = new HashMap();
    inputMap.put("P_LANDING_ID", cPCPageVO.getLandingId());
    inputMap.put("P_BROWSE_CAT_ID", cPCPageVO.getBrowseCatId());
    inputMap.put("P_SORT_BY", cPCPageVO.getSortBy());
    inputMap.put("P_PAGE_NO", cPCPageVO.getPageNo());
    inputMap.put("P_PREVIEW_FLAG", cPCPageVO.getPreviewFlag());
    inputMap.put("P_AJAX_FLAG", cPCPageVO.getAjaxFlag());
    outMap = execute(inputMap);
    return outMap;
  }
  
  private class TopNavArticlesListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();
	  cPCPageVO.setBlogCategoryId(rs.getString("blog_category_id"));
	  cPCPageVO.setBlogCategoryName(rs.getString("blog_category_name"));
	  cPCPageVO.setBlogUrl(rs.getString("blog_url"));
      return cPCPageVO;    	
    }
  }
  
  private class subjectListRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();
	  cPCPageVO.setBrowseCatId(rs.getString("BROWSE_CAT_ID"));
	  cPCPageVO.setBrowseCatDesc(rs.getString("BROWSE_CAT_DESC"));
	  cPCPageVO.setParentCategoryCode(rs.getString("PARENT_CATEGORY_CODE"));
	  cPCPageVO.setSelectedSubjectFlag(rs.getString("SELECTED_SUBJECT_FLAG"));
	  return cPCPageVO;
	}
  }
  private class ProviderDetailsListRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();
	  cPCPageVO.setHeroImagePath(rs.getString("HERO_MEDIA_PATH"));
	  cPCPageVO.setHeroImageType(rs.getString("HERO_MEDIA_TYPE"));	
	  cPCPageVO.setHeroImageThumbnail(rs.getString("HERO_THUMBNAIL_PATH"));
	  cPCPageVO.setCollegeName(rs.getString("COLLEGE_NAME"));
	  cPCPageVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
	  cPCPageVO.setCollegeId(rs.getString("COLLEGE_ID"));
	  cPCPageVO.setSubTitle(rs.getString("SUBTITLE"));
	  cPCPageVO.setOverAllRating(rs.getString("OVERALL_RATING")); 
	  cPCPageVO.setOverAllRatingExact(rs.getString("OVERALL_RATING_EXACT")); 
	  cPCPageVO.setReviewCount(rs.getString("REVIEW_COUNT"));
	  cPCPageVO.setEditorialMediaPath(rs.getString("EDITORIAL_MEDIA_PATH"));
	  cPCPageVO.setEditorialMediaType(rs.getString("EDITORIAL_MEDIA_TYPE"));
	  cPCPageVO.setThumbnailPath(rs.getString("EDITORIAL_THUMBNAIL_PATH"));
	  cPCPageVO.setContentHubMediadFlag(rs.getString("CONTENTHUB_MEDIA_FLAG"));
	  cPCPageVO.setEditorialContent(rs.getString("EDITORIAL_CONTENT"));
	  cPCPageVO.setStudyLevel(rs.getString("QUAL_DESC"));
	  //cPCPageVO.setUniHomeURL(seoUrls.construnctUniHomeURL(cPCPageVO.getCollegeId(), cPCPageVO.getCollegeName()));
	  return cPCPageVO;
    } 
  }
  private class courseDetailsRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();	 
	  cPCPageVO.setCourseId(rs.getString("COURSE_ID"));
	  cPCPageVO.setCourseTitle(rs.getString("COURSE_TITLE"));
	  cPCPageVO.setCollegeName(rs.getString("COLLEGE_NAME"));
	  cPCPageVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
	  cPCPageVO.setEntryRequirments(rs.getString("ENTRY_REQUIREMENTS"));
	  ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            cPCPageVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));
            cPCPageVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));            
            cPCPageVO.setEmail(enquiryDetailsRS.getString("EMAIL"));
            cPCPageVO.setEmailWebForm(enquiryDetailsRS.getString("EMAIL_WEBFORM"));
            cPCPageVO.setProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            cPCPageVO.setProspectusWebForm(enquiryDetailsRS.getString("PROSPECTUS_WEBFORM"));
            cPCPageVO.setWebsite(enquiryDetailsRS.getString("WEBSITE"));
            cPCPageVO.setNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
            cPCPageVO.setWebformprice(enquiryDetailsRS.getString("WEBFORM_PRICE"));
            cPCPageVO.setWebsitePrice(enquiryDetailsRS.getString("WEBSITE_PRICE"));
            cPCPageVO.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        if (enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      }
      cPCPageVO.setSeoStudyLevelText(rs.getString("SEO_STUDY_LEVEL"));
      cPCPageVO.setCollegeId(rs.getString("COLLEGE_ID"));
      cPCPageVO.setCourseDetailUrl(seoUrls.construnctCourseDetailsPageURL(cPCPageVO.getSeoStudyLevelText(), cPCPageVO.getCourseTitle(), cPCPageVO.getCourseId(), cPCPageVO.getCollegeId(), "NORMAL_COURSE"));//24_JUN_2014
	  cPCPageVO.setOpenDate(rs.getString("OPEN_DATE"));
      cPCPageVO.setOpenDay(rs.getString("OPEN_DAY"));
      cPCPageVO.setOpenMonthYear(rs.getString("OPEN_MONTH_YEAR"));
      cPCPageVO.setBookingURL(rs.getString("BOOKING_URL"));
      cPCPageVO.setTotalOpenDay(rs.getString("TOTAL_OPEN_DAYS"));
      cPCPageVO.setViewCoursesFlag(rs.getString("VIEW_COURSES_FLAG"));
      return cPCPageVO;
	}
  }	
}
