package spring.dao.sp.cpc;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
 * PAGE_NAME: CPC Landing Page
 * @author  Hema.S
 * @since   wu598_20200204 
 * Change Log :
 * *************************************************************************************************************************************
 * author                 Ver            Modified On     Modification Details    Change
 * *************************************************************************************************************************************
 * Keerthana.E            2.0             21.07.2020      Added Clearing Flag
 */

public class GetCPCTypeValuesSP extends StoredProcedure {
  public GetCPCTypeValuesSP(DataSource datasource) throws Exception {
    setDataSource(datasource);
	setSql(ProcedureNames.GET_CPC_TYPE_VALUE);
	declareParameter(new SqlParameter("P_LANDING_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_PREVIEW_FLAG", OracleTypes.VARCHAR));	      
	declareParameter(new SqlOutParameter("P_LANDING_PAGE_TYPE", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_FALLBACK_URL", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_STATUS", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_SHOW_CLEARING_DATA_FLAG", OracleTypes.VARCHAR));
  }

  public Map execute(CPCPageVO cPCPageVO) throws Exception {
    Map outMap = null;
    Map inputMap = new HashMap();
    inputMap.put("P_LANDING_ID", cPCPageVO.getLandingId());
    inputMap.put("P_PREVIEW_FLAG", cPCPageVO.getPreviewFlag());
    outMap = execute(inputMap);
    return outMap;
  }
} 


