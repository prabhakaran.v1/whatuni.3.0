package spring.dao.sp.cpc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.seo.SeoUrls;

import WUI.search.form.SearchBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : MultipleLandingPageSP.java
 * Description   : Getting multiple page content.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Hema.S                  1.0            04.02.2020        First draft
 * Keerthana.E             2.0            21.07.2020        Added Clearing Flag, Random no Param and Hotline no column for clearing
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public class MultipleLandingPageSP extends StoredProcedure {
  SeoUrls seoUrl = new SeoUrls();

  public MultipleLandingPageSP(DataSource datasource) throws Exception {
    setDataSource(datasource);
	setSql(ProcedureNames.GET_MULTIPLE_CPC_PAGE_VALUE);
	declareParameter(new SqlParameter("P_LANDING_ID", Types.NUMERIC));
	
	declareParameter(new SqlParameter("p_browse_cat_id", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_ID", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_SORT_BY", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_PAGE_NO", Types.NUMERIC));
	declareParameter(new SqlParameter("P_PREVIEW_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_AJAX_CALL_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("PC_TOP_NAV_ARTICLE_LINKS", OracleTypes.CURSOR, new TopNavArticlesListRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_LANDING_PAGE_DET", OracleTypes.CURSOR ,new landingPageDetailsRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_PROVIDER_DET_LIST", OracleTypes.CURSOR,new getProviderDetailsRowMapperImpl()));   
	declareParameter(new SqlOutParameter("PC_SUBJECT_FILTER", OracleTypes.CURSOR, new subjectFilterRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_LOCATION_FILTER", OracleTypes.CURSOR, new regionFilterRowMapperImpl()));
	declareParameter(new SqlOutParameter("P_HAMBURGER_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_WHATUNI_LOGO_FLAG", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("p_filters_flag", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("p_sorting_flag", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_RECORD_COUNT", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("P_RESULT_EXISTS", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_SHOW_CLEARING_DATA_FLAG", OracleTypes.VARCHAR));
	compile();
  }

  public Map execute(CPCPageVO cPCPageVO) throws Exception {
    Map outMap = null;
    Map inputMap = new HashMap();
    inputMap.put("P_LANDING_ID", cPCPageVO.getLandingId());
    inputMap.put("p_browse_cat_id", cPCPageVO.getBrowseCatId());
    inputMap.put("P_REGION_ID", cPCPageVO.getRegionId());
    inputMap.put("P_SORT_BY", cPCPageVO.getSortingCode());
    inputMap.put("P_PAGE_NO", cPCPageVO.getPageNo());
    inputMap.put("P_PREVIEW_FLAG", cPCPageVO.getPreviewFlag());
    inputMap.put("P_AJAX_CALL_FLAG", cPCPageVO.getAjaxFlag());
    inputMap.put("P_DYNAMIC_RANDOM_NUMBER", cPCPageVO.getDynamicRandomNumber());
    outMap = execute(inputMap);
    return outMap;
  }
  
  private class TopNavArticlesListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CPCPageVO cPCPageVO = new CPCPageVO();
	  cPCPageVO.setBlogCategoryId(rs.getString("blog_category_id"));
	  cPCPageVO.setBlogCategoryName(rs.getString("blog_category_name"));
	  cPCPageVO.setBlogUrl(rs.getString("blog_url"));
	  return cPCPageVO;    	
	}
  }
  
  private class landingPageDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();
	  cPCPageVO.setTitle(rs.getString("title"));
	  //System.out.println("title---->>"+cPCPageVO.getTitle());
	  cPCPageVO.setSubTitle(rs.getString("sub_title"));
	  cPCPageVO.setEditorialContent(rs.getString("editorial_content"));
	  cPCPageVO.setMediaId(rs.getString("media_id"));
	  cPCPageVO.setMediaPath(rs.getString("media_path"));
	  cPCPageVO.setMediaType(rs.getString("media_type"));
	  cPCPageVO.setStudyLevel(rs.getString("QUAL_DESC"));
	  //cPCPageVO.setStatus(rs.getString("status"));
	  //cPCPageVO.setFallbackURL(rs.getString("fallback_url"));
	  return cPCPageVO;
	}
  }
  private class getProviderDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CPCPageVO cPCPageVO = new CPCPageVO();
      seoUrl = new SeoUrls();
      cPCPageVO.setCollegeId(rs.getString("COLLEGE_ID"));
      cPCPageVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
      cPCPageVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      cPCPageVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
      cPCPageVO.setUniHomeURL(seoUrl.construnctUniHomeURL(cPCPageVO.getCollegeId(), cPCPageVO.getCollegeName()));
      cPCPageVO.setContentHubMediadFlag(rs.getString("CONTENTHUB_MEDIA_FLAG"));
      cPCPageVO.setMediaId(rs.getString("MEDIA_ID"));
      cPCPageVO.setMediaPath(rs.getString("MEDIA_PATH"));
      cPCPageVO.setThumbnailPath(rs.getString("THUMBNAIL_PATH"));  
      cPCPageVO.setMediaType(rs.getString("MEDIA_TYPE"));
      cPCPageVO.setReviewCount(rs.getString("REVIEW_COUNT"));
      cPCPageVO.setOverAllRating(rs.getString("OVERALL_RATING"));
      cPCPageVO.setOverAllRatingExact(rs.getString("OVERALL_RATING_EXACT"));
      cPCPageVO.setEmail(rs.getString("EMAIL"));
      cPCPageVO.setEmailWebForm(rs.getString("EMAIL_WEBFORM"));
      cPCPageVO.setProspectus(rs.getString("PROSPECTUS"));
      cPCPageVO.setProspectusWebForm(rs.getString("PROSPECTUS_WEBFORM"));
      cPCPageVO.setWebsite(rs.getString("WEBSITE"));
      cPCPageVO.setHotlineNo(rs.getString("HOTLINE"));
      cPCPageVO.setWebformprice(rs.getString("WEBFORM_PRICE"));
      cPCPageVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
      cPCPageVO.setOpenDate(rs.getString("OPEN_DATE"));
      cPCPageVO.setOpenDay(rs.getString("OPEN_DAY"));
      cPCPageVO.setOpenMonthYear(rs.getString("OPEN_MONTH_YEAR"));
      cPCPageVO.setBookingURL(rs.getString("BOOKING_URL"));
      cPCPageVO.setOpenDaySubOrderItemId(rs.getString("OPEN_DAY_SUBORDER_ITEM_ID"));
      cPCPageVO.setOpenDaysCount(rs.getString("OPEN_DAYS_COUNT"));
      cPCPageVO.setCourseCount(rs.getString("COURSE_COUNT"));
      //System.out.println("count-->"+cPCPageVO.getCourseCount());
      cPCPageVO.setOrderItemId(rs.getString("order_item_id"));
      cPCPageVO.setSubOrderItemId(rs.getString("suborder_item_id"));
      cPCPageVO.setNetworkId(rs.getString("network_id"));
      cPCPageVO.setViewCourseFlag(rs.getString("view_courses_flag"));
      cPCPageVO.setUrlFormCode(rs.getString("url_forming_code"));
      cPCPageVO.setQualCode(rs.getString("qual_code"));
      cPCPageVO.setSubjectNameURL(rs.getString("subject_name"));
      cPCPageVO.setRegionNameURL(rs.getString("region_name"));
      cPCPageVO.setProviderResultsURL(seoUrl.constructCourseUrl(cPCPageVO.getQualCode(), GlobalConstants.NORMAL_JOURNEY, cPCPageVO.getCollegeName(), cPCPageVO.getSubjectNameURL(), cPCPageVO.getRegionNameURL()));
      cPCPageVO.setClearingProfileMediaFlag(rs.getString("CLEARING_PROFILE_MEDIA_FLAG"));
	  return cPCPageVO;
	}
  }
  private class subjectFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    	CPCPageVO cPCPageVO = new CPCPageVO();
    	//System.out.println("subject-->");
    	cPCPageVO.setSubjectCode(rs.getString("browse_cat_id"));
    	cPCPageVO.setSubjectName(rs.getString("browse_cat_desc"));
    	
    	cPCPageVO.setSelectedSubjectFlag(rs.getString("selected_subject_flag"));
    	cPCPageVO.setParentCategoryCode(rs.getString("parent_category_code"));
    	//System.out.println("subject-->end");
    	return cPCPageVO;
	}
  }
  private class regionFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	  CPCPageVO cPCPageVO = new CPCPageVO();
	 // System.out.println("regin-->");
	  cPCPageVO.setRegionName(rs.getString("region_name"));
	  cPCPageVO.setParentReginName(rs.getString("top_region_name"));
	  cPCPageVO.setRegionId(rs.getString("region_id"));
	  cPCPageVO.setSelectedRegionFlag(rs.getString("selected_region_flag"));
	 // System.out.println("regin end-->");
	  return cPCPageVO;
    }
  }
}

