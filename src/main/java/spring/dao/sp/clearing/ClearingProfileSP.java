package spring.dao.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewSubjectRowMapperImpl;
import com.layer.util.rowmapper.search.UniRankingRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.contenthub.CampusVO;
import com.wuni.util.valueobject.contenthub.CollegeDetailsVO;
import com.wuni.util.valueobject.contenthub.CostOfLivingVO;
import com.wuni.util.valueobject.contenthub.EnquiryVO;
import com.wuni.util.valueobject.contenthub.GalleryVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.contenthub.PopularSubjectsVO;
import com.wuni.util.valueobject.contenthub.ReviewVO;
import com.wuni.util.valueobject.contenthub.SectionsVO;
import com.wuni.util.valueobject.contenthub.SocialVO;
import com.wuni.util.valueobject.contenthub.StudentStatsVO;
import com.wuni.util.valueobject.contenthub.UserDetailsVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import oracle.jdbc.OracleTypes;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

public class ClearingProfileSP extends StoredProcedure {

  public ClearingProfileSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.GET_CLEARING_PROFILE_PRC);
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_NETWORK_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_IP", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_BROWSER", OracleTypes.VARCHAR));
    //declareParameter(new SqlParameter("P_COLLEGE_SECTION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION", OracleTypes.VARCHAR));        
    declareParameter(new SqlOutParameter("O_COLLEGE_IN_BASKET", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_PIXEL_TRACKING_CODE", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("O_COURSE_EXIST_FLAG", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("O_ACCOMODATION_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_SCHOLARSHIP_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("OC_COLLEGE_DETAILS", OracleTypes.CURSOR, new CollegeDetailsRowMapperImpl()));    
    declareParameter(new SqlOutParameter("OC_ENQUIRY_INFO", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_PROFILE_SECTIONS", OracleTypes.CURSOR, new ProfileSectionsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_STATS_INFO", OracleTypes.CURSOR, new CollegeUnistatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_RAKING", OracleTypes.CURSOR, new UniRankingRowMapperImpl()));  
    declareParameter(new SqlOutParameter("OC_WUSCA_CATEGORIES_RATINGS", OracleTypes.CURSOR, new WuscaCategoriesRatingRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_OPEN_DAY_INFO", OracleTypes.CURSOR, new OpendayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_LATEST_UNI_REVIEWS", OracleTypes.CURSOR, new LatestUniReviewsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_STUDENT_UNI_REVIEWS", OracleTypes.CURSOR, new StudentUniReviewsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_REVIEW_SUBJECT", OracleTypes.CURSOR,new ReviewSubjectRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_REVIEW_BREAKDOWN", OracleTypes.CURSOR,new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_HERO_GALLERY", OracleTypes.CURSOR, new HeroGalleryRowMapperImpl())); 
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("OC_META_DETAILS", OracleTypes.CURSOR, new ClearingProfileSEODetails()));
    compile();
  }

  public Map execute(ClearingProfileVO clearingProfileVO) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_SESSION_ID", clearingProfileVO.getSessionId());
      inputMap.put("P_USER_ID", clearingProfileVO.getUserId());
      inputMap.put("P_BASKET_ID", clearingProfileVO.getBasketId());
      inputMap.put("P_COLLEGE_ID", clearingProfileVO.getCollegeId());
      inputMap.put("P_NETWORK_ID", clearingProfileVO.getNetworkId());      
      inputMap.put("P_CLIENT_IP", clearingProfileVO.getClientIp());
      inputMap.put("P_CLIENT_BROWSER", clearingProfileVO.getClientBrowser());
      //inputMap.put("P_COLLEGE_SECTION", clearingProfileVO.getCollegeSection());      
      inputMap.put("P_TRACK_SESSION", clearingProfileVO.getTrackSession());      
      inputMap.put("P_URL", clearingProfileVO.getClearingProfilePageURL());   
      //System.out.println(inputMap);
      outMap = execute(inputMap);
      //System.out.println(outMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }

  public class LatestUniReviewsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      SeoUrls seoUrl = new SeoUrls();
      ReviewVO reviewsVO = new ReviewVO();
      //System.out.println("LatestUniReviewsRowMapperImpl start");
      reviewsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      reviewsVO.setReviewId(resultset.getString("REVIEW_ID"));
      reviewsVO.setUserId(resultset.getString("USER_ID"));
      reviewsVO.setReviewTitle(resultset.getString("REVIEW_TITLE"));
      reviewsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      reviewsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      reviewsVO.setUserName(resultset.getString("USER_NAME"));
      reviewsVO.setOverAllRating(resultset.getString("OVERALL_RATING"));
      reviewsVO.setAtUni(resultset.getString("AT_UNI"));
      reviewsVO.setGender(resultset.getString("GENDER"));
      reviewsVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
      reviewsVO.setCourseId(resultset.getString("COURSE_ID"));
      reviewsVO.setCourseDeletedFlag(resultset.getString("COURSE_DELETED_FLAG"));
      reviewsVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
      reviewsVO.setReviewText(resultset.getString("REVIEW_TEXT"));
      reviewsVO.setReviewedDate(resultset.getString("REVIEWED_DATE"));
      reviewsVO.setUserNameInitial(resultset.getString("USER_NAME_INT"));
      reviewsVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(reviewsVO.getSeoStudyLevelText(), reviewsVO.getCourseTitle(), reviewsVO.getCourseId(), reviewsVO.getCollegeId(), "COURSE_SPECIFIC"));
      //System.out.println("LatestUniReviewsRowMapperImpl END");
      return reviewsVO;
    }

  }

  private class EnquiryInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      EnquiryVO enquiryInfoVO = new EnquiryVO();
      //System.out.println("EnquiryInfoRowMapperImpl start");
      enquiryInfoVO.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
      enquiryInfoVO.setEmail(resultset.getString("EMAIL"));
      enquiryInfoVO.setEmailWebform(resultset.getString("EMAIL_WEBFORM"));
      String emailFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmail()) || !GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebform())) ? "Y" : "N";
      String emailWebFormFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebform())) ? "Y" : "N";
      enquiryInfoVO.setEmailWebFormFlag(emailWebFormFlag);
      enquiryInfoVO.setEmailFlag(emailFlag);
      enquiryInfoVO.setProspectus(resultset.getString("PROSPECTUS"));
      enquiryInfoVO.setProspectusWebform(resultset.getString("PROSPECTUS_WEBFORM"));
      String prospectusFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getProspectus()) || !GenericValidator.isBlankOrNull(enquiryInfoVO.getProspectusWebform())) ? "Y" : "N";
      enquiryInfoVO.setProspectusFlag(prospectusFlag);
      enquiryInfoVO.setWebsite(resultset.getString("WEBSITE"));
      String websiteFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getWebsite())) ? "Y" : "N";
      enquiryInfoVO.setWebsiteFlag(websiteFlag);
      enquiryInfoVO.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      enquiryInfoVO.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID"));
      enquiryInfoVO.setProfileType(resultset.getString("PROFILE_TYPE"));
      enquiryInfoVO.setNetworkId(resultset.getString("NETWORK_ID"));
      enquiryInfoVO.setHotline(resultset.getString("HOTLINE"));
      enquiryInfoVO.setAdvertName(resultset.getString("ADVERT_NAME"));
      enquiryInfoVO.setWebsitePrice(resultset.getString("WEBSITE_PRICE"));
      enquiryInfoVO.setWebformPrice(resultset.getString("WEBFORM_PRICE"));
      //System.out.println("EnquiryInfoRowMapperImpl END");
      return enquiryInfoVO;
    }

  }

  public class ProfileSectionsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    	// System.out.println("sec1 st");
      SectionsVO sectionsVO = new SectionsVO();
      sectionsVO.setCollegeId(rs.getString("COLLEGE_ID"));
      sectionsVO.setDescription(rs.getString("DESCRIPTION"));
      //System.out.println(rs.getString("DESCRIPTION"));
      sectionsVO.setMediaId(rs.getString("MEDIA_ID"));
      sectionsVO.setImagePath(rs.getString("IMAGE_PATH"));            
      sectionsVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      String mediaType = null;
      if ("27".equals(sectionsVO.getMediaTypeId())) {
        mediaType = "IMAGE";
      }
      if ("29".equals(sectionsVO.getMediaTypeId())) {
        mediaType = "VIDEO";
      }
      sectionsVO.setMediaType(mediaType);      
      sectionsVO.setSectionNameDisplay(rs.getString("SECTION_NAME_DISPLAY"));
      sectionsVO.setSectionName(rs.getString("SECTION_NAME"));
      sectionsVO.setProfileId(rs.getString("PROFILE_ID"));
      sectionsVO.setMyhcProfileId(rs.getString("MYHC_PROFILE_ID"));
      sectionsVO.setMediaName(rs.getString("MEDIA_NAME"));
      sectionsVO.setThumbnailPath(rs.getString("THUMBNAIL_PATH"));      
      sectionsVO.setWuscaRank(rs.getString("WUSCA_RANK"));
      sectionsVO.setWuscaOverall(rs.getString("TOTAL_RANK_IN_CATEGORY"));
      sectionsVO.setAwardImage(rs.getString("AWARD_IMAGE")); 
      sectionsVO.setSubOrderItemId(rs.getString("SUBORDER_ITEM_ID"));
      //System.out.println("1 end");
      return sectionsVO;
      
    }

  }
 

  public class CollegeUnistatsInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudentStatsVO studentStatsVO = new StudentStatsVO();
      //System.out.println("CollegeUnistatsInfoRowMapperImpl start");
      studentStatsVO.setStatsDescription(rs.getString("STATS_DESC"));
      studentStatsVO.setStatsValue1(rs.getString("STAT_VALUE1"));
      studentStatsVO.setStatsValue2(rs.getString("STAT_VALUE2"));
      studentStatsVO.setCustomizedFlag(rs.getString("CUSTOMIZED_FLAG"));
      //System.out.println("CollegeUnistatsInfoRowMapperImpl end");
      return studentStatsVO;
    }

  }

  public class OpendayInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpendayVO opendayVO = new OpendayVO();
      //System.out.println("OpendayInfoRowMapperImpl start");
	  opendayVO.setCollegeId(rs.getString("COLLEGE_ID"));
      opendayVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      opendayVO.setCollegeDispName(rs.getString("COLLEGE_NAME_DISPLAY"));
      opendayVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
      opendayVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
      opendayVO.setBookingUrl(rs.getString("BOOKING_URL"));
      opendayVO.setTotalOpenDays(rs.getString("TOTAL_OPEN_DAYS"));
      opendayVO.setNetworkId(rs.getString("NETWORK_ID"));
      opendayVO.setSuborderItemId(rs.getString("SUB_ORDER_ITEM_ID"));			
      opendayVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(opendayVO.getCollegeName(), opendayVO.getCollegeId()));
      //System.out.println("OpendayInfoRowMapperImpl END");
      return opendayVO;
    }

  }

  
  public class CollegeDetailsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CollegeDetailsVO collegeDetailsVO = new CollegeDetailsVO();
      //System.out.println("CollegeDetailsRowMapperImpl start");
      collegeDetailsVO.setCollegeId(rs.getString("COLLEGE_ID"));
      collegeDetailsVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      collegeDetailsVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
      collegeDetailsVO.setReviewCount(rs.getString("REVIEW_COUNT"));
      collegeDetailsVO.setOverAllRating(rs.getString("OVERALL_RATING"));
      collegeDetailsVO.setAbsoluteRating(rs.getString("OVERALL_RATING_EXACT"));
      collegeDetailsVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
      //System.out.println("CollegeDetailsRowMapperImpl end");
      return collegeDetailsVO;
    }

  }

  

  public class WuscaCategoriesRatingRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewVO reviewVO = new ReviewVO();
      //System.out.println("WuscaCategoriesRatingRowMapperImpl start");
      reviewVO.setQuestionTitle(rs.getString("QUESTION_TITLE"));
      reviewVO.setRating(rs.getString("RATING"));
      reviewVO.setRoundRating(rs.getString("ROUND_RATING"));
      reviewVO.setQuestionId(rs.getString("QUESTION_ID"));
      //System.out.println("WuscaCategoriesRatingRowMapperImpl END");
      return reviewVO;
    }

  }

  public class StudentUniReviewsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewVO reviewsVO = new ReviewVO();
      //System.out.println("StudentUniReviewsRowMapperImpl start");
      reviewsVO.setMediaId(rs.getString("MEDIA_ID"));
      reviewsVO.setMediaPath(rs.getString("MEDIA_PATH"));
      reviewsVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));      
      reviewsVO.setStudentName(rs.getString("STUDENT_NAME"));
      reviewsVO.setSubjectOfStudy(rs.getString("SUBJECT_OF_STUDY"));
      reviewsVO.setStudentView(rs.getString("STUDENT_VIEW"));
      reviewsVO.setStudentShortDesc(rs.getString("STUDENT_SHORT_DESC"));
      reviewsVO.setMediaName(rs.getString("MEDIA_NAME"));     
      reviewsVO.setThumbNailPath(rs.getString("THUMBNAIL_PATH"));
      reviewsVO.setSectionId(rs.getString("SECTION_ID"));
      //System.out.println("StudentUniReviewsRowMapperImpl End");
      //SECTION_ID need to add
      return reviewsVO;
    }

  }

  public class HeroGalleryRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      GalleryVO galleryVO = new GalleryVO();
      //System.out.println("HeroGalleryRowMapperImpl start");
      galleryVO.setMediaId(rs.getString("MEDIA_ID"));
      galleryVO.setMediaPath(rs.getString("MEDIA_PATH"));
      galleryVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      galleryVO.setMediaName(rs.getString("MEDIA_NAME"));
      galleryVO.setRichFlag(rs.getString("RICH_FLAG"));
      galleryVO.setThumbNailPath(rs.getString("THUMBNAIL_PATH"));      
      galleryVO.setPromotionalText(rs.getString("PROMOTIONAL_TEXT"));
      galleryVO.setSectionId(rs.getString("SECTION_ID"));  
      //System.out.println("HeroGalleryRowMapperImpl end");
      return galleryVO;
    }

  }  
  
  private class ClearingProfileSEODetails implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SEOMetaDetailsVO seoMetaDetailsVO = new SEOMetaDetailsVO();
	  seoMetaDetailsVO.setMetaTitle(resultSet.getString("META_TITLE")); 
	  seoMetaDetailsVO.setMetaDesc(resultSet.getString("META_DESC"));
	  seoMetaDetailsVO.setMetaKeyword(resultSet.getString("META_KEYWORD"));
	  seoMetaDetailsVO.setMetaRobots(resultSet.getString("META_ROBO"));
	  seoMetaDetailsVO.setCategoryDesc(resultSet.getString("CATEGORY_DESC"));
	  seoMetaDetailsVO.setStudyDesc(resultSet.getString("STUDY_DESC"));
	  //System.out.println("META_TITLE profile-->"+ resultSet.getString("META_TITLE"));
	  //System.out.println("META_DESC profile-->" + resultSet.getString("META_DESC"));
	  return seoMetaDetailsVO;
	}
  }
  
}
