package spring.dao.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.clearing.ClearingHomeVO;

/**
 * @ClearingSubjectAjaxSP
 * @author   Kailash L
 * @version  1.0
 * @since    23_06_2020
 * @purpose  This SP used to get the objects for clearing subjects via ajax.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	             Change
 * *************************************************************************************************************************
 * 
*/
public class ClearingSubjectAjaxSP extends StoredProcedure {

    public ClearingSubjectAjaxSP(DataSource datasource) {
        setDataSource(datasource);
        setSql(ProcedureNames.GET_CLEARING_SUBJECT_AJAX_LIST_PRC);
        declareParameter(new SqlParameter("P_SUB_KEYWORD", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("P_BROWSE_CATEGORY_CODE", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("P_BROWSE_CAT_ID", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("PC_GET_SUBJECT_LIST", OracleTypes.CURSOR, new subjectAjaxListRowMapperImpl()));
        compile();
    }

    public Map execute(String subjectKeywordSearch) {
        Map outMap = null;
        try {
            Map inMap = new HashMap();
            inMap.put("P_SUB_KEYWORD", subjectKeywordSearch);
            outMap = execute(inMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outMap;
    }

    public class subjectAjaxListRowMapperImpl implements RowMapper {

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            ClearingHomeVO clearingHomeVO = new ClearingHomeVO();
            clearingHomeVO.setDescription(rs.getString("DESCRIPTION"));
            clearingHomeVO.setUrl(rs.getString("URL"));
            clearingHomeVO.setCategoryCode(rs.getString("CATEGORY_CODE"));
            clearingHomeVO.setCategoryCode(rs.getString("BROWSE_CAT_ID"));
            clearingHomeVO.setCategoryCode(rs.getString("MATCHED_TEXT"));
            clearingHomeVO.setCategoryCode(rs.getString("ORDER_SEQ"));
            return clearingHomeVO;
        }
    }

}