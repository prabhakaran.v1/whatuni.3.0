package spring.dao.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.rowmapper.advert.EnquiryInfoRowMapperImpl;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.search.ModuleListRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.ReviewSubjectVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.util.valueobject.search.CourseKISDataVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import com.wuni.util.valueobject.search.UniSpecificSearchResultsVO;
import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.rowmapper.seo.PageMetaTagsRowMapperImpl;
import spring.valueobject.search.TimesRankingVO;

public class ClearingCourseDetailsSP extends StoredProcedure{

  public ClearingCourseDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(ProcedureNames.GET_CL_COURSE_DETAILS_PAGE_PRC);	
    //
    declareParameter(new SqlParameter("P_COURSE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_OPPORTUNITY_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_PAGENAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_AGENT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_course_info", OracleTypes.CURSOR, new CourseDetailsDsktpRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_enquiry_info", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_module_info", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_module_details", OracleTypes.CURSOR, new ModuleListRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_INFO_FN", OracleTypes.CURSOR, new UniProfileInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_REVIEW_LIST_FN", OracleTypes.CURSOR, new ReviewLstRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_OPPORTUNITY_DETAILS", OracleTypes.CURSOR, new OppDropDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_KEY_STATS_INFO", OracleTypes.CURSOR, new KeyStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_RANKING_FN", OracleTypes.CURSOR, new UniRankningRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_GET_COURSE_DURATIONS", OracleTypes.CURSOR, new GetCourseDurationRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_GET_ASSESSED_COURSE", OracleTypes.CURSOR, new GetAssessesCourseRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_VENUE_DETAILS", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_PREVIOUS_STUDY_SUBJS", OracleTypes.CURSOR, new PrvisstdySubjsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_APPLICANTS_DATA", OracleTypes.CURSOR, new ApplicantsDataRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_FEES_DATA", OracleTypes.CURSOR, new FeesDataRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_ADVERTISER_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_OPP_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_OPP_DET", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("OC_PAGE_META_TAGS", OracleTypes.CURSOR, new PageMetaTagsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_logo", OracleTypes.VARCHAR));  
    declareParameter(new SqlOutParameter("p_degree_course_flag", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("p_course_type", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("p_course_last_updated_date", OracleTypes.VARCHAR));   
    declareParameter(new SqlOutParameter("p_opp_last_updated_date", OracleTypes.VARCHAR));        
    declareParameter(new SqlOutParameter("p_part_time_msg_flag", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("p_ldcs_descriptions", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN",OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_SUB_REVIEW_BREAKDOWN", OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_CRS_REVIEW_SUB", OracleTypes.CURSOR, new CourseReviewSubjectRowMapperImpl()));
    declareParameter(new SqlParameter("P_USER_SUBJGRADES_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_SCORE_LABEL", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_SCORE_LABEL_TOOLTIP", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_USER_SCORE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_MIN_EQUIVALENT_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_QUALIFICATION_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_SUBJECT_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", OracleTypes.VARCHAR));   
    declareParameter(new SqlOutParameter("P_SCORE_LEVEL", OracleTypes.VARCHAR));   
    //
    compile();
  }
  public Map execute(List inputList) {
    Map outMap = null;	    
	try {
	  Map inMap = new HashMap();
	  inMap.put("P_COURSE_ID", inputList.get(0));
	  inMap.put("P_COLLEGE_ID", inputList.get(1));
	  inMap.put("P_OPPORTUNITY_ID", inputList.get(2));
	  inMap.put("P_USER_ID", inputList.get(4));
	  //inMap.put("P_USER_ID", "4590495");	  
	  inMap.put("P_SESSION_ID", inputList.get(3));
	  inMap.put("P_BASKET_ID", inputList.get(5));
	  inMap.put("P_PAGENAME", inputList.get(6));
	  inMap.put("P_META_PAGE_NAME", inputList.get(7));
	  inMap.put("P_META_PAGE_FLAG", inputList.get(8));
	  inMap.put("P_USER_AGENT", inputList.get(9));
	  inMap.put("P_TRACK_SESSION_ID", inputList.get(10));   
	  inMap.put("P_USER_SUBJGRADES_SESSION_ID", inputList.get(11));
	  //System.out.println("inMap-->"+ inMap);
	  outMap = execute(inMap);
	  //System.out.println("outMap-->"+outMap);
	}catch(Exception ex){
	  ex.printStackTrace();
	}
	return outMap;
  }
  
  public class CourseDetailsDsktpRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
	  CourseDetailsVO courseDetailsVO = new CourseDetailsVO();
      SeoUrls seoUrl = new SeoUrls();
      //
      courseDetailsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      courseDetailsVO.setCourseSummaryFull(resultset.getString("COURSE_SUMMARY"));
      courseDetailsVO.setCourseSummary(resultset.getString("CUT_OFF_COURSE_SUMMARY"));
      courseDetailsVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
      courseDetailsVO.setUcasCode(resultset.getString("UCAS_CODE"));
      courseDetailsVO.setCourseId(resultset.getString("COURSE_ID"));
      courseDetailsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      courseDetailsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      courseDetailsVO.setDepartmentOrFaculty(resultset.getString("DEPT_NAME"));
      courseDetailsVO.setWasThisCourseShortlisted(resultset.getString("COURSE_IN_BASKET"));
      courseDetailsVO.setUniHomeUrl(seoUrl.construnctProfileURL(courseDetailsVO.getCollegeId(),courseDetailsVO.getCollegeName(),GlobalConstants.CLEARING_PAGE_FLAG));
      courseDetailsVO.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID"));
      courseDetailsVO.setOverAllRatingExact(resultset.getString("OVERALL_RATING_EXACT"));
      courseDetailsVO.setOverAllRating(resultset.getString("OVERALL_RATING"));
      courseDetailsVO.setReviewCount(resultset.getString("REVIEW_COUNT"));
      courseDetailsVO.setNetworkId(resultset.getString("NETWORK_ID"));
      courseDetailsVO.setCollegeLocation(resultset.getString("COLLEGE_LOCATION"));
      courseDetailsVO.setProfileURL(seoUrl.construnctSpURL(courseDetailsVO.getCollegeId(), courseDetailsVO.getCollegeName(), courseDetailsVO.getMyhcProfileId(), "", courseDetailsVO.getNetworkId(), ""));
      courseDetailsVO.setReviewURL("/degrees/university-course-reviews/highest-rated/" + courseDetailsVO.getCollegeId() + "/0/1/reviews.html");
      courseDetailsVO.setStudyModes(resultset.getString("STUDY_MODE"));
      courseDetailsVO.setStudyLevelCode(resultset.getString("QUAL_CODE"));
      courseDetailsVO.setAddedToFinalChoice(resultset.getString("FINAL_CHOICES_EXIST_FLAG")); 
      courseDetailsVO.setHybridFlag(resultset.getString("HYBRID_FLAG"));
      courseDetailsVO.setCompressedFlag(resultset.getString("COMPRESSED_FLAG"));
      courseDetailsVO.setStartDate(resultset.getString("START_DATE"));
      return courseDetailsVO;
	}
  }
  
  public class UniProfileInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
	 SeoUrls seoUrl = new SeoUrls();
      UniSpecificSearchResultsVO uniSpecificSearchResultsVO = new UniSpecificSearchResultsVO();
      uniSpecificSearchResultsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      uniSpecificSearchResultsVO.setCollegeName(resultset.getString("COLLEGE_NAME").toLowerCase());
      uniSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      uniSpecificSearchResultsVO.setAwardImagePath(resultset.getString("AWARD_IMAGE"));
      uniSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctProfileURL(uniSpecificSearchResultsVO.getCollegeId(),uniSpecificSearchResultsVO.getCollegeName(),GlobalConstants.CLEARING_PAGE_FLAG));
      uniSpecificSearchResultsVO.setClearingProfileURL(seoUrl.construnctProfileURL(uniSpecificSearchResultsVO.getCollegeId(),uniSpecificSearchResultsVO.getCollegeName(),GlobalConstants.CLEARING_PAGE_FLAG));
      uniSpecificSearchResultsVO.setMediaId(resultset.getString("MEDIA_ID"));
      uniSpecificSearchResultsVO.setMediaTypeId(resultset.getString("MEDIA_TYPE_ID"));
      uniSpecificSearchResultsVO.setMediaPath(resultset.getString("MEDIA_PATH"));
      uniSpecificSearchResultsVO.setAccommodationFlag(resultset.getString("ACCOMMODATION_FLAG"));
      uniSpecificSearchResultsVO.setScholarshipsFlag(resultset.getString("SCHOLARSHIPS_FLAG"));
      uniSpecificSearchResultsVO.setMediaType(resultset.getString("MEDIA_TYPE"));
      uniSpecificSearchResultsVO.setThumbnailPath(resultset.getString("THUMBNAIL_PATH"));
      //System.out.println(uniSpecificSearchResultsVO.getUniHomeUrl() + "url=======================");
      
         /* uniSpecificSearchResultsVO.setImagePath("");
          String dataArray[] = null;
          String durationArray[] = null;
          if (resultset.getString("media_path") != null && resultset.getString("media_path").trim().length() > 0) {
        	  System.out.println("media path-->"+resultset.getString("media_path"));
            dataArray = resultset.getString("media_path").split("###", 3);
          }
          System.out.println("dataArray-->" + dataArray.toString());
          String imagePath = "", thumbnailPath = "";
          int lenth = dataArray != null? dataArray.length: 0;
          System.out.println("lenth-->"+lenth);
          System.out.println("dataArray0-->" + dataArray[0]);
          //System.out.println("dataArray1-->" + dataArray[1]);
          if (lenth == 1) {
            imagePath = dataArray[0];
            System.out.println("imagePath-->"+imagePath);
            uniSpecificSearchResultsVO.setImagePath(imagePath);
          } else if (lenth > 1) {
        	  System.out.println("dataArray1_2-->" + dataArray[1]);
            uniSpecificSearchResultsVO.setVideoId(dataArray[0]);
            uniSpecificSearchResultsVO.setVideoUrl(dataArray[1]);
            if ( (uniSpecificSearchResultsVO.getVideoUrl().indexOf(".flv")  > -1) ||  (uniSpecificSearchResultsVO.getVideoUrl().indexOf(".mp4")>-1)   ) {
              String videoDuration = dataArray[2];
              System.out.println("videoDuration" + videoDuration);
              if (!GenericValidator.isBlankOrNull(videoDuration)) {
                durationArray = videoDuration.split(":", 2);
                int length1 = durationArray != null? durationArray.length: 0;
                if (length1 > 1) {
                  String minutes = durationArray[0];
                  String seconds = durationArray[1];
                  uniSpecificSearchResultsVO.setVideoMetaDuration("T" + minutes + "M" + seconds + "S");
                }
              }
            }
            if (resultset.getString("media_type_id") != null && "19".equals(resultset.getString("media_type_id"))) {
              thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(dataArray[0], "7"), rowNum);
            } else {
              thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(dataArray[0], "4"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path. 
            }
            uniSpecificSearchResultsVO.setVideoThumbnailUrl(thumbnailPath);
          }*/
          
     
      return uniSpecificSearchResultsVO;
    }
  }
  
  public class ReviewLstRowMapperImpl implements RowMapper {//Modified column names for review redesign by Hema.S on 18_DEC_2018
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewBean = new ReviewListBean();
      reviewBean.setReviewId(rs.getString("REVIEW_ID"));
      reviewBean.setReviewerName(rs.getString("USER_NAME"));
      reviewBean.setCollegeId(rs.getString("COLLEGE_ID"));
      reviewBean.setCourseId(rs.getString("COURSE_ID"));
      reviewBean.setOverAllRating(rs.getString("OVERALL_RATING"));
      reviewBean.setOverallRatingComment(rs.getString("REVIEW_TEXT"));
      reviewBean.setCollegeName(rs.getString("COLLEGE_NAME"));  
      String reviewUrl = new SeoUrls().constructReviewPageSeoUrl(reviewBean.getCollegeName(), reviewBean.getCollegeId());
      reviewUrl = reviewUrl+ "?reviewId="+reviewBean.getReviewId();
      reviewBean.setReviewDetailUrl(reviewUrl);
      reviewBean.setCourseTitle(rs.getString("COURSE_TITLE"));
      reviewBean.setReviewDate(rs.getString("REVIEW_DATE"));  
      reviewBean.setUserNameInitial(rs.getString("USER_NAME_INT"));  
      reviewBean.setCollegeName(rs.getString("COLLEGE_NAME"));
      if((!GenericValidator.isBlankOrNull(reviewBean.getCourseId()) || !"0".equals(reviewBean.getCourseId()))){
        reviewBean.setCourseDetailsReviewURL(new SeoUrls().courseDetailsPageURL(reviewBean.getCourseTitle(), reviewBean.getCourseId(), reviewBean.getCollegeId()));
      }
      reviewBean.setCourseDeletedFlag(rs.getString("COURSE_DELETED_FLAG"));
      return reviewBean;
    }
  }
  
  public class OppDropDownRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CourseKISDataVO oppListVO = new CourseKISDataVO();
      oppListVO.setOpportunityId(rs.getString("OPPORTUNITY_ID"));
      oppListVO.setOpportunityDesc(rs.getString("OPPORTUNITY"));
      return oppListVO;
    }
  }
  
  public class KeyStatsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      kisDataVO.setSubjectName(resultset.getString("SUBJECT_NAME"));
      kisDataVO.setTwoistooneRatio(resultset.getString("RATIO"));
      kisDataVO.setDropOutRate(resultset.getString("DROP_OUT_RATE"));
      kisDataVO.setGraduates(resultset.getString("GRADUATES_PERCENT"));
      kisDataVO.setAvgSalaryAfterSixMonths(resultset.getString("SALARY_6_MONTHS"));
      kisDataVO.setAvgSalaryAfterFourtyMonths(resultset.getString("SALARY_40_MONTHS"));
      kisDataVO.setAvgSalAfterSixMonthsAtuni(resultset.getString("SALARY_6_MONTHS_AT_UNI"));
      kisDataVO.setPercentageSixMonths(resultset.getString("SALARY_6_MONTHS_PER"));
      kisDataVO.setPercentage40Months(resultset.getString("SALARY_40_MONTHS_PER"));
      kisDataVO.setPercentageSixMonthsAtUni(resultset.getString("SALARY_6_MONTHS_AT_UNI_PER"));
      //Added three columns to show the drop-out rate calculation in tooltip By Thiyagu G on 24_jan_2017
      kisDataVO.setContinuingCourse(resultset.getString("CONTINUING_COURSE")); 
      kisDataVO.setCompletedCourse(resultset.getString("COMPLETED_COURSE"));
      kisDataVO.setCourseContinuation(resultset.getString("COURSE_CONTINUATION"));
      return kisDataVO;
    }
  }
  
  public class UniRankningRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TimesRankingVO timesRankingVO = new TimesRankingVO();
      timesRankingVO.setTimesRanking(resultset.getString("TIMES_RANKING"));
      String uniRanking = "";
      uniRanking = !GenericValidator.isBlankOrNull(timesRankingVO.getTimesRanking()) ? timesRankingVO.getTimesRanking().substring(0,timesRankingVO.getTimesRanking().length()-2) : "";
      String subScript = "";
      subScript = !GenericValidator.isBlankOrNull(timesRankingVO.getTimesRanking()) ? timesRankingVO.getTimesRanking().substring(timesRankingVO.getTimesRanking().length()-2) : "";
      timesRankingVO.setUniTimesRanking(uniRanking);
      timesRankingVO.setThAndRdValue(subScript);
      timesRankingVO.setStudentRanking(resultset.getString("STUDENT_RANKING"));
      timesRankingVO.setTotalStudentRanking(resultset.getString("TOTAL_STUDENT_RANKING"));
      ResultSet timesRankingRs = (ResultSet)resultset.getObject("TIMES_SUBJECT_RANKING");
      ArrayList timeRankingList = null;
      try {
        if (timesRankingRs != null) {
          timeRankingList = new ArrayList();
          while (timesRankingRs.next()) {
            TimesRankingVO timeRankingVO = new TimesRankingVO();
            timeRankingVO.setSubjectName(timesRankingRs.getString("TR_SUBJECT_NAME"));
            timeRankingVO.setCugSubjectRank(timesRankingRs.getString("CURRENT_RANKING"));
            String cugRanking = "";
            cugRanking = !GenericValidator.isBlankOrNull(timeRankingVO.getCugSubjectRank()) ? timeRankingVO.getCugSubjectRank().substring(0,timeRankingVO.getCugSubjectRank().length()-2) : "";
            String superScript = "";
            superScript = !GenericValidator.isBlankOrNull(timeRankingVO.getCugSubjectRank()) ? timeRankingVO.getCugSubjectRank().substring(timeRankingVO.getCugSubjectRank().length()-2) : "";
            timeRankingVO.setCurrentRanking(cugRanking);
            timeRankingVO.setThAndRdValue(superScript);
            timeRankingList.add(timeRankingVO);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (timesRankingRs != null) {
            timesRankingRs.close();
            timesRankingRs = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      timesRankingVO.setTimesSubRankingList(timeRankingList);
      return timesRankingVO;
    }
  }
  
  public class GetCourseDurationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseDetailsVO courseDetailsVO = new CourseDetailsVO();
      courseDetailsVO.setStudyModes(resultset.getString("DESCRIPTION"));
      courseDetailsVO.setDurations(resultset.getString("DURATION_DESC"));
      courseDetailsVO.setStartDate(resultset.getString("START_DATE"));
      courseDetailsVO.setShortStartDate(resultset.getString("SHORT_TEXT"));
      return courseDetailsVO;
    }
  }
  
  public class GetAssessesCourseRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      kisDataVO.setExam(resultset.getString("EXAM"));
      kisDataVO.setCourseWork(resultset.getString("COURSE_WORK"));
      kisDataVO.setPracticalWork(resultset.getString("PRACTICAL_WORK"));
      return kisDataVO;
    }
  }
  
  public class LocationsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
      //
      locationsInfoVO.setLatitude(resultset.getString("LATITUDE"));
      locationsInfoVO.setLongitude(resultset.getString("LONGITUDE"));
      locationsInfoVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      locationsInfoVO.setTown(resultset.getString("TOWN"));
      locationsInfoVO.setPostcode(resultset.getString("POSTCODE"));
      locationsInfoVO.setNearestStationName(resultset.getString("NEAREST_TRAIN_STN"));
      locationsInfoVO.setNearestTubeStation(resultset.getString("NEAREST_TUBE_STN"));
      locationsInfoVO.setDistanceFromNearestStation(resultset.getString("DISTANCE_FROM_TRAIN_STN"));
      locationsInfoVO.setDistanceFromTubeStation(resultset.getString("DISTANCE_FROM_TUBE_STN"));
      locationsInfoVO.setAddLine1(resultset.getString("ADD_LINE_1"));
      locationsInfoVO.setAddLine2(resultset.getString("ADD_LINE_2"));
      locationsInfoVO.setCountryState(resultset.getString("COUNTRY_STATE"));
      //Changing is in london to uppercase by Prabha on 17_05_2018
      if(!GenericValidator.isBlankOrNull(resultset.getString("IS_IN_LONDON"))){
        locationsInfoVO.setIsInLondon(resultset.getString("IS_IN_LONDON").toUpperCase());
      }
      //End of code
      locationsInfoVO.setCityGuideDisplayFlag(resultset.getString("CITY_GUIDE_DISPLAY_FLAG"));
      locationsInfoVO.setLocation(resultset.getString("LOCATION"));
      locationsInfoVO.setCityGuideUrl(new SeoUrls().constructAdviceSeoUrl(resultset.getString("ARTICLE_CATEGORY"), resultset.getString("POST_URL"), resultset.getString("ARTICLE_ID")));
      //
      return locationsInfoVO;
    }
  }
  
  public class PrvisstdySubjsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kissubjectdate = new CourseKISDataVO();
      kissubjectdate.setKisSubject(resultset.getString("KIS_SUBJECT_NAME"));
      ResultSet kisSubjectrs = (ResultSet)resultset.getObject("KIS_SUBJECTS");
      List subjectList = new ArrayList();
      try {
        if (kisSubjectrs != null) {
          while (kisSubjectrs.next()) {
            CourseKISDataVO kissubjectvo = new CourseKISDataVO();
            kissubjectvo.setKisSubject(kisSubjectrs.getString("HELD_QUALIFICATION_SUBJECT"));
            kissubjectvo.setHeldByPercentage(kisSubjectrs.getString("DERIVED_HELD_PERCENTAGE"));
            subjectList.add(kissubjectvo);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          kisSubjectrs.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      kissubjectdate.setSubjectList(subjectList);
      return kissubjectdate;
    }
  }
  
  public class ApplicantsDataRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO courseDetailKisVO = new CourseKISDataVO();
      courseDetailKisVO.setTotalApplicants(resultset.getString("TOTAL_APPLICANTS"));
      courseDetailKisVO.setTotalApplicantsStickMen(resultset.getString("APPLICANTS_VAL"));
      courseDetailKisVO.setSuccessfulApplicants(resultset.getString("SUCCESSFUL_APPLICANTS"));
      courseDetailKisVO.setSuccessfulApplicantsStickMen(resultset.getString("OFFERS_VAL"));
      courseDetailKisVO.setApplicantsRate(resultset.getString("APPLICANT_SUCCESSRATE"));
      return courseDetailKisVO;
    }
  }
  
  public class FeesDataRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO courseDetailKisVO = new CourseKISDataVO();
      courseDetailKisVO.setFeesType(resultset.getString("FEE_TYPE"));
      courseDetailKisVO.setFeesTypeTooltip("Rest of World".equalsIgnoreCase(resultset.getString("FEE_TYPE")) ? "International" : resultset.getString("FEE_TYPE"));
      courseDetailKisVO.setFees(resultset.getString("FEE"));
      courseDetailKisVO.setDuration(resultset.getString("DURATION"));
      courseDetailKisVO.setCurrency(resultset.getString("CURRENCY"));
      courseDetailKisVO.setFeesDesc(resultset.getString("FEE_DESC"));
      courseDetailKisVO.setState("free".equalsIgnoreCase(resultset.getString("STATE")) ? resultset.getString("STATE").toUpperCase() : resultset.getString("STATE"));
      courseDetailKisVO.setUrl(resultset.getString("URL"));
      courseDetailKisVO.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      courseDetailKisVO.setNetworkId(resultset.getString("NETWORK_ID"));
      courseDetailKisVO.setSequenceNo(resultset.getString("SEQ_NO"));
      return courseDetailKisVO;
    }
  }
  
  public class CourseReviewSubjectRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      ReviewSubjectVO reviewSubjectVO = new ReviewSubjectVO();
      reviewSubjectVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      reviewSubjectVO.setSubjectName(resultset.getString("SUBJECT_NAME"));
      return reviewSubjectVO;
    }
  }
}
