package spring.dao.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.clearing.ClearingHomeVO;

/**
 * @ClearingUniversityAjaxSP
 * @author   Kailash L
 * @version  1.0
 * @since    23_06_2020
 * @purpose  This SP used to get the objects for clearing university via ajax.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	             Change
 * *************************************************************************************************************************
 * 
*/
public class ClearingUniversityAjaxSP extends StoredProcedure {

	public ClearingUniversityAjaxSP(DataSource datasource) {
		setDataSource(datasource);
		setFunction(true);
		setSql(ProcedureNames.GET_CLEARING_UNIVERSITY_AJAX_LIST_FN);
		declareParameter(new SqlOutParameter("PC_UNIVERSITY_AJAX_LIST", OracleTypes.CURSOR, new universityAjaxListRowMapperImpl()));
		declareParameter(new SqlParameter("P_UNI_KEYWORD", OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String uniKeywordSearch) {
		Map outMap = null;
		try {
			Map inMap = new HashMap();
			inMap.put("P_UNI_KEYWORD", uniKeywordSearch);
			outMap = execute(inMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outMap;
	}

	public class universityAjaxListRowMapperImpl implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			ClearingHomeVO clearingHomeVO = new ClearingHomeVO();
			clearingHomeVO.setDescription(rs.getString("DESCRIPTION"));
			clearingHomeVO.setUrl(rs.getString("URL"));
			clearingHomeVO.setCategoryCode(rs.getString("CATEGORY_CODE"));
			clearingHomeVO.setCategoryCode(rs.getString("BROWSE_CAT_ID"));
			clearingHomeVO.setCategoryCode(rs.getString("MATCHED_TEXT"));
			clearingHomeVO.setCategoryCode(rs.getString("ORDER_SEQ"));
			clearingHomeVO.setSdMessage(rs.getNString("SD_MSG"));
			return clearingHomeVO;
		}
	}

}

