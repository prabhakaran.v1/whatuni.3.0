package spring.dao.sp.clearing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.dao.util.valueobject.clearing.ClearingHomeVO;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.ClearingFeaturedProvidersVO;
import com.wuni.util.valueobject.ClearingHomeFaqVO;
import com.wuni.util.valueobject.ClearingInfoVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ClearingHomePageSP.java
 * Description   : Class used to call the stored procedure.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author                   Ver            Created/Modified On            Modification Details             Change
 * *************************************************************************************************************************************
 * Thiyagu G                1.0                01.04.2012                 First draft                      wu542_09062015  
 * Kailash L                1.1                23.06.2020                 Modified                         JIRA - 287 - Added Location cursor   
 * *************************************************************************************************************************************
 *
 */
public class ClearingHomePageSP extends StoredProcedure {
  public ClearingHomePageSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.CLEARING_HOME_PAGE_PRC);
    declareParameter(new SqlParameter("P_WEBSITE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REFERER_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUBJECT_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION", Types.VARCHAR));
    declareParameter(new SqlOutParameter("O_ARTICLE_TEASER", OracleTypes.CURSOR, new StudentArticleGroupRowMapper()));
    declareParameter(new SqlOutParameter("O_CLEARING_SUBJECTS", OracleTypes.CURSOR, new ClearingSubjects()));
    declareParameter(new SqlOutParameter("O_FEATURED_PROVIDERS", OracleTypes.CURSOR, new ClearingFeaturedProviders()));
    declareParameter(new SqlOutParameter("O_SPONSORED_COLLEGE", OracleTypes.CURSOR, new ClearingSubjectsSponsoredCollege()));
    declareParameter(new SqlOutParameter("O_SUBJECT_LIST", OracleTypes.CLOB));
    declareParameter(new SqlOutParameter("O_SNIPPET_TEXT", OracleTypes.CLOB));
    declareParameter(new SqlOutParameter("O_VIEW_COURSE_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_FAQ_DETAILS",OracleTypes.CURSOR, new clearingFAQDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_LOCATION_LIST",OracleTypes.CURSOR, new clearingLocationRowMapperImpl()));
    compile();
  }
  public Map execute(List parameters) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_WEBSITE_ID", parameters.get(0));
      inMap.put("P_SESSION_ID", parameters.get(2));
      inMap.put("P_USER_ID", parameters.get(1));
      inMap.put("P_USER_AGENT", parameters.get(3));
      inMap.put("P_USER_IP", parameters.get(4));
      inMap.put("P_REQUEST_URL", parameters.get(5));
      inMap.put("P_REFERER_URL", parameters.get(6));
      inMap.put("P_SUBJECT_ID", parameters.get(7));
      inMap.put("P_TRACK_SESSION", parameters.get(8));
      outMap = execute(inMap);
      if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
        new AdminUtilities().logDbCallParameterDetails("WU_CLEARING_PKG_WRK.CLEARING_HOME_PAGE_PRC", inMap);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  // This is used for the atilce details in clearing hoem page.
  private class StudentArticleGroupRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {            
      AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
        try{
      adviceHomeInfoVO.setPostTitle(rs.getString("POST_TITLE"));
      adviceHomeInfoVO.setPostId(rs.getString("POST_ID"));
      adviceHomeInfoVO.setPostUrl(rs.getString("POST_URL"));
      adviceHomeInfoVO.setPostShortText(rs.getString("POST_SHORT_TEXT"));
      adviceHomeInfoVO.setMediaPath(rs.getString("MEDIA_PATH"));
      if (!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getMediaPath())) {
        String splitMediaPath = adviceHomeInfoVO.getMediaPath().substring(0, adviceHomeInfoVO.getMediaPath().lastIndexOf("."));
        adviceHomeInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + "_225px"+adviceHomeInfoVO.getMediaPath().substring(adviceHomeInfoVO.getMediaPath().lastIndexOf("."), adviceHomeInfoVO.getMediaPath().length())), rowNum)); //Change the image extention by Prabha 28_Jun_16
      }
      adviceHomeInfoVO.setMediaAltText(rs.getString("MEDIA_ALT_TEXT"));
      adviceHomeInfoVO.setVideoUrl(rs.getString("VIDEO_URL"));
      adviceHomeInfoVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
      adviceHomeInfoVO.setReadCount(rs.getString("READ_COUNT"));
      adviceHomeInfoVO.setTotalCount(rs.getString("TOTAL_COUNT"));
      /*adviceHomeInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
        adviceHomeInfoVO.setSubCategoryName(rs.getString("sub_category_name"));
        if(rs.getString("sub_category_name")!=null){
            adviceHomeInfoVO.setSubCategoryNameCapital(rs.getString("sub_category_name").toUpperCase());
        }*/
      adviceHomeInfoVO.setPrimaryCategoryName(rs.getString("PRIMARY_CATEGORY_NAME"));
      adviceHomeInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceHomeInfoVO.getPrimaryCategoryName(), adviceHomeInfoVO.getPostUrl(), adviceHomeInfoVO.getPostId()).toLowerCase());
      adviceHomeInfoVO.setSponsoredByDetails(rs.getString("SPONSORED_BY_DETAILS"));
      if (!GenericValidator.isBlankOrNull(adviceHomeInfoVO.getSponsoredByDetails())) {
        String sponsorshipdtls = adviceHomeInfoVO.getSponsoredByDetails();
        String sponsorDtls[] = sponsorshipdtls.split("##SP##");
        adviceHomeInfoVO.setCollegeId(sponsorDtls[0]);
        adviceHomeInfoVO.setCollegeDisplayName(sponsorDtls[1]);
        adviceHomeInfoVO.setSponsorUrl(sponsorDtls[2]);
        adviceHomeInfoVO.setSponsorLogo(new CommonUtil().getImgPath(sponsorDtls[3], rowNum)); //Added sponsor's logo for 16_May_2017, Bt Thiyagu G.
      }
      }catch(Exception e){
        e.printStackTrace();
      }
      return adviceHomeInfoVO;
      
    }
  }
  //This is used to display the right side in the clearing home page.
  private class ClearingSubjects implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ClearingInfoVO clearingVO = new ClearingInfoVO();
      clearingVO.setSubjectName(rs.getString("SUBJECT_NAME"));
      clearingVO.setCategoryId(rs.getString("CATEGORY_ID"));
      if (rs.getString("SUBJECT_NAME") != null) {
        clearingVO.setSubjectUrl(new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(rs.getString("SUBJECT_NAME"))).toLowerCase());
      } else {
        clearingVO.setSubjectUrl("");
      }
      return clearingVO;
    }
  }
  private class ClearingFeaturedProviders implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ClearingFeaturedProvidersVO featuredProviders = new ClearingFeaturedProvidersVO();
      featuredProviders.setCatSponsorId(rs.getString("CAT_SPONSOR_ID"));
      featuredProviders.setOrderItemId(rs.getString("ORDER_ITEM_ID"));      
      featuredProviders.setCollegeId(rs.getString("COLLEGE_ID"));
      featuredProviders.setCollegeDisplayName(rs.getString("COLLEGE_NAME_DISPLAY"));
      featuredProviders.setCollegeName(rs.getString("COLLEGE_NAME"));
      featuredProviders.setUrl(rs.getString("URL"));
      featuredProviders.setKeyword(rs.getString("KEYWORD"));
      featuredProviders.setMediaPath(rs.getString("MEDIA_PATH"));
      if (!GenericValidator.isBlankOrNull(featuredProviders.getMediaPath())) {
        featuredProviders.setDomainMediaPath(new CommonUtil().getImgPath(featuredProviders.getMediaPath(), rowNum));
      }
      featuredProviders.setText(rs.getString("TEXT"));
      //Added hotline numbers for featured providers for 16_May_2017, By Thiyagu G.
      featuredProviders.setHotline(rs.getString("HOTLINE_NUMBER"));
      featuredProviders.setHotlineOriginal(rs.getString("HOTLINE_ORIGINAL_NUMBER"));      
      String courseUrl = "/all-courses/csearch?clearing&university=" + (new CommonFunction().replaceHypen(rs.getString("COLLEGE_NAME"))).toLowerCase(); //Changed new all courses clearing url pettern, By Thiyagu G for 27_Jan_2016.
      featuredProviders.setCourseUrl(courseUrl);
      return featuredProviders;
    }
  }
  private class ClearingSubjectsSponsoredCollege implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ClearingInfoVO clearingInfoVO = new ClearingInfoVO();
      clearingInfoVO.setClearingCollegeId(rs.getString("COLLEGE_ID"));
      clearingInfoVO.setCollegeDisplayName(rs.getString("COLLEGE_NAME_DISPLAY"));
      clearingInfoVO.setClearingCollegeName(rs.getString("COLLEGE_NAME"));
      clearingInfoVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
      if (!GenericValidator.isBlankOrNull(clearingInfoVO.getCollegeLogo())) {
        clearingInfoVO.setDomainCollegeLogo(new CommonUtil().getImgPath(clearingInfoVO.getCollegeLogo(), rowNum));
      }
      clearingInfoVO.setSponsorReviewURL(new SeoUrls().constructReviewPageSeoUrl(clearingInfoVO.getClearingCollegeName(), clearingInfoVO.getClearingCollegeId()));
      clearingInfoVO.setUniHomeURL(new SeoUrls().construnctUniHomeURL(clearingInfoVO.getClearingCollegeId(), clearingInfoVO.getClearingCollegeName()));
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            clearingInfoVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));
            clearingInfoVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            clearingInfoVO.setSubOrderEmailWebform(tmpUrl);
            clearingInfoVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            clearingInfoVO.setSubOrderProspectusWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            clearingInfoVO.setSubOrderWebsite(tmpUrl);
            clearingInfoVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            clearingInfoVO.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
            clearingInfoVO.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
            clearingInfoVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
            clearingInfoVO.setHotline(enquiryDetailsRS.getString("HOTLINE"));
            clearingInfoVO.setMediaId(enquiryDetailsRS.getString("MEDIA_ID"));
            clearingInfoVO.setMediaPath(enquiryDetailsRS.getString("MEDIA_PATH"));
            clearingInfoVO.setMediaThumbPath(enquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
            clearingInfoVO.setMediaType(enquiryDetailsRS.getString("MEDIA_TYPE"));
            if ("VIDEO".equalsIgnoreCase(clearingInfoVO.getMediaType())) {
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(clearingInfoVO.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
              clearingInfoVO.setVideoThumbPath(thumbnailPath);
            }
            clearingInfoVO.setAdvertName(enquiryDetailsRS.getString("ADVERT_NAME"));
            clearingInfoVO.setQlFlag(enquiryDetailsRS.getString("QL_FLAG"));
            //clearingInfoVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
            //clearingInfoVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
            clearingInfoVO.setMediaTypeId(enquiryDetailsRS.getString("MEDIA_TYPE_ID"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          enquiryDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      clearingInfoVO.setReviewCount(rs.getString("REVIEW_COUNT"));
      clearingInfoVO.setOverallRating(rs.getString("OVERALL_RATING"));
      clearingInfoVO.setOverallRatingExact(rs.getString("OVERALL_RATING_EXACT"));
      return clearingInfoVO;
    }
  }
  private class clearingFAQDetailsRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
       ClearingHomeFaqVO clearingHomeFaqVO = new ClearingHomeFaqVO();
       clearingHomeFaqVO.setQuestionId(rs.getString("QUESTION_ID"));
       clearingHomeFaqVO.setQuestionTitle(rs.getString("QUESTION_TITLE"));
       clearingHomeFaqVO.setAnswer(rs.getString("ANSWER"));
       return clearingHomeFaqVO;
     }
   }
  
  private class clearingLocationRowMapperImpl implements RowMapper {
	  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    	 ClearingHomeVO clearingHomeVO = new ClearingHomeVO();
    	 clearingHomeVO.setRegionName(rs.getString("REGION_NAME"));
    	 clearingHomeVO.setRegionId(rs.getString("REGION_ID"));
    	 clearingHomeVO.setRegionUrlText(rs.getString("URL_TEXT"));
		return clearingHomeVO;
	  }
	}
}
