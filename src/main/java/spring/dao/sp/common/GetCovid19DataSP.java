package spring.dao.sp.common;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.common.Covid19VO;

/**
 * @GetCovid19DataSP Used to get the COVID-19 related SYSVAR data.
 * @author prabhakaran.v
 * @version 1.0
 * @since 21.04.2020
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 *  
 *                                                       
 */
public class GetCovid19DataSP extends StoredProcedure {
  private String SP_NAME = ProcedureNames.GET_COVID19_DATA_PRC;

  public GetCovid19DataSP(DataSource dataSource) {
    setDataSource(dataSource);
    //setFunction(true);
    setSql(SP_NAME);
    declareParameter(new SqlOutParameter("PC_COVID_19_PAGE_DETAILS", OracleTypes.CURSOR, new GetCovid19DataRowmapperImpl()));
    compile();
  }

  public Map<String, Object> executeProcedure() {
    //
    Map<String, Object> outMap = null;
    HashMap<String, String> inMap = new HashMap<>();
    //
    outMap = execute(inMap);
    return outMap;
  }

  private class GetCovid19DataRowmapperImpl implements RowMapper<Object> {

    @Override
    public Object mapRow(ResultSet rs, int num) {
      Covid19VO covid19VO = new Covid19VO();
      try {
        covid19VO.setSysvarName(rs.getString("NAME"));
        covid19VO.setSysvarText(rs.getString("VALUE"));
      } catch (Exception e) {
        e.printStackTrace();
      }
      return covid19VO;
    }
  }
}
