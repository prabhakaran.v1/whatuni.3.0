package spring.dao.sp.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.valueobject.common.SysvarsCallVO;

/**
 * @GetSysvarsCallSP
 * @author Sri Sankari R
 * @version 1.0
 * @since 19/01/2021
 * @purpose This program is used to call the database procedure to fetch the Sysvar'S details.
 * Change Log
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 */
public class GetSysvarsCallSP extends StoredProcedure {

  public GetSysvarsCallSP(DataSource dataSource) {

    setDataSource(dataSource);
    setFunction(true);
    setSql(ProcedureNames.GET_SYSVAR_VALUES_FN);
    declareParameter(new SqlOutParameter("LC_SYSVAR_VALUES", OracleTypes.CURSOR, new GetSysvarsCallRowmapperImpl()));
    compile();
  }

  public Map<String, Object> execute() throws Exception {
    
    Map<String, String> inMap = new HashMap<>();
    //
    return execute(inMap);
  }

  private class GetSysvarsCallRowmapperImpl implements RowMapper<Object> {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		SysvarsCallVO sysvarsCallVO = new SysvarsCallVO();
		sysvarsCallVO.setName(rs.getString("NAME"));
		sysvarsCallVO.setValue(rs.getString("VALUE")); 
		return sysvarsCallVO;
	} 
  }
}
