package spring.dao.sp.openday;

import org.springframework.jdbc.object.StoredProcedure;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.openday.OpendayInfoVO;
import spring.form.openday.OpenDayProviderLandingPageBean;

/**
 * @OpenDayLightBoxSP
 * @author   Kailash L
 * @version  1.0
 * @since    28_04_2020
 * @purpose  This SP used to get the objects for open day provider landing page for light box.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	             Change
 * *************************************************************************************************************************
 * 
*/
public class OpenDayLightBoxSP extends StoredProcedure {

	public OpenDayLightBoxSP(DataSource datasource) {
		setDataSource(datasource);
		setFunction(true);
		setSql(ProcedureNames.GET_PROVIDER_OPENDAYS_LIST_FN);
		declareParameter(new SqlOutParameter("PC_LIGHTBOX_LIST", OracleTypes.CURSOR, new openDaysLighgtBoxRowMapperImpl()));
		declareParameter(new SqlParameter("P_EVENT_ID", OracleTypes.NUMERIC));
		declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.NUMERIC));
		declareParameter(new SqlParameter("P_APP_FLAG", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_DISPLAY_CNT", OracleTypes.NUMERIC));
		declareParameter(new SqlParameter("P_BOOKING_FORM_FLAG", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", OracleTypes.NUMERIC));
		declareParameter(new SqlParameter("P_NETWORK_ID", OracleTypes.NUMERIC));
	    declareParameter(new SqlOutParameter("P_SHOW_UG_TAB_FLAG", OracleTypes.VARCHAR));
		compile();
	}

	public Map<String, Object> execute(OpenDayProviderLandingPageBean openDayBean) {
		Map<String, Object> outMap = null;
		try {
			Map<String,String> inMap = new HashMap<>();
			inMap.put("P_EVENT_ID", openDayBean.getEventId());
			inMap.put("P_COLLEGE_ID", openDayBean.getCollegeId());
			inMap.put("P_APP_FLAG", openDayBean.getAppFlage());
			inMap.put("P_DISPLAY_CNT", openDayBean.getDisplayCount());
			inMap.put("P_BOOKING_FORM_FLAG", openDayBean.getBookingFormFlag());
			inMap.put("P_DYNAMIC_RANDOM_NUMBER", openDayBean.getRandomNumer());
			inMap.put("P_NETWORK_ID", openDayBean.getNetworkId());
			outMap = execute(inMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outMap;
	}

	public class openDaysLighgtBoxRowMapperImpl implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			OpendayInfoVO openDayProviderVO = new OpendayInfoVO();
			openDayProviderVO.setEventItemId(rs.getString("EVENT_ITEM_ID"));
			openDayProviderVO.setStartDate(rs.getString("START_DATE"));
			openDayProviderVO.setStartTime(rs.getString("START_TIME"));
			openDayProviderVO.setVenue(rs.getString("VENUE"));
			openDayProviderVO.setQualType(rs.getString("QUAL_TYPE"));
			openDayProviderVO.setTotalOpenDayCount(rs.getString("TOTAL_OPEN_DAY_COUNT"));
			openDayProviderVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
			openDayProviderVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
			return openDayProviderVO;
		}
	}
}
