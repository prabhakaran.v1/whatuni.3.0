package spring.dao.sp.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.seo.SeoUrls;

import oracle.jdbc.OracleTypes;
import spring.dao.util.name.ProcedureNames;
import spring.dao.util.valueobject.openday.AdditionalResourcesInfoVO;
import spring.dao.util.valueobject.openday.ContentSectionInfoVO;
import spring.dao.util.valueobject.openday.GalleryInfoVO;
import spring.dao.util.valueobject.openday.LocationInfoVO;
import spring.dao.util.valueobject.openday.OpendayInfoVO;
import spring.dao.util.valueobject.openday.ProviderInfoVO;
import spring.form.openday.OpenDayProviderLandingPageBean;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;

/**
 * @OpenDayProviderLandingPageSP
 * @author   Sujitha V
 * @version  1.0
 * @since    28_04_2020
 * @purpose  This SP used to get the objects for open day provider landing page.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	             Change
 * *************************************************************************************************************************
 * Sri Sankari R          1.1                 20-10-2020     Added new cursor and in param for dynamic meta details from Back office           
*/
public class OpenDayProviderLandingPageSP extends StoredProcedure {

	public OpenDayProviderLandingPageSP(DataSource dataSource) {
		setDataSource(dataSource);
		setSql(ProcedureNames.GET_UNI_OPENDAY_LANDING_PRC);
		declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
		declareParameter(new SqlParameter("P_TRACK_SESSION_ID", Types.NUMERIC));
		declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
		declareParameter(new SqlParameter("P_APP_FLAG", Types.VARCHAR));
		declareParameter(new SqlParameter("P_DISPLAY_CNT", Types.NUMERIC));
		declareParameter(new SqlParameter("P_EVENT_ID", Types.NUMERIC));
		declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", Types.NUMERIC));
		declareParameter(new SqlParameter("P_NETWORK_ID", Types.NUMERIC));
		declareParameter(new SqlOutParameter("PC_UNI_INFO", OracleTypes.CURSOR, new UniInfoRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_UNI_OPENDAYS_LIST", OracleTypes.CURSOR, new SelectOpenDaysRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_HERO_IMAGE", OracleTypes.CURSOR, new GallerySectionRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_CONTENT_SECTION", OracleTypes.CURSOR, new ContentSectionRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_UNI_ADDRESS", OracleTypes.CURSOR, new LocationRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_TESTIMONIALS", OracleTypes.CURSOR, new TestimonialsResourcesRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_ADDITIONAL_CONTENT", OracleTypes.CURSOR,new AdditionalResourcesRowMapperImpl()));
		declareParameter(new SqlOutParameter("PC_EVENT_DETAILS", OracleTypes.CURSOR, new BookingFormRowMapperImpl()));
		declareParameter(new SqlOutParameter("P_BREAD_CRUMB", Types.VARCHAR));
		declareParameter(new SqlOutParameter("P_SHOW_UG_TAB_FLAG", Types.VARCHAR));
        declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR)); // Added in param for dynamic meta details from Back office, 20201020 rel by Sri Sankari
        declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl())); // Added new cursor for dynamic meta details from Back office, 20201020 rel by Sri Sankari
		compile();
	}

	public Map<String, Object> execute(OpenDayProviderLandingPageBean openDayProviderLandingPageBean) {
		Map<String, Object> outMap = null;
		try {
			Map<String, String> inMap = new HashMap<>();
			inMap.put("P_USER_ID", openDayProviderLandingPageBean.getUserId());
			inMap.put("P_TRACK_SESSION_ID", openDayProviderLandingPageBean.getTrackSessionId());
			inMap.put("P_COLLEGE_ID", openDayProviderLandingPageBean.getCollegeId());
			inMap.put("P_APP_FLAG", openDayProviderLandingPageBean.getAppFlage());
			inMap.put("P_DISPLAY_CNT", openDayProviderLandingPageBean.getDisplayCount());
			inMap.put("P_EVENT_ID", openDayProviderLandingPageBean.getEventId());
			inMap.put("P_DYNAMIC_RANDOM_NUMBER", openDayProviderLandingPageBean.getRandomNumer());
			inMap.put("P_NETWORK_ID", openDayProviderLandingPageBean.getNetworkId());
			inMap.put("P_URL", openDayProviderLandingPageBean.getPageUrl());
			outMap = execute(inMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outMap;
	}

	private class UniInfoRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			ProviderInfoVO providerInfoVO = new ProviderInfoVO();
			providerInfoVO.setCollegeId(rs.getString("COLLEGE_ID"));
			providerInfoVO.setCollegeName(rs.getString("COLLEGE_NAME"));
			providerInfoVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
			providerInfoVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
			providerInfoVO.setReviewCount(rs.getString("REVIEW_COUNT"));
			providerInfoVO.setOverallRating(rs.getString("OVERALL_RATING"));
			providerInfoVO.setOverallRatingExact(rs.getString("OVERALL_RATING_EXACT"));
			providerInfoVO.setCollegeLandingUrl(new SeoUrls().construnctUniHomeURL(providerInfoVO.getCollegeId(), providerInfoVO.getCollegeName()));
			providerInfoVO.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(providerInfoVO.getCollegeName(),providerInfoVO.getCollegeId()));
			return providerInfoVO;
		}
	}

	private class SelectOpenDaysRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			OpendayInfoVO selectOpenDayVO = new OpendayInfoVO();
			selectOpenDayVO.setEventItemId(rs.getString("EVENT_ITEM_ID"));
			selectOpenDayVO.setStartDate(rs.getString("START_DATE"));
			selectOpenDayVO.setStartTime(rs.getString("START_TIME"));
			selectOpenDayVO.setVenue(rs.getString("VENUE"));
			selectOpenDayVO.setQualType(rs.getString("QUAL_TYPE"));
			selectOpenDayVO.setTotalOpenDayCount(rs.getString("TOTAL_OPEN_DAY_COUNT"));
			selectOpenDayVO.setMoreOpenDayFlag(rs.getString("MORE_OPEN_DAYS_FLAG"));
			selectOpenDayVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
			selectOpenDayVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
			selectOpenDayVO.setQualTypeGA(rs.getString("QUAL_TYPE_FOR_GA"));
			selectOpenDayVO.setSelectedFlag(rs.getString("SELECTED_FLAG"));
		    selectOpenDayVO.setSubOrderItemId(rs.getString("SUB_ORDER_ITEM_ID"));
		    selectOpenDayVO.setBookingFormFlag(rs.getString("BOOKING_FORM_FLAG"));
		    selectOpenDayVO.setBookingUrl(rs.getString("BOOKING_URL"));
		    selectOpenDayVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
		    selectOpenDayVO.setCollegeName(rs.getString("COLLEGE_NAME"));
		    selectOpenDayVO.setOpenDay(rs.getString("OPENDATE"));
            selectOpenDayVO.setOpenMonth(rs.getString("OPEN_MONTH_YEAR"));
            selectOpenDayVO.setCollegeId(rs.getString("COLLEGE_ID"));
            selectOpenDayVO.setNetworkId(rs.getString("NETWORK_ID"));
			return selectOpenDayVO;
		}
	}

	private class GallerySectionRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			GalleryInfoVO galleryInfoVO = new GalleryInfoVO();
			galleryInfoVO.setMediaId(rs.getString("MEDIA_ID"));
			galleryInfoVO.setMediaPath(rs.getString("MEDIA_PATH"));
			galleryInfoVO.setMediaType(rs.getString("MEDIA_TYPE"));
			galleryInfoVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
			galleryInfoVO.setContentHubFlag(rs.getString("CONTENTHUB_FLAG"));
			galleryInfoVO.setThumbnailPath(rs.getString("THUMBNAIL_PATH"));
			galleryInfoVO.setMediaName(rs.getString("MEDIA_NAME"));
			return galleryInfoVO;
		}
	}

	private class ContentSectionRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			ContentSectionInfoVO contentSectionVO = new ContentSectionInfoVO();
			contentSectionVO.setSectionName(rs.getString("SECTION_NAME"));
			contentSectionVO.setSelectedOpenDate(rs.getString("SELECTED_OPEN_DATE"));
			ResultSet sectionValuesRS = (ResultSet) rs.getObject("SECTION_VALUE");
			if (sectionValuesRS != null) {
				ArrayList contentSectionValuesList = new ArrayList();
				ContentSectionInfoVO contentSectionValuesVO = null;
				while (sectionValuesRS.next()) {
					contentSectionValuesVO = new ContentSectionInfoVO();
					contentSectionValuesVO.setSectionValue(sectionValuesRS.getString("SECTION_VALUE"));
					if (StringUtils.isNotBlank(sectionValuesRS.getString("SECTION_VALUE"))) {
						contentSectionValuesList.add(contentSectionValuesVO);
					}
				}
				contentSectionVO.setContentSectionBulletValuesList(contentSectionValuesList);
			}
			return contentSectionVO;
		}
	}

	private class LocationRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			LocationInfoVO locationVO = new LocationInfoVO();
			locationVO.setLatitude(rs.getString("LATITUDE"));
			locationVO.setLongitude(rs.getString("LONGITUDE"));
			locationVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
			locationVO.setAddressLineOne(rs.getString("ADDRESS_LINE_1"));
			locationVO.setAddreeslineTwo(rs.getString("ADDRESS_LINE_2"));
			locationVO.setTown(rs.getString("TOWN"));
			locationVO.setPostcode(rs.getString("POSTCODE"));
			locationVO.setCountryName(rs.getString("COUNTRY_STATE"));
			locationVO.setIsInLondon(rs.getString("IS_IN_LONDON"));
			locationVO.setNearestTrainStation(rs.getString("NEAREST_TRAIN_STN"));
			locationVO.setNearestTubeStation(rs.getString("NEAREST_TUBE_STN"));
			locationVO.setDistanceFromTrainStation(rs.getString("DISTANCE_FROM_TRAIN_STN"));
			locationVO.setDistanceFromTubeStation(rs.getString("DISTANCE_FROM_TUBE_STN"));
			locationVO.setCityGuideFlag(rs.getString("CITY_GUIDE_DISPLAY_FLAG"));
			locationVO.setCityGuideLocation(rs.getString("LOCATION"));
			locationVO.setCityGuideUrl(new SeoUrls().constructAdviceSeoUrl(rs.getString("ARTICLE_CATEGORY"),rs.getString("POST_URL"), rs.getString("ARTICLE_ID")));
			return locationVO;
		}
	}

	private class TestimonialsResourcesRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			ContentSectionInfoVO testimonialsVO = new ContentSectionInfoVO();
			testimonialsVO.setSectionValue(rs.getString("SECTION_VALUE"));
			testimonialsVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
			testimonialsVO.setTitle(rs.getString("TITLE"));
			return testimonialsVO;
		}
	}

	private class AdditionalResourcesRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			AdditionalResourcesInfoVO additionalResourceVO = new AdditionalResourcesInfoVO();
			additionalResourceVO.setPdfName(rs.getString("PDF_NAME"));
			additionalResourceVO.setPdfPath(rs.getString("PDF_PATH"));
			return additionalResourceVO;
		}
	}

	private class BookingFormRowMapperImpl implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			OpendayInfoVO bookingFormDetailsVO = new OpendayInfoVO();
			bookingFormDetailsVO.setCollegeId(rs.getString("COLLEGE_ID"));
			bookingFormDetailsVO.setCollegeName(rs.getString("COLLEGE_NAME"));
			bookingFormDetailsVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
			bookingFormDetailsVO.setSubOrderItemId(rs.getString("SUB_ORDER_ITEM_ID"));
			bookingFormDetailsVO.setNetworkId(rs.getString("NETWORK_ID"));
			bookingFormDetailsVO.setStartDate(rs.getString("START_DATE"));
			bookingFormDetailsVO.setOpenDate(rs.getString("OPEN_DATE"));
			bookingFormDetailsVO.setOpenDay(rs.getString("OPENDATE"));
			bookingFormDetailsVO.setOpenMonth(rs.getString("OPEN_MONTH_YEAR"));
			bookingFormDetailsVO.setBookingUrl(rs.getString("BOOKING_URL"));
			bookingFormDetailsVO.setBookingFormFlag(rs.getString("BOOKING_FORM_FLAG"));
			bookingFormDetailsVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
			bookingFormDetailsVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
			bookingFormDetailsVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
			return bookingFormDetailsVO;
		}
	}
}
