package spring.dao.sp.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import oracle.jdbc.driver.OracleTypes;
import spring.pojo.user.UnSubcribeForm;
import spring.valueobject.user.UnSubscribePageVO;
/**
* @IOpenDayBusiness
* @author  Sangeeth.S
* @version  1.0
* @since    08_12_2020
* @purpose  Getting user unsubscribe page
* Change Log
* *************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	                 Change
* *************************************************************************************************************************
* Sangeeth.S   													initial draft
* 
*/
public class GetUnSubscribePageSP extends StoredProcedure{
  
	public GetUnSubscribePageSP(DataSource dataSource){
	  setDataSource(dataSource);
	  setSql(SpringConstants.GET_UNSUBSCRIBE_PAGE_PRC);
	  declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	  declareParameter(new SqlParameter("P_ENCRYPTED_USR_ID", Types.VARCHAR));
	  declareParameter(new SqlOutParameter("PC_USER_PREFERENCE", OracleTypes.CURSOR, new UserPreferenceRowMapperImpl()));
	  declareParameter(new SqlOutParameter("P_VALID_USER_FLAG", Types.VARCHAR));
	  compile();
   }
	public Map execute(UnSubcribeForm unSubcribeForm) throws Exception{
	  Map outMap = null;	  
	  Map<String, String> inMap = new HashMap<>();
	  inMap.put("P_USER_ID", unSubcribeForm.getUserId());
	  inMap.put("P_ENCRYPTED_USR_ID", unSubcribeForm.getEncryptUserId());			
	  outMap = execute(inMap);	   
	  return outMap;
	}
	
	private class UserPreferenceRowMapperImpl implements RowMapper{
	  @Override
	  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        UnSubscribePageVO unSubscribePageVO = new UnSubscribePageVO();
        unSubscribePageVO.setNewsletter(rs.getString("NEWSLETTER"));
        unSubscribePageVO.setRemainder(rs.getString("REMINDER"));
        unSubscribePageVO.setUniversityUpdates(rs.getString("UNIVERSITY_UPDATES"));
        unSubscribePageVO.setSurvey(rs.getString("SURVEY"));
        unSubscribePageVO.setEncryptedId(rs.getString("ENCRYPTED_ID"));
        unSubscribePageVO.setReviewSurveyFlag(rs.getString("REVIEW_SURVEY_FLAG"));
	    return unSubscribePageVO;
	  }	
	}
}
