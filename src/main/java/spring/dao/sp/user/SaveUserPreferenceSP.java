package spring.dao.sp.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import spring.pojo.user.UnSubcribeForm;
/**
* @IOpenDayBusiness
* @author  Sangeeth.S
* @version  1.0
* @since    08_12_2020
* @purpose  save user preference in unsubscribe page
* Change Log
* *************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	                 Change
* *************************************************************************************************************************
* Sangeeth.S   													initial draft
* 
*/

public class SaveUserPreferenceSP extends StoredProcedure{
	public SaveUserPreferenceSP(DataSource dataSource){
		  setDataSource(dataSource);
		  setSql(SpringConstants.SAVE_USR_PREFERENCE_PRC);
		  declareParameter(new SqlParameter("P_ENCRYPTED_USR_ID", Types.VARCHAR));
		  declareParameter(new SqlParameter("P_MARKETING_EMAIL", Types.VARCHAR));
		  declareParameter(new SqlParameter("P_RECEIVE_NO_EMAIL_FLAG", Types.VARCHAR));
		  declareParameter(new SqlParameter("P_SOLUS_EMAIL", Types.VARCHAR));
		  declareParameter(new SqlParameter("P_SURVEY_FLAG", Types.VARCHAR));		  
         declareParameter(new SqlParameter("P_REVIEW_SURVEY_FLAG", Types.VARCHAR));
		  compile();
	   }
		public Map execute(UnSubcribeForm unSubcribeForm) throws Exception{
		  Map outMap = null;		  
		  Map<String, String> inMap = new HashMap<>();
		  inMap.put("P_ENCRYPTED_USR_ID", unSubcribeForm.getEncryptUserId());
		  inMap.put("P_MARKETING_EMAIL", unSubcribeForm.getNewsLetters());
		  inMap.put("P_RECEIVE_NO_EMAIL_FLAG", unSubcribeForm.getRemainderMail());
		  inMap.put("P_SOLUS_EMAIL", unSubcribeForm.getSolusEmail());
		  inMap.put("P_SURVEY_FLAG", unSubcribeForm.getSurvey());			
         inMap.put("P_REVIEW_SURVEY_FLAG", unSubcribeForm.getReviewSurveyFlag());
		  outMap = execute(inMap);			
		  return outMap;
		}
}
