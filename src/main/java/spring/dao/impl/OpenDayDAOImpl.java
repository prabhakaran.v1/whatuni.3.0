package spring.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.dao.IOpenDayDAO;
import spring.dao.sp.openday.OpenDayLightBoxSP;
import spring.dao.sp.openday.OpenDayProviderLandingPageSP;
import spring.form.openday.OpenDayProviderLandingPageBean;

/**
 * @OpenDayDAOImpl
 * @author   Sujitha V
 * @version  1.0
 * @since    06_05_2020
 * @purpose  Class to get Open day provider related contents
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
 * Kailash L              1.1                 06_05_2020       Added ajax method for light box   
 * 
*/

@Repository
public class OpenDayDAOImpl implements IOpenDayDAO {

	@Autowired
	private DataSource dataSource;
    
	//Added this for getting open day provider landing page values by Sujitha.V on 6_May_2020 rel
	public Map<String, Object> getOpenDayProviderLandingList(OpenDayProviderLandingPageBean openDayBean) throws Exception {
		OpenDayProviderLandingPageSP openDayProviderLandingPageSP = new OpenDayProviderLandingPageSP(dataSource);
		return openDayProviderLandingPageSP.execute(openDayBean);
	}

	//Added this for getting open day provider landing page light box values by Kailash.L on 6_May_2020 rel
	public Map<String, Object> getOpenDayLightBox(OpenDayProviderLandingPageBean openDayBean) {
		OpenDayLightBoxSP openDayLightBoxSP = new OpenDayLightBoxSP(dataSource);
		return openDayLightBoxSP.execute(openDayBean);
	}

}
