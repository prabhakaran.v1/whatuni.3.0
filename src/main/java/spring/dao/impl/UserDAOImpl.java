package spring.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.dao.IUserDAO;
import spring.dao.sp.user.GetUnSubscribePageSP;
import spring.dao.sp.user.SaveUserPreferenceSP;
import spring.pojo.user.UnSubcribeForm;

@Repository
public class UserDAOImpl implements IUserDAO{
    @Autowired
	DataSource dataSource;
	
	@Override
	public Map getUnSubscribePage(UnSubcribeForm unSubcribeForm) throws Exception {
		GetUnSubscribePageSP getUnSubscribePageSP = new GetUnSubscribePageSP(dataSource);
		return getUnSubscribePageSP.execute(unSubcribeForm);
	}
	@Override
	public Map saveUserPreference(UnSubcribeForm unSubcribeForm) throws Exception {
		SaveUserPreferenceSP saveUserPreferenceSP = new SaveUserPreferenceSP(dataSource);
		return saveUserPreferenceSP.execute(unSubcribeForm);
	}

}
