package spring.dao.impl;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.layer.dao.sp.search.CourseDetailsInfoSP;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import spring.dao.IPreClearingLandingDAO;
import spring.dao.sp.clearing.ClearingCourseDetailsSP;
import spring.dao.sp.clearing.ClearingHomePageSP;
import spring.dao.sp.clearing.ClearingSubjectAjaxSP;
import spring.dao.sp.clearing.ClearingUniversityAjaxSP;
import spring.dao.sp.preclearing.ClearingEbookAboutWhatuniContentsSP;

@Repository
public class PreClearingLandingDAOImpl implements IPreClearingLandingDAO {

    @Autowired
    private DataSource dataSource;

    //Added to get pre clearing About whatuni SHTML contents details - Sujitha V for 31 March 2020
    public Map getEbookAboutWhatuniContents() {
        ClearingEbookAboutWhatuniContentsSP clearingEbookAboutWhatuniContentsSP = new ClearingEbookAboutWhatuniContentsSP(dataSource);
        Map resultMap = clearingEbookAboutWhatuniContentsSP.execute();
        return resultMap;
    }

    //This will return Clearing Home Page Details.
    public Map getClearingHomePageDetails(List inputList) { //## Start of WU11042012_RELEASE_SEKHAR K
        Long startDbtime = new Long(System.currentTimeMillis());
        ClearingHomePageSP clearingHomePageSP = new ClearingHomePageSP(dataSource);
        Map resultmap = clearingHomePageSP.execute(inputList);
        Long endDbtime = new Long(System.currentTimeMillis());
        if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
            new AdminUtilities().logDbCallTimeDetails("WU_CLEARING_PKG.clearing_home_page_prc", startDbtime, endDbtime);
        }
        return resultmap;
    }

    @Override
    public Map getClearingSubjectAjaxList(String subjectKeywordSearch) throws Exception {
        ClearingSubjectAjaxSP clearingSubjectAjax = new ClearingSubjectAjaxSP(dataSource);
        Map resultMap = clearingSubjectAjax.execute(subjectKeywordSearch);
        return resultMap;
    }

    @Override
    public Map getClearingUniAjaxList(String uniKeywordSearch) throws Exception {
        ClearingUniversityAjaxSP clearingUniversityAjaxSP = new ClearingUniversityAjaxSP(dataSource);
        Map resultMap = clearingUniversityAjaxSP.execute(uniKeywordSearch);
        return resultMap;
    }

    public Map getClearingCourseDetails(List inputList) {
      ClearingCourseDetailsSP clearingCourseDetailsSP = new ClearingCourseDetailsSP(dataSource);
      Map resultmap = clearingCourseDetailsSP.execute(inputList);
      return resultmap;
    }

}