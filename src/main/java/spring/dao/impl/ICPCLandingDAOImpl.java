package spring.dao.impl;

import java.util.Map;

import javax.sql.DataSource;

import spring.dao.ICPCLandingDAO;
import spring.dao.sp.cpc.GetCPCLightboxSP;
import spring.dao.sp.cpc.GetCPCTypeValuesSP;
import spring.dao.sp.cpc.MultipleLandingPageSP;
import spring.dao.sp.cpc.SingleLandingPageSP;
import spring.dao.util.valueobject.cpc.CPCLightboxVO;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ICPCLandingDAOImpl.java
 * Description   : Getting cpc related content..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Hema s.                   1.0               04.02.2020        First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public class ICPCLandingDAOImpl implements ICPCLandingDAO {
	
  private DataSource dataSource = null;
  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
  public DataSource getDataSource() {
    return dataSource;
  }
  
  //
  public Map getCpcTypeValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting campaign type by Hema.S on 04_02_2020_REL
    GetCPCTypeValuesSP GetCPCTypeValuesSP = new GetCPCTypeValuesSP(dataSource);
	Map resultMap = GetCPCTypeValuesSP.execute(cPCPageVO);
	return resultMap;
  }
  //
  public Map getMultipleLandingPageValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting multiple landing page values by Hema.S on 04_02_2020_REL
    MultipleLandingPageSP multipleLandingPageSP = new MultipleLandingPageSP(dataSource);
    Map resultMap = multipleLandingPageSP.execute(cPCPageVO);
    return resultMap;
  }
  //
  public Map getSingleLandingPageValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting single landing page values by Jeyalakshmi.D on 04_02_2020_REL
    SingleLandingPageSP singleLandingPageSP = new SingleLandingPageSP(dataSource);
    Map resultMap = singleLandingPageSP.execute(cPCPageVO);
    return resultMap;
  }
  //
  public Map getCPCLightboxValues(CPCLightboxVO cPCLightboxVO) throws Exception {// Added this for getting single landing page values by Jeyalakshmi.D on 04_02_2020_REL
    GetCPCLightboxSP getCPCLightboxSP = new GetCPCLightboxSP(dataSource);
    Map resultMap = getCPCLightboxSP.execute(cPCLightboxVO);
    return resultMap;
  }
}
