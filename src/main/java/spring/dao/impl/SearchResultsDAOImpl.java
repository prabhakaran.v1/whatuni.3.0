package spring.dao.impl;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.dao.ISearchResultsDAO;
import spring.dao.sp.searchresult.SearchResultSP;
import spring.pojo.searchresult.SearchResultsBean;
import spring.dao.sp.searchresult.GetFooterSectionSP;
import spring.dao.sp.searchresult.GetInstitutionNameAjaxSP;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.pojo.searchresult.GetInstitutionNameBean;

@Repository
public class SearchResultsDAOImpl implements ISearchResultsDAO{

	@Autowired
	private DataSource dataSource;
	
	@Override
	public Map<String, Object> getSearchResultsList(SearchResultsBean searchResultsBean) throws Exception {
		SearchResultSP searchResultSP = new SearchResultSP(dataSource);
		return searchResultSP.execute(searchResultsBean);
	}
	
	public Map<String, Object> getInstitutionNameList(GetInstitutionNameBean institutionNameBean) throws Exception {
		
		GetInstitutionNameAjaxSP getInstitutionNameAjaxSP = new GetInstitutionNameAjaxSP(dataSource);
		return getInstitutionNameAjaxSP.execute(institutionNameBean);
	}

	public Map<String, Object> getFooterSectionHtml(GetFooterSectionBean getFooterSectionBean) throws Exception {

		GetFooterSectionSP getFooterSectionSP = new GetFooterSectionSP(dataSource);
		return getFooterSectionSP.execute(getFooterSectionBean);
	}
}
