package spring.dao.impl;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import spring.dao.IRestDAO;

@Repository
public class RestDAOImpl implements IRestDAO {
  @Bean
  public RestTemplate restTemplate() {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
    return new RestTemplate(factory);
  }

  @Override
  public String callWebServices(final String URL, final String jsonString) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entity = new HttpEntity<>(jsonString, headers);
    return restTemplate().postForObject(URL, entity, String.class);
  }

  @Override
  public String callContentfulWebServices(String URL) { 
    return restTemplate().getForObject(URL, String.class);
  }
}
