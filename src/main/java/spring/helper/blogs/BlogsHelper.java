package spring.helper.blogs;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import spring.valueobject.blog.BlogVO;
import spring.valueobject.blog.PostVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.util.sql.DataModel;

public class BlogsHelper {

  public ArrayList customizeLatestPosts(ArrayList listOfLatestPostVO) {
    ArrayList listOfLatestPost = new ArrayList();
    if (listOfLatestPostVO != null && listOfLatestPostVO.size() > 0) {
      for (int i = 0; i < listOfLatestPostVO.size(); i++) {
        PostVO postVO = (PostVO)listOfLatestPostVO.get(i);
        PostVO customizedPostVO = new PostVO();
        customizedPostVO.setBlogId(postVO.getBlogId());
        customizedPostVO.setBlogUrlSeoName(postVO.getBlogUrlSeoName());
        customizedPostVO.setPostId(postVO.getPostId());
        customizedPostVO.setPostHeading(postVO.getPostHeading());
        customizedPostVO.setPostUrlSeoTitle(postVO.getPostUrlSeoTitle());
        customizedPostVO.setPostUpdatedDate(postVO.getPostUpdatedDate());
        customizedPostVO.setPostUrl(constructPostSeoUrl(customizedPostVO.getPostUrlSeoTitle()));
        listOfLatestPost.add(customizedPostVO);
      }
    }
    return listOfLatestPost;
  } //customizeBlogHomeLatestPosts

  public ArrayList customizePostDetails(ArrayList listOfPostHomeInfoVO, HttpServletRequest request) {
    ArrayList listOfPostHomeInfo = new ArrayList();
    if (listOfPostHomeInfoVO != null && listOfPostHomeInfoVO.size() > 0) {
      StringBuffer tmpSeoUrl = new StringBuffer();
      CommonFunction comFn = new CommonFunction();
      for (int i = 0; i < listOfPostHomeInfoVO.size(); i++) {
        PostVO postVO = (PostVO)listOfPostHomeInfoVO.get(i);
        PostVO customizedPostVO = new PostVO();
        customizedPostVO.setBlogId(postVO.getBlogId());
        customizedPostVO.setAuthorName(postVO.getAuthorName());
        customizedPostVO.setAuthorName(postVO.getAuthorName());
        customizedPostVO.setAuthorMediaPath(postVO.getAuthorMediaPath());
        customizedPostVO.setAuthorMediaPath(getThumbnail(customizedPostVO.getAuthorMediaPath(), "_60px"));
        customizedPostVO.setPostId(postVO.getPostId());
        customizedPostVO.setPostHeading(postVO.getPostHeading());
        customizedPostVO.setPostFullDescription(postVO.getPostFullDescription());
        customizedPostVO.setPostLdcsCode(postVO.getPostLdcsCode());
        customizedPostVO.setPostBlogQualId(postVO.getPostBlogQualId());
        customizedPostVO.setPostUrlSeoTitle(postVO.getPostUrlSeoTitle());
        customizedPostVO.setPostMetaTitle(postVO.getPostMetaTitle());
        customizedPostVO.setPostMetaDescription(postVO.getPostMetaDescription());
        customizedPostVO.setPostMetaKeywords(postVO.getPostMetaKeywords());
        //set following details for <meta> usage
        request.setAttribute("currentBlogId", customizedPostVO.getBlogId());
        request.setAttribute("currentPostId", customizedPostVO.getPostId());
        request.setAttribute("postAuthorName", customizedPostVO.getAuthorName());
        request.setAttribute("postUrlSeoTitle", customizedPostVO.getPostUrlSeoTitle());
        request.setAttribute("postMetaTitle", customizedPostVO.getPostMetaTitle());
        request.setAttribute("postMetaDescription", customizedPostVO.getPostMetaDescription());
        request.setAttribute("postMetaKeywords", customizedPostVO.getPostMetaKeywords());
        //
        customizedPostVO.setPostMediaId(postVO.getPostMediaId());
        customizedPostVO.setPostMediaPath(postVO.getPostMediaPath());
        if (customizedPostVO.getPostMediaPath() != null) {
          customizedPostVO.setPostMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(postVO.getPostMediaPath(), "4"));
        }
        customizedPostVO.setPostMediaType(postVO.getPostMediaType());
        customizedPostVO.setPostUpdatedDate(postVO.getPostUpdatedDate());
        //construct next/previous post URLs          
        customizedPostVO.setNextPostUrlSeoTitle(postVO.getNextPostUrlSeoTitle());
        customizedPostVO.setPreviousPostUrlSeoTitle(postVO.getPreviousPostUrlSeoTitle());
        customizedPostVO.setNextPostUrl(constructPostSeoUrl(customizedPostVO.getNextPostUrlSeoTitle()));
        customizedPostVO.setPreviousPostUrl(constructPostSeoUrl(customizedPostVO.getPreviousPostUrlSeoTitle()));
        listOfPostHomeInfo.add(customizedPostVO);
      }
    }
    return listOfPostHomeInfo;
  } //customizePostHomeInfo 

  public boolean isThisPostAvailable(String postUrlSeoTitle) {
    boolean availabilityFlag = false;
    if (postUrlSeoTitle != null && postUrlSeoTitle.trim().length() > 0) {
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      String isAvailabilityFlag = "";
      parameters.add(postUrlSeoTitle);
      try {
        isAvailabilityFlag = dataModel.getString("WU_BLOGS_PKG.IS_POST_AVILABLE_FN", parameters);
      } catch (Exception exception) {
        exception.printStackTrace();
      }
      if (isAvailabilityFlag != null && isAvailabilityFlag.equalsIgnoreCase("TRUE")) {
        availabilityFlag = true;
      }
    }
    return availabilityFlag;
  }

  public ArrayList customizePremiumPosts(ArrayList listOfPremiumPostsVO) {
    ArrayList listOfPremiumPosts = new ArrayList();
    if (listOfPremiumPostsVO != null && listOfPremiumPostsVO.size() > 0) {
      for (int i = 0; i < listOfPremiumPostsVO.size(); i++) {
        BlogVO blogVO = (BlogVO)listOfPremiumPostsVO.get(i);
        BlogVO customizedBlogVO = new BlogVO();
        customizedBlogVO.setAuthorId(blogVO.getAuthorId());
        customizedBlogVO.setAuthorName(blogVO.getAuthorName());
        customizedBlogVO.setAuthorMediaPath(blogVO.getAuthorMediaPath());
        customizedBlogVO.setAuthorMediaPath(getThumbnail(customizedBlogVO.getAuthorMediaPath(), "_60px"));
        customizedBlogVO.setAuthorShortBio(blogVO.getAuthorShortBio());
        customizedBlogVO.setPostId(blogVO.getPostId());
        customizedBlogVO.setPostHeading(blogVO.getPostHeading());
        customizedBlogVO.setPostShortDescription(blogVO.getPostShortDescription());
        customizedBlogVO.setPostUpdatedDate(blogVO.getPostUpdatedDate());
        customizedBlogVO.setPostUrlSeoTitle(blogVO.getPostUrlSeoTitle());
        customizedBlogVO.setPostUrl(constructPostSeoUrl(customizedBlogVO.getPostUrlSeoTitle()));
        listOfPremiumPosts.add(customizedBlogVO);
      }
    }
    return listOfPremiumPosts;
  } //customizeBlogHomeLatestPosts

  public String getThumbnail(String fileName, String thumbnailVersion) {
    String thumbnail = fileName;
    if (thumbnail != null && thumbnail.trim().length() > 0) {
      CommonUtil comUtil = new CommonUtil();
      String fileExtension = comUtil.getFileExtension(thumbnail);
      thumbnail = comUtil.removeFileExtension(thumbnail);
      thumbnail = thumbnail + thumbnailVersion + fileExtension;
    }
    return thumbnail;
  }

  public String constructPostSeoUrl(String postSeoUrlSeoTitle) {
    String postSeoUrl = "";
    if (postSeoUrlSeoTitle != null && postSeoUrlSeoTitle.trim().length() > 0) {
      StringBuffer tmpSeoUrl = new StringBuffer();
      // POST_DETAILS page SEO_URL_PATTERN: http://www.whatuni.com/student-blog/[SEO_TITLE]/blog.html 
      tmpSeoUrl.append("/student-blog/");
      tmpSeoUrl.append(replaceSpaceByHypen(postSeoUrlSeoTitle));
      tmpSeoUrl.append("/blog.html");
      postSeoUrl = tmpSeoUrl.toString().toLowerCase();
      //nullify unused objects
      tmpSeoUrl.setLength(0); // clear the string buffer to reuse       
      tmpSeoUrl = null;
    }
    return postSeoUrl;
  }

  public boolean isPostSeoTitleSame(String postSeoTitleFromTable, String postSeoTitleInUrl) {
    return false;
  }

  public String replaceSpaceByHypen(String inString) {
    if (inString != null && inString.trim().length() > 0) {
      inString = inString.replaceAll("     ", "-"); //five spaces
      inString = inString.replaceAll("    ", "-"); //four spaces
      inString = inString.replaceAll("   ", "-"); //three spaces
      inString = inString.replaceAll("  ", "-"); //two spaces
      inString = inString.replaceAll(" ", "-"); //one space    
      inString = inString.replaceAll("-----", "-"); //five hyphen
      inString = inString.replaceAll("----", "-"); //four hyphen
      inString = inString.replaceAll("---", "-"); //three hyphen
      inString = inString.replaceAll("--", "-"); //two hyphen      
    }
    return inString;
  }
  //
  /*
   public ArrayList customizeBlogHomeLatestPosts(ArrayList listOfLatestPostVO) {
     ArrayList listOfLatestPosts = new ArrayList();
     if (listOfLatestPostVO != null && listOfLatestPostVO.size() > 0) {
       CommonFunction comFn = new CommonFunction();
       StringBuffer tmpSeoUrl = new StringBuffer();
       ArrayList listOfPostsBelongsToThisBlogVO = new ArrayList();
       ArrayList listOfPostsBelongsToThisBlog = new ArrayList();
       for (int i = 0; i < listOfLatestPostVO.size(); i++) {
         BlogVO blogVO = (BlogVO)listOfLatestPostVO.get(i);
         BlogVO customizedBlogVO = new BlogVO();
         customizedBlogVO.setBlogId(blogVO.getBlogId());
         customizedBlogVO.setBlogUrlSeoName(blogVO.getBlogUrlSeoName());
         listOfPostsBelongsToThisBlogVO = blogVO.getListOfPostsBelongsToThisBlog();
         if (listOfPostsBelongsToThisBlogVO != null && listOfPostsBelongsToThisBlogVO.size() > 0) {
           for (int j = 0; j < listOfPostsBelongsToThisBlogVO.size(); j++) {
             PostVO postVO = (PostVO)listOfPostsBelongsToThisBlogVO.get(j);
             PostVO customizedPostVO = new PostVO();
             customizedPostVO.setPostId(postVO.getPostId());
             customizedPostVO.setPostHeading(postVO.getPostHeading());
             customizedPostVO.setPostUrlSeoTitle(postVO.getPostUrlSeoTitle());
             customizedPostVO.setPostUpdatedDate(postVO.getPostUpdatedDate());
             /// http://www.whatuni.com/degrees/student-blog/[SEO_TITLE]/[POST_ID]/blog.html //
             tmpSeoUrl.append("/degrees/student-blog/");
             tmpSeoUrl.append(new CommonFunction().replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(customizedPostVO.getPostUrlSeoTitle()))));
             tmpSeoUrl.append("/");
             tmpSeoUrl.append(customizedPostVO.getPostId());
             tmpSeoUrl.append("/blog.html");
             customizedPostVO.setPostUrl(tmpSeoUrl.toString().toLowerCase());
             tmpSeoUrl.setLength(0); // clear the string buffer to reuse
             listOfPostsBelongsToThisBlog.add(customizedPostVO);
           }
         }
         customizedBlogVO.setListOfPostsBelongsToThisBlog(listOfPostsBelongsToThisBlog);
         listOfLatestPosts.add(customizedBlogVO);
       }
     }
     return listOfLatestPosts;
   } //customizeBlogHomeLatestPosts
*/

}
