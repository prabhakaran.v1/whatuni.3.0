package spring.helper;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import spring.valueobject.interaction.CpeInteractionVO;
import spring.valueobject.profile.ProfileTabsVO;
import spring.valueobject.search.CpeWorstPerformersVO;
import spring.valueobject.seo.PageMetaTagsVO;
import WUI.basket.form.BasketBean;
import WUI.cpe.form.CollegeCpeInteractionDetailsWithMediaBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.form.CpeWorstPerformersBean;

public class CommonHelper {

  public ArrayList addRobotToMetaTagDetails(ArrayList pageMetaTagDetails, String robotValue, HttpServletRequest request) {
    ArrayList customizedPageMetaTagDetails = new ArrayList();
    if (pageMetaTagDetails != null && pageMetaTagDetails.size() > 0) {
      PageMetaTagsVO sourceVO = null;
      PageMetaTagsVO targetVO = null;
      for (int i = 0; i < pageMetaTagDetails.size(); i++) {
        //
        sourceVO = (PageMetaTagsVO)pageMetaTagDetails.get(i);
        targetVO = new PageMetaTagsVO();
        //
        targetVO.setMetaDescription(sourceVO.getMetaDescription());
        targetVO.setMetaKeywords(sourceVO.getMetaKeywords());
        targetVO.setMetaTitle(sourceVO.getMetaTitle());
        targetVO.setMetaStudylevelDesc(sourceVO.getMetaStudylevelDesc());
        targetVO.setMetaCategoryDesc(sourceVO.getMetaCategoryDesc());
        //
        targetVO.setMetaRobots(robotValue);
        request.setAttribute("currentPageMetaTitle",targetVO.getMetaTitle());
        //
        customizedPageMetaTagDetails.add(targetVO);
      }
    }
    return customizedPageMetaTagDetails;
  }

  public ArrayList customizeUserBasketPodList(ArrayList userBasketPodList, HttpServletRequest request, HttpServletResponse response) {
    ArrayList customizedUserBasketPodList = new ArrayList();
    if (userBasketPodList != null && userBasketPodList.size() > 0) {
      BasketBean sourceBean = null;
      BasketBean targetBean = null;
      int collegeCount = 0;
      int courseCount = 0;
      String cookieBasketId = "";
      for (int i = 0; i < userBasketPodList.size(); i++) {
        sourceBean = (BasketBean)userBasketPodList.get(i);
        targetBean = new BasketBean();
        //
        targetBean.setAssociationId(sourceBean.getAssociationId());
        targetBean.setAssociationName(sourceBean.getAssociationName());
        targetBean.setAssociationType(sourceBean.getAssociationType());
        targetBean.setWebSiteAddress(sourceBean.getWebSiteAddress());
        targetBean.setCourseCollegeId(sourceBean.getCourseCollegeId());
        targetBean.setCourseCollegeName(sourceBean.getCourseCollegeName());
        targetBean.setCourseStudyLevel(sourceBean.getCourseStudyLevel());
        targetBean.setSubOrderItemId(sourceBean.getSubOrderItemId());
        targetBean.setSubOrderWebsite(sourceBean.getSubOrderItemId());
        if (targetBean.getAssociationType().equalsIgnoreCase("C")) {
          collegeCount++;
        } else {
          courseCount++;
        }
        targetBean.setBasketId(sourceBean.getBasketId());
        cookieBasketId = targetBean.getBasketId();
        customizedUserBasketPodList.add(targetBean);
      }
      if (cookieBasketId != null && !cookieBasketId.equals("")) {
        Cookie cookie = CookieManager.createCookie(request,"basketId", cookieBasketId);
        response.addCookie(cookie);
      }
      request.getSession().setAttribute("basketpodcollegecount", String.valueOf(collegeCount));
      request.getSession().setAttribute("basketpodcourseCount", String.valueOf(courseCount));
    }
    return customizedUserBasketPodList;
  }

  /**
   * will teafer VO to Bean
   * @param listOfCpeWorstPerformersVO
   * @param request
   * @return
   */
  public ArrayList customizeCpeWorstPerformers(ArrayList listOfCpeWorstPerformersVO, HttpServletRequest request) {
    ArrayList customizedCpeWorstPerformers = new ArrayList();
    if (listOfCpeWorstPerformersVO != null && listOfCpeWorstPerformersVO.size() > 0) {
      CpeWorstPerformersVO cpeWorstPerformersVO = null;
      CpeWorstPerformersBean cpeWorstPerformersBean = null;
      for (int i = 0; i < listOfCpeWorstPerformersVO.size(); i++) {
        cpeWorstPerformersVO = (CpeWorstPerformersVO)listOfCpeWorstPerformersVO.get(i);
        cpeWorstPerformersBean = new CpeWorstPerformersBean();
        cpeWorstPerformersBean.setCollegeId(cpeWorstPerformersVO.getCollegeId());
        cpeWorstPerformersBean.setCollegeName(cpeWorstPerformersVO.getCollegeName());
        cpeWorstPerformersBean.setCollegeNameDisplay(cpeWorstPerformersVO.getCollegeNameDisplay());
        cpeWorstPerformersBean.setLdcs1Code(cpeWorstPerformersVO.getLdcs1Code());
        cpeWorstPerformersBean.setLdcs1Description(cpeWorstPerformersVO.getLdcs1Description());
        cpeWorstPerformersBean.setHashRemovedLdcs1Description(cpeWorstPerformersVO.getHashRemovedLdcs1Description());
        request.setAttribute("worstPerformerSubjectName", cpeWorstPerformersBean.getLdcs1Description());
        cpeWorstPerformersBean.setQualificationCode(cpeWorstPerformersVO.getQualificationCode());
        cpeWorstPerformersBean.setQualificationDescription(cpeWorstPerformersVO.getQualificationDescription());
        cpeWorstPerformersBean.setOrderItemId(cpeWorstPerformersVO.getOrderItemId());
        cpeWorstPerformersBean.setSubOrderItemId(cpeWorstPerformersVO.getSubOrderItemId());
        cpeWorstPerformersBean.setMyhcProfileId(cpeWorstPerformersVO.getMyhcProfileId());
        cpeWorstPerformersBean.setProfileType(cpeWorstPerformersVO.getProfileType());
        cpeWorstPerformersBean.setSubOrderEmail(cpeWorstPerformersVO.getSubOrderEmail());
        cpeWorstPerformersBean.setSubOrderProspectus(cpeWorstPerformersVO.getSubOrderProspectus());
        cpeWorstPerformersBean.setCpeQualificationNetworkId(cpeWorstPerformersVO.getCpeQualificationNetworkId());
        cpeWorstPerformersBean.setSubOrderWebsite(cpeWorstPerformersVO.getSubOrderWebsite());
        cpeWorstPerformersBean.setSubOrderEmailWebform(cpeWorstPerformersVO.getSubOrderEmailWebform());
        cpeWorstPerformersBean.setSubOrderProspectusWebform(cpeWorstPerformersVO.getSubOrderProspectusWebform());
        cpeWorstPerformersBean.setMediaId(cpeWorstPerformersVO.getMediaId());
        cpeWorstPerformersBean.setMediaPath(cpeWorstPerformersVO.getMediaPath());
        cpeWorstPerformersBean.setMediaType(cpeWorstPerformersVO.getMediaType());
        cpeWorstPerformersBean.setMediaThumbPath(cpeWorstPerformersVO.getMediaThumbPath());
        if (cpeWorstPerformersBean.getMediaThumbPath() == null || cpeWorstPerformersBean.getMediaThumbPath().trim().length() == 0) {
          cpeWorstPerformersBean.setMediaThumbPath(cpeWorstPerformersBean.getMediaPath());
        }
        customizedCpeWorstPerformers.add(cpeWorstPerformersBean);
      }
    }
    return customizedCpeWorstPerformers;
  }

  /**
   * will customize all profile-tabs related details
   *
   * @param listOfProfileTabsVO
   * @param cpeQualificationNetworkId
   * @return
   */
  public ArrayList customizeProfileTabsDetails(ArrayList listOfProfileTabsVO, String cpeQualificationNetworkId) {
    ArrayList listOfProfileTabs = new ArrayList();
    if (listOfProfileTabsVO != null && listOfProfileTabsVO.size() > 0) {
      CommonUtil commonUtil = new CommonUtil();
      HashMap sectionmap = new HashMap();
      sectionmap.put("WELFARE", "Welfare");
      sectionmap.put("STUDENT STORY", "Student story");
      sectionmap.put("STUDENT UNION & ENTERTAINMENT", "Funding");
      sectionmap.put("OPEN DAYS", "Open days");
      sectionmap.put("LOCATION", "Location");
      sectionmap.put("COURSES, ACADEMICS", "Courses/academics");
      sectionmap.put("ACCOMMODATION", "Accommodation");
      sectionmap.put("OVERVIEW", "Overview");
      sectionmap.put("UNI CONTACT DETAILS", "Contact");
      sectionmap.put("JOB PROSPECTS", "Jobs");
      sectionmap.put("SUBJECT STRENGTHS", "Subject strengths");
      sectionmap.put("CONTACT DETAILS", "Contact");
      sectionmap.put("ACCOMMODATION & LOCATION", "Accommodation");
      sectionmap.put("COURSES, ACADEMICS & FACILITIES", "Courses/facilities");
      sectionmap.put("FEES AND FUNDING", "Funding");
      sectionmap.put("ENTERTAINMENT", "Entertainment");
      sectionmap.put("COURSES AND ADMISSIONS", "Courses/admissions");
      String tmpStr = "";
      for (int i = 0; i < listOfProfileTabsVO.size(); i++) {
        ProfileTabsVO profileTabsVO = (ProfileTabsVO)listOfProfileTabsVO.get(i);
        ProfileTabsVO customizedProfileTabsVO = new ProfileTabsVO();
        customizedProfileTabsVO.setMyhcProfileId(profileTabsVO.getMyhcProfileId().trim());
        customizedProfileTabsVO.setProfileId(profileTabsVO.getProfileId().trim());
        tmpStr = profileTabsVO.getButtonLabel();
        if (tmpStr != null && tmpStr.equalsIgnoreCase("OVERVIEW")) {
          tmpStr = new CommonUtil().toTitleCase(tmpStr);
          tmpStr=  tmpStr.trim();
            
        }
        customizedProfileTabsVO.setButtonLabel(tmpStr);
        customizedProfileTabsVO.setCpeQualificationNetworkId(cpeQualificationNetworkId);
        customizedProfileTabsVO.setSeoButtonLabel(commonUtil.removeSpecialCharAndConstructSeoText(customizedProfileTabsVO.getButtonLabel()));
        //customizedProfileTabsVO.setCustomButtonLabel((String)sectionmap.get(customizedProfileTabsVO.getButtonLabel().toUpperCase()));   
        customizedProfileTabsVO.setSeoCustomButtonLabel(commonUtil.removeSpecialCharAndConstructSeoText(customizedProfileTabsVO.getButtonLabel()));
        listOfProfileTabs.add(customizedProfileTabsVO);
      }
    }
    return listOfProfileTabs; // now it is returning empty value, because all value sat into request scope
  } //end of customizeProfileTabsDetails()

  /**
   * this fuction will customize all interactivity related details
   *
   * @param listOfCpeInteractionVO
   * @param request
   * @return
   */
  public ArrayList customizeProfileInteractionDetails(ArrayList listOfCpeInteractionVO, HttpServletRequest request) {
    ArrayList listOfCpeInteractionDetails = new ArrayList();
    if (listOfCpeInteractionVO != null && listOfCpeInteractionVO.size() > 0) {
      CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
      for (int i = 0; i < listOfCpeInteractionVO.size(); i++) {
        collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
        CpeInteractionVO cpeInteractionVO = (CpeInteractionVO)listOfCpeInteractionVO.get(i);
        //
        collegeCpeInteractionDetailsWithMedia.setOrderItemId(cpeInteractionVO.getOrderItemId());
        collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(cpeInteractionVO.getSubOrderItemId());
        collegeCpeInteractionDetailsWithMedia.setMyhcProfileId(cpeInteractionVO.getMyhcProfileId());
        collegeCpeInteractionDetailsWithMedia.setProfileType(cpeInteractionVO.getProfileType());
        collegeCpeInteractionDetailsWithMedia.setSubOrderEmail(cpeInteractionVO.getSubOrderEmail());
        collegeCpeInteractionDetailsWithMedia.setSubOrderProspectus(cpeInteractionVO.getSubOrderProspectus());
        collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(cpeInteractionVO.getCpeQualificationNetworkId());
        //
        String tmpUrl = cpeInteractionVO.getSubOrderWebsite();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
        //
        tmpUrl = cpeInteractionVO.getSubOrderEmailWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderEmailWebform(tmpUrl);
        //
        tmpUrl = cpeInteractionVO.getSubOrderProspectusWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderProspectusWebform(tmpUrl);
        collegeCpeInteractionDetailsWithMedia.setHotLineNo(cpeInteractionVO.getHotLineNo());
        //request.setAttribute("subOrderItemId", collegeCpeInteractionDetailsWithMedia.getSubOrderItemId());
        listOfCpeInteractionDetails.add(collegeCpeInteractionDetailsWithMedia);
      }
    }
    return listOfCpeInteractionDetails; // now it is returning empty value, because all value sat into request scope
  } //end of customizeProfileInteractionDetails()

}//end of class
