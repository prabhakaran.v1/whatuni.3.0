package spring.helper.opendays;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import spring.valueobject.opendays.OpenDaysCalendarVO;
import WUI.openday.form.OpenDaysCalendarBean;

public class OpendaysHelper {

  public ArrayList customizeOpendaysCalendar(ArrayList listOfOpendaysCalendarVO, HttpServletRequest request) {
    ArrayList customizedOpendaysCalendar = new ArrayList();
    if (listOfOpendaysCalendarVO != null && listOfOpendaysCalendarVO.size() > 0) {
      OpenDaysCalendarVO openDaysCalendarVO = null;
      OpenDaysCalendarBean openDaysCalendarBean = null;
      String commaSeparatedOpendays = "";
      for (int i = 0; i < listOfOpendaysCalendarVO.size(); i++) {
        //       
        openDaysCalendarVO = (OpenDaysCalendarVO)listOfOpendaysCalendarVO.get(i);
        openDaysCalendarBean = new OpenDaysCalendarBean();
        //
        openDaysCalendarBean.setCollegeId(openDaysCalendarVO.getCollegeId());
        openDaysCalendarBean.setCollegeName(openDaysCalendarVO.getCollegeName());
        openDaysCalendarBean.setCollegeNameDisplay(openDaysCalendarVO.getCollegeNameDisplay());
        openDaysCalendarBean.setCollegeLocation(openDaysCalendarVO.getCollegeLocation());
        openDaysCalendarBean.setCollegeDepartmentDescription(openDaysCalendarVO.getCollegeDepartmentDescription());
        openDaysCalendarBean.setCollegeOpendayExternalUrl(openDaysCalendarVO.getCollegeOpendayExternalUrl());
        openDaysCalendarBean.setOpenDate(openDaysCalendarVO.getOpenDate());
        //wu_320 changes done by Sekhar K 
        openDaysCalendarBean.setOpenDay_date(openDaysCalendarVO.getOpenDay_date());
        openDaysCalendarBean.setOpenDay_day(openDaysCalendarVO.getOpenDay_day());
        openDaysCalendarBean.setOpenDay_month_year(openDaysCalendarVO.getOpenDay_month_year());
        openDaysCalendarBean.setSubOrderItemId(openDaysCalendarVO.getSubOrderItemId());
        openDaysCalendarBean.setSubOrderWebsite(openDaysCalendarVO.getSubOrderWebsite());
        openDaysCalendarBean.setCpeQualificationNetworkId(openDaysCalendarVO.getCpeQualificationNetworkId());
        openDaysCalendarBean.setSubOrderProspectusWebform(openDaysCalendarVO.getSubOrderProspectusWebform());
        openDaysCalendarBean.setTotalRecordsCount(openDaysCalendarVO.getTotalRecordsCount());
        openDaysCalendarBean.setDateExistFlg(openDaysCalendarVO.getDateExistFlg());
        openDaysCalendarBean.setPersonalNotes(openDaysCalendarVO.getPersonalNotes());
        openDaysCalendarBean.setPastFutureDate(openDaysCalendarVO.getPastFutureDate());
        openDaysCalendarBean.setEmailURLString(openDaysCalendarVO.getEmailURLString());
        openDaysCalendarBean.setEventId(openDaysCalendarVO.getEventId());
        if (openDaysCalendarBean.getOpenDate() != null && openDaysCalendarBean.getOpenDate().trim().length() > 0) {
          String[] opendayArray = openDaysCalendarBean.getOpenDate().split("/"); //if date-format = YYYY/MM/DD HH:MM:SS.MS
          if (opendayArray.length < 2) {
            opendayArray = openDaysCalendarBean.getOpenDate().split("-"); //if date-format = YYYY-MM-DD HH:MM:SS.MS
          }
          //
          openDaysCalendarBean.setOpenDate_day(opendayArray[2].replaceAll(" 00:00:00.0", "")); //remove time part from day        
          if (openDaysCalendarBean.getOpenDate_day().charAt(0) == '0') { //remove first zero from day
            openDaysCalendarBean.setOpenDate_day(openDaysCalendarBean.getOpenDate_day().replaceAll("0", ""));
          }
          /*if ("".equals(commaSeparatedOpendays)) {
            commaSeparatedOpendays = openDaysCalendarBean.getOpenDate_day();
          } else {
            commaSeparatedOpendays = commaSeparatedOpendays + "," + openDaysCalendarBean.getOpenDate_day();
          }
          request.setAttribute("commaSeparatedOpendays",commaSeparatedOpendays);
          */
          //
          openDaysCalendarBean.setOpenDate_month(opendayArray[1]);
          if (openDaysCalendarBean.getOpenDate_month().charAt(0) == '0') { //remove first zero from month
            openDaysCalendarBean.setOpenDate_month(openDaysCalendarBean.getOpenDate_month().replaceAll("0", ""));
          }
          request.setAttribute("currentMonth",openDaysCalendarBean.getOpenDate_month());
          //
          openDaysCalendarBean.setOpenDate_year(opendayArray[0]);
          request.setAttribute("currentYear",openDaysCalendarBean.getOpenDate_year());
          //
        }
        customizedOpendaysCalendar.add(openDaysCalendarBean);
      }
    }
    return customizedOpendaysCalendar;
  }
  
}
