package spring.helper.articles;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import spring.valueobject.articles.ArticleGroupVO;
import spring.valueobject.articles.ArticleVO;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;

public class ArticleHelper {

  /**
   * location based articles
   *
   * @param location
   * @return studyLevelArticleList
   */
  public ArrayList getLocationBasedArticleList(String location, String numberOfArticlesNeeded, String articleDescCharecterCount) {
    ArrayList locationBasedlArticleList = new ArrayList();
    if (location != null && location.trim().length() > 0) {
      DataModel dataModel = new DataModel();
      ResultSet resultset = null;
      ArticleGroupVO articleGroupVO = null;
      Vector parameters = new Vector();
      parameters.add(location);
      parameters.add(articleDescCharecterCount);
      parameters.add(numberOfArticlesNeeded);
      try {
        resultset = dataModel.getResultSet("WU_ARTICLES_PKG.GET_ARTICLES_BY_LOCATION_FN", parameters);
        while (resultset.next()) {
          articleGroupVO = new ArticleGroupVO();
          articleGroupVO.setArticleGroupUrlSeoName(resultset.getString("ARTICLE_GROUP_URL_SEO_NAME"));
          articleGroupVO.setArticleGroupName(articleGroupVO.getArticleGroupUrlSeoName());
          articleGroupVO.setArticleHeading(resultset.getString("ARTICLE_HEADING"));
          articleGroupVO.setArticleUrlSeoTitle(resultset.getString("ARTICLE_URL_SEO_TITLE"));
          articleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(articleGroupVO.getArticleUrlSeoTitle(),articleGroupVO.getArticleGroupUrlSeoName()));
          articleGroupVO.setArticleUpdatedDate(resultset.getString("ARTICLE_UPDATED_DATE"));
          articleGroupVO.setTotalArticlesForThisArticleGroup(resultset.getString("ARTICLE_COUNT"));
          articleGroupVO.setArticleShortDescription(resultset.getString("ARTICLE_SHORT_DESC"));
          locationBasedlArticleList.add(articleGroupVO);
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel.closeCursor(resultset);
        dataModel = null;
        resultset = null;
        parameters.clear();
        parameters = null;
      }
    }
    return locationBasedlArticleList;
  } //getLocationBasedArticleList

  public ArrayList customizeArchivedArticles(ArrayList listOfArchivedArticlesVO) {
    ArrayList listOfArchivedArticles = new ArrayList();
    if (listOfArchivedArticlesVO != null && listOfArchivedArticlesVO.size() > 0) {
      for (int i = 0; i < listOfArchivedArticlesVO.size(); i++) {
        ArticleGroupVO articleGroupVO = (ArticleGroupVO)listOfArchivedArticlesVO.get(i);
        ArticleGroupVO customizedArticleGroupVO = new ArticleGroupVO();
        customizedArticleGroupVO.setArticleGroupName(articleGroupVO.getArticleGroupName());
        customizedArticleGroupVO.setArticleGroupUrlSeoName(articleGroupVO.getArticleGroupUrlSeoName());
        customizedArticleGroupVO.setTotalArticlesForThisArticleGroup(articleGroupVO.getTotalArticlesForThisArticleGroup());
        customizedArticleGroupVO.setArticleHeading(articleGroupVO.getArticleHeading());
        customizedArticleGroupVO.setArticleUpdatedDate(articleGroupVO.getArticleUpdatedDate());
        customizedArticleGroupVO.setArticleUrlSeoTitle(articleGroupVO.getArticleUrlSeoTitle());
        customizedArticleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(customizedArticleGroupVO.getArticleUrlSeoTitle(),customizedArticleGroupVO.getArticleGroupName()));
        listOfArchivedArticles.add(customizedArticleGroupVO);
      }
    }
    return listOfArchivedArticles;
  } //customizeArchivedArticles

  /**
   * @param qualCode, categoryCode
   * @return
   */
  public ArrayList getCategoryRelatedArticleList(String qualCode, String categoryId,String articleDescCharecterCount) {
    ArrayList categoryRelatedArticleList = null;
    if (qualCode != null && qualCode.trim().length() == 1) {
      categoryRelatedArticleList = new ArrayList();
      DataModel dataModel = new DataModel();
      ResultSet resultset = null;
      ArticleGroupVO articleGroupVO = null;
      //
      Vector parameters = new Vector();
      parameters.add(qualCode);
      parameters.add(categoryId);
      parameters.add(articleDescCharecterCount);
      //parameters.add(includeStudyLevelArticle);
      try {
        resultset = dataModel.getResultSet("WU_ARTICLES_PKG.GET_ARTICLES_BY_CATEGORY_FN", parameters);
        while (resultset.next()) {
          articleGroupVO = new ArticleGroupVO();
          articleGroupVO.setArticleGroupName(resultset.getString("ARTICLE_GROUP_URL_SEO_NAME"));
          articleGroupVO.setArticleGroupUrlSeoName(articleGroupVO.getArticleGroupName());
          articleGroupVO.setArticleHeading(resultset.getString("ARTICLE_HEADING"));
          articleGroupVO.setArticleUrlSeoTitle(resultset.getString("ARTICLE_URL_SEO_TITLE"));
          articleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(articleGroupVO.getArticleUrlSeoTitle(),articleGroupVO.getArticleGroupUrlSeoName()));
          articleGroupVO.setArticleUpdatedDate(resultset.getString("ARTICLE_UPDATED_DATE"));
          articleGroupVO.setArticleShortDescription(resultset.getString("ARTICLE_SHORT_DESC"));
         // articleGroupVO.setAuthorName(resultset.getString("AUTHOR_NAME"));
          articleGroupVO.setArticleMediaPath(new CommonUtil().getThumbnail(resultset.getString("ARTICLE_THUMB_PATH"), ""));
          categoryRelatedArticleList.add(articleGroupVO);
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel.closeCursor(resultset);
        dataModel = null;
        resultset = null;
        parameters.clear();
        parameters = null;
        articleGroupVO = null;
      }
    }
    return categoryRelatedArticleList;
  } //getCategoryRelatedArticleList

  /**
   * qualCode = {M - Degree, N - , A - , T - , L - , S - Scholarship, E - Editor's rambling, G - General}
   *
   * @param qualCode
   * @return studyLevelArticleList
   */
  public ArrayList getStudyLevelArticleList(String qualCode, String numberOfArticlesNeeded, String articleDescCharecterCount) {
    ArrayList studyLevelArticleList = new ArrayList();
    if (qualCode != null && qualCode.trim().length() == 1) {
      DataModel dataModel = new DataModel();
      ResultSet resultset = null;
      ArticleGroupVO articleGroupVO = null;
      Vector parameters = new Vector();
      parameters.add(qualCode);
      parameters.add(articleDescCharecterCount);
      parameters.add(numberOfArticlesNeeded);
      try {
        resultset = dataModel.getResultSet("WU_ARTICLES_PKG.GET_ARTICLES_BY_QUAL_FN", parameters);
        while (resultset.next()) {
          articleGroupVO = new ArticleGroupVO();
          articleGroupVO.setArticleGroupUrlSeoName(resultset.getString("ARTICLE_GROUP_URL_SEO_NAME"));
          articleGroupVO.setArticleGroupName(articleGroupVO.getArticleGroupUrlSeoName());
          articleGroupVO.setArticleHeading(resultset.getString("ARTICLE_HEADING"));
          articleGroupVO.setArticleUrlSeoTitle(resultset.getString("ARTICLE_URL_SEO_TITLE"));
          articleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(articleGroupVO.getArticleUrlSeoTitle(),articleGroupVO.getArticleGroupName()));
          articleGroupVO.setArticleUpdatedDate(resultset.getString("ARTICLE_UPDATED_DATE"));
          articleGroupVO.setTotalArticlesForThisArticleGroup(resultset.getString("ARTICLE_COUNT"));
          articleGroupVO.setArticleShortDescription(resultset.getString("ARTICLE_SHORT_DESC"));
          studyLevelArticleList.add(articleGroupVO);
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel.closeCursor(resultset);
        dataModel = null;
        resultset = null;
        parameters.clear();
        parameters = null;
        articleGroupVO = null;
      }
    }
    return studyLevelArticleList;
  } //getStudyLevelArticleList

  public ArrayList customizeArticleDetails(ArrayList listOfArticleDetailsInfoVO, HttpServletRequest request) {
    ArrayList listOfArticleDetailsInfo = new ArrayList();
    if (listOfArticleDetailsInfoVO != null && listOfArticleDetailsInfoVO.size() > 0) {
      for (int i = 0; i < listOfArticleDetailsInfoVO.size(); i++) {
        ArticleVO articleVO = (ArticleVO)listOfArticleDetailsInfoVO.get(i);
        ArticleVO customizedArticleVO = new ArticleVO();
        customizedArticleVO.setArticleGroupId(articleVO.getArticleGroupId());
        customizedArticleVO.setArticleGroupHeading(articleVO.getArticleGroupHeading());
        customizedArticleVO.setArticleGroupUrlSeoName(articleVO.getArticleGroupUrlSeoName());
        customizedArticleVO.setAuthorName(articleVO.getAuthorName());
        customizedArticleVO.setAuthorName(articleVO.getAuthorName());
        customizedArticleVO.setAuthorMediaPath(articleVO.getAuthorMediaPath());
        customizedArticleVO.setAuthorMediaPath(new CommonUtil().getThumbnail(customizedArticleVO.getAuthorMediaPath(), "_60px"));
        customizedArticleVO.setArticleId(articleVO.getArticleId());
        customizedArticleVO.setArticleHeading(articleVO.getArticleHeading());
        customizedArticleVO.setArticleFullDescription(articleVO.getArticleFullDescription());
        customizedArticleVO.setArticleLdcsCode(articleVO.getArticleLdcsCode());
        customizedArticleVO.setArticleArticleGroupQualId(articleVO.getArticleArticleGroupQualId());
        customizedArticleVO.setArticleUrlSeoTitle(articleVO.getArticleUrlSeoTitle());
        customizedArticleVO.setArticleMetaTitle(articleVO.getArticleMetaTitle());
        customizedArticleVO.setArticleMetaDescription(articleVO.getArticleMetaDescription());
        customizedArticleVO.setArticleMetaKeywords(articleVO.getArticleMetaKeywords());
        //set following details for <meta> usage
        request.setAttribute("currentArticleGroupId", customizedArticleVO.getArticleGroupId());
        request.setAttribute("currentArticleId", customizedArticleVO.getArticleId());
        request.setAttribute("articleAuthorName", customizedArticleVO.getAuthorName());
        request.setAttribute("articleUrlSeoTitle", customizedArticleVO.getArticleUrlSeoTitle());
        request.setAttribute("articleMetaTitle", customizedArticleVO.getArticleMetaTitle());
        request.setAttribute("articleMetaDescription", customizedArticleVO.getArticleMetaDescription());
        request.setAttribute("articleMetaKeywords", customizedArticleVO.getArticleMetaKeywords());
        request.setAttribute("articleStudyLevel", customizedArticleVO.getArticleGroupHeading());
        //
        customizedArticleVO.setArticleMediaId(articleVO.getArticleMediaId());
        customizedArticleVO.setArticleMediaPath(articleVO.getArticleMediaPath());
        customizedArticleVO.setArticleMediaType(articleVO.getArticleMediaType());
        //
        if (customizedArticleVO.getArticleMediaType() != null && customizedArticleVO.getArticleMediaType().equalsIgnoreCase("VIDEO")) {
          customizedArticleVO.setArticleMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(articleVO.getArticleMediaPath(), "4"));
          customizedArticleVO.setArticleMediaThumbPath0001(new GlobalFunction().videoThumbnailFormatChange(articleVO.getArticleMediaPath(), "1"));
        } else if (customizedArticleVO.getArticleMediaType() != null) {
          customizedArticleVO.setArticleMediaThumbPath(new CommonUtil().getThumbnail(customizedArticleVO.getArticleMediaPath(), "_92_60px"));
          customizedArticleVO.setArticleMediaThumbPath0001(customizedArticleVO.getArticleMediaThumbPath());
        }
        //
        customizedArticleVO.setArticleUpdatedDate(articleVO.getArticleUpdatedDate());
        //construct next/previous Article URLs          
        customizedArticleVO.setNextArticleUrlSeoTitle(articleVO.getNextArticleUrlSeoTitle());
        customizedArticleVO.setPreviousArticleUrlSeoTitle(articleVO.getPreviousArticleUrlSeoTitle());
        customizedArticleVO.setNextArticleUrl(new SeoUrls().constructArticleSeoUrl(customizedArticleVO.getNextArticleUrlSeoTitle(),customizedArticleVO.getArticleGroupUrlSeoName()));
        customizedArticleVO.setPreviousArticleUrl(new SeoUrls().constructArticleSeoUrl(customizedArticleVO.getPreviousArticleUrlSeoTitle(),customizedArticleVO.getArticleGroupUrlSeoName()));
        customizedArticleVO.setArticleCreatedDate(articleVO.getArticleCreatedDate());
        customizedArticleVO.setAuthorGoogleAccount(articleVO.getAuthorGoogleAccount());
        request.setAttribute("articleMediaPath", customizedArticleVO.getArticleMediaPath() );
        request.setAttribute("articleHeading", customizedArticleVO.getArticleHeading() );
        customizedArticleVO.setFirstParagraph(articleVO.getFirstParagraph());
        customizedArticleVO.setLastParagraph(articleVO.getLastParagraph());//16-Apr-2014
        customizedArticleVO.setUpdatedFlag(articleVO.getUpdatedFlag());//28-Oct-2014 added by Thiyagu G.
        listOfArticleDetailsInfo.add(customizedArticleVO);
      }
    }
    return listOfArticleDetailsInfo;
  } //customizeArticleDetails 

  public boolean isThisArticleAvailable(String articleUrlSeoTitle) {
    boolean availabilityFlag = false;
    if (articleUrlSeoTitle != null && articleUrlSeoTitle.trim().length() > 0) {
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      String isAvailabilityFlag = "";
      parameters.add(articleUrlSeoTitle);
      try {
        isAvailabilityFlag = dataModel.getString("WUNI_ARTICLES_PKG.IS_ARTICLE_AVILABLE_FN", parameters);
      } catch (Exception exception) {
        exception.printStackTrace();
      }
      if (isAvailabilityFlag != null && isAvailabilityFlag.equalsIgnoreCase("TRUE")) {
        availabilityFlag = true;
      }
    }
    return availabilityFlag;
  } //isThisArticleAvailable

  public ArrayList customizePremiumArticles(ArrayList listOfPremiumArticlesVO) {
    ArrayList listOfPremiumArticles = new ArrayList();
    if (listOfPremiumArticlesVO != null && listOfPremiumArticlesVO.size() > 0) {
      for (int i = 0; i < listOfPremiumArticlesVO.size(); i++) {
        ArticleGroupVO articleGroupVO = (ArticleGroupVO)listOfPremiumArticlesVO.get(i);
        ArticleGroupVO customizedArticleGroupVO = new ArticleGroupVO();
        customizedArticleGroupVO.setAuthorId(articleGroupVO.getAuthorId());
        customizedArticleGroupVO.setAuthorName(articleGroupVO.getAuthorName());
        customizedArticleGroupVO.setAuthorMediaPath(articleGroupVO.getAuthorMediaPath());
        customizedArticleGroupVO.setAuthorMediaPath(new CommonUtil().getThumbnail(customizedArticleGroupVO.getAuthorMediaPath(), "_60px"));
        customizedArticleGroupVO.setAuthorShortBio(articleGroupVO.getAuthorShortBio());
        customizedArticleGroupVO.setArticleId(articleGroupVO.getArticleId());
        customizedArticleGroupVO.setArticleHeading(articleGroupVO.getArticleHeading());
        customizedArticleGroupVO.setArticleShortDescription(articleGroupVO.getArticleShortDescription());
        customizedArticleGroupVO.setArticleUpdatedDate(articleGroupVO.getArticleUpdatedDate());
        customizedArticleGroupVO.setArticleUrlSeoTitle(articleGroupVO.getArticleUrlSeoTitle());
        customizedArticleGroupVO.setArticleGroupName(articleGroupVO.getArticleGroupName());
        customizedArticleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(customizedArticleGroupVO.getArticleUrlSeoTitle(),customizedArticleGroupVO.getArticleGroupName()));
        customizedArticleGroupVO.setArticleMediaId(articleGroupVO.getArticleMediaId());
        customizedArticleGroupVO.setArticleMediaType(articleGroupVO.getArticleMediaType());
        customizedArticleGroupVO.setArticleMediaPath(articleGroupVO.getArticleMediaPath());
        if (customizedArticleGroupVO.getArticleMediaType() != null && customizedArticleGroupVO.getArticleMediaType().equalsIgnoreCase("VIDEO")) {
          customizedArticleGroupVO.setArticleMediaPath(new CommonUtil().getThumbnail(customizedArticleGroupVO.getArticleMediaPath(), "_tmb/0001"));
        } else if (customizedArticleGroupVO.getArticleMediaType() != null) {
          customizedArticleGroupVO.setArticleMediaPath(new CommonUtil().getThumbnail(customizedArticleGroupVO.getArticleMediaPath(), "_92_60px"));
        }
        listOfPremiumArticles.add(customizedArticleGroupVO);
      }
    }
    return listOfPremiumArticles;
  } //customizePremiumArticles

  public ArrayList customizeLatestArticles(ArrayList listOfLatestArticlesVO) {
    ArrayList listOfLatestArticles = new ArrayList();
    if (listOfLatestArticlesVO != null && listOfLatestArticlesVO.size() > 0) {
      for (int i = 0; i < listOfLatestArticlesVO.size(); i++) {
        ArticleVO articleVO = (ArticleVO)listOfLatestArticlesVO.get(i);
        ArticleVO customizedArticleVO = new ArticleVO();
        customizedArticleVO.setArticleGroupId(articleVO.getArticleGroupId());
        customizedArticleVO.setArticleGroupUrlSeoName(articleVO.getArticleGroupUrlSeoName());
        customizedArticleVO.setArticleId(articleVO.getArticleId());
        customizedArticleVO.setArticleHeading(articleVO.getArticleHeading());
        customizedArticleVO.setArticleUrlSeoTitle(articleVO.getArticleUrlSeoTitle());
        customizedArticleVO.setArticleUpdatedDate(articleVO.getArticleUpdatedDate());
        customizedArticleVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(customizedArticleVO.getArticleUrlSeoTitle(),customizedArticleVO.getArticleGroupUrlSeoName()));
        customizedArticleVO.setAuthorName(articleVO.getAuthorName());
        listOfLatestArticles.add(customizedArticleVO);
      }
    }
    return listOfLatestArticles;
  } //customizeLatestArticles

}
