package spring.pojo.searchresult;

import lombok.Data;

@Data
public class GetFooterSectionBean {

	private String type = null;
	private String collegeId = null;
}
