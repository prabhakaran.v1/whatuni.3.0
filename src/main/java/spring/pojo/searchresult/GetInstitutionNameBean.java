package spring.pojo.searchresult;

import lombok.Data;

@Data
public class GetInstitutionNameBean {
	
  private String keywordText = null;
  private String pageName = null;
  private String collegeName = null;
  private String collegeNameAlias = null;
  private String collegeNameDisplay = null;
  private String collegeLocation = null;
  private String collegeId = null;
  private String odMsg = null;
  private String reviewMsg = null;
}
