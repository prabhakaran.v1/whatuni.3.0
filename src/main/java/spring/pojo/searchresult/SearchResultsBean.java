package spring.pojo.searchresult;

import lombok.Data;

@Data
public class SearchResultsBean {
  private String accessToken;
  private String affiliateId;
  private String userId;
  private String websiteAppCode;
  private String x;
  private String y;
  private String trackSessionId;
  private String userIpAddress;
  private String userAgent;
  private String pageNo;
  private String basketId;
  private String dynamicRandomNumber;
  private String phraseSearch;
  private String searchCategory;
  private String qualification;
  private String studyMode;
  private String townCity;
  private String searchHow;
  private String univLocTypeName;
  private String campusBasedUnivName;
  private String russellGroupName;
  private String accomodationFlag;
  private String scholarshipFlag;
  private String hybridStudyModeFlag;
  private String searchUrl;
  private String userScore;
  private String subjGradesSessionId;
  private String acceptanceCriteria;
  private String keywordText;
  private String collegeName; 
  private String searchWhat = null; 
  private String searchCol = null; 
  private String entityText = null; 
  private String ucasCode = null; 
  private String pageName = null;  
  private String entryLevel = null; 
  private String entryPoints = null;
  private String empRate = null;
  private String campusTypeName = null;
  private String russelGroupName = null;  
  private String ucasTariff = null;
  private String jacsCode = null;
  private String requestURL = null; 
  private String userTrackSessionId = null;
  private String assessmentType = null;
  private String reviewSubjects = null;
  private String subjectSessionId = null;
  private String randomNumber = null;
  private String clientIp = null;
  private String recentFilterApplied = null;
  private String studyLevelId = null;
  private String regionId = null;
  private String courseSearchText = null;

}
