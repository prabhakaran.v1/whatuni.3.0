package spring.pojo.clearing.provider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CollegeDetailsResponse {
  private String collegeId;
  private String collegeName;
  private String collegeDisplayName;
  private String overAllRating;
  private String collegeLogo;
  private String reviewCount;
  private String suborderItemId;
  private String orderItemId;
  private String website;
  private String hotLineNo;
  private String myhcProfileId;
  private String networkId;
  private String accommodationAvailable;
  private String scholarshipAvailable;
  private String collegeTextKey;
  private String uniReviewUrl;
  private String uniHomeUrl;
  private String actualRating;

}
