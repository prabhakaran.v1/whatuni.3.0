package spring.pojo.clearing.provider;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import spring.pojo.clearing.common.AcceptanceResponse;
import spring.pojo.clearing.common.QualificationResponse;
import spring.pojo.clearing.common.StatsResponse;
import spring.pojo.clearing.common.StudyModeResponse;

@Data
public class ProviderResultsResponse {

  @JsonProperty("providerResultsList")
  private ArrayList<ProviderResultsContentResponse> providerResultsList;
  @JsonProperty("studyModeList")
  private ArrayList<StudyModeResponse> studyModeList;
  @JsonProperty("qualificationList")
  private ArrayList<QualificationResponse> qualificationList;
  @JsonProperty("collegeDetails")
  private ArrayList<CollegeDetailsResponse> collegeDetails;
  @JsonProperty("statsLogList")
  private ArrayList<StatsResponse> statsLogList;
  private ArrayList<AcceptanceResponse> acceptanceCriteriaList = null;
  @JsonProperty("pixelTrackingCode")
  private String pixelTrackingCode;
  @JsonProperty("advertiserFlag")
  private String advertiserFlag;
  @JsonProperty("invalidCategoryFlag")
  private String invalidCategoryFlag;
  @JsonProperty("studyModeId")
  private String studyModeId;
  @JsonProperty("showSwitchFlag")
  private String showSwitchFlag;
  @JsonProperty("browseCategoryDesc")
  private String browseCategoryDesc;
  @JsonProperty("searchPhrase")
  private String searchPhrase;
}
