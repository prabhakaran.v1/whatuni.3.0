package spring.pojo.clearing.provider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ProviderResultsContentResponse {

  private String courseId;
  private String opportunityId;
  private String courseTitle;
  private String studyLevel;
  private String collegeId;
  private String suborderItemId;
  private String entryReqPoints;
  private String scoreLabel;
  private String scoreLabelTooltip;
  private String scoreLevel;
  private String ucasCode;
  private String courseCount;
  private String startDate;
  private String tagLine;
  private String courseTextKey;
  private String courseDetailPageUrl;
  private String scoreTooltip;
  private String studyLevelText;
  private String matchTypeDim;  
}
