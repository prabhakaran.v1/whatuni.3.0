package spring.pojo.clearing.provider;

import lombok.Data;

@Data
public class ProviderResultsRequest {
  private String affiliateId;
  private String accessToken;
  private String userId; 
  private String websiteAppCode; 
  private String collegeName; 
  private String searchWhat; 
  private String searchHow; 
  private String searchCategory;
  private String phraseSearch; 
  private String qualification; 
  private String studyMode; 
  private String townCity; 
  private String hybridStudyModeFlag;
  private String userScore;
  private String acceptanceCriteria;
  private String userAgent; 
  private String pageNo; 
  private String pageName; 
  private String networkId;  
  private String searchUrl;
  private String userTrackSessionId;  
  private String x;
  private String y;
  private String basketId;
}
