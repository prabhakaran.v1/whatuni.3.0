package spring.pojo.clearing.provider;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SubjectAjaxResponse {
  @JsonProperty("providerResultsSubjectAjaxList")
  private ArrayList<SubjectAjaxListResponse> subjectAjaxList = null;
}
