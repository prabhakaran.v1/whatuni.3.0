package spring.pojo.clearing.provider;

import lombok.Data;

@Data
public class SubjectAjaxListResponse {
	
  private String subjectTextKey = null;
  private String categoryCode = null;
  private String subject = null;
}
