package spring.pojo.clearing.search;

import lombok.Data;

@Data
public class SearchResultsRequest {
  private String accessToken;
  private String affiliateId;
  private String userId;
  private String websiteAppCode;
  private String x;
  private String y;
  private String trackSessionId;
  private String userIpAddress;
  private String userAgent;
  private String pageNo;
  private String basketId;
  private String dynamicRandomNumber;
  private String phraseSearch;
  private String searchCategory;
  private String qualification;
  private String studyMode;
  private String townCity;
  private String searchHow;
  private String univLocTypeName;
  private String campusBasedUnivName;
  private String russellGroupName;
  private String accomodationFlag;
  private String scholarshipFlag;
  private String hybridStudyModeFlag;
  private String searchUrl;
  private String userScore;
  private String subjGradesSessionId;
  private String acceptanceCriteria;
  private String keywordText;

}
