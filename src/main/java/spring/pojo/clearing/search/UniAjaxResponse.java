package spring.pojo.clearing.search;

import java.util.List;

import lombok.Data;

@Data
public class UniAjaxResponse {
  private List<UniAjaxListResponse> searchResultUniAjaxList;
}
