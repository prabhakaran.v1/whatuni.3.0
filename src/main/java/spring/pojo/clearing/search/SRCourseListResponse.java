package spring.pojo.clearing.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SRCourseListResponse {

  private String courseId;
  private String courseTitle;
  private String ucasCode;
  private String entryPoints;
  private String scoreLabel;
  private String scoreTooltip;
  private String scoreLevel;
  private String studyLevelDesc;
  private String seoStudyLevelText;
  private String courseInBasket;
  private String orderItemId;
  private String subOrderItemId;
  private String myhcProfileId;
  private String networkId;
  private String website;
  private String hotLineNo;
  private String placesAvailable;
  private String lastUpdatedDate;
  private String courseRank;
  private String startDate;
  private String tagLine;
  private String courseDetailPageUrl;
  private String courseTitleText;
  private String matchTypeDim;
}
