package spring.pojo.clearing.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import spring.pojo.clearing.common.AcceptanceResponse;
import spring.pojo.clearing.common.FeaturedBrandResponse;
import spring.pojo.clearing.common.LocationTypeResponse;
import spring.pojo.clearing.common.QualificationResponse;
import spring.pojo.clearing.common.StatsResponse;
import spring.pojo.clearing.common.StudyModeResponse;

@Data
public class SearchResultsResponse {

  @JsonProperty("subjGradesSessionId")
  private String subjGradesSessionId;
  @JsonProperty("resultExists")
  private String resultExists;
  private String searchPhrase;
  private String browseCatDesc;
  @JsonProperty("recordCount")
  private String recordCount;
  @JsonProperty("totalCourseCount")
  private String totalCourseCount; 
  @JsonProperty("invalidCategoryFlag")
  private String invalidCategoryFlag;
  @JsonProperty("showSwitchFlag")
  private String showSwitchFlag;
  @JsonProperty("searchResultsList")
  private List<SearchResultsContentResponse> searchResultsList;
  @JsonProperty("studyModeList")
  private List<StudyModeResponse> studyModeList;
  @JsonProperty("qualificationList")
  private List<QualificationResponse> qualificationList;
  @JsonProperty("regionList")
  private List<RegionResponse> regionList;
  @JsonProperty("uniLocationTypeList")
  private List<LocationTypeResponse> locationTypeList;
  @JsonProperty("acceptanceCriteriaList")
  private List<AcceptanceResponse> acceptanceList;
  @JsonProperty("statsLogList")
  private List<StatsResponse> statsLogList;
  @JsonProperty("searchCollegeIds")
  private List<SearchCollegeIdsResponse> collegeIdsForThisSearch;
  @JsonProperty("searchInstitutionIds")
  private List<SearchInstitutionIdsResponse> institutionIdsForThisSearch;
  @JsonProperty("featuredBrandList")
  private List<FeaturedBrandResponse> featuredBrandList;
}
