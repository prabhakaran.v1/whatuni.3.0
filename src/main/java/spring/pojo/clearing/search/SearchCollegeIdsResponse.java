package spring.pojo.clearing.search;

import lombok.Data;

@Data
public class SearchCollegeIdsResponse {

  private String collegeIds;
}
