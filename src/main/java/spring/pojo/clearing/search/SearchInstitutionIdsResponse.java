package spring.pojo.clearing.search;

import lombok.Data;

@Data
public class SearchInstitutionIdsResponse {

  private String institutionIds;
}
