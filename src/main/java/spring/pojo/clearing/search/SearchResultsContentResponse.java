package spring.pojo.clearing.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SearchResultsContentResponse {

  private String collegeId;
  private String collegeName;
  private String collegeTextKey;
  private String collegePRUrl;
  private String collegeDisplayName;
  private String rating;
  private String exactRating;
  private String hits;
  private String collegeInBasket;
  private String collegeLogoPath;
  private String accommodationAvailable;
  private String scholarshipAvailable;
  @JsonProperty("bestMatchCoursesList")
  private List<SRCourseListResponse> courseDetailsList;
  private String reviewCount;
  private String currentRankInCategory;
  private String totalRankInCategory;
  private String providerId;
  private String stdAdvertType;
  private String cugUniRank;
  private String uniProfileUrl;
  private String reviewUrl;
  private String gaCollegeName;
  private String matchTypeDim;  
}
