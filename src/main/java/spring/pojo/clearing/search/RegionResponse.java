package spring.pojo.clearing.search;

import lombok.Data;

@Data
public class RegionResponse {

  private String regionName;
  private String regionTextKey;
  private String selectedFlag;
}
