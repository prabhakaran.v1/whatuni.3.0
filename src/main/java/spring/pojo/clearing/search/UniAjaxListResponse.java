package spring.pojo.clearing.search;

import lombok.Data;

@Data
public class UniAjaxListResponse {
  private String description = null;
  private String url = null;
  private String matchedText = null;
  private String orderSeq = null;
  private String sdMsg = null;
  private String collegeId = null;
  private String providerTextKey;
}
