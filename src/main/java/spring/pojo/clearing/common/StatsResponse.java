package spring.pojo.clearing.common;

import lombok.Data;

@Data
public class StatsResponse {
 
  private String browseCatFlag;
  private String browseCatId;
  private String browseCatDesc;
  private String studyModeDesc;
  private String browseCatCode;
  private String collegeId;
  private String collegeName;
  private String collegeDisplayName;
  private String location;
}
