package spring.pojo.clearing.common;

import lombok.Data;

@Data
public class LocationTypeResponse {
  
  private String locTypeId = null;
  private String locTypeDesc = null;
  private String locTypeTextKey = null;
  private String optionType = null;
  private String selectedFlag;
}
