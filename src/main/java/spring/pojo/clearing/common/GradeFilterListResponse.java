package spring.pojo.clearing.common;

import lombok.Data;

@Data
public class GradeFilterListResponse {

   private String qualId;
   private String qualification;
   private String qualificationUrl;
   private String parentQualification;
   private String gradeOptions;
   private String maxPoint;
   private String maxTotalPoint;
   private String gradeOptionflag;
}
