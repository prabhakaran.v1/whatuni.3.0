package spring.pojo.clearing.common;

import java.util.List;
import lombok.Data;

@Data
public class GradeFilterResponse {

   private List<GradeFilterListResponse> gradeFilterList;
   private String userStudyLevelEntry;
   private String userEntryPoint;
   private String ucasPoint;
   private String retVal;
   private String userTrackSessionId;
}
