package spring.pojo.clearing.common;

import lombok.Data;

@Data
public class FeaturedBrandResponse {

  private String collegeId;
  private String collegeName;
  private String mediaId;
  private String mediaPath;
  private String thumbnailPath;
  private String mediaType;
  private String headline;
  private String profileHeadlineUrl;
  private String tagline;
  private String reviewDisplayflag;
  private String reviewCount;
  private String overallRating;
  private String overallRatingExact;
  private String navigationText;
  private String navigationUrl;
  private String networkId;
  private String cugUniRank;
  private String buttonLabel;
  private String buttonUrl;
  private String hotlineLabel;
  private String hotlineNumber;
  private String orderItemId;
  private String collegeLogoPath;

}
