package spring.pojo.clearing.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class StudyModeResponse {

  @JsonProperty("studyModeDesc")
  private String displayStudyModeDesc;
  @JsonProperty("studyModeCode")
  private String studyModeId;
  @JsonProperty("studyModeTextKey")
  private String studyModeTextKey;
  private String selectedFlag;
}
