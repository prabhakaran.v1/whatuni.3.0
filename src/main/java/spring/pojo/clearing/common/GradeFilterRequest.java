package spring.pojo.clearing.common;


import lombok.Data;

@Data
public class GradeFilterRequest {

   private String accessToken;
   private String affiliateId;
   private String qualId;
   private String userId;
   private String userTrackSessionId;
   private String userStudyLevelEntry;
   private String userEntryPoint;
   private String ucasPoint;
}
