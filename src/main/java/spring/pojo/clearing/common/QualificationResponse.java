package spring.pojo.clearing.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualificationResponse {

  private String qualDisplayDesc;
  private String qualCode;
  private String qualTextKey;
  private String categoryId;
  private String categoryDesc;
  private String browseCatTextKey;
  private String categoryCode;
  private String selectedFlag;

}
