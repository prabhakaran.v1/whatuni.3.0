package spring.pojo.clearing.common;

import lombok.Data;

@Data
public class AcceptanceResponse {
  
  private String dispAcceptanceDec;
  private String acceptanceText;
  private String acceptanceValue;
  private String selectedFlag;
}
