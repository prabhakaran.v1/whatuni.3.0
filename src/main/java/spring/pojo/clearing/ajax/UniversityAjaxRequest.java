package spring.pojo.clearing.ajax;

import lombok.Data;

@Data
public class UniversityAjaxRequest {

  private String keywordText;
  private String affiliateId;
  private String accessToken;
}
