package spring.pojo.clearing.ajax;

import lombok.Data;

@Data
public class SubjectAjaxRequest {

  private String affiliateId;
  private String accessToken;
  private String websiteAppCode;
  private String keywordText;
  private String qualification;
  private String collegeId;
}
