package spring.pojo.user;

import lombok.Data;

@Data
public class UnSubcribeForm {
  private String affiliateId;
  private String userId;
  private String encryptUserId;
  private String newsLetters = "N";
  private String solusEmail = "N";
  private String remainderMail = "N";
  private String survey = "N";
  private String reviewSurveyFlag = null;
}
