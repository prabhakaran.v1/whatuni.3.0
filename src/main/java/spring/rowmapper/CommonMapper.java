package spring.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.homepage.form.HomePageBean;
import WUI.members.form.MembersSearchBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.SearchBean;

import com.wuni.util.seo.SeoUrls;

public class CommonMapper {
  public CommonMapper() {
  }
  public static class latestReviewMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageBean bean = new HomePageBean();
      bean.setReviewId(rs.getString("review_id"));
      bean.setReviewTitle(rs.getString("review_title"));
      bean.setUserId(rs.getString("user_id"));
      bean.setUserName(rs.getString("user_name") != null? rs.getString("user_name").trim(): rs.getString("user_name"));
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setUserGraduate(rs.getString("at_uni") != null? rs.getString("at_uni").trim(): rs.getString("at_uni"));
      bean.setNationality(rs.getString("nationality") != null? rs.getString("nationality").trim(): rs.getString("nationality"));
      bean.setUserGender(rs.getString("gender") != null? rs.getString("gender").toLowerCase(): rs.getString("gender"));
      bean.setOverAllRating(rs.getString("overall_rating"));
      bean.setCourseTitle(rs.getString("course_title"));
      bean.setNewUniReviewURL(new SeoUrls().constructReviewPageSeoUrl(bean.getCollegeName(), bean.getCollegeId()));
      return bean;
    }
  }
  public static class collegeRelatedMembersMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MembersSearchBean bean = new MembersSearchBean();
      //bean.setCollegeId(rs.getString("college_id"));
      //bean.setCollegeName(rs.getString("college_name"));
      bean.setUserName(rs.getString("username") != null? rs.getString("username"): "");
      //bean.setForeName(rs.getString("forename"));
      bean.setGender(rs.getString("gender"));
      bean.setUserId(rs.getString("user_id"));
      bean.setNationality(rs.getString("nationality"));
      bean.setYearOfGraduation(rs.getString("year_of_graduation"));
      bean.setIama(rs.getString("graduation"));
      return bean;
    }
  }
  /* Loads the uni information that has to display in most review uni pod */
  public static class mostReviewedUniMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageBean bean = new HomePageBean();
      bean.setCollegeId(rs.getString("COLLEGE_ID"));
      bean.setCollegeName(rs.getString("COLLEGE_NAME"));
      return bean;
    }
  }
  /* Loads the randomize uni information that has to display in order prospectus pod  */
  public static class orderProspectusUniMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageBean bean = new HomePageBean();
      bean.setCollegeId(rs.getString("COLLEGE_ID"));
      bean.setCollegeName(rs.getString("COLLEGE_NAME"));
      return bean;
    }
  }
  /* Store the information that has to display in the subject dropdown list */
  public static class subjectListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SearchBean bean = new SearchBean();
      bean.setOptionId(rs.getString("category_code"));
      bean.setOptionText(rs.getString("subject_name"));
      bean.setOptionSeqId(rs.getString("subject_id"));
      return bean;
    }
  }
  /* Loads the latest registered user nformation that has to display in latest members pod */
  public static class latestMembersMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MembersSearchBean bean = new MembersSearchBean();
      bean.setUserName(rs.getString("username"));
      bean.setGender(rs.getString("gender"));
      bean.setUserId(rs.getString("user_id"));
      bean.setNationality(rs.getString("nationality"));
      bean.setHomeTown(rs.getString("hometown"));
      bean.setYearOfGraduation(rs.getString("year_of_graduation"));
      bean.setIama(rs.getString("i_am_a"));
      return bean;
    }
  }
  public static class collegeLatestReviewsMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean bean = new ReviewListBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setReviewId(rs.getString("review_id"));
      bean.setUserId(rs.getString("user_id"));
      bean.setReviewTitle(rs.getString("review_title"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setCollegeNameDisplay(rs.getString("college_name_display"));
      bean.setReviewerName(rs.getString("user_name"));
      bean.setReviewRatings(rs.getString("overall_rating"));
      bean.setUserGraduate(rs.getString("at_uni"));
      bean.setUserGender(rs.getString("gender"));
      bean.setCourseName(rs.getString("course_title"));
      bean.setCourseTitle(rs.getString("course_title"));
      bean.setReviewDesc(rs.getString("review_text"));
      bean.setCourseId(rs.getString("course_id"));
      bean.setCourseDeletedFlag(rs.getString("course_deleted_flag"));
      bean.setCollegeLandingUrl(new SeoUrls().construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
      bean.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(bean.getCollegeName(), bean.getCollegeId()));
      if((!GenericValidator.isBlankOrNull(bean.getCourseId()) || !"0".equals(bean.getCourseId()))){
        bean.setCourseDetailsReviewURL(new SeoUrls().courseDetailsPageURL(bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId()));
      }
      bean.setSeoStudyLevelText(rs.getString("seo_study_level_text"));
      bean.setUserNameInitial(rs.getString("USER_NAME_INT"));
      bean.setReviewDate(rs.getString("REVIEW_DATE"));
      return bean;
    }
  }
}
