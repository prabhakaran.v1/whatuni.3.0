package spring.rowmapper.advice;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.advice.AdviceDetailInfoVO;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;

public class MostPopularAdviceRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    AdviceDetailInfoVO adviceDetailInfoVO = new AdviceDetailInfoVO();
    adviceDetailInfoVO.setPostId(rs.getString("post_id"));
    adviceDetailInfoVO.setPostTitle(rs.getString("post_title"));
    adviceDetailInfoVO.setPostUrl(rs.getString("post_url"));
    adviceDetailInfoVO.setMediaPath(rs.getString("media_path"));
    if (!GenericValidator.isBlankOrNull(adviceDetailInfoVO.getMediaPath())) {
      String splitMediaPath = adviceDetailInfoVO.getMediaPath().substring(0, adviceDetailInfoVO.getMediaPath().lastIndexOf("."));
      adviceDetailInfoVO.setImageName(new CommonUtil().getImgPath((splitMediaPath + "_140_92px.jpg"), rowNum));
    }
    adviceDetailInfoVO.setMediaAltText(rs.getString("media_alt_text"));
    adviceDetailInfoVO.setMediaUrl(rs.getString("media_url"));
    adviceDetailInfoVO.setUpdatedDate(rs.getString("updated_date"));
    adviceDetailInfoVO.setArticleViewedCount(rs.getString("article_viewed_count"));    
    adviceDetailInfoVO.setParentCategoryName(rs.getString("parent_category_name"));
    adviceDetailInfoVO.setSubCategoryName(rs.getString("sub_category_name"));
    adviceDetailInfoVO.setPrimaryCategoryName(rs.getString("primary_category_name"));
    adviceDetailInfoVO.setPostShortText(rs.getString("personalized_text"));    
    adviceDetailInfoVO.setAdviceDetailURL(new SeoUrls().constructAdviceSeoUrl(adviceDetailInfoVO.getPrimaryCategoryName(), adviceDetailInfoVO.getPostUrl(), adviceDetailInfoVO.getPostId()).toLowerCase());
    return adviceDetailInfoVO;
  }

}  