package spring.rowmapper.blogs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.blog.BlogVO;

public class PremiumPostListRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    BlogVO blogVO = new BlogVO();
    blogVO.setBlogId(rs.getString("BLOG_ID"));
    blogVO.setBlogName(rs.getString("BLOG_URL_SEO_NAME"));
    blogVO.setBlogUrlSeoName(rs.getString("BLOG_URL_SEO_NAME"));
    blogVO.setAuthorName(rs.getString("author_id"));
    blogVO.setAuthorName(rs.getString("author_name"));
    blogVO.setAuthorMediaPath(rs.getString("author_media_path"));
    blogVO.setAuthorShortBio(rs.getString("author_short_bio"));
    blogVO.setPostId(rs.getString("post_id"));
    blogVO.setPostHeading(rs.getString("post_heading"));
    blogVO.setPostUrlSeoTitle(rs.getString("post_url_seo_title"));
    blogVO.setPostShortDescription(rs.getString("post_short_desc"));
    blogVO.setPostUpdatedDate(rs.getString("post_updated_date"));
    blogVO.setTotalPostForThisBlog(rs.getString("blog_post_count"));    
    return blogVO;
  }

}
