package spring.rowmapper.blogs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.blog.PostVO;

public class PostDetailsRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    PostVO postVO = new PostVO();
    postVO.setBlogId(rs.getString("blog_id"));
    postVO.setAuthorName(rs.getString("author_id"));
    postVO.setAuthorName(rs.getString("author_name"));
    postVO.setAuthorMediaPath(rs.getString("author_media_path"));
    postVO.setPostId(rs.getString("post_id"));
    postVO.setPostHeading(rs.getString("post_heading"));
    postVO.setPostFullDescription(rs.getString("post_description"));
    postVO.setPostLdcsCode(rs.getString("post_ldcs1"));
    postVO.setPostBlogQualId(rs.getString("post_blog_qual_id"));
    postVO.setPostUrlSeoTitle(rs.getString("post_url_seo_title"));
    postVO.setPostMetaTitle(rs.getString("post_meta_title"));
    postVO.setPostMetaDescription(rs.getString("post_meta_description"));
    postVO.setPostMetaKeywords(rs.getString("post_meta_keywords"));
    postVO.setPostMediaId(rs.getString("post_media_id"));
    postVO.setPostMediaPath(rs.getString("post_media_path"));
    postVO.setPostMediaType(rs.getString("post_media_type"));
    postVO.setPostUpdatedDate(rs.getString("post_updated_date"));
    postVO.setNextPostUrlSeoTitle(rs.getString("next_post_url_seo_title"));
    postVO.setPreviousPostUrlSeoTitle(rs.getString("previous_post_url_seo_title"));
    return postVO;
  }

}
