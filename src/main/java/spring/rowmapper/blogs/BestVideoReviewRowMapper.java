package spring.rowmapper.blogs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.form.VideoReviewListBean;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

public class BestVideoReviewRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
    VideoReviewListBean bean = new VideoReviewListBean();
    bean.setCollegeId(rs.getString("college_id"));
    bean.setCollegeName(rs.getString("college_name"));
    bean.setCollegeNameDisplay(rs.getString("college_name_display"));
    bean.setCollegeProfileFlag(rs.getString("college_profile"));
    bean.setVideoReviewId(rs.getString("review_id"));
    bean.setVideoReviewTitle(rs.getString("review_title"));
    bean.setMyhcProfileId(rs.getString("myhc_profile_id"));
    bean.setProfileType(rs.getString("myhc_profile_type"));
    bean.setVideoType(rs.getString("video_type"));
    String videoType = rs.getString("video_type");
    String videoThumbNumber = rs.getString("video_thumb_nos"); //adeed for 28th July 2009 Release
    if (videoType != null && videoType.equalsIgnoreCase("V")) {
      bean.setThumbnailUrl(rs.getString("thumbnail_assoc_text"));
    } else {
      bean.setVideoUrl(limeLightPath + rs.getString("video_assoc_text"));
      if (videoThumbNumber != null) {
        bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + rs.getString("video_assoc_text"), videoThumbNumber));
      }
    }
    return bean;
  }

}
