package spring.rowmapper.blogs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.blog.PostVO;

public class LatestPostListRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    PostVO postVO = new PostVO();
    postVO.setBlogId(rs.getString("blog_id"));
    postVO.setBlogUrlSeoName(rs.getString("blog_url_seo_name"));
    postVO.setPostId(rs.getString("post_id"));
    postVO.setPostHeading(rs.getString("post_heading"));
    postVO.setPostUrlSeoTitle(rs.getString("post_url_seo_title"));
    postVO.setPostUpdatedDate(rs.getString("post_updated_date"));    
    return postVO;
  }

}

