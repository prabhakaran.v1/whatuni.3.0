package spring.rowmapper.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.review.bean.UniBrowseBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

/**
    * This class is used to load the open days browse list.
    */
public class OpenDaysBrowseListRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    UniBrowseBean bean = new UniBrowseBean();
    bean.setCollegeId(rs.getString("college_id"));
    bean.setCollegeName(rs.getString("college_name"));
    bean.setCollegeNameDisplay(rs.getString("college_name_display"));
    bean.setCollegeLocation(rs.getString("college_location"));
    bean.setSeoURLString(GlobalConstants.SEO_PATH_OPEN_DAY_PAGE + (rs.getString("college_location") != null && rs.getString("college_location").trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("college_location").toLowerCase())) + "-" : "") + "open-days-" + (new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("college_name")))).toLowerCase() + "/" + rs.getString("college_id") + "/page.html");
    String tmpUrl = "";
    ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
    try {
      if (enquiryDetailsRS != null) {
        while (enquiryDetailsRS.next()) {
          bean.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          bean.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
          bean.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          bean.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
          bean.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
          bean.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
          tmpUrl = enquiryDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderWebsite(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderEmailWebform(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderProspectusWebform(tmpUrl);
          bean.setMediaId("-999");
          bean.setMediaType("picture");
          bean.setMediaPath("/img/whatuni/unihome_default.png");
          if (enquiryDetailsRS.getString("MEDIA_ID") != null && enquiryDetailsRS.getString("MEDIA_TYPE") != null && enquiryDetailsRS.getString("MEDIA_PATH") != null) {
            bean.setMediaId(enquiryDetailsRS.getString("MEDIA_ID"));
            bean.setMediaPath(enquiryDetailsRS.getString("MEDIA_PATH"));
            bean.setMediaThumbPath(enquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
            bean.setMediaType(enquiryDetailsRS.getString("MEDIA_TYPE").toLowerCase());
            if (bean.getMediaType().equals("video")) {
              bean.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(bean.getMediaPath(), "2"));
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return bean;
  }

}
