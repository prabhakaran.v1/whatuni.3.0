package spring.rowmapper.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.opendays.OpenDaysCalendarVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

public class OpenDayCalendarRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    OpenDaysCalendarVO openDaysCalendarVO = new OpenDaysCalendarVO();
    openDaysCalendarVO.setCollegeId(rs.getString("college_id"));
    openDaysCalendarVO.setCollegeName(rs.getString("college_name"));
    openDaysCalendarVO.setCollegeNameDisplay(rs.getString("college_name_display"));
    openDaysCalendarVO.setCollegeLocation(rs.getString("college_location"));
    openDaysCalendarVO.setCollegeDepartmentDescription(rs.getString("open_day_type"));
    openDaysCalendarVO.setCollegeOpendayExternalUrl(rs.getString("openday_external_url"));
    openDaysCalendarVO.setOpenDate(rs.getString("open_date"));
    //Added By Sekhar for wu_320
    openDaysCalendarVO.setOpenDay_day(rs.getString("open_day"));
    openDaysCalendarVO.setOpenDay_date(rs.getString("open_day_date"));
    openDaysCalendarVO.setOpenDay_month_year(rs.getString("open_month"));
    openDaysCalendarVO.setTotalRecordsCount(rs.getString("total_cnt"));
    openDaysCalendarVO.setPastFutureDate(rs.getString("past_future_date"));
    openDaysCalendarVO.setDateExistFlg(rs.getString("date_exist"));
    openDaysCalendarVO.setEventId(rs.getString("event_id"));
      String tmpUrl = "";
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            openDaysCalendarVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            openDaysCalendarVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            openDaysCalendarVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            openDaysCalendarVO.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
            openDaysCalendarVO.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
            openDaysCalendarVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
            tmpUrl = enquiryDetailsRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            openDaysCalendarVO.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            openDaysCalendarVO.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            openDaysCalendarVO.setSubOrderProspectusWebform(tmpUrl);
            openDaysCalendarVO.setMediaId("-999");
            openDaysCalendarVO.setMediaType("picture");
            openDaysCalendarVO.setMediaPath("/img/whatuni/unihome_default.png");
            if (enquiryDetailsRS.getString("MEDIA_ID") != null && enquiryDetailsRS.getString("MEDIA_TYPE") != null && enquiryDetailsRS.getString("MEDIA_PATH") != null) {
              openDaysCalendarVO.setMediaId(enquiryDetailsRS.getString("MEDIA_ID"));
              openDaysCalendarVO.setMediaPath(enquiryDetailsRS.getString("MEDIA_PATH"));
              openDaysCalendarVO.setMediaThumbPath(enquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
              openDaysCalendarVO.setMediaType(enquiryDetailsRS.getString("MEDIA_TYPE").toLowerCase());
              if (openDaysCalendarVO.getMediaType().equals("video")) {
                openDaysCalendarVO.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(openDaysCalendarVO.getMediaPath(), "2"));
              }
            }
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
    
    
    return openDaysCalendarVO;
  }

}
