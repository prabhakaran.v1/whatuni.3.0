package spring.rowmapper.browsebylocation;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.browsebylocation.BrowseUgCoursesBySubjectVO;

import com.wuni.util.seo.SeoUrls;

public class BrowseUgCoursesBySubjectRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    BrowseUgCoursesBySubjectVO browseUgCoursesBySubjectVO = new BrowseUgCoursesBySubjectVO();
    String browseMoneyPageUrlPattern = "/degrees/courses/degree-courses/SUBJECT_NAME-degree-courses-LOCATION_1/m/LOCATION_2/r/SUBJECT_ID/page.html";
    String countryUrlToSubjectListingPage = "";
    //
    browseUgCoursesBySubjectVO.setSubjectId(rs.getString("SUBJECT_ID"));
    browseUgCoursesBySubjectVO.setSubjectDesc(rs.getString("SUBJECT_DESC"));
    browseUgCoursesBySubjectVO.setSubjectCode(rs.getString("SUBJECT_CODE"));
    browseUgCoursesBySubjectVO.setLocation(rs.getString("LOCATION"));
    browseUgCoursesBySubjectVO.setTotalSubjectCountForThisLocation(rs.getString("TOTAL_SUBJECT_COUNT"));
    //
    countryUrlToSubjectListingPage = new SeoUrls().construnctUrlToBrowseMoneyPage(browseMoneyPageUrlPattern, browseUgCoursesBySubjectVO.getLocation(), browseUgCoursesBySubjectVO.getSubjectId(), browseUgCoursesBySubjectVO.getSubjectDesc());
    browseUgCoursesBySubjectVO.setLinkToBrowseMoneyPage(countryUrlToSubjectListingPage);
    //
    return browseUgCoursesBySubjectVO;
  }

}
