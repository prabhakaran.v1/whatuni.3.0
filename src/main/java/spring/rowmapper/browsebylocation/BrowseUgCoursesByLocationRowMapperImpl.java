package spring.rowmapper.browsebylocation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.browsebylocation.BrowseUgCoursesByLocationChildVO;
import spring.valueobject.browsebylocation.BrowseUgCoursesByLocationVO;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;

public class BrowseUgCoursesByLocationRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    BrowseUgCoursesByLocationVO browseUgCoursesByLocationVO = new BrowseUgCoursesByLocationVO();
    SeoUrls seoUrls = new SeoUrls();
    String countryUrlToSubjectListingPage = "";
    String childLocationUrlToSubjectListingPage = "";
    CommonUtil comUtil = new CommonUtil();
    //    String tempLocation = "";
    //    
    browseUgCoursesByLocationVO.setCountryName(rs.getString("COUNTRY_NAME"));
    //child details
    ArrayList listOfChildLocations = new ArrayList();
    ResultSet childLocationsRS = (ResultSet)rs.getObject("CHILD_LOCATIONS");
    try {
      if (childLocationsRS != null) {
        while (childLocationsRS.next()) {
          BrowseUgCoursesByLocationChildVO browseUgCoursesByLocationChildVO = new BrowseUgCoursesByLocationChildVO();
          browseUgCoursesByLocationChildVO.setParentCountryName(browseUgCoursesByLocationVO.getCountryName());
          browseUgCoursesByLocationChildVO.setChildLocationName(childLocationsRS.getString("CHILD_LOCATION_NAME"));
          browseUgCoursesByLocationChildVO.setChildLocationCount(childLocationsRS.getString("CHILD_LOCATION_COUNT"));
          childLocationUrlToSubjectListingPage = seoUrls.construnctUrlToSubjectListingPage(browseUgCoursesByLocationChildVO.getChildLocationName(), "M");
          //wu309_20110504_Syed - location name customized
          browseUgCoursesByLocationChildVO.setChildLocationName(comUtil.getSeoFriendlyLocationName(browseUgCoursesByLocationChildVO.getChildLocationName()));
          browseUgCoursesByLocationChildVO.setChildLocationUrlToSubjectListingPage(childLocationUrlToSubjectListingPage);
          listOfChildLocations.add(browseUgCoursesByLocationChildVO);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (childLocationsRS != null) {
          childLocationsRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    //
    browseUgCoursesByLocationVO.setListOfChildLocations(listOfChildLocations);
    browseUgCoursesByLocationVO.setChildLocationCount(Integer.toString(listOfChildLocations.size()));
    //    tempLocation = browseUgCoursesByLocationVO.getCountryName().replaceAll("ALL ","");
    countryUrlToSubjectListingPage = seoUrls.construnctUrlToSubjectListingPage(browseUgCoursesByLocationVO.getCountryName(), "M");
    browseUgCoursesByLocationVO.setCountryUrlToSubjectListingPage(countryUrlToSubjectListingPage);
    //
    //nullify unused objects
    countryUrlToSubjectListingPage = null;
    childLocationUrlToSubjectListingPage = null;
    seoUrls = null;
    childLocationsRS = null;
    comUtil = null;
    //
    return browseUgCoursesByLocationVO;
  }

}
