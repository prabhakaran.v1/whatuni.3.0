package spring.rowmapper.seo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.seo.PageMetaTagsVO;

public class PageMetaTagsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    PageMetaTagsVO metaTagsVO = new PageMetaTagsVO();
    //
    metaTagsVO.setMetaDescription(resultset.getString("meta_desc"));
    metaTagsVO.setMetaKeywords(resultset.getString("meta_keyword"));
    metaTagsVO.setMetaTitle(resultset.getString("meta_title"));
    metaTagsVO.setMetaStudylevelDesc(resultset.getString("study_desc"));
    metaTagsVO.setMetaCategoryDesc(resultset.getString("category_desc"));
    //
    return metaTagsVO;
  }

}
