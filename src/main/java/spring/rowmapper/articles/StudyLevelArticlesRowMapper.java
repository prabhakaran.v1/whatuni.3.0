package spring.rowmapper.articles;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.articles.ArticleGroupVO;

import com.wuni.util.seo.SeoUrls;

public class StudyLevelArticlesRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ArticleGroupVO articleGroupVO = new ArticleGroupVO();
    articleGroupVO.setArticleGroupUrlSeoName(rs.getString("ARTICLE_GROUP_URL_SEO_NAME"));
    articleGroupVO.setArticleGroupName(articleGroupVO.getArticleGroupUrlSeoName());
    articleGroupVO.setArticleHeading(rs.getString("ARTICLE_HEADING"));
    articleGroupVO.setArticleUrlSeoTitle(rs.getString("ARTICLE_URL_SEO_TITLE"));
    articleGroupVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(articleGroupVO.getArticleUrlSeoTitle(),articleGroupVO.getArticleGroupUrlSeoName()));
    articleGroupVO.setArticleUpdatedDate(rs.getString("ARTICLE_UPDATED_DATE"));
    articleGroupVO.setTotalArticlesForThisArticleGroup(rs.getString("ARTICLE_COUNT"));
    articleGroupVO.setArticleShortDescription(rs.getString("ARTICLE_SHORT_DESC"));
    return articleGroupVO;
  }

}
