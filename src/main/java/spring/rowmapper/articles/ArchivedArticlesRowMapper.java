package spring.rowmapper.articles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.articles.ArticleGroupVO;

import com.wuni.util.seo.SeoUrls;

public class ArchivedArticlesRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ArticleGroupVO articleGroupVO = new ArticleGroupVO();
    articleGroupVO.setArticleGroupName(rs.getString("ARTICLE_GROUP_URL_SEO_NAME"));
    articleGroupVO.setArticleGroupUrlSeoName(articleGroupVO.getArticleGroupName());
    articleGroupVO.setTotalArticlesForThisArticleGroup(rs.getString("ARTICLE_COUNT"));
    //
    ArrayList listOfArticlesBelongsToThisArticleGroup = new ArrayList();
    ResultSet allArticlesRS = (ResultSet)rs.getObject("ALL_ARTICLES");
    try {
      if (allArticlesRS != null) {
        while (allArticlesRS.next()) {
          ArticleGroupVO articleVO = new ArticleGroupVO(); ////we are going to use very minimal details from ArticleVO, this is also available in ArticleGroupVO which have less memory, so ArticleGroupVO is used insteed ArticleVO. in fucture we can refer ArticleVO, if we want more details.
          articleVO.setArticleHeading(allArticlesRS.getString("ARTICLE_HEADING"));
          articleVO.setArticleUrlSeoTitle(allArticlesRS.getString("ARTICLE_URL_SEO_TITLE"));
          articleVO.setArticleUpdatedDate(allArticlesRS.getString("ARTICLE_UPDATED_DATE"));
          articleVO.setAuthorName(allArticlesRS.getString("AUTHOR_NAME"));
          articleVO.setArticleUrl(new SeoUrls().constructArticleSeoUrl(articleVO.getArticleUrlSeoTitle(),articleGroupVO.getArticleGroupName()));
          listOfArticlesBelongsToThisArticleGroup.add(articleVO);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (allArticlesRS != null) {
          allArticlesRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    articleGroupVO.setListOfArticlesBelongsToThisArticleGroup(listOfArticlesBelongsToThisArticleGroup);
    return articleGroupVO;
  }

}
