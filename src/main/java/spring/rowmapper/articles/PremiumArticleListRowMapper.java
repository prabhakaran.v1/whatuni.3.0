package spring.rowmapper.articles;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.articles.ArticleGroupVO;

public class PremiumArticleListRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ArticleGroupVO articleGroupVO = new ArticleGroupVO();
    articleGroupVO.setArticleGroupId(rs.getString("ARTICLE_GROUP_ID"));
    articleGroupVO.setArticleGroupName(rs.getString("ARTICLE_GROUP_URL_SEO_NAME"));
    articleGroupVO.setArticleGroupUrlSeoName(articleGroupVO.getArticleGroupName());
    articleGroupVO.setAuthorName(rs.getString("AUTHOR_ID"));
    articleGroupVO.setAuthorName(rs.getString("AUTHOR_NAME"));
    articleGroupVO.setAuthorMediaPath(rs.getString("AUTHOR_MEDIA_PATH"));
    articleGroupVO.setAuthorShortBio(rs.getString("AUTHOR_SHORT_BIO"));
    articleGroupVO.setArticleId(rs.getString("ARTICLE_ID"));
    articleGroupVO.setArticleHeading(rs.getString("ARTICLE_HEADING"));
    articleGroupVO.setArticleUrlSeoTitle(rs.getString("ARTICLE_URL_SEO_TITLE"));
    articleGroupVO.setArticleShortDescription(rs.getString("ARTICLE_SHORT_DESC"));
    articleGroupVO.setArticleUpdatedDate(rs.getString("ARTICLE_UPDATED_DATE"));
    articleGroupVO.setTotalArticlesForThisArticleGroup(rs.getString("ARTICLE_COUNT"));
    articleGroupVO.setArticleMediaId(rs.getString("ARTICLE_MEDIA_ID"));
    articleGroupVO.setArticleMediaPath(rs.getString("ARTICLE_MEDIA_PATH"));
    articleGroupVO.setArticleMediaType(rs.getString("ARTICLE_MEDIA_TYPE"));
    return articleGroupVO;
  }

}
