package spring.rowmapper.articles;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.articles.ArticleVO;

public class LatestArticleListRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ArticleVO articleVO = new ArticleVO();
    articleVO.setArticleGroupId(rs.getString("ARTICLE_GROUP_ID"));
    articleVO.setArticleGroupUrlSeoName(rs.getString("ARTICLE_GROUP_URL_SEO_NAME"));
    articleVO.setArticleId(rs.getString("ARTICLE_ID"));
    articleVO.setArticleHeading(rs.getString("ARTICLE_HEADING"));
    articleVO.setArticleUrlSeoTitle(rs.getString("ARTICLE_URL_SEO_TITLE"));
    articleVO.setArticleUpdatedDate(rs.getString("ARTICLE_UPDATED_DATE"));
    articleVO.setAuthorName(rs.getString("AUTHOR_NAME"));
    return articleVO;
  }

}
