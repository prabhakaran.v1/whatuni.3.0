package spring.rowmapper.articles;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.articles.ArticleVO;

public class ArticleDetailsRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ArticleVO articleVO = new ArticleVO();
    articleVO.setArticleId(rs.getString("article_id"));
    articleVO.setArticleGroupId(rs.getString("article_group_id"));
    articleVO.setArticleGroupUrlSeoName(rs.getString("article_group_url_seo_name"));
    articleVO.setArticleGroupHeading(articleVO.getArticleGroupUrlSeoName());
    articleVO.setAuthorName(rs.getString("author_id"));
    articleVO.setAuthorName(rs.getString("author_name"));
    articleVO.setAuthorMediaPath(rs.getString("author_media_path"));
    articleVO.setArticleHeading(rs.getString("article_heading"));
    articleVO.setArticleUrlSeoTitle(rs.getString("article_url_seo_title"));
    articleVO.setArticleMetaTitle(rs.getString("article_meta_title"));
    articleVO.setArticleMetaDescription(rs.getString("article_meta_description"));
    articleVO.setArticleMetaKeywords(rs.getString("article_meta_keywords"));
    articleVO.setArticleLdcsCode(rs.getString("article_ldcs1"));
    articleVO.setArticleFullDescription(rs.getString("article_description"));
    articleVO.setArticleArticleGroupQualId(rs.getString("article_group_qual_id"));
    articleVO.setArticleUpdatedDate(rs.getString("article_updated_date"));
    articleVO.setArticleMediaId(rs.getString("article_media_id"));
    articleVO.setArticleMediaPath(rs.getString("article_media_path"));
    articleVO.setArticleMediaType(rs.getString("article_media_type"));
    articleVO.setNextArticleUrlSeoTitle(rs.getString("next_article_url_seo_title"));
    articleVO.setPreviousArticleUrlSeoTitle(rs.getString("previous_article_url_seo_title"));
    articleVO.setArticleCreatedDate(rs.getString("article_created_date"));
    articleVO.setAuthorGoogleAccount(rs.getString("author_google_account"));
    return articleVO;
  }

}
