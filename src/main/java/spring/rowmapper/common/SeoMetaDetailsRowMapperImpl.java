package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
 * @SeoMetaDetailsRowMapperImpl
 * @author   Sri Sankari R
 * @version  1.0
 * @since    20_10_2020
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
 *    
 * 
*/
public class SeoMetaDetailsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    
    SEOMetaDetailsVO seoMetaDetailsVO = new SEOMetaDetailsVO();
    seoMetaDetailsVO.setMetaTitle(resultSet.getString("META_TITLE")); 
    seoMetaDetailsVO.setMetaDesc(resultSet.getString("META_DESC"));
    seoMetaDetailsVO.setMetaKeyword(resultSet.getString("META_KEYWORD"));
    seoMetaDetailsVO.setCategoryDesc(resultSet.getString("CATEGORY_DESC"));
    seoMetaDetailsVO.setStudyDesc(resultSet.getString("STUDY_DESC"));
    seoMetaDetailsVO.setMetaRobots(resultSet.getString("META_ROBO")); 
    return seoMetaDetailsVO;
  }	
}