package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.form.CourseJourneyBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

/**
  *  This class is used to load the random subjects.
  */
public class RandomSubjectListRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    CourseJourneyBean bean = new CourseJourneyBean();
    bean.setSubjectId(rs.getString("subject_id"));
    bean.setSubjectName(rs.getString("subject_name"));
    bean.setStudyLevelText(rs.getString("study_level"));
    bean.setStudyLevelId(rs.getString("qualification_code"));
    bean.setCategoryCodeLength(rs.getString("category_code_length"));
    // To for Subject url pointing to location page 
    /* http://217.33.19.173/degrees/courses/Degree-list/Agricultural-Biology-Degree-courses-UK/qualification/M/search_category/8244/loc.html*/
    String seoStudyLevelDesc = rs.getString("seo_studylevel_text");
    String subjectName = rs.getString("subject_name");
    subjectName = (subjectName != null && subjectName.trim().length() > 0 && subjectName.indexOf("#") >= 0) ? subjectName.replaceAll("#", "") : subjectName;
    subjectName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subjectName));
    String randomizeURL = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + seoStudyLevelDesc + "list/" + subjectName + "-" + seoStudyLevelDesc + "courses-UK/qualification/" + rs.getString("qualification_code") + "/search_category/" + rs.getString("subject_id") + "/loc.html";
    bean.setHyperLinkUrl(randomizeURL);
    return bean;
  }

}
