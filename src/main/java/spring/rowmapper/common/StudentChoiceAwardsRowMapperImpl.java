package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.homepage.form.HomePageBean;
import WUI.utilities.CommonFunction;

public class StudentChoiceAwardsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    HomePageBean bean = new HomePageBean();
    bean.setCollegeId(resultset.getString("college_id"));
    bean.setCollegeName(resultset.getString("college_name"));
    bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
    bean.setQuestionTitle(resultset.getString("question_title"));
    bean.setRating(resultset.getString("rating"));
    String questionTitle = resultset.getString("question_title");
    String seoText = new CommonFunction().seoQuestionTitle(questionTitle);
    bean.setSeoQuestionText(seoText);
    bean.setQuestionId(resultset.getString("question_id"));
    return bean;
  }

}
