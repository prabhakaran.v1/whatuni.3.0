package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.profile.bean.ProfileBean;

public class UserInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    ProfileBean bean = new ProfileBean();
    bean.setUserName(resultset.getString("user_name"));
    /*bean.setEducationStatus(resultset.getString("at_uni"));
    bean.setGender(resultset.getString("gender"));
    bean.setSex(resultset.getString("sex"));
    bean.setNationality(resultset.getString("nationality"));
    bean.setNationalityDescription(resultset.getString("nationality_desc"));
    bean.setDateOfBirth(resultset.getString("dob"));
    bean.setCurrentlyLiving(resultset.getString("currently_living"));
    bean.setNickName(resultset.getString("username"));
    bean.setFirstName(resultset.getString("forename"));
    bean.setSurName(resultset.getString("surname"));*/
    bean.setConfirmEmailId(resultset.getString("user_email"));
    bean.setPassword(resultset.getString("password"));
    /*bean.setEmailId(resultset.getString("user_email"));
    bean.setAboutMe(resultset.getString("about_me"));
    bean.setTurnOn(resultset.getString("turns_on"));
    bean.setTurnOff(resultset.getString("turn_off"));
    bean.setMyMusic(resultset.getString("my_music"));
    bean.setMyMovies(resultset.getString("my_movies"));
    bean.setMyTvShows(resultset.getString("my_tv_show"));
    bean.setHero_heroines(resultset.getString("my_heros"));
    bean.setUnReadMessage(resultset.getString("unread_message"));*/
     //NEED TODO - need to remove for Ram's reference
    /*String userImageName = resultset.getString("user_photo");
    String userThumbName = (userImageName != null ? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()) : userImageName);
    bean.setUserPodImage(userThumbName);
    bean.setUserImage(userImageName);
    bean.setUserImageLarge(userImageName);*/
    return bean;
  }

}
