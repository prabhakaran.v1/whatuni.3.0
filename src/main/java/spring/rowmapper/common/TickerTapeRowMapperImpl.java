package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.GlobalConstants;
import WUI.utilities.TickerTapeBean;

public class TickerTapeRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    TickerTapeBean tickerBean = new TickerTapeBean();
    tickerBean.setTickerName(rs.getString("TICKER_NAME"));
    tickerBean.setTickerCollegeId(rs.getString("COLLEGE_ID"));
    tickerBean.setTickerDescription(rs.getString("DESCRIPTION"));
    String externalUrl = rs.getString("DESTINATION_URL");
    if(externalUrl.indexOf("whatuni.com") >= 0){
      externalUrl = (externalUrl.indexOf("https://") >= 0) ? externalUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (externalUrl.indexOf("http://") >= 0) ? externalUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + externalUrl;
    } else {
      externalUrl = (externalUrl.indexOf("http://") >= 0 || externalUrl.indexOf("https://") >= 0) ? externalUrl : "http://" + externalUrl;
    }
    tickerBean.setTickerExternalUrl(externalUrl);
    return tickerBean;
  }

}
