package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.basket.form.BasketBean;

public class UserSearchBasketRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    BasketBean bean = new BasketBean();
    bean.setSearchName(resultset.getString("search_name"));
    bean.setPageUrl(resultset.getString("search_url"));
    return bean;
  }

}
