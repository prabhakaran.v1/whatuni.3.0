package spring.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.search.form.SearchBean;

public class SubjectCategoriesRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    SearchBean bean = new SearchBean();
    bean.setOptionId(resultset.getString("category_code"));
    bean.setOptionText(resultset.getString("subject_name"));
    bean.setOptionSeqId(resultset.getString("subject_id"));
    return bean;
  }

}
