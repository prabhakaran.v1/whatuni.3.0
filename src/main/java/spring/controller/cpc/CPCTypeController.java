package spring.controller.cpc;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.business.ICPCLandingBusiness;
import spring.dao.util.valueobject.cpc.CPCPageVO;
import com.config.SpringContextInjector;
import WUI.utilities.GlobalConstants;
import com.layer.util.SpringConstants;



/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : CPCType.java
* Description   : Class to get cpc landing page type
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	             Modified On         	Modification Details 	     Change
* *************************************************************************************************************************************
* Hema.S                  1.0              04.02.2020          First draft 
* Keerthana.E             2.0              21.07.2020          Added clearing Changes  		
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/

@Controller
public class CPCTypeController {
	
  @Autowired
  ICPCLandingBusiness cpcLandingBusiness = null;
  
  @Autowired
  SingleCPCLandingPageController singleCPCLandingPageController =null;
  
  @Autowired
  MultipleCPCLandingPageController multipleCPCLandingPageController = null;
	
  @RequestMapping(value = {"/get-cpc-type","/clearing/get-cpc-type"}, method = RequestMethod.GET)
  public ModelAndView getCPCType(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
  final String URL_STRING = StringUtils.replace(StringUtils.substring(request.getRequestURI(), request.getContextPath().length()), ".html", "");
  String landingId = request.getParameter("campaign");
    String previewFlag = request.getParameter("preview");
    previewFlag = (!GenericValidator.isBlankOrNull(previewFlag)) ? previewFlag : "N";
    CPCPageVO cPCPageVO = new CPCPageVO();
	cPCPageVO.setLandingId(landingId);
	cPCPageVO.setPreviewFlag(previewFlag.toUpperCase());
	Map resultMap = cpcLandingBusiness.getCpcTypeValues(cPCPageVO);
	String cpcType = (String)resultMap.get("P_LANDING_PAGE_TYPE"); 
	String status = (String)resultMap.get("P_STATUS"); 
	String fallBackURL = (String)resultMap.get("P_FALLBACK_URL");
	String clearingFlag = (String)resultMap.get("P_SHOW_CLEARING_DATA_FLAG");     
	if("EXPIRED".equalsIgnoreCase(status) || (StringUtils.equalsIgnoreCase(clearingFlag,"N") && URL_STRING.indexOf("clearing") > -1) || (StringUtils.equalsIgnoreCase(clearingFlag,"Y") && URL_STRING.indexOf("clearing") <= -1)){
	  response.sendRedirect(fallBackURL);
	}else{
	  if("MULTIPLE".equalsIgnoreCase(cpcType)){
		  if(StringUtils.equalsIgnoreCase(clearingFlag,"Y") && URL_STRING.indexOf("clearing") > -1){
		        request.setAttribute("cpcClearingFlag", clearingFlag);	
		    }
	    return multipleCPCLandingPageController.getMultipleCPCLandingPage(model, request, response, session, landingId);
	  }else if("SINGLE".equalsIgnoreCase(cpcType)){
	    return singleCPCLandingPageController.getSingleCPCLandingPage(model, request, response, session,landingId);
	  }else{
        request.setAttribute(GlobalConstants.REASON_FOR_404, GlobalConstants.INVALID_CAMPAIGN_ID);
        response.sendError(404, GlobalConstants.ERROR_MSG_404);
	    return null;
	  }
	}
	return null;
  }
}
