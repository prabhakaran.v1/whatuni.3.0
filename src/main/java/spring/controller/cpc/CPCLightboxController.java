package spring.controller.cpc;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.business.ICPCLandingBusiness;
import spring.dao.util.valueobject.cpc.CPCLightboxVO;
import com.config.SpringContextInjector;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;



/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : CPCLightboxController
* Description   : Class to get the lightbox for both the cpc landing page type
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	             Modified On         	Modification Details 	     Change
* *************************************************************************************************************************************
* Jeyalakshmi.D                     1.0                 06.02.2020             First draft   		
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/

@Controller
public class CPCLightboxController {
	
  @Autowired
  ICPCLandingBusiness cpcLandingBusiness = null;
	
  @RequestMapping(value = {"/get-cpc-lightbox"}, method = RequestMethod.POST)
  public ModelAndView getCPCLightbox(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    CommonUtil utilities = new CommonUtil();
    String collegeId = request.getParameter("collegeId");
    String landingId =  request.getParameter("landingId");
    String subject = !GenericValidator.isBlankOrNull(request.getParameter("subject")) ? request.getParameter("subject") : null;
    String location = !GenericValidator.isBlankOrNull(request.getParameter("location")) ? request.getParameter("location") : null;
    String preview = !GenericValidator.isBlankOrNull(request.getParameter("preview")) ? request.getParameter("preview") : "N";
    String landingPage = request.getParameter("landingPage");
    String clearingFlag = StringUtils.isNotBlank(request.getParameter("clearingFlag")) ? request.getParameter("clearingFlag") : null;
    boolean mobileFlag = utilities.getMobileFlag(request);
    String gaCollegeName = "";
	CPCLightboxVO cPCLightboxVO = new CPCLightboxVO();
	CommonUtil comUtil = new CommonUtil();
	CommonFunction commonFunction = new CommonFunction();
	request.setAttribute("preview", preview);
	cPCLightboxVO.setCollegeId(collegeId);
	cPCLightboxVO.setLandingId(landingId);
	cPCLightboxVO.setBrowseCatId(subject);
	cPCLightboxVO.setRegionId(location);
	cPCLightboxVO.setPreviewFlag(preview.toUpperCase());
	String networkId = "";
	String courseExistsFlag = "";
	String collegeName = "";
	String studyLevelDesc = "";
	Map resultMap = cpcLandingBusiness.getCPCLightboxValues(cPCLightboxVO);
	ArrayList imageDetailsList = (ArrayList)resultMap.get("PC_IMAGE_DETAILS");
    if (imageDetailsList != null && imageDetailsList.size() > 0) {
      request.setAttribute("imageDetailsList", imageDetailsList);
    }
    ArrayList uniDetailsList = (ArrayList)resultMap.get("PC_UNI_DETAILS");
    if (uniDetailsList != null && uniDetailsList.size() > 0) {
      request.setAttribute("uniDetailsList", uniDetailsList);
     // System.out.println("uniDetailsList------>" + uniDetailsList.size());
      CPCLightboxVO cPCLightboxVO1 = new CPCLightboxVO();
      cPCLightboxVO1 = (CPCLightboxVO)uniDetailsList.get(0);                  
      networkId = (!GenericValidator.isBlankOrNull(cPCLightboxVO1.getNetworkId())) ? cPCLightboxVO1.getNetworkId() : "";          
      collegeName = (!GenericValidator.isBlankOrNull(cPCLightboxVO1.getCollegeName())) ? cPCLightboxVO1.getCollegeName() : "";      
      studyLevelDesc = (!GenericValidator.isBlankOrNull(cPCLightboxVO1.getStudyLevel())) ? cPCLightboxVO1.getStudyLevel() : "";
      if(!GenericValidator.isBlankOrNull(collegeName)) {
    	  gaCollegeName = commonFunction.replaceSpecialCharacter(collegeName);  
      }	  
    }
    ArrayList reviewDetailsList = (ArrayList)resultMap.get("PC_LATEST_REVIEW");
    if (reviewDetailsList != null && reviewDetailsList.size() > 0) {
      request.setAttribute("reviewDetailsList", reviewDetailsList);
      String userNameColorClassName = "rev_grey";      
      for (int i = 0; i < reviewDetailsList.size(); i++) {
    	cPCLightboxVO = (CPCLightboxVO)reviewDetailsList.get(i);
        //getting the user name color class name 
        if(!GenericValidator.isBlankOrNull(cPCLightboxVO.getUserNameInt()) ||!GenericValidator.isBlankOrNull(cPCLightboxVO.getUserNameInitialColourcode()) ){   
          userNameColorClassName = comUtil.getReviewUserNameColorCode(cPCLightboxVO.getUserNameInt());
        }        		
        cPCLightboxVO.setUserNameInitialColourcode(userNameColorClassName);
      }
  	courseExistsFlag = cPCLightboxVO.getCourseDeletedFlag();  
    }
  //Creating the view all course url(need to clarify the logic courseExistsFlag)
    String viewAllCourseUrl = "";
    String qualification = "degree";
    if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
      qualification="postgraduate";
    }        
    if("TRUE".equalsIgnoreCase(courseExistsFlag)){
      qualification = "all";
    }
    viewAllCourseUrl = new SeoUrls().constructCourseUrl(qualification, "", collegeName);
    if(!GenericValidator.isBlankOrNull(studyLevelDesc)) {			
		request.setAttribute("studyLevelDesc", studyLevelDesc);
	}
    request.setAttribute("viewAllCourseUrl", viewAllCourseUrl);	
    request.setAttribute("landingPage", landingPage);    
	request.setAttribute("gaCollegeName", gaCollegeName);   
    request.setAttribute("hitbox_college_name", collegeName);
    model.addAttribute("clearingFlag", clearingFlag);
    model.addAttribute("mobileFlag", mobileFlag);
	return new ModelAndView("cpclightbox");
  }
}
