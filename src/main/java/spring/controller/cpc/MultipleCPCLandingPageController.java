package spring.controller.cpc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.business.ICPCLandingBusiness;
import spring.dao.util.valueobject.cpc.CPCPageVO;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SearchUtilities;

/**
*
* Class         : MultipleCPCLandingPageController.java
* Description   : Class to get multiple cpc landing page type
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author                 Ver              Modified On         Modification Details      Change
* *************************************************************************************************************************************
* Keerthana.E            2.0              21.07.2020          Added clearing Changes  
*
*/

@Controller
public class MultipleCPCLandingPageController {
  
  @Autowired
  ICPCLandingBusiness cpcLandingBusiness = null;
  
  @RequestMapping(value = {"/get-multiple-type-cpc-landing-page"}, method = RequestMethod.GET)
  public ModelAndView getMultipleCPCLandingPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session, String campaignId) throws Exception {
	    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    final String URL_QUR_STRING = request.getQueryString();
    CommonUtil utilities = new CommonUtil();
    CPCPageVO cPCPageVO = new CPCPageVO();
    String forward = "multiple-cpc-landing-page";
    String action = request.getParameter("action");
    String subject = request.getParameter("subject");
    String regionId = request.getParameter("region");
    String sortCode = request.getParameter("sortCode");
    String campaign = request.getParameter("campaign");
    String browseCatId = request.getParameter("browseCatId");
    String previewFlag = request.getParameter("preview");

    String studyLevelDesc = "";
    previewFlag = !GenericValidator.isBlankOrNull(previewFlag) ? previewFlag : "N";
    request.setAttribute("preview", previewFlag);
    request.setAttribute("campaign", campaign);
    String sortName = ("ta".equalsIgnoreCase(sortCode)) ? "A - Z" : ("td".equalsIgnoreCase(sortCode)) ? "Z - A" : ("wh".equalsIgnoreCase(sortCode)) ? "Whatuni WUSCA Rating High to Low" : ("wl".equalsIgnoreCase(sortCode)) ? "Whatuni WUSCA Rating Low to High" : "Most info";
    String collegeName = "";
    String networkId = "2";
    cPCPageVO.setAjaxFlag("N");
    cPCPageVO.setPreviewFlag(previewFlag.toUpperCase());
    cPCPageVO.setBrowseCatId(browseCatId);
    cPCPageVO.setRegionId(regionId);
    request.setAttribute("selectedSortCode", sortCode);
    request.setAttribute("sortName", sortName);
    SearchUtilities searchutil = new SearchUtilities();
    String p_recentFilter = GenericValidator.isBlankOrNull(request.getParameter("rf")) ? null : (request.getParameter("rf"));
	    String pageno = !GenericValidator.isBlankOrNull(request.getParameter("pageno")) ? (request.getParameter("pageno")) : "1";
    if ("DROPDOWN_AJAX".equalsIgnoreCase(action)) {
      cPCPageVO.setAjaxFlag("Y");
      cPCPageVO.setSubjectCode(subject);
      if ("def".equalsIgnoreCase(sortCode)) {
        sortCode = "";
      }
      cPCPageVO.setSortingCode(sortCode);
      forward = "multiple-cpc-ajax-pod";
    }
    cPCPageVO.setPageNo(pageno);
    cPCPageVO.setLandingId(campaign);
    cPCPageVO.setDynamicRandomNumber(utilities.getSessionRandomNumber(request, session));
    Map resultMap = cpcLandingBusiness.getMultipleLandingPageValues(cPCPageVO);
    // Getting Article list for Top nav
    ArrayList topNavArticlesList = (ArrayList) resultMap.get("PC_TOP_NAV_ARTICLE_LINKS");
    if (!CommonUtil.isBlankOrNull(topNavArticlesList)) {
      model.addAttribute("articlesList", topNavArticlesList);
    }

    ArrayList landingPageTopSeclist = (ArrayList) resultMap.get("PC_LANDING_PAGE_DET");
    if (landingPageTopSeclist != null && landingPageTopSeclist.size() > 0) {
      CPCPageVO cpcPageVO = (CPCPageVO) landingPageTopSeclist.get(0);
      request.setAttribute("title", cpcPageVO.getTitle());
      request.setAttribute("subTitle", cpcPageVO.getSubTitle());
      request.setAttribute("editorialContent", cpcPageVO.getEditorialContent());
      request.setAttribute("mediaPath", cpcPageVO.getMediaPath());
      request.setAttribute("landingPageTopSeclist", landingPageTopSeclist);
      studyLevelDesc = (!GenericValidator.isBlankOrNull(cpcPageVO.getStudyLevel())) ? cpcPageVO.getStudyLevel() : "";
      if (!GenericValidator.isBlankOrNull(studyLevelDesc)) {
        request.setAttribute("studyLevelDesc", studyLevelDesc);
      }
    }
    ArrayList providerDetailsList = (ArrayList) resultMap.get("PC_PROVIDER_DET_LIST");
    if (providerDetailsList != null && providerDetailsList.size() > 0) {
      request.setAttribute("providerDetailsList", providerDetailsList);
    }
    ArrayList subjectFilterList = (ArrayList) resultMap.get("PC_SUBJECT_FILTER");
    if (subjectFilterList != null && subjectFilterList.size() > 0) {
      request.setAttribute("subjectFilterList", subjectFilterList);
      List<CPCPageVO> subjectFilterListVal = subjectFilterList;
      String selectedSubjectCode = subjectFilterListVal.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedSubjectFlag())).map(i -> i.getSubjectCode()).collect(Collectors.joining(""));
	   	   String selectedSubjectName = subjectFilterListVal.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedSubjectFlag())).map(i -> i.getSubjectName()).collect(Collectors.joining(""));
      request.setAttribute("selectedSubjectCode", selectedSubjectCode);
      request.setAttribute("selectedSubjectName", selectedSubjectName);
    }
    ArrayList locationFilterList = (ArrayList) resultMap.get("PC_LOCATION_FILTER");
    if (locationFilterList != null && locationFilterList.size() > 0) {
      request.setAttribute("locationFilterList", locationFilterList);
      List<CPCPageVO> locationFilterListVal = locationFilterList;
      String selectedRegionId = locationFilterListVal.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedRegionFlag())).map(i -> i.getRegionId()).collect(Collectors.joining(""));
		      String selectedRegionName = locationFilterListVal.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedRegionFlag())).map(i -> i.getRegionName()).collect(Collectors.joining(""));
      request.setAttribute("selectedRegionId", selectedRegionId);
      request.setAttribute("selectedRegionName", selectedRegionName);
    }
    String whatuniLogoFlag = (String) resultMap.get("P_WHATUNI_LOGO_FLAG");
    if (!GenericValidator.isBlankOrNull(whatuniLogoFlag)) {
      request.setAttribute("whatuniLogoFlag", whatuniLogoFlag);
    }
    String filtersHideFlag = (String) resultMap.get("p_filters_flag");
    if (!GenericValidator.isBlankOrNull(filtersHideFlag)) {
      request.setAttribute("filtersHideFlag", filtersHideFlag);
    }
    String sortHideFlag = (String) resultMap.get("p_sorting_flag");
    if (!GenericValidator.isBlankOrNull(sortHideFlag)) {
      request.setAttribute("sortHideFlag", sortHideFlag);
    }
    String hamburgerFlag = (String) resultMap.get("P_HAMBURGER_FLAG");
    if (!GenericValidator.isBlankOrNull(hamburgerFlag)) {
      request.setAttribute("hamburgerFlag", hamburgerFlag);
    }
    String uniCount = (String) resultMap.get("P_RECORD_COUNT");
    if (!GenericValidator.isBlankOrNull(uniCount)) {
      request.setAttribute("universityCount", uniCount);
    }
    String resultExists = (String) resultMap.get("P_RESULT_EXISTS");
    if (!GenericValidator.isBlankOrNull(resultExists)) {
      request.setAttribute("resultExists", resultExists);
    }
    String clearingFlag = (String) resultMap.get("P_SHOW_CLEARING_DATA_FLAG");
    if (StringUtils.isNotBlank(clearingFlag)) {
      request.setAttribute("clearingFlag", clearingFlag);
    }
    boolean mobileFlag = utilities.getMobileFlag(request);
    model.addAttribute("mobileFlag", mobileFlag);
    String cpeQualificationNetworkId = (String) request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = GlobalConstants.UG_NETWORK_ID;
    }
    String qualification = "degree";
    if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
      qualification = "postgraduate";
    }

    String URL_1 = "";
    String URL_2 = "";
    String seoFirstPageUrl = "";
    URL_1 = URL_STRING;
    seoFirstPageUrl = URL_1 + "?" + URL_QUR_STRING;
    request.setAttribute("URL_STRING", URL_STRING);
    request.setAttribute("URL_1", URL_1);
    request.setAttribute("URL_2", URL_2);
    request.setAttribute("SEO_FIRST_PAGE_URL", seoFirstPageUrl);
    request.setAttribute("hitbox_college_name", collegeName);
    request.setAttribute("pageno", pageno);
    return new ModelAndView(forward);
  }
}
