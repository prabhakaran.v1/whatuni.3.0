package spring.controller.cpc;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.contenthub.EnquiryVO;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SearchUtilities;
import spring.business.ICPCLandingBusiness;
import spring.dao.util.valueobject.cpc.CPCPageVO;
/**
 * @ReviewAction
 * @author Jeyalakshmi D
 * @version 1.0
 * @since 10.01.2020
 * @purpose This program is used to display the single CPC related information.
 * **************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * ************************************************************************************************************************************** *
 * Sangeeth.S                             		10.01.20				initial draft
 */
@Controller
public class SingleCPCLandingPageController {
  
  @Autowired
  ICPCLandingBusiness cpcLandingBusiness = null;
  
  @RequestMapping(value = {"/get-single-type-cpc-landing-page"}, method = RequestMethod.GET)
  public ModelAndView getSingleCPCLandingPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session, String campaignId) throws Exception {
	//Declaring variables
	final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
	final String URL_QUR_STRING = request.getQueryString();		
	CPCPageVO cpcPageVO = new CPCPageVO();
	String forward = "single-cpc-landing-page";
	String action = request.getParameter("action");				
	String landingId = request.getParameter("campaign");
	request.setAttribute("campaign", landingId);
	String pageNo = request.getParameter("pageno");
	String recordCount = "10";
	String browseCatId = request.getParameter("browseCatId");
	String sortBy = request.getParameter("sortCode");	
	String sortName = ("ta".equalsIgnoreCase(sortBy)) ? "A - Z" : ("td".equalsIgnoreCase(sortBy)) ? "Z - A" : ("entd".equalsIgnoreCase(sortBy)) ? "UCAS tariff points - High to low" : ("enta".equalsIgnoreCase(sortBy)) ? "UCAS tariff points - Low to high" : "Most info";	
	String previewFlag = request.getParameter("preview");	
	String ajaxFlag = request.getParameter("ajaxFlag");	
	String whatuniLogoFlag = "";
	String hamBurgerFlag = "";	
	String courseCount = "0";
	String networkId = "";
	String courseExistsFlag = "";
	String collegeName = "";
	String courseCountFlag = "";
	String studyLevelDesc = "";
	ArrayList subjectList = null;
	ArrayList providerDetailList = null;
	ArrayList courseDetailList = null;	
	ArrayList topNavArticlesList = null;
	//
	if("DROPDOWN_AJAX".equalsIgnoreCase(action)){  
	  ajaxFlag = "Y";
	  forward = "single-cpc-ajax-pod";
	} 	
	landingId = (!GenericValidator.isBlankOrNull(landingId)) ? landingId : "0";
	pageNo = (!GenericValidator.isBlankOrNull(pageNo)) ? pageNo : "1";
	browseCatId = (!GenericValidator.isBlankOrNull(browseCatId)) ? browseCatId : null;
	sortBy = (!GenericValidator.isBlankOrNull(sortBy)) ? sortBy : null;
	previewFlag = (!GenericValidator.isBlankOrNull(previewFlag)) ? previewFlag : "N";
	ajaxFlag = (!GenericValidator.isBlankOrNull(ajaxFlag)) ? ajaxFlag : "N";
	request.setAttribute("preview", previewFlag);
	//DB call	
	cpcPageVO.setLandingId(landingId);
	cpcPageVO.setPageNo(pageNo);
	cpcPageVO.setBrowseCatId(browseCatId);
	cpcPageVO.setSortBy(sortBy);	
	cpcPageVO.setPreviewFlag(previewFlag.toUpperCase());
	cpcPageVO.setAjaxFlag(ajaxFlag);
	Map resultMap = cpcLandingBusiness.getSingleLandingPageValues(cpcPageVO);
    //Getting data from DB assigning in request
	if(!CommonUtil.isBlankOrNull(resultMap)) {    	 
	  subjectList = (ArrayList)resultMap.get("PC_SUBJECT_LIST");
	  if (!CommonUtil.isBlankOrNull(subjectList)) { 		
		request.setAttribute("subjectFilterList", subjectList);		   
		CPCPageVO cpcPageVO1 = new CPCPageVO();
		for(int i=0; i < subjectList.size() ; i++) {
		  cpcPageVO1 = (CPCPageVO)subjectList.get(i);                  
		  if("Y".equalsIgnoreCase(cpcPageVO1.getSelectedSubjectFlag())){
            request.setAttribute("selectedSubjectCode", cpcPageVO1.getBrowseCatId());
            request.setAttribute("selectedSubjectName", cpcPageVO1.getBrowseCatDesc());            
          }
		  //System.out.println(cpcPageVO1.getSelectedSubjectFlag()+"selsubflag" +cpcPageVO1.getBrowseCatId());
		}
	  }
	  providerDetailList = (ArrayList)resultMap.get("PC_PROVIDER_DETAILS");
	  if (!CommonUtil.isBlankOrNull(providerDetailList)) { 		
		request.setAttribute("providerDetailList", providerDetailList);
		CPCPageVO cpcPageVO1 = new CPCPageVO();       
		cpcPageVO1 = (CPCPageVO)providerDetailList.get(0);  
		collegeName = (!GenericValidator.isBlankOrNull(cpcPageVO1.getCollegeName())) ? cpcPageVO1.getCollegeName() : ""; 
		studyLevelDesc = (!GenericValidator.isBlankOrNull(cpcPageVO1.getStudyLevel())) ? cpcPageVO1.getStudyLevel() : "";
		if(!GenericValidator.isBlankOrNull(studyLevelDesc)) {			
			request.setAttribute("studyLevelDesc", studyLevelDesc);
		}
	  }
	  courseDetailList = (ArrayList)resultMap.get("PC_COURSE_DETAILS");
	  if (!CommonUtil.isBlankOrNull(courseDetailList)) { 		
		request.setAttribute("courseDetailList", courseDetailList);
		CPCPageVO cpcPageVO1 = new CPCPageVO();       
		cpcPageVO1 = (CPCPageVO)courseDetailList.get(0);                  
        networkId = (!GenericValidator.isBlankOrNull(cpcPageVO1.getNetworkId())) ? cpcPageVO1.getNetworkId() : "";          
        collegeName = (!GenericValidator.isBlankOrNull(cpcPageVO1.getCollegeName())) ? cpcPageVO1.getCollegeName() : "";       
	  }
	  //Getting Article list for Top nav
	  topNavArticlesList = (ArrayList)resultMap.get("PC_TOP_NAV_ARTICLE_LINKS"); 
	  if(!CommonUtil.isBlankOrNull(topNavArticlesList)){ 	
	    model.addAttribute("articlesList", topNavArticlesList);
	  }
	  whatuniLogoFlag = (String)resultMap.get("P_WHATUNI_LOGO_FLAG");        
	  if (!GenericValidator.isBlankOrNull(whatuniLogoFlag)) {
		request.setAttribute("whatuniLogoFlag", whatuniLogoFlag);
	  }
	  hamBurgerFlag = (String)resultMap.get("P_HAMBURGER_FLAG");        
	  if (!GenericValidator.isBlankOrNull(hamBurgerFlag)) {
		request.setAttribute("hamBurgerFlag", hamBurgerFlag);
	  }
	  courseCount = (String)resultMap.get("P_COURSE_COUNT");        
	  if (!GenericValidator.isBlankOrNull(courseCount)) {
		request.setAttribute("courseCount", courseCount);
	  }
	  courseCountFlag = (String)resultMap.get("P_COURSE_COUNT_FLAG");        
	  if (!GenericValidator.isBlankOrNull(courseCountFlag)) {
		request.setAttribute("courseCountFlag", courseCountFlag);
	  }
	  String filterFlag = (String)resultMap.get("P_FILTERS_FLAG");        
	  if (!GenericValidator.isBlankOrNull(filterFlag)) {
		request.setAttribute("filterFlag", filterFlag);
	  }
	  String sortingFlag = (String)resultMap.get("P_SORTING_DROPDOWN_FLAG");        
	  if (!GenericValidator.isBlankOrNull(sortingFlag)) {
		request.setAttribute("sortingFlag", sortingFlag);
	  }
	//Creating the view all course url(need to clarify the logic courseExistsFlag)
      String viewAllCourseUrl = "";
      String qualification = "degree";
      if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
        qualification="postgraduate";
      }        
      if("N".equalsIgnoreCase(courseExistsFlag)){
        qualification = "all";
      }
      viewAllCourseUrl = new SeoUrls().constructCourseUrl(qualification, "", collegeName);
      request.setAttribute("viewAllCourseUrl", viewAllCourseUrl);		  
    }
	//
	String URL_1 = "";
    String URL_2 = "";
    String seoFirstPageUrl = "";      
    URL_1 = URL_STRING;            
    seoFirstPageUrl = URL_1 + "?" + URL_QUR_STRING;
    request.setAttribute("URL_STRING", URL_STRING);
    request.setAttribute("URL_1", URL_1);
    request.setAttribute("URL_2", URL_2);
    request.setAttribute("SEO_FIRST_PAGE_URL", seoFirstPageUrl);
    request.setAttribute("pageno", pageNo);
	request.setAttribute("selectedSortCode", sortBy);
	request.setAttribute("sortName", sortName);	
	request.setAttribute("hitbox_college_name", collegeName);	
	return new ModelAndView(forward);
  }
}
