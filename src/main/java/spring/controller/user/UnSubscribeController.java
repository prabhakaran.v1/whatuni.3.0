package spring.controller.user;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import spring.business.IUserBusiness;
import spring.pojo.user.UnSubcribeForm;
import spring.valueobject.user.UnSubscribePageVO;
/**
* @IOpenDayBusiness
* @author  Sangeeth.S
* @version  1.0
* @since    08_12_2020
* @purpose  Getting user unsubscribe page
* Change Log
* *************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	                 Change
* *************************************************************************************************************************
* Sangeeth.S   													initial draft
* 
*/
@Controller
public class UnSubscribeController {
  @Autowired	
  IUserBusiness userBusiness = null;
	
  @RequestMapping(value = { "/unsubscribe/{encryptedUserId}"},method = {RequestMethod.GET})
  public ModelAndView getUnSubscribePage(@PathVariable("encryptedUserId") Optional<String> encryptedUserId, @ModelAttribute("unSubcribeForm") UnSubcribeForm unSubcribeForm, Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)throws Exception {
	  //String fromUrl = "/unsubscribe.html";
	  String userId = new SessionData().getData(request, "y");
      userId = StringUtils.isEmpty(userId) || "0".equals(userId)? null : userId;
      String encryptUserId = null;
             encryptUserId = encryptedUserId.isPresent() ? encryptedUserId.get() : null;
      boolean redirectToLogin = false;
	  if(StringUtils.isEmpty(encryptUserId) && (StringUtils.isEmpty(userId) || "0".equals(userId))){		
		  redirectToLogin = true;
	  }else{		  
		  unSubcribeForm.setEncryptUserId(encryptUserId);
		  unSubcribeForm.setUserId(userId);
		  Map<String, Object> unSubscribeMap = userBusiness.getUnSubscribePage(unSubcribeForm);
		  if(!CollectionUtils.isEmpty(unSubscribeMap)){
			  ArrayList<UnSubscribePageVO> userPreferenceList = (ArrayList<UnSubscribePageVO>) unSubscribeMap.get("PC_USER_PREFERENCE");
			  if(!CollectionUtils.isEmpty(userPreferenceList)){
				model.addAttribute("userPreferenceList", userPreferenceList);
				UnSubscribePageVO unSubscribePageVO =  userPreferenceList.get(0);
				model.addAttribute("newsLetterFlag", unSubscribePageVO.getNewsletter());
				model.addAttribute("remainderFlag", unSubscribePageVO.getRemainder());
				model.addAttribute("solusFlag", unSubscribePageVO.getUniversityUpdates());
				model.addAttribute("surveyFlag", unSubscribePageVO.getSurvey());
				model.addAttribute("encryptedUserId", unSubscribePageVO.getEncryptedId());				
               model.addAttribute("reviewSurveyFlag", unSubscribePageVO.getReviewSurveyFlag());
		     }
			  String validUserIdFlag = (String)unSubscribeMap.get("P_VALID_USER_FLAG");			  
			  if("N".equalsIgnoreCase(validUserIdFlag)){					
				  redirectToLogin = true;				  
			  } 
	     }		 
	  }
	  if(redirectToLogin){					
        /*if (session.getAttribute("fromUrl") == null) {
          session.setAttribute("fromUrl", fromUrl);
        }        
        return new ModelAndView("loginPage", "userLoginBean", new UserLoginBean());*/
		  String redirectUrl = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName() + "/unsubscribe";
	      response.setStatus(response.SC_MOVED_TEMPORARILY);
	      response.sendRedirect(redirectUrl);
	      return null;
	  }	
	return new ModelAndView("unsubscribe-page");  	 	     
  }
  @RequestMapping(value={"/save-user-preference"},method = {RequestMethod.POST })
  public ModelAndView saveUserPreference(@ModelAttribute("unSubcribeForm") UnSubcribeForm unSubcribeForm, Model model, HttpServletRequest request, HttpSession session) throws Exception{
	
	userBusiness.saveUserPreference(unSubcribeForm);
	model.addAttribute("newsLetterFlag", unSubcribeForm.getNewsLetters());
	model.addAttribute("remainderFlag", unSubcribeForm.getRemainderMail());
	model.addAttribute("solusFlag", unSubcribeForm.getSolusEmail());
	model.addAttribute("surveyFlag", unSubcribeForm.getSurvey());
	model.addAttribute("encryptedUserId", unSubcribeForm.getEncryptUserId());
	model.addAttribute("saveFlag", "Y");
   model.addAttribute("reviewSurveyFlag", unSubcribeForm.getReviewSurveyFlag());
    return new ModelAndView("unsubscribe-page");
  }
}
