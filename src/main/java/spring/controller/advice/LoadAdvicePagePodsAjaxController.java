package spring.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import WUI.utilities.SessionData;

@Controller
public class LoadAdvicePagePodsAjaxController {
	
  @RequestMapping(value = "/loadAdviceAjaxPods", method = {RequestMethod.GET, RequestMethod.POST})	
  public ModelAndView getLoadAdviceAjaxPods(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String podName = request.getParameter("podName"); //p_type_of_popular
    if (!GenericValidator.isBlankOrNull(podName)) {
      String userId = new SessionData().getData(request, "y");
      String userTrackId = new SessionData().getData(request, "userTrackId");
      String primaryCategoryName = request.getParameter("primaryCategory"); //p_primary_category_name
      String parentCategoryName = request.getParameter("parentCategory"); //p_primary_category_name
      String secondaryCategoryName = request.getParameter("secondaryCategory"); //p_primary_category_name
      String articleId = request.getParameter("articleId"); //p_post_id
      String podPosition = !GenericValidator.isBlankOrNull(request.getParameter("podposition")) ? request.getParameter("podposition") : null; //p_pagename       
      String forwardString = "loadAdvicePagePods";
      if(GenericValidator.isBlankOrNull(userId)){
        userId = null;
      }
      List parameters = new ArrayList();
      parameters.add(articleId);
      parameters.add(podName);
      parameters.add(primaryCategoryName);
      parameters.add(userId);
      parameters.add(userTrackId);
      parameters.add(podPosition);
      parameters.add(parentCategoryName);
      parameters.add(secondaryCategoryName);
      IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
      Map adviceAjaxInfoMap = adviceBusiness.getAdviceAjaxDetail(parameters);
      ArrayList adviceAjaxPodResults = (ArrayList)adviceAjaxInfoMap.get("OC_ARTICLE_DETAILS_INFO");
      if (adviceAjaxPodResults != null && adviceAjaxPodResults.size() > 0) {
        if ("TRENDING".equalsIgnoreCase(podName)) {
          request.setAttribute("trendingPopularAdviceList", adviceAjaxPodResults);
        } else if ("MOST".equalsIgnoreCase(podName)) {          
          if ("CONTENT_PAGE_MOST_POPULAR_POD_BOTTOM".equalsIgnoreCase(podPosition)) {            
            request.setAttribute("articlesList", adviceAjaxPodResults);
            request.setAttribute("podType", "Popular Pod");
            forwardString = "articles-pod";
          } else {
            request.setAttribute("mostPopularAdviceList", adviceAjaxPodResults);  
          }
        }
      }
      return new ModelAndView(forwardString); //Trending and most popular advice pod
    } else {
      return new ModelAndView("loadSpambox"); //Don't miss out pod
    }
  }
}
