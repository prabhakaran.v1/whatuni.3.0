package spring.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.valueobject.advice.AdviceHomeInfoVO;
import spring.valueobject.seo.SEOMetaDetailsVO;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

/**
 * @AdviceSearchResultsController
 * @since        wu333_20141209
 * @author       Thiyagu G
 * Change Log
 * ******************************************************************************************************
 * author	              Ver 	              Modified On     	     Modification Details 	                
 * ******************************************************************************************************
 * Sujitha V              1.1                 2021_JAN_19            Added SEM cursor
 * 
*/

@Controller
public class AdviceSearchResultsController {
	
  @RequestMapping(value = "/article-search", method = {RequestMethod.GET,RequestMethod.POST})	
  public ModelAndView getArticleSearch(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());
    //-----------------------------------------------------------------------------------------------------------------------------------------
    //session = request.getSession();
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    //Added getSchemeName by Indumathi Mar-29-16    
    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length()))); //this used for addThis
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
     return new ModelAndView("forward: /userLogin.html");
   }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    global.removeSessionData(session, request, response);
    if(session.getAttribute("gamKeywordOrSubject")!=null){session.removeAttribute("gamKeywordOrSubject");}
    //
    String userId = new SessionData().getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
    String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0)) ? common.checkCookieStatus(request) : "";
    basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
    userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------    
    //
    //loading ArticleGroupsHome details
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) { 
      cpeQualificationNetworkId = "2";
    }    
    String keyword = request.getParameter("keyword") != null? request.getParameter("keyword").trim(): "";
    String page = request.getParameter("page") != null? request.getParameter("page").trim(): "1";
    String pageName = (request.getParameter("pageName") != null) ? request.getParameter("pageName").trim() : "";    
    
    if (keyword == null || "".equals(keyword)) {
      request.setAttribute("reason_for_404", "--(1)-- this page dont have keyword");
      response.sendError(404, "404 error message");
      return null;
    }
    if (page == null || "".equals(page)) {
      request.setAttribute("reason_for_404", "--(1)-- this page dont have page number");
      response.sendError(404, "404 error message");
      return null;
    }
    String totalCount = "0";
    
    String pageURL = common.getFullURL(request, false);
    pageURL = StringUtils.replaceEach(pageURL, new String[]{"/degrees", ".html"}, new String[]{"", "/"});
    
    List parameters = new ArrayList();
    parameters.add(keyword);
    parameters.add(page);    
    parameters.add(pageURL);//Added for SEM
    IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
    Map adviceSearchInfoMap = adviceBusiness.getAdviceSearchResults(parameters);
    request.setAttribute("keyword", keyword);
    request.setAttribute("page", page);
    request.setAttribute("pageName", pageName);
    request.setAttribute("mappingPath", request.getRequestURI().substring(request.getContextPath().length()).replace(".html", ""));
    if (keyword!=null || !"".equals(keyword)) {
      session.setAttribute("gamKeywordOrSubject", common.replaceHypen(common.replaceURL(keyword)).toLowerCase());
      request.setAttribute("gamKeywordOrSubject", common.replaceHypen(common.replaceURL(keyword)).toLowerCase());//Added by Indumathi.S To get in googleAdSlots July_5_2016
    } 
    //premium article list
    ArrayList adviceSearchResults = (ArrayList)adviceSearchInfoMap.get("pc_search_results");    
    if (adviceSearchResults != null && adviceSearchResults.size() > 0) {
      request.setAttribute("adviceSearchResults", adviceSearchResults);
      AdviceHomeInfoVO adviceHomeInfoVO = (AdviceHomeInfoVO) adviceSearchResults.get(0);
      request.setAttribute("totalRecordCount", adviceHomeInfoVO.getTotalCount());
      totalCount = adviceHomeInfoVO.getTotalCount();      
    } else {        
      if (!GenericValidator.isBlankOrNull(keyword)) {
        //Get the trending and popular articles from the cursor and assigned into the request to show the front end for 11_Aug_2015, By Thiyagu G.
    	ArrayList tendingAndMostPopular = new ArrayList();
    	ArrayList trendingPopularAdviceList = (ArrayList)adviceSearchInfoMap.get("pc_trending_popular_articles");
    	if (trendingPopularAdviceList != null && trendingPopularAdviceList.size() > 0) {
    	  tendingAndMostPopular.addAll(trendingPopularAdviceList);               
    	}
    	ArrayList mostPopularAdviceList = (ArrayList)adviceSearchInfoMap.get("pc_most_popular_articles");
    	if (mostPopularAdviceList != null && mostPopularAdviceList.size() > 0) {
    	  tendingAndMostPopular.addAll(mostPopularAdviceList);               
    	}            
    	request.setAttribute("adviceSearchResults", tendingAndMostPopular);
    	request.setAttribute("searchkeyword", "notfound");
    	return new ModelAndView("advicesearchresults");
      }     
      if (keyword == null || "".equals(keyword)) {
        request.setAttribute("reason_for_404", "--(1)-- this page dont have keyword");
        response.sendError(404, "404 error message");
        return null;
      }
      if ((page == null || "".equals(page)) || Integer.parseInt(page) > 0) {
        request.setAttribute("reason_for_404", "--(1)-- this page dont have page number");
        response.sendError(404, "404 error message");
        return null;
      }
    }
    
    //Added SEM cursor by Sujitha V on 2021_JAN_19 rel 
    ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) adviceSearchInfoMap.get("PC_META_DETAILS");
    if(!CollectionUtils.isEmpty(seoMetaDetailsList)) {
      new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);
    }
    
    //16-Sep-2014 - Insight  
    request.setAttribute("insightPageFlag", "yes");
    if ("2".equals(cpeQualificationNetworkId)) {
      new GlobalFunction().getStudyLevelDesc("M", request);
    } else {
      new GlobalFunction().getStudyLevelDesc("L", request);   
    }
    /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
      new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
    } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    //
    request.setAttribute("curActiveMenu","advice");
    request.setAttribute("showResponsiveTimeLine","true");	
    return new ModelAndView("advicesearchresults");
  }
}
