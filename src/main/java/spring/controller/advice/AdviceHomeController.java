package spring.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoPageNamesAndFlags;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import spring.valueobject.seo.SEOMetaDetailsVO;
import WUI.utilities.CommonUtil;

/**
 * @AdviceHomeController
 * @since        wu333_20141209
 * @author       Thiyagu G
 * Change Log
 * ******************************************************************************************************
 * author	              Ver 	              Modified On     	     Modification Details 	                
 * ******************************************************************************************************
 * Sujitha V              1.1                 2021_JAN_19            Added SEM cursor(Meta Details)
 * 
*/ 

@Controller
public class AdviceHomeController {
	
  @RequestMapping(value = "/advice", method = {RequestMethod.GET, RequestMethod.POST})	
  public ModelAndView getAdviceHome(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());
    //-----------------------------------------------------------------------------------------------------------------------------------------
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    CommonUtil commonUtil = new CommonUtil();
    //Added getSchemeName by Indumathi Mar-29-16    
    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length()))); //this used for addThis
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    global.removeSessionData(session, request, response);
    if(session.getAttribute("gamKeywordOrSubject")!=null){session.removeAttribute("gamKeywordOrSubject");}
    //
    String userId = new SessionData().getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
    String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0)) ? common.checkCookieStatus(request) : "";
    basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
    userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------    
    //
    //loading ArticleGroupsHome details
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }
    String pageURL = common.getFullURL(request, false);
    pageURL = StringUtils.replaceEach(pageURL, new String[]{"/degrees", ".html"}, new String[]{"", "/"});
    
    List parameters = new ArrayList();
    parameters.add(GlobalConstants.WHATUNI_WEBSITE_ID);    
    parameters.add(userId);
    parameters.add(basketId);
    parameters.add(SeoPageNamesAndFlags.ARTICLE_GROUPS_HOME_PAGE_NAME); //page naem to fetch seo-meta-tag-descriptions
    parameters.add(SeoPageNamesAndFlags.ARTICLE_GROUPS_HOME_PAGE_FLAG); //page flag to alter seo-meta-tag-descriptions
    parameters.add(cpeQualificationNetworkId);
    parameters.add(pageURL);//Added for SEM
    IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
    Map adviceHomeInfoMap = adviceBusiness.getAdviceHomeInfo(parameters);
    
    //premium article list
    ArrayList adviceHomeEditorsPickList = (ArrayList)adviceHomeInfoMap.get("oc_editors_pick_feature_list");    
    if (adviceHomeEditorsPickList != null && adviceHomeEditorsPickList.size() > 0) {
      request.setAttribute("adviceHomeEditorsPickList", adviceHomeEditorsPickList);
    }
    
    ArrayList adviceHomeResearchPrepList = (ArrayList)adviceHomeInfoMap.get("oc_research_prep_feature_list");    
    if (adviceHomeResearchPrepList != null && adviceHomeResearchPrepList.size() > 0) {
      request.setAttribute("adviceHomeResearchPrepList", adviceHomeResearchPrepList);
    }
    
    ArrayList adviceHomeStudLifeList = (ArrayList)adviceHomeInfoMap.get("oc_stud_life_feature_list");    
    if (adviceHomeStudLifeList != null && adviceHomeStudLifeList.size() > 0) {
      request.setAttribute("adviceHomeStudLifeList", adviceHomeStudLifeList);
    }
    
    ArrayList adviceHomeBlogList = (ArrayList)adviceHomeInfoMap.get("oc_blog_feature_list");    
    if (adviceHomeBlogList != null && adviceHomeBlogList.size() > 0) {
      request.setAttribute("adviceHomeBlogList", adviceHomeBlogList);
    }
    
    ArrayList adviceHomeNewsList = (ArrayList)adviceHomeInfoMap.get("oc_news_feature_list");    
    if (adviceHomeNewsList != null && adviceHomeNewsList.size() > 0) {
      request.setAttribute("adviceHomeNewsList", adviceHomeNewsList);
    }
    
    ArrayList adviceHomeGuidesList = (ArrayList)adviceHomeInfoMap.get("oc_guides_feature_list");    
    if (adviceHomeGuidesList != null && adviceHomeGuidesList.size() > 0) {
      request.setAttribute("adviceHomeGuidesList", adviceHomeGuidesList);
    }
    
    ArrayList adviceHomeClearingList = (ArrayList)adviceHomeInfoMap.get("oc_clearing_feature_list");    
    if (adviceHomeClearingList != null && adviceHomeClearingList.size() > 0) {
      request.setAttribute("adviceHomeClearingList", adviceHomeClearingList);
    }
    
    String adviceHomeBreadCrumb = (String) adviceHomeInfoMap.get("o_bread_crumb");
    if (!GenericValidator.isBlankOrNull(adviceHomeBreadCrumb)) {
      request.setAttribute("adviceHomeBreadCrumb", adviceHomeBreadCrumb);
    }    
    //Added parents and accommodation category on 08_Aug_2017, By Thiyagu G
    ArrayList adviceHomeParentsList = (ArrayList)adviceHomeInfoMap.get("oc_parents_feature_list");    
    if (adviceHomeParentsList != null && adviceHomeParentsList.size() > 0) {
      request.setAttribute("adviceHomeParentsList", adviceHomeParentsList);
    }
    ArrayList adviceHomeAccommodationList = (ArrayList)adviceHomeInfoMap.get("oc_accommoadation_feature_list");    
    if (adviceHomeAccommodationList != null && adviceHomeAccommodationList.size() > 0) {
      request.setAttribute("adviceHomeAccommodationList", adviceHomeAccommodationList);
    }
  //Added teacher category on 04_Mar_2020, By Sri Sankari R
    ArrayList adviceHomeTeacherList = (ArrayList)adviceHomeInfoMap.get("OC_TEACHER_FEATURE_LIST");    
    if (!commonUtil.isBlankOrNull(adviceHomeTeacherList)) {
      request.setAttribute("adviceHomeTeacherList", adviceHomeTeacherList);
    }
    
    //Added SEM cursor by Sujitha V on 2021_JAN_19 rel 
    ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) adviceHomeInfoMap.get("PC_META_DETAILS");
    if(!CollectionUtils.isEmpty(seoMetaDetailsList)) {
      commonUtil.iterateMetaDetails(seoMetaDetailsList, model);
    }
	  
    request.setAttribute("insightPageFlag", "yes");
    if ("2".equals(cpeQualificationNetworkId)) {
      new GlobalFunction().getStudyLevelDesc("M", request);
    } else {
      new GlobalFunction().getStudyLevelDesc("L", request);   
    }
    
    /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
      new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
    } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    //
    request.setAttribute("curActiveMenu","advice");
    request.setAttribute("showResponsiveTimeLine","true");
    
    return new ModelAndView("advicehome");
  }
}
