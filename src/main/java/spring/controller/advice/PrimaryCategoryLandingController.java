package spring.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.valueobject.advice.AdviceHomeInfoVO;
import spring.valueobject.seo.SEOMetaDetailsVO;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import WUI.utilities.CommonUtil;;

/**
 * @PrimaryCategoryLandingController
 * @since        wu333_20141209
 * @author       Thiyagu G
 * Change Log
 * ******************************************************************************************************
 * author	              Ver 	              Modified On     	     Modification Details 	                
 * ******************************************************************************************************
 * Sujitha V              1.1                 2021_JAN_19            Added SEM cursor(Meta Details)
 * 
*/ 

@Controller
public class PrimaryCategoryLandingController {
	
  @RequestMapping(value = "/advice/*", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getPrimaryCategoryLanding(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap modelMap) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());
    //-----------------------------------------------------------------------------------------------------------------------------------------
    CommonFunction common = new CommonFunction();
    CommonUtil commonUtil = new CommonUtil();
    GlobalFunction global = new GlobalFunction();
    AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
    //Added getSchemeName by Indumathi Mar-29-16   
    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length()))); //this used for addThis
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    String priCat = "student-life";
    global.removeSessionData(session, request, response);
    if (session.getAttribute("gamKeywordOrSubject")!=null){session.removeAttribute("gamKeywordOrSubject");}
    //
    String userId = new SessionData().getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
    String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0)) ? common.checkCookieStatus(request) : "";
    basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
    userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------    
    String parentCategory = "";
    String page = (request.getParameter("page") != null) ? request.getParameter("page").trim() : "1";
    String pageName = (request.getParameter("pageName") != null) ? request.getParameter("pageName").trim() : "";        
    String urlArray[] = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "/").split("/");
    if (urlArray.length > 0) {
      parentCategory = urlArray[2];
    } 
    String findForwardString = "primarycategorylanding";
    //loading ArticleGroupsHome details
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) { 
      cpeQualificationNetworkId = "2";
    }
    
    String pageURL = common.getFullURL(request, false);
    pageURL = StringUtils.replaceEach(pageURL, new String[]{"/degrees", ".html"}, new String[]{"", "/"});
    
    List parameters = new ArrayList();    
    parameters.add(parentCategory.replaceAll("-"," ").toUpperCase());    
    parameters.add(page);
    parameters.add(pageURL); //Added for SEM
    IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
    Map adviceHomeInfoMap = adviceBusiness.getPrimaryCategoryLandingInfo(parameters);
    //premium article list
    ArrayList categoryArticleList = (ArrayList)adviceHomeInfoMap.get("OC_CATEGORY_ARTICLE_LIST"); 
    if (categoryArticleList != null && categoryArticleList.size() > 0) {
      request.setAttribute("categoryArticleList", categoryArticleList);        
      adviceHomeInfoVO = (AdviceHomeInfoVO) categoryArticleList.get(0);
      request.setAttribute("totalRecordCount", adviceHomeInfoVO.getTotalCount());
      request.setAttribute("page", page);
      request.setAttribute("pageName", pageName);
      request.setAttribute("mappingPath", request.getRequestURI().substring(request.getContextPath().length()).replace(".html", ""));
      request.setAttribute("linkHilight", parentCategory);
      request.setAttribute("parentCategory", parentCategory);
      session.setAttribute("pCategory", parentCategory);      
      if (!GenericValidator.isBlankOrNull(parentCategory) && "WELLBEING".equalsIgnoreCase(parentCategory)) {
        request.setAttribute("linkHilight", parentCategory);
        request.setAttribute("parentCategory", priCat);
        session.setAttribute("pCategory", priCat);
        request.setAttribute("secondaryCategory", adviceHomeInfoVO.getSubCategoryName());
        session.setAttribute("sCategory", parentCategory);
        findForwardString = "secondarycategorylanding";
      }
    } else {        
      request.setAttribute("reason_for_404", "--(1)-- this page dont have page number");
      response.sendError(404, "404 error message");
      return null;
    }
    String adviceLandingBreadCrumb = (String) adviceHomeInfoMap.get("o_bread_crumb");
    if (!GenericValidator.isBlankOrNull(adviceLandingBreadCrumb)) {
      request.setAttribute("adviceLandingBreadCrumb", adviceLandingBreadCrumb);
    }
    ArrayList<AdviceHomeInfoVO> covidSnippetList = (ArrayList) adviceHomeInfoMap.get("OC_DEFAULT_COVID_SNIPPET");
    if(!commonUtil.isBlankOrNull(covidSnippetList)) {
        adviceHomeInfoVO = (AdviceHomeInfoVO) covidSnippetList.get(0);
    	modelMap.addAttribute("totalRow", adviceHomeInfoVO.getCovidNewsTotalCount());
        modelMap.addAttribute("covidSnippetList", covidSnippetList);
        modelMap.addAttribute("pageNo", page);
    }
    
    //Added SEM cursor by Sujitha V on 2021_JAN_19 rel 
    ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) adviceHomeInfoMap.get("PC_META_DETAILS");
    if(!CollectionUtils.isEmpty(seoMetaDetailsList)) {
      commonUtil.iterateMetaDetails(seoMetaDetailsList, model);
    }
    
    request.setAttribute("insightPageFlag", "yes");
    /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
      new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
    } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    //
    request.setAttribute("curActiveMenu","advice");
    request.setAttribute("showResponsiveTimeLine","true");    
    return new ModelAndView(findForwardString);
  }	
}
