package spring.controller.advice;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.valueobject.advice.AdviceHomeInfoVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ArticlesPodsAction.java
 * Description   : This class is used to load the suggested uni/course pods and friends activity pod..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Thiyagu G.              1.0             13.06.2017       First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
@Controller
public class ArticlesPodsController {
  
  @RequestMapping(value = "/get-articles-pod", method = RequestMethod.POST)	
  public ModelAndView getArticlesPod(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    CommonFunction common = new CommonFunction();
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    String forwardPath = "articles-pod";
    SessionData sessionData = new SessionData();
    String userId = sessionData.getData(request, "y");
    String userTrackId = sessionData.getData(request, "userTrackId");
    String pagename = request.getParameter("pagename");
    String oppid = !GenericValidator.isBlankOrNull(request.getParameter("oppid")) ? request.getParameter("oppid") : null;
    AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();    
    IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
    adviceHomeInfoVO.setUserId(userId);
    adviceHomeInfoVO.setUserTrackId(userTrackId);
    adviceHomeInfoVO.setPageName(pagename);
    adviceHomeInfoVO.setOpportunityId(oppid);
    adviceHomeInfoVO.setPostId(null);
    adviceHomeInfoVO.setPrimaryCategoryName(null);
    try{    
      Map resultMap = adviceBusiness.getArticlesPod(adviceHomeInfoVO);
      if (resultMap != null) {
        ArrayList articlesList = (ArrayList)resultMap.get("PC_ARTICLE_POD");
        if (articlesList != null && articlesList.size() > 0) {
          request.setAttribute("articlesList", articlesList);
          request.setAttribute("articlePodPageName", pagename);
          if("HOMEPAGE".equals(pagename)){
            forwardPath = "home-articles-pod";
          }
        }
      }   
    }catch(Exception e){
      e.printStackTrace();
    }
    return new ModelAndView(forwardPath);
  }
}
