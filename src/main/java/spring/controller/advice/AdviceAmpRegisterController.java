package spring.controller.advice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.GlobalConstants;

@Controller
public class AdviceAmpRegisterController {
	
  @RequestMapping(value = "/register", method = {RequestMethod.GET, RequestMethod.POST})	
  public ModelAndView getAdviceAmpRegister(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String ampPageUrl = request.getHeader("referer");
    request.setAttribute("ampPageUrl",  !GenericValidator.isBlankOrNull(ampPageUrl) ?  ampPageUrl : GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName());
    return new ModelAndView("amp-register-page");
  }
}
