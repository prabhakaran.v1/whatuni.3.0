package spring.controller.advice;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import WUI.utilities.CommonUtil;
import spring.valueobject.advice.AdviceHomeInfoVO;

@Controller
public class CovidSnippetAjaxController {

	 @RequestMapping(value = "/ajax/covid-snippet", method = RequestMethod.POST)
	  public ModelAndView reviewAjax(HttpServletRequest request, ModelMap modelMap) throws Exception {
	    
		IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
	    AdviceHomeInfoVO adviceHomeInfoVO = new AdviceHomeInfoVO();
	    CommonUtil commonUtil = new CommonUtil();
	    String pageNo = request.getParameter("pageNo");
	    adviceHomeInfoVO.setPageNo(pageNo);
	    Map adviceHomeInfoMap = adviceBusiness.getCovidSnippetPod(adviceHomeInfoVO);
	    if(!commonUtil.isBlankOrNull(adviceHomeInfoMap)) {
	    	 ArrayList<AdviceHomeInfoVO> covidSnippetList = (ArrayList) adviceHomeInfoMap.get("OC_COVID_SNIPPET");
	    	    if(!commonUtil.isBlankOrNull(covidSnippetList)) {
	    	        adviceHomeInfoVO = (AdviceHomeInfoVO) covidSnippetList.get(0);
	    	    	modelMap.addAttribute("totalRow", adviceHomeInfoVO.getCovidNewsTotalCount());
	    	        modelMap.addAttribute("covidSnippetList", covidSnippetList);
	    	        modelMap.addAttribute("pageNo", pageNo);
	    	    }
	    }
	    return new ModelAndView("covidSnippetAjax");
	  }
}
