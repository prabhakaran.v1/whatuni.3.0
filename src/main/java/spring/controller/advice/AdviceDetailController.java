package spring.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import WUI.utilities.GlobalConstants;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import spring.helper.articles.ArticleHelper;
import spring.valueobject.advice.AdviceDetailInfoVO;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

@Controller
public class AdviceDetailController {
	
  @RequestMapping(value = {"/advice/*/*/*", "/blog-article/preview"}, method = {RequestMethod.GET, RequestMethod.POST})	
  public ModelAndView getAdviceDetail(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());
    //-----------------------------------------------------------------------------------------------------------------------------------------
    // session = request.getSession();
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    String currentPageUrl = "";
    String articleURLName = "";
    String redirectURL = null;
    //Added getSchemeName by Indumathi Mar-29-16    
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    global.removeSessionData(session, request, response);
    if (session.getAttribute("gamKeywordOrSubject") != null) {
      session.removeAttribute("gamKeywordOrSubject");
    }
    //
    /* ::START:: Yogeswari :: 30.06.2015 :: changed for UCAS CALCULATOR task*/
    String urlArray[] = URL_STRING.split("/");
    if (urlArray.length > 3) {
      articleURLName = urlArray[3];
      request.setAttribute("ucasArticleLinkHilight", articleURLName);
    }
    /* ::END:: Yogeswari :: 30.06.2015 :: changed for UCAS CALCULATOR task */
    //
    String userId = new SessionData().getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
    String basketId = "";
    //basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
    //userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = null;
    }
    
    basketId = common.checkCookieStatus(request);
    if (GenericValidator.isBlankOrNull(basketId) && "0".equals(basketId)) {
      basketId = null;
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------    
    //
    //loading ArticleGroupsHome details
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    String parentCategory = session.getAttribute("pCategory") != null ? String.valueOf(session.getAttribute("pCategory")) : null;
    String subCategory = session.getAttribute("sCategory") != null ? String.valueOf(session.getAttribute("sCategory")) : null;
    String primaryCategory = null;
    String postUrl = null;
    String postId = null;
    String previewFlag = null;
    boolean articlePrvw = false;
    String findForwardString = "advicedetail";
    if (urlArray.length == 3) {
      if ("preview".equals(urlArray[2])) {
        articlePrvw = true;
      }
    }
    String urlPrimaryCategory = "";
    String urlPostUrl = "";
    if (articlePrvw) {
      if (request.getParameter("p_post_id") != null && !"".equals(request.getParameter("p_post_id"))) {
        postId = request.getParameter("p_post_id");
        previewFlag = "Y";
        request.setAttribute("articlePrvw", "true");
        session.setAttribute("noSplashpopup", "true");
      }
    } else {
      if (urlArray.length > 0) {
        primaryCategory = urlArray[2];
        postUrl = urlArray[3];
        postId = urlArray[4];
        postId = postId.replace(".html","");
      }
      urlPrimaryCategory = primaryCategory;
      urlPostUrl = postUrl;
      postUrl = postUrl.replaceAll("-", " ");
      ArticleHelper articleHelper = new ArticleHelper();
      if (!articleHelper.isThisArticleAvailable(postUrl)) { // reidrecting 404 if SEO_TITLE changes in table
        request.setAttribute("reason_for_404", "--(1)--> This SEO_TITLE in URL & Table didn't match");
        response.sendError(404, "404 error message");
        return null;
      }
    }
    IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
    List parameters = new ArrayList();
    parameters.add(postId); //p_post_id    
    if (parentCategory != null && !"".equals(parentCategory)) {
      parentCategory = parentCategory.replaceAll("-", " ").toUpperCase();
    }
    if (subCategory != null && !"".equals(subCategory)) {
      subCategory = subCategory.replaceAll("-", " ").toUpperCase();
    }
    if (primaryCategory != null && !"".equals(primaryCategory)) {
      primaryCategory = primaryCategory.replaceAll("-", " ").toUpperCase();
    }    
    AdviceDetailInfoVO adviceInfoVO = new AdviceDetailInfoVO();
    adviceInfoVO.setPostId(postId);
    if (parentCategory == null && subCategory == null) {
      adviceInfoVO.setPrimaryCategoryName(primaryCategory);     
    } else {
      adviceInfoVO.setPrimaryCategoryName(parentCategory);     
    }
    if (!GenericValidator.isBlankOrNull(URL_STRING)){
        if(!articleURLName.equals(urlArray[3].toLowerCase())) {
          redirectURL = new SeoUrls().constructAdviceSeoUrl(primaryCategory, urlPostUrl, postId).toLowerCase();
          response.setStatus(response.SC_MOVED_PERMANENTLY);
          response.setHeader("Location", redirectURL);
          response.setHeader("Connection", "close");
          return null;
        }
      }
      currentPageUrl =  common.getSchemeName(request) + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH + URL_STRING + "/"; 
      request.setAttribute("currentPageUrl", currentPageUrl); //this used for addThis
    adviceInfoVO.setSubCategoryName(subCategory);    
    
    //Added 301 redirection for primary category namefor 21-02-2017, By Thiyagu G.
    Map adviceInfoMap = adviceBusiness.checkArticleAndPrimaryCategoryAvilable(adviceInfoVO);    
    String primaryCategoryName = (String)adviceInfoMap.get("P_PRIMARY_CATEGORY_NAME");
    if (!GenericValidator.isBlankOrNull(urlPrimaryCategory) && !GenericValidator.isBlankOrNull(primaryCategoryName)) {
      primaryCategoryName = new CommonUtil().replaceSpaceByHypen(new CommonFunction().replaceSpecialCharacter(primaryCategoryName)).toLowerCase();
      if (!urlPrimaryCategory.equals(primaryCategoryName)) {
        redirectURL = new SeoUrls().constructAdviceSeoUrl(primaryCategoryName, urlPostUrl, postId).toLowerCase();
        response.setStatus(response.SC_MOVED_PERMANENTLY);
        response.setHeader("Location", redirectURL);
        response.setHeader("Connection", "close");
        return null;
      }         
    }
    String articleCheck = adviceInfoMap.get("RetVal") != null ? (String)adviceInfoMap.get("RetVal") : "FALSE";
    if ("TRUE".equals(articleCheck)) {
      parameters.add(parentCategory); //p_parent_category_name     
      parameters.add(subCategory); //p_sub_category_name    
    } else {
      parameters.add(null); //p_parent_category_name is null
      parameters.add(null); //p_sub_category_name is null
    }
    parameters.add(primaryCategory); //p_parent_category_name
    request.setAttribute("primaryCategory", primaryCategory); //Added by Prabha on 03_NOV_2015_REL
    if ("".equals(userId)) {
      userId = "0";
    }
    parameters.add(userId); //p_user_id    
    if ("".equals(basketId)) {
      basketId = null;
    }
    parameters.add(basketId); //p_basket_id
    parameters.add(cpeQualificationNetworkId); //p_network_id
    parameters.add(previewFlag); //p_preview_flag     
    String viewedPostIds = "";
    if (session.getAttribute("viewedPostIdsList") != null && !"".equals(session.getAttribute("viewedPostIdsList"))) {
      if (!session.getAttribute("viewedPostIdsList").toString().contains(postId)) {
        viewedPostIds = session.getAttribute("viewedPostIdsList").toString() + ", " + postId;
      }
      session.setAttribute("viewedPostIdsList", viewedPostIds);
    } else {
      session.setAttribute("viewedPostIdsList", postId);
    }
    parameters.add(new SessionData().getData(request, "userTrackId")); // Yogeswari :: 19.05.2015 :: Added  tracking_session_id.
    parameters.add("true".equals(request.getParameter("amp")) ? "Y" : "N"); //Added param for AMP page by Prabha on 21_02_17
    Map adviceDetailInfoMap = adviceBusiness.getAdviceDetailInfo(parameters);
    //premium article details
    ArrayList adviceDetailInfoList = (ArrayList)adviceDetailInfoMap.get("OC_ARTICLE_DETAILS_INFO");
    if (adviceDetailInfoList != null && adviceDetailInfoList.size() > 0) {
      request.setAttribute("adviceDetailInfoList", adviceDetailInfoList);
      AdviceDetailInfoVO adviceDetailInfoVO = (AdviceDetailInfoVO)adviceDetailInfoList.get(0);
      request.setAttribute("metaTitle", adviceDetailInfoVO.getMetaTitle());
      request.setAttribute("metaKeywords", adviceDetailInfoVO.getMetaKeywords());
      request.setAttribute("metaDescription", adviceDetailInfoVO.getMetaDescription());
      request.setAttribute("ogImage", adviceDetailInfoVO.getImageName());
      request.setAttribute("articleId", postId);
      request.setAttribute("parentCategory", parentCategory);
      request.setAttribute("secondaryCategory", subCategory);
    } else {
      if (articlePrvw) {
        request.setAttribute("noArticleFlag", "Preview");
      }
      findForwardString = "nopreview";
    }
    ArrayList mostPopularAdviceList = (ArrayList)adviceDetailInfoMap.get("OC_MOST_POPULAR_ARTICLES");
    if (mostPopularAdviceList != null && mostPopularAdviceList.size() > 0) {
      request.setAttribute("mostPopularAdviceList", mostPopularAdviceList);
    }
    ArrayList trendingPopularAdviceList = (ArrayList)adviceDetailInfoMap.get("OC_TRENDING_POPULAR_ARTICLES");
    if (trendingPopularAdviceList != null && trendingPopularAdviceList.size() > 0) {
      request.setAttribute("trendingPopularAdviceList", trendingPopularAdviceList);
    }
    ArrayList otherCoursesAdviceList = (ArrayList)adviceDetailInfoMap.get("oc_other_courses");
    if (otherCoursesAdviceList != null && otherCoursesAdviceList.size() > 0) {
      request.setAttribute("otherCoursesAdviceList", otherCoursesAdviceList);
    }
    ArrayList univsInCityAdviceList = (ArrayList)adviceDetailInfoMap.get("oc_univs_in_city");
    if (univsInCityAdviceList != null && univsInCityAdviceList.size() > 0) {
      request.setAttribute("univsInCityAdviceList", univsInCityAdviceList);
    }
    List similarArticleList = (List)adviceDetailInfoMap.get("oc_similar_article"); //13_JAN_2015 Added by Amir to show similar article pod
    if (similarArticleList != null && similarArticleList.size() > 0) {
      request.setAttribute("similarArticleList", similarArticleList);
    }
	String adviceDetailBreadCrumb = (String)adviceDetailInfoMap.get("o_bread_crumb");
	if (!GenericValidator.isBlankOrNull(adviceDetailBreadCrumb)) {
	  request.setAttribute("adviceDetailBreadCrumb", adviceDetailBreadCrumb);
	}
	String clCourseFlag = (String)adviceDetailInfoMap.get("o_cl_course_flag");
	if (!GenericValidator.isBlankOrNull(clCourseFlag)) {
	  request.setAttribute("clCourseFlag", clCourseFlag);
	}
	String browseSearchUrl = (String)adviceDetailInfoMap.get("o_browse_search_url");
	if (!GenericValidator.isBlankOrNull(browseSearchUrl)) {
	  request.setAttribute("browseSearchUrl", browseSearchUrl);
	}
	String tags = (String)adviceDetailInfoMap.get("o_tags");
    if (!GenericValidator.isBlankOrNull(tags)) {
      request.setAttribute("tags", tags);
    }
    //
    ArrayList sponsorDetailList = (ArrayList)adviceDetailInfoMap.get("pc_sponsors_details"); //9_June_2015_rel
    if (sponsorDetailList != null && sponsorDetailList.size() > 0) {
      AdviceDetailInfoVO sponsorDetailVO = (AdviceDetailInfoVO)sponsorDetailList.get(0);
      request.setAttribute("sponsPixelTracking",sponsorDetailVO.getSponsPixelTracking());//Added pixel tracking for sponsor articles by Hema.S on NOV_20_2018_REL
      request.setAttribute("sponsorDetailList", sponsorDetailList);
    }
    //Added top articles cursor for amp page on 13_June_2017. By Thiyagu G.
    ArrayList topArticlesList = (ArrayList)adviceDetailInfoMap.get("oc_top_article"); 
    if (topArticlesList != null && topArticlesList.size() > 0) {
      request.setAttribute("articlesList", topArticlesList);
    }
    // 
    request.setAttribute("mappingPath", URL_STRING);
    request.setAttribute("insightPageFlag", "yes");
    if ("2".equals(cpeQualificationNetworkId)) {
      new GlobalFunction().getStudyLevelDesc("M", request);
    } else {
      new GlobalFunction().getStudyLevelDesc("L", request);
    }
    //if(session.getAttribute("pCategory")!=null){session.removeAttribute("pCategory");}
    //if(session.getAttribute("sCategory")!=null){session.removeAttribute("sCategory");}
    /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
      new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
    } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
    //
    request.setAttribute("curActiveMenu", "advice");
    request.setAttribute("showResponsiveTimeLine", "true");
    
    //Added code for AMP page by Prabha on 21_Feb_17 
    if ("true".equals(request.getParameter("amp"))) {
      findForwardString = findForwardString + "-amp-page";
    }
    //End of AMP code
    return new ModelAndView(findForwardString);
  }
  public String checkArticleAvilable(String postId, String parentCategory, String subCategory) {
    DataModel dataModel = new DataModel();
    String articleCheck = "";
    try {
      Vector artVector = new Vector();
      artVector.clear();
      artVector.add(postId); // postId
      artVector.add(parentCategory); // parentCategory 
      artVector.add(subCategory); // subCategory
      articleCheck = dataModel.getString("hot_wuni.wuni_articles_pkg.check_article_avilable_fn", artVector);
    } catch (Exception hitException) {
      hitException.printStackTrace();
    }
    return articleCheck; 
  }
}
