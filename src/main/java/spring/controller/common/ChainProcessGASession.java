package spring.controller.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ChainProcessGASession
 * @author   Sujitha V
 * @version  1.0
 * @since    18_08_2020
 * @purpose  This class is used to set and remove session values for chain process GA logging(SR->PR->CD->Profile->Interactions)
 * Change Log
 * *******************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *******************************************************************************************************************************
 * 
*/

@Controller
public class ChainProcessGASession {

  @RequestMapping(value="/chain-process-ga-session")
  public ModelAndView getChainProcessGASession(ModelMap model, @RequestParam(value = "sponsoreTypeFlag") String sponsoreTypeFlag, @RequestParam(value = "sponsoredTypeInstitutionId") String sponsoredTypeInstitutionId, HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {

	if(StringUtils.isNotBlank(sponsoredTypeInstitutionId) && StringUtils.isNotBlank(sponsoreTypeFlag)) {
      if(!"none".equalsIgnoreCase(sponsoreTypeFlag)){
        if("sponsored".equalsIgnoreCase(sponsoreTypeFlag)){
          session.setAttribute("sponsoredInstId", sponsoredTypeInstitutionId);
        //model.addAttribute("sponsoredListFlag", "SPONSORED_LISTING");
	   }else{
		 session.removeAttribute("sponsoredInstId");
	   } 
     
       if("manual".equalsIgnoreCase(sponsoreTypeFlag)){
         session.setAttribute("boostedInstId", sponsoredTypeInstitutionId); 
         //model.addAttribute("manualBoostingFlag", "MANUAL_BOOSTING");
	   }else{
	     session.removeAttribute("boostedInstId");
	   }  
     }
	}
   return null;		
  }
}
