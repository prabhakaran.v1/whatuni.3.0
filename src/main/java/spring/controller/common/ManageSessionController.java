package spring.controller.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

/**
*
* ManageSessionController.java
* @Version : 2.0
* @www.whatuni.com
* @Purpose  : This program is used to manage Session attribute.
* *************************************************************************************************************************
* Date           Name                      Ver.     Changes desc                                                 Rel Ver.
* *************************************************************************************************************************
*  10.03.20		Sangeeth.S							initial draft												wu_31032020
*/
@Controller
public class ManageSessionController {	
  @RequestMapping(value="/manage-session", method=RequestMethod.POST)	
  public ModelAndView manageSessionAttributes(HttpServletRequest request, HttpServletResponse response) throws Exception {
	SessionData sessionData = new SessionData();
	String action =  request.getParameter("action");    
    String latitude = request.getParameter("latitude");
	String longitude = request.getParameter("longitude");   
    if("add-geolocation".equalsIgnoreCase(action) && !GenericValidator.isBlankOrNull(latitude) && !GenericValidator.isBlankOrNull(longitude)){      
	  String latAndLong = latitude + GlobalConstants.SPLIT_CONSTANT + longitude;	  
	  String prevLatAndLongVal = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG);
	  if(!latAndLong.equalsIgnoreCase(prevLatAndLongVal)) {
	    sessionData.addData(request, response, GlobalConstants.SESSION_KEY_LAT_LONG, latAndLong);	    
      }
	  sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG);
	  response.setContentType("text/html");
	  response.setHeader("Cache-Control", "no-cache");   
	  response.getWriter().write("SUCEESS");      
    }
    return null;
  }
}
