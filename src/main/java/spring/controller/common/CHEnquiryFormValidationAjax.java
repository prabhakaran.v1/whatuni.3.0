package spring.controller.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.layer.util.SpringConstants;

/**
 * @ChainProcessGASession
 * @author   Sujitha V
 * @version  1.0
 * @since    17_09_2020
 * @purpose  This class is used return the error messages for content hub enquiry form.
 * Change Log
 * *******************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *******************************************************************************************************************************
 * 
*/


@Controller
public class CHEnquiryFormValidationAjax {

  @RequestMapping(value="/enquiry-form-validation-ajax", method = {RequestMethod.GET, RequestMethod.POST})
  public void getCHEnquiryFormValidationAjax(HttpServletResponse response, @RequestParam(value = "enquiryBoxField", required=false) String enquiryBoxField, @RequestParam(value = "postcodeBoxField", required=false) String postcodeField) throws Exception {
	  
	 String returnFlag = "";
	  if(StringUtils.isNotBlank(enquiryBoxField)){
		if(enquiryBoxField.length() < SpringConstants.FIFTY) {
		  returnFlag = "enquiryMessageError";
		}else {
		  returnFlag = "enquiryMessageNoErrors";
		}
	  }
	  
	  if(StringUtils.isNotBlank(postcodeField)) {
	    final Pattern alphaNumericPattern = Pattern.compile(SpringConstants.ALPHA_NUMERIC_WITH_SPACE);
		Matcher alphaNumberiValidation = alphaNumericPattern.matcher(postcodeField);		 
		if(!alphaNumberiValidation.matches()){
		  returnFlag = "postcodeError";
		}else {
		  returnFlag = "postcodeNoErrors";
		}
	  }
	response.getWriter().println(returnFlag);
  }
}
