package spring.controller.ourbirthday;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WUBirthdayController {
	
  @RequestMapping(value = "/our-birthday", method = RequestMethod.GET)	
  public ModelAndView getOurBirthdayPage(ModelMap model,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    return new ModelAndView("wu-birthday-page");
  }
}
