
package spring.controller.clearing;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.layer.util.SpringConstants;
import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;
import spring.business.IRestBusiness;
import spring.pojo.clearing.common.GradeFilterRequest;
import spring.pojo.clearing.common.GradeFilterResponse;

@Controller
public class GetGradeFilterInfoController {

	@Autowired
	IRestBusiness restBusiness = null;

	
	CommonUtil commonUtil = new CommonUtil();

	SessionData sessionData = new SessionData();
	// private static final Logger LOGGER=Logger.getLogger(GetGradeFilterInfoController.class);

	@RequestMapping(value = { "/get-grade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView getGradeFilterInfo(GradeFilterRequest gradeFilterInputBean, HttpServletRequest request, HttpServletResponse	response, HttpSession session) throws JsonParseException, JsonMappingException, IOException { 
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ?	sessionData.getData(request, "y") : "0";
		String subjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		gradeFilterInputBean.setQualId(StringUtils.isNotBlank(gradeFilterInputBean.getQualId()) ? gradeFilterInputBean.getQualId() : null);
		gradeFilterInputBean.setUserId(userId);
		gradeFilterInputBean.setUserTrackSessionId(subjectSessionId);
		ObjectMapper mapper = new ObjectMapper();
		String url = commonUtil.generateWebServiceUrl(SpringConstants.MAPPING_FOR_GET_GRADE_FILTER_INFO_AJAX); 
		//System.out.println("Web service url for get the values for grade filter  dropdown " +url); 
		String jsonInString = mapper.writeValueAsString(gradeFilterInputBean);
		//System.out.println("jsonInString----" +jsonInString);
		String result = restBusiness.callWebServices(url, jsonInString);
		//System.out.println("result----" +result);    
		GradeFilterResponse gradeFilterOutputBean = mapper.readValue(result,GradeFilterResponse.class);
		if (gradeFilterOutputBean.getGradeFilterList() != null && gradeFilterOutputBean.getGradeFilterList().size() > 0) {
			gradeFilterOutputBean.getGradeFilterList().stream().forEach(grdList -> {
				if("14".equals(grdList.getQualId())){
					grdList.setQualification("IB (Diploma) Higher Level");
			    }else if("15".equals(grdList.getQualId())){
			    	grdList.setQualification("IB (Diploma) Standard Level");
			    }				
				
			});		   	 
		}
		request.setAttribute("gradeFilterList",	gradeFilterOutputBean.getGradeFilterList()); 
		subjectSessionId = gradeFilterOutputBean.getUserTrackSessionId(); 
		session.setAttribute("subjectSessionId" , subjectSessionId);
		request.setAttribute("qualId",	gradeFilterInputBean.getQualId()); 
		request.setAttribute("userStudyLevel",gradeFilterOutputBean.getUserStudyLevelEntry()); 
		request.setAttribute("userEntryPoints",	gradeFilterOutputBean.getUserEntryPoint());
		request.setAttribute("userUcasPoints",	gradeFilterOutputBean.getUcasPoint());
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag"));
		request.setAttribute("filterHitCount",request.getParameter("filterHitCount"));
		if("Y".equalsIgnoreCase(request.getParameter("addQualFlag")) || StringUtils.isNotBlank(gradeFilterInputBean.getQualId())){
			//System.out.println("addQUalFlag" + request.getParameter("studyLevel"));
			request.setAttribute("studyLevel", ""); 
			request.setAttribute("entryPoints",	"");
			if("Y".equalsIgnoreCase(request.getParameter("removeFlag")) && StringUtils.isNotBlank(request.getParameter("studyLevel")) && StringUtils.isNotBlank(request.getParameter("studyLevel"))){
				request.setAttribute("studyLevel", request.getParameter("studyLevel")); 
				request.setAttribute("entryPoints",	request.getParameter("entryPoints"));
			}
		}else{
			request.setAttribute("studyLevel",gradeFilterOutputBean.getUserStudyLevelEntry()); 
			request.setAttribute("entryPoints",	gradeFilterOutputBean.getUserEntryPoint());	
		}		
		//request.setAttribute("studyLevel",request.getParameter("studyLevel")); 
		//request.setAttribute("entryPoints",	request.getParameter("entryPoints"));
		request.setAttribute("studyLevelBanner", request.getParameter("studyLevel"));
		return new ModelAndView("form-grade-filter-section");
	}

	@RequestMapping(value = { "/save-grade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView saveGradeFilterInfo(GradeFilterRequest gradeFilterInputBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException { 
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ?	sessionData.getData(request, "y") : "0";
		String userSubjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		String userStudyLevel =	StringUtils.isNotBlank(gradeFilterInputBean.getUserStudyLevelEntry()) ? gradeFilterInputBean.getUserStudyLevelEntry() : request.getParameter("studyLevel");
		String userEntryPoint =	StringUtils.isNotBlank(gradeFilterInputBean.getUserEntryPoint()) ? gradeFilterInputBean.getUserEntryPoint() :  request.getParameter("entryPoints");
		String userUCASPoint = StringUtils.isNotBlank(gradeFilterInputBean.getUcasPoint()) ? gradeFilterInputBean.getUcasPoint() :  request.getParameter("ucasPoint");
		gradeFilterInputBean.setQualId(StringUtils.isNotBlank(gradeFilterInputBean.getQualId()) ? gradeFilterInputBean.getQualId() : null);
		gradeFilterInputBean.setUserId(userId);
		gradeFilterInputBean.setUserTrackSessionId(userSubjectSessionId);//p_subject_session
		gradeFilterInputBean.setUserStudyLevelEntry(userStudyLevel);
		gradeFilterInputBean.setUserEntryPoint(userEntryPoint);
		if(StringUtils.isNotBlank(userUCASPoint)){       
	      session.setAttribute("USER_UCAS_SCORE", userUCASPoint);
	    }
		ObjectMapper mapper = new ObjectMapper();
		String url = commonUtil.generateWebServiceUrl(SpringConstants.MAPPING_FOR_SAVE_GRADE_FILTER_INFO_AJAX);
		//System.out.println("Web service url for save the values for grade filter  dropdown " +url); 
		String jsonInString = mapper.writeValueAsString(gradeFilterInputBean);
		//System.out.println("jsonInString----" +jsonInString);
		String result = restBusiness.callWebServices(url, jsonInString);
		//System.out.println("result----" +result);    
		GradeFilterResponse gradeFilterOutputBean = mapper.readValue(result, GradeFilterResponse.class); 
		//request.setAttribute("gradeFilterList", gradeFilterOutputBean.getGradeFilterList()); 
		request.setAttribute("qualId",gradeFilterInputBean.getQualId()); 
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag")); 
		request.setAttribute("filterHitCount", request.getParameter("filterHitCount"));
		request.setAttribute("studyLevel",userStudyLevel); 
		request.setAttribute("entryPoints", userEntryPoint);
		request.setAttribute("studyLevelBanner", userStudyLevel); 
		String returnString	= ""; 
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/html"); response.setHeader("Cache-Control","no-cache"); returnString = "save"; returnString += "##SPLIT##";
		buffer.append(returnString); 
		CommonUtil.setResponseText(response, buffer);
		return null; 
	}
	
	@RequestMapping(value = { "/delete-grade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView deleteGradeFilterInfo(GradeFilterRequest gradeFilterInputBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException { 
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ? sessionData.getData(request, "y") : "0";
		String userSubjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		String userStudyLevel =	request.getParameter("studyLevel");
		String userEntryPoint =	request.getParameter("entryPoints");
		gradeFilterInputBean.setQualId(StringUtils.isNotBlank(gradeFilterInputBean.getQualId()) ? gradeFilterInputBean.getQualId() : null);
		gradeFilterInputBean.setUserId(userId);
		gradeFilterInputBean.setUserTrackSessionId(userSubjectSessionId);//p_subject_session
		gradeFilterInputBean.setUserStudyLevelEntry(userStudyLevel);
		gradeFilterInputBean.setUserEntryPoint(userEntryPoint);
		ObjectMapper mapper = new ObjectMapper();
		String url = commonUtil.generateWebServiceUrl(SpringConstants.MAPPING_FOR_DELETE_GRADE_FILTER_INFO_AJAX);
		//System.out.println("Web service url for delete the values for grade filter  dropdown " +url); 
		String jsonInString = mapper.writeValueAsString(gradeFilterInputBean);
		String result = restBusiness.callWebServices(url, jsonInString);
		GradeFilterResponse gradeFilterOutputBean = mapper.readValue(result, GradeFilterResponse.class); 
		request.setAttribute("retVal", gradeFilterOutputBean.getRetVal()); 
		request.setAttribute("qualId",gradeFilterInputBean.getQualId()); 
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag")); 
		request.setAttribute("filterHitCount", request.getParameter("filterHitCount"));
		request.setAttribute("studyLevel",userStudyLevel); 
		request.setAttribute("entryPoints", userEntryPoint);
		request.setAttribute("studyLevelBanner", userStudyLevel);
		if (session.getAttribute("USER_UCAS_SCORE") != null) {
	        session.removeAttribute("USER_UCAS_SCORE");
	    }
		String returnString	= ""; 
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/html"); response.setHeader("Cache-Control","no-cache"); returnString = "save"; returnString += "##SPLIT##";
		buffer.append(returnString); 
		CommonUtil.setResponseText(response, buffer);
		return null; 
	}
}
