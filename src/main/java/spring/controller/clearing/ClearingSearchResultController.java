package spring.controller.clearing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.layer.util.SpringConstants;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.OmnitureProperties;
import WUI.utilities.SessionData;
import mobile.util.MobileUtils;
import spring.business.IRestBusiness;
import spring.pojo.clearing.ajax.UniversityAjaxRequest;
import spring.pojo.clearing.common.AcceptanceResponse;
import spring.pojo.clearing.common.FeaturedBrandResponse;
import spring.pojo.clearing.common.LocationTypeResponse;
import spring.pojo.clearing.common.QualificationResponse;
import spring.pojo.clearing.common.StatsResponse;
import spring.pojo.clearing.common.StudyModeResponse;
import spring.pojo.clearing.search.RegionResponse;
import spring.pojo.clearing.search.SRCourseListResponse;
import spring.pojo.clearing.search.SearchCollegeIdsResponse;
import spring.pojo.clearing.search.SearchInstitutionIdsResponse;
import spring.pojo.clearing.search.SearchResultsContentResponse;
import spring.pojo.clearing.search.SearchResultsRequest;
import spring.pojo.clearing.search.SearchResultsResponse;
import spring.pojo.clearing.search.UniAjaxListResponse;
import spring.pojo.clearing.search.UniAjaxResponse;
import spring.valueobject.search.OmnitureLoggingVO;

@Controller
public class ClearingSearchResultController {
  
  @Autowired
  IRestBusiness restBusiness = null;
    
  @SuppressWarnings("unused")
  @RequestMapping(value = "/clearing/search-results", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView searchResultsLanding(@RequestParam Map<String, String> requestParam, ModelMap model, SearchResultsRequest searchResultsInputBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException, Exception {
    CommonUtil utilities = new CommonUtil();
    CommonFunction common = new CommonFunction();
    ObjectMapper mapper = new ObjectMapper();
    SeoUrls seoUrls = new SeoUrls();
    String url = utilities.generateWebServiceUrl(SpringConstants.MAPPING_FOR_SEARCH_RESULTS);
    
    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    final String URL_QUR_STRING = request.getQueryString();
    String urlArray[] = URL_STRING.split("/");
    String filterparameters = (StringUtils.isNotBlank(URL_QUR_STRING) ? "?" + URL_QUR_STRING : "");
    String currentPageUrl = common.getSchemeName(request) + GlobalConstants.WHATUNI_DOMAIN + URL_STRING + filterparameters;
    String pageNo = StringUtils.isNotBlank((String) requestParam.get("pageno")) ? requestParam.get("pageno") : "1";
    String URL_1 = "";
    String URL_2 = "";
    String firstPageSeoUrl = "";  
    String SubjGradesSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
    String p_subject = StringUtils.isNotBlank((String) requestParam.get("subject")) ? requestParam.get("subject") : "";
    String searchKeyword = StringUtils.isNotBlank((String) requestParam.get("q")) ? requestParam.get("q") : null;
    String scoreValue = !GenericValidator.isBlankOrNull((String) requestParam.get("score")) ? (String) requestParam.get("score") : null;
    String location = GenericValidator.isBlankOrNull((String) requestParam.get("location")) ? "united-kingdom" : ((String) requestParam.get("location"));
     
    String searchHow = !GenericValidator.isBlankOrNull(requestParam.get("sort"))? requestParam.get("sort"): "R"; 
    String locationType = GenericValidator.isBlankOrNull((String) requestParam.get("location-type")) ? null : ((String) requestParam.get("location-type"));
    String campusType = GenericValidator.isBlankOrNull((String) requestParam.get("campus-type")) ? null : ((String) requestParam.get("campus-type"));
    String studyMode = GenericValidator.isBlankOrNull((String) requestParam.get("study-mode")) ? null : ((String) requestParam.get("study-mode"));
    String accommodationFlag = GenericValidator.isBlankOrNull((String) requestParam.get("accommodation")) ? null : ((String) requestParam.get("accommodation"));
    String scholarshipFlag = GenericValidator.isBlankOrNull((String) requestParam.get("scholarship")) ? null : ((String) requestParam.get("scholarship"));
    String hybridFlag = GenericValidator.isBlankOrNull((String) requestParam.get("hybrid")) ? null : ((String) requestParam.get("hybrid"));
    String acceptanceFlag = GenericValidator.isBlankOrNull((String) requestParam.get("acceptance")) ? null : ((String) requestParam.get("acceptance"));
    String x = new SessionData().getData(request, "x");
    x = x != null && !x.equals("0") && x.trim().length() >= 0 ? x : "16180339";      
    String y = new SessionData().getData(request, "y");
    y = y != null && !y.equals("0") && y.trim().length() >= 0 ? y : "0";      
    String basketId = GenericValidator.isBlankOrNull(common.checkCookieStatus(request)) ? null : common.checkCookieStatus(request);
    String thumbnailPath = "";
    String imagePathFeatured ="";
    String featuredPlayIcon = CommonUtil.getImgPath("/wu-cont/images/fbrnd_vid_play_icn.png",0);
    String mdevDomainName = GlobalConstants.MDEV_DOMAIN;
    String mtestDomainName = GlobalConstants.MTEST_DOMAIN;
    String wwwDomainName = GlobalConstants.LIVE_DOMAIN;
   
    if(!GenericValidator.isBlankOrNull(searchKeyword)){
      p_subject = "";
    }
    String clientIp = GenericValidator.isBlankOrNull(request.getParameter("ip")) ? new RequestResponseUtils().getRemoteIp(request) : (request.getParameter("ip"));//Added by Sangeeth.S for the 280420 release
//    String clientIp = request.getHeader("CLIENTIP");
//    if (clientIp == null || clientIp.length() == 0) {
//      clientIp = request.getRemoteAddr();
//    }
    String userAgent = request.getHeader("user-agent");
    String domainPath = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
    String domainPathImg = CommonUtil.getImgPath(SpringConstants.WU_CONT_IMAGE,1);
    String selQual = null;
    if (!GenericValidator.isBlankOrNull(urlArray[1])) {
      selQual = urlArray[1].substring(0, urlArray[1].indexOf("-courses"));
    }
    String studyLevelDesc = "degrees";
    String qualCode = "M";      
    if ("degree".equals(selQual)) {
      qualCode = "M"; 
      studyLevelDesc = "degrees";
    } else if ("postgraduate".equals(selQual)) {
      qualCode = "L";
      studyLevelDesc = "postgraduate degrees";
    } else if ("foundation-degree".equals(selQual)) {
      qualCode = "A";        
      studyLevelDesc = "foundation degrees";
    } else if ("access-foundation".equals(selQual)) {
      qualCode = "T";        
      studyLevelDesc = "access foundation degrees";
    } else if ("hnd-hnc".equals(selQual)) {
      qualCode = "N";     
      studyLevelDesc = "HND/HNC degrees";
    } else if ("ucas_code".equals(selQual)) {
      qualCode = "UCAS_CODE";
    } else if ("Clearing".equals(selQual)) {
      qualCode = "M";        
    } else if ("all".equals(selQual)) {
      qualCode = "";        
    }
    if(StringUtils.isNotBlank(scoreValue)) {
    	session.setAttribute("USER_UCAS_SCORE", scoreValue);
    }
    searchResultsInputBean.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
    searchResultsInputBean.setX(x);
    searchResultsInputBean.setY(y);
    searchResultsInputBean.setWebsiteAppCode(SpringConstants.WEBSITE_APP_CODE);
    searchResultsInputBean.setPageNo(pageNo);
    searchResultsInputBean.setSearchHow(searchHow.toUpperCase());
    searchResultsInputBean.setQualification(qualCode);
    searchResultsInputBean.setPhraseSearch(searchKeyword);
    searchResultsInputBean.setSearchCategory(p_subject);
    searchResultsInputBean.setUserScore(scoreValue);
    searchResultsInputBean.setUserIpAddress(clientIp);
    searchResultsInputBean.setTownCity(location);
    searchResultsInputBean.setCampusBasedUnivName(campusType);
    searchResultsInputBean.setAccomodationFlag(accommodationFlag);
    searchResultsInputBean.setScholarshipFlag(scholarshipFlag);
    searchResultsInputBean.setStudyMode(studyMode);
    searchResultsInputBean.setUnivLocTypeName(locationType);
    searchResultsInputBean.setHybridStudyModeFlag(!GenericValidator.isBlankOrNull(hybridFlag) ? hybridFlag.toUpperCase() : null);
    searchResultsInputBean.setUserAgent(userAgent);
    searchResultsInputBean.setSearchUrl(currentPageUrl);
    searchResultsInputBean.setAcceptanceCriteria(!GenericValidator.isBlankOrNull(acceptanceFlag) ? acceptanceFlag.toUpperCase().replace('-', ' ') : null);
    searchResultsInputBean.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
    searchResultsInputBean.setSubjGradesSessionId(SubjGradesSessionId);
    searchResultsInputBean.setDynamicRandomNumber(utilities.getSessionRandomNumber(request, session));
    searchResultsInputBean.setBasketId(basketId);
    
    String jsonInString = mapper.writeValueAsString(searchResultsInputBean);
   // System.out.println("jsonInString----" +jsonInString);
    String result = restBusiness.callWebServices(url, jsonInString);
    //System.out.println("result----" +result);    
    
    SearchResultsResponse searchResultsOutputBean = mapper.readValue(result, SearchResultsResponse.class);    
    String subjGradesSessionId = searchResultsOutputBean.getSubjGradesSessionId();
    session.setAttribute("subjectSessionId" , subjGradesSessionId);
    String resultExists = searchResultsOutputBean.getResultExists();
    String recordCount = searchResultsOutputBean.getRecordCount();
    String totalCourseCount = searchResultsOutputBean.getTotalCourseCount();
    String invalidCategoryFlag = searchResultsOutputBean.getInvalidCategoryFlag();
    String showSwitchFlag = searchResultsOutputBean.getShowSwitchFlag();
    List<SearchResultsContentResponse> searchResultsList = searchResultsOutputBean.getSearchResultsList();
    String matchTypeDim = "";
    String match_type_likely = "";
    String match_type_stretch = "";
    String match_type_possible = "";
    Map<String, String> uniMatchTypeMap = new HashMap<String, String>();
    Map<String, String> courseMatchTypeMap = new HashMap<String, String>();
    if (!CollectionUtils.isEmpty(searchResultsList)) {	

        String instIds = searchResultsList.stream().map(ins -> ins.getCollegeId()).collect(Collectors.joining("###"));
	   	String instNames = searchResultsList.stream().map(ins -> ins.getCollegeName()).collect(Collectors.joining("###"));	
    	for(SearchResultsContentResponse searchResult : searchResultsList)
    	{
		    String uni_match_type_likely = "";
		    String uni_match_type_stretch = "";
		    String uni_match_type_possible = "";
			searchResult.setUniProfileUrl(seoUrls.construnctProfileURL(searchResult.getCollegeId(), searchResult.getCollegeTextKey(), GlobalConstants.CLEARING_PAGE_FLAG));
			searchResult.setReviewUrl(seoUrls.constructReviewPageSeoUrl(searchResult.getCollegeTextKey(), searchResult.getCollegeId()));
			searchResult.setGaCollegeName(new WUI.utilities.CommonFunction().replaceSpecialCharacter(searchResult.getCollegeName()));
			
			for(SRCourseListResponse courseList : searchResult.getCourseDetailsList()) {
				courseList.setCourseDetailPageUrl(seoUrls.construnctCourseDetailsPageURL(courseList.getStudyLevelDesc(), courseList.getCourseTitle(), courseList.getCourseId(), searchResult.getCollegeId(), GlobalConstants.CLEARING_PAGE_FLAG));		
		        String course_match_type_likely = "";
		        String course_match_type_stretch = "";
		        String course_match_type_possible = "";
    	        
		    if(StringUtils.isNotBlank(courseList.getScoreLevel())) {
		      if("LIKELY".equalsIgnoreCase(courseList.getScoreLevel())) {
		    	  course_match_type_likely  = GlobalConstants.LIKELY_MATCH_TYPE;
		    	  uni_match_type_likely = GlobalConstants.LIKELY_MATCH_TYPE;
		    	  match_type_likely  = GlobalConstants.LIKELY_MATCH_TYPE;
		      }
		      if("POSSIBLE".equalsIgnoreCase(courseList.getScoreLevel())) {
		    	  course_match_type_possible  = GlobalConstants.POSSIBLE_MATCH_TYPE;
		    	  uni_match_type_possible  = GlobalConstants.POSSIBLE_MATCH_TYPE;
		    	  match_type_possible  = GlobalConstants.POSSIBLE_MATCH_TYPE;
		      }
		      if("STRETCH".equalsIgnoreCase(courseList.getScoreLevel())) {
		    	  course_match_type_stretch  = GlobalConstants.STRETCH_MATCH_TYPE;
		    	  uni_match_type_stretch  = GlobalConstants.STRETCH_MATCH_TYPE;
		    	  match_type_stretch  = GlobalConstants.STRETCH_MATCH_TYPE;
		      }
          }
    matchTypeDim = course_match_type_likely+"|"+course_match_type_possible+"|"+course_match_type_stretch;
    courseList.setMatchTypeDim(matchTypeDim);
    courseMatchTypeMap.put(courseList.getCourseId(), matchTypeDim);
    }
    matchTypeDim = uni_match_type_likely+"|"+uni_match_type_possible+"|"+uni_match_type_stretch;
    searchResult.setMatchTypeDim(matchTypeDim);
    uniMatchTypeMap.put(searchResult.getCollegeId(), matchTypeDim);
    }    
    model.addAttribute("searchResultsList", searchResultsList);
    model.addAttribute("listOfSearchResults", searchResultsList);
    model.addAttribute("instIds", instIds);
   	model.addAttribute("instNames", instNames);

    matchTypeDim = match_type_likely+"|"+match_type_possible+"|"+match_type_stretch;
    request.setAttribute("matchTypeDim", matchTypeDim);    
    model.addAttribute("courseMatchTypeMap", courseMatchTypeMap);
    model.addAttribute("uniMatchTypeMap", uniMatchTypeMap);	
    
    /*Added the session for sponsored list and manual boosting GA logging by Sujitha V on 01_07_2020*/
   	String sponsoredListing = "SPONSORED_LISTING";
   	String manualBoosting = "MANUAL_BOOSTING";
   	String sponsoredInstID = searchResultsList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getStdAdvertType())).map(SearchResultsContentResponse :: getCollegeId).collect(Collectors.joining(""));
   	String boostedInstID = searchResultsList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getStdAdvertType())).map(SearchResultsContentResponse :: getCollegeId).collect(Collectors.joining(""));
    String sponsoredInstFlag = searchResultsList.stream().filter(sponsoredFlag -> sponsoredListing.equalsIgnoreCase(sponsoredFlag.getStdAdvertType())).map(SearchResultsContentResponse :: getStdAdvertType).collect(Collectors.joining(""));
   	String manualInstFlag = searchResultsList.stream().filter(manualFlag -> manualBoosting.equalsIgnoreCase(manualFlag.getStdAdvertType())).map(SearchResultsContentResponse :: getStdAdvertType).collect(Collectors.joining(""));
   	String sponsoredInstName = searchResultsList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getStdAdvertType())).map(SearchResultsContentResponse :: getCollegeName).collect(Collectors.joining(""));
   	String boostedInstName = searchResultsList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getStdAdvertType())).map(SearchResultsContentResponse :: getCollegeName).collect(Collectors.joining(""));
   	session.setAttribute("sponsoredInstId", sponsoredInstID);
   	session.setAttribute("boostedInstId", boostedInstID); 
    model.addAttribute("sponsoredListFlag", sponsoredInstFlag);
    model.addAttribute("manualBoostingFlag", manualInstFlag);
    model.addAttribute("sponsoredInstName", sponsoredInstName);
    model.addAttribute("boostedInstName", boostedInstName);
   	/*End of ga logging session*/
    }
    List<QualificationResponse> qualificationList = searchResultsOutputBean.getQualificationList();
    List<StudyModeResponse> studyModeList = searchResultsOutputBean.getStudyModeList();
    List<RegionResponse> regionList = searchResultsOutputBean.getRegionList();
    List<LocationTypeResponse> uniLocationTypeList = searchResultsOutputBean.getLocationTypeList();
    List<AcceptanceResponse> acceptanceList = searchResultsOutputBean.getAcceptanceList();
    List<StatsResponse> statsLogList = searchResultsOutputBean.getStatsLogList();
    List<SearchCollegeIdsResponse> collegeIdsForThisSearch = searchResultsOutputBean.getCollegeIdsForThisSearch();
    List<SearchInstitutionIdsResponse> institutionIdsForThisSearch = searchResultsOutputBean.getInstitutionIdsForThisSearch();
    List<FeaturedBrandResponse> featuredBrandList = searchResultsOutputBean.getFeaturedBrandList();
    if (!CollectionUtils.isEmpty(featuredBrandList)) {
    	FeaturedBrandResponse featuredBrandResponse = featuredBrandList.get(0);
    	thumbnailPath = featuredBrandResponse.getThumbnailPath();
        thumbnailPath = utilities.getImageUrl(request, thumbnailPath, "_278px", "_369px", "_469px");
        thumbnailPath = CommonUtil.getImgPath(thumbnailPath, 1);
        imagePathFeatured = featuredBrandResponse.getMediaPath();
        imagePathFeatured = utilities.getImageUrl(request, imagePathFeatured, "_278px", "_369px", "_469px");
        imagePathFeatured = CommonUtil.getImgPath(imagePathFeatured, 1);
 	    String reviewUrlFeatured = new SeoUrls().constructReviewPageSeoUrl(featuredBrandResponse.getCollegeName(), featuredBrandResponse.getCollegeId());
        model.addAttribute("reviewUrlFeatured", reviewUrlFeatured);
        model.addAttribute("thumbnailPath", thumbnailPath);
    	model.addAttribute("imagePathFeatured", imagePathFeatured);
        model.addAttribute("featuredBrandListClearing", featuredBrandList);
        model.addAttribute("featuredBrandList", featuredBrandList);
        model.addAttribute("gaCollegeNameFeatured", new WUI.utilities.CommonFunction().replaceSpecialCharacter(featuredBrandResponse.getCollegeName()));
        request.setAttribute("featuredCollegeName", featuredBrandResponse.getCollegeName());

    }
    String keywordDisp = searchResultsOutputBean.getSearchPhrase();
    String browseCatDesc = statsLogList.stream().map(ins -> ins.getBrowseCatDesc()).collect(Collectors.joining(""));
    String subjectDip = browseCatDesc;
    model.addAttribute("subjectL1", subjectDip);
    String browseCatFlag = statsLogList.stream().map(ins -> ins.getBrowseCatFlag()).collect(Collectors.joining(""));
    subjectDip = (!GenericValidator.isBlankOrNull(browseCatDesc)) ? browseCatDesc : keywordDisp;
    String browseCatCode = "";
    
    
    if(GenericValidator.isBlankOrNull(browseCatDesc)|| "null".equalsIgnoreCase(browseCatDesc)) {
      subjectDip = keywordDisp;
    }
    
    String selectedQual = (!CollectionUtils.isEmpty(qualificationList)) ? qualificationList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getQualDisplayDesc()).collect(Collectors.joining("")) : "";
    String selectedStudyMode = (!CollectionUtils.isEmpty(studyModeList)) ? studyModeList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getDisplayStudyModeDesc()).collect(Collectors.joining("")) : "";
    String selectedRegion = (!CollectionUtils.isEmpty(regionList))  ? regionList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getRegionName()).collect(Collectors.joining("")) : "";
    String selectedLocType = (!CollectionUtils.isEmpty(uniLocationTypeList)) ? uniLocationTypeList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getLocTypeDesc()).collect(Collectors.joining("")) : "";
    //String selectedAcceptance = (!CollectionUtils.isEmpty(acceptanceList)) ? acceptanceList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getDispAcceptanceDec()).collect(Collectors.joining("")) : "";
    String collegeIds = (!CollectionUtils.isEmpty(institutionIdsForThisSearch)) ? institutionIdsForThisSearch.stream().map(ins -> ins.getInstitutionIds()).collect(Collectors.joining(",")) : "";
    String collegeDispNames = (!CollectionUtils.isEmpty(searchResultsList)) ? searchResultsList.stream().map(ins -> ins.getCollegeDisplayName()).collect(Collectors.joining(",")) : "";
    String cDimCollegeDisName = ""; 
    String cDimCollegeIds = "";
    if (!CollectionUtils.isEmpty(searchResultsList)) {  
      for(SearchResultsContentResponse searchResult : searchResultsList)
      {
        if(!GenericValidator.isBlankOrNull(searchResult.getCollegeName())){
          cDimCollegeDisName += "\"" + searchResult.getCollegeName() + "\", ";
        }
        if(!GenericValidator.isBlankOrNull(searchResult.getProviderId())){
          cDimCollegeIds += "'" + searchResult.getProviderId() + "', ";
        }
      }
      }
    if(!GenericValidator.isBlankOrNull(cDimCollegeIds)){
      cDimCollegeIds = cDimCollegeIds.substring(0, cDimCollegeIds.length()-2);              
      cDimCollegeIds = "[" + cDimCollegeIds + "]";              
      request.setAttribute("cDimCollegeIds", cDimCollegeIds);
    }
 if (!CollectionUtils.isEmpty(institutionIdsForThisSearch)) {
        
	 SearchInstitutionIdsResponse institutionIdResponse = null;
	 institutionIdResponse = (SearchInstitutionIdsResponse)institutionIdsForThisSearch.get(0);
        request.setAttribute("cDimCollegeId", institutionIdResponse.getInstitutionIds());                   
      }   
    if(!GenericValidator.isBlankOrNull(cDimCollegeDisName)){
      cDimCollegeDisName = cDimCollegeDisName.substring(0, cDimCollegeDisName.length()-2);
      if(searchResultsList != null && searchResultsList.size() > 1){
        cDimCollegeDisName = ("[" + cDimCollegeDisName + "]");
      }
      request.setAttribute("cDimCollegeDisName", cDimCollegeDisName);
    }
    
    if(StringUtils.isNotBlank(collegeIds)) {collegeIds = "["+collegeIds+"]";}
    if(StringUtils.isNotBlank(collegeDispNames)) {collegeDispNames = "["+collegeDispNames+"]";}
    
    
    if("Y".equalsIgnoreCase(browseCatFlag)){
      request.setAttribute("cDimLDCS", GenericValidator.isBlankOrNull(browseCatDesc) ? "" : new GlobalFunction().getReplacedString(browseCatDesc.trim().toLowerCase()));
      if (!GenericValidator.isBlankOrNull(searchResultsInputBean.getQualification())) {
        new GlobalFunction().getStudyLevelDesc(searchResultsInputBean.getQualification(), request);
      }  
      request.setAttribute("setPageType", "browsemoneypageresults");
    }else{
      request.setAttribute("MONEY_PAGE_GEO_TARGET", "TRUE");        
      request.setAttribute("cDimKeyword", GenericValidator.isBlankOrNull(keywordDisp)? "": new GlobalFunction().getReplacedString(keywordDisp.trim().toLowerCase()));      
      //request.setAttribute("cDimJACS", GenericValidator.isBlankOrNull(p_jacsCode)? "": p_jacsCode.trim());      
      if(!GenericValidator.isBlankOrNull(searchResultsInputBean.getQualification())){
        request.setAttribute("qualification", GenericValidator.isBlankOrNull(searchResultsInputBean.getQualification())? "": searchResultsInputBean.getQualification().trim());
        new GlobalFunction().getStudyLevelDesc(searchResultsInputBean.getQualification(), request);
      }
      request.setAttribute("setPageType", "moneypageresults");
    }
    session.removeAttribute("gamKeywordOrSubject");
    session.removeAttribute("gamStudyLevelDesc");
    
    session.setAttribute("gamStudyLevelDesc", selQual);
    session.setAttribute("gamKeywordOrSubject", GenericValidator.isBlankOrNull(p_subject) ? searchKeyword : p_subject);     
    request.setAttribute("gamKeywordOrSubject", GenericValidator.isBlankOrNull(p_subject) ? searchKeyword : p_subject);
    
    model.addAttribute("selectedQual", selectedQual);
    model.addAttribute("selectedStudyMode", selectedStudyMode);
    model.addAttribute("selectedRegion", selectedRegion);
    model.addAttribute("selectedLocType", selectedLocType);
    //model.addAttribute("selectedAcceptance", selectedAcceptance);
    
    model.addAttribute("scoreValue", scoreValue);
    model.addAttribute("locationType", locationType);
    model.addAttribute("campusType", campusType);
    model.addAttribute("studyMode", studyMode);
    model.addAttribute("accommodationFlag", accommodationFlag);
    model.addAttribute("scholarshipFlag", scholarshipFlag);
    model.addAttribute("searchKeyword", searchKeyword);
    model.addAttribute("region", location);
    model.addAttribute("hybridFlag", hybridFlag);
    model.addAttribute("subject", p_subject);
    model.addAttribute("qual", selQual);
    model.addAttribute("acceptanceFlag", acceptanceFlag);
    model.addAttribute("uniLocationTypeList", uniLocationTypeList);
    model.addAttribute("regionList", regionList);
    model.addAttribute("qualificationList", qualificationList);
    model.addAttribute("studyModeList", studyModeList);
    model.addAttribute("universityCount", recordCount);      
    model.addAttribute("totalCourseCount", totalCourseCount);
    model.addAttribute("sortBy", searchHow);
   // model.addAttribute("cDimCollegeIds", collegeIds);
   // model.addAttribute("cDimCollegeDisName", collegeDispNames);
    model.addAttribute("studyLevelDesc", studyLevelDesc);
    model.addAttribute("featuredPlayIcon", featuredPlayIcon);
    model.addAttribute("mdevDomain", mdevDomainName);
    model.addAttribute("mtestDomain", mtestDomainName);
    model.addAttribute("wwwDomain", wwwDomainName);
    model.addAttribute("videoClass", "fiveSecPlay");
    
  	model.addAttribute("pageName", "CLEARING_SEARCH_RESULTS");

    String categoryId = null;
    if("N".equalsIgnoreCase(resultExists)) {
      String errorSubject = !GenericValidator.isBlankOrNull(p_subject) ? p_subject : searchKeyword;
      if(!GenericValidator.isBlankOrNull(errorSubject)) {
      request.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(errorSubject) ? utilities.toTitleCase(common.replaceHypenWithSpace(errorSubject)).trim() : "");
      session.setAttribute("err_subject_name", !GenericValidator.isBlankOrNull(errorSubject) ? utilities.toTitleCase(common.replaceHypenWithSpace(errorSubject)).trim() : "");          
      session.setAttribute("keyword", !GenericValidator.isBlankOrNull(errorSubject) ? utilities.toTitleCase(common.replaceHypenWithSpace(errorSubject)).trim() : "");
      }
        response.sendRedirect(request.getContextPath() + "/notfound.html");
  }
    if(!CollectionUtils.isEmpty(statsLogList)) {
      StatsResponse statsList = statsLogList.get(0);
      browseCatDesc = statsList.getBrowseCatDesc();
      categoryId = statsList.getBrowseCatId();
      browseCatCode = statsList.getBrowseCatCode();
    }
    location = ((!GenericValidator.isBlankOrNull(location)) && (!"".equals(location))) ? location.trim() : common.replacePlus(location);   
    String regionTitleCase = GenericValidator.isBlankOrNull(location) ? "" : new CommonUtil().toTitleCase(location.trim());
    if (pageNo != null && pageNo.equals("1")) {
        Vector parameters = new Vector();
        parameters.add(categoryId); //categoryId
        OmnitureProperties omnitureProperties = utilities.getCpeCategory(parameters);
        String sc_prob42_keyword = omnitureProperties.getProb42_keyword() != null ? omnitureProperties.getProb42_keyword() : "";        
        String sc_prob43_category = omnitureProperties.getProb43_cpe_category() != null ? omnitureProperties.getProb43_cpe_category() : "";
        String sc_prob44_location = location != null ? location.toUpperCase() : "";
        if (sc_prob44_location.equalsIgnoreCase("LONDON")) {
          sc_prob44_location = "GREATER LONDON";
        }
        if (sc_prob43_category != null && sc_prob43_category.trim().length() > 1) {
          sc_prob43_category = sc_prob43_category.trim().toLowerCase();
          sc_prob42_keyword = "";
        } else if (sc_prob42_keyword != null && ((sc_prob43_category == null) || (sc_prob43_category != null && sc_prob43_category.trim().length() < 1))) {
          sc_prob43_category = "";
          sc_prob42_keyword = sc_prob42_keyword.trim().toLowerCase();
        }
              
        if(!"Y".equalsIgnoreCase(browseCatFlag)){
          sc_prob42_keyword = searchKeyword != null? new CommonFunction().replaceSpecialCharacter(searchKeyword.toLowerCase()): "";
        }        
        //remove previous attributes        
        session.removeAttribute("sc_prob42_keyword");
        session.removeAttribute("sc_prob43_category");
        session.removeAttribute("sc_prob44_location");
        //set the values into session scope                      
        session.setAttribute("sc_prob42_keyword", sc_prob42_keyword.toLowerCase());
        session.setAttribute("sc_prob43_category", sc_prob43_category.toLowerCase());
        session.setAttribute("sc_prob44_location", sc_prob44_location.toUpperCase());        
      }
    request.setAttribute("dimCategoryCode", GenericValidator.isBlankOrNull(browseCatCode) ? "" : new GlobalFunction().getReplacedString(browseCatCode.trim().toUpperCase()));
    
    request.setAttribute("subjectDesc", GenericValidator.isBlankOrNull(browseCatDesc) ? null : browseCatDesc);
    String studyLevel = searchResultsInputBean.getQualification();
    studyLevel = studyLevel != null ? utilities.toUpperCase(studyLevel) : studyLevel;        
    if (studyLevel != null && studyLevel.trim().length() > 0 && studyLevel.equalsIgnoreCase("L")) {
      String browseId = null; //DB_CALL
      if (browseId != null && browseId.trim().length() > 0 && browseId.equals("3")) {
        request.setAttribute("reason_for_404", "--(4)--> Redirect to 404 page for the postgraduate categodyid which belongs to hotcourses");
        response.sendError(404, "404 error message");
        return null;
      }
    } 
    String qualification = studyLevel;
    request.setAttribute("qualification1", qualification);
    request.getSession().setAttribute("bredqual", qualification);
    request.getSession().setAttribute("bredcatcode", categoryId);
    request.getSession().setAttribute("bredlocation", location);
    String breadCrumb = "";
      breadCrumb = new SeoUtilities().getKeywordBreadCrumb(request, "CLEARING_BROWSE_MONEY_PAGE", qualification, categoryId, "", utilities.toTitleCase(common.replaceHypenWithSpace(location)), "", searchKeyword);
    
    if (breadCrumb != null && breadCrumb.trim().length() > 0) {      
      request.setAttribute("breadCrumb", breadCrumb);
    }     
    
	boolean mobileFlag = MobileUtils.userAgentCheck(request);
	request.setAttribute("paramSubjectDesc", GenericValidator.isBlankOrNull(browseCatDesc) ? searchKeyword : browseCatDesc);
    location = location != null ? utilities.toUpperCase(location) : location;
    request.setAttribute("paramlocationValue", (location != null && !location.equals("null") && location.trim().length() > 0 ? location : ""));
    request.setAttribute("paramStudyLevelId", studyLevel);
    request.setAttribute("paramSubjectCode", browseCatCode);
    request.setAttribute("insightPageFlag", "yes");      
    if(!GenericValidator.isBlankOrNull(location)){ 
      request.setAttribute("location", common.replaceHypenWithSpace(location).toUpperCase());
    }
    if (!GenericValidator.isBlankOrNull(request.getQueryString())) {
        if(URL_QUR_STRING.contains("subject=") || URL_QUR_STRING.contains("location=")){
  	    request.setAttribute("meta_robot", "index,follow");
        }
        if(URL_QUR_STRING.contains("campus-type=") || URL_QUR_STRING.contains("study-mode=") || URL_QUR_STRING.contains("hybrid=") 
        || URL_QUR_STRING.contains("accommodation=") || URL_QUR_STRING.contains("scholarship=") || URL_QUR_STRING.contains("sort=")
        || URL_QUR_STRING.contains("score=") || URL_QUR_STRING.contains("location-type=") || URL_QUR_STRING.contains("acceptance=")){ 
  	    request.setAttribute("meta_robot", "noindex,follow");
        }
      }
    URL_1 = URL_STRING;            
    firstPageSeoUrl = URL_1 + "?" + URL_QUR_STRING;
    int numofpages = 0;
    int noOfrecords = 0;
    if(StringUtils.isNotBlank(recordCount)){
    noOfrecords =  Integer.parseInt(recordCount.replaceAll(",", ""));
    }
    numofpages =  noOfrecords / 10;
    if(noOfrecords % 10 > 0) {  numofpages++; }
    String paginationPageName = "newPagination";
    if(numofpages >1 && ("1").equals(pageNo)){  
    paginationPageName = "searchInitialPagination";
    }
    request.setAttribute("SHOW_SWITCH_FLAG", showSwitchFlag); 
    model.addAttribute("URL_1", URL_1);
    model.addAttribute("URL_2", URL_2);
    model.addAttribute("resultExists", resultExists);
    model.addAttribute("firstPageSeoUrl", firstPageSeoUrl);
    model.addAttribute("recordCount", recordCount);
    model.addAttribute("pageNo", pageNo); 
    model.addAttribute("pageno", pageNo);
    model.addAttribute("domainPath", domainPath);
    model.addAttribute("domainPathImg", domainPathImg);
    model.addAttribute("paginationPageName", paginationPageName);
    //System.out.println("len-->"+studyModeList.size());
    //model.addAttribute("acceptanceList", acceptanceList);
    model.addAttribute("dispSubjectName", subjectDip);
    model.addAttribute("mobileFlag", mobileFlag);
    model.addAttribute("SEO_FIRST_PAGE_URL", firstPageSeoUrl);
    model.addAttribute("queryStr", URL_QUR_STRING);
    model.addAttribute("img_px_gif", CommonUtil.getImgPath("/wu-cont/images/img_px.gif",1));
    model.addAttribute("pageUrl", request.getRequestURI());
    model.addAttribute("browseCatCode", browseCatCode);
    model.addAttribute("categoryId", categoryId);
    
    return new ModelAndView("clearingSearchResultsPage");
  }
  
  @RequestMapping(value = "/clearing/get-institution-ajax", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getInsitutionAjaxList(ModelMap model, UniversityAjaxRequest universityAjaxRequest, HttpServletRequest request, HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException, Exception {
    ObjectMapper mapper = new ObjectMapper();
    CommonUtil utilities = new CommonUtil();
    String url = utilities.generateWebServiceUrl(SpringConstants.MAPPING_FOR_UNI_AJAX);

    //searchResultsInputBean.setKeywordText("uni");
    String jsonInString = mapper.writeValueAsString(universityAjaxRequest);
    //System.out.println("jsonInString----" +jsonInString);
    String result = restBusiness.callWebServices(url, jsonInString);
    //System.out.println("result----" +result);
    model.addAttribute("uniAjaxListResult", result);
    UniAjaxResponse searchResultsOutputBean = mapper.readValue(result, UniAjaxResponse.class);    
    ArrayList<UniAjaxListResponse> uniAjaxList = (ArrayList<UniAjaxListResponse>) searchResultsOutputBean.getSearchResultUniAjaxList();
    model.addAttribute("uniAjaxList", uniAjaxList);
    return new ModelAndView("clearingSearchUniversityAjax");
  }
}
