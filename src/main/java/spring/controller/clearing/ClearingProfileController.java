package spring.controller.clearing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.token.TokenFormController;
import com.wuni.util.seo.ContentHubUrls;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.valueobject.contenthub.CampusVO;
import com.wuni.util.valueobject.contenthub.CollegeDetailsVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.CostOfLivingVO;
import com.wuni.util.valueobject.contenthub.EnquiryVO;
import com.wuni.util.valueobject.contenthub.GalleryVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.contenthub.PopularSubjectsVO;
import com.wuni.util.valueobject.contenthub.ReviewVO;
import com.wuni.util.valueobject.contenthub.SectionsVO;
import com.wuni.util.valueobject.contenthub.SocialVO;
import com.wuni.util.valueobject.contenthub.StudentStatsVO;
import com.wuni.util.valueobject.contenthub.UserDetailsVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
 * @ContentHubAction
 * @author Sangeeth.S
 * @version 1.0
 * @since 8.MAY.2020
 * @purpose This program is used to display the clearing profile page.
 * Change Log
 * *************************************************************************************************************************
 * Date              Name                      Ver.     Changes desc                              Change
 * *************************************************************************************************************************
 * 5.8.20		    Sangeeth.S							initial draft  
 * 18-AUG-2020      Sujitha V                           Modified                           Added dynamic meta data details for clearing profile page URLs in the Back office         
 * 30-sept-2020     Vedha R                             Modified                           Added Post clearing light box changes 
*/
@Controller
public class ClearingProfileController {
  public ModelAndView getClearingProfileDetails(ClearingProfileVO clearingProfileVO,  HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    // ---(A)--- DECLARE local objects & varialbes
	CommonUtil commonUtil = new CommonUtil();  
    Map clearingProfileMap = null;
    ArrayList latestUniReviewList = null;
    ArrayList studentUniReviewList = null;
    ArrayList enquiryInfoList = null;
    ArrayList socialNetworkUrlList = null;
    ArrayList campusesVenueList = null;
    ArrayList uniStatsList = null;
    ArrayList sectionsList = null;
    ArrayList livingCostList = null;
    ArrayList collegeDetailsList = null;
    ArrayList openDayList = null;
    ArrayList galleryList = null;
    ArrayList popularSubjectsList = null;
    ArrayList reviewRatingList = null;
    ArrayList userInfoList = null;
    String orderItemId = null;
    String subOrderItemId = null;
    String myhcProfileId = null;
    String profileType = null;
    String networkId = null;
    String collegeName = null;
    String collegeId = null;
    String collegeNameDisplay = null;
    String totalReviewCount = null;
    String overAllRating = null;
    String accomodationLower = null;
    String accomodationUpper = null;
    String livingCost = null;
    String costPint = null;
    String openDayFlag = null;
    String emailFlag = null;
    String emailWebFormFlag = null;
    String keyStatsEmptyFlag = null;
    String studentsStatsEmptyFlag = null;
    String allStatEmptyFlag = null;
    String collegeLogo = null;
    String absoluteRating = null;
    String galleryFlag = "N"; 
    ArrayList emailFlagList = null;

    //String clearingResultFlag = null;
    String clearingCourseExistFlag = null;
    //
    // ---(B) Business logic follow
    //
    try {
      String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
      if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
        cpeQualificationNetworkId = GlobalConstants.UG_NETWORK_ID;
      }
      if (GlobalConstants.UG_NETWORK_ID.equals(cpeQualificationNetworkId)) {
        request.setAttribute("highlightTab", "UG");
      } else {
        request.setAttribute("highlightTab", "PG");
      }
      //
      //----DB call
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      //
      // Getting details from DB
      clearingProfileMap = commonBusiness.getClearingProfileDetails(clearingProfileVO);
      //
      // Setting Request atributes for using in JSP
      if (!CommonUtil.isBlankOrNull(clearingProfileMap)) {
        latestUniReviewList = (ArrayList)clearingProfileMap.get("OC_LATEST_UNI_REVIEWS");
        if (!CommonUtil.isBlankOrNull(latestUniReviewList)) {
          request.setAttribute("LATEST_UNI_REVIEW_LIST", latestUniReviewList);
          ReviewVO reviewVO = (ReviewVO)latestUniReviewList.get(0);
          request.setAttribute("collegeId",reviewVO.getCollegeId());
          request.setAttribute("collegeName",reviewVO.getCollegeName());
          request.setAttribute("rating",reviewVO.getOverAllRating());
        }
        //
        studentUniReviewList = (ArrayList)clearingProfileMap.get("OC_STUDENT_UNI_REVIEWS");
        if (!CommonUtil.isBlankOrNull(studentUniReviewList)) {
          ReviewVO reviewVO = null;
          for (int i = 0; i < studentUniReviewList.size(); i++) {
            reviewVO = (ReviewVO)studentUniReviewList.get(i);
            if (!GenericValidator.isBlankOrNull(reviewVO.getMediaPath()) && !"30".equals(reviewVO.getMediaTypeId())) {
              reviewVO.setMediaPath(CommonUtil.getImgPath(reviewVO.getMediaPath(), 0));
            } 
            if (!GenericValidator.isBlankOrNull(reviewVO.getThumbNailPath())) {
              reviewVO.setThumbNailPath(CommonUtil.getImgPath(reviewVO.getThumbNailPath(), 0));
            }
          }
          request.setAttribute("STUDENT_UNI_REVIEW_LIST", studentUniReviewList);
        }
        //
        enquiryInfoList = (ArrayList)clearingProfileMap.get("OC_ENQUIRY_INFO");
        if (!CommonUtil.isBlankOrNull(enquiryInfoList)) {
          request.setAttribute("ENQUIRY_INFO_LIST", enquiryInfoList);
          EnquiryVO enquiryInfoVO = new EnquiryVO();
          for (int i = 0; i < enquiryInfoList.size(); i++) {
            enquiryInfoVO = (EnquiryVO)enquiryInfoList.get(i);
            orderItemId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getOrderItemId())) ? enquiryInfoVO.getOrderItemId() : "";
            subOrderItemId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getSubOrderItemId())) ? enquiryInfoVO.getSubOrderItemId() : "";
            myhcProfileId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getMyhcProfileId())) ? enquiryInfoVO.getMyhcProfileId() : "";
            profileType = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getProfileType())) ? enquiryInfoVO.getProfileType() : "";
            networkId = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getNetworkId())) ? enquiryInfoVO.getNetworkId() : "";
            emailFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailFlag())) ? enquiryInfoVO.getEmailFlag() : "";
            emailWebFormFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebFormFlag())) ? enquiryInfoVO.getEmailWebFormFlag() : "";
          }
        }
        //
        galleryList = (ArrayList)clearingProfileMap.get("OC_HERO_GALLERY");
        if (!CommonUtil.isBlankOrNull(galleryList)) {
          GalleryVO galleryVO = null;
          for (int i = 0; i < galleryList.size(); i++) {
            galleryVO = (GalleryVO)galleryList.get(i);
            request.setAttribute("PromotionalText",galleryVO.getPromotionalText());
            if (!GenericValidator.isBlankOrNull(galleryVO.getMediaPath()) && !"67".equals(galleryVO.getMediaTypeId())) {
              galleryVO.setMediaPath(CommonUtil.getImgPath(galleryVO.getMediaPath(), 0));
            } 
            if (!GenericValidator.isBlankOrNull(galleryVO.getThumbNailPath())) {
              galleryVO.setThumbNailPath(CommonUtil.getImgPath(galleryVO.getThumbNailPath(), 0));
            }
          }
          request.setAttribute("GALLERY_LIST", galleryList);   
          if( galleryList.size() > 1){
            galleryFlag = "Y";            
          }
        }
        //
        openDayList = (ArrayList)clearingProfileMap.get("OC_OPEN_DAY_INFO");
        if (!CommonUtil.isBlankOrNull(openDayList)) {
          /*
          for (int i = 0; i < openDayList.size(); i++) {
            opendayVO = (OpendayVO)openDayList.get(i);
            openDayFlag = (!GenericValidator.isBlankOrNull(opendayVO.getOpenDayFlag())) ? opendayVO.getOpenDayFlag() : "";
          }*/    	  
          OpendayVO opendayVO = null;
          String openDayEventType = "";
    	  //for(int i=0;i < openDayList.size(); i++) {
    		opendayVO  = (OpendayVO)openDayList.get(0);
    		openDayEventType = commonUtil.setEventTypeValueGA(openDayEventType, opendayVO.getEventCategoryName());			
          //}
    	  if(!GenericValidator.isBlankOrNull(openDayEventType)) {
    	    openDayEventType = openDayEventType.substring(0, openDayEventType.length() - 1);
    	    request.setAttribute("eventCategoryNameGa", commonUtil.setOpenDayEventGALabel(openDayEventType));
    	    request.setAttribute("openDayEventType", openDayEventType);//For logging open day event type in dimension 18 by sangeeth.S in APR-24-20
    	  }	
          request.setAttribute("listOfOpendayInfo", openDayList);
          request.setAttribute("OPEN_DAY_LIST", openDayList);
          request.setAttribute("opendayListSize", openDayList.size());          
        }        
        uniStatsList = (ArrayList)clearingProfileMap.get("OC_UNI_STATS_INFO");
        if (!CommonUtil.isBlankOrNull(uniStatsList)) {
          StudentStatsVO studentStatsVO = null;
          keyStatsEmptyFlag = "Y";
          studentsStatsEmptyFlag = "Y";
          allStatEmptyFlag = "N";
          for(int i = 0; i < uniStatsList.size(); i++){
            studentStatsVO = (StudentStatsVO)uniStatsList.get(i);
            if(i < 4){
              if(!GenericValidator.isBlankOrNull(studentStatsVO.getCustomizedFlag())){
                keyStatsEmptyFlag = "N";
              }
            }
            if(i >= 4){
              if(!GenericValidator.isBlankOrNull(studentStatsVO.getCustomizedFlag())){
                studentsStatsEmptyFlag = "N";
              }
            }
          }
          if("Y".equalsIgnoreCase(keyStatsEmptyFlag) && "Y".equalsIgnoreCase(studentsStatsEmptyFlag)){
            allStatEmptyFlag = "Y";                        
          }
          request.setAttribute("UNI_STATS_LIST", uniStatsList);
        }       
        sectionsList = (ArrayList)clearingProfileMap.get("OC_PROFILE_SECTIONS");
        if (!CommonUtil.isBlankOrNull(sectionsList)) {
          SectionsVO sectionsVO = null;
          for (int i = 0; i < sectionsList.size(); i++) {
            sectionsVO = (SectionsVO)sectionsList.get(i);
            if("Overview".equalsIgnoreCase(sectionsVO.getSectionName())){              
              myhcProfileId = (!GenericValidator.isBlankOrNull(sectionsVO.getMyhcProfileId())) ? sectionsVO.getMyhcProfileId() : "";
              subOrderItemId = (!GenericValidator.isBlankOrNull(sectionsVO.getSubOrderItemId())) ? sectionsVO.getSubOrderItemId() : "";
            }
            if (!GenericValidator.isBlankOrNull(sectionsVO.getImagePath()) && !"VIDEO".equals(sectionsVO.getMediaType())) {
              sectionsVO.setImagePath(CommonUtil.getImgPath(sectionsVO.getImagePath(), 0));
            }
            if (!GenericValidator.isBlankOrNull(sectionsVO.getThumbnailPath())) {
              sectionsVO.setThumbnailPath(CommonUtil.getImgPath(sectionsVO.getThumbnailPath(), 0));
            }
          }
          request.setAttribute("SECTIONS_LIST", sectionsList);
        }
        //        
        collegeDetailsList = (ArrayList)clearingProfileMap.get("OC_COLLEGE_DETAILS");
        if (!CommonUtil.isBlankOrNull(collegeDetailsList)) {
          CollegeDetailsVO collegeDetailsVO = null;
          for (int i = 0; i < collegeDetailsList.size(); i++) {
            collegeDetailsVO = (CollegeDetailsVO)collegeDetailsList.get(i);
            collegeName = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeName())) ? collegeDetailsVO.getCollegeName() : "";
            collegeId = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeId())) ? collegeDetailsVO.getCollegeId() : "";
            collegeNameDisplay = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeNameDisplay())) ? collegeDetailsVO.getCollegeNameDisplay() : "";
            totalReviewCount = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getReviewCount())) ? collegeDetailsVO.getReviewCount() : "";
            overAllRating = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getOverAllRating())) ? collegeDetailsVO.getOverAllRating() : "";
            collegeLogo = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getCollegeLogo())) ? CommonUtil.getImgPath(collegeDetailsVO.getCollegeLogo(), 0) : "";
            absoluteRating = (!GenericValidator.isBlankOrNull(collegeDetailsVO.getAbsoluteRating())) ? collegeDetailsVO.getAbsoluteRating() : "";
          }
        }
        //
        reviewRatingList = (ArrayList)clearingProfileMap.get("OC_WUSCA_CATEGORIES_RATINGS");
        if (!CommonUtil.isBlankOrNull(reviewRatingList)) {
          request.setAttribute("REVIEW_RATING_LIST", reviewRatingList);
        }
        //
        clearingCourseExistFlag = (String)clearingProfileMap.get("O_COURSE_EXIST_FLAG");
        //        
        String collegeBasketFlag = (String)clearingProfileMap.get("O_COLLEGE_IN_BASKET");
        if (!GenericValidator.isBlankOrNull(clearingCourseExistFlag)) {
          request.setAttribute("COLLEGE_IN_BASKET", collegeBasketFlag);
        }
        String pixelTrackingCode = (String)clearingProfileMap.get("O_PIXEL_TRACKING_CODE");
          if (!GenericValidator.isBlankOrNull(pixelTrackingCode)) {
            request.setAttribute("pixelTrackingCode", pixelTrackingCode);
          }
          String accomdationAvailFlag = (String)clearingProfileMap.get("O_ACCOMODATION_FLAG");
          if (!GenericValidator.isBlankOrNull(accomdationAvailFlag)) {
            request.setAttribute("ACCOMODATION_AVAIL_FLAG", accomdationAvailFlag);
          }
          String scholarshipAvailFlag = (String)clearingProfileMap.get("O_SCHOLARSHIP_FLAG");
          if (!GenericValidator.isBlankOrNull(scholarshipAvailFlag)) {
            request.setAttribute("SCHOLARSHIP_AVAIL_FLAG", scholarshipAvailFlag);
          }
        //Added review subject cursor for review 
         List reviewSubjectList = (List)clearingProfileMap.get("OC_REVIEW_SUBJECT");
         if (reviewSubjectList != null && reviewSubjectList.size() > 0) {
           request.setAttribute("reviewSubjectList", reviewSubjectList);
         }
        //Added review breakdown cursor for review 
         List reviewBreakdownlist = (List)clearingProfileMap.get("OC_REVIEW_BREAKDOWN");
         if (reviewBreakdownlist != null && reviewBreakdownlist.size() > 0) {
           request.setAttribute("reviewBreakdownlist", reviewBreakdownlist);
           ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakdownlist.get(0);
           request.setAttribute("questionTitle",reviewBreakDownVO.getQuestionTitle());
          request.setAttribute("rating",reviewBreakDownVO.getRating());
          request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
          request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
           
         }
          ArrayList commonPhrasesList = (ArrayList)clearingProfileMap.get("OC_COMMON_PHRASES");
          if (commonPhrasesList != null && commonPhrasesList.size() > 0) { 
            CommonPhrasesVO  commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(0);
              if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
                request.setAttribute("commonPhrasesList", commonPhrasesList);
              }      
          }
          
          /*Dynamic meta data details for clearing profile page URLs in the Back office - added by Sujitha V on 18 AUG 2020 Rel*/
  	      ArrayList clearingProfileSeoMetaDetailsList = (ArrayList) clearingProfileMap.get("OC_META_DETAILS");
  		  if(!CollectionUtils.isEmpty(clearingProfileSeoMetaDetailsList)){
  			request.setAttribute("seoMetaDetailsSRPage", clearingProfileSeoMetaDetailsList);
  			SEOMetaDetailsVO clearingProfileSeoMetaDetailsVO = (SEOMetaDetailsVO) clearingProfileSeoMetaDetailsList.get(0);
  			request.setAttribute("CLR_PROFILE_META_TITLE", clearingProfileSeoMetaDetailsVO.getMetaTitle());
  			request.setAttribute("CLR_PROFILE_META_DESC", clearingProfileSeoMetaDetailsVO.getMetaDesc());
  			request.setAttribute("CLR_PROFILE_META_KEYWORDS", clearingProfileSeoMetaDetailsVO.getMetaKeyword());
  			request.setAttribute("CLR_PROFILE_META_ROBOTS", clearingProfileSeoMetaDetailsVO.getMetaRobots());
  			request.setAttribute("CLR_PROFILE_STUDY_DESC", clearingProfileSeoMetaDetailsVO.getStudyDesc());
  			request.setAttribute("CLR_PROFILE_CATEGORY_DESC", clearingProfileSeoMetaDetailsVO.getCategoryDesc());
  		  }
  		  /*End of Dynamic meta data details for clearing profile page URLs in the Back office*/
          
        //Creating the view all course url
        String viewAllCourseUrl = "";
        String qualification = "degree";
        if (!GenericValidator.isBlankOrNull(networkId) && GlobalConstants.PG_NETWORK_ID.equals(networkId)) {
          qualification="postgraduate";
        }        
        if("N".equalsIgnoreCase(clearingCourseExistFlag)){
          qualification = "all";
        }
        viewAllCourseUrl = new SeoUrls().constructCourseUrl(qualification, profileType, collegeName);  
         new TokenFormController().saveToken(request);
         String  breadCrumb = new SeoUtilities().getBreadCrumb("UNI_PROFILE_PAGE", "", "", "", "", collegeId);
         if (breadCrumb != null && breadCrumb.trim().length() > 0) {
          request.setAttribute("breadCrumb", breadCrumb);
         }
         //
         request.setAttribute("SHOW_SWITCH_FLAG", "Y"); 
        request.setAttribute("CLEARING_COURSE_EXIST_FLAG", clearingCourseExistFlag); 
        request.setAttribute("ORDER_ITEM_ID", orderItemId);
        request.setAttribute("SUBORDER_ITEM_ID", subOrderItemId);
        request.setAttribute("MYHC_PROFILE_ID", myhcProfileId);
        request.setAttribute("PROFILE_TYPE", profileType);
        request.setAttribute("NETWORK_ID", networkId);
        request.setAttribute("EMAIL_FLAG", emailFlag);
        request.setAttribute("EMAIL_WEBFORM_FLAG", emailWebFormFlag);
        request.setAttribute("COLLEGE_NAME", collegeName);
        request.setAttribute("hitbox_college_name", collegeName);
        request.setAttribute("COLLEGE_ID", collegeId);
        request.setAttribute("COLLEGEID_IN_INTERACTION", collegeId);
        request.setAttribute("COLLEGE_NAME_DISPLAY", collegeNameDisplay);
        //
        if(!GenericValidator.isBlankOrNull(collegeName)){
          request.setAttribute("cDimCollegeDisName", ("\""+collegeName+"\""));
        }
        //
        request.setAttribute("COLLEGE_NAME_GA", new CommonFunction().replaceSpecialCharacter(collegeNameDisplay));
        request.setAttribute("TOTAL_REVIEW_COUNT", totalReviewCount);
        request.setAttribute("OVERALL_RATING", overAllRating);
        
        request.setAttribute("collegeNameDisplay", collegeNameDisplay);
        request.setAttribute("reviewCount", totalReviewCount);
        request.setAttribute("overallRating", absoluteRating);
        
        request.setAttribute("ACCOMMODATION_LOWER", accomodationLower);
        request.setAttribute("ACCOMMODATION_UPPER", accomodationUpper);
        request.setAttribute("LIVING_COST", livingCost);
        request.setAttribute("COST_PINT", costPint);
        request.setAttribute("OPEN_DAY_FLAG", openDayFlag);
        request.setAttribute("REQUEST_URL", CommonUtil.getRequestedURL(request));
        request.setAttribute("REFERER_URL", request.getHeader("referer") == null ? "" : request.getHeader("referer"));
        request.setAttribute("KEY_STATS_EMPTY_FLAG", keyStatsEmptyFlag);
        request.setAttribute("STUDENTS_STATS_EMPTY_FLAG", studentsStatsEmptyFlag);
        request.setAttribute("ALL_STATS_EMPTY_FLAG", allStatEmptyFlag);
        request.setAttribute("VIEW_ALL_COURSE_URL", viewAllCourseUrl);
        request.setAttribute("COLLEGE_LOGO", collegeLogo);
        request.setAttribute("showBanner", "no");
        request.setAttribute("ABSOLUTE_RATING", absoluteRating);
        request.setAttribute("GALLERY_FLAG", galleryFlag);
        
        request.setAttribute("cDimCollegeId", collegeId);
        if(!GenericValidator.isBlankOrNull(collegeId)){
          request.setAttribute("cDimCollegeIds", ("'" + collegeId + "'"));
        }
      }
      //
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (clearingProfileMap != null) {
        clearingProfileMap.clear();
      }
      clearingProfileMap = null;
    }
    //
    return null;
  }

  /**
   * This method is used to display the post clearing light box
   * 
   * @return
   * @throws Exception
   */
  @RequestMapping(value = {"/ajax/clearinglightbox"}, method = RequestMethod.POST)
  public ModelAndView ajaxClearingLightBox() throws Exception {
      return new ModelAndView("clearingLightBox");
  }
}
