package spring.controller.clearing;

import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import spring.business.IPreClearingLandingBusiness;

import com.wuni.util.RequestResponseUtils;
import com.wuni.util.valueobject.ClearingFeaturedProvidersVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ClearingHome.java
 * Description   : Class to load the Clearing Home Page Details
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * Author	               Ver 	           Modified On         Modification Details 	     Change
 * *************************************************************************************************************************************
 * Thiyagu G               1.0               12.06.2014           First draft   		
 * Thiyagu G               1.1               09.06.2015           Modified                   Clearing home page revamp.
 * Yogeswari               1.2               09.06.2015           Modified                   Added clearing cookie and session variable.
 * Sangeeth.S              1.3               25.09.2018           Modified                   Redirected the CLearing home page to normal home page when clearing is OFF
 * Kailash L               1.4               23.06.2020           Modified                   JIRA 287- Clearing home page changes               
 * Minu S                  1.5               30.09.2020           Modified                   JIRA 810- Clearing switch off updates             
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
@Controller
@SuppressWarnings({"rawtypes","unchecked"})
public class ClearingHomePageController {

    @Autowired IPreClearingLandingBusiness preClearingLandingBusiness;
    
    /**
     * This metheod is used to display the clearing home page
     * 
     * @param model
     * @param request
     * @param response
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/university-clearing-*"})
    public ModelAndView getClearingHome(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Long startActionTime = new Long(System.currentTimeMillis());
        CommonFunction common = new CommonFunction();
        String clearingOnOff = common.getWUSysVarValue("CLEARING_HOMEPAGE");
		String wuClearingOnOff = common.getWUSysVarValue("CLEARING_ON_OFF");
        String postClearingOnOff = common.getWUSysVarValue("POST_CLEARING_ON_OFF"); // POST clearing ON OFF, 26-08-2020 by Minu
        String user_id = new SessionData().getData(request,"y");
        //Added clearing home redirection when clearing is OFF for 27_SEP_18 rel by Sangeeth.S
        if (GlobalConstants.OFF.equalsIgnoreCase(clearingOnOff) && "OFF".equalsIgnoreCase(postClearingOnOff)) {
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.sendRedirect(GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN);
            return null;
        }
        request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI()).substring(request.getContextPath().length())); //Added getSchemeName by Indumathi Mar-29-16
        if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) {
            return new ModelAndView("forward: /userLogin.html");
        }
        try {
            if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
                String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
                session.setAttribute("fromUrl", fromUrl);
                return new ModelAndView("loginPage");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mappingForward = "clearingHome";
        String cpeQualificationNetworkId = (String) request.getSession().getAttribute("cpeQualificationNetworkId");
        if (StringUtils.isBlank(cpeQualificationNetworkId)) {
            cpeQualificationNetworkId = "2";
        }
        String loadingImgPath = CommonUtil.getImgPath(GlobalConstants.WU_CONT + "/images/hm_ldr.gif", 0);
        model.addAttribute("loadingImgPath", loadingImgPath);
        String subjectId = "";
        
        /* ::START:: Yogeswari :: 09.06.2015 ::  Added clearing cookie and session variable. */
        session.setAttribute("USER_TYPE", GlobalConstants.CLEARING_USER_TYPE_VAL);
        Cookie cookie = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.CLEARING_USER_TYPE_VAL);
        response.addCookie(cookie);
        /* ::END:: Yogeswari :: 09.06.2015 ::  Added clearing cookie and session variable. */
        request.setAttribute("studyLevelDesc", "Clearing");

        List parameters = new ArrayList();
        parameters.add(GlobalConstants.WHATUNI_WEBSITE_ID);
        parameters.add(new SessionData().getData(request, "y"));
        parameters.add(new SessionData().getData(request, "x"));
        parameters.add(request.getHeader("user-agent"));
        parameters.add(new RequestResponseUtils().getRemoteIp(request));
        parameters.add(CommonUtil.getRequestedURL(request)); //Change the getRequestURL by Prabha on 28_Jun_2016  
        parameters.add(request.getHeader("referer"));
        parameters.add(subjectId);
        parameters.add(new SessionData().getData(request, "userTrackId"));
        Map clearingHomePageInfo = preClearingLandingBusiness.getClearingHomePageDetails(parameters);
        try {

            ArrayList < ClearingFeaturedProvidersVO > listOfFeaturedProviders = (ArrayList) clearingHomePageInfo.get("O_FEATURED_PROVIDERS");
            ArrayList < ClearingFeaturedProvidersVO > featuredProvidersList = listOfFeaturedProviders;
            if (!CollectionUtils.isEmpty(listOfFeaturedProviders)) {
                String collegeIdValues = featuredProvidersList.stream().map(ins -> ins.getCollegeId()).collect(Collectors.joining("###"));
                String collegeNameValues = featuredProvidersList.stream().map(ins -> ins.getCollegeName()).collect(Collectors.joining("###"));
                String orderItemId = featuredProvidersList.stream().map(ins -> ins.getOrderItemId()).collect(Collectors.joining("###"));
                request.setAttribute("collegeIdValues", collegeIdValues);
                request.setAttribute("collegeNameValues", collegeNameValues);
                request.setAttribute("listOfFeaturedProviders", listOfFeaturedProviders);
                request.setAttribute("orderItemId", orderItemId);
            }

            ArrayList faqPodList = (ArrayList) clearingHomePageInfo.get("PC_FAQ_DETAILS");
            if (!CollectionUtils.isEmpty(faqPodList)) {
                request.setAttribute("faqPodList", faqPodList);
            }

            ArrayList listOfArticleTeaser = (ArrayList) clearingHomePageInfo.get("O_ARTICLE_TEASER");
            if (!CollectionUtils.isEmpty(listOfArticleTeaser)) {
                request.setAttribute("listOfArticleTeaser", listOfArticleTeaser);
            }

            ArrayList locationList = (ArrayList) clearingHomePageInfo.get("PC_LOCATION_LIST");
            if (!CollectionUtils.isEmpty(locationList)) {
                request.setAttribute("locationList", locationList);
            }
            
            model.addAttribute("curActiveMenu", "CLEARING_"+GlobalConstants.CLEARING_YEAR);

        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userId", user_id);
        request.setAttribute("insightPageFlag", "yes");
		session.setAttribute("postClearingOnOff", postClearingOnOff);
		session.setAttribute("wuClearingOnOff", wuClearingOnOff);
        if ("2".equals(cpeQualificationNetworkId)) {
            new GlobalFunction().getStudyLevelDesc("M", request);
        } else {
            new GlobalFunction().getStudyLevelDesc("L", request);
        }
        /*  DONT ALTER THIS ---> mainly used to track time taken details  */
        if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
            new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
        } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
        return new ModelAndView(mappingForward);
    }


    /**
     * This method is used to display the clearing course subject via ajax
     * 
     * @param subjectKeywordSearch
     * @param model
     * @param request
     * @param response
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/ajax/clearing-courseSubject"}, method = RequestMethod.POST)
    public ModelAndView ajaxClearingSubject(@RequestParam(value = "subjectKeywordSearch", required = false) String subjectKeywordSearch, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        Map resultMap = preClearingLandingBusiness.getClearingSubjectAjaxList(subjectKeywordSearch);
        if (resultMap != null) {
            ArrayList subjectAjaxList = (ArrayList) resultMap.get("PC_GET_SUBJECT_LIST");
            if (!CollectionUtils.isEmpty(subjectAjaxList)) {
                model.addAttribute("subjectAjaxList", subjectAjaxList);
            }
            
            String keywordmatchBrowseCatId= (String) resultMap.get("P_BROWSE_CAT_ID");
            
            if(StringUtils.isNotBlank(keywordmatchBrowseCatId)){
        		model.addAttribute("keywordmatchBrowseCatId", keywordmatchBrowseCatId);
        	  }

        }
        return new ModelAndView("clearingCourseSubjectAjax");
    }

    /**
     * This method is used to display the university via ajax
     * 
     * @param uniKeywordSearch
     * @param model
     * @param request
     * @param response
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/ajax/clearing-university"}, method = RequestMethod.POST)
    public ModelAndView ajaxClearingUniversity(@RequestParam(value = "uniKeywordSearch", required = false) String uniKeywordSearch, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        Map resultMap = preClearingLandingBusiness.getClearingUniAjaxList(uniKeywordSearch);
        if (resultMap != null) {
            ArrayList uniAjaxList = (ArrayList) resultMap.get("PC_UNIVERSITY_AJAX_LIST");
            if (!CollectionUtils.isEmpty(uniAjaxList)) {
                model.addAttribute("uniAjaxList", uniAjaxList);
            }
        }
        return new ModelAndView("clearingUniversityAjax");
    }
}