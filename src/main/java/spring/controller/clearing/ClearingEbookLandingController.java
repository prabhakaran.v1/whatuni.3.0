package spring.controller.clearing;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.IAdviceBusiness;
import com.layer.util.SpringConstants;
import WUI.registration.bean.RegistrationBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import spring.business.IPreClearingLandingBusiness;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : ClearingEbookLandingController.java
* Description   : This class will give pre clearing landing page and success page details
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	             Change
* ************************************************************************************************************************************** 
* Sujitha.V               			          31-March-2020     Modified                             Added to get SHTML Contents details
* */

@Controller
public class ClearingEbookLandingController {
	
  @Autowired
  IPreClearingLandingBusiness preClearingLandingBusiness = null;
  
  @RequestMapping(value = {SpringConstants.PRE_CLEARING_LANDING_URL, SpringConstants.PRE_CLEARING_SUCCESS_URL}) 
  public ModelAndView getClearingEbookLanding(ModelMap model, RegistrationBean registerBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
     //System.out.println("pre clearing controller");
	  try {
      // ---- INSIGHTS
      request.setAttribute("cDimPageName", "lead capture");
      request.setAttribute("cDimLevel", "Undergraduate");
      String userId = new SessionData().getData(request, "y");
      if(!GenericValidator.isBlankOrNull(userId)) {
        request.setAttribute("cDimUID", userId);
      }
	  //
	  // ----- GA
      request.setAttribute("ebookDim14", "clearing");
      if (session.getAttribute("userInfoList") != null) {
    	List values  = (List)session.getAttribute("userInfoList");
    	//System.out.println("session.getAttribute(userInfoList)"+ values.size());
      }
      if (request.getRequestURI().indexOf("/success") >= 0) {
        if (session.getAttribute("userInfoList") == null) {
      
        	//System.out.println("HAAAAAAI");
          response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + SpringConstants.PRE_CLEARING_LANDING_URL + SpringConstants.DOT_HTML);
        }
        
        ArrayList articleList = null;
        List parameters = new ArrayList();
        parameters.add("Clearing Landing page");
        parameters.add("6");
        IAdviceBusiness adviceBusiness = (IAdviceBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_ADVICE_BUSINESS);
        Map adviceHomeInfoMap = adviceBusiness.getArticleList(parameters);
        articleList = (ArrayList)adviceHomeInfoMap.get("RetVal");
        request.setAttribute("CLEARING_LANDING_ARICLE_LIST", articleList);
        return new ModelAndView("clearingEbookSuccess", "registerBean" , registerBean);
      } else {
        request.setAttribute("referrerURL_GA", "referrerURL_GA");
        CommonFunction common = new CommonFunction();
        String[] yoeArr = common.getYearOfEntryArr();
        request.setAttribute("yearofEntry", yoeArr[0]);
        
        //Added to get pre clearing About whatuni SHTML contents details - Sujitha V for 31 March 2020
        Map getAboutWhatuniSHTMLMap = preClearingLandingBusiness.getEbookAboutWhatuniContents();
        Clob clob = (Clob)getAboutWhatuniSHTMLMap.get("P_HTML_CONTENT");
        String aboutWhatuniHtmlContents = new String();
        aboutWhatuniHtmlContents = CommonFunction.getClobValue(clob);
        model.addAttribute("aboutWhatuniHtmlContents", aboutWhatuniHtmlContents);
  
        return new ModelAndView("clearingEbookLandingPage", "registerBean" , registerBean);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
