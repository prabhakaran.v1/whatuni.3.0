package spring.controller.clearing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUtilities;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import mobile.util.MobileUtils;
import spring.business.IRestBusiness;
import spring.pojo.clearing.ajax.SubjectAjaxRequest;
import spring.pojo.clearing.common.AcceptanceResponse;
import spring.pojo.clearing.common.QualificationResponse;
import spring.pojo.clearing.common.StatsResponse;
import spring.pojo.clearing.common.StudyModeResponse;
import spring.pojo.clearing.provider.CollegeDetailsResponse;
import spring.pojo.clearing.provider.ProviderResultsContentResponse;
import spring.pojo.clearing.provider.ProviderResultsRequest;
import spring.pojo.clearing.provider.ProviderResultsResponse;
import spring.pojo.clearing.provider.SubjectAjaxListResponse;
import spring.pojo.clearing.provider.SubjectAjaxResponse;

@Controller
public class ClearingProviderResultController {

  @Autowired
  IRestBusiness restBusiness = null;

  @RequestMapping(value = "/clearing/provider-results", method = { RequestMethod.GET, RequestMethod.POST })
  public ModelAndView providerResultsLanding(@RequestParam Map<String, String> requestParam, ModelMap model, ProviderResultsRequest providerResultsInputBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException, Exception {
    CommonUtil util = new CommonUtil();
    GlobalFunction globalFn = new GlobalFunction();
    ObjectMapper mapper = new ObjectMapper();
    SeoUrls seoUrls = new SeoUrls();
    CommonFunction common = new CommonFunction();
    String url = util.generateWebServiceUrl(SpringConstants.MAPPING_FOR_PROVIDER_RESULTS);
    final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    final String URL_QUR_STRING = request.getQueryString();
    String urlArray[] = URL_STRING.split("/");
    String pageNo = StringUtils.isNotBlank((String) requestParam.get("pageno")) ? requestParam.get("pageno") : "1";
    String URL_1 = "";
    String URL_2 = "";
    String firstPageSeoUrl = "";   
    String recordCount = "";
	String userId = new SessionData().getData(request, "y");
	userId = StringUtils.isNotBlank(userId) && !userId.equals("0") ? userId : "";
    String searchKeyword = GenericValidator.isBlankOrNull(requestParam.get("q"))? "": (requestParam.get("q"));
    String subject = GenericValidator.isBlankOrNull(requestParam.get("subject"))? "": (requestParam.get("subject"));
    String location = GenericValidator.isBlankOrNull(requestParam.get("location")) ? "united-kingdom" : (requestParam.get("location"));
    String universityName = GenericValidator.isBlankOrNull(requestParam.get("university"))? "": (requestParam.get("university"));
    String searchWhat = "Z"; // SEARCH TYPE Z-COLLEGE A -All COURSES 
    String searchHow = !GenericValidator.isBlankOrNull(requestParam.get("sort"))? requestParam.get("sort"): "R"; 
    String scoreValue = !GenericValidator.isBlankOrNull((String) requestParam.get("score")) ? (String) requestParam.get("score") : null;
    String hybridFlag = GenericValidator.isBlankOrNull((String) requestParam.get("hybrid")) ? null : ((String) requestParam.get("hybrid"));
    String acceptanceFlag = GenericValidator.isBlankOrNull((String) requestParam.get("acceptance")) ? null : ((String) requestParam.get("acceptance"));
    String studyMode = GenericValidator.isBlankOrNull((String) requestParam.get("study-mode")) ? null : ((String) requestParam.get("study-mode"));
	String basketId = GenericValidator.isBlankOrNull(common.checkCookieStatus(request)) ? null : common.checkCookieStatus(request);
	String searchUrl = "";
	searchUrl = URL_STRING + (GenericValidator.isBlankOrNull(URL_QUR_STRING) ? "" : ("?" + URL_QUR_STRING));
	String x = new SessionData().getData(request, "x");
	x = x != null && !x.equals("0") && x.trim().length() >= 0 ? x : "16180339";
	
//    String clientIp = request.getHeader("CLIENTIP");
//    if (clientIp == null || clientIp.length() == 0) {
//      clientIp = request.getRemoteAddr();
//    }
    String userAgent = request.getHeader("user-agent");
//    if ("".equals(subject) && !"".equals(universityName)) {
//      searchWhat = "A";
//    }
//    if (!GenericValidator.isBlankOrNull(subject)) {
//      searchHow = "R";
//    }
    if ("".equals(subject) && !"".equals(universityName) && GenericValidator.isBlankOrNull(searchKeyword)) {      
      searchWhat = "A";
      searchHow = !GenericValidator.isBlankOrNull(request.getParameter("sort")) ? request.getParameter("sort") : "TA";
    }
    String selQual = null;
    if (!GenericValidator.isBlankOrNull(urlArray[1])) {
      selQual = urlArray[1].substring(0, urlArray[1].indexOf("-courses"));
    }
    String qualCode = "M";      
    if ("degree".equals(selQual)) {
      qualCode = "M";        
    } else if ("postgraduate".equals(selQual)) {
      qualCode = "L";
    } else if ("foundation-degree".equals(selQual)) {
      qualCode = "A";        
    } else if ("access-foundation".equals(selQual)) {
      qualCode = "T";        
    } else if ("hnd-hnc".equals(selQual)) {
      qualCode = "N";        
    } else if ("ucas_code".equals(selQual)) {
      qualCode = "UCAS_CODE";
    } else if ("Clearing".equals(selQual)) {
      qualCode = "M";        
    } else if ("all".equals(selQual)) {
      qualCode = "";        
    }
    if (GenericValidator.isBlankOrNull(universityName)) {
      request.setAttribute("reason_for_404","--(1.1)--> URL doesn't have university");
      response.sendError(404, "404 error message");
      return null;
    }
    if(!"all".equalsIgnoreCase(selQual) && !GenericValidator.isBlankOrNull(subject)){
      String canonicalURL = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName() + "/" + selQual + "-courses/csearch";
      canonicalURL += "?subject=" + subject; 
      if(!GenericValidator.isBlankOrNull(universityName)){
        canonicalURL += "&university=" + universityName; 
      }
      request.setAttribute("clrCanonicalURL", canonicalURL);
    }
    String studyLevel = qualCode;
    providerResultsInputBean.setX(x);
    providerResultsInputBean.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
    providerResultsInputBean.setWebsiteAppCode(SpringConstants.WEBSITE_APP_CODE);
    providerResultsInputBean.setCollegeName(universityName);
    providerResultsInputBean.setSearchHow(searchHow.toUpperCase());
    providerResultsInputBean.setSearchWhat(searchWhat);
    providerResultsInputBean.setSearchCategory(subject);
    providerResultsInputBean.setPageNo(pageNo);
    providerResultsInputBean.setPhraseSearch(searchKeyword);
    providerResultsInputBean.setQualification(qualCode);
    providerResultsInputBean.setTownCity(location);
    providerResultsInputBean.setUserScore(scoreValue);
    providerResultsInputBean.setHybridStudyModeFlag(!GenericValidator.isBlankOrNull(hybridFlag) ? hybridFlag.toUpperCase() : null);
    providerResultsInputBean.setStudyMode(studyMode);
    providerResultsInputBean.setUserAgent(userAgent);
    providerResultsInputBean.setAcceptanceCriteria(!GenericValidator.isBlankOrNull(acceptanceFlag) ? acceptanceFlag.toUpperCase().replace('-', ' ') : null);
    providerResultsInputBean.setUserTrackSessionId(new SessionData().getData(request, "userTrackId"));
    providerResultsInputBean.setBasketId(basketId);
    providerResultsInputBean.setSearchUrl(searchUrl);
    providerResultsInputBean.setUserId(userId);
    //networkId
    
    String jsonInString = mapper.writeValueAsString(providerResultsInputBean);
    //System.out.println("jsonInString----" +jsonInString);
    String result = restBusiness.callWebServices(url, jsonInString);
    //System.out.println("result----" +result);    
    String domainPath = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
    String domainPathImg = CommonUtil.getImgPath(SpringConstants.WU_CONT_IMAGE,1);

    
    ProviderResultsResponse providerResultsOutputBean = mapper.readValue(result, ProviderResultsResponse.class); 
    String matchTypeDim = "";
    String match_type_likely = "";
    String match_type_stretch = "";
    String match_type_possible = "";
    Map<String, String> uniMatchTypeMap = new HashMap<String, String>();
    Map<String, String> courseMatchTypeMap = new HashMap<String, String>();
    ArrayList<ProviderResultsContentResponse> providerResultsList = providerResultsOutputBean.getProviderResultsList();
    if (!CollectionUtils.isEmpty(providerResultsList)) {
    	
    	for(ProviderResultsContentResponse providerResults : providerResultsList) {
  		
    		providerResults.setCourseDetailPageUrl(seoUrls.construnctCourseDetailsPageURL(providerResults.getStudyLevelText(), providerResults.getCourseTitle(), providerResults.getCourseId(), providerResults.getCollegeId(), GlobalConstants.CLEARING_PAGE_FLAG));

	        String course_match_type_likely = "";
	        String course_match_type_stretch = "";
	        String course_match_type_possible = "";
    		if(StringUtils.isNotBlank(providerResults.getScoreLevel())) {
      if("LIKELY".equalsIgnoreCase(providerResults.getScoreLevel())) {
    	  course_match_type_likely  = GlobalConstants.LIKELY_MATCH_TYPE;
    	  match_type_likely  = GlobalConstants.LIKELY_MATCH_TYPE;
      }
      if("POSSIBLE".equalsIgnoreCase(providerResults.getScoreLevel())) {
    	  course_match_type_possible  = GlobalConstants.POSSIBLE_MATCH_TYPE;
    	  match_type_possible  = GlobalConstants.POSSIBLE_MATCH_TYPE;
      }
      if("STRETCH".equalsIgnoreCase(providerResults.getScoreLevel())) {
    	  course_match_type_stretch  = GlobalConstants.STRETCH_MATCH_TYPE;
    	  match_type_stretch  = GlobalConstants.STRETCH_MATCH_TYPE;
        }
    }
    matchTypeDim = course_match_type_likely+"|"+course_match_type_possible+"|"+course_match_type_stretch;
    providerResults.setMatchTypeDim(matchTypeDim);
    courseMatchTypeMap.put(providerResults.getCourseId(), matchTypeDim);
    String allCourseTitle = providerResultsList.stream().map(cour -> cour.getCourseTitle()).collect(Collectors.joining("###"));
	String allCourseId = providerResultsList.stream().map(cour -> cour.getCourseId()).collect(Collectors.joining("###"));
	model.addAttribute("allCourseTitle", allCourseTitle);
	model.addAttribute("allCourseId", allCourseId);
    }    
    	ProviderResultsContentResponse providerResultsContentResponse = (ProviderResultsContentResponse) providerResultsList.get(0);
    	recordCount = providerResultsContentResponse.getCourseCount();
    	
    	model.addAttribute("providerResultsList", providerResultsList);
		request.setAttribute("courseList", providerResultsList);
	    matchTypeDim = match_type_likely+"|"+match_type_possible+"|"+match_type_stretch;
	    uniMatchTypeMap.put(providerResultsContentResponse.getCollegeId(), matchTypeDim);
	    model.addAttribute("matchTypeDim", matchTypeDim);    
	    model.addAttribute("courseMatchTypeMap", courseMatchTypeMap);
	    model.addAttribute("uniMatchTypeMap", uniMatchTypeMap);	
	    
    }else {
        request.setAttribute("reason_for_404","--(1.1)--> College doesn't exists");
        response.sendError(404, "404 error message");
        return null;
    }
    ArrayList<StudyModeResponse> studyModeList = providerResultsOutputBean.getStudyModeList();
    ArrayList<QualificationResponse> qualificationList = providerResultsOutputBean.getQualificationList();
    ArrayList<CollegeDetailsResponse> collegeDetails = providerResultsOutputBean.getCollegeDetails();    
    ArrayList<StatsResponse> statsLogList = providerResultsOutputBean.getStatsLogList();
    String pixelTrackingCode = providerResultsOutputBean.getPixelTrackingCode();
    String advertiserFlag = providerResultsOutputBean.getAdvertiserFlag();
    String invalidCategoryFlag = providerResultsOutputBean.getInvalidCategoryFlag();
    String studyModeId = providerResultsOutputBean.getStudyModeId();
    String showSwitchFlag = providerResultsOutputBean.getShowSwitchFlag();
    ArrayList<AcceptanceResponse> acceptanceList = providerResultsOutputBean.getAcceptanceCriteriaList();
    String searchSubject = providerResultsOutputBean.getBrowseCategoryDesc();
    String searchPhrase = providerResultsOutputBean.getSearchPhrase();
    
    String selectedQual = (!CollectionUtils.isEmpty(qualificationList)) ? qualificationList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getQualDisplayDesc()).collect(Collectors.joining("")) : "";
    String selectedStudyMode = (!CollectionUtils.isEmpty(studyModeList)) ? studyModeList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getDisplayStudyModeDesc()).collect(Collectors.joining("")) : "";
    //String selectedRegion = (!CollectionUtils.isEmpty(regionList))  ? regionList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getRegionName()).collect(Collectors.joining("")) : "";
    //String selectedLocType = (!CollectionUtils.isEmpty(uniLocationTypeList)) ? uniLocationTypeList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getLocTypeDesc()).collect(Collectors.joining("")) : "";
    //String selectedAcceptance = (!CollectionUtils.isEmpty(acceptanceList)) ? acceptanceList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(i -> i.getDispAcceptanceDec()).collect(Collectors.joining("")) : "";
    
    model.addAttribute("selectedQual", selectedQual);
    model.addAttribute("selectedStudyMode", selectedStudyMode);
   // model.addAttribute("selectedRegion", selectedRegion);
   // model.addAttribute("selectedLocType", selectedLocType);
    //model.addAttribute("selectedAcceptance", selectedAcceptance);
    model.addAttribute("searchSubject", searchSubject);
    model.addAttribute("searchPhrase", searchPhrase);
    
    String qualification = providerResultsInputBean.getQualification();
    String postcode = new CommonFunction().replacePlus(util.toUpperCase(location));
    String browseCatDesc = null;
    String categoryCode = null;
    String categoryId = null;
    String collegeId = null;
    String uniReviewUrl = null;
    String uniHomeUrl = null;
    String collegeName = null;
    String gaCollegeName= null;
    if(!CollectionUtils.isEmpty(statsLogList)) {
      StatsResponse statsList = statsLogList.get(0);
      browseCatDesc = statsList.getBrowseCatDesc();
      categoryCode = statsList.getBrowseCatCode();  
      categoryId = statsList.getBrowseCatId();
      collegeName = statsList.getCollegeName();
      model.addAttribute("gaCollegeName", new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName));
    }
    if (!CollectionUtils.isEmpty(collegeDetails)) {
      CollegeDetailsResponse listOfcollegeDetails = collegeDetails.get(0);
      collegeId = listOfcollegeDetails.getCollegeId();
      uniReviewUrl = new SeoUrls().constructReviewPageSeoUrl(listOfcollegeDetails.getCollegeDisplayName(), listOfcollegeDetails.getCollegeId());
      uniHomeUrl = new SeoUrls().construnctProfileURL(listOfcollegeDetails.getCollegeId(), listOfcollegeDetails.getCollegeTextKey(), GlobalConstants.CLEARING_PAGE_FLAG);
      String hotLine = listOfcollegeDetails.getHotLineNo();
      String website = listOfcollegeDetails.getWebsite();
      String networkId = listOfcollegeDetails.getNetworkId();
      String orderItemId = listOfcollegeDetails.getOrderItemId();
      listOfcollegeDetails.setUniReviewUrl(uniReviewUrl);
      listOfcollegeDetails.setUniHomeUrl(uniHomeUrl);
      model.addAttribute("website", website);
      model.addAttribute("hotLine", hotLine);
      model.addAttribute("collegeId", collegeId);
      model.addAttribute("networkId", networkId);
      model.addAttribute("orderItemId", orderItemId);
      model.addAttribute("listOfcollegeDetails", collegeDetails);
    }
    model.addAttribute("subjectDesc", GenericValidator.isBlankOrNull(browseCatDesc) ? null : browseCatDesc);
    model.addAttribute("searchText", GenericValidator.isBlankOrNull(browseCatDesc) ? null : browseCatDesc);
    if (!CollectionUtils.isEmpty(providerResultsList)) {
      ProviderResultsContentResponse providerResultsDetails = providerResultsList.get(0);
      model.addAttribute("courseCount", providerResultsDetails.getCourseCount());
    }
    String searchCategory = util.toUpperCase(categoryCode);
    String qual = qualification;
    if ("all".equalsIgnoreCase(selQual)) {
    if(!CollectionUtils.isEmpty(qualificationList)) {
    	for(QualificationResponse qualList : qualificationList) {
    		if("M".equalsIgnoreCase(qualList.getQualCode())) {
                searchCategory = qualList.getCategoryCode();
    		}
    	}         
    }
        qual = "M";
    }
      String breadCrumb = "";
     // breadCrumb = new SeoUtilities().getBreadCrumb("CLEARING_PROVIDER_RESULT_PAGE", qual, "", searchCategory, postcode, collegeId);
      breadCrumb = new SeoUtilities().getKeywordBreadCrumb(request, "CLEARING_PROVIDER_RESULT_PAGE", qual, categoryId, searchCategory, postcode, collegeId, searchKeyword);
            
      if (breadCrumb != null && breadCrumb.trim().length() > 0) {
        model.addAttribute("breadCrumb", breadCrumb);
      }
      
    URL_1 = URL_STRING;            
    firstPageSeoUrl = URL_1 + "?" + URL_QUR_STRING;  
    int numofpages = 0;
    int noOfrecords = 0;
    if(StringUtils.isNotBlank(recordCount)){
    noOfrecords =  Integer.parseInt(recordCount.replaceAll(",", ""));
    }
    numofpages =  noOfrecords / 10;
    if(noOfrecords % 10 > 0) {  numofpages++; }
    String paginationPageName = "newPagination";
    if(numofpages >1 && ("1").equals(pageNo)){  
    paginationPageName = "searchInitialPagination";
    }
    String studyLevelDesc = common.getStudyLevelDesc(util.toUpperCase(studyLevel), request);
    String searchSubjectName = browseCatDesc;
    if(GenericValidator.isBlankOrNull(searchSubjectName)) {
      searchSubjectName = searchPhrase;
    }
    String gamKeywordOrSubject = ""; 
    String courseHeader = (categoryCode != null && !categoryCode.equalsIgnoreCase("null") && categoryCode.trim().length() > 0 ? globalFn.getCategoryDescFromBrowseCategories(categoryCode,studyLevel) : ((searchSubjectName != null && !searchSubjectName.equalsIgnoreCase("null") && searchSubjectName.trim().length() > 0) ? util.toTitleCase(searchSubjectName) :  ""));
    gamKeywordOrSubject = courseHeader;
    if (GenericValidator.isBlankOrNull(gamKeywordOrSubject) && !GenericValidator.isBlankOrNull(searchKeyword)) {
      gamKeywordOrSubject = searchKeyword.replaceAll("-", " ").toLowerCase();
    }
    
    session.removeAttribute("gamKeywordOrSubject");
    session.removeAttribute("gamStudyLevelDesc");
    
    session.setAttribute("gamKeywordOrSubject", gamKeywordOrSubject);
    request.setAttribute("gamKeywordOrSubject", gamKeywordOrSubject);
    session.setAttribute("gamStudyLevelDesc", studyLevelDesc);
    request.setAttribute("insightPageFlag", "yes");
    if (categoryCode != null && categoryCode.trim().length() > 0) {
      request.setAttribute("dimCategoryCode", new GlobalFunction().getReplacedString(categoryCode));
    } else {
      if (!GenericValidator.isBlankOrNull(gamKeywordOrSubject)) {
        request.setAttribute("cDimKeyword", new GlobalFunction().getReplacedString(gamKeywordOrSubject.toLowerCase()));
      }
    }
    if (studyLevel != null && studyLevel.trim().length() > 0) {
      request.setAttribute("qualification", qualification);
      new GlobalFunction().getStudyLevelDesc(studyLevel, request);
    }
    if (!GenericValidator.isBlankOrNull(universityName)) {
      request.setAttribute("cDimCollegeDisName",("\"" + universityName + "\""));
    }
    request.setAttribute("cDimCollegeId", GenericValidator.isBlankOrNull(collegeId) ? "" : collegeId.trim());
    if (!GenericValidator.isBlankOrNull(collegeId)) {
      request.setAttribute("cDimCollegeIds",("'" + collegeId.trim() + "'"));
    }
    if (!GenericValidator.isBlankOrNull(location)) { // Added by Prabha for insight(dimension-13) on 29_Mar_2016
      request.setAttribute("location",common.replaceHypenWithSpace(location).toUpperCase());
    }
    if ("degree".equalsIgnoreCase(selQual)) {
      request.setAttribute("CANONICAL_URL", "CANONICAL_URL");
    }
    if ("all".equalsIgnoreCase(selQual)) {
      request.setAttribute("meta_robot", "noindex,follow");
    }
	boolean mobileFlag = MobileUtils.userAgentCheck(request);
	request.setAttribute("SHOW_SWITCH_FLAG", showSwitchFlag); 
    model.addAttribute("URL_1", URL_1);
    model.addAttribute("URL_2", URL_2);
    //model.addAttribute("resultExists", resultExists);
    model.addAttribute("firstPageSeoUrl", firstPageSeoUrl);
    model.addAttribute("recordCount", recordCount);
    model.addAttribute("pageNo", pageNo);
    model.addAttribute("pageno", pageNo);
    model.addAttribute("paginationPageName", paginationPageName);
    model.addAttribute("domainPathImg", domainPathImg);
    model.addAttribute("university", universityName);
    model.addAttribute("scoreValue", scoreValue);
    model.addAttribute("studyMode", studyMode);
    model.addAttribute("hybridFlag", hybridFlag);
	model.addAttribute("domainPath", domainPath);
    model.addAttribute("qualificationList", qualificationList);
    model.addAttribute("studyModeList", studyModeList);
    //model.addAttribute("acceptanceList", acceptanceList);
    model.addAttribute("acceptanceFlag", acceptanceFlag);
	model.addAttribute("mobileFlag", mobileFlag);
	model.addAttribute("subjectText", subject);
	model.addAttribute("queryStr", URL_QUR_STRING);
	model.addAttribute("pixelTrackingCode", pixelTrackingCode);
	model.addAttribute("sortBy", searchHow);
	  
    model.addAttribute("subject", subject);
    model.addAttribute("qualCode", qualCode);
	model.addAttribute("paramCategoryCode", categoryCode);
	model.addAttribute("instNames", universityName);
    model.addAttribute("qual", selQual);
	if (!GenericValidator.isBlankOrNull(selQual) && !"all".equalsIgnoreCase(selQual)) {
		request.setAttribute("paramStudyLevelId", qualCode);
	} else {
		request.setAttribute("paramStudyLevelId", "");
	}
	String subjectName = null;
	if (GenericValidator.isBlankOrNull(searchSubject)) {
		subjectName = searchKeyword;
	    }else{
	    	subjectName = searchSubject;
	     }
	    request.setAttribute("subjectL1", subjectName);
	    request.setAttribute("subjectName", subjectName);
		model.addAttribute("subject", subject);
	    if (!GenericValidator.isBlankOrNull(request.getQueryString())) {
	        if(URL_QUR_STRING.contains("subject=") || URL_QUR_STRING.contains("university=")){
	  	    request.setAttribute("meta_robot", "index,follow");
	        }
	        if(URL_QUR_STRING.contains("location=") || URL_QUR_STRING.contains("campus-type=") || URL_QUR_STRING.contains("study-mode=") || URL_QUR_STRING.contains("hybrid=") 
	            || URL_QUR_STRING.contains("accommodation=") || URL_QUR_STRING.contains("scholarship=") || URL_QUR_STRING.contains("sort=")
	            || URL_QUR_STRING.contains("score=") || URL_QUR_STRING.contains("location-type=") || URL_QUR_STRING.contains("acceptance=")){ 
	            request.setAttribute("meta_robot", "noindex,follow");
	        }
	      }
		  model.addAttribute("pageName", "CLEARING_PROVIDER_RESULTS");
    String COVID19_SYSVAR = new SessionData().getData(request, GlobalConstants.COVID19_SYSVAR);
    model.addAttribute("COVID19_SYSVAR", COVID19_SYSVAR);
    return new ModelAndView("clearingProviderResultsPage");
  }
  
  @RequestMapping(value = "/clearing/get-subject-ajax", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getSubjecctAjaxList(@RequestParam Map<String, String> requestParam, ModelMap model, SubjectAjaxRequest subjectAjaxRequest, HttpServletRequest request, HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException, Exception {
    ObjectMapper mapper = new ObjectMapper();
    CommonUtil utilities = new CommonUtil();
    String url = utilities.generateWebServiceUrl(SpringConstants.MAPPING_FOR_SUBJECT_AJAX);
    String collegeId = GenericValidator.isBlankOrNull(requestParam.get("id"))? null : (requestParam.get("id"));
    String qualification = GenericValidator.isBlankOrNull(requestParam.get("qual"))? null : (requestParam.get("qual"));

    subjectAjaxRequest.setWebsiteAppCode("WU");
    subjectAjaxRequest.setQualification(qualification);
    subjectAjaxRequest.setCollegeId(collegeId);
    String jsonInString = mapper.writeValueAsString(subjectAjaxRequest);
    //System.out.println("jsonInString----" +jsonInString);
    String result = restBusiness.callWebServices(url, jsonInString);
    //System.out.println("result----" +result);   

    SubjectAjaxResponse subjectAjaxOutputBean = mapper.readValue(result, SubjectAjaxResponse.class); 
    ArrayList<SubjectAjaxListResponse> subjectAjaxList = (ArrayList<SubjectAjaxListResponse>) subjectAjaxOutputBean.getSubjectAjaxList();
    model.addAttribute("subjectAjaxList", subjectAjaxList);
    
    return new ModelAndView("clearingSearchSubjectAjax");
  }
}
