package spring.controller.contenful;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.layer.util.SpringConstants;
import WUI.utilities.CommonUtil;
import spring.business.IRestBusiness;

@Controller
public class GetContentfuldata {

  @Autowired
  IRestBusiness restBusiness = null;

  @RequestMapping(value = "/get-contentful-data", method = {RequestMethod.GET, RequestMethod.POST})
  public ArrayList<String> getContentfulData(@RequestParam String contentType,@RequestParam String fieldName,@RequestParam String fieldValue) throws JsonProcessingException {
    CommonUtil utilities = new CommonUtil();
    String preview = "false";
    String environment = "master";
    String url = utilities.generateContentfulServiceUrl(SpringConstants.MAPPING_FOR_CONTENTFUL_WEBSERVICE);

    Map<String, String> inputMap = new HashMap<String, String>();
    if (StringUtils.isNotBlank(contentType)) {
      inputMap.put("contentType", contentType);
    }
    if (StringUtils.isNotBlank(fieldName)) {
      inputMap.put("fieldsValue", fieldValue);
    }
    if (StringUtils.isNotBlank(fieldValue)) {
      inputMap.put("fieldsName", fieldName);
    }
    inputMap.put("preview", preview);
    inputMap.put("environment", environment);

    String jsonResponse = restBusiness.callContentfulWebServices(url, inputMap);
    JSONObject jsonObject = new JSONObject(jsonResponse);
    ArrayList<String> result = new ArrayList<String>();
    result = setProperty(jsonObject, "items", result);
    
    result = (ArrayList<String>) result.stream().distinct().collect(Collectors.toList());
    if (result != null) {
      return result;
    }
    return null;
  }

  public static String getJsonValue(JSONObject json, String key) {
    boolean exists = json.has(key);
    Iterator keys;
    String nextKeys;
    String val = null;
    if (!exists) {
      keys = json.keys();
      while (keys.hasNext()) {
        nextKeys = (String) keys.next();
        try {
          if (json.get(nextKeys) instanceof JSONObject) {
            return getJsonValue(json.getJSONObject(nextKeys), key);
          } else if (json.get(nextKeys) instanceof JSONArray) {
            JSONArray jsonArray = json.getJSONArray(nextKeys);
            int i = 0;
            if (i < jsonArray.length())
              do {
                String jsonArrayString = jsonArray.get(i).toString();
                JSONObject innerJson = new JSONObject(jsonArrayString);
                return getJsonValue(innerJson, key);
              } while (i < jsonArray.length());
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } else {
      val = json.get(key).toString();
    }
    return val;
  }

  public static ArrayList<String> setProperty(JSONObject js1, String keys,ArrayList<String> result) throws JSONException {
    String[] keyMain = keys.split("\\.");
    int idx = 0;
    for (String keym : keyMain) {
      Iterator iterator = js1.keys();
      String key = null;
      while (iterator.hasNext()) {
        key = (String) iterator.next();
        if ((js1.optJSONArray(key) == null) && (js1.optJSONObject(key) == null)) {
          if ((key.equals(keym))) {
            String res = getJsonValue(js1, "slugUrl");
            if(StringUtils.isNoneBlank(res)) {
              result.add(res);              
            }
          }
        }
        if (js1.optJSONObject(key) != null) {
          if ((key.equals(keym))) {
            js1 = js1.getJSONObject(key);
            String res = getJsonValue(js1, "slugUrl");
            if(StringUtils.isNoneBlank(res)) {
              result.add(res);              
            }
          }
        }
        if (js1.optJSONArray(key) != null) {
          JSONArray jArray = js1.getJSONArray(key);
          JSONObject j;
          for (int i = 0; i < jArray.length(); i++) {
            js1 = jArray.getJSONObject(i);
            setProperty(js1, "fields", result);
          }
        }
      }
    }
    return result;
  }
}
