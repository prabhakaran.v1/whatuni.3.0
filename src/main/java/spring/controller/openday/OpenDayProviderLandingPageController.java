package spring.controller.openday;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.layer.util.SpringConstants;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;

import org.springframework.util.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import spring.business.IOpenDayBusiness;
import spring.dao.util.valueobject.openday.AdditionalResourcesInfoVO;
import spring.dao.util.valueobject.openday.ContentSectionInfoVO;
import spring.dao.util.valueobject.openday.GalleryInfoVO;
import spring.dao.util.valueobject.openday.LocationInfoVO;
import spring.dao.util.valueobject.openday.OpendayInfoVO;
import spring.dao.util.valueobject.openday.ProviderInfoVO;
import spring.form.openday.OpenDayProviderLandingPageBean;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
 * @OpenDayProviderLandingPageController
 * @author   Sujitha V
 * @version  1.0
 * @since    06_05_2020
 * @purpose  This controller is used to display the open day provider landing page.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
 * Kailash L              1.1                 06_05_2020       Added ajax method for light box   
 * Sri Sankari R          1.1                 20-10-2020       Added dynamic meta details from Back office
*/
@SuppressWarnings("unchecked")
@Controller
public class OpenDayProviderLandingPageController {

  @Autowired
  private IOpenDayBusiness openDayBusiness;
  @Autowired 
  private CommonFunction commonFunction;
  @Autowired
  private CommonUtil utilities;
  @Autowired
  private GlobalFunction globalFn;

  /**
   * 
   * @param pathVar
   * @param eventId
   * @param model
   * @param request
   * @param response
   * @param session
   * @return
   * @throws Exception
   */
  @RequestMapping(value = { "/open-days/{collegeName}/{collegeId}" }, method = RequestMethod.GET)
  public ModelAndView getProviderOpenDay(@PathVariable Map<String, String> pathVar, @RequestParam(value = "eventId", required = false) String eventId, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

	CollegeNamesVO uniNames = null;
	final String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html","");
	eventId = StringUtils.isBlank(eventId) ? null : eventId;
	String userId = new SessionData().getData(request, "y");
	userId = (StringUtils.isNotBlank(userId) && !StringUtils.equals(userId, "0")) ? userId : null;
	OpenDayProviderLandingPageBean openDayBean = new OpenDayProviderLandingPageBean();
	String collegeId = StringUtils.isNotBlank(pathVar.get(SpringConstants.COLLEGE_ID)) ? pathVar.get(SpringConstants.COLLEGE_ID) : "";
	String collegeName = StringUtils.isNotBlank(pathVar.get(SpringConstants.COLLEGE_NAME)) ? pathVar.get(SpringConstants.COLLEGE_NAME): "";
    String cpeQualificationNetworkId = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID)) ? (String)session.getAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID) : null; 
    openDayBean.setUserId(userId);
	openDayBean.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
	openDayBean.setCollegeId(collegeId);
	openDayBean.setAppFlage(SpringConstants.APP_FLAG);
	openDayBean.setDisplayCount("3");
	openDayBean.setEventId(eventId);
    openDayBean.setNetworkId(cpeQualificationNetworkId);
	openDayBean.setRandomNumer(utilities.getSessionRandomNumber(request, session));
	String canonicalUrl = commonFunction.getFullURL(request, true);
	canonicalUrl = StringUtils.replaceEach(canonicalUrl, new String[]{"/degrees",".html"}, new String[]{"","/"});
    canonicalUrl = StringUtils.substringBefore(canonicalUrl, "?");
    canonicalUrl = StringUtils.substringBefore(canonicalUrl, "#");
    openDayBean.setPageUrl(canonicalUrl);
	model.addAttribute(SpringConstants.COLLEGE_NAME, collegeName);
	model.addAttribute(SpringConstants.COLLEGE_ID, collegeId);
	model.addAttribute("eventId", eventId);
	Map<String, Object> openDayMap = openDayBusiness.getOpenDayProviderLandingList(openDayBean);
	if(!CollectionUtils.isEmpty(openDayMap)){
	  ArrayList<ProviderInfoVO> uniInforList = (ArrayList<ProviderInfoVO>) openDayMap.get("PC_UNI_INFO");
	  if(!CollectionUtils.isEmpty(uniInforList)){
		model.addAttribute("uniInforList", uniInforList);
	  }

  	  ArrayList<OpendayInfoVO> uniOpenDaysList = (ArrayList<OpendayInfoVO>) openDayMap.get("PC_UNI_OPENDAYS_LIST");
	  if(!CollectionUtils.isEmpty(uniOpenDaysList)){
		model.addAttribute("uniOpenDaysList", uniOpenDaysList);
		OpendayInfoVO selectOpenDayInfoVO =  uniOpenDaysList.get(0);
		model.addAttribute("moreOpenDayFlag", selectOpenDayInfoVO.getMoreOpenDayFlag());
		model.addAttribute("totalOpenDayCount", selectOpenDayInfoVO.getTotalOpenDayCount());
		model.addAttribute("eventTime", selectOpenDayInfoVO.getStartTime());
		String gaCollegeName = StringUtils.isNotBlank(selectOpenDayInfoVO.getCollegeName()) ? selectOpenDayInfoVO.getCollegeName() : "";
		gaCollegeName= commonFunction.replaceSpecialCharacter(globalFn.getReplacedString(gaCollegeName));
		model.addAttribute("gaCollegeName", gaCollegeName);
		/* This is for GA Logging - Here adding the qual type has studyLevelDesc */
		String qualType = selectOpenDayInfoVO.getQualTypeGA();
		String[] qualTypeSplit = qualType.split(" ");
		String levelType = "";
		if(qualTypeSplit != null && qualTypeSplit.length > 0){
		  if(qualTypeSplit[0].equalsIgnoreCase("Undergraduate")) {
			 levelType = "ug"; 
		  } else if(qualTypeSplit[0].equalsIgnoreCase("Postgraduate"))  {
			 levelType = "pg"; 
		  } else if(qualTypeSplit[0].equalsIgnoreCase("Undergraduate/Postgraduate")) {
			 levelType = "all"; 
		  } else {
			 levelType = ""; 
		  }
		  if(StringUtils.isNotBlank(levelType)){
			model.addAttribute("studyLevelDesc", levelType);
		  }
		}
	  String openDayEventType = "";
  	  for(int i=0;i < uniOpenDaysList.size(); i++) {
  		selectOpenDayInfoVO  = uniOpenDaysList.get(i);
  		openDayEventType = utilities.setEventTypeValueGA(openDayEventType, selectOpenDayInfoVO.getEventCategoryName());			
        }
  	  if(!GenericValidator.isBlankOrNull(openDayEventType)) {
  	    openDayEventType = openDayEventType.substring(0, openDayEventType.length() - 1);
  	    model.addAttribute("openDayEventType", openDayEventType);//For logging open day event type in dimension 18 by sangeeth.S in APR-24-20
  	  }	
		/* End of GA Logging */
	  }

	  ArrayList<GalleryInfoVO> gallerySectionList = (ArrayList<GalleryInfoVO>) openDayMap.get("PC_HERO_IMAGE");
	  if(!CollectionUtils.isEmpty(gallerySectionList)){
		model.addAttribute("gallerySectionList", gallerySectionList);
		GalleryInfoVO galleryInfoVO = gallerySectionList.get(0);
		model.addAttribute("collegeNameDisplayGallery", galleryInfoVO.getCollegeNameDisplay());
	  }

	  ArrayList<ContentSectionInfoVO> contentSectionList = (ArrayList<ContentSectionInfoVO>) openDayMap.get("PC_CONTENT_SECTION");
	  if(!CollectionUtils.isEmpty(contentSectionList)){
		model.addAttribute("contentSectionList", contentSectionList);
		ContentSectionInfoVO contentSectionInfoVO = contentSectionList.get(0);
		model.addAttribute("selectedOpenDate", StringUtils.isNotBlank(contentSectionInfoVO.getSelectedOpenDate()) ? contentSectionInfoVO.getSelectedOpenDate() : "");
	  }

	  ArrayList<LocationInfoVO> locationDetailsList = (ArrayList<LocationInfoVO>) openDayMap.get("PC_UNI_ADDRESS");
	  if(!CollectionUtils.isEmpty(locationDetailsList)){
		model.addAttribute("locationDetailsList", locationDetailsList);
	  }

	  ArrayList<ContentSectionInfoVO> testimonialSectionList = (ArrayList<ContentSectionInfoVO>) openDayMap.get("PC_TESTIMONIALS");
	  if(!CollectionUtils.isEmpty(testimonialSectionList)){
		model.addAttribute("testimonialSectionList", testimonialSectionList);
	  }

	  ArrayList<AdditionalResourcesInfoVO> additionResourcesList = (ArrayList<AdditionalResourcesInfoVO>) openDayMap.get("PC_ADDITIONAL_CONTENT");
	  if(!CollectionUtils.isEmpty(additionResourcesList)){
		model.addAttribute("additionResourcesList", additionResourcesList);
	  }

	  ArrayList<OpendayInfoVO> bookingFormDetailsList = (ArrayList<OpendayInfoVO>) openDayMap.get("PC_EVENT_DETAILS");
	  if(!CollectionUtils.isEmpty(bookingFormDetailsList)){
		model.addAttribute("bookingFormDetailsList", bookingFormDetailsList);
	  }
	  
	  String breadCrumb = (String) openDayMap.get("P_BREAD_CRUMB");
	  if(StringUtils.isNotBlank(breadCrumb)){
		model.addAttribute("breadCrumb", breadCrumb);
	  }

	  // Added for dynamic meta data details from Back office, 20201020 rel by Sri Sankari
	  ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) openDayMap.get("PC_META_DETAILS");
	  new CommonUtil().iterateMetaDetails(seoMetaDetailsList, model);
	  if(StringUtils.isNotBlank(URL_STRING)){
		model.addAttribute("openDayProviderLandingURL", URL_STRING);
	  }
	}
	
    if(StringUtils.isNotBlank(collegeId) && !StringUtils.equals(collegeId, "0")){
	  uniNames = new CollegeUtilities().getCollegeNames(collegeId, request);
	  if(uniNames != null){
		String collegeNameGA = uniNames.getCollegeName();
		model.addAttribute("hitbox_college_name", collegeNameGA);
		model.addAttribute("collegeNameBreadCrumb", collegeNameGA);
	  }
	}
    if(StringUtils.isNotBlank(userId)){
	  request.setAttribute("cDimUID", userId);
	}
    
	model.addAttribute("curActiveMenu", "opendays");
	model.addAttribute("provCanonicalUrl", canonicalUrl);
   
    String tabFlag = StringUtils.isNotBlank((String) openDayMap.get("P_SHOW_UG_TAB_FLAG")) ? (String) openDayMap.get("P_SHOW_UG_TAB_FLAG") : "";
    tabFlag = tabFlag.equalsIgnoreCase("Y") ? "2" : "3";
    
    if(StringUtils.isBlank(cpeQualificationNetworkId)) {
      session.setAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID, tabFlag);
    }
    
	return new ModelAndView("openday-provider-landing-page");
  }
  /**
   * This method is used to display the light box open day events 
   * @param model
   * @param request
   * @param response
   * @param session
   * @return
   * @throws Exception
   */
  @RequestMapping(value = { "/ajax/opendaylightbox" }, method = RequestMethod.POST)
  public ModelAndView ajaxOpenDayLightBox(@RequestParam Map<String, String> requestParam, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		String collegeId = StringUtils.isNotBlank(requestParam.get(SpringConstants.COLLEGE_ID)) ? requestParam.get(SpringConstants.COLLEGE_ID) : "";
		String displayOpenDayCount = StringUtils.isNotBlank(requestParam.get(SpringConstants.DISPLAY_OPENDAY_COUNT)) ? requestParam.get(SpringConstants.DISPLAY_OPENDAY_COUNT) : "";
		String networkId = StringUtils.isNotBlank(requestParam.get("networkId")) ? requestParam.get("networkId") : "";
		OpenDayProviderLandingPageBean openDayBean = new OpenDayProviderLandingPageBean();
		openDayBean.setCollegeId(collegeId);
		openDayBean.setDisplayCount(displayOpenDayCount);
		openDayBean.setAppFlage(SpringConstants.APP_FLAG);
		openDayBean.setNetworkId(networkId);
		openDayBean.setBookingFormFlag(null);
		openDayBean.setEventId(null);
        openDayBean.setRandomNumer(null);
		model.addAttribute(SpringConstants.COLLEGE_ID, collegeId);
		model.addAttribute(SpringConstants.DISPLAY_OPENDAY_COUNT, displayOpenDayCount);
		Map<String, Object> resultMap = openDayBusiness.getOpenDayLightBox(openDayBean);
		if (!CollectionUtils.isEmpty(resultMap)) {
			ArrayList<OpendayInfoVO> uniOpenDaysList = (ArrayList<OpendayInfoVO>) resultMap.get("PC_LIGHTBOX_LIST");
			if (!CollectionUtils.isEmpty(uniOpenDaysList)) {
				model.addAttribute("uniOpenDaysList", uniOpenDaysList);
			}
		}
		return new ModelAndView("opendaylightbox");
	}

}
