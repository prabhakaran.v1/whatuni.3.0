package spring.controller.searchresult;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;
import com.wuni.valueobjects.whatunigo.GradeFilterVO;

import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;
import spring.business.IRestBusiness;
import spring.gradefilter.bean.SRGradeFilterBean;
import spring.pojo.clearing.common.GradeFilterRequest;
import spring.pojo.clearing.common.GradeFilterResponse;
import spring.valueobject.gradefilter.SRGradeFilterVO;

@Controller
public class SRGradeFilterInfoController {
	@Autowired
	IRestBusiness restBusiness = null;

	
	CommonUtil commonUtil = new CommonUtil();

	SessionData sessionData = new SessionData();
	// private static final Logger LOGGER=Logger.getLogger(GetGradeFilterInfoController.class);

	@RequestMapping(value = { "/sr-grade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView getGradeFilterInfo(SRGradeFilterBean srgradeFilterBean, HttpServletRequest request, HttpServletResponse	response, HttpSession session) throws JsonParseException, JsonMappingException, IOException {
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ?	sessionData.getData(request, "y") : "0";
		String pageName = !GenericValidator.isBlankOrNull(request.getParameter("pageName")) ? request.getParameter("pageName"): "";
		String subjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		srgradeFilterBean.setQualId(StringUtils.isNotBlank(srgradeFilterBean.getQualId()) ? srgradeFilterBean.getQualId() : null);
		srgradeFilterBean.setUserId(userId);
		srgradeFilterBean.setUserTrackSessionId(subjectSessionId);
		ObjectMapper mapper = new ObjectMapper();
		 ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		 Map<String, Object> resultMap = null;
		 resultMap = commonBusiness.getQualificationList(srgradeFilterBean);
		 ArrayList<SRGradeFilterVO> qualList = (ArrayList<SRGradeFilterVO>)resultMap.get("PC_QUALIFICATION_LIST");
		 String userStudyLevel = (String)resultMap.get("P_USER_QUAL_STR");
		 String userEntryPoints = (String)resultMap.get("P_USER_GRADE_STR");
		 String userUcasPoints = (String)resultMap.get("P_UCAS_POINTS");
		 String sessionId = (String)resultMap.get("P_SESSION_ID");
		if (qualList != null && qualList.size() > 0) {
			qualList.stream().forEach(grdList -> {
				if("14".equals(grdList.getQualId())){
					grdList.setQualification("IB (Diploma) Higher Level");
			    }else if("15".equals(grdList.getQualId())){
			    	grdList.setQualification("IB (Diploma) Standard Level");
			    }				
				
			});		   	 
		}
		request.setAttribute("gradeFilterList",	qualList); 
		subjectSessionId = sessionId; 
		session.setAttribute("subjectSessionId" , subjectSessionId);
		request.setAttribute("qualId",	srgradeFilterBean.getQualId()); 
		request.setAttribute("userStudyLevel",userStudyLevel); 
		request.setAttribute("userEntryPoints",	userEntryPoints);
		request.setAttribute("userUcasPoints",	userUcasPoints);
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag"));
		request.setAttribute("filterHitCount",request.getParameter("filterHitCount"));
		if("Y".equalsIgnoreCase(request.getParameter("addQualFlag")) || StringUtils.isNotBlank(srgradeFilterBean.getQualId())){
			//System.out.println("addQUalFlag" + request.getParameter("studyLevel"));
			request.setAttribute("studyLevel", ""); 
			request.setAttribute("entryPoints",	"");
			if("Y".equalsIgnoreCase(request.getParameter("removeFlag")) && StringUtils.isNotBlank(request.getParameter("studyLevel")) && StringUtils.isNotBlank(request.getParameter("studyLevel"))){
				request.setAttribute("studyLevel", request.getParameter("studyLevel")); 
				request.setAttribute("entryPoints",	request.getParameter("entryPoints"));
			}
		}else{
			request.setAttribute("studyLevel", userStudyLevel); 
			request.setAttribute("entryPoints",	userEntryPoints);	
		}		
		//request.setAttribute("studyLevel",request.getParameter("studyLevel")); 
		//request.setAttribute("entryPoints",	request.getParameter("entryPoints"));
		request.setAttribute("studyLevelBanner", request.getParameter("studyLevel"));
		request.setAttribute("page_name", pageName);
		return new ModelAndView("sr-user-grade-section");
	}
	@RequestMapping(value = { "/save-srgrade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView saveGradeFilterInfo(SRGradeFilterBean srgradeFilterBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException { 
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ?	sessionData.getData(request, "y") : "0";
		String userSubjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		String userStudyLevel =	StringUtils.isNotBlank(srgradeFilterBean.getUserStudyLevelEntry()) ? srgradeFilterBean.getUserStudyLevelEntry() : request.getParameter("studyLevel");
		String userEntryPoint =	StringUtils.isNotBlank(srgradeFilterBean.getUserEntryPoint()) ? srgradeFilterBean.getUserEntryPoint() :  request.getParameter("entryPoints");
		String userUCASPoint = StringUtils.isNotBlank(srgradeFilterBean.getUcasPoint()) ? srgradeFilterBean.getUcasPoint() :  request.getParameter("ucasPoint");
		srgradeFilterBean.setQualId(StringUtils.isNotBlank(srgradeFilterBean.getQualId()) ? srgradeFilterBean.getQualId() : null);
		srgradeFilterBean.setUserId(userId);
		srgradeFilterBean.setUserTrackSessionId(userSubjectSessionId);//p_subject_session
		srgradeFilterBean.setUserStudyLevelEntry(userStudyLevel);
		srgradeFilterBean.setUserEntryPoint(userEntryPoint);
		if(StringUtils.isNotBlank(userUCASPoint)){       
	      session.setAttribute("USER_UCAS_SCORE", userUCASPoint);
	    }
		ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		Map<String, Object> resultMap = null;  
		resultMap = commonBusiness.saveSRGradeFilterInfo(srgradeFilterBean); 
		request.setAttribute("qualId",srgradeFilterBean.getQualId()); 
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag")); 
		request.setAttribute("filterHitCount", request.getParameter("filterHitCount"));
		request.setAttribute("studyLevel",userStudyLevel); 
		request.setAttribute("entryPoints", userEntryPoint);
		request.setAttribute("studyLevelBanner", userStudyLevel); 
		String returnString	= ""; 
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/html"); response.setHeader("Cache-Control","no-cache"); returnString = "save"; returnString += "##SPLIT##";
		buffer.append(returnString); 
		CommonUtil.setResponseText(response, buffer);
		return null; 
	}
	@RequestMapping(value = { "/delete-srgrade-filter-info" }, method = {RequestMethod.POST, RequestMethod.GET })
	public ModelAndView deleteGradeFilterInfo(SRGradeFilterBean srgradeFilterBean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JsonParseException, JsonMappingException, IOException { 
		String userId =	StringUtils.isNotBlank(sessionData.getData(request, "y")) ? sessionData.getData(request, "y") : "0";
		String userSubjectSessionId = StringUtils.isNotBlank((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
		String userStudyLevel =	request.getParameter("studyLevel");
		String userEntryPoint =	request.getParameter("entryPoints");
		srgradeFilterBean.setQualId(StringUtils.isNotBlank(srgradeFilterBean.getQualId()) ? srgradeFilterBean.getQualId() : null);
		srgradeFilterBean.setUserId(userId);
		srgradeFilterBean.setUserTrackSessionId(userSubjectSessionId);//p_subject_session
		srgradeFilterBean.setUserStudyLevelEntry(userStudyLevel);
		srgradeFilterBean.setUserEntryPoint(userEntryPoint);
		ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		Map<String, Object> resultMap = null;  
		resultMap = commonBusiness.deleteSRGradeFilterInfo(srgradeFilterBean); 
		String retVal = (String)resultMap.get("RetVal");
		request.setAttribute("retVal", retVal); 
		request.setAttribute("qualId",srgradeFilterBean.getQualId()); 
		request.setAttribute("addQualFlag",	request.getParameter("addQualFlag")); 
		request.setAttribute("filterHitCount", request.getParameter("filterHitCount"));
		request.setAttribute("studyLevel",userStudyLevel); 
		request.setAttribute("entryPoints", userEntryPoint);
		request.setAttribute("studyLevelBanner", userStudyLevel);
		if (session.getAttribute("USER_UCAS_SCORE") != null) {
	        session.removeAttribute("USER_UCAS_SCORE");
	    }
		String returnString	= ""; 
		StringBuffer buffer = new StringBuffer();
		response.setContentType("text/html"); response.setHeader("Cache-Control","no-cache"); returnString = "save"; returnString += "##SPLIT##";
		buffer.append(returnString); 
		CommonUtil.setResponseText(response, buffer);
		return null; 
	}
}
