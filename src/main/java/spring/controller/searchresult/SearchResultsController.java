package spring.controller.searchresult;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.layer.util.SpringConstants;
import com.wuni.util.RequestResponseUtils;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.OmnitureProperties;
import WUI.utilities.SearchUtilities;
import WUI.utilities.SessionData;
import mobile.util.MobileUtils;
import spring.business.ISearchResultsBusiness;
import spring.controller.clearing.ClearingSearchResultController;
import spring.pojo.clearing.search.SearchResultsRequest;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.pojo.searchresult.GetInstitutionNameBean;
import spring.pojo.searchresult.SearchResultsBean;
import spring.valueobject.search.FeaturedBrandVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.searchresult.BreadCrumbVO;
import spring.valueobject.searchresult.CourseSpecificListVO;
import spring.valueobject.searchresult.FooterSectionVO;
import spring.valueobject.searchresult.GetInstitutionNameVO;
import spring.valueobject.searchresult.LocationListVO;
import spring.valueobject.searchresult.LocationTypeVO;
import spring.valueobject.searchresult.QualificationListVO;
import spring.valueobject.searchresult.SearchResultContentsVO;
import spring.valueobject.searchresult.StudyModeVO;
import spring.valueobject.searchresult.SubjectFilterVO;
import spring.valueobject.seo.SEOMetaDetailsVO;

/**
 * @SearchResultsController
 * @author   Kailash.Lakshmanan
 * @author   Sujitha V
 * @author   Sri Sankari R
 * @version  1.0
 * @since    19_01_2021   
 * @purpose  This controller takes cares of all methods created for the Search Results page.
 * Change Log
 * *************************************************************************************************************************
 * Author	              Ver 	              Modified On     	Modification Details 	                 Change
*/

@Controller
public class SearchResultsController {

	@Autowired
	private ClearingSearchResultController clearingSearchResultController = null;
	@Autowired
	private ISearchResultsBusiness searchResultsBusiness;
	@Autowired
	private CommonFunction commonFunction;
	@Autowired
	private CommonUtil utilities;
	@Autowired
	private GlobalFunction global;
	@Autowired
	private SearchUtilities search;
	
	/**
	 * This method will return the Search Results page information
	 * 
	 * @param requestParam
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/*-courses/search", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView getSearchResultsDetails(@RequestParam Map<String, String> requestParam, ModelMap model,	HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		
		final String requestURI = request.getRequestURI().substring(request.getContextPath().length()).replace(".html","");
		final String queryString = request.getQueryString();
		String clearingFlag = new SessionData().getData(request, "CLEARING_ON_OFF");
		String filterparameters = StringUtils.isNotBlank(queryString) ? "?" + queryString : "";
		String currentPageUrl = commonFunction.getSchemeName(request) + GlobalConstants.WHATUNI_DOMAIN + requestURI	+ filterparameters;
		SearchResultsBean searchResultsBean = new SearchResultsBean();
		String domainPath = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
		String contextPath = GlobalConstants.WU_CONTEXT_PATH;
		String domainPathImg = CommonUtil.getImgPath(SpringConstants.WU_CONT_IMAGE, 1);
		String subjectNameDisplay = "";
		String seoFirstPageUrl = "";
	    String studyLevel = "";
	    String subjectCode = "";

		
		/* Clearing OFF Redirection */
		if (StringUtils.equalsIgnoreCase(clearingFlag, GlobalConstants.OFF) && StringUtils.isNotBlank(queryString) && StringUtils.indexOf(queryString.toLowerCase(), GlobalConstants.SEO_PATH_CLEARING) > -1) {
			String tempRedirectUrl = currentPageUrl.replace((GlobalConstants.SEO_PATH_CLEARING + "&"), "");
			response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
			response.sendRedirect(tempRedirectUrl);
			return null;
		}
		/* End of Clearing OFf Redirection */

		/* Clearing SR page method calling */
		SearchResultsRequest searchResultsInputBean = new SearchResultsRequest();
		if (StringUtils.containsIgnoreCase(queryString, "clearing")) {
			return clearingSearchResultController.searchResultsLanding(requestParam, model, searchResultsInputBean,
					request, response, session);
		}
		/* End of Clearing SR page method calling */

		String[] urlArray = requestURI.split("/");
		SessionData sessiondata = new SessionData();
		session = request.getSession();
		ServletContext context = request.getServletContext();
		if (commonFunction.checkSessionExpired(request, response, session, "ANONYMOUS")) { // DB_CALL
			return new ModelAndView("forward: /userLogin.html");
		}
		// STORE'S PARAM VALUE OF X, Y, A TO HASMAP UNDER THE SESSION VARIABLE SESSIONDATA //
		sessiondata.setParameterValuestoSessionData(request, response);
		sessiondata.addData(request, response, "a", GlobalConstants.WHATUNI_AFFILATE_ID);
		try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
			if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
				String fromUrl = "/home.html";
				session.setAttribute("fromUrl", fromUrl);
				return new ModelAndView("loginPage");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		String searchType = "COURSE_SPECIFIC";
		String odEvntAction = "false";
	    String exitOpenDayRes = (String)session.getAttribute("exitOpRes");
	    exitOpenDayRes = StringUtils.isNotBlank(exitOpenDayRes) ? exitOpenDayRes : "";
	    if(StringUtils.isNotBlank(exitOpenDayRes) && StringUtils.equals(exitOpenDayRes, "true")) {
	       odEvntAction = "true";
	    }
	    
		String keywordSearch = StringUtils.isNotBlank(requestParam.get("q")) ? (requestParam.get("q")) : null;
		String subject = StringUtils.isNotBlank(requestParam.get("subject")) ? requestParam.get("subject") : null;
		if (StringUtils.isNotBlank(keywordSearch)) {
			subject = "";
		}
		String locationType = StringUtils.isNotBlank(requestParam.get("location-type"))	? requestParam.get("location-type")	: null;
		String location = StringUtils.isNotBlank(requestParam.get("location")) ? requestParam.get("location") : null;
		String campusType = StringUtils.isNotBlank(requestParam.get("campus-type")) ? requestParam.get("campus-type") : null;
		String studyMode = StringUtils.isNotBlank(requestParam.get("study-mode")) ? requestParam.get("study-mode"): null;
		String empRateMax = StringUtils.isNotBlank(requestParam.get("employment-rate-max"))	? requestParam.get("employment-rate-max"): null;
		String empRateMin = StringUtils.isNotBlank(requestParam.get("employment-rate-min"))	? requestParam.get("employment-rate-min"): null;
		String empRate = (StringUtils.isNotBlank(empRateMin) ? empRateMin : "")	+ (StringUtils.isNotBlank(empRateMax) ? ("," + empRateMax) : "");
		String ucasTariffMax = StringUtils.isNotBlank(requestParam.get("ucas-points-max")) ? (requestParam.get("ucas-points-max")): null;
		String ucasTariffMin = StringUtils.isNotBlank(requestParam.get("ucas-points-min")) ? (requestParam.get("ucas-points-min")): null;
		String ucasTariff = (StringUtils.isNotBlank(ucasTariffMin) ? ucasTariffMin : "")+ (StringUtils.isNotBlank(ucasTariffMax) ? ("," + ucasTariffMax) : "");
		String russel = StringUtils.isNotBlank(requestParam.get("russell-group")) ? (requestParam.get("russell-group"))	: null;
		String sortOrder = StringUtils.isNotBlank(requestParam.get("sort")) ? (requestParam.get("sort")) : "R";
		String jacsCode = StringUtils.isNotBlank(requestParam.get("jacs")) ? (requestParam.get("jacs")) : null;
		String entryLevel = StringUtils.isNotBlank(requestParam.get("entry-level"))	? (requestParam.get("entry-level").replaceAll("-", "_")): "";
		String entryPoints = StringUtils.isNotBlank(requestParam.get("entry-points"))? (requestParam.get("entry-points")): " ";
		String pageNo = StringUtils.isNotBlank(requestParam.get("pageno")) ? (requestParam.get("pageno")) : "1";
		String scoreValue = StringUtils.isNotBlank(requestParam.get("score")) ? requestParam.get("score") : "";
		
		//redirecting 404 page if the url doesn't have the below listed parameters
	    boolean qry404Flag = true;
	    if (queryString.contains("q=")) {
	      qry404Flag = false;
	    }
	    if (queryString.contains("subject=")) {
	      qry404Flag = false;
	    }
	    if (queryString.contains("ucas-code=")) {
	      qry404Flag = false;
	    }
	    if (queryString.contains("jacs=")) {
	      qry404Flag = false;
	    }
	    if (qry404Flag) {
	      model.addAttribute(GlobalConstants.REASON_FOR_404, "--(1.1)--> URL doesn't have q, subject, ucas code and jacs code");
	      response.sendError(404, GlobalConstants.ERROR_MSG_404);
	      return null;
	    }

		String country = "united-kingdom";
	    String studyLevelDesc = "degrees";
	    String selQual = null;
		if (StringUtils.isNotBlank(urlArray[1])) {
			selQual = urlArray[1].substring(0, urlArray[1].indexOf("-courses"));
		}

		String qualCode = "";
		if ("degree".equals(selQual)) {
			qualCode = "M";
		    studyLevelDesc = "degrees";
		} else if ("postgraduate".equals(selQual)) {
			qualCode = "L";
		    studyLevelDesc = "postgraduate degrees";
		} else if ("foundation-degree".equals(selQual)) {
			qualCode = "A";
		    studyLevelDesc = "foundation degrees";
		} else if ("access-foundation".equals(selQual)) {
			qualCode = "T";
		    studyLevelDesc = "access foundation degrees";
		} else if ("hnd-hnc".equals(selQual)) {
			qualCode = "N";
		    studyLevelDesc = "HND/HNC degrees";
		} else if ("ucas_code".equals(selQual)) {
			qualCode = "UCAS_CODE";
		} else { // 301 redirection for the not matched and in valid qual code to degrees
			String redirectUrlPath = requestURI;
			String redirectURL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN+ redirectUrlPath.replaceFirst(selQual, "degree") + "?" + queryString;
			response.sendRedirect(redirectURL);
			return null;
		}
		
	    // URL having greater london instead of london.        
	    if (location != null && location.equalsIgnoreCase("greater london")) {
	       model.addAttribute(GlobalConstants.REASON_FOR_404, "--(1.1)--> URL having greater london insteed of london");
	       response.sendError(404, GlobalConstants.ERROR_MSG_404);
	       return null;
	    }
        
	    //To get the User Info
		commonFunction.loadUserLoggedInformationForMoneyPage(request, context, response); 
		global.removeSessionData(session, request, response);
	    
		seoFirstPageUrl = requestURI + "?" + queryString;
		model.addAttribute("URL_1", requestURI);
		model.addAttribute("URL_2", "");
		model.addAttribute("selected_qual", selQual);
		model.addAttribute("SEO_FIRST_PAGE_URL", seoFirstPageUrl);// Don't modified this attribute since it is used various place like(sorting url,etc) apart from seo purpose

		//Setting noindex based on the parameters starts
		if (pageNo != null && Integer.parseInt(pageNo) > 1) {
			model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
		}

		if (StringUtils.isNotBlank(keywordSearch) || StringUtils.isNotBlank(jacsCode)) {
			model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
		}
		if (StringUtils.isNotBlank(queryString)) {
			if (!queryString.contains("clearing") && !queryString.contains("subject=")
					&& !queryString.contains("location=")) {
				model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
			}
			if (queryString.contains("russell-group=") || queryString.contains("campus-type=")
					|| queryString.contains("study-mode=") || queryString.contains("employment-rate-max=")
					|| queryString.contains("ucas-points-max=") || queryString.contains("sort=")
					|| queryString.contains("jacs=") || queryString.contains("ucas-code=")
					|| queryString.contains("location-type=") || queryString.contains("assessment-type=")
					|| queryString.contains("your-pref=") || queryString.contains("entry-level=")
					|| queryString.contains("ip=") || queryString.contains("score=")) { // Added condition to no index
				model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
			}
		}

		if (StringUtils.isNotBlank(sortOrder) && !StringUtils.equals("R", sortOrder)) {
			model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
		}
		
	    if(StringUtils.equalsIgnoreCase("all", selQual)){
	    	model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
		}
	    
        if(StringUtils.equalsIgnoreCase("postgraduate", selQual)){
        	model.addAttribute(SpringConstants.META_ROBOTS_NOINDEX_FOLLOW, SpringConstants.NO_INDEX_FOLLOW);
        }
		// End noindex based on the parameters
        
		location = (StringUtils.isNotBlank(location)) ? location.trim() : commonFunction.replacePlus(country);
		location = (StringUtils.isNotBlank(location)) ? StringUtils.upperCase(location) : location;
        studyLevel = StringUtils.isNotBlank(qualCode) ? StringUtils.upperCase(qualCode).trim() : "";
	    String regionTitleCase = StringUtils.isBlank(location) ? "" : utilities.toTitleCase(location.trim());
	    searchResultsBean.setRegionId(regionTitleCase.trim());
	    searchResultsBean.setCourseSearchText(keywordSearch);
	    
	    //Redirect to 404 page for the postgraduate categodyid which belongs to hotcourses
	    if (StringUtils.isNotBlank(studyLevel) && StringUtils.equalsIgnoreCase(studyLevel, "L")) {
	        String browseId = global.getBrowseTypeId(subjectCode); //DB_CALL
	        if (browseId != null && browseId.trim().length() > 0 && browseId.equals("3")) {
	          model.addAttribute(GlobalConstants.REASON_FOR_404, "--(4)--> Redirect to 404 page for the postgraduate categodyid which belongs to hotcourses");
	          response.sendError(404, GlobalConstants.ERROR_MSG_404);
	          return null;
	       }
	    }      

		String searchHow = sortOrder;
		model.addAttribute("sortByValue", searchHow);
		searchHow = StringUtils.isNotBlank(searchHow) ? StringUtils.upperCase(searchHow) : searchHow;
	    String courseSearchText = searchResultsBean.getCourseSearchText();
	    courseSearchText = courseSearchText != null && courseSearchText.trim().equalsIgnoreCase("Enter course keyword") ? "" : courseSearchText;


		boolean mobileFlag = MobileUtils.userAgentCheck(request);
		String basketId = commonFunction.checkCookieStatus(request);
	    basketId = StringUtils.isNotBlank(basketId)	? basketId	: null;
		String userAgent = request.getHeader("user-agent");
		String clientIp = StringUtils.isNotBlank(requestParam.get("ip")) ? (request.getParameter("ip"))	: new RequestResponseUtils().getRemoteIp(request);
		String x = new SessionData().getData(request, "x");
		x = x != null && !x.equals("0") && x.trim().length() >= 0 ? x : "16180339";
		String y = new SessionData().getData(request, "y");
		y = y != null && !y.equals("0") && y.trim().length() >= 0 ? y : "0";
	    String userId = y;
	    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";

		String subjectSessionId = StringUtils.isNotBlank((String) session.getAttribute("subjectSessionId"))	? (String) session.getAttribute("subjectSessionId")	: null;

		searchResultsBean.setX(x);
		searchResultsBean.setY(y);
		searchResultsBean.setSearchWhat("Z"); // SEARCH TYPE Z-COLLEGE O-COURSES
		searchResultsBean.setPhraseSearch(keywordSearch); //
		searchResultsBean.setCollegeName(""); // COLLEGE NAME
		searchResultsBean.setQualification(qualCode); // STYDY LEVEL - DEFAULT = M,N,A,T,L /// M,N,A,T,L [OR] M [OR] N	// [OR] A [OR] T [OR] L
		searchResultsBean.setTownCity(location); // LOCATION (BATH)
		searchResultsBean.setStudyMode(studyMode); // STYDY MODE DEFAULT NULL must be // "A1" [OR] // "A2,A21,A22,A3,A23,A4,A5" [OR]// B,B1,B2,B3,B4,B5,B6,C,C1,C2,C3,C4,C5,C6" [OR] "A6"
		searchResultsBean.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID); // AFFLIATE ID
		searchResultsBean.setSearchHow(searchHow); // DEFAULT 'R' VALUE MAY BE
		searchResultsBean.setUcasCode("");
		searchResultsBean.setBasketId(basketId);
		searchResultsBean.setPageNo(pageNo);
		searchResultsBean.setPageName(searchType);
		searchResultsBean.setEntryLevel(entryLevel);
		searchResultsBean.setEntryPoints(entryPoints);
		searchResultsBean.setUserAgent(userAgent);
		searchResultsBean.setSearchCategory(subject);
		searchResultsBean.setEmpRate(empRate);
		searchResultsBean.setCampusTypeName(campusType);
		searchResultsBean.setUnivLocTypeName(locationType);
		searchResultsBean.setRusselGroupName(russel);
		if (StringUtils.isNotBlank(scoreValue)) { // change
			searchResultsBean.setUcasTariff(scoreValue);
		} else {
			searchResultsBean.setUcasTariff(ucasTariff);
		}
		searchResultsBean.setBasketId(basketId);
		searchResultsBean.setJacsCode(jacsCode);
		searchResultsBean.setRequestURL(currentPageUrl);
		searchResultsBean.setUserTrackSessionId(new SessionData().getData(request, "userTrackId"));
		searchResultsBean.setSubjectSessionId(subjectSessionId);
		searchResultsBean.setRandomNumber(utilities.getSessionRandomNumber(request, session));
		searchResultsBean.setClientIp(clientIp);
		if (StringUtils.isNotBlank(jacsCode)) {
			searchResultsBean.setUcasCode("");
			searchResultsBean.setPhraseSearch("");
			searchResultsBean.setSearchCategory("");
		}
		
        //Footer section
		GetFooterSectionBean getFooterSectionBean = new GetFooterSectionBean();
		getFooterSectionBean.setType("F");
		getFooterSectionBean.setCollegeId("0");
		Map<String, Object> footerSectionMap = searchResultsBusiness.getFooterSectionHtml(getFooterSectionBean);
		if (!CollectionUtils.isEmpty(footerSectionMap)) {
			ArrayList<FooterSectionVO> footerSectionList = (ArrayList<FooterSectionVO>) footerSectionMap.get("LC_TEXT_ID");
			if (!CollectionUtils.isEmpty(footerSectionList)) {
				FooterSectionVO footerSectionVO = footerSectionList.get(0);
				String footerSection = "";
				Clob clob = footerSectionVO.getFooterHtmlText();
				if (clob != null) {
					footerSection = CommonFunction.getClobValue(clob);
				}
				model.addAttribute("footerSection", footerSection);
			}
		}
	    boolean searchResultFlag = false;

	    Map<String, Object> searchResultMap = null;

	    try 
	    {
		 
	       searchResultMap = searchResultsBusiness.getSearchResultsList(searchResultsBean);
	    
	    } catch (Exception e) {		
	    	searchResultFlag = true;  
	    	e.printStackTrace();
	    }
	    
	    if (searchResultFlag) {
	        return new ModelAndView("errorcodepage");
	     }
		if (!CollectionUtils.isEmpty(searchResultMap)) {

			//Roll up search
	        String invalidCategoryFlag = (String)searchResultMap.get("P_INVALID_CATEGORY_FLAG"); 
	        if (StringUtils.isNotBlank(invalidCategoryFlag) && StringUtils.equals("Y", invalidCategoryFlag)) {          
	          String newQuery = "";          
	          if(StringUtils.isNotBlank(subject)){
	        	  newQuery =  "?q=" + subject;
	          }          
	          String redirectingKeywordURL = requestURI + newQuery;       
	          response.sendRedirect(redirectingKeywordURL);          
	          return null;
	        } 			
			
			// SR Middle Content
			ArrayList<SearchResultContentsVO> searchResultsList = (ArrayList<SearchResultContentsVO>) searchResultMap.get("PC_SEARCH_RESULTS");
			if (!CollectionUtils.isEmpty(searchResultsList)) {
      			model.addAttribute("listOfSearchResults", searchResultsList);
      			
      			
		        String instIds = searchResultsList.stream().map(SearchResultContentsVO::getCollegeId).collect(Collectors.joining("###"));
			    String instNames = searchResultsList.stream().map(SearchResultContentsVO::getCollegeName).collect(Collectors.joining("###"));		   	
			    searchResultsList.stream().forEach(srchList -> srchList.setEventCategoryNameGa(utilities.setOpenDayEventGALabel(srchList.getEventCategoryName())));		   	 
			   	///For logging open day event type in dimension 18
			    String openDayEventTypeDim18 = searchResultsList.stream().filter(ins -> StringUtils.isNotBlank(ins.getEventCategoryName())).map(SearchResultContentsVO::getEventCategoryName).distinct().collect(Collectors.joining("|"));		   	
			    model.addAttribute("openDayEventType", openDayEventTypeDim18);
				
				/* Added the session for sponsored list and manual boosting GA logging */
				String sponsoredListing = "SPONSORED_LISTING";
				String manualBoosting = "MANUAL_BOOSTING";
				String sponsoredInstID = searchResultsList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getSponsoredListFlag())).map(SearchResultContentsVO::getCollegeId).collect(Collectors.joining(""));
				String boostedInstID = searchResultsList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getSponsoredListFlag())).map(SearchResultContentsVO::getCollegeId).collect(Collectors.joining(""));
				String sponsoredInstFlag = searchResultsList.stream().filter(sponsoredFlag -> sponsoredListing.equalsIgnoreCase(sponsoredFlag.getSponsoredListFlag())).map(SearchResultContentsVO::getSponsoredListFlag).collect(Collectors.joining(""));
				String manualInstFlag = searchResultsList.stream().filter(manualFlag -> manualBoosting.equalsIgnoreCase(manualFlag.getSponsoredListFlag())).map(SearchResultContentsVO::getSponsoredListFlag).collect(Collectors.joining(""));
				String sponsoredInstName = searchResultsList.stream().filter(sponsored -> sponsoredListing.equalsIgnoreCase(sponsored.getSponsoredListFlag())).map(SearchResultContentsVO::getCollegeName).collect(Collectors.joining(""));
				String boostedInstName = searchResultsList.stream().filter(boosted -> manualBoosting.equalsIgnoreCase(boosted.getSponsoredListFlag())).map(SearchResultContentsVO::getCollegeName).collect(Collectors.joining(""));
				session.setAttribute("sponsoredInstId", sponsoredInstID);
				session.setAttribute("boostedInstId", boostedInstID);
				model.addAttribute("sponsoredListFlag", sponsoredInstFlag);
				model.addAttribute("manualBoostingFlag", manualInstFlag);
				model.addAttribute("sponsoredInstName", sponsoredInstName);
				model.addAttribute("boostedInstName", boostedInstName);
				/* End of ga logging session */				
				
    		    model.addAttribute("instIds", instIds);
    		   	model.addAttribute("instNames", instNames);
			}
			
	        //smart pixel tagging
		    String cDimCollegeDisName = ""; 
		    String cDimCollegeIds = "";
		    StringBuilder cDimCollegeDisNamebld = new StringBuilder();
		    StringBuilder cDimCollegeIdsbld = new StringBuilder();
			if (!CollectionUtils.isEmpty(searchResultsList)) {  
			  for(SearchResultContentsVO searchResult : searchResultsList)
			  {
			    if(StringUtils.isNotBlank(searchResult.getCollegeName())){
			      cDimCollegeDisNamebld.append("\""+ searchResult.getCollegeName()+ "\", ");
			     }
			     if(StringUtils.isNotBlank(searchResult.getProviderId())){
			       cDimCollegeIdsbld.append("'"+ searchResult.getCollegeName()+ "', ");
			     }
			     
			     if(StringUtils.isNotBlank(searchResult.getExactRating())){
			       searchResult.setExactRating(utilities.formatNumberReview(searchResult.getExactRating())); 
			     }
			     if(StringUtils.isNotBlank(searchResult.getCollegeName())) {
			       searchResult.setCollegeNameSpecialCharRemove(commonFunction.replaceSpecialCharacter(searchResult.getCollegeName())); 
			     }
			    
			     /*Search Results middle contents inner cursor iteration starts*/
			     for(CourseSpecificListVO courseList : searchResult.getBestMatchCoursesList()){
			       if(StringUtils.isNotBlank(courseList.getSubOrderWebsite())){
			          courseList.setSubOrderWebsite(commonFunction.appendingHttptoURL(courseList.getSubOrderWebsite()));
			        }else{
			          courseList.setSubOrderWebsite("");
			        }
			        	
			        if(StringUtils.isNotBlank(courseList.getSubOrderEmailWebform())){
			          courseList.setSubOrderEmailWebform(commonFunction.appendingHttptoURL(courseList.getSubOrderEmailWebform()));
				    }else{
				      courseList.setSubOrderEmailWebform("");
				    }
			        	
			        if(StringUtils.isNotBlank(courseList.getSubOrderProspectusWebform())){
				      courseList.setSubOrderProspectusWebform(commonFunction.appendingHttptoURL(courseList.getSubOrderProspectusWebform()));
					}else{
					  courseList.setSubOrderProspectusWebform("");
					}
			        if(StringUtils.isNotBlank(courseList.getCourseTitle())){
			          courseList.setCourseTitleSpecialCharRemove(commonFunction.replaceSpecialCharacter(courseList.getCourseTitle()));
			        }
				  }
			     /*Search Results middle contents inner cursor iteration ends*/
			   }
			      cDimCollegeDisName = cDimCollegeDisNamebld.toString();
			      cDimCollegeIds = cDimCollegeIdsbld.toString();
			 }
					
			if(StringUtils.isNotBlank(cDimCollegeIds)){
	            cDimCollegeIds = cDimCollegeIds.substring(0, cDimCollegeIds.length()-2);              
	            cDimCollegeIds = "[" + cDimCollegeIds + "]";              
	            model.addAttribute("cDimCollegeIds", cDimCollegeIds);
	         }
	          
	          if(StringUtils.isNotBlank(cDimCollegeDisName)){
	            cDimCollegeDisName = cDimCollegeDisName.substring(0, cDimCollegeDisName.length()-2);
	             if(searchResultsList != null && searchResultsList.size() > 1){
	                cDimCollegeDisName = ("[" + cDimCollegeDisName + "]");
	             }
	            model.addAttribute("cDimCollegeDisName", cDimCollegeDisName);
	         }

            //To show the exact error message in the message page 
			if (CollectionUtils.isEmpty(searchResultsList)) {
	           searchResultFlag = true;
	          
	          if (StringUtils.isNotBlank(searchResultsBean.getRegionId())) {
	            model.addAttribute(SpringConstants.ERR_LOCATION, (location != null? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(location)).trim(): location));
	          }
	         
	          session.removeAttribute(SpringConstants.ERR_SUBJECT_NAME);
	          session.removeAttribute(SpringConstants.ERR_STUDY_LEVEL_ID);
	          session.removeAttribute(SpringConstants.ERR_STUDY_LEVEL_NAME);
	          session.removeAttribute("keyword");
	          if (StringUtils.isNotBlank(subject)) {
	            model.addAttribute(SpringConstants.ERR_SUBJECT_NAME, StringUtils.isNotBlank(subject) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(subject)).trim() : "");
	            session.setAttribute(SpringConstants.ERR_SUBJECT_NAME, StringUtils.isNotBlank(subject) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(subject)).trim() : "");          
	            session.setAttribute("keyword", StringUtils.isNotBlank(subject) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(subject)).trim() : "");
	          } else {
	            model.addAttribute(SpringConstants.ERR_SUBJECT_NAME, !StringUtils.isNotBlank(courseSearchText) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(courseSearchText)).trim() : "");
	            session.setAttribute(SpringConstants.ERR_SUBJECT_NAME, StringUtils.isNotBlank(courseSearchText) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(courseSearchText)).trim() : "");
	            session.setAttribute("keyword", StringUtils.isNotBlank(courseSearchText) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(courseSearchText)).trim() : "");
	          }
	          if (StringUtils.isNotBlank(studyLevel)) {
	            model.addAttribute(SpringConstants.ERR_STUDY_LEVEL_ID, studyLevel);
	            session.setAttribute(SpringConstants.ERR_STUDY_LEVEL_ID, studyLevel);
	          }
	          String studyLevelDisplayText = (String) searchResultMap.get("P_STUDY_LEVEL_DISPLAY_TEXT");
	          if(StringUtils.isNotBlank(studyLevelDisplayText)){
			      model.addAttribute(SpringConstants.ERR_STUDY_LEVEL_NAME, studyLevelDisplayText);
			      session.setAttribute(SpringConstants.ERR_STUDY_LEVEL_NAME, studyLevelDisplayText);
			  }
	          
	          model.addAttribute("providerResult", "true");
	          model.addAttribute("otherindex", "noindex,nofollow");
	          session.removeAttribute(SpringConstants.ERR_LOCATION);
	          session.removeAttribute("providerResult");
	          session.setAttribute(SpringConstants.ERR_LOCATION, request.getAttribute(SpringConstants.ERR_LOCATION));
	          session.setAttribute("providerResult", "true");

	          //Redirecting to did-you-mean page
	          if (StringUtils.isNotBlank(keywordSearch)) {
	            String tmpKeyword = StringUtils.isNotBlank(subject)? subject: courseSearchText;
	            ArrayList listOfSpellingSuggestions = search.getSpellingSuggestions(tmpKeyword, studyLevel); //DB_CALL        
				if (!CollectionUtils.isEmpty(listOfSpellingSuggestions)) {
	              model.addAttribute("listOfSpellingSuggestions", listOfSpellingSuggestions);
	              session.removeAttribute("listOfSpellingSuggestions");
	              session.setAttribute("listOfSpellingSuggestions", listOfSpellingSuggestions);
	              response.sendRedirect("/degrees/did-you-mean.html");
	           }
	         }
	       }   
			
		   //when searchResultFlag is true
		   if (searchResultFlag) {
	          model.addAttribute("providerResult", "true"); 
	          session.removeAttribute(SpringConstants.ERR_LOCATION);
	          session.removeAttribute("providerResult");
	          session.setAttribute(SpringConstants.ERR_LOCATION, request.getAttribute(SpringConstants.ERR_LOCATION));
	          session.setAttribute("providerResult", "true");     
	          
	          //For 404 redirection the URL to parent
	          String parentCatDesc = (String) searchResultMap.get("P_PARENT_CAT_TEXT_KEY");
	          if (StringUtils.isNotBlank(parentCatDesc)) {
	              String newURL = "";
	  		      StringBuilder queryStringBld = new StringBuilder();
	              if (StringUtils.isNotBlank(queryString)) {
	                  String[] splittedURL = queryString.split("&");
	                  for (int i = 0; i < splittedURL.length; i++) {
	                      if (splittedURL[i].indexOf("subject") > -1) {
	                          splittedURL[i] = "subject=" + parentCatDesc;
	                      }
	                      queryStringBld.append(splittedURL[i]);
	                      if (i + 1 != splittedURL.length) {
		                      queryStringBld.append("&");
	                      }
	                  }
	                newURL = queryStringBld.toString();
	             }
	              String redirectingBrowseURL = requestURI + "?" + newURL;
	              response.sendRedirect(redirectingBrowseURL);
	          } else {
	              response.sendRedirect(request.getContextPath() + "/notfound.html");
	           }
	        }
		   
		    //setting cpeQualificationNetworkId
		    String tmpQual = StringUtils.isNotBlank(studyLevel) ? studyLevel : "M";
	        String cpeQualificationNetworkId = "";
	        if (tmpQual.equalsIgnoreCase("M") || tmpQual.equalsIgnoreCase("N") || tmpQual.equalsIgnoreCase("A") || tmpQual.equalsIgnoreCase("T")) {
	          cpeQualificationNetworkId = "2";
	        } else { //L or postgraduate-courses
	          cpeQualificationNetworkId = "3";
	        }
	        session.removeAttribute("cpeQualificationNetworkId");
	        session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);

		      	          	          
			// Subject
			ArrayList<SubjectFilterVO> subjectList = (ArrayList<SubjectFilterVO>) searchResultMap.get("PC_SUBJECT_REFINE");
			if (!CollectionUtils.isEmpty(subjectList)) {
				model.addAttribute("subjectList", subjectList);
			}

			// Study level
			ArrayList<QualificationListVO> qualList = (ArrayList<QualificationListVO>) searchResultMap.get("PC_QUALIFICATION_REFINE");
			if (!CollectionUtils.isEmpty(qualList)) {
				model.addAttribute("qualificationList", qualList);
			}

			// Study mode
			ArrayList<StudyModeVO> studyModeList = (ArrayList<StudyModeVO>) searchResultMap.get("PC_STUDY_MODE_REFINE");
			if (!CollectionUtils.isEmpty(studyModeList)) {
				model.addAttribute("studyModeList", studyModeList);
			}

			// Region
			ArrayList<LocationListVO> locationList = (ArrayList<LocationListVO>) searchResultMap.get("PC_LOCATION_REFINE");
			if (!CollectionUtils.isEmpty(locationList)) {
				model.addAttribute("locationList", locationList);
			}

			// Location type
			ArrayList<LocationTypeVO> locationTypeList = (ArrayList<LocationTypeVO>) searchResultMap.get("PC_REFINES");
			if (!CollectionUtils.isEmpty(locationTypeList)) {
				model.addAttribute("locationTypeList", locationTypeList);
			}

			// Dynamic meta data details for SR page URLs in the Back office
			ArrayList<SEOMetaDetailsVO> seoMetaDetailsList = (ArrayList<SEOMetaDetailsVO>) searchResultMap.get("PC_META_DETAILS");
			if (!CollectionUtils.isEmpty(seoMetaDetailsList)) {
				model.addAttribute("seoMetaDetailsSRPage", seoMetaDetailsList);
			}

			//Stats
			ArrayList<RefineByOptionsVO> statsList = (ArrayList<RefineByOptionsVO>) searchResultMap.get("PC_STATS_LOG");
			String browseCatId = "";
			String browseCatFlag = "";
			String browseCatDesc = "";
			if (!CollectionUtils.isEmpty(statsList)) {
				model.addAttribute("statsList", statsList);
			    browseCatDesc = statsList.stream().map(RefineByOptionsVO::getBrowseCatDesc).collect(Collectors.joining(""));
			    browseCatId = statsList.stream().map(RefineByOptionsVO::getBrowseCatId).collect(Collectors.joining(""));
	            browseCatFlag = statsList.stream().map(RefineByOptionsVO::getBrowseCatFlag).collect(Collectors.joining(""));  
	            subjectCode =  statsList.stream().map(RefineByOptionsVO::getBrowseCatCode).collect(Collectors.joining("")); 
			}
			//keyword
			String searchPhrase = (String) searchResultMap.get("P_SEARCH_PHRASE");
			subjectNameDisplay = StringUtils.isNotBlank(browseCatDesc) ? browseCatDesc : searchPhrase;
			model.addAttribute("keywordOrSubject", StringUtils.isNotBlank(browseCatDesc) ? browseCatDesc : searchPhrase);
			//set the institution ids instead of college ids for dimension 3 in SR page
	        ArrayList<OmnitureLoggingVO> searchInstitutionIds = (ArrayList<OmnitureLoggingVO>)searchResultMap.get("PC_SEARCH_INSTITUTION_IDS");
			if (!CollectionUtils.isEmpty(searchInstitutionIds)) {
				OmnitureLoggingVO institutionIdsForThisSearch = null;
				institutionIdsForThisSearch = searchInstitutionIds.get(0);
				model.addAttribute("cDimCollegeId", institutionIdsForThisSearch.getInstitutionIdsForThisSearch());                   
			}	
						
			//
	        ArrayList searchCollegeIds = (ArrayList)searchResultMap.get("PC_SEARCH_COLLEGE_IDS");
			if(!CollectionUtils.isEmpty(searchCollegeIds)) {	
		         model.addAttribute("collegeIdsForThisSearchVO", searchCollegeIds);
	         }      
			
	        if (StringUtils.isNotBlank(subjectCode) && StringUtils.isNotBlank(browseCatId)) {
		          searchResultsBean.setCourseSearchText("");
		     }
			

	       if (StringUtils.isNotBlank(pageNo) && StringUtils.equals(pageNo, "1")) {
	          String sc_prob40_qualification = StringUtils.isNotBlank(studyLevel) ? studyLevel : "M";
	          Vector parameters = new Vector();
	          parameters.add(browseCatId); //categoryId
	          OmnitureProperties omnitureProperties = utilities.getCpeCategory(parameters); //DB call
	          String sc_prob42_keyword = omnitureProperties.getProb42_keyword() != null ? omnitureProperties.getProb42_keyword() : "";        
	          String sc_prob43_category = omnitureProperties.getProb43_cpe_category() != null ? omnitureProperties.getProb43_cpe_category() : "";
	          String sc_prob44_location = searchResultsBean.getRegionId() != null ? searchResultsBean.getRegionId().toUpperCase() : "";
	          if (sc_prob44_location.equalsIgnoreCase("LONDON")) {
	             sc_prob44_location = "GREATER LONDON";
	          }
	          if (sc_prob43_category != null && sc_prob43_category.trim().length() > 1) {
	             sc_prob43_category = sc_prob43_category.trim().toLowerCase();
	             sc_prob42_keyword = "";
	          } else if (sc_prob42_keyword != null && ((sc_prob43_category == null) || (sc_prob43_category != null && sc_prob43_category.trim().length() < 1))) {
	             sc_prob43_category = "";
	             sc_prob42_keyword = sc_prob42_keyword.trim().toLowerCase();
	          }
	          if (sc_prob40_qualification.equalsIgnoreCase("M") || sc_prob40_qualification.equalsIgnoreCase("N") || sc_prob40_qualification.equalsIgnoreCase("A") || sc_prob40_qualification.equalsIgnoreCase("T")) {
	             sc_prob40_qualification = "ug";
	          } else { //L or postgraduate-courses
	             sc_prob40_qualification = "pg";
	          }        
	          if(!"Y".equalsIgnoreCase(browseCatFlag)){
	             sc_prob42_keyword = searchResultsBean.getCourseSearchText() != null? commonFunction.replaceSpecialCharacter(searchResultsBean.getCourseSearchText().toLowerCase()): "";
	          }        
	          //remove previous attributes
	          session.removeAttribute("sc_prob40_qualification");
	          session.removeAttribute("sc_prob42_keyword");
	          session.removeAttribute("sc_prob43_category");
	          session.removeAttribute("sc_prob44_location");
	          //set the values into session scope      
	          model.addAttribute("sc_pageno", pageNo);
	          session.setAttribute("sc_prob40_qualification", sc_prob40_qualification.toLowerCase());
	          session.setAttribute("sc_prob42_keyword", sc_prob42_keyword.toLowerCase());
	          session.setAttribute("sc_prob43_category", sc_prob43_category.toLowerCase());
	          session.setAttribute("sc_prob44_location", sc_prob44_location.toUpperCase());
	          
		    }      
			
	        //P_PARENT_CAT_DESC out param
			String parentCatDesc = (String) searchResultMap.get("P_PARENT_CAT_DESC");			
	        String subjectL1 = null;
	        String subjectL2 = null;
	        if (StringUtils.isBlank(parentCatDesc)) {
	        	subjectL1 = subject;
	        	subjectL2 = "";
	        } else{
	        	subjectL1 = parentCatDesc;
	        	subjectL2 = subject;
	        }

			//Course found flag
	        String courseRankFoundFlag = (String)searchResultMap.get("P_COURSE_RANK_FOUND_FLAG"); 
	        if (StringUtils.isNotBlank(courseRankFoundFlag)) {
	        	model.addAttribute("courseRankFoundFlag", courseRankFoundFlag);
	        }      
	                	        
	        //Added qualification exist flag for the enter and edit qualification button for grade filter
	        String userQualificationExistFlag = (String)searchResultMap.get("P_USER_QUAL_EXIST_FLAG");
            if (StringUtils.isNotBlank(userQualificationExistFlag)) {
                model.addAttribute("userQualificationExistFlag", userQualificationExistFlag);
            }			
			
	        //Added subject session id for for generating session id entering in SR page.
	        subjectSessionId = (String)searchResultMap.get("P_SUBJ_GRADES_SESSION_ID");
	        session.setAttribute("subjectSessionId" , subjectSessionId);

			//Content snippet section 
	        Clob clob = (Clob)searchResultMap.get("P_SNIPPET_CONTENT");
	        String contentSnippetSection = "";
	        if(clob !=null) {
	        contentSnippetSection = CommonFunction.getClobValue(clob);
	        }
	        model.addAttribute("contentSnipteSection", contentSnippetSection);

			// Total record count
			int count = Integer.parseInt(searchResultMap.get("P_RECORD_COUNT").toString());
			String recordCount = Integer.toString(count);
			// Total course count
			int courseCount = Integer.parseInt(searchResultMap.get("P_TOTAL_COURSE_COUNT").toString());
			String totalCourseCount = Integer.toString(courseCount);
			// Result Exists
			String resultExists = (String) searchResultMap.get("P_RESULT_EXISTS");
			
			//Sponsored order item id for stats log
			String sponsoredOrderItemId= (String) searchResultMap.get("P_SPONS_ORDER_ITEM_ID");
	        if(StringUtils.isNotBlank(sponsoredOrderItemId)){
	          model.addAttribute("sponsoredOrderItemId", sponsoredOrderItemId);
	        }else{
	          model.addAttribute("sponsoredOrderItemId", "");  
	        }
	        String studyLevelDisplayText = (String) searchResultMap.get("P_STUDY_LEVEL_DISPLAY_TEXT");
	        if(StringUtils.isNotBlank(studyLevelDisplayText)){
		      model.addAttribute("studyLevelDesc", StringUtils.lowerCase(studyLevelDisplayText));
		    }
	        String courseSearchTextDisp = StringUtils.isNotBlank(courseSearchText) ? utilities.toTitleCase(commonFunction.replaceHypenWithSpace(courseSearchText)).replaceAll(" And ", " and ") : "";      
	        
			//Basket count
			String basketCount= (String) searchResultMap.get("P_BASKET_COUNT");
			basketCount = StringUtils.isNotBlank(basketCount) ? basketCount : "0";
		    session.setAttribute("basketpodcollegecount", basketCount);
		    
			//Terms and condition url
		    String termsAndCondition= (String) searchResultMap.get("P_TERMS_AND_CONDITION_URL");
		    termsAndCondition = StringUtils.isNotBlank(termsAndCondition) ? termsAndCondition : "";
	        model.addAttribute("termsAndConditionUrl", termsAndCondition);  
	        
	        //Privacy policy url
		    String privacyPolicy= (String) searchResultMap.get("P_PRIVACY_POLICY_URL");
		    privacyPolicy = StringUtils.isNotBlank(privacyPolicy) ? privacyPolicy : "";
	        model.addAttribute("privacyPolicyUrl", privacyPolicy);  
	        
	        //Canonical url
			String canonicalUrl= (String) searchResultMap.get("P_CANONICAL_URL");
			canonicalUrl = StringUtils.isNotBlank(canonicalUrl)  ? canonicalUrl : "";
	        model.addAttribute("canonicalUrl", canonicalUrl);
  	        
	        //Setting GAM changes
	        session.removeAttribute("gamKeywordOrSubject");
	        session.removeAttribute("gamStudyLevelDesc");
	        session.removeAttribute("FaqStudyLevel"); 
	        session.removeAttribute("FaqCategoryCode");
	        session.removeAttribute("FaqSubject");  
	        session.removeAttribute("entryRequirementsForGAM"); 
	        session.setAttribute("FaqStudyLevel", studyLevel);
	        session.setAttribute("FaqCategoryCode", subjectCode);
	        session.setAttribute("FaqSubject", subject);
	        model.addAttribute("seoStudyLevelText", commonFunction.seoStudyLevelDescription(studyLevel, "university", "COURSE_LIST"));
	        model.addAttribute("paramlocationValue", (StringUtils.isNotBlank(location) ? location : ""));
	       	      
	        //To get the keyword in googleAdSlots
	        session.setAttribute("gamKeywordOrSubject", StringUtils.isBlank(subject) ? keywordSearch : subject);     
	        model.addAttribute("gamKeywordOrSubject", StringUtils.isBlank(subject) ? keywordSearch : subject); 
	        session.setAttribute("gamStudyLevelDesc", selQual);
	        

	        //Added for insight(dimension13)
	        model.addAttribute("insightPageFlag", "yes");      
	        if(StringUtils.isNotBlank(location)){
	        	model.addAttribute("location", commonFunction.replaceHypenWithSpace(location).toUpperCase());
	        }
	        model.addAttribute("dimCategoryCode", StringUtils.isNotBlank(studyLevel) ? "" : global.getReplacedString(studyLevel.trim().toUpperCase()));
        
	        if("Y".equalsIgnoreCase(browseCatFlag)){		       
	        	model.addAttribute("cDimLDCS", StringUtils.isBlank(browseCatDesc) ? "" : global.getReplacedString(browseCatDesc.trim().toLowerCase()));
	          if (StringUtils.isNotBlank(studyLevel)) {
	            global.getStudyLevelDesc(studyLevel, request);
	          }  
	          model.addAttribute("setPageType", "browsemoneypageresults");
		    } else {
	          model.addAttribute("MONEY_PAGE_GEO_TARGET", "TRUE");        
	          model.addAttribute("cDimKeyword", StringUtils.isBlank(courseSearchTextDisp)? "": global.getReplacedString(courseSearchTextDisp.trim().toLowerCase()));      
	          if(StringUtils.isNotBlank(studyLevel)){
	            model.addAttribute("qualification", StringUtils.isBlank(studyLevel)? "": studyLevel.trim());
	            global.getStudyLevelDesc(studyLevel, request);
	          }
	          model.addAttribute("setPageType", "moneypageresults");
	        }

			//Pagination
			int numofpages = 0;
			int noOfrecords = 0;
			if (StringUtils.isNotBlank(recordCount)) {
				noOfrecords = Integer.parseInt(StringUtils.replace(recordCount, ",", ""));
			}
			numofpages = noOfrecords / 10;
			if (noOfrecords % 10 > 0) {
				numofpages++;
			}
			String paginationPageName = "newPagination";
			if (numofpages > 1 && ("1").equals(pageNo)) {
				paginationPageName = "searchInitialPagination";
			}
			//Featured brand slot
			ArrayList<FeaturedBrandVO> featuredBrandList = (ArrayList<FeaturedBrandVO>)searchResultMap.get("PC_FEATURED_BRAND");
            if (!CollectionUtils.isEmpty(featuredBrandList)) {
              FeaturedBrandVO featuredBrandVO = featuredBrandList.get(0);
              model.addAttribute("featuredCollegeName", featuredBrandVO.getCollegeName());
              model.addAttribute("featuredBrandList", featuredBrandList);
            }	
			//To fetch bread crumb        
	        request.getSession().setAttribute("bredqual", qualCode);
	        request.getSession().setAttribute("bredcatcode", browseCatId);
	        request.getSession().setAttribute("bredlocation", location);
	        String breadCrumb = (String) searchResultMap.get("P_BREADCRUMBS");
	        if(StringUtils.isNotBlank(breadCrumb)) {
	        	model.addAttribute("breadCrumb", breadCrumb);
	        }
	        ArrayList<BreadCrumbVO> breadcrumbSchemaList = (ArrayList<BreadCrumbVO>)searchResultMap.get("PC_BC_SCHEMA_TAGGING"); //Schema tagging details, it comes only for SR page
	        if(!CollectionUtils.isEmpty(breadcrumbSchemaList)){
	        	model.addAttribute("breadcrumbSchemaList", breadcrumbSchemaList);
	        	model.addAttribute("schemaListSize", String.valueOf(breadcrumbSchemaList.size()));
	        }
	        String selectedQual = (!CollectionUtils.isEmpty(qualList)) ? qualList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(QualificationListVO::getRefineDisplayDesc).collect(Collectors.joining("")) : "";
	        String selectedStudyMode = (!CollectionUtils.isEmpty(studyModeList)) ? studyModeList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(StudyModeVO::getRefineDesc).collect(Collectors.joining("")) : "";
	        String selectedRegion = (!CollectionUtils.isEmpty(locationList))  ? locationList.stream().filter(i -> "Y".equalsIgnoreCase(i.getSelectedFlag())).map(LocationListVO::getRegionName).collect(Collectors.joining("")) : "";
	        String selectedLocType = (!CollectionUtils.isEmpty(locationTypeList)) ? locationTypeList.stream().filter(i -> ("Y".equalsIgnoreCase(i.getSelectedFlag()) && "UNIV_LOC_TYPE".equalsIgnoreCase(i.getOptionType()))).map(LocationTypeVO::getOptionDesc).collect(Collectors.joining("")) : "";
	        String selectedCampusType = (!CollectionUtils.isEmpty(locationTypeList)) ? locationTypeList.stream().filter(i -> ("Y".equalsIgnoreCase(i.getSelectedFlag()) && "CAMPUS_BASED_UNIV".equalsIgnoreCase(i.getOptionType()))).map(LocationTypeVO::getOptionDesc).collect(Collectors.joining("")) : "";
	        String selectedRussellGroup = (!CollectionUtils.isEmpty(locationTypeList)) ? locationTypeList.stream().filter(i -> ("Y".equalsIgnoreCase(i.getSelectedFlag()) && "RUSSELL_GROUP".equalsIgnoreCase(i.getOptionType()))).map(LocationTypeVO::getOptionDesc).collect(Collectors.joining("")) : "";
	        	        
	        cpeQualificationNetworkId = StringUtils.isNotBlank((String)request.getSession().getAttribute("cpeQualificationNetworkId")) ? (String)request.getSession().getAttribute("cpeQualificationNetworkId") : "2";
	                
	        model.addAttribute("selectedQual", selectedQual);
	        model.addAttribute("selectedStudyMode", selectedStudyMode);
	        model.addAttribute("selectedRegion", selectedRegion);
	        model.addAttribute("selectedLocType", selectedLocType);
	        model.addAttribute("selectedCampusType", selectedCampusType);
	        model.addAttribute("selectedRussellGroup", selectedRussellGroup);
			model.addAttribute("subject", subject);
			model.addAttribute("subjectNameDisplay", subjectNameDisplay);
			model.addAttribute("domainPath", domainPath);
			model.addAttribute("userId", userId);
			model.addAttribute("contextPath", contextPath);
			model.addAttribute("domainPathImg", domainPathImg);
			model.addAttribute("paginationPageName", paginationPageName);
			model.addAttribute("seoFirstPageUrl", seoFirstPageUrl);
			model.addAttribute("pageNo", pageNo);
			model.addAttribute("recordCount", recordCount);
			model.addAttribute("totalCourseCount", totalCourseCount);
			model.addAttribute("resultExists", resultExists);
			model.addAttribute("studyLevelDescription", studyLevelDesc);
		    model.addAttribute("dispSubjectName", "");
		    model.addAttribute("mobileFlag", mobileFlag);
		    model.addAttribute("campusType", campusType);
		  	model.addAttribute("russellFlag", russel);
		  	model.addAttribute("empRate", empRateMin);
		  	model.addAttribute("studylevel", selQual);
		    model.addAttribute("searchKeyword", keywordSearch);
		    model.addAttribute("studyMode", studyMode);
		    model.addAttribute("locationType", locationType);
		    model.addAttribute("region", location);
		    model.addAttribute("sortByValue", searchHow);
		    model.addAttribute("qualCode", qualCode);
		    model.addAttribute("subjectL1", subjectL1);
	        model.addAttribute("subjectL2", subjectL2);
	        model.addAttribute("odEvntAction", odEvntAction);
			model.addAttribute("exitOpenDayRes", exitOpenDayRes);
			model.addAttribute("paramSubjectCode", subjectCode);
			model.addAttribute("paramStudyLevelId", studyLevel);
			model.addAttribute("networkId", cpeQualificationNetworkId);
			model.addAttribute("contextJsPath", CommonUtil.getJsPath());
			model.addAttribute("clearingYear", GlobalConstants.CLEARING_YEAR);
			model.addAttribute("scoreValue", scoreValue);

		    model.addAttribute("whiteTickIcon", CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0));
		    model.addAttribute("whitePlusIcon", CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0));
		    model.addAttribute("bluePlusIcon", CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0));
		    model.addAttribute("closeGreyIcon", CommonUtil.getImgPath("/wu-cont/images/clr20_icn_close_grey.svg",0));
			model.addAttribute("closeButton", CommonUtil.getImgPath("/wu-cont/images/clr_fltr_close_gry.svg", 1));		    
			model.addAttribute("subArwLeft", CommonUtil.getImgPath("/wu-cont/images/sub_arw_left.png", 1));
			model.addAttribute("subArwRight", CommonUtil.getImgPath("/wu-cont/images/sub_arw_right.png", 1));
			model.addAttribute("img_px_gif", CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1));
			model.addAttribute("covid_wht_arw", CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 1));
			model.addAttribute("cmn_loading_1", CommonUtil.getImgPath("/wu-cont/img/whatuni/cmn_loading_1.svg", 1));
			model.addAttribute("hm_ldr", CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1));
			model.addAttribute("bk_2_top", CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png", 1));


			model.addAttribute("dimCategoryCode", StringUtils.isBlank(subjectCode) ? "" : new GlobalFunction().getReplacedString(subjectCode.trim().toUpperCase()));
			model.addAttribute("qualification1", studyLevel);
			model.addAttribute("whatuniDomain", GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName());
			request.setAttribute("pageno", pageNo);
		} else {
			model.addAttribute(GlobalConstants.REASON_FOR_404, "--(5)--> Redirect to 404 page for empty results");
			response.sendError(404, GlobalConstants.ERROR_MSG_404);
			return null;
		}


	    return new ModelAndView("searchresultspage");	  
	  }
  
	   
	  /**
	   * This method will return the University Details
	   * 
	   * @param model
	   * @param request
	   * @param response
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping(value = "/search-result/get-institution-ajax", method = {RequestMethod.GET, RequestMethod.POST})
	  public ModelAndView getInsitutionAjaxList(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		  
		  String keywordText = StringUtils.isNotBlank(request.getParameter("keywordText")) ? request.getParameter("keywordText") : null;
		  String pageName = StringUtils.isNotBlank(request.getParameter("pageName")) ? request.getParameter("pageName") : null;
		  GetInstitutionNameBean institutionNameBean = new GetInstitutionNameBean();
		  institutionNameBean.setKeywordText(keywordText);
		  institutionNameBean.setPageName(pageName);
		  Map<String, Object> institutionMap = searchResultsBusiness.getInstitutionNameList(institutionNameBean);
		  if(!CollectionUtils.isEmpty(institutionMap)) {
			  ArrayList<GetInstitutionNameVO> institutionList = (ArrayList<GetInstitutionNameVO>) institutionMap.get("LC_GET_COLLEGE");
			  if(!CollectionUtils.isEmpty(institutionList)) {
				  model.addAttribute("uniAjaxList", institutionList);
			  }
		  }
		  return new ModelAndView("searchResultsUniversityAjax");
	  }
}
