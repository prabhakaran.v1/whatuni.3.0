package spring.business;

import java.util.Map;

import spring.form.openday.OpenDayProviderLandingPageBean;

/**
 * @IOpenDayBusiness
 * @author   Sujitha V
 * @version  1.0
 * @since    06_05_2020
 * @purpose  Getting Open day provider related contents
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
 * Kailash L              1.1                 06_05_2020       Added ajax method for light box   
 * 
*/

public interface IOpenDayBusiness {

	public Map<String, Object> getOpenDayProviderLandingList(OpenDayProviderLandingPageBean openDayBean) throws Exception; //Added this for getting open day provider landing page values by Sujitha.V on 6_May_2020 rel

	public Map<String, Object> getOpenDayLightBox(OpenDayProviderLandingPageBean openDayBean) throws Exception; //Added this for getting open day provider landing page light box values by Kailash.L on 6_May_2020 rel

}
