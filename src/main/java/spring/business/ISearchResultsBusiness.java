package spring.business;

import java.util.Map;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.pojo.searchresult.GetInstitutionNameBean;
import spring.pojo.searchresult.SearchResultsBean;

public interface ISearchResultsBusiness {

	Map<String, Object> getSearchResultsList(SearchResultsBean searchResultsBean) throws Exception;
    Map<String, Object> getInstitutionNameList(GetInstitutionNameBean institutionNameBean) throws Exception; 
    Map<String, Object> getFooterSectionHtml(GetFooterSectionBean getFooterSectionBean) throws Exception; 
}
