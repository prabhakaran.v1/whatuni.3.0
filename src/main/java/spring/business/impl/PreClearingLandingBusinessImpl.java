package spring.business.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.business.IPreClearingLandingBusiness;
import spring.dao.IPreClearingLandingDAO;

@Service
public class PreClearingLandingBusinessImpl implements IPreClearingLandingBusiness {

    @Autowired
    private IPreClearingLandingDAO preClearingLandingDAO;

    //Added to get pre clearing About whatuni SHTML contents details - Sujitha V for 31 March 2020
    public Map getEbookAboutWhatuniContents() {
        Map resultMap = preClearingLandingDAO.getEbookAboutWhatuniContents();
        return resultMap;
    }

    public Map getClearingHomePageDetails(List inputList) {
        return preClearingLandingDAO.getClearingHomePageDetails(inputList);
    }

    @Override
    public Map getClearingSubjectAjaxList(String subjectKeywordSearch) throws Exception {
        Map resultMap = preClearingLandingDAO.getClearingSubjectAjaxList(subjectKeywordSearch);
        return resultMap;
    }

    @Override
    public Map getClearingUniAjaxList(String uniKeywordSearch) throws Exception {
        Map resultMap = preClearingLandingDAO.getClearingUniAjaxList(uniKeywordSearch);
        return resultMap;
    }

    public Map getClearingCourseDetails(List inputList) {
      Map resultmap = preClearingLandingDAO.getClearingCourseDetails(inputList);
      return resultmap;
    }
}