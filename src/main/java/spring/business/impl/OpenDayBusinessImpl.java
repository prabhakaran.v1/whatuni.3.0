package spring.business.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.business.IOpenDayBusiness;
import spring.dao.IOpenDayDAO;
import spring.form.openday.OpenDayProviderLandingPageBean;

/**
 * @OpenDayBusinessImpl
 * @author   Sujitha V
 * @version  1.0
 * @since    06_05_2020
 * @purpose  Class to get Open day provider related contents
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	                 Change
 * *************************************************************************************************************************
 * Kailash L              1.1                 06_05_2020       Added ajax method for light box   
 * 
*/

@Service
public class OpenDayBusinessImpl implements IOpenDayBusiness {

	@Autowired
	private IOpenDayDAO openDayDAO;

	//Added this for getting open day provider landing page values by Sujitha.V on 6_May_2020 rel
	public Map<String, Object> getOpenDayProviderLandingList(OpenDayProviderLandingPageBean openDayBean) throws Exception {
		return openDayDAO.getOpenDayProviderLandingList(openDayBean);
	}

	//Added this for getting open day provider landing page light box values by Kailash.L on 6_May_2020 rel
	public Map<String, Object> getOpenDayLightBox(OpenDayProviderLandingPageBean openDayBean) throws Exception {
		return openDayDAO.getOpenDayLightBox(openDayBean);
	}

}
