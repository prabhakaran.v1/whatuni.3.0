package spring.business.impl;

import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.layer.util.SpringConstants;
import spring.business.IRestBusiness;
import spring.dao.IRestDAO;

@Service
public class RestBusinessImpl implements IRestBusiness {
  
  @Autowired
  public IRestDAO restDAO = null;

  @Override
  public String callWebServices(final String URL, final String jsonString) {

    JSONObject jsonObject;

    if (jsonString == null) {
      jsonObject = new JSONObject();
    } else {
      jsonObject = new JSONObject(jsonString);
    }

    String affiliateId = "220703";
    String accessToken = "C8F80AF1D025C74252CFC98A47C8CD34A1B84864";

    jsonObject.put(SpringConstants.WU_AFFILIATE_ID, affiliateId);
    jsonObject.put(SpringConstants.CLEAR_ACCESS_TOKEN_KEY, accessToken);

    final String responseJsonString = restDAO.callWebServices(URL, jsonObject.toString());
    if (responseJsonString != null) {
      return getJsonResponse(responseJsonString);
    }
    return null;
  }

  @Override
  public String callContentfulWebServices(String URL, Map<String, String> inputMap) {

    String queryParam = null;
    String ACCESS_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ3dVJldmlld0FwaSJ9.2nBqK5Kgfi6E9IpTnPRj3GpEO2Xo1CpZCnJF6GY3B-9kr-quMIqtT1ukxCjp9FBRO0muCYwdxzp3R2YJ0Bhp2A";
    inputMap.put("accessToken", ACCESS_TOKEN);
    queryParam = inputMap.entrySet().stream().map((entry) -> entry.getKey()+"="+entry.getValue()).collect(Collectors.joining("&"));  
    if(StringUtils.isNotBlank(queryParam)) {      
      URL += "?".concat(queryParam);
    }
    final String responseJsonString = restDAO.callContentfulWebServices(URL);
    if (responseJsonString != null) {
      return getJsonResponse(responseJsonString);
    }
    return null;
  }

  public String getJsonResponse(String responseJsonString) {
    final JSONObject responseJson = new JSONObject(responseJsonString);
    final String apiStatusCode = responseJson.optString("status");
    if (StringUtils.isEmpty(apiStatusCode) || StringUtils.equals(apiStatusCode, String.valueOf(HttpStatus.OK.value())) || StringUtils.equals(apiStatusCode, String.valueOf(HttpStatus.OK))) {
      return responseJsonString;
    }
    try {
      throw new RuntimeException(responseJson.getString("exception"));
    } catch (JSONException je) {
   //LOGGER.error("Exception Occurred while getting Exception Details for API URL: " + URL, je);
   return null;
    }
  }
}
