package spring.business.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.business.ISearchResultsBusiness;
import spring.dao.ISearchResultsDAO;
import spring.pojo.searchresult.SearchResultsBean;
import spring.pojo.searchresult.GetFooterSectionBean;
import spring.pojo.searchresult.GetInstitutionNameBean;

@Service
public class SearchResultsBusinessImpl implements ISearchResultsBusiness{

	@Autowired
	private ISearchResultsDAO searchResultsDAO;
	
	@Override
	public Map<String, Object> getSearchResultsList(SearchResultsBean searchResultsBean) throws Exception {
		return searchResultsDAO.getSearchResultsList(searchResultsBean);
	}

	public Map<String, Object> getInstitutionNameList(GetInstitutionNameBean institutionNameBean) throws Exception {

		return searchResultsDAO.getInstitutionNameList(institutionNameBean);
	}

	public Map<String, Object> getFooterSectionHtml(GetFooterSectionBean getFooterSectionBean) throws Exception {
		return searchResultsDAO.getFooterSectionHtml(getFooterSectionBean);
	}
}
