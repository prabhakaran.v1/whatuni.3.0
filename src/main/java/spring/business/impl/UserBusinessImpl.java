package spring.business.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.business.IUserBusiness;
import spring.dao.IUserDAO;
import spring.pojo.user.UnSubcribeForm;

@Service
public class UserBusinessImpl implements IUserBusiness{
	
    @Autowired
	IUserDAO userDAO = null;
	
	@Override
	public Map getUnSubscribePage(UnSubcribeForm unSubcribeForm) throws Exception {		
		return userDAO.getUnSubscribePage(unSubcribeForm);
	}
    
	@Override
	public Map saveUserPreference(UnSubcribeForm unSubcribeForm) throws Exception {		
		return userDAO.saveUserPreference(unSubcribeForm);
	}
}
