package spring.business.impl;

import java.util.Map;
import spring.business.ICPCLandingBusiness;
import spring.dao.ICPCLandingDAO;
import spring.dao.util.valueobject.cpc.CPCLightboxVO;
import spring.dao.util.valueobject.cpc.CPCPageVO;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : ICPCLandingBusinessImpl.java
* Description   : Class to get cpc releated content
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	             Modified On         	Modification Details 	     Change
* *************************************************************************************************************************************
* Hema.S                     1.0                 04.02.2020             First draft   		
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/

public class ICPCLandingBusinessImpl implements ICPCLandingBusiness {
  private ICPCLandingDAO cpcLandingDAO = null;
  
  public ICPCLandingDAO getCpcLandingDAO() {
	return cpcLandingDAO;
  }

  public void setCpcLandingDAO(ICPCLandingDAO cpcLandingDAO) {
	this.cpcLandingDAO = cpcLandingDAO;
  }

  public Map getCpcTypeValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting campaign type by Hema.S on 04_02_2020_REL
    Map resultMap = cpcLandingDAO.getCpcTypeValues(cPCPageVO);
	return resultMap;
  }
  //
  public Map getMultipleLandingPageValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting multiple landing page values by Hema.S on 04_02_2020_REL
    Map resultMap = cpcLandingDAO.getMultipleLandingPageValues(cPCPageVO);
	return resultMap;
  }
  //
  public Map getSingleLandingPageValues(CPCPageVO cPCPageVO) throws Exception {// Added this for getting single landing page values by Jeyalakshmi.D on 04_02_2020_REL
    Map resultMap = cpcLandingDAO.getSingleLandingPageValues(cPCPageVO);
	return resultMap;
  }
  //
  public Map getCPCLightboxValues(CPCLightboxVO cPCLightboxVO) throws Exception {// Added this for getting single landing page values by Jeyalakshmi.D on 04_02_2020_REL
    Map resultMap = cpcLandingDAO.getCPCLightboxValues(cPCLightboxVO);
    return resultMap;
  }
}
