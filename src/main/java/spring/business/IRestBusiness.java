package spring.business;

import java.util.Map;

public interface IRestBusiness {
  public String callWebServices(String URL, String json);
  public String callContentfulWebServices(String URL, Map<String, String> inputMap);
}
