package spring.business;

import java.util.Map;
import spring.pojo.user.UnSubcribeForm;

public interface IUserBusiness {

	Map getUnSubscribePage(UnSubcribeForm unSubcribeForm) throws Exception;

	Map saveUserPreference(UnSubcribeForm unSubcribeForm) throws Exception;

}
