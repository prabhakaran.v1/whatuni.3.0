package spring.util.taglibraries;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;

import WUI.utilities.CommonUtil;
import lombok.Data;

@Data
public class CssPathTagClass extends SimpleTagSupport{
	 
  private String source;
	  
  public void doTag() throws JspException, IOException{
    String cssPath = CommonUtil.getCSSPath();
    JspWriter out = getJspContext().getOut();
    if(StringUtils.isNotBlank(source)){
      /* Use message from attribute */
      out.print(cssPath + source);
    }else{
      /* use message from the body */
	  out.print(cssPath);
    }
  }
}
