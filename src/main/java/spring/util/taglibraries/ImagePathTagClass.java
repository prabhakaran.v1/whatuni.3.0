package spring.util.taglibraries;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;

import WUI.utilities.CommonUtil;
import lombok.Data;

@Data
public class ImagePathTagClass extends SimpleTagSupport{
	
  private String source;
  private String imageNum;
	  
  public void doTag() throws JspException, IOException{
	String imgPath = CommonUtil.getImgPath(source, Integer.parseInt(imageNum));
	JspWriter out = getJspContext().getOut();
	if(StringUtils.isNotBlank(imgPath)){
	  /* Use message from attribute */
	  out.print(imgPath);
	}
  }
}
