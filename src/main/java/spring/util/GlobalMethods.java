package spring.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import spring.dao.util.valueobject.common.Covid19VO;

/**
  * @GlobalMethods
  * @author Balraj Selvakumar
  * @version 1.0
  * @since 10.07.2007
  * @purpose This program is used to group the functions that are all used frequesntly.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 21.04.2020     Prabhakaran V.                       Added setCovid19SessionData method to get the SYSVAR data
  * 02_06_2020     Sujitha V                            Added getRedirectURLSFromExcel method to read the data from excel sheet
  *  
  */
public class GlobalMethods {

  public GlobalMethods() {
  }

  /**
    *   This function is used to load the common pod details.
    * @param servletContext
    * @param request
    * @param response
    * @return none.
    */
  public void setCommonPodInformation(ServletContext servletContext, HttpServletRequest request, HttpServletResponse response) {
    String userId = new SessionData().getData(request, "y");
    HttpSession session = request.getSession();
   /* if (session.getAttribute("mySearches") == null) {
      String basketId = new CookieManager().getCookieValue(request, "basketId");
      ArrayList mySearches = new GlobalFunction().getMySearchesList(basketId, request);
      if (mySearches != null && mySearches.size() > 0) {
        session.setAttribute("mySearches", mySearches);
      }
    }*///Commented for 19th May release as part of W_USER_SEARCHES 
    if (userId != null && !userId.equals("0") && userId.trim().length() > 0) {
      if (session.getAttribute("userInfoList") == null) {
        ArrayList userInfoList = new CommonFunction().getUserInformation(userId, request, servletContext);
        if (userInfoList != null && userInfoList.size() > 0) {
          session.setAttribute("userInfoList", userInfoList);
        }
      }
    } else {
      session.removeAttribute("userInfoList");
    }
    if (session.getAttribute("subjectList") == null) {
      List subjectList = new CommonFunction().getCourseSubject(null, request);
      session.setAttribute("subjectList", subjectList);
    }
  }

  /**
    *   This function is used yo check the given url content is valid for SEO.
    * @param pageTitle
    * @param urlContent
    * @return Response string as String.
    */
  public String changeSeoURLLink(String pageTitle, String urlContent) {
    // SEO URL for unilanding page //
    String responseString = "";
    if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("unilanding")) {
      //http://www.whatuni.com/degrees/universities/guide/Aston-University/3443/page.html
      responseString = GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE;
      if (urlContent != null) {
        String urlArray[] = urlContent.split("#");
        for (int i = 0; i < urlArray.length; i++) {
          if (i == 0) {
            String collegeName = urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
            responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(collegeName))) : "");
          }
          if (i == 1) {
            responseString = responseString + "/" + urlArray[1];
          }
        }
        responseString = responseString + "/";
      }
    }
    if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("univideoreview")) {
      //http://www.whatuni.com/degrees/college-videos/university-of-york-videos/highest-rated/3773/1/student-videos.html
      responseString = GlobalConstants.SEO_PATH_UNI_VIDEOS;
      if (urlContent != null) {
        String urlArray[] = urlContent.split("#");
        for (int i = 0; i < urlArray.length; i++) {
          if (i == 0) {
            String providerName = urlArray[0];
            responseString = responseString + (providerName != null && providerName.trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(providerName.trim().toLowerCase()))) + "-videos" : "");
          } else if (i == 1) {
            responseString = responseString + "/highest-rated/" + (urlArray[1] != null ? urlArray[1].trim() : urlArray[1]);
          }
        }
        responseString = responseString + "/1/student-videos.html";
      }
    }
    return responseString;
  }
  
  /**
   * @setCovid19SessionData method to get the COVID-19 SYSVAR datas and set into sessionData
   * @author prabhakaran.v
   * @param request
   * @param response
   * @return null
   */
  @SuppressWarnings("unchecked")
  public String setCovid19SessionData(HttpServletRequest request, HttpServletResponse response) {
	  SessionData sessionData = new SessionData();
	  String COVID19_ON_OFF = sessionData.getData(request, GlobalConstants.COVID19_SYSVAR);
	  if(GenericValidator.isBlankOrNull(COVID19_ON_OFF)) {
	    new SpringContextInjector();
		ICommonBusiness commonBusiness = (ICommonBusiness)SpringContextInjector.getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS); 
	    Map<String, Object> covid19DataMap = commonBusiness.getCovid19Data();
	    if(!CommonUtil.isBlankOrNull(covid19DataMap)) {
		  
		ArrayList<Covid19VO> covid19DataList = (ArrayList<Covid19VO>)covid19DataMap.get("PC_COVID_19_PAGE_DETAILS");
		  if(!CommonUtil.isBlankOrNull(covid19DataList)) {
			List<Covid19VO> covid19DataListVal = covid19DataList;
//		    sessionData.addData(request, response, GlobalConstants.COVID19_SYSVAR, GlobalConstants.COVID19_ON_OFF);
//		    sessionData.addData(request, response, "COVID19_ARTICLE_TEXT", GlobalConstants.COVID_19_ARTICLE_PAGE);
//		    sessionData.addData(request, response, "COVID19_OPENDAY_TEXT", GlobalConstants.COVID_19_OPENDAY_PAGE);
//		    sessionData.addData(request, response, "COVID19_COURSE_TEXT", GlobalConstants.COVID_19_SR_PR_CD_PROFILE);
//		    sessionData.addData(request, response, "COVID19_HEADER_TEXT", GlobalConstants.COMMON_COVID_19_HEADER);
		    
		    sessionData.addData(request, response, "COVID19_ARTICLE_TEXT", covid19DataListVal.stream().filter(i -> "COVID_19_ARTICLE_PAGE".equalsIgnoreCase(i.getSysvarName())).map(i -> i.getSysvarText()).collect(Collectors.joining("")));
		    sessionData.addData(request, response, "COVID19_OPENDAY_TEXT", covid19DataListVal.stream().filter(i -> "COVID_19_OPENDAY_PAGE".equalsIgnoreCase(i.getSysvarName())).map(i -> i.getSysvarText()).collect(Collectors.joining("")));
		    sessionData.addData(request, response, "COVID19_COURSE_TEXT", covid19DataListVal.stream().filter(i -> "COVID_19_SR_PR_CD_PROFILE".equalsIgnoreCase(i.getSysvarName())).map(i -> i.getSysvarText()).collect(Collectors.joining("")));
		    sessionData.addData(request, response, "COVID19_HEADER_TEXT", covid19DataListVal.stream().filter(i -> "COMMON_COVID_19_HEADER".equalsIgnoreCase(i.getSysvarName())).map(i -> i.getSysvarText()).collect(Collectors.joining("")));
			sessionData.addData(request, response, GlobalConstants.COVID19_SYSVAR, "ON");
		  }else {
			sessionData.addData(request, response, GlobalConstants.COVID19_SYSVAR, "OFF");
		  }
	    }else {
		sessionData.addData(request, response, GlobalConstants.COVID19_SYSVAR, "OFF");
	    }
	  }
	  return null;
  }
  
  /**
   * @getRedirectURLSFromExcel method is used to get the bulk of urls from excel and put into map.
   * @return the values as key and value pair.
   */
  public static final Map urlMap = new HashMap(); 
  public static Map getRedirectURLSFromExcel() throws IOException {

    final String FILE_PATH = SpringConstants.FILE_PATH_301_REDIRECTS;
  
    FileInputStream inputStream = null;
    try{
	 inputStream = new FileInputStream(FILE_PATH);
	 if(urlMap.size() < 1) {  
	   //Using XSSF for xlsx format,
	   Workbook workBook = new XSSFWorkbook(inputStream); 
	   Sheet sheet = workBook.getSheetAt(0);

	   Iterator<Row> rowIterator = sheet.iterator();
	   //Iterating over each row
	   while(rowIterator.hasNext()){
	     Row row = rowIterator.next();
	     Iterator<Cell> cellIterator = row.cellIterator();
	           
	     //Iterating over each cell (column wise)  in a particular row.
	     while(cellIterator.hasNext()){
	       Cell cell = cellIterator.next();
           if(StringUtils.isNotEmpty(row.getCell(cell.getColumnIndex()).getStringCellValue())) {
             //System.out.println("urlToBeRedirect-->"+row.getCell(cell.getColumnIndex()).getStringCellValue());
             urlMap.put(row.getCell(cell.getColumnIndex()).getStringCellValue().trim(), row.getCell(cell.getColumnIndex()+1).getStringCellValue().trim());
           }             
	     }	     
	   } 
	 }
   }catch(FileNotFoundException e){
	 e.printStackTrace();
   }catch (IOException e){
	 e.printStackTrace();
   }finally {
	 inputStream.close(); 
   }
   return urlMap;	  
  }
}
