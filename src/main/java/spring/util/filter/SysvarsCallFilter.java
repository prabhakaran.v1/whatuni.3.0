package spring.util.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import WUI.utilities.SessionData;
import spring.valueobject.common.SysvarsCallVO;

/**
 * @SysvarsCallFilter
 * @author Sri Sankari R
 * @version 1.0
 * @since 19/01/2021
 * @purpose This is used to get sysvar values from db.
 * Change Log
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 */
@WebFilter(urlPatterns = { "*.html" })
public class SysvarsCallFilter implements Filter {

	private FilterConfig filterConfig = null;

	@Override
	public void init(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		SessionData sessionData = new SessionData();
		SysvarsCallVO sysvarCallsVO = new SysvarsCallVO();
		try {
			if (!StringUtils.equals(sessionData.getData(req, SpringConstants.SYSVAR_IN_SESSION), SpringConstants.YES)) { 
				ICommonBusiness commonBusiness = (ICommonBusiness) new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
				Map<String, Object> sysvarValuesMap = commonBusiness.getSysvarValues();
				if (!CollectionUtils.isEmpty(sysvarValuesMap)) {
					ArrayList<SysvarsCallVO> sysvarValuesList = (ArrayList<SysvarsCallVO>) sysvarValuesMap.get("LC_SYSVAR_VALUES");
					if (!CollectionUtils.isEmpty(sysvarValuesList)) {
						Iterator<SysvarsCallVO> iterator = sysvarValuesList.iterator();
						while (iterator.hasNext()) {
							sysvarCallsVO = iterator.next();
							sessionData.addData(req, res, sysvarCallsVO.getName(), sysvarCallsVO.getValue());
						}
						sessionData.addData(req, res, SpringConstants.SYSVAR_IN_SESSION, SpringConstants.YES);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			chain.doFilter(request, response);	
		}
	}

	@Override
	public void destroy() {
		this.filterConfig = null;
	}
}